//
//  FormatterLabelField.m
//
//  Created by Jurayev Nodir on 8/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "FormatterLabelField.h"

@implementation FormatterLabelField


-(void) logicInitialization
{
    _formatter = [[SHSPhoneNumberFormatter alloc] init];
    _formatter.textField = self;
    
    logicDelegate = [[SHSPhoneLogic alloc] init];
    
    [super setDelegate:logicDelegate];
    self.keyboardType = UIKeyboardTypeNumberPad;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self logicInitialization];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self logicInitialization];
    }
    return self;
}

#pragma mark -
#pragma mark Delegates

-(void) setDelegate:(id<UITextFieldDelegate>)delegate
{
    logicDelegate.delegate = delegate;
}

-(id<UITextFieldDelegate>) delegate
{
    return logicDelegate.delegate;
}

#pragma mark -
#pragma mark Additional Text Setter

-(void) setFormattedText:(NSString *)text
{
    [SHSPhoneLogic applyFormat:(SHSPhoneTextField *)self forText:text];
}

-(NSString *) phoneNumber
{
    return [self.formatter digitOnlyString:self.text];
}

-(NSString *)phoneNumberWithoutPrefix
{
    return [self.formatter digitOnlyString:[self.text stringByReplacingOccurrencesOfString:self.formatter.prefix withString:@""]];
}


@end
