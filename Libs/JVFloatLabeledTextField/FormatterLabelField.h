//
//  FormatterLabelField.h
//
//  Created by Jurayev Nodir on 8/11/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "JVFloatLabeledTextField.h"
#import "SHSPhoneLogic.h"
#import "SHSPhoneNumberFormatter.h"
#import "SHSPhoneNumberFormatter+UserConfig.h"

@interface FormatterLabelField : JVFloatLabeledTextField
{
    SHSPhoneLogic *logicDelegate;
}

/**
 SHSPhoneNumberFormatter instance.
 Use is to configure format properties.
 */
@property (readonly, strong) SHSPhoneNumberFormatter *formatter;

/**
 Formate a text and set it to a textfield.
 */
-(void) setFormattedText:(NSString *)text;

/**
 Return phone number without format. Ex: 89201235678
 */
-(NSString *) phoneNumber;

/**
 Return phone number without format and prefix
 */
-(NSString *) phoneNumberWithoutPrefix;

/**
 Block will be called when text changed
 */
typedef void (^SHSTextBlock)(UITextField *textField);
@property (nonatomic, copy) SHSTextBlock textDidChangeBlock;
@end
