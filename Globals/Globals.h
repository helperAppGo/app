//
//  Globals.h
//  TaxiPriz
//
//  Created by Nodir on 26.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//
#ifndef Globals_h
#define Globals_h
#import "UIColor+Additions.h"
// kk
#define COLOR(r, g, b) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:1.f]
#define registerBtnColor COLOR(27, 55, 140)

#define appGreyTextColor COLOR(120, 120, 120)
#define appGreenColor [UIColor colorFromHex:@"#009C00"]
#define appStatusColor(k) (k == 1) ? appGreenColor : [UIColor clearColor]


#define KSCREEN_HEIGHT   MAX([UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)

#define IPHONE_X (KSCREEN_HEIGHT == 812.0)
#define NIL_TO_STRING(str) str == nil ? @"" : str;


#define LOCATION_UPDATED @"LOCATION_UPDATED"

#define translation(str)   [[SettingsManager instance] translate:str]

#define trCancel                    translation (@"cancel")
#define trYes                       translation (@"yes")

#define trError                     translation (@"error")
#define trReady                     translation (@"ready")
#define trShare                     translation (@"share")
#define trCity                      translation (@"city")
#define trSign                      translation (@"sign")
#define trSend                      translation (@"send")
#define trAddress                   translation (@"address")
#define trAll                       translation (@"all")
#define trNearby                    translation (@"nearby")
#define trMap                       translation (@"map")
#define trRecods                    translation (@"records")
#define trView                      translation (@"view")
#define trUnSubs                    translation (@"unsubs")
#define trSubs                      translation (@"subs")
#define trMembers                   translation (@"members")
#define trCreate                    translation (@"create")
#define trAnswers                   translation (@"answers")
#define trFriends                   translation (@"friends")
#define trRequests                  translation (@"requests")
#define trOutgoing                  translation (@"outgoing")
#define trCityList                  translation (@"cityList")
#define trAddFriend                 translation (@"addFriend")
#define trWrite                     translation (@"write")
#define trSearchSettings            translation (@"searchSettings")


#define trShareQ                    translation (@"shareQuestion")
#define trYoutubeError              translation (@"youtubeError")

#define trSignSmsDesc               translation (@"signSmsDesc")
#define trSignSmsTime               translation (@"signSmsTime")
#define trSignEnterCode             translation (@"signEnterCode")

#define trSignLastname              translation (@"lastname")
#define trSignFirstname             translation (@"firstname")
#define trSignLastnameError         translation (@"lastnameError")
#define trSignFirstnameError        translation (@"firstnameError")
#define trSignCityError             translation (@"cityError")
#define trSignMale                  translation (@"genderMale")
#define trSignFemale                translation (@"genderFemale")

#define trPermPushTop               translation (@"permissionPushTop")
#define trPermPushCenter            translation (@"permissionPushCenter")
#define trPermContactTop            translation (@"permissionContactTop")
#define trPermLocationTop           translation (@"permissionLocationTop")
#define trPermLocationCenter        translation (@"permissionLocationCenter")
#define trPermOK                    translation (@"permissionOk")

#define trChatMediaTitle            translation (@"chatMediaTitle")
#define trChatMediaPhoto            translation (@"chatMediaPhoto")
#define trChatTitle                 translation (@"chatTitle")
#define trChatFriendTitle           translation (@"chatFriendTitle")


#define trGroupInfoTitle            translation (@"groupInfoTitle")
#define trGroupInfoDesc             translation (@"groupInfoDesc")
#define trGroupInfoFull             translation (@"groupInfoFull")
#define trGroupInfoAdd              translation (@"groupInfoAdd")
#define trGroupInfoAdmin            translation (@"groupInfoAdmins")
#define trGroupInfoWeb              translation (@"groupInfoWeb")

#define trGroupInfoInvite           translation (@"groupInfoInvite")
#define trGroupInfoAddMember        translation (@"groupInfoAddMember")
#define trGroupInfoToAdmin          translation (@"groupInfoToAdmin")
#define trGroupInfoBlock            translation (@"groupInfoBlock")

#define trGroupListTitle            translation (@"groupListTitle")
#define trGroupListSelf             translation (@"groupListSelf")
#define trGroupListSettings         translation (@"groupListSettings")

#define trQuestionTitle             translation (@"questionTitle")
#define trQuestionDelete            translation (@"questionDelete")
#define trQuestionReport            translation (@"questionReport")
#define trQuestionSelCat            translation (@"questionSelectCateory")
#define trQuestionCreatePlaceholder            translation (@"questionCreatePlaceholder")

#define trProfileTitle               translation (@"profileTitle")
#define trProfileSearch              translation (@"profileSearch")
#define trProfileGroups              translation (@"profileGroups")
#define trProfileContacts            translation (@"profileContacts")
#define trProfileQuestions           translation (@"profileQuestion")
#define trProfileFollowers           translation (@"profileFollowers")
#define trProfileTakePhoto           translation (@"profileTakPhoto")
#define trProfilePhoto               translation (@"profilePhoto")
#define trProfilePhotoSelf           translation (@"profilePhotoSelf")
#define trProfileAddFeed             translation (@"profileAddFeed")

#define trRelationDelete             translation (@"relationDeleteQuestion")
#define trRelationCancelRequest      translation (@"relationCancelRequest")

#define trAddQuestionMap             translation(@"addquestionMap")
#define trUploadFile                 translation(@"uploadFile")
#define trCreatePost                 translation(@"createPost")
#define trCreateQuestion             translation(@"createQuestion")
#define trDeleteFile                 translation(@"deleteFile")

#define trFilterNews                 translation(@"filterNews")
#define trFilterFriend               translation(@"filterFriend")
#define trFilterGroup                  translation(@"filterGroup")
#define trFilterNearby                 translation(@"filterNearby")
#define trFilterVideo                  translation(@"filterVideo")
#define trFilterRecomendation          translation(@"filterRecomendation")

#define trTakeCamera          translation(@"takeCamera")
#define trTakeLibrary          translation(@"takeLibrary")



#define trFeedTitle                 translation (@"feedTitle")
#define trFeedDelete                translation (@"feedDelete")
#define trFeedReport                translation (@"feedReport")
#define trFeedBlockUser             translation (@"feedBlockUser")
#define trFeedRecomendation         translation (@"recomendation")




#endif /* Globals_h */
