//// Header
//

#import "CDCity+CoreDataClass.h"
#import "UIFont+Additions.h"
#import "NSManagedObject+ActiveRecord.h"
@implementation CDCity

+(CDCity *)createCity:(NSDictionary *)dict{
    NSInteger objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
    CDCity *city = [CDCity find:@{@"objId":@(objId)}];
    if (!city) {
        city = [CDCity create];
    }
    city.objId = objId;
    city.name = NULL_TO_NIL(dict[@"name"]);
    [city save];
    return city;
}
@end
