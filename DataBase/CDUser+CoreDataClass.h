//// Header
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDUser : NSManagedObject

+(CDUser *)createUserRegisterResponse:(NSDictionary *)response;
+(CDUser *)currentUserUpdate:(NSDictionary *)response;

-(NSString *)cityString;
-(NSString *)birthDayString;
-(NSString *)gender;
-(NSString *)nameString;

@end

NS_ASSUME_NONNULL_END

#import "CDUser+CoreDataProperties.h"
