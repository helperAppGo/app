//// Header
//

#import "CDUser+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDUser (CoreDataProperties)

+ (NSFetchRequest<CDUser *> *)fetchRequest;

@property (nonatomic) int64_t objId;
@property (nullable, nonatomic, copy) NSString *token;
@property (nullable, nonatomic, copy) NSDate *birthDay;
@property (nonatomic) int64_t friendsCount;
@property (nonatomic) int64_t followersCount;
@property (nonatomic) int64_t postsCount;
@property (nonatomic) int64_t fileAva;
@property (nonatomic) int64_t fileCover;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nonatomic) int64_t cityid;
@property (nullable, nonatomic, copy) NSString *info;
@property (nonatomic) BOOL online;
@property (nullable, nonatomic, copy) NSString *lang;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSString *avatar;
@property (nullable, nonatomic, copy) NSString *cover;
@property (nonatomic) int64_t sex;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSDate *disconnectDate;


@end

NS_ASSUME_NONNULL_END
