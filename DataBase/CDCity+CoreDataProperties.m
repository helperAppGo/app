//// Header
//

#import "CDCity+CoreDataProperties.h"

@implementation CDCity (CoreDataProperties)

+ (NSFetchRequest<CDCity *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDCity"];
}

@dynamic name;
@dynamic objId;

@end
