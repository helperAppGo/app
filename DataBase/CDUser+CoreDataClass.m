//// Header
//

#import "CDUser+CoreDataClass.h"

@implementation CDUser

+(CDUser *)createUserRegisterResponse:(NSDictionary *)response{
    NSInteger objId = [response[@"id_user"] integerValue];
    CDUser *user = [CDUser find:@{@"objId":@(objId)}];
    if (!user) {
        user = [CDUser create];
    }
    user.objId = objId;
    user.token = NULL_TO_NIL(response[@"phone_token"]);
    [user save];
    
    return user;
}

+(CDUser *)currentUserUpdate:(NSDictionary *)response{
    CDUser *user = [[SettingsManager instance] currentUser];
    
    
    NSInteger day = [NULL_TO_NIL(response[@"b_day"]) integerValue];
    NSInteger month = [NULL_TO_NIL(response[@"b_month"]) integerValue];
    NSInteger year = [NULL_TO_NIL(response[@"b_year"]) integerValue];
    NSString *dateString = [NSString stringWithFormat:@"%02ld.%02ld.%ld",(long)day,(long)month,(long)year];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    user.birthDay = [formatter dateFromString:dateString];
   
    user.followersCount = [NULL_TO_NIL(response[@"count_followers"]) integerValue];
    user.friendsCount = [NULL_TO_NIL(response[@"count_friends"]) integerValue];
    user.postsCount = [NULL_TO_NIL(response[@"count_posts"]) integerValue];
    
    user.fileAva = [NULL_TO_NIL(response[@"file_ava"]) integerValue];
    user.fileCover = [NULL_TO_NIL(response[@"file_cover"]) integerValue];
    
    user.firstName = NULL_TO_NIL(response[@"first_name"]);
    user.lastName = NULL_TO_NIL(response[@"last_name"]);
    
    user.cityid = [NULL_TO_NIL(response[@"id_city"]) integerValue];
    user.info = NULL_TO_NIL(response[@"info"]);
    user.online = [NULL_TO_NIL(response[@"is_online"]) integerValue] == 1;
    user.lang = NULL_TO_NIL(response[@"lang"]);
    user.cover = NULL_TO_NIL(response[@"path_cover"]);
    user.sex = [NULL_TO_NIL(response[@"sex"]) integerValue];
    user.status = NULL_TO_NIL(response[@"status"]);
    user.avatar = NULL_TO_NIL(response[@"path_ava"]);
    [user save];
    
    return user;
}

-(NSString *)birthDayString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    return [formatter stringFromDate:self.birthDay];
}

-(NSString *)cityString{
    CDCity *city = [CDCity find:@{@"objId":@(self.cityid)}];
    return city.name;;
}

-(NSString *)gender{
    return self.sex == 0 ? @"Женский":@"Мужской";
}
-(NSString *)nameString{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

@end
