//// Header
//

#import "CDUser+CoreDataProperties.h"

@implementation CDUser (CoreDataProperties)

+ (NSFetchRequest<CDUser *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CDUser"];
}

@dynamic objId;
@dynamic token;
@dynamic birthDay;
@dynamic friendsCount;
@dynamic followersCount;
@dynamic postsCount;
@dynamic fileAva;
@dynamic fileCover;
@dynamic firstName;
@dynamic cityid;
@dynamic info;
@dynamic online;
@dynamic lang;
@dynamic lastName;
@dynamic avatar;
@dynamic cover;
@dynamic sex;
@dynamic status;
@dynamic disconnectDate;

@end
