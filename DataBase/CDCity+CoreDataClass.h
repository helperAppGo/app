//// Header
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface CDCity : NSManagedObject

+(CDCity *)createCity:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "CDCity+CoreDataProperties.h"
