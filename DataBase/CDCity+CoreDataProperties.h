//// Header
//

#import "CDCity+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CDCity (CoreDataProperties)

+ (NSFetchRequest<CDCity *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) int64_t objId;

@end

NS_ASSUME_NONNULL_END
