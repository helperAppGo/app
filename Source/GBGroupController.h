//
//  GBGroupController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/5/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBAllGroupController.h"
#import "GBGEOController.h"
#import "GBSwitcher.h"

@interface GBGroupController : UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) GBAllGroupController *allGroup;
@property (nonatomic, strong) GBGEOController *geoGroup;
@property (strong, nonatomic) GBSwitcher *switcher;

//@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentedController;
@property (strong, nonatomic) IBOutlet UIView *bottonview;//contaiinerView

- (IBAction)notificationBarButton:(id)sender;
- (IBAction)addGroupBarButton:(id)sender;

// View
- (void) fullfillToBottomView:(UIView *)subView ;

@end
