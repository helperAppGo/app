//
//  GBWallCell.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBWallCell.h"

@interface GBWallCell()
@property (strong, nonatomic) NSMutableArray *items;

@property (strong, nonatomic) UIView *head;
@property (strong, nonatomic) UIImageView *avaGroupImg;
@property (strong, nonatomic) UILabel *nameGroupLabel;
@property (strong, nonatomic) UILabel *dateGroupLabel;
@property (strong, nonatomic) UIButton *moreButton;
@property (strong, nonatomic) UIButton *adButton;

@property (strong, nonatomic) UILabel *textGroupLabel;
@property (strong, nonatomic) UIImageView *imageGroupImg;

@property (strong, nonatomic) UIView *likeBar;
@property (strong, nonatomic) GBButtonLike *likeButton;
@property (strong, nonatomic) UIButton *followButton;
@property (strong, nonatomic) UIButton *commentsButton;

@property (nonatomic, strong) UIButton *btnUser;


- (IBAction)followAction:(id)sender;
- (IBAction)onBtnLike:(UIButton*)sender;
- (IBAction)commentAction:(id)sender;


@end

@implementation GBWallCell 

- (void)configureWithModel:(GBWallModel *)wallModel delegate:(id<GBStreamItemCellDelegate>)delegate
{
    self.model = wallModel;
    
    self.delegate = delegate;
    
    [self.contentView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
    // - HEAD ----------------
    self.head = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 44)];
    self.head.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.head];
    
    
    // - ADD LINE ----------------
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 3)];
    lineView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.contentView addSubview:lineView];
    
    
    // - PATH AVA ----------------
    self.avaGroupImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 40, 40)];
    self.avaGroupImg.layer.cornerRadius = self.avaGroupImg.frame.size.width/2;
    self.avaGroupImg.layer.masksToBounds = YES;
    [self.avaGroupImg sd_setImageWithURL:wallModel.path_ava placeholderImage:[UIImage imageNamed:@"user"]];
    [self.head addSubview:self.avaGroupImg];
    
    
// - NAME GROUP ----------------
    CGRect nameFrame = CGRectMake(self.avaGroupImg.bounds.size.width+25, 12, [[GBNotAdminGroupController alloc] init].view.bounds.size.width*0.5, 20);
    self.nameGroupLabel = [[UILabel alloc] initWithFrame:nameFrame];
    self.nameGroupLabel.text = wallModel.name;
    [self.nameGroupLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
    [self.head addSubview:self.nameGroupLabel];
    
    self.btnUser = [[UIButton alloc] initWithFrame:CGRectMake(self.avaGroupImg.frame.origin.x, self.avaGroupImg.frame.origin.y, nameFrame.origin.x + nameFrame.size.width - self.avaGroupImg.frame.origin.x, self.avaGroupImg.frame.size.height)];
    [self.btnUser addTarget:self action:@selector(onBtnAvatar:) forControlEvents:UIControlEventTouchUpInside];
    [self.head addSubview:self.btnUser];
    
    
// - MORE BUTTON ----------------
    self.moreButton = [[UIButton alloc] initWithFrame:CGRectMake([[GBNotAdminGroupController alloc] init].view.bounds.size.width - 29, 10, 24, 24)];
    [self.moreButton setImage:[UIImage imageNamed:@"more"] forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(onBtnMore:) forControlEvents:UIControlEventTouchUpInside];
    [self.head addSubview:self.moreButton];
    
    
    // - DATE ----------------
    self.dateGroupLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.moreButton.frame)-70, 16, 70, 12)];
    self.dateGroupLabel.textAlignment = 1;
    [self.dateGroupLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    
    self.dateGroupLabel.textColor = [UIColor grayColor];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    self.dateGroupLabel.text = [formatter stringFromDate:wallModel.date];
    [self.head addSubview:self.dateGroupLabel];
    
    
// - TEXT ----------------
    if(wallModel.text) {
        self.textGroupLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 49, [[GBNotAdminGroupController alloc] init].view.bounds.size.width-10, 20)];
        self.textGroupLabel.text = wallModel.text;
        [self.textGroupLabel setFont:[UIFont fontWithName:@"Helvetica" size:14]];
        [self.contentView addSubview:self.textGroupLabel];
    }
    
    
// - IMAGE ----------------
    if (!(wallModel.isNoImageGroup)) {
        CGFloat imageStartHeigh = 55;
        if (wallModel.text) {
            imageStartHeigh = CGRectGetMaxY(self.textGroupLabel.frame);
        }
        self.imageGroupImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, imageStartHeigh, wallModel.widthObjects, wallModel.heightObjects)];
        
        if (wallModel.loadError) {
            [self.imageGroupImg setImage:[UIImage imageNamed:@"imageError.png"]];
        } else {
            [self.imageGroupImg setImageWithURL: wallModel.pathUrl];
        }
        [self.contentView addSubview:self.imageGroupImg];
    }
    
    
    // - LIKE BAR ----------------
    CGFloat likeBarStartHeigh;
    if (wallModel.isNoImageGroup){
        likeBarStartHeigh = CGRectGetMaxY(self.textGroupLabel.frame);
        
    } else {
        likeBarStartHeigh = CGRectGetMaxY(self.imageGroupImg.frame);
    }
    
    self.likeBar = [[UIView alloc] initWithFrame:CGRectMake(0, likeBarStartHeigh, [UIScreen mainScreen].bounds.size.width, 40)];
    self.likeBar.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.likeBar];
    
    
    // - LIKE ICON + COUNT LIKES ----------------
    self.likeButton = [[GBButtonLike alloc] initWithModel:self.model];
    self.likeButton.model = self.model;
    [self.likeButton setFrame:CGRectMake(5, 8, 60, 24)];
    NSString *nameLikeIcon;
    if (self.model.is_liked) {
        nameLikeIcon = @"ic_like_color";
        self.likeButton.tag = 1;
    } else {
        nameLikeIcon = @"ic_like_gray";
        self.likeButton.tag = 2;
    }
    
    [self.likeButton setImage:[UIImage imageNamed:nameLikeIcon] forState:UIControlStateNormal];
    [self.likeButton addTarget:self action:@selector(onBtnLike:) forControlEvents:UIControlEventTouchUpInside];
    [self.likeButton setClipsToBounds:false];
    [self.likeButton setTitle:[NSString stringWithFormat:@"%ld", (long)wallModel.countLikes] forState:UIControlStateNormal];
    [self.likeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.likeButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [self.likeButton setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
    [self.likeButton setImageEdgeInsets:UIEdgeInsetsMake(0.f, -30.f, 0.f, 0.f)];
    [self.likeBar addSubview:self.likeButton];
    
    
    // - COMMENT ICON + COUNT COMMENTS ----------------
    self.commentsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.commentsButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width - 65, 8, 60, 24)];
    [self.commentsButton setImage:[UIImage imageNamed:@"ic_comment"] forState:UIControlStateNormal];
    [self.commentsButton setClipsToBounds:false];
    [self.commentsButton setTitle:[NSString stringWithFormat:@"%ld", (long)wallModel.countComments] forState:UIControlStateNormal];
    [self.commentsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.commentsButton.titleLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [self.commentsButton setTitleEdgeInsets:UIEdgeInsetsMake(0.f, 0.f, 0.f, 0.f)];
    [self.commentsButton setImageEdgeInsets:UIEdgeInsetsMake(0.f, -30.f, 0.f, 0.f)];
    [self.commentsButton addTarget:self  action:@selector(onBtnComment:) forControlEvents:UIControlEventTouchUpInside];
    [self.likeBar addSubview:self.commentsButton];
    
    
    // - SHARE ----------------
    self.followButton = [[UIButton alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.commentsButton.frame)-44, 8, 24, 24)];
    [self.followButton setImage:[UIImage imageNamed:@"ic_share"] forState:UIControlStateNormal];
    [self.likeBar addSubview:self.followButton];
}


-(void) onBtnLike:(GBButtonLike *)sender{
    NSString *nameLikeIcon;
    NSLog(@"%ld", (long)sender.model.idCountWall);
    
    [GBFeedDataSource likePostGroup:sender.model.idCountWall completion:^(BOOL result) {}];
    if (sender.model.is_liked) {
        nameLikeIcon = @"ic_like_gray";
    } else {
        nameLikeIcon = @"ic_like_color";
    }
    [sender.model likeAction];
    [sender setImage:[UIImage imageNamed:nameLikeIcon] forState:UIControlStateNormal];
    [sender setTitle:[NSString stringWithFormat:@"%ld", (long)sender.model.countLikes] forState:UIControlStateNormal];
    
//    NSInteger intID = [model.idCountWall integerValue];
//    
//    [[GBServerManager sharedManager] likeGroupPostId:intID success:^(id responseObject) {
//        NSLog(@"like post %@ succes", model.idCountWall );
//    } failure:^(NSError *error, NSDictionary *userInfo) {
//        NSLog(@"fail like post %@", model.idCountWall );
//    }];
}


#pragma mark - GBStreamItemCellProtocol

- (int64_t) streamItemId
{
    return self.model.idCountWall;
}

- (int64_t) userId
{
    return (self.model.idUser.longLongValue == self.model.idWall.longLongValue) ? self.model.idUser.longLongValue : 0;
}

@end

