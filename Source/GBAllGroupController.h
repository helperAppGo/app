//
//  GBAllGroupController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/6/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBServerManager.h"
#import "GBAllGroupCell.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBDataParser.h"
#import "UIScrollView+InfiniteScroll.h"
#import "UIApplication+NetworkIndicator.h"
#import "CustomInfiniteIndicator.h"
#import "GBNotAdminGroupController.h"
#import "GBAdminGroupController.h"

@interface GBAllGroupController : UIViewController <UITextViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    GBDataParser *dataParser;
    MainDataHolder *dataHolder;
}

@property (weak, nonatomic) IBOutlet UITableView *allGroupTableView;
@property(nonatomic) UIEdgeInsets separatorInset;

-(void) getAllGroup:(void(^)(void))completion;
-(void) refresh;

@end
