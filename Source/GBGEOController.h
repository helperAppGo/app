//
//  GBGEOController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/6/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBServerManager.h"
#import "GBAllGroupCell.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBDataParser.h"
#import "UIScrollView+InfiniteScroll.h"
#import "UIApplication+NetworkIndicator.h"
#import "CustomInfiniteIndicator.h"
#import "GBNotAdminGroupController.h"
#import "GBAdminGroupController.h"

@interface GBGEOController : UIViewController <UITextViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    GBDataParser *dataParser;
    MainDataHolder *dataHolder;
}

@property (weak, nonatomic) IBOutlet UITableView *geoGroupTableView;
@property(nonatomic) UIEdgeInsets separatorInset;

-(void) getGeoGroup:(void(^)(void))completion;
-(void) refresh;

@end
