//
//  GBButtonLike.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBWallModel.h"

@interface GBButtonLike : UIButton

@property (strong, nonatomic) GBWallModel *model;

- (instancetype)initWithModel:(GBWallModel*)model;

@end


