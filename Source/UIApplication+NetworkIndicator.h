//
//  UIApplication+NetworkIndicator.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/9/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (NetworkIndicator)

- (void)startNetworkActivity;
- (void)stopNetworkActivity;

@end
