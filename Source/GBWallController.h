//
//  GBWallController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/16/17.
//  Copyright © 2017 SG. All rights reserved.
//

//#import <UIKit/UIKit.h>
//#import "iCarousel.h"
//#import "GBWallCell.h"
//#import "GBServerManager.h"
//#import "UGSFetchController.h"
//#import "UGSSuperFetch.h"
//#import "GBDataParser.h"
//#import "UIScrollView+InfiniteScroll.h"
//#import "UIApplication+NetworkIndicator.h"
//#import "CustomInfiniteIndicator.h"
//#import "GBNotAdminGroupController.h"
//#import "GBAdminGroupController.h"
//
//
//@interface GBWallController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
//    GBDataParser *dataParser;
//    MainDataHolder *dataHolder;
//}
//
//@property (weak, nonatomic) IBOutlet UITableView *wallTableView;
//@property (strong, nonatomic) NSMutableArray *jsonGroup;
//
//@property (strong,nonatomic) NSNumber* idGroupInWall;
//
//@end
