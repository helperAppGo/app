//
//  GBGroupController.m
//  GoBeside
//
//  Created by P on 9/5/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBGroupController.h"

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface GBGroupController ()

@end

@implementation GBGroupController
@synthesize allGroup, geoGroup;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    allGroup = [storyboard instantiateViewControllerWithIdentifier:@"GBAllGroupController"];
    geoGroup = [storyboard instantiateViewControllerWithIdentifier:@"GBGEOController"];
    
    [self switchSegmentedController];
}


-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    //    self.navigationController.navigationBar.shadowImage = nil;
    //    self.navigationController.navigationBar.tintColor = nil;
    //    self.navigationController.navigationBar.translucent = false;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) switchSegmentedController {
    // SHOW FIRST VIEW CONTROLLER -----
    [allGroup removeFromParentViewController];
    [allGroup.view removeFromSuperview];
    [self addChildViewController:allGroup];
    [self fullfillToBottomView:allGroup.view];
    
    // SWITCHER -----
    NSInteger margin = 57;
    
    self.switcher = [[GBSwitcher alloc] initWithStringsArray:@[@"Групи", @"Біля мене"]];
    self.switcher.frame = CGRectMake(margin, 20, 200, 30);
    self.switcher.backgroundColor = Rgb2UIColor(214, 214, 214);
    self.switcher.labelTextColorOutsideSlider = Rgb2UIColor(0, 0, 0);
    self.switcher.labelTextColorInsideSlider = Rgb2UIColor(7, 70, 151);
    self.navigationItem.titleView = self.switcher;
    [self.switcher setPressedHandler:^(NSUInteger index) {
        
        if(index == 0){
            NSLog(@"Did press position on switch at index: %lu", (unsigned long)index);
            
            [allGroup removeFromParentViewController];
            [allGroup.view removeFromSuperview];
            [self addChildViewController:allGroup];
            [self fullfillToBottomView:allGroup.view];
            
        } else {
            NSLog(@"Did press position on switch at index: %lu", (unsigned long)index);
            
            [geoGroup removeFromParentViewController];
            [geoGroup.view removeFromSuperview];
            [self addChildViewController:geoGroup];
            [self fullfillToBottomView:geoGroup.view];
        }
    }];
}


-(void) fullfillToBottomView:(UIView *)subView {
    [self.bottonview addSubview:subView];
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.bottonview addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.bottonview attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
    [self.bottonview addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.bottonview attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    [self.bottonview addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.bottonview attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [self.bottonview addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.bottonview attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
}


- (IBAction)notificationBarButton:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@"COMING SOON..."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)addGroupBarButton:(id)sender {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@"COMING SOON..."
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
}

@end
