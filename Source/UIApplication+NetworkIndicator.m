//
//  UIApplication+NetworkIndicator.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/9/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "UIApplication+NetworkIndicator.h"

@implementation UIApplication (NetworkIndicator)

#if TARGET_OS_TV

- (void)startNetworkActivity {}
- (void)stopNetworkActivity {}

#else

static NSInteger networkActivityCount = 0;

- (void)startNetworkActivity {
    networkActivityCount++;
    
    [self setNetworkActivityIndicatorVisible:YES];
}

- (void)stopNetworkActivity {
    if(networkActivityCount < 1) {
        return;
    }
    
    if(--networkActivityCount == 0) {
        [self setNetworkActivityIndicatorVisible:NO];
    }
}

#endif

@end

