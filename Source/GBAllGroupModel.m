//
//  GBAllGroupModel.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/7/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBAllGroupModel.h"

#define   SHOW_LOGS             NO
#define   Error(format, ...)    if (SHOW_LOGS) NSLog(@"ERROR: %@", [NSString stringWithFormat:format, ## __VA_ARGS__]);

@implementation GBAllGroupModel

+ (NSArray *) getObjectsFromArray:(NSArray *)data {
    if ((id)data == [NSNull null]) {
        return @[];
    }
    
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSDictionary *dict in data) {
        @try {
            [result insertObject:[GBAllGroupModel allGroupFromDictionary:dict] atIndex:0];
        }
        @catch (NSException *exception) {
            Error(@"%@", exception);
        }
    }
    return result;
}


+(GBAllGroupModel *) allGroupFromDictionary:(NSDictionary *)data {
    GBAllGroupModel *allGroup = [[GBAllGroupModel alloc] init];
    
    allGroup.address = data[@"address"];
    allGroup.can_post = data[@"can_post"];
    allGroup.count_followers = data[@"count_followers"];
    allGroup.distance = data[@"distance"];
    allGroup.file_ava = data[@"file_ava"];
    allGroup.file_cover = data[@"file_cover"];
    allGroup.idGroup = data[@"id"];
    allGroup.id_category = data[@"id_category"];
    allGroup.id_city = data[@"id_city"];
    allGroup.is_admin = data[@"is_admin"];
    allGroup.is_banned = data[@"is_banned"];
    allGroup.is_member = [data[@"is_member"]intValue];
    allGroup.lat = data[@"lat"];
    allGroup.lon = data[@"lon"];
    allGroup.name = data[@"name"];
    allGroup.path_ava = [NSURL URLWithString:data[@"path_ava"]];
    allGroup.path_cover = data[@"path_cover"];
    allGroup.short_description = data[@"short_description"];
    allGroup.type = data[@"type"];
    allGroup.web_site = data[@"web_site"];
    
    return allGroup;
}




//+(NSArray *)getApiGroupObjectsFromArray:(NSArray *)data {
//    if ((id)data == [NSNull null]) {
//        return @[];
//    }
//    
//    NSMutableArray *result = [NSMutableArray array];
//    
//    for (NSDictionary *dict in data) {
//        @try {
//            [result insertObject:[GBAllGroupModel allApiGroupFromDictionary:dict] atIndex:0];
//        }
//        @catch (NSException *exception) {
//            Error(@"%@", exception);
//        }
//    }
//    return result;
//    
//}


//+(GBAllGroupModel *) allApiGroupFromDictionary:(NSDictionary *)data {
//    GBAllGroupModel *allApiGroup = [[GBAllGroupModel alloc] init];

//    if ([data isEqual:@"path_ava"]) {
//        allApiGroup.path_ava = data[@"path_ava"];
//        NSLog(@"PATH AVA: %@", data[@"path_ava"]);
//    } else if ([data isEqual:@"path_cover"]) {
//        allApiGroup.path_ava = data[@"path_cover"];
//    }
//    
//    if ([data isEqual:@"address"]) {
//        allApiGroup.address = data[@"address"];
//    }
    
//    for (int i = 0; i < data.count; i++) {
//        allApiGroup.address = [[data objectAtIndex:i] objectForKey:@"address"];
//        allApiGroup.can_post = [[data objectAtIndex:i] objectForKey:@"can_post"];
//        allApiGroup.count_followers = [[data objectAtIndex:i] objectForKey:@"count_followers"];
//    }
    
//    allApiGroup.address = data[@"address"];
//    allApiGroup.can_post = data[@"can_post"];
    
//    return allApiGroup;
//}



- (void) followAction {
    
    if(self.is_member){
        
        self.is_member = FALSE;
//        self.countLikes--;
        //post liked
    }
    else
    {
        self.is_member = TRUE;
//        self.countLikes++;
        //Delete POST
    }
}


@end
