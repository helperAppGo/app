//  FRHyperTextView.h
//
//  Created by Jinghan Wang on 23/9/15.
//  Copyright © 2015 JW. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^FRMentionHandler)(id container, NSString *selection);

@interface FRHyperTextView : UITextView

@property (nonatomic) NSDictionary *linkAttributeDefault;
@property (nonatomic) NSDictionary *linkAttributeHighlight;

- (void)setLinkForRange:(NSRange)range withAttributes:(NSDictionary *)attributes andLinkHandler:(void (^)(FRHyperTextView *label, NSRange selectedRange))handler;
- (void)setLinkForRange:(NSRange)range withLinkHandler:(void(^)(FRHyperTextView *label, NSRange selectedRange))handler;

- (void)setLinkForSubstring:(NSString *)substring withAttribute:(NSDictionary *)attribute andLinkHandler:(FRMentionHandler)handler;
- (void)setLinkForSubstring:(NSString *)substring withLinkHandler:(FRMentionHandler)handler;
- (void)setLinksForSubstrings:(NSArray *)substrings withLinkHandler:(FRMentionHandler)handler;
- (void)clearActionDictionary;

- (void)fillWithText:(NSString *)text mentions:(NSArray *)mentions handler:(FRMentionHandler)handler;

@end
