//  FRHyperTextView.m
//
//  Created by Jinghan Wang on 23/9/15.
//  Copyright © 2015 JW. All rights reserved.
//

#import "FRHyperTextView.h"

@interface FRHyperTextView ()
@property (nonatomic) NSMutableDictionary *handlerDictionary;
@property (nonatomic) NSAttributedString *backupAttributedText;

@end

@implementation FRHyperTextView

static CGFloat highLightAnimationTime = 0.15;
static UIColor *__HyperLinkColorDefault;
static UIColor *__HyperLinkColorHighlight;

+ (void)initialize
{
    if (self == [FRHyperTextView class])
    {
        __HyperLinkColorDefault = [UIColor colorWithRed:28/255.0
                                                  green:135/255.0
                                                   blue:199/255.0 alpha:1];
        __HyperLinkColorHighlight = [UIColor lightGrayColor];
    }
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self checkInitialization];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self)
    {
        [self checkInitialization];
    }
    
    return self;
}

- (void)checkInitialization
{
    if (!self.handlerDictionary)
    {
        self.handlerDictionary = [NSMutableDictionary new];
    }
    
    if (!self.userInteractionEnabled)
    {
        self.userInteractionEnabled = YES;
    }
    
    if (!self.linkAttributeDefault)
    {
        self.linkAttributeDefault = @{NSForegroundColorAttributeName: __HyperLinkColorDefault,
                                      NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    }
    
    if (!self.linkAttributeHighlight)
    {
        self.linkAttributeHighlight = @{NSForegroundColorAttributeName: __HyperLinkColorHighlight,
                                        NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    }
}

#pragma mark - APIs

- (void)clearActionDictionary
{
    [self.handlerDictionary removeAllObjects];
}

//designated setter
- (void)setLinkForRange:(NSRange)range withAttributes:(NSDictionary *)attributes andLinkHandler:(void (^)(FRHyperTextView *label, NSRange selectedRange))handler
{
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    
    if (attributes)
    {
        [mutableAttributedString addAttributes:attributes range:range];
    }
    
    if (handler)
    {
        [self.handlerDictionary setObject:handler forKey:[NSValue valueWithRange:range]];
    }
    
    self.attributedText = mutableAttributedString;
}

- (void)setLinkForRange:(NSRange)range withLinkHandler:(void(^)(FRHyperTextView *label, NSRange selectedRange))handler
{
    [self setLinkForRange:range withAttributes:self.linkAttributeDefault andLinkHandler:handler];
}

- (void)setLinkForSubstring:(NSString *)substring withAttribute:(NSDictionary *)attribute andLinkHandler:(FRMentionHandler)handler
{
    NSRange range = NSMakeRange(0, self.attributedText.string.length);
    
    do
    {
        range = [self.attributedText.string rangeOfString:substring options:NSCaseInsensitiveSearch range:range];
        
        if (range.location != NSNotFound)
        {
            [self setLinkForRange:range withAttributes:attribute andLinkHandler:^(FRHyperTextView *label, NSRange range)
            {
                handler(label, [label.attributedText.string substringWithRange:range]);
            }];
            
            range = NSMakeRange(range.location+1, self.attributedText.string.length - range.location-1);
        }
    }
    while (range.location != NSNotFound);
}

- (void)setLinkForSubstring:(NSString *)substring withLinkHandler:(FRMentionHandler)handler
{
    [self setLinkForSubstring:substring withAttribute:self.linkAttributeDefault andLinkHandler:handler];
}

- (void)setLinksForSubstrings:(NSArray *)linkStrings withLinkHandler:(FRMentionHandler)handler
{
    [self clearActionDictionary];
    
    for (NSString *linkString in linkStrings) {
        [self setLinkForSubstring:linkString withLinkHandler:handler];
    }
}

#pragma mark - Event Handler
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backupAttributedText = self.attributedText;
    
    for (UITouch *touch in touches)
    {
        CGPoint touchPoint = [touch locationInView:self];
        NSValue *rangeValue = [self attributedTextRangeForPoint:touchPoint];
        
        if (rangeValue)
        {
            NSRange range = [rangeValue rangeValue];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
            
            [attributedString addAttributes:self.linkAttributeHighlight range:range];
            
            [UIView transitionWithView:self duration:highLightAnimationTime options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                self.attributedText = attributedString;
            } completion:nil];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [UIView transitionWithView:self duration:highLightAnimationTime options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.attributedText = self.backupAttributedText;
    } completion:nil];
    
    for (UITouch *touch in touches)
    {
        NSValue *rangeValue = [self attributedTextRangeForPoint:[touch locationInView:self]];
        
        if (rangeValue)
        {
            void(^handler)(FRHyperTextView *label, NSRange selectedRange) = self.handlerDictionary[rangeValue];
            handler(self, [rangeValue rangeValue]);
        }
    }
}

#pragma mark - Substring Locator

- (NSValue *)attributedTextRangeForPoint:(CGPoint)point
{
    NSLayoutManager *layoutManager = [NSLayoutManager new];
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    
    textContainer.lineFragmentPadding = 0.0;
//    textContainer.lineBreakMode = self.lineBreakMode;
//    textContainer.maximumNumberOfLines = self.numberOfLines;
    textContainer.size = self.bounds.size;
    [layoutManager addTextContainer:textContainer];
    
    //textStorage to calculate the position
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:self.attributedText];
    [textStorage addAttribute:NSFontAttributeName value:self.font range:NSMakeRange(0, self.text.length)];
    [textStorage addLayoutManager:layoutManager];
    
    
    // find the tapped character location and compare it to the specified range
    CGPoint locationOfTouchInLabel = point;
//    CGRect textBoundingBox = [layoutManager usedRectForTextContainer:textContainer];
    CGPoint textContainerOffset = CGPointZero;
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x, locationOfTouchInLabel.y - textContainerOffset.y);
    NSInteger indexOfCharacter = [layoutManager characterIndexForPoint:locationOfTouchInTextContainer inTextContainer:textContainer fractionOfDistanceBetweenInsertionPoints:nil];
    
    for (NSValue *rangeValue in self.handlerDictionary)
    {
        NSRange range = [rangeValue rangeValue];
        
        if (NSLocationInRange(indexOfCharacter, range))
        {
            return rangeValue;
        }
    }
    
    return nil;
}

- (void)fillWithText:(NSString *)text mentions:(NSArray *)mentions handler:(FRMentionHandler)handler
{
    NSDictionary *attributes = @{NSForegroundColorAttributeName: [GBCommonStyles colour_505c6c],
                                 NSFontAttributeName: [UIFont systemFontOfSize:15 weight:UIFontWeightLight]
                                 };
    self.attributedText = [[NSAttributedString alloc] initWithString:text attributes:attributes];
    
    self.linkAttributeDefault = @{NSForegroundColorAttributeName: [GBCommonStyles colour_67c0ef],
                                          NSFontAttributeName: [UIFont systemFontOfSize:15 weight:UIFontWeightLight],
                                          NSUnderlineStyleAttributeName: @(NSUnderlineStyleNone)};
    
    NSArray *tagschemes = [NSArray arrayWithObjects:NSLinguisticTagSchemeLanguage, nil];
    NSLinguisticTagger *tagger = [[NSLinguisticTagger alloc] initWithTagSchemes:tagschemes options:0];
    [tagger setString:[[NSString stringWithFormat:@"%@", text] substringToIndex:1]];
    
    NSString *language = [tagger tagAtIndex:0 scheme:NSLinguisticTagSchemeLanguage tokenRange:NULL sentenceRange:NULL];
    self.textAlignment = ([language isEqualToString:@"ar"]) ? NSTextAlignmentRight : NSTextAlignmentLeft;
    
    [self setLinksForSubstrings:mentions withLinkHandler:handler];
}

@end
