//
//  NSDate+Extention.h
//
//  Created by Ruslan Mishin on 19.08.18.
//

@interface NSDate (Extention)

+ (NSCalendar *)currentCalendar;

+ (int64_t) timestampBasedUUID;

+ (NSDate*) dateWithYear:(NSUInteger)year month:(NSUInteger)month;

- (NSDate *)dateByAddingDays:(NSInteger)dDays;
- (NSDate *)dateBySubtractingDays:(NSInteger)dDays;
- (NSDate *)dateByAddingWeeks:(NSInteger)dWeeks;
- (NSDate *)dateBySubtractingWeeks:(NSInteger)dWeeks;
- (NSDate *)dateByAddingMonths:(NSInteger)dMonths;
- (NSDate *)dateBySubtractingMonths:(NSInteger)dMonths;
- (NSDate *)dateByAddingYears:(NSInteger)dYears;
- (NSDate *)dateBySubtractingYears:(NSInteger)dYears;


- (NSDate*)startOfDay;
- (NSDate*)firstDayOfWeek;
- (NSDate*)firstDayOfMonth;

+ (NSInteger)dateDifferenceFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate calendarUnit:(NSCalendarUnit)calendarUnit;
- (NSString*)intervalString:(BOOL)groupNearest;

@end
