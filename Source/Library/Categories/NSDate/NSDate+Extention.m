//
//  NSDate+Extention.m
//
//  Created by Ruslan Mishin on 19.08.18.
//

#import "NSDate+Extention.h"

@implementation NSDate (Extention)

+ (NSCalendar *)currentCalendar
{
    static NSCalendar * sharedNSDateCalendar;
    static dispatch_once_t sharedNSDateCalendar_once;
    dispatch_once(&sharedNSDateCalendar_once, ^{
        sharedNSDateCalendar = [NSCalendar autoupdatingCurrentCalendar];
//        sharedNSDateCalendar.firstWeekday = 2;
    });
    
    return sharedNSDateCalendar;
}

+ (int64_t) timestampBasedUUID
{
    return (int64_t)([[NSDate date] timeIntervalSinceReferenceDate] * 1000);
}

+ (NSDate*) dateWithYear:(NSUInteger)year month:(NSUInteger)month
{
    NSDateComponents * components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

- (NSDate *)dateByAddingDays:(NSInteger)dDays {
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:dDays];
    NSDate *newDate = [[NSDate currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    return newDate;
}

- (NSDate *)dateBySubtractingDays:(NSInteger)dDays {
    return [self dateByAddingDays: (dDays * -1)];
}

- (NSDate *)dateByAddingWeeks:(NSInteger)dWeeks {
    NSDate *newDate = [self dateByAddingDays:dWeeks*7];
    return newDate;
}

- (NSDate *)dateBySubtractingWeeks:(NSInteger)dWeeks {
    return [self dateByAddingWeeks: (dWeeks * -1)];
}

- (NSDate *)dateByAddingMonths:(NSInteger)dMonths {
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setMonth:dMonths];
    NSDate *newDate = [[NSDate currentCalendar] dateByAddingComponents:dateComponents toDate:self options:0];
    return newDate;
}

- (NSDate *)dateBySubtractingMonths:(NSInteger)dMonths {
    return [self dateByAddingMonths: (dMonths * -1)];
}

- (NSDate *)dateByAddingYears:(NSInteger)dYears {
    NSCalendar *cal = [NSCalendar currentCalendar];
    return [cal dateByAddingUnit:NSCalendarUnitYear value:1 toDate:self options:0];
}

- (NSDate *)dateBySubtractingYears:(NSInteger)dYears {
    return [self dateByAddingYears: (dYears * -1)];
}

- (NSDate*)startOfDay
{
    NSCalendar * calendar = [NSDate currentCalendar];
    NSDateComponents * components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitNanosecond | NSCalendarUnitTimeZone) fromDate:self];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    [components setNanosecond:0];
    
    return [calendar dateFromComponents:components];
}

- (NSDate*)firstDayOfWeek
{
    NSCalendar * calendar = [NSDate currentCalendar];
    NSDate * date = [self startOfDay];
    
    NSDateComponents * components = [calendar components:(NSCalendarUnitYearForWeekOfYear | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekOfYear |  NSCalendarUnitWeekday | NSCalendarUnitTimeZone) fromDate:date];
    
    [components setWeekday:[calendar firstWeekday]]; //because 1 is Sunday, but need to start from Monday
    
    return [calendar dateFromComponents:components];
}

- (NSDate*)firstDayOfMonth
{
    NSCalendar * calendar = [NSDate currentCalendar];
    NSDate * date = [self startOfDay];
    NSDateComponents *comp = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitTimeZone) fromDate:date];
    [comp setDay:1];
    return [calendar dateFromComponents:comp];
}

+ (NSInteger)dateDifferenceFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate calendarUnit:(NSCalendarUnit)calendarUnit
{
    
    if(!fromDate || !toDate)
        return 0;
    
    NSDateComponents *difference = [[NSCalendar currentCalendar] components:calendarUnit fromDate:fromDate toDate:toDate options:0];
    
    NSInteger diff = 0;
    if(calendarUnit == NSCalendarUnitDay)
        diff = [difference day];
    
    else if(calendarUnit == NSCalendarUnitHour)
        diff = [difference hour];
    
    else if(calendarUnit == NSCalendarUnitMinute)
        diff = [difference minute];
    
    return diff;
}

+ (NSString *)countdownMessageWithDays:(NSInteger)days hours:(NSInteger)hours minutes:(NSInteger)minutes
{
    NSString *countdown = nil;
    if(days > 0)
        countdown = [[@(days) stringValue] stringByAppendingFormat:@"d"];
    
    else if(hours > 0)
        countdown = [[@(hours) stringValue] stringByAppendingFormat:@"h"];
    
    else if(minutes > 0)
        countdown = [[@(minutes) stringValue] stringByAppendingFormat:@"m"];
    else
        countdown = @"now";
    
    return countdown;
}

- (NSString*)intervalString:(BOOL)groupNearest
{
    NSDate * currentDate = [NSDate date];
    
    
    BOOL isSelfFuture = ([currentDate compare:self] == NSOrderedAscending);
    
    if (groupNearest) {
        if (isSelfFuture) {
            NSDate * todayEnd = [[[currentDate dateByAddingDays:1] startOfDay] dateByAddingTimeInterval:-0.001];
            if ([todayEnd compare:self] == NSOrderedDescending) {
                return NSLocalizedString(@"Today", nil);
            }else if ([[todayEnd dateByAddingDays:1] compare:self] == NSOrderedDescending) {
                return NSLocalizedString(@"Yesterday", nil);
            }
        }else{
            NSDate * todayStart = [currentDate startOfDay];
            if ([todayStart compare:self] == NSOrderedAscending) {
                return NSLocalizedString(@"Today", nil);
            }else if([[todayStart dateByAddingDays:-1] compare:self] == NSOrderedAscending) {
                return NSLocalizedString(@"Yesterday", nil);
            }
        }
    }
    
    NSDate * date1 = isSelfFuture ? currentDate : self;
    NSDate * date2 = isSelfFuture ? self : currentDate;
    
    NSInteger days = [NSDate dateDifferenceFromDate:date1 toDate:date2 calendarUnit:NSCalendarUnitDay];
    NSInteger hours = [NSDate dateDifferenceFromDate:date1 toDate:date2 calendarUnit:NSCalendarUnitHour];
    NSInteger minutes = [NSDate dateDifferenceFromDate:date1 toDate:date2 calendarUnit:NSCalendarUnitMinute];
    
    NSString * result = [NSDate countdownMessageWithDays:days hours:hours minutes:minutes];
    
    return result;
}

@end
