//  NSObject+GBToast.m
//  GoBeside

#import "NSObject+GBToast.h"

@implementation NSObject (GBToast)

- (void)showGlobalToast:(NSString *)toast
{
    [MSharedApplication.keyWindow makeToast:toast
                                   duration:2.
                                   position:CSToastPositionBottom];

}

- (void)showGlobalToastWithError:(NSError *)error
{
    [self showGlobalToast:error.userInfo[kGBErrorMessageKey] ? : error.localizedDescription];
}

@end
