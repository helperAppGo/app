//  NSObject+GBToast.h
//  GoBeside

#import <Toast/UIView+Toast.h>

@interface NSObject (MToast)

- (void)showGlobalToast:(NSString *)toast;
- (void)showGlobalToastWithError:(NSError *)error;

@end
