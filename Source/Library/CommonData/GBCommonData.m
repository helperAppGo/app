//  GBCommonData.m
//  GoBeside

#import <UIKit/UIKit.h>

#import "GBNavigationManager.h"
#import "GBDataFacade.h"
#import "GBCache.h"
#import "GBMediaManager.h"

NSString * SECRET_TOKEN;
NSNumber * PUSH_TOKEN;
NSNumber * USER_ID;
NSString * LOCALIZATION;
NSString * TEMP_LOCALIZATION;
GBUser   * CURRENT_USER;
NSString * BASE_URL_STRING;
NSArray  * CITIES;

@implementation GBCommonData

+ (void)initCommon
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    TEMP_LOCALIZATION = LOCALIZATION = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    SECRET_TOKEN = [ud objectForKey:@"secret_token"];
    PUSH_TOKEN = [ud objectForKey:@"push_token"] ? : @"";
    USER_ID = @([[ud objectForKey:@"user_id"] intValue]);
    
    NSDictionary * userInfo = [ud objectForKey:@"user"];
    if (userInfo) {
        NSError *error = nil;
        CURRENT_USER = [[GBUser alloc] initWithDictionary:userInfo error:&error];
        DLogIf(CURRENT_USER == nil, @"Failed to parse user model (4) from <%@>: %@",userInfo,error);
        dAssert(CURRENT_USER != nil, @"Failed to parse user model (4) from <%@>: %@",userInfo,error);
    }
    
    BASE_URL_STRING = [ud objectForKey:@"BASE_URL_STRING"] ? : BASE_URL;
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        SECRET_TOKEN = user.token;
        USER_ID = @(user.objId);
    }
    
}

+ (void)setToken:(NSString *)token
{
//    if (!token)
//    {
//        return;
//    }
    
    SECRET_TOKEN = token;
    
    [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"secret_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUserId:(NSNumber *)userId
{
    USER_ID = userId;
    
    [[NSUserDefaults standardUserDefaults] setValue:userId forKey:@"user_id"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setUser:(GBUser *)user
{
    CURRENT_USER = user;
    if (user) {
        NSDictionary * userInfo = [CURRENT_USER toDictionary];
        [[NSUserDefaults standardUserDefaults] setValue:userInfo forKey:@"user"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

+ (void)setPushToken:(NSNumber *)code {
    PUSH_TOKEN = code;
}

+ (void)setBaseUrlSring:(NSString *)baseUrlSring {
    BASE_URL_STRING = baseUrlSring;
    
    [[NSUserDefaults standardUserDefaults] setValue:baseUrlSring forKey:@"BASE_URL_STRING"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
