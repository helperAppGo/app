//  GBCommonData.h
//  GoBeside

#import <Foundation/Foundation.h>
#import "GBUser.h"

FOUNDATION_EXPORT NSString * SECRET_TOKEN;
FOUNDATION_EXPORT NSNumber * PUSH_TOKEN;
FOUNDATION_EXPORT NSNumber * USER_ID;
FOUNDATION_EXPORT NSString * LOCALIZATION;
FOUNDATION_EXPORT NSString * TEMP_LOCALIZATION;
//FOUNDATION_EXPORT GBUser   * CURRENT_USER;
FOUNDATION_EXPORT NSString * BASE_URL_STRING;
FOUNDATION_EXPORT NSArray  * CITIES;

FOUNDATION_EXPORT NSString * NeedFullUpdateNotification;

@interface GBCommonData : NSObject

@property (nonatomic, readonly) NSString* baseUrlString;


+ (void)initCommon;
+ (void)setToken:(NSString *)token;
+ (void)setUserId:(NSNumber *)userId;
+ (void)setUser:(GBUser *)user;
+ (void)setPushToken:(NSNumber *)code;
+ (void)setBaseUrlSring:(NSString *)baseUrlSring;

@end
