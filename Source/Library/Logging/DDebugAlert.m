//
//  DDebugAlert.m
//

#import "DDebugAlert.h"
#import "CustomIOSAlertView.h"
//#import "MessagingHelper.h"
#import "DLogger+Sending.h"

typedef NS_ENUM(NSUInteger, DebugAlertResult) {
    DebugAlertResultSuccess,
    DebugAlertResultCancel,
    DebugAlertResultError
};

@implementation DDebugAlert

+ (void) showAssertionAlertWithWithInfo:(NSString*)anInfo withCompletion:(GBVoidBlock)aCompletionBlock
{
    NSString * info = [NSString stringWithFormat:@"App will crash because assertion fired. Add your comments here to describe situation (don't clear debug info below):\n\n\n\n\n\n--------------------------------\nDebug info:\n%@",anInfo];
    [self showDebugAlertWithWithInfo:info withCompletion:aCompletionBlock];
}

+ (void) showDebugAlertWithWithInfoMainThread:(NSString*)anInfo withCompletion:(GBVoidBlock)aCompletionBlock
{
    UITextView * tvDebug = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 300, 250)];
    tvDebug.text = anInfo;
    
    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
    [alertView setContainerView:tvDebug];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Close", @"Send", nil]];
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        if (buttonIndex==1) {
            NSString * msg = tvDebug.text;
            [alertView close];
            [[DLogger sharedLogger] sendLogsByEmailWithSubject:@"Debug" body:msg completion:aCompletionBlock];
        }else{
            [alertView close];
            if (aCompletionBlock) aCompletionBlock();
        }
    }];
    [alertView setUseMotionEffects:true];
    [alertView show];
}

+ (void) showDebugAlertWithWithInfo:(NSString*)anInfo withCompletion:(GBVoidBlock)aCompletionBlock
{
    if ([NSThread isMainThread]) {
        [self showDebugAlertWithWithInfoMainThread:anInfo withCompletion:aCompletionBlock];
    }else{
        dispatch_async(DISP_MAIN_THREAD, ^{
            [self showDebugAlertWithWithInfoMainThread:anInfo withCompletion:aCompletionBlock];
        });
    }
}

+ (void) showDebugAlertWithWithInfo:(NSString*)anInfo
{
    [self showDebugAlertWithWithInfo:anInfo withCompletion:nil];
}

@end
