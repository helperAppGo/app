//
//  DLogger.m
//

#import "DLogger.h"
#import "Utils.h"

#import <zlib.h>
#import <sys/sysctl.h>

const NSUInteger kMaxLogFiles = 10;
const NSUInteger kMaxLogFileSize = 5242880;

@interface DLogger () {
    NSString *_logPath;
}

@property (nonatomic, strong, readonly) NSFileHandle *fileHandler;
@property (nonatomic, copy) GBVoidBlock emailSendingCompletionBlock;


-(void)createNewFileWithInitialLine:(NSString *)aLine;

-(void)removeOldFiles;

@end

@implementation DLogger

+ (DLogger *)sharedLogger {
    static id sharedLogger = nil;
    static dispatch_once_t sharedLogger_once;
    dispatch_once(&sharedLogger_once, ^{
        sharedLogger = [[DLogger alloc] init];
        [sharedLogger createNewFileWithInitialLine:[NSString stringWithFormat:@"Log started. iOS %@\n", [[UIDevice currentDevice] systemVersion]]];
    });
    return sharedLogger;
}

+ (void)viewHierarchyFromRoot:(UIView*)rootView lev:(NSInteger)lev{
    if (rootView!=nil){
        NSMutableString * spaces = [[NSMutableString alloc] init];
        for (NSInteger ii=0; ii<lev; ii++){
            [spaces appendString:@"|  "];
        }
        DLog(@"-> L%ld %@class <%@> frame <%@> subviews <%lu> alabel <%@> h <%d> alpha <%f>",(long)lev,spaces,[rootView class],NSStringFromCGRect(rootView.frame),(unsigned long)[rootView.subviews count],[rootView accessibilityLabel],rootView.isHidden,rootView.alpha);
        if ([rootView isKindOfClass:[UIImageView class]]) {
            UIImage * img = [(UIImageView*)rootView image];
            DLog(@"-> L%ld %@. Image: %@, size:%@",(long)lev,spaces,img,img?NSStringFromCGSize(img.size):@"---");
        }
        for (NSInteger ii=0; ii<[rootView.subviews count]; ii++){
            [self viewHierarchyFromRoot:[rootView.subviews objectAtIndex:ii] lev:(lev+1)];
        }
    }
}

- (NSString *)logsPath {
    if (_logPath == nil) {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _logPath = [paths[0] stringByAppendingPathComponent:@"logs"];
    }
    return _logPath;
}

-(void)createNewFileWithInitialLine:(NSString *)aLine
{
    if (_fileHandler) {
        [_fileHandler closeFile];
        _fileHandler = nil;
    }

    NSDate *date = [NSDate date];
    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    [formater setDateFormat:@"yyyy-MM-dd-HH-mm-ss-SSSS"];
    NSString *timeString = [formater stringFromDate:date];
    NSString *fileName = [NSString stringWithFormat:@"log-%@%@", timeString, @".log"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *logsDirectory = [self logsPath];
    BOOL isDir;
    if (([fileManager fileExistsAtPath:logsDirectory isDirectory:&isDir] && isDir) == NO) {
        [fileManager createDirectoryAtPath:logsDirectory
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil];
    }
    
    fileName = [logsDirectory stringByAppendingPathComponent:fileName];
    
    BOOL result = [[NSFileManager defaultManager] createFileAtPath:fileName contents:nil attributes:nil];
    NSAssert(result, @"Error can not create log file");
    _fileHandler = [NSFileHandle fileHandleForWritingAtPath:fileName];
    NSAssert(_fileHandler != nil, @"Can not create handler for log file");
    /*BOOL successSkipBackup = */[Utils addSkipBackupAttributeToItemAtPath:fileName];
    //dAssert(successSkipBackup, @"Can't add 'skip' flag to %@",fileName);
    
    NSString *systemVer = [[UIDevice currentDevice] systemVersion];
    
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    free(machine);
    
    NSData *dataForFile = [[NSString stringWithFormat:@"%@\n%@\n%@ %@", timeString, aLine,platform,systemVer] dataUsingEncoding:NSUTF8StringEncoding];
    [_fileHandler writeData:dataForFile];
    
    [self removeOldFiles];
}

-(void)writeLine:(NSString *)logLine {
    @synchronized(_fileHandler) {
        [self checkLogFileSize];
        NSDate *now = [NSDate date];
        NSDateFormatter *formater = [[NSDateFormatter alloc] init];
        [formater setDateFormat:@"(yyyy-MM-dd HH:mm:ss:SSSS)"];
        NSString *timeString = [formater stringFromDate:now];
        NSData *dataForFile = [[NSString stringWithFormat:@"%@%@\n", timeString, logLine] dataUsingEncoding:NSUTF8StringEncoding];
        
        [_fileHandler writeData:dataForFile];
        [_fileHandler synchronizeFile];
    }
}

-(void)removeOldFiles {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSArray *files = [fileManager contentsOfDirectoryAtPath:[self logsPath] error:nil];
    NSInteger diff = files.count - kMaxLogFiles;
    if (diff > 0){
        NSStringCompareOptions compareOptions = NSCaseInsensitiveSearch | NSNumericSearch | NSWidthInsensitiveSearch |NSForcedOrderingSearch;
        NSLocale *locale = [NSLocale currentLocale];
    
        NSArray *sortedFiles = [files sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSRange string1Range = NSMakeRange(0, [obj1 length]);
            return [obj1 compare:obj1
                         options:compareOptions
                           range:string1Range
                          locale:locale];
        }];
        [sortedFiles enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSError *err = nil;
            NSString *path = [[self logsPath] stringByAppendingPathComponent:obj];
            [fileManager removeItemAtPath:path error:&err];
            if (err) {
                DLog(@"Can't remove log file %@", err);
            }
            *stop = idx >= diff - 1;
        }];
    }
}

- (void) checkLogFileSize {
    if ([self.fileHandler seekToEndOfFile] >= kMaxLogFileSize) {
        [self createNewFileWithInitialLine:@"Continue log..."];
    }
}

@end
