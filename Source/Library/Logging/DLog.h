//
//  DLog.h
//

#ifndef _DLog_h
#define _DLog_h


#ifdef LOGS_ENABLED
#define DLOG_ON
#define DLOG_LOGINFILE
#define DLOG_LOGINNSLOG
#endif


#ifdef DLOG_ON

#import "DDebugLog.h"
#import "DLogger.h"
#import "DLogger+Sending.h"
#import "DAssert.h"


#   define DLOGFORMAT [NSString stringWithFormat:@"%s (%s:%d) %@: ", __PRETTY_FUNCTION__, (strrchr(__FILE__, '/') ?: __FILE__ - 1) + 1, __LINE__, threadLogDescr()]

#   define DLog(fmt, ...) [[DDebugLog sharedLog] dLogDebug:[NSString stringWithFormat:@"%@ " fmt, DLOGFORMAT, ##__VA_ARGS__]]
#   define DLogIf(condition, fmt, ...) if (condition) [[DDebugLog sharedLog] dLogDebug:[NSString stringWithFormat:@"%@ " fmt, DLOGFORMAT, ##__VA_ARGS__]]
#   define startLogging() [DDebugLog sharedLog];
#   define viewHierarchyFromRoot(rootView) [DLogger viewHierarchyFromRoot:rootView];
#else

#ifdef DEBUG
#   define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define DLog(...)                    {}
#endif

#   define DLogIf(...)                  {}
#   define startLogging()               {}
#   define viewHierarchyFromRoot(...)   {}
#endif



#endif //#ifdef DEBUG
