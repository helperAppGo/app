//
//  DLogger.h
//

@interface DLogger : NSObject

+(DLogger *)sharedLogger;

-(void)writeLine:(NSString *)logLine;
-(NSString *)logsPath;

@end
