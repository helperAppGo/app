//
//  DAssert.m
//

#import "DAssert.h"

#import "DLog.h"

#import "DDebugAlert.h"

#ifdef DASSERTION_DEBUG

void handleAssertionDebug(NSString *placedInfo, NSString *format) {
    DLog(@">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ASSERTION FIRED HERE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
    NSString * info = [NSString stringWithFormat:@"ASSERT: %@\n%@\nCall stack %@", placedInfo, format,[NSThread callStackSymbols]];
    DLog(@"%@",info);
    DLog(@"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< END OF ASSERTION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    [DDebugAlert showAssertionAlertWithWithInfo:info withCompletion:^{
        __builtin_trap();
    }];
    
    
}

#endif //#ifdef DASSERTION_DEBUG
