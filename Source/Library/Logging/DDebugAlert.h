//
//  DDebugAlert.h
//

@interface DDebugAlert : NSObject

+ (void) showAssertionAlertWithWithInfo:(NSString*)anInfo withCompletion:(GBVoidBlock)aCompletionBlock;
+ (void) showDebugAlertWithWithInfo:(NSString*)anInfo withCompletion:(GBVoidBlock)aCompletionBlock;
+ (void) showDebugAlertWithWithInfo:(NSString*)anInfo;

@end
