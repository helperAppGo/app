//
//  DDebugLog.h
//

//#import "Crashlytics/Crashlytics.h"

#ifdef DLOG_ON

NSString *threadLogDescr();

@interface DDebugLog : NSObject

+(instancetype)sharedLog;

- (void) dLogDebug:(NSString *)format;


@end

#endif
