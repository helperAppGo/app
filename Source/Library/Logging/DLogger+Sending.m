//
//  DLogger+Sending.m
//

#import "DLogger+Sending.h"
#import <MessageUI/MessageUI.h>
#import "ZKFileArchive.h"
#import "GBAlerter.h"


@interface DLogger ()

@property (nonatomic, copy) GBVoidBlock emailSendingCompletionBlock;

@end

@implementation DLogger (Sending)

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [controller dismissViewControllerAnimated:YES completion:self.emailSendingCompletionBlock];
    self.emailSendingCompletionBlock = nil;
}

- (void)sendLogsByEmailWithSubject:(NSString*)aSubject body:(NSString*)aMessageBody completion:(GBVoidBlock)aCompletionBlock
{
    if ([MFMailComposeViewController canSendMail]) {
        
        DLog(@"Sending logs by email");
        self.emailSendingCompletionBlock = aCompletionBlock;
        NSString *zipFilePath = [self createZipFile];
        
        MFMailComposeViewController *mailController = [[MFMailComposeViewController alloc] init];
        [[mailController navigationBar] setTintColor:[UIColor blackColor]];
        mailController.mailComposeDelegate = (id<MFMailComposeViewControllerDelegate>)self;
        [mailController setSubject:IS_STR_EMPTY(aSubject) ? @"Debug logs" : aSubject];
        [mailController setMessageBody:IS_STR_EMPTY(aMessageBody)?@"logs":aMessageBody isHTML:NO];
        [mailController setToRecipients:@[@"ruslan.mishyn@gmail.com"]];
        
        NSData *logData = [NSData dataWithContentsOfFile:zipFilePath];
        [[NSFileManager defaultManager] removeItemAtPath:zipFilePath error:NULL];
        [mailController addAttachmentData:logData mimeType:@"application/zip" fileName:@"logs.zip"];

        UIViewController * vc = [UIApplication sharedApplication].keyWindow.rootViewController;
        [vc presentViewController:mailController animated:YES completion:NULL];
    }else{
        WEAKSELF_DECLARATION
        [GBAlerter showAlertWithMessage:NSLocalizedString(@"Can't send email. Please open your iPhone settings and set up an Email account.",nil) okHandler:^(id value) {
            STRONGSELF_DECLARATION
            if (strongSelf.emailSendingCompletionBlock) {
                strongSelf.emailSendingCompletionBlock();
                strongSelf.emailSendingCompletionBlock = nil;
            }
        }];
    }
}

- (void)sendLogsByEmail
{
    [self sendLogsByEmailWithSubject:[NSString stringWithFormat:@"%@ logs",APPNAME] body:@"logs" completion:nil];
}

- (NSString *)createZipFile
{
    NSString *pathLogs = [self logsPath];
    NSString *zipFilePath = [Utils getPathForTempFile:@"logs.zip"];

    NSFileManager *fileManager = [NSFileManager defaultManager];
    DLog(@"Creating zip file %@", zipFilePath );
    if ([fileManager fileExistsAtPath:zipFilePath]) {
        [fileManager removeItemAtPath:zipFilePath error:NULL];
    }

    ZKFileArchive *zipFile = [ZKFileArchive archiveWithArchivePath:zipFilePath];
    [zipFile deflateDirectory:pathLogs relativeToPath:[pathLogs stringByDeletingLastPathComponent] usingResourceFork:NO];
//    NSString *dbpath = [DBManager databaseStorePath];
//    if (dbpath) {
//        [zipFile deflateFile:dbpath relativeToPath:[dbpath stringByDeletingLastPathComponent] usingResourceFork:NO];
//    }
    DLog(@"Zip File created. SIZE: %@", [fileManager attributesOfItemAtPath:zipFilePath error:NULL][@"NSFileSize"] );
    return zipFilePath;
}

@end
