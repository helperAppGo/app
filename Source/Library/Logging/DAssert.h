//
//  DAssert.h
//

#import "DLog.h"

#ifdef DLOG_ON
void handleAssertionDebug(NSString *placedInfo, NSString *format);
#   define DASSERTION_DEBUG
#   define dAssert(condition, format, ...) if (!(condition)) handleAssertionDebug(DLOGFORMAT,[NSString stringWithFormat:format, ##__VA_ARGS__])
#else
#   define dAssert(condition, format, ...)
#endif //#ifdef DLOG_ON