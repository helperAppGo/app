//
//  DLogger+Sending.h
//

#import "DLogger.h"

@interface DLogger (Sending)

- (void)sendLogsByEmailWithSubject:(NSString*)aSubject body:(NSString*)aMessageBody completion:(GBVoidBlock)aCompletionBlock;
- (void)sendLogsByEmail;

@end
