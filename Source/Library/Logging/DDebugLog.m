//
//  DDebugLog.m
//

#ifdef DLOG_ON

#import "DDebugLog.h"

#import "DLogger.h"
#import <pthread.h>
#import <execinfo.h>
#import <signal.h>

NSString * const kClsLogDisabled = @"clsLogDisabled";

@interface DDebugLog ()

@end


NSString *threadLogDescr() {
    if ([NSThread isMainThread]) {
        return @"thread[main]";
    }
    NSInteger res = -1;
    NSString * description = [[NSThread currentThread] description ] ;
    NSArray * keyValuePairs = [ description componentsSeparatedByString:@"," ] ;
    for( NSString * keyValuePair in keyValuePairs )
    {
        NSArray * components = [ keyValuePair componentsSeparatedByString:@"=" ] ;
        NSString * key = components[0] ;
        key = [ key stringByTrimmingCharactersInSet:[ NSCharacterSet whitespaceCharacterSet ] ] ;
        if ( [ key isEqualToString:@"num" ] )
        {
            res = [ components[1] integerValue ] ;
        }
    }
    return [NSString stringWithFormat:@"thread[%@]", res == -1 ? @"unknown" : @(res)];
}


@implementation DDebugLog

+(instancetype)sharedLog {
    static dispatch_once_t once;
    static DDebugLog *sharedLog = nil;
    dispatch_once(&once, ^{
        sharedLog = [[DDebugLog alloc] init];
    });
    return sharedLog;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void) dLogDebug:(NSString *)format {
#ifdef DLOG_LOGINNSLOG
    NSLog(@"%@", format);
#endif
#ifdef DLOG_LOGINFILE
    [[DLogger sharedLogger] writeLine:format];
#endif
}


@end

#endif

