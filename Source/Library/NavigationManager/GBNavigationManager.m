//  GBNavigationManager.m
//  GoBeside

#import "GBNavigationManager.h"
#import "GBDataFacade.h"
#import "AppDelegate.h"

@interface GBNavigationManager()

@end

@implementation GBNavigationManager

+ (GBNavigationManager *)sharedNavigationManager
{
    static dispatch_once_t onceToken;
    static GBNavigationManager *_sharedNavigationManager = nil;
    dispatch_once(&onceToken, ^{
        _sharedNavigationManager = [[GBNavigationManager alloc] init];
    });
    return _sharedNavigationManager;
}

- (void)setRootControllerWithAnimation:(UIViewController *)viewController
{
    self.rootViewController = viewController;
    [UIView transitionWithView:self.window
                      duration:0.5
                       options: UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        [self.window setRootViewController:viewController];
                    }
                    completion:nil];
}

- (void)setRegistrationQueue
{
    self.tabViewController = nil;

    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
    [self setRootControllerWithAnimation:controller];
}

- (void)openMainAppInitializer
{
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] openTabbar];
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TabViewController"];
//    [self setRootControllerWithAnimation:controller];
    
}

@end
