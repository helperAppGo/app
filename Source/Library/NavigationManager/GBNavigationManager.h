//  GBNavigationManager.h
//  GoBeside

#import <UIKit/UIKit.h>
#import "TabViewController.h"
#import "GBServerCommand.h"

//#define NAVIGATION_MANAGER [GBNavigationManager sharedNavigationManager]

@interface GBNavigationManager : NSObject

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) UIViewController * rootViewController;
@property (weak, nonatomic) TabViewController * tabViewController;

+ (GBNavigationManager *)sharedNavigationManager;

- (void)setRootControllerWithAnimation:(UIViewController *)viewController;

- (void)setRegistrationQueue;
- (void)openMainAppInitializer;

@end
