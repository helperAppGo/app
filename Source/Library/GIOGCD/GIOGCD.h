#import <Foundation/Foundation.h>

typedef void(^GIOGCDBlock)();

typedef enum {
    GIODispatchQueuePriorityHigh        = DISPATCH_QUEUE_PRIORITY_HIGH,
    GIODispatchQueuePriorityDefault     = DISPATCH_QUEUE_PRIORITY_DEFAULT,
    GIODispatchQueuePriorityLow         = DISPATCH_QUEUE_PRIORITY_LOW,
    GIODispatchQueuePriorityBackground  = DISPATCH_QUEUE_PRIORITY_BACKGROUND,
} GIODispatchQueuePriorityType;

typedef enum {
    GIOBlockExecutionSynchronous,
    GIOBlockExecutionAsynchronous
} GIOBlockExecutionType;


void GIOPerformAsyncBlockOnMainQueue(GIOGCDBlock block);
void GIOPerformSyncBlockOnMainQueue(GIOGCDBlock block);

void GIOPerformAsyncBlockOnBackgroundQueue(GIOGCDBlock block);
void GIOPerformSyncBlockOnBackgroundQueue(GIOGCDBlock block);

void GIOPerformAsyncBlockOnLowQueue(GIOGCDBlock block);
void GIOPerformSyncBlockOnLowQueue(GIOGCDBlock block);

dispatch_queue_t GIODisptchQueueWithPriorityType(GIODispatchQueuePriorityType type);
