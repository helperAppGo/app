//
//  GBWeakifier.m
//  GoBeside
//
//  Created by Ruslan Mishin on 23.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBWeakifier.h"

@implementation GBWeakifier

+ (instancetype)weakifierWithObject:(id)anOriginalObject
{
    GBWeakifier * weakifier = [[GBWeakifier alloc] init];
    weakifier.obj = anOriginalObject;
    return weakifier;
}

@end
