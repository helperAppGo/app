//
//  Utils.m
//

@implementation Utils

+ (NSString *) getApplicationBasePath
{
    static NSString *basePath = nil;
    if (!basePath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSApplicationDirectory, NSUserDomainMask, YES);
        basePath = (paths.count > 0) ? [paths objectAtIndex:0] : nil;
    }
    return basePath;
}

+ (NSString *) getDocumentsBasePath
{
    static NSString *basePath = nil;
    if (!basePath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        basePath = (paths.count > 0) ? [paths objectAtIndex:0] : nil;
    }
    return basePath;
}

+ (NSString *) getCachesBasePath
{
    static NSString *basePath = nil;
    if (!basePath) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        basePath = (paths.count > 0) ? [paths objectAtIndex:0] : nil;
    }
    return basePath;
}

+ (NSString *) getPathForDocumentsFile:(NSString *)fileName
{
    NSString *documentsPath = [self getDocumentsBasePath];
    return documentsPath ? [documentsPath stringByAppendingPathComponent:fileName] : nil;
}

+ (NSString *) getPathForTempFile:(NSString *)fileName
{
    NSString *tempDir = NSTemporaryDirectory();
    return tempDir ? [tempDir stringByAppendingPathComponent:fileName] : nil;
}

+ (NSString *) getPathForDirectory:(NSString *)directoryName basePath:(NSString *)basePath
{
    NSString *result = nil;
    if (basePath) {
        result = [basePath stringByAppendingPathComponent:directoryName];
        if (![[NSFileManager defaultManager] fileExistsAtPath:result]) {
            NSError *error = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:result withIntermediateDirectories:YES attributes:nil error:&error];
            if (error) {
                result = nil;
            }
        }
    }
    return result;
}

+ (NSString *) getPathForDocumentsDirectory:(NSString *)directoryName
{
    return [self getPathForDirectory:directoryName basePath:[self getDocumentsBasePath]];
}

+ (NSString *) getPathForTempDirectory:(NSString *)directoryName
{
    return [self getPathForDirectory:directoryName basePath:NSTemporaryDirectory()];
}

+ (NSString *) getPathForCacheDirectory:(NSString *)directoryName
{
    return [self getPathForDirectory:directoryName basePath:[self getCachesBasePath]];
}

+ (NSString *) getPathForFile:(NSString *)fileName ofType:(NSString *)fileType inDirectory:(NSString *)directoryName
{
    if (!directoryName.length || !fileName.length || !fileType.length) {
        return nil;
    }
    NSString *basePath = [self getDocumentsBasePath];
    NSString *dirPath = nil;
    if (basePath) {
        dirPath = [basePath stringByAppendingPathComponent:directoryName];
        if (![[NSFileManager defaultManager] fileExistsAtPath:dirPath]) {
            NSError *error = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
            if (error) {
            }
        }
    }
    if (dirPath.length) {
        dirPath = [dirPath stringByAppendingPathComponent:[[fileName stringByDeletingPathExtension] stringByAppendingPathExtension:fileType]];
    }
    return dirPath;
}

+ (NSString *) getPathForFileWithoutCheckExistsDirectory:(NSString *)fileName ofType:(NSString *)fileType inDirectory:(NSString *)directoryName{
    if (!directoryName.length || !fileName.length || !fileType.length) {
        return nil;
    }
    NSString *basePath = [self getDocumentsBasePath];
    NSString *dirPath = nil;
    if (basePath) {
        dirPath = [basePath stringByAppendingPathComponent:directoryName];
    }
    if (dirPath.length) {
        dirPath = [dirPath stringByAppendingPathComponent:[[fileName stringByDeletingPathExtension] stringByAppendingPathExtension:fileType]];
    }
    return dirPath;
}

+ (BOOL) addSkipBackupAttributeToItemAtPath:(NSString *)filePathString
{
    NSURL* URL= [NSURL fileURLWithPath: filePathString];
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        DLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}


+ (BOOL) isAirplaneModeOn
{
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:@"/Library/Preferences/SystemConfiguration/com.apple.radios.plist"];
    return [[dict objectForKey:@"AirplaneMode"] boolValue];
}


@end

