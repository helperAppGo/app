//
//  GBWeakifier.h
//  GoBeside
//
//  Created by Ruslan Mishin on 23.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBWeakifier : NSObject

@property (nonatomic,weak) id obj;

+ (instancetype)weakifierWithObject:(id)anOriginalObject;

@end
