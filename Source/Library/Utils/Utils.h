//
//  Utils.h
//

@interface Utils : NSObject

+ (NSString *) getApplicationBasePath;
+ (NSString *) getDocumentsBasePath;
+ (NSString *) getPathForDocumentsFile:(NSString *)fileName;
+ (NSString *) getPathForTempFile:(NSString *)fileName;
+ (NSString *) getPathForDocumentsDirectory:(NSString *)directoryName;
+ (NSString *) getPathForTempDirectory:(NSString *)directoryName;
+ (NSString *) getPathForCacheDirectory:(NSString *)directoryName;
+ (NSString *) getPathForFile:(NSString *)fileName ofType:(NSString *)fileType inDirectory:(NSString *)directoryName;
+ (NSString *) getPathForFileWithoutCheckExistsDirectory:(NSString *)fileName ofType:(NSString *)fileType inDirectory:(NSString *)directoryName;

+ (BOOL) addSkipBackupAttributeToItemAtPath:(NSString *)filePathString;

+ (BOOL) isAirplaneModeOn;

@end
