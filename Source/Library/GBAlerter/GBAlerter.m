//  GBAlerter.m
//  GoBeside

#import "GBAlerter.h"

@implementation GBAlerter

+ (void)showErrorMessage:(NSError *)error
{
    [self showErrorMessage:error okHandler:nil];
}

+ (void)showErrorMessage:(NSError *)error okHandler:(GBValueBlock)okHandler
{
    NSString * msg;
    
    if (!error.userInfo[kGBErrorMessageKey])
    {
        NSDictionary *firstError = [error.userInfo[kGBErrorResponseKey][@"errors"] firstObject];
        msg = firstError[@"description"] ?: firstError[@"message"];
    }
    else
    {
        msg = error.userInfo[kGBErrorMessageKey] ? : error.localizedDescription;
    }
    
    if (msg)
    {
        [self showAlertWithMessage:msg okHandler:okHandler];
    }
}

+ (void)showAlertWithMessage:(NSString *)message
{
    [self showAlertWithMessage:message okHandler:nil];
}

+ (void)showAlertWithMessage:(NSString *)message okHandler:(GBValueBlock)okHandler
{
    if (!message) return;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:message
                                                                   message:@""
                                                            preferredStyle:UIAlertControllerStyleAlert];

    [alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"ok", nil)
                                              style:UIAlertActionStyleDefault
                                            handler:okHandler]];
    
    [[self presenter] presentViewController:alert animated:YES completion:nil];
}

+ (void)showAlertWithMessage:(NSString *)message action:(UIAlertAction *)okButton
{
    if (!message) return;
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    
    if (okButton)
    {
        [alert addAction:okButton];
    }
    
    [[self presenter] presentViewController:alert animated:YES completion:nil];
}

+ (void)showAlertWithMessage:(NSString *)message actionChangeLanguage:(UIAlertAction *)button
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    
    if (button)
    {
        [alert addAction:button];
    }
    
    [[self presenter] presentViewController:alert animated:YES completion:nil];
}

+ (void) show2ButtonsAlertWithTitle:(NSString*)aTitle
                            message:(NSString*)aMessage
                       cancelButton:(NSString*)aCancelTitle
                        otherButton:(NSString*)anOtherTitle
                     viewController:(UIViewController*)aViewController
                           onCancel:(GBVoidBlock)onCancelBlock
                            onOther:(GBVoidBlock)onOtherBlock
{
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:aTitle
                                                                              message:aMessage
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:aCancelTitle
                                                            style:UIAlertActionStyleCancel
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              if (onCancelBlock)
                                                                  onCancelBlock();
                                                          }];
    UIAlertAction * otherAction = [UIAlertAction actionWithTitle:anOtherTitle
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             if (onOtherBlock)
                                                                 onOtherBlock();
                                                         }];
    [alertController addAction:cancelAction];
    [alertController addAction:otherAction];
    [[self presenter] presentViewController:alertController animated:YES completion:nil];
}

+ (UIViewController *)presenter
{
    return [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

+ (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController
{
    if ([rootViewController isKindOfClass:[UITabBarController class]])
    {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
    }
    else if ([rootViewController isKindOfClass:[UINavigationController class]])
    {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
    }
    else if (rootViewController.presentedViewController)
    {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController];
    }
    else
    {
        return rootViewController;
    }
}

@end
