//  GBAlerter.h
//  GoBeside

#import <Foundation/Foundation.h>

static NSString *const kGBErrorMessageKey        = @"gbErrorMessage";
static NSString *const kGBErrorResponseKey       = @"gbErrorResponse";
static NSString *const kGBErrorResponseDataKey   = @"gbErrorResponseData";
static NSString *const kGBErrorResponseStringKey = @"gbErrorResponseString";

@interface GBAlerter : NSObject

+ (UIViewController *)presenter;

+ (void)showErrorMessage:(NSError *)error;
+ (void)showErrorMessage:(NSError *)error okHandler:(GBValueBlock)okHandler;

+ (void)showAlertWithMessage:(NSString *)message;
+ (void)showAlertWithMessage:(NSString *)message okHandler:(GBValueBlock)okHandler;
+ (void)showAlertWithMessage:(NSString *)message action:(UIAlertAction *)cancelButton;
+ (void)showAlertWithMessage:(NSString *)message actionChangeLanguage:(UIAlertAction *)button;

+ (void) show2ButtonsAlertWithTitle:(NSString*)aTitle
                            message:(NSString*)aMessage
                       cancelButton:(NSString*)aCancelTitle
                        otherButton:(NSString*)anOtherTitle
                     viewController:(UIViewController*)aViewController
                           onCancel:(GBVoidBlock)onCancelBlock
                            onOther:(GBVoidBlock)onOtherBlock;

@end
