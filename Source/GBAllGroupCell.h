//
//  GBAllGroupCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/7/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBAllGroupModel.h"
#import "GBServerManager.h"
#import "GBAllGroupController.h"
#import "GBFollowButton.h"

@interface GBAllGroupCell : UITableViewCell

- (void) setGroup:(GBAllGroupModel *)allGroup;

@end
