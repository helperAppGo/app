//
//  GBWallModel.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/25/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBWallModel.h"

#define   SHOW_LOGS             NO
#define   Error(format, ...)    if (SHOW_LOGS) NSLog(@"ERROR: %@", [NSString stringWithFormat:format, ## __VA_ARGS__]);

@implementation GBWallModel

+ (NSArray *) getObjectsFromArray:(NSArray *)data {
    if ((id)data == [NSNull null]) {
        return @[];
    }
    
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSDictionary *dict in data) {
        @try {
            [result insertObject:[GBWallModel wallFromDictionary:dict] atIndex:0];
        }
        @catch (NSException *exception) {
            Error(@"%@", exception);
        }
    }
    return result;
}


+(GBWallModel *) wallFromDictionary:(NSDictionary *)data {
    GBWallModel *wallModel = [[GBWallModel alloc] init];
    
    wallModel.idCountWall = [data[@"id"] intValue];
    wallModel.idUser = data[@"id_user"];
    wallModel.idWall = data[@"id_wall"];
    wallModel.files = data[@"files"];
    
    NSString *time = data[@"time"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [formatter dateFromString:time];
    wallModel.date = date;
    
    wallModel.countLikes = [data[@"count_likes"] intValue];
    wallModel.countComments = [data[@"count_comments"] intValue];
    wallModel.name = data[@"name"];
    wallModel.path_ava = [NSURL URLWithString:data[@"path_ava"]];
    wallModel.text = data[@"text"];
    wallModel.is_liked = [data[@"is_liked"] intValue];
    
    
    
    
    NSDictionary *file_objects = [[data valueForKey:@"file_objects"] objectAtIndex:0];
    
    wallModel.heightObjects = [file_objects[@"height"] floatValue];
    wallModel.idObjects = file_objects[@"id"];
    wallModel.typeObjects = file_objects[@"type"];
    wallModel.widthObjects = [file_objects[@"width"] floatValue];
    
    NSString *path = file_objects[@"path"];
    wallModel.pathUrl = [NSURL URLWithString:path];
    
    
//////////--------------------------------------------------
    
    
    float oldWidth = wallModel.widthObjects;
    if (oldWidth == 0.f) {
        wallModel.loadError = 1;
        UIImage *image = [[UIImage alloc] init];
        image = [UIImage imageNamed:@"imageError.png"];
        wallModel.widthObjects = image.size.width;
        wallModel.heightObjects = image.size.height;
        oldWidth = wallModel.widthObjects;
    }
    
    float scaleFactor = [UIScreen mainScreen].bounds.size.width / oldWidth;
    
    wallModel.heightObjects = wallModel.heightObjects * scaleFactor;
    wallModel.widthObjects = wallModel.widthObjects * scaleFactor;

    
    return wallModel;
}


- (void) likeAction {
    
    if(self.is_liked){
        
        self.is_liked = FALSE;
        self.countLikes--;
        //post liked
    }
    else
    {
        self.is_liked = TRUE;
        self.countLikes++;
        //Delete POST
    }
}



@end
