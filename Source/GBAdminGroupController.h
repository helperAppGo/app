//
//  GBAdminGroupController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/15/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBInGroupViewController.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBAllGroupModel.h"
#import "GBWallController.h"
#import "AppDelegate.h"
#import "GBListMembers.h"
#import "GBWallCell.h"

@interface GBAdminGroupController : GBInGroupViewController <UITableViewDelegate, UITableViewDataSource>

// SETTINGS VIEW
@property (weak, nonatomic) IBOutlet UIView *settingGroupView;
@property (weak, nonatomic) IBOutlet UIImageView *iconSettingGroup;
@property (weak, nonatomic) IBOutlet UILabel *settingGroup;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingHeight;

// STATISTIC VIEW
@property (weak, nonatomic) IBOutlet UIView *statisticView;
@property (weak, nonatomic) IBOutlet UILabel *statisticSite;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statisticHight;

@property (nonatomic) BOOL isStretched;


@end
