//  GBCommonStyles.m
//  GoBeside

#import "GBCommonStyles.h"

@implementation GBCommonStyles

+ (UIColor *)headerColour
{
    static UIColor * headerColour = nil;
    if (!headerColour){
        headerColour = [UIColor colorWithRed:0.051 green:0.278 blue:0.631 alpha:1.00];
    }
    return headerColour;
}


+ (UIColor *)mainThemeColour
{
    static UIColor * mainThemeColour = nil;
    if (!mainThemeColour){
        mainThemeColour = [UIColor colorWithRed:29/255.0 green:117/255.0 blue:188/255.0 alpha:1.0];
    }
    return mainThemeColour;
}

+ (UIColor *)colour_8b94a3
{
    static UIColor * colour_8b94a3 = nil;
    if (!colour_8b94a3){
        colour_8b94a3 = [UIColor colorWithRed:139/255.0 green:148/255.0 blue:163/255.0 alpha:1.0];
    }
    return colour_8b94a3;
}

+ (UIColor *)colour_b3b9c2
{
    static UIColor * colour_b3b9c2 = nil;
    if (!colour_b3b9c2){
        colour_b3b9c2 = [UIColor colorWithRed:179/255.0 green:185/255.0 blue:194/255.0 alpha:1.0];
    }
    return colour_b3b9c2;
}

+ (UIColor *)colour_505c6c
{
    static UIColor * colour_505c6c = nil;
    if (!colour_505c6c){
        colour_505c6c = [UIColor colorWithRed:80/255.0 green:92/255.0 blue:108/255.0 alpha:1.0];
    }
    return colour_505c6c;
}

+ (UIColor *)colour_a6adb7
{
    static UIColor * colour_a6adb7 = nil;
    if (!colour_a6adb7){
        colour_a6adb7 = [UIColor colorWithRed:166/255.0 green:173/255.0 blue:183/255.0 alpha:1.0];
    }
    return colour_a6adb7;
}

+ (UIColor *)colour_67c0ef
{
    static UIColor * colour_67c0ef = nil;
    if (!colour_67c0ef){
        colour_67c0ef = [UIColor colorWithRed:103/255.0 green:192/255.0 blue:239/255.0 alpha:1.0];
    }
    return colour_67c0ef;
}

+ (UIColor *)colour_f16173
{
    static UIColor * colour_f16173 = nil;
    if (!colour_f16173){
        colour_f16173 = [UIColor colorWithRed:241/255.0 green:97/255.0 blue:115/255.0 alpha:1.0];
    }
    return colour_f16173;
}

+ (UIColor *)colour_f3f7f9
{
    static UIColor * colour_f3f7f9 = nil;
    if (!colour_f3f7f9){
        colour_f3f7f9 = [UIColor colorWithRed:243/255.0 green:247/255.0 blue:249/255.0 alpha:1.0];
    }
    return colour_f3f7f9;
}

+ (UIColor *)colour_cbd3d6
{
    static UIColor * colour_cbd3d6 = nil;
    if (!colour_cbd3d6){
        colour_cbd3d6 = [UIColor colorWithRed:203/255.0 green:212/255.0 blue:215/255.0 alpha:1.0];
    }
    return colour_cbd3d6;
}

+ (UIColor *)colour_fffaee
{
    static UIColor * colour_fffaee = nil;
    if (!colour_fffaee){
        colour_fffaee = [UIColor colorWithRed:255/255.0 green:250/255.0 blue:238/255.0 alpha:1.0];
    }
    return colour_fffaee;
}

+ (UIColor *)colour_6fbbff
{
    static UIColor * colour_6fbbff = nil;
    if (!colour_6fbbff){
        colour_6fbbff = [UIColor colorWithRed:111/255.0 green:187/255.0 blue:255/255.0 alpha:1.0];
    }
    return colour_6fbbff;
}

+ (UIColor *)colour_e2e3e4
{
    static UIColor * colour_e2e3e4 = nil;
    if (!colour_e2e3e4){
        colour_e2e3e4 = [UIColor colorWithRed:226/255.0 green:227/255.0 blue:228/255.0 alpha:1.0];
    }
    return colour_e2e3e4;
}

+ (UIColor *)colour_42a9df
{
    static UIColor * colour_42a9df = nil;
    if (!colour_42a9df){
        colour_42a9df = [UIColor colorWithRed:66/255.0 green:169/255.0 blue:223/255.0 alpha:1.0];
    }
    return colour_42a9df;
}

+ (UIColor *)colour_b6bbc1
{
    static UIColor * colour_b6bbc1 = nil;
    if (!colour_b6bbc1){
        colour_b6bbc1 = [UIColor colorWithRed:182/255.0 green:187/255.0 blue:193/255.0 alpha:1.0];
    }
    return colour_b6bbc1;
}

+ (UIImage *)bgImage
{
    static UIImage * bgImage = nil;
    if (!bgImage){
        bgImage = [UIImage imageNamed:@"error_bg"];
    }
    return bgImage;
}

+ (UIImage *)avaImage
{
    static UIImage * bgImage = nil;
    if (!bgImage){
        bgImage = [UIImage imageNamed:@"ava_noborder"];
    }
    return bgImage;
}

+ (UIImage *)likedImage
{
    static UIImage * likedImage = nil;
    if (!likedImage){
        likedImage = [UIImage imageNamed:@"ico_like_yes"];
    }
    return likedImage;
}

@end
