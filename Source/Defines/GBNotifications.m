//  GBNotifications.m
//  GoBeside

#import "GBNotifications.h"

NSString * const AddNewPost = @"AddNewPost";

NSString * const UpgradeMessage = @"UpgradeMessage";
NSString * const UpgradeMessageList = @"UpgradeMessageList";

NSString * const ServerIsDeadNotification = @"ServerIsDeadNotification";


