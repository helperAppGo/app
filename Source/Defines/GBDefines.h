//  GBDefines.h
//  GoBeside

#ifndef GBDefines_h
#define GBDefines_h

#import <Foundation/Foundation.h>

#define CONTACTS_SYNC_ENABLED       YES

#define kProductionVersion  0
#define TEST_SERVER kProductionVersion == 0

#define kContactSyncDate  @"ContactSynDate1"
#define IS_IOS11  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11.0)

static NSString * const BASE_URL =  kProductionVersion ? @"https://gobeside.com" : @"http://helper.darx.net";

typedef NS_ENUM(NSInteger, FeedActionType) {
    FeedActionTypeLike  = 0,
    FeedActionTypeComment,
    FeedActionTypeShare,
    FeedActionTypeAvatar,
    FeedActionTypeMore,
    FeedActionTypeAction,
    FeedActionTypeLocation,
    
    FeedActionTypeVideo,
    FeedActionTypeImage,
    FeedActionTypeAnswers,
    FeedActionTypeName,
    FeedActionTypeDoubleClick,
    FeedActionTypeLongPress
};



#endif
