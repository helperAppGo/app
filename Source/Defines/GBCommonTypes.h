//  GBCommonTypes.h
//  GoBeside

typedef void (^GBValueBlock)(id value);
typedef void (^GBVoidBlock)(void);
typedef void (^GBBoolBlock)(BOOL result);
typedef void (^GBUIntBlock)(NSUInteger result);
typedef void (^GBStringBlock)(NSString * str);
typedef void (^GBSelectionBlock)(NSInteger selection);
typedef void (^GBArrayBlock)(NSArray *arr);
typedef void (^GBErrorBlock)(NSError *err);
typedef void (^GBCompletionBlock)(id response, NSError *error);

typedef void (^GBCompletionBlockTest)(NSData *data, NSURLResponse *response, NSError *error);


typedef void (^SelectItemBlock)(NSInteger);
typedef void (^SelectStringItemBlock)(NSString *);
typedef void (^ActionBlock)(void);
typedef void (^ActionDateBlock)(void);
typedef void (^ShowPhotoBlock)(UIImageView * imageView);
typedef void (^ShowMediaBlock)(UIView * view);
typedef void (^ShowUserProfileBlock)(NSInteger row);
typedef void (^LoadUserProfileBlock)(NSInteger row);

typedef NS_ENUM(NSInteger, TagCellType) {
    TagCellTypeSkill,
    TagCellTypeInterest,
    TagCellTypeTag,
    TagCellTypeFile,
    TagCellTypeLocation,
};

typedef NS_ENUM(NSInteger, StatusCellType) {
    StatusCellTypeActivate,
    StatusCellTypeProductAvailable,
    StatusCellTypeServiceAvailable,
};

typedef NS_ENUM(NSInteger, EditUserState) {
    EditUserStateDefault,
};

typedef NS_ENUM(NSInteger, MediaType) {
    MediaTypeImage = 1,
    MediaTypeFile,
    MediaTypeLink,
};

typedef NS_ENUM(NSInteger, CategoryCellType) {
    CTypeCellCategory,
};

typedef NS_ENUM(NSInteger, MLocationType) {
    MLocationTypeSingle,
};

typedef NS_ENUM(NSInteger, SuccessInfoViewType) {
    SuccessInfoViewTypeConfirmation,
    SuccessInfoViewTypeRegistration,
    SuccessInfoViewTypeContactUs,
    SuccessInfoViewTypeReportInappropriate,
    SuccessInfoViewTypeRequest
};

typedef void (^SetTextToModelBlock)(NSString *);
typedef void (^SetTextToWithConditionBlock)(NSString * text, BOOL condition);
typedef void (^GetIndexPathBlock)(NSIndexPath *);

typedef NS_ENUM(NSInteger, EUCellType) {
    EUCellTypeDefault,
    EUCellTypeJob,
    EUCellTypeJobDate,
    EUCellTypeDate,
    EUCellTypePhone
};

typedef NS_ENUM (NSInteger, EStreamItemType) {
    EStreamItemTypeUnknown,
    EStreamItemTypeFeed,
    EStreamItemTypeQuestions
};

typedef NS_ENUM(NSInteger, ECommentItemType) {
    ECommentItemTypeFeedComment,
    ECommentItemTypeAnswer
};

typedef NS_ENUM(NSUInteger, GBAction) {
    MActionUnknown,
    MActionDefault,
    MActionSelectAllPressed,
    
    MActionUserShowAvatar,
    MActionUserShowSettings,
    MActionUserShowInfo,
    MActionUserShowGroups,
    MActionUserShowMedia,
    MActionUserShowSubscriptionsList,
    MActionUserShowMessenger,
    
    MActionShowProfileWithMessagesUserID,
    MActionShowMessageAttachment,
    MActionRemoveAttachment,
    
    MActionUpdateCell,
    
    MActionLike,
    MActionShowProfile,
    MActionShowMediaView,
    MActionShowComments,
    MActionShowMenu,
};

typedef NS_ENUM(NSInteger, EGender) {
    EGenderFemale = 0,
    EGenderMale = 1
};

typedef NS_ENUM(NSInteger, EAttachmentType) {
    EAttachmentTypeUnknown  = 0,
    EAttachmentTypeImage    = 1,
    EAttachmentTypeVideo    = 2,
    EAttachmentTypeYoutube  = 3
};

@protocol GBActionDelegate <NSObject>

@optional
- (void)control:(id)control didSendAction:(GBAction)action withMessage:(id)message;

@end

@protocol MActionProtocol <NSObject>

@required
@property (nonatomic, weak) id<GBActionDelegate> actionDelegate;

- (void)sendAction:(GBAction)action;
- (void)sendAction:(GBAction)action withMessage:(id)message;

@end

@class EUAddTagView, MJobModel, MItemModel, HomePageTableViewController, ODRefreshControl, MPostEnvelop, MBaseEditVC;


@protocol MEditUserDelegate <NSObject>

- (void)showDatePicker:(NSDate *)date;
- (void)showAlertWithMessage:(NSString *)message;
- (void)showCameraWithConfigurator:(GBValueBlock)configurator;
- (void)showCoverCamera;
- (void)hidePickers;

@end

@protocol MRemoveMediaProtocol <NSObject>

- (void)removeMediaWithIndexPath:(NSIndexPath *)indexPath;
- (void)makeCoverWithIndexPath:(NSIndexPath *)indexPath;

@end

@protocol GetCellHeightProtocol <NSObject>

@optional

- (double)getCellHeightByString:(NSString *)data;

@end

@protocol GBEditItemProtocol <NSObject>

- (void)itemDidChanged:(MItemModel *)item;
- (void)itemDidCreated:(MItemModel *)item;
- (void)itemDidDeleted:(MItemModel *)item;
- (void)updateData;
- (void)reloadTable;

@end

@class SearchFilterModel;
@protocol GBQuestionPageDelegate <NSObject, UITableViewDelegate, UITableViewDataSource/*, MNMBottomPullToRefreshManagerClient*/>

@required
- (instancetype)initWithVC:(MBaseEditVC *)questionVC;
- (void)getDataForPage:(NSInteger)page;
- (void)setRefreshControls;

@optional
- (void)refreshData:(ODRefreshControl *)refreshControl_;
- (void)itemDidChanged:(id)item;
- (void)itemDidCreated:(id)item;
- (void)itemDidDeleted:(id)item;


@end

