//  GBCommonStyles.h
//  GoBeside

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GBCommonStyles : NSObject

+ (UIColor *)headerColour;
+ (UIColor *)mainThemeColour;
+ (UIColor *)colour_8b94a3;
+ (UIColor *)colour_b3b9c2;
+ (UIColor *)colour_505c6c;
+ (UIColor *)colour_a6adb7;
+ (UIColor *)colour_67c0ef;
+ (UIColor *)colour_f16173;
+ (UIColor *)colour_f3f7f9;
+ (UIColor *)colour_cbd3d6;
+ (UIColor *)colour_fffaee;
+ (UIColor *)colour_6fbbff;
+ (UIColor *)colour_e2e3e4;
+ (UIColor *)colour_42a9df;
+ (UIColor *)colour_b6bbc1;
+ (UIImage *)bgImage;
+ (UIImage *)avaImage;

@end
