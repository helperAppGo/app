//  GBNotifications.h
//  GoBeside

#import <Foundation/Foundation.h>

FOUNDATION_EXPORT NSString * const AddNewPost;

FOUNDATION_EXPORT NSString * const UpgradeMessage;
FOUNDATION_EXPORT NSString * const UpgradeMessageList;

FOUNDATION_EXPORT NSString * const ServerIsDeadNotification;
