//
//  GBInGroupViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 26.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBInGroupViewController.h"
#import "GBPostTableViewCell.h"
#import "GBFeedDataSource.h"

@interface GBInGroupViewController ()

@end

@implementation GBInGroupViewController

#pragma mark - GBStreamViewController

+ (EStreamItemType) streamItemType
{
    return EStreamItemTypeFeed;
}

#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    self.mainStaticView.alpha = 0.0;
    [GBPostTableViewCell registerForTableView:self.tableView];
}

#pragma mark - Public

- (IBAction)countPeopleButton:(id)sender{}

- (void) showMainStaticView
{
    WEAKSELF_DECLARATION
    [UIView animateWithDuration:0.2 animations:^{
        STRONGSELF_DECLARATION
        strongSelf.mainStaticView.alpha = 1.0;
    }];
}

#pragma mark - GBStreamItemCellDelegate

- (void) didChangeLikeValueInCell:(id<GBStreamItemCellProtocol>)aCell
{
    [super didChangeLikeValueInCell:aCell];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell*)aCell];
    GBWallModel *model = [(GBPostTableViewCell*)aCell wmodel];
    WEAKSELF_DECLARATION
    [GBFeedDataSource likePostGroup:model.idCountWall completion:^(BOOL result) {
        if (!result) {
            STRONGSELF_DECLARATION
            model.is_liked = !model.is_liked;
            model.countLikes += model.is_liked ? 1 : (-1);
            if (indexPath)
                [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}

//

@end
