//
//  GBWallCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBStreamItemTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "iCarousel.h"
#import "GBWallModel.h"
#import "GBServerManager.h"
#import "GBWallController.h"
#import "GBButtonLike.h"
#import "GBFeedDataSource.h"
#import "GBNotAdminGroupController.h"

@interface GBWallCell : GBStreamItemTableViewCell <iCarouselDelegate, iCarouselDataSource>

@property (nonatomic,strong) GBWallModel * model;

- (void)configureWithModel:(GBWallModel *)wallModel delegate:(id<GBStreamItemCellDelegate>)delegate;

@end

