//
//  GBWallModel.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/25/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBWallModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) NSInteger idCountWall;
@property (nonatomic, strong) NSNumber *idUser;
@property (nonatomic, strong) NSNumber *idWall;
@property (nonatomic, strong) NSString *files;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, assign) NSInteger countLikes;
@property (nonatomic, assign) NSInteger countComments;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL *path_ava;
@property (nonatomic, assign) BOOL is_liked;
@property (nonatomic, strong) NSString *text;
@property (assign, nonatomic) BOOL isNoImageGroup;
@property (assign, nonatomic) BOOL isNoTextGroup;

@property (nonatomic, assign) CGFloat heightObjects;
@property (nonatomic, strong) NSNumber *idObjects;
@property (strong, nonatomic) NSURL *pathUrl;
@property (nonatomic, strong) NSString *typeObjects;
@property (nonatomic, assign) CGFloat widthObjects;
@property (assign, nonatomic) CGFloat loadError;

+(NSArray *)getObjectsFromArray:(NSArray *)data;
+(GBWallModel *)wallFromDictionary:(NSDictionary *)data;
- (void) likeAction;

@end
