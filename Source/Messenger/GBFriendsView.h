//
//  GBFriendsView.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/23/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBServerManager.h"
#import "GBFriendsGroupCell.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBDataParser.h"

@interface GBFriendsView : UIViewController <UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    GBDataParser *dataParser;
//    MainDataHolder *dataHolder;
}

@property (weak, nonatomic) IBOutlet UITableView *friendsTableView;

- (void) getDataFriendsList;

@end
