//
//  Inputbar.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/20/17.
//  Copyright © 2017 SG. All rights reserved.
//

@protocol InputbarDelegate;

@interface Inputbar : UIToolbar

@property (nonatomic, assign) id<InputbarDelegate>delegate;
@property (nonatomic) NSString *placeholder;
@property (nonatomic) UIImage *leftButtonImage;
@property (nonatomic) NSString *rightButtonText;
@property (nonatomic) UIColor  *rightButtonTextColor;

-(void)resignFirstResponder;
-(NSString *)text;

@end



@protocol InputbarDelegate <NSObject>
-(void)inputbarDidPressRightButton:(Inputbar *)inputbar;
-(void)inputbarDidPressLeftButton:(Inputbar *)inputbar;
@optional
-(void)inputbarDidChangeHeight:(CGFloat)new_height;
-(void)inputbarDidBecomeFirstResponder:(Inputbar *)inputbar;
@end

