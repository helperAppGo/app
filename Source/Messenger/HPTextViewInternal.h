//
//  HPTextViewInternal.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/20/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HPTextViewInternal : UITextView

@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, strong) UIColor *placeholderColor;
@property (nonatomic) BOOL displayPlaceHolder;

@end

