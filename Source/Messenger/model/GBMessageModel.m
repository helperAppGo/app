//
//  GBMessage.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBMessageModel.h"

#define   SHOW_LOGS             NO
#define   Error(format, ...)    if (SHOW_LOGS) NSLog(@"ERROR: %@", [NSString stringWithFormat:format, ## __VA_ARGS__]);


@implementation GBMessageModel

+ (NSArray *)getObjectsFromArray:(NSArray *)data
{
    if ((id)data == [NSNull null])
    {
        return @[];
    }
    
    NSMutableArray * result = [NSMutableArray array];
    
    for (NSDictionary * dict in data)
    {
        @try
        {
            // [result addObject:[GBMessageModel messageFromDictionary:dict]];
            [result insertObject:[GBMessageModel messageFromDictionary:dict] atIndex:0];
        }
        @catch (NSException *exception)
        {
            Error(@"%@", exception);
        }
    }
    return result;
}

+ (GBMessageModel *)messageFromDictionary:(NSDictionary *)data {
    
    GBMessageModel *message = [[GBMessageModel alloc] init];
    
    message.filesMessage = data[@"files"];
    message.idMessage = data[@"id"];
    NSLog(@"ID MESSAGE FROM DICTIONARY - %@ \n", data[@"id"]);
    message.idFromMessage = data[@"id_from"];
    message.idToMessage = data[@"id_to"];
    message.isReadMessage = data[@"is_read"];
    message.textMessage = data[@"text"];
    
    NSString *time = data[@"time"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [formatter dateFromString:time];
    message.dateMessage = date;
    
    message.countUnread = data[@"count_unread"];
    
    
    // --- MY MESSAGE OR SOMEONE MESSEGE ---------------------------------------------
    //    int userId = [[[UGSFetchController sharedInstance] userID] integerValue];
    //    int idFrom = [message.idFromMessage integerValue];
    //    if (idFrom == userId) {
    //        NSLog(@"true");
    //        message.sender = MessageSenderMyself;
    //    } else {
    //        message.sender = MessageSenderSomeone;
    //    }
    // --- MY MESSAGE OR SOMEONE MESSEGE ---------------------------------------------
    
    
    
    return message;
}


@end
//
