//
//  GBUserModel.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/1/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBUserModel.h"

#define   SHOW_LOGS             NO
#define   Error(format, ...)    if (SHOW_LOGS) NSLog(@"ERROR: %@", [NSString stringWithFormat:format, ## __VA_ARGS__]);

@implementation GBUserModel

#pragma mark - Others

+ (GBUserModel *)userWithNumberId:(NSNumber *)ID from:(NSArray *)users
{
    return [self userWithStringId:ID.description from:users];
}

+ (GBUserModel *)userWithStringId:(NSString *)ID from:(NSArray *)users
{
    if (ID.integerValue == 0) return nil;
    
    NSArray *filtered = [users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"idUser == %d", ID.integerValue]];
    
    return filtered.firstObject;
}

+ (instancetype)userWithID:(NSNumber *)ID
{
    GBUserModel *user = [self new];
    user.idUser = ID;
    
    return user;
}

+ (NSArray *)getObjectsFromArray:(NSArray *)data
{
    if ((id)data == [NSNull null])
    {
        return @[];
    }
    
    NSMutableArray * result = [NSMutableArray array];
    
    for (NSDictionary * dict in data)
    {
        @try
        {
            if (![[dict[@"id"] stringValue] isEqual:@"1167"] ){
                
                [result addObject:[GBUserModel getObjectByDictionary:dict]];
                
            }
        }
        @catch (NSException *exception)
        {
            Error(@"%@", exception);
        }
    }
    return result;
}

+ (GBUserModel *)getObjectByDictionary:(NSDictionary *)data
{
    GBUserModel *user = [[GBUserModel alloc] init];
    
    user.bDayUser = data[@"b_day"];
    user.bMonthUser = data[@"b_month"];
    user.bYearUser = data[@"b_year"];
    user.fileAvaUser = data[@"file_ava"];
    user.fileCoverUser = data[@"file_cover"];
    user.firstNameUser = data[@"first_name"];
    user.idUser = data[@"id"];
    user.idCityUser = data[@"id_city"];
    user.infoUser = data[@"info"];
    user.langUser = data[@"lang"];
    user.lastNameUser = data[@"last_name"];
    user.pathAvaUser = data[@"path_ava"];
    user.pathCoverUser = data[@"path_cover"];
    user.statusUser = data[@"status"];
    
    return user;
}

+ (NSString *)parsePhone:(NSString *)phone
{
    if (!phone.length)
    {
        return phone;
    }
    
    NSRange rg = [phone rangeOfString:@"+" options:NSBackwardsSearch];
    
    if (rg.location == NSNotFound)
    {
        return [NSString stringWithFormat:@"+%@",phone];
    }
    
    return phone;
}

- (id)copyWithZone:(NSZone *)zone
{
    GBUserModel *another = [[GBUserModel alloc] init];
    
    another.bDayUser = self.bDayUser;
    another.bMonthUser = self.bMonthUser;
    another.bYearUser = self.bYearUser;
    another.fileAvaUser = self.fileAvaUser;
    another.fileCoverUser = self.fileCoverUser;
    another.firstNameUser = self.firstNameUser;
    another.idUser = self.idUser;
    another.idCityUser = self.idCityUser;
    another.infoUser = self.infoUser;
    another.langUser = self.langUser;
    another.lastNameUser = self.lastNameUser;
    another.pathAvaUser = self.pathAvaUser;
    another.pathCoverUser = self.pathCoverUser;
    another.statusUser = self.statusUser;
    
    return another;
}

- (NSMutableDictionary *)getDictionary
{
    NSMutableDictionary * data = [NSMutableDictionary dictionary];
    
    data[@"b_day"] = self.bDayUser;
    data[@"b_month"] = self.bMonthUser;
    data[@"b_year"] = self.bYearUser;
    data[@"file_ava"] = self.fileAvaUser;
    data[@"file_cover"] = self.fileCoverUser;
    data[@"first_name"] = self.firstNameUser;
    data[@"id"] = self.idUser;
    data[@"id_city"] = self.idCityUser;
    data[@"info"] = self.infoUser;
    data[@"lang"] = self.langUser;
    data[@"last_name"] = self.lastNameUser;
    data[@"path_ava"] = self.pathAvaUser;
    data[@"path_cover"] = self.pathCoverUser;
    data[@"status"] = self.statusUser;
    
    
    return data;
}

- (BOOL)isEqual:(GBUserModel *)object
{
    if ([object isKindOfClass:[GBUserModel class]] && [_idUser isEqual:object.idUser])
    {
        return YES;
    }
    return NO;
}

- (NSUInteger)hash
{
    return [_idUser hash];
}

@end
