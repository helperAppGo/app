//
//  GBMessage.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "UGSFetchController.h"

@interface GBMessageModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *filesMessage;
@property (nonatomic, strong) NSNumber *idMessage;
@property (nonatomic, strong) NSNumber *idFromMessage;
@property (nonatomic, strong) NSNumber *idToMessage;
@property (nonatomic, strong) NSString *isReadMessage;
@property (nonatomic, strong) NSString *textMessage;
@property (nonatomic, strong) NSDate *dateMessage;
@property (nonatomic, strong) NSNumber *countUnread;

+(NSArray *)getObjectsFromArray:(NSArray *)data;

+(GBMessageModel *)messageFromDictionary:(NSDictionary *)data;

@end
