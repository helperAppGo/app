//
//  GBUserModel.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/1/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface GBUserModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *bDayUser;
@property (nonatomic, strong) NSNumber *bMonthUser;
@property (nonatomic, strong) NSNumber *bYearUser;
@property (nonatomic, strong) NSNumber *fileAvaUser;
@property (nonatomic, strong) NSNumber *fileCoverUser;
@property (nonatomic, strong) NSString *firstNameUser;
@property (nonatomic, strong) NSNumber *idUser;
@property (nonatomic, strong) NSNumber *idCityUser;
@property (nonatomic, strong) NSString *infoUser;
@property (nonatomic, strong) NSString *langUser;
@property (nonatomic, strong) NSString *lastNameUser;
@property (nonatomic, strong) NSString *pathAvaUser;
@property (nonatomic, strong) NSString *pathCoverUser;
@property (nonatomic, strong) NSString *statusUser;

@property (nonatomic, strong) NSString * lastMes;

+ (instancetype)userWithID:(NSNumber *)ID;
+ (NSArray *)getObjectsFromArray:(NSArray *)data;

+ (GBUserModel *)getObjectByDictionary:(NSDictionary *)data;
+ (GBUserModel *)userWithNumberId:(NSNumber *)ID from:(NSArray *)users;
+ (GBUserModel *)userWithStringId:(NSString *)ID from:(NSArray *)users;

- (NSMutableDictionary *)getDictionary;


@end

