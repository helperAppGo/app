//
//  GBMessageController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//
#import "GBMessageController.h"

#define getMessages                 [BASE_URL stringByAppendingString:@"/api/dialogs/messages/last"]
#define sendMessageURL              [BASE_URL stringByAppendingString:@"/api/dialogs/messages"]
#define sendFilesURL                [BASE_URL stringByAppendingString:@"/api/files"]
#define putReadMessage              [BASE_URL stringByAppendingString:@"/api/dialogs/messages/read"]
#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

#define RIGHT_BUTTON_SIZE 60
#define LEFT_BUTTON_SIZE 45
#define kOFFSET_FOR_KEYBOARD 80.0

@interface GBMessageController () <UITabBarDelegate, UIActionSheetDelegate>
@property (nonatomic) NSMutableArray *jsonAllMessage;
@property (nonatomic) NSMutableArray *localParseArray;
@property (nonatomic) UIView *viewBar;
@property (nonatomic) UITextView *messageTV;
@property (nonatomic) UIButton *sendButton;
@property (nonatomic) UIButton *addButton;
@property (nonatomic) UIImage *leftButtonImage;

@property (assign, nonatomic) BOOL keyboardIsShown;


@end

@implementation GBMessageController{
    NSString *filesPath;
    BOOL isReadMessage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerForKeyboardNotifications];
    self.jsonAllMessage = [[NSMutableArray alloc] init];
    self.localParseArray = [[NSMutableArray alloc] init];
    
    
    NSLog(@"our id = %@", _idUserForChat);
    dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
    
    self.messageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.messageTableView.delegate = self;
    self.messageTableView.dataSource = self;
    self.backgroundImageView.backgroundColor = Rgb2UIColor(211, 211, 211);
    
// при нажатии на CELL показывает серым цветом.
//    [self.messageTableView registerClass: UITableViewCell.self forCellReuseIdentifier:@"cell"];
    
    

    
    
    
    
    //    //Instantiating custom view that adjusts itself to keyboard show/hide
    //    self.handler = [[ContentView alloc] initWithTextView:self.messageTV ChatTextViewHeightConstraint:self.chatTextViewHeightConstraint contentView:self.contentView ContentViewHeightConstraint:self.contentViewHeightConstraint andContentViewBottomConstraint:self.contentViewBottomConstraint];
    //
    //    //Setting the minimum and maximum number of lines for the textview vertical expansion
    //    [self.handler updateMinimumNumberOfLines:1 andMaximumNumberOfLine:3];
    //
    
    //Tap gesture on table view so that when someone taps on it, the keyboard is hidden
//    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
//    
//    [self.messageTableView addGestureRecognizer:gestureRecognizer];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    self.navigationItem.title = _nameUserForChat;
    
    [self getAllMessanges];
    
    double screenWidth = self.view.frame.size.width;
    CGSize size = [[UIScreen mainScreen]bounds].size;
    
    UIView *viewB = [[UIView alloc] initWithFrame:CGRectMake(-1, 0, screenWidth, 50)];
    viewB.backgroundColor = [UIColor whiteColor];
    viewB.layer.borderWidth = 1.0;
    viewB.layer.borderColor = [Rgb2UIColor(204, 204, 204) CGColor];
    
    
    self.messageTV = [[UITextView alloc] initWithFrame: CGRectMake(LEFT_BUTTON_SIZE, 10, size.width - LEFT_BUTTON_SIZE - RIGHT_BUTTON_SIZE, 30)];
    self.messageTV.layer.cornerRadius = 5.0;
    self.messageTV.clipsToBounds = YES;
    self.messageTV.layer.borderColor = [[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor];
    self.messageTV.layer.borderWidth = 1.0;
    self.messageTV.font = [UIFont systemFontOfSize:16];
    self.messageTV.delegate = self;
    
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.sendButton.frame = CGRectMake(size.width - RIGHT_BUTTON_SIZE, 0, RIGHT_BUTTON_SIZE, 50);
    [self.sendButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
    [self.sendButton setTitle:@"Send" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.sendButton addTarget:self action:@selector(sendAction:) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.addButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.addButton.frame = CGRectMake(0, 0, LEFT_BUTTON_SIZE, 50);
    self.leftButtonImage = [UIImage imageNamed:@"share"];
    [self.addButton setImage:self.leftButtonImage forState:UIControlStateNormal];
    [self.addButton addTarget:self action:@selector(AddButton:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.view addSubview:self.messageTV];
    [viewB addSubview:self.sendButton];
    [viewB addSubview:self.addButton];
    
    self.viewBar = viewB;
    
    [viewB addSubview:self.messageTV];
}


- (void) viewDidAppear:(BOOL)animated {
    [self createExampleChat];
    if (_jsonAllMessage.count)
        [_messageTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([_jsonAllMessage count] - 1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}


-(void) viewWillDisappear:(BOOL)animated
{
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdated"
                                                            object:self];
    }
    [super viewWillDisappear:animated];
}


- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}


// Called when the UIKeyboardWillShowNotification is sent
- (void)keyboardWillBeShown:(NSNotification*)aNotification {
    if(self.keyboardIsShown) {
        
    CGFloat keyboardHeight = [[[aNotification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
        
    // Change table contentInset
    UIEdgeInsets contentInset = UIEdgeInsetsMake(0.0, 0.0, (keyboardHeight), 0.0);;
    contentInset.bottom = keyboardHeight-50; // КОСТЫЛЬ
    
    UIEdgeInsets scrollIndicatorInsets = self.messageTableView.scrollIndicatorInsets;
    scrollIndicatorInsets.bottom = keyboardHeight;
    
    [UIView animateWithDuration:3.0 animations:^{
        self.messageTableView.contentInset = contentInset;
        self.messageTableView.scrollIndicatorInsets = scrollIndicatorInsets;
    }];
    
// Optional : Display last cell with animation
    if ([_jsonAllMessage count] != 0)
    {
        [_messageTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([_jsonAllMessage count] - 1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
        return;
    }
    self.keyboardIsShown = YES;
    
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    self.keyboardIsShown = NO;
    
    // Reset table contentInset
    UIEdgeInsets contentInset = self.messageTableView.contentInset;
    contentInset.bottom = 0.0f;
    UIEdgeInsets scrollIndicatorInsets = self.messageTableView.scrollIndicatorInsets;
    scrollIndicatorInsets.bottom = 0.0f;
    
    [UIView animateWithDuration:3.0 animations:^{
        self.messageTableView.contentInset = UIEdgeInsetsZero;
        self.messageTableView.scrollIndicatorInsets = UIEdgeInsetsZero;
    }];
    
    // Optional : Display last cell with animation
    if ([_jsonAllMessage count] != 0)
    {
        [_messageTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:([_jsonAllMessage count] - 1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    [self scrollToTheBottom:YES];
}


- (BOOL)canBecomeFirstResponder {
    return YES;
}


- (UIView *)inputAccessoryView {
    return self.viewBar;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [[[NSBundle mainBundle] loadNibNamed:@"messageController" owner:self options:nil] objectAtIndex:0];
    }
    return self; }


- (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:completion];
    
    [dataTask resume];
}

- (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link{
    NSMutableURLRequest *request;
    if (method == postMethod) {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
        }
        
        NSError *err;
        NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:dict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&err];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonDict];
        
        return request;
    } else if (method == getMethodChat){
        NSString *requestString = [NSString stringWithString:link];
        NSArray *k = [dict allKeys];
        NSArray *v = [dict allValues];
        requestString = [requestString stringByAppendingString:@"?data={"];
        for (id key in k) {
            NSString *keyString = [NSString stringWithFormat:@"\"%@\":", key];
            NSString *valueString = [NSString stringWithFormat:@"%@", [v objectAtIndex:[k indexOfObject:key]]];
            keyString = [keyString stringByAppendingString:valueString];
            requestString = [requestString stringByAppendingFormat:@"%@", keyString];
            if (key != k.lastObject)
                requestString = [requestString stringByAppendingString:@","];
        }
        requestString = [requestString stringByAppendingString:@"}"];
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        return request;
    } else if (method == getMethodMessage) {
        NSString *requestString = [NSString stringWithString:link];
        
        NSString *id_user = @"id_user";
        NSString *idUser = [NSString stringWithFormat:@"%@",_idUserForChat];
        
        NSString *time = @"time";
        NSString *kTime = @"null";
        
        NSString *limit = @"limit";
        NSString *kLimit = @"25";
        
        NSString *offset = @"offset";
        NSString *kOffset = @"0";
        //        NSString *kOffset = [NSString stringWithFormat:@"%i",self.tableArray.numberOfSections];
        
        
        requestString = [requestString stringByAppendingString:@"?data={"];
        
        NSString *keyIdUser = [NSString stringWithFormat:@"\"%@\":", id_user];
        requestString = [requestString stringByAppendingString:keyIdUser];
        requestString = [requestString stringByAppendingString:idUser];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyTime = [NSString stringWithFormat:@"\"%@\":", time];
        requestString = [requestString stringByAppendingString:keyTime];
        requestString = [requestString stringByAppendingString:kTime];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyLimit = [NSString stringWithFormat:@"\"%@\":", limit];
        requestString = [requestString stringByAppendingString:keyLimit];
        requestString = [requestString stringByAppendingString:kLimit];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyOffset = [NSString stringWithFormat:@"\"%@\":", offset];
        requestString = [requestString stringByAppendingString:keyOffset];
        requestString = [requestString stringByAppendingString:kOffset];
        
        requestString = [requestString stringByAppendingString:@"}"];
        
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        return request;
    } else {
        request = [NSMutableURLRequest new];
        
        return request;
        
    }
}




-(void) getAllMessanges{
    NSLog(@"GO TO ALL MESSANGES");
    
    NSURLRequest *request = [self makeRequestWithMethod:getMethodMessage params:NULL andURL: getMessages];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [self getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            self.localParseArray = [dataParser parseMessages:[NSJSONSerialization JSONObjectWithData:data
                                                                                             options:kNilOptions
                                                                                               error:nil]];
            NSLog(@"json: %@", self.localParseArray);
            [self isReadMessage];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //                [self.messageTableView reloadData];
                //                [self scrollToTheBottom:NO];
            });
        }
    }];
}


- (void) isReadMessage {
    NSArray *isReadLast = [[NSArray alloc] init];
    NSNumber *idMessageLast = [[NSNumber alloc] init];
    
    for (NSDictionary * dict in self.localParseArray) {
        if (![USER_ID isEqual:[dict valueForKey:@"idFromMessage"]]) {
            isReadLast = [dict valueForKey:@"isReadMessage"];
            idMessageLast = [dict valueForKey:@"idMessage"];

            NSLog(@"IS READ: %@", isReadLast);
            NSLog(@"ID MESSAGE: %@", idMessageLast);


            NSDictionary *parameters = @{@"id": idMessageLast};

            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
            NSLog(@"JSON = %@", jsonData);
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[jsonData length]];
            NSURL *url = [NSURL URLWithString:putReadMessage];
            NSMutableURLRequest  *request = [[NSMutableURLRequest alloc] init];

            if ([[UGSFetchController sharedInstance] token]) {
                [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
                [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
            }

            [request setURL:url];
            [request setHTTPMethod:@"PUT"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];

            // get the value for the CONTENT-TYPE from postman
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:jsonData];

            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        return;

                                                    }
                                                    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                                        NSLog(@"PUT Response HTTP Status code: %ld\n", (long) [(NSHTTPURLResponse *) response statusCode]);
                                                        NSLog(@"PUT Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *) response allHeaderFields]);
                                                    }

                                                    NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                    NSLog(@"PUT Responcse Body:\n%@\n", body);
                                                }];
            [task resume];
        }
    }
}


-(UIImage *)imageNamed:(NSString *)imageName {
    return [UIImage imageNamed:imageName
                      inBundle:[NSBundle bundleForClass:[self class]]
 compatibleWithTraitCollection:nil];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"nubber message --- = %lu" , (unsigned long)self.jsonAllMessage.count);
    return   self.jsonAllMessage.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    UIView *chatBubble = [self.jsonAllMessage objectAtIndex:indexPath.row];
    chatBubble.tag = indexPath.row;
    
    for (int i = 0; i < cell.contentView.subviews.count; i++ ) {
        UIView *subV = cell.contentView.subviews[i];
        if (subV.tag != chatBubble.tag)
            [subV removeFromSuperview];
    }
    
    [cell.contentView addSubview:chatBubble];
    
    cell.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    
    return  cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.messageTV resignFirstResponder];
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UIView *bubble = self.jsonAllMessage[indexPath.row];
    return bubble.frame.size.height+20;
}

// PNG Image with color ----------------------------------------------------------------------
- (UIImage *)overlayImage:(UIImage *)image withColor:(UIColor *)color
{
    //  Create rect to fit the PNG image
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    //  Start drawing
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    //  Fill the rect by the final color
    [color setFill];
    CGContextFillRect(context, rect);
    
    //  Make the final shape by masking the drawn color with the images alpha values
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    [image drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1];
    
    //  Create new image from the context
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    //  Release context
    UIGraphicsEndImageContext();
    
    return img;
}


#pragma mark - Message UI creation function(s)

- (UIView*)createMessageWithText: (NSString*)text Image: (UIImage*)image DateTime: (NSString*)dateTimeString isReceived: (BOOL)isReceived {
    
    //Get Screen Width
    
    double screenWidth = [[UIScreen mainScreen] bounds].size.width; //    self.view.frame.size.width;  // hot fix
    NSLog(@" nash ekran = %f" , screenWidth);
    CGFloat maxBubbleWidth = screenWidth - 50;
    
    UIView *outerView = [[UIView alloc]init];
    
    
    // color BABBLE image and BUBBLE background ------------------------------------------------
    UIView *chatBubbleView = [[UIView alloc]init];
    UIView *chatBubbleContentView = [[UIView alloc]init];
    if (isReceived == 0) {
        chatBubbleView.backgroundColor = Rgb2UIColor(0, 204, 71);
        chatBubbleView.layer.masksToBounds = YES;
        chatBubbleView.clipsToBounds = NO;
        chatBubbleView.layer.cornerRadius = 15;
        chatBubbleView.layer.shadowOffset = CGSizeMake(0, 0.7);
        chatBubbleView.layer.shadowRadius = 4;
        chatBubbleView.layer.shadowOpacity = 0.4;
        
        chatBubbleContentView.backgroundColor = Rgb2UIColor(0, 204, 71);
        chatBubbleContentView.clipsToBounds = YES;
    } else {
        chatBubbleView.backgroundColor = Rgb2UIColor(229, 229, 234);
        chatBubbleView.layer.masksToBounds = YES;
        chatBubbleView.clipsToBounds = NO;
        chatBubbleView.layer.cornerRadius = 15;
        chatBubbleView.layer.shadowOffset = CGSizeMake(0, 0.7);
        chatBubbleView.layer.shadowRadius = 4;
        chatBubbleView.layer.shadowOpacity = 0.4;
        
        chatBubbleContentView.backgroundColor = Rgb2UIColor(229, 229, 234);
        chatBubbleContentView.clipsToBounds = YES;
    }
    
    //Add Time
    UILabel *chatTimeLabel;
    if (dateTimeString != nil) {
        chatTimeLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, 16)];
        chatTimeLabel.font = [UIFont systemFontOfSize:10];
        chatTimeLabel.text = dateTimeString;
        chatTimeLabel.textColor = [UIColor grayColor];
        
        [chatBubbleContentView addSubview:chatTimeLabel];
    }
    
    //Add Image
    UIImageView *chatBubbleImageView;
    if (image != nil) {
        chatBubbleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 26, maxBubbleWidth-30, maxBubbleWidth-30)];
        chatBubbleImageView.image = image;
        chatBubbleImageView.contentMode = UIViewContentModeScaleAspectFill;
        chatBubbleImageView.layer.masksToBounds = YES;
        chatBubbleImageView.layer.cornerRadius = 4;
        
        [chatBubbleContentView addSubview:chatBubbleImageView];
    }
    
    
    //Add Text
    UILabel *chatBubbleLabel;
    if (text != nil) {
        UIFont *messageLableFont = [UIFont systemFontOfSize:16];
        
        CGSize maximumLableSize;
        if (chatBubbleImageView != nil) {
            maximumLableSize = CGSizeMake(chatBubbleImageView.frame.size.width, 1000);
            
            CGSize expectedLabelSize = [text sizeWithFont:messageLableFont
                                        constrainedToSize:maximumLableSize
                                            lineBreakMode:NSLineBreakByWordWrapping];
            chatBubbleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 21+chatBubbleImageView.frame.size.height, expectedLabelSize.width, expectedLabelSize.height+10)];
            
        }else{
            maximumLableSize = CGSizeMake(maxBubbleWidth, 1000);
            
            CGSize expectedLabelSize = [text sizeWithFont:messageLableFont
                                        constrainedToSize:maximumLableSize
                                            lineBreakMode:NSLineBreakByWordWrapping];
            
            chatBubbleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, expectedLabelSize.width, expectedLabelSize.height)];
        }
        
        chatBubbleLabel.frame = CGRectMake(chatBubbleLabel.frame.origin.x, chatBubbleLabel.frame.origin.y+5, chatBubbleLabel.frame.size.width, chatBubbleLabel.frame.size.height+10);
        
        chatBubbleLabel.text = text;
        chatBubbleLabel.font = messageLableFont;
        chatBubbleLabel.numberOfLines = 100;
        
        [chatBubbleContentView addSubview:chatBubbleLabel];
    }
    
    [chatBubbleView addSubview:chatBubbleContentView];
    
    CGFloat totalHeight = 0;
    CGFloat decidedWidth = 0;
    for (UIView *subView in chatBubbleContentView.subviews) {
        totalHeight += subView.frame.size.height;
        
        CGFloat width = subView.frame.size.width;
        if (decidedWidth < width)
            decidedWidth = width;
    }
    
    chatBubbleContentView.frame = CGRectMake(5, 5, decidedWidth, totalHeight);
    chatBubbleView.frame = CGRectMake(10, 10, chatBubbleContentView.frame.size.width+10, chatBubbleContentView.frame.size.height+10);
    
    outerView.frame = CGRectMake(7, 0, chatBubbleView.frame.size.width, chatBubbleView.frame.size.height);
    
    UIImageView *arrowIV = [[UIImageView alloc] init];
    [outerView addSubview:chatBubbleView];
    
    // color ACCET image --------------------------------------------------------------------
    if (isReceived == 0) {
        UIImage *pngImage = [UIImage imageNamed:@"chat_arrow"];
        UIColor *overlayColor = Rgb2UIColor(0, 204, 71);
        arrowIV.image = [self overlayImage:pngImage withColor:overlayColor];
        arrowIV.layer.shadowRadius = 4;
        arrowIV.layer.shadowOpacity = 0.4;
        arrowIV.layer.shadowOffset = CGSizeMake(-7.0, 0.7);
        arrowIV.layer.zPosition = 1;
        arrowIV.frame = CGRectMake(chatBubbleView.frame.origin.x-7, chatBubbleView.frame.size.height-10, 11, 14);
    } else {
        UIImage *pngImage = [UIImage imageNamed:@"chat_arrow"];
        UIColor *overlayColor = Rgb2UIColor(229, 229, 234);
        arrowIV.image = [self overlayImage:pngImage withColor:overlayColor];
        arrowIV.layer.shadowRadius = 4;
        arrowIV.layer.shadowOpacity = 0.4;
        arrowIV.layer.shadowOffset = CGSizeMake(-7.0, 0.7);
        arrowIV.layer.zPosition = 1;
        arrowIV.frame = CGRectMake(chatBubbleView.frame.origin.x-7, chatBubbleView.frame.size.height-10, 11, 14);
        
    }
    
    if (isReceived == 0) {
        chatBubbleContentView.frame = CGRectMake(5, 5, decidedWidth, totalHeight);
        chatBubbleView.frame = CGRectMake(screenWidth-(chatBubbleContentView.frame.size.width+10)-10, 10, chatBubbleContentView.frame.size.width+10, chatBubbleContentView.frame.size.height+10);
        
        arrowIV.transform = CGAffineTransformMakeScale(-1,1);
        arrowIV.frame = CGRectMake(chatBubbleView.frame.origin.x+chatBubbleView.frame.size.width-4, chatBubbleView.frame.size.height-10, 11, 14);
        
        outerView.frame = CGRectMake(screenWidth-((screenWidth+chatBubbleView.frame.size.width)-chatBubbleView.frame.size.width)-7, 0, chatBubbleView.frame.size.width, chatBubbleView.frame.size.height);
    }
    
    [outerView addSubview:arrowIV];
    
    return outerView;
}

#pragma mark - Other functions

- (void) createExampleChat {
    //    __block NSArray *objArr;
    //    __block NSString *filesPath;
    
    for (int i = 0; i < self.localParseArray.count ; i++){
        if ([[self.localParseArray[i] valueForKey:@"idFromMessage"] isEqual: [[UGSFetchController sharedInstance] userID]]) {
            if ([[self.localParseArray[i] valueForKey:@"filesMessage"] isEqual:@""]) {
                NSString *mysrt = [self.localParseArray[i] valueForKey:@"textMessage"];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm"];
                NSDate *date = [self.localParseArray[i] valueForKey:@"dateMessage"];
                NSString *mydate = [formatter stringFromDate:date];
//                NSString *mydate = [self.localParseArray[i] valueForKey:@"dateMessage"];
                
                UIView *ff = [self createMessageWithText:mysrt Image:nil DateTime:mydate isReceived:false];
                [self.jsonAllMessage addObject:ff];
            } else {
                NSNumber *idInteger = [NSNumber numberWithInteger: [[self.localParseArray[i] valueForKey:@"filesMessage"] integerValue]];
                NSArray *objArr = [NSArray arrayWithObject: idInteger];
                
                [[GBServerManager sharedManager] getFileList:objArr success:^(id responseObject) {
                    NSLog(@"file: %@", responseObject);
                    NSArray *files = [[responseObject valueForKey:@"response"] valueForKey:@"path"];
                    NSString *myArrayString = [files description];
                    NSCharacterSet *trim = [NSCharacterSet characterSetWithCharactersInString:@" (\n\")"];
                    NSString *result = [[myArrayString componentsSeparatedByCharactersInSet:trim] componentsJoinedByString:@""];
                    NSLog(@"%@", result);
                    
                    NSString *st = [BASE_URL stringByAppendingString:@"/"];
                    filesPath = [st stringByAppendingString:result];
                    NSLog(@"FULL URL IMAGE: %@", filesPath);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:filesPath]];
                        
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"HH:mm"];
                        NSDate *date = [self.localParseArray[i] valueForKey:@"dateMessage"];
                        NSString *mydate = [formatter stringFromDate:date];
//                        NSString *mydate = [self.localParseArray[i] valueForKey:@"dateMessage"];
                        
                        NSLog(@"NEW MESSAGE");
                        
                        UIView *ff = [self createMessageWithText:nil Image:[UIImage imageWithData:imageData] DateTime:mydate isReceived:false];
                        [self.jsonAllMessage addObject:ff];
                        
                        [self.messageTableView reloadData];
                        //                    [self scrollToTheBottom: YES];
                        
                        //                    [self.messageTV resignFirstResponder];
                        //                    self.messageTV.text = @"";
                        
                    });
                    
                } failure:^(NSError *error, NSDictionary *userInfo) {
                    NSLog(@"error: %@, userInfo: %@", error, userInfo);
                }];
                
                
                
                //                NSString *myFiles = [self.localParseArray[i] valueForKey:@"filesMessage"];
                //                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:myFiles]];
                //                NSString *mydate = [self.localParseArray[i] valueForKey:@"dateMessage"];
                //
                //                UIView *ff = [self createMessageWithText:nil Image:[UIImage imageWithData:imageData] DateTime:mydate isReceived:false];
                //                [self.jsonAllMessage addObject:ff];
            }
        } else {
            if ([[self.localParseArray[i] valueForKey:@"filesMessage"] isEqual:@""]) {
                NSString *mysrt = [self.localParseArray[i] valueForKey:@"textMessage"];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"HH:mm"];
                NSDate *date = [self.localParseArray[i] valueForKey:@"dateMessage"];
                NSString *mydate = [formatter stringFromDate:date];
//                NSString *mydate = [self.localParseArray[i] valueForKey:@"dateMessage"];
                
                UIView *ff = [self createMessageWithText:mysrt Image:nil DateTime:mydate isReceived:true];
                [self.jsonAllMessage addObject:ff];
            } else {
                NSNumber *idInteger = [NSNumber numberWithInteger: [[self.localParseArray[i] valueForKey:@"filesMessage"] integerValue]];
                NSArray *objArr = [NSArray arrayWithObject: idInteger];
                
                [[GBServerManager sharedManager] getFileList:objArr success:^(id responseObject) {
                    NSLog(@"file: %@", responseObject);
                    NSArray *files = [[responseObject valueForKey:@"response"] valueForKey:@"path"];
                    NSString *myArrayString = [files description];
                    NSCharacterSet *trim = [NSCharacterSet characterSetWithCharactersInString:@" (\n\")"];
                    NSString *result = [[myArrayString componentsSeparatedByCharactersInSet:trim] componentsJoinedByString:@""];
                    NSLog(@"%@", result);
                    
                    NSString *st = [BASE_URL stringByAppendingString:@"/"];
                    filesPath = [st stringByAppendingString:result];
                    NSLog(@"FULL URL IMAGE: %@", filesPath);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:filesPath]];
                        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"HH:mm"];
                        NSDate *date = [self.localParseArray[i] valueForKey:@"dateMessage"];
                        NSString *mydate = [formatter stringFromDate:date];
//                        NSString *mydate = [self.localParseArray[i] valueForKey:@"dateMessage"];
                        
                        NSLog(@"NEW MESSAGE");
                        
                        UIView *ff = [self createMessageWithText:nil Image:[UIImage imageWithData:imageData] DateTime:mydate isReceived:true];
                        [self.jsonAllMessage addObject:ff];
                        
                        [self.messageTableView reloadData];
                        //                    [self scrollToTheBottom: YES];
                        
                        //                    [self.messageTV resignFirstResponder];
                        //                    self.messageTV.text = @"";
                        
                    });
                    
                } failure:^(NSError *error, NSDictionary *userInfo) {
                    NSLog(@"error: %@, userInfo: %@", error, userInfo);
                }];
                
                
                //                dispatch_async(dispatch_get_main_queue(), ^{
                //
                //                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:filesPath]];
                //                    NSString *mydate = [self.localParseArray[i] valueForKey:@"dateMessage"];
                //
                //                    NSLog(@"NEW MESSAGE");
                //
                //                    UIView *ff = [self createMessageWithText:nil Image:[UIImage imageWithData:imageData] DateTime:mydate isReceived:true];
                //                    [self.jsonAllMessage addObject:ff];
                //                });
            }
        }
    }
    
    [self.messageTableView reloadData];
    [self scrollToTheBottom:NO];
}

- (void)scrollToTheBottom:(BOOL)animated {
    if (self.jsonAllMessage.count > 0) {
        NSLog(@"In scroll 1 ");
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.jsonAllMessage.count - 1 inSection:0];
        [self.messageTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}

// -----------------------------------------------------------------------------------------------
#pragma mark - DATA FORMATTER

- (NSString*)getDateTimeStringFromNSDate: (NSDate*)date {
    NSString *dateTimeString = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM, hh:mm a"];
    dateTimeString = [dateFormatter stringFromDate:date];
    
    return dateTimeString;
}



// -----------------------------------------------------------------------------------------------
#pragma mark - SEND MESSAGE

- (void)sendAction: (id)selector {
    GBMessageModel *tMessage = [[GBMessageModel alloc] init];
    
    NSDictionary *parameters = @{@"id_user": _idUserForChat, @"text": self.messageTV.text};
    
    NSString *post = [NSString stringWithFormat:@"%@", parameters];
    NSLog(@"POST: %@", post);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters
                                                       options:0
                                                         error:&error];
    NSString *postLength = @"0";
    //[NSString stringWithFormat:@"lu", (unsigned long)[jsonData length]];
    NSURL *url = [NSURL URLWithString:sendMessageURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
    
    if ([[UGSFetchController sharedInstance]token]) {
        [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
    }
    NSLog(@"TOKEN: %@", [[UGSFetchController sharedInstance] token]);
    NSLog(@"USER ID: %@", [[[UGSFetchController sharedInstance] userID] stringValue]);
    
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                if (!error) {
                                                    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                                                                 options:0
                                                                                                                   error:nil];
                                                    NSLog(@"RESPONSE: %@", responseDict);
                                                    tMessage.textMessage = [[responseDict valueForKey:@"response"] valueForKey:@"text"];
                                                    NSLog(@"new messege: %@", [[responseDict valueForKey:@"response"] valueForKey:@"text"]);
                                                    
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        UIView *newMsg = [self createMessageWithText:tMessage.textMessage Image:nil DateTime:[self getDateTimeStringFromNSDate:[NSDate date]] isReceived:0];
                                                        NSLog(@"NEW MSG: %@", newMsg);
                                                        NSLog(@"NEW TEXT: %@", tMessage.textMessage);
                                                        
                                                        [self.jsonAllMessage addObject:newMsg];
                                                        [self.messageTableView reloadData];
                                                        [self scrollToTheBottom: YES];
                                                        
//                                                        [self.messageTV resignFirstResponder];
                                                        self.messageTV.text = @"";
                                                    });
                                                    
                                                    
                                                } else {
                                                    NSLog(@"error: %@", error);
                                                    return;
                                                }
                                            }];
    [task resume];
    
    //    UIView *newMsg = [self createMessageWithText:tMessage.textMessage Image:nil DateTime:[self getDateTimeStringFromNSDate:[NSDate date]] isReceived:0];
    //    NSLog(@"NEW MSG: %@", newMsg);
    //    NSLog(@"NEW TEXT: %@", tMessage.textMessage);
    //
    //    [self.jsonAllMessage addObject:newMsg];
    //    [self.messageTableView reloadData];
    //    [self scrollToTheBottom: YES];
    //
    //    [self.messageTV resignFirstResponder];
    //    self.messageTV.text = @"";
}


// -----------------------------------------------------------------------------------------------
#pragma mark - ADD BUTTON

- (IBAction)AddButton:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style: UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        NSLog(@"IAM CAMERA");
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo&Video Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        NSLog(@"IAM PHOTO AND VIDEO");
        [self photoOpen];
        
        //        [self dismissViewControllerAnimated:YES completion:^{
        //        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Document" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        NSLog(@"IAM DOCUMENT");
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Location" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        NSLog(@"IAM LOCATION");
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Contact" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // Distructive button tapped.
        NSLog(@"IAM CONTACT");
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}



// -----------------------------------------------------------------------------------------------
#pragma mark - UIAlertController VOID FUNCTIONS
- (void) cameraOpen {
    
}

- (void) photoOpen {
    [self libraryPhoto];
}

- (void) documentOpen {
    
}

- (void) locationOpen {
    
}

- (void) contactOpen {
    
}

// -----------------------------------------------------------------------------------------------
#pragma mark - IMAGE LIBRARY PICKER

- (void) libraryPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    NSLog(@"INFO : %@", info);
    
    __block NSArray *objArr;
    __block NSString *filesPath;
    
    UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];
    [[GBServerManager sharedManager] uploadFile:UIImagePNGRepresentation(image) withName:@"bg.jpg" success:^(id responseObject) {
        NSLog(@"file: %@", responseObject);
        NSLog(@"FILE PATH: %@", [[responseObject valueForKey:@"response"] valueForKey:@"path"]);
        NSNumber *idInteger = [NSNumber numberWithInteger: [[[responseObject valueForKey:@"response"] valueForKey:@"id"] integerValue]];
        objArr = [NSArray arrayWithObject: idInteger];
        NSLog(@"FILE OBJARR: %@", objArr);
        
    } failure:^(NSError *error, NSDictionary *userInfo) {
        NSLog(@"error: %@, userInfo: %@", error, userInfo);
    }];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 10 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [[GBServerManager sharedManager] getFileList:objArr success:^(id responseObject) {
            NSLog(@"GET file: %@", responseObject);
            
            filesPath = [[responseObject valueForKey:@"response"] valueForKey:@"path"];
            
            NSString *st = BASE_URL;
            NSString *q = (@"%@/%@", st, filesPath);
            
            filesPath = q;
            
        } failure:^(NSError *error, NSDictionary *userInfo) {
            NSLog(@"error: %@, userInfo: %@", error, userInfo);
        }];
        
    });
    
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:filesPath]];
    UIImage *img = [UIImage imageWithData:imageData];
    
    UIView *newMsg = [self createMessageWithText:@"" Image:img DateTime:[self getDateTimeStringFromNSDate:[NSDate date]] isReceived:0];
    //    UIView *newMsg = [self createMessageWithText:@"" Image:image DateTime:[self getDateTimeStringFromNSDate:[NSDate date]] isReceived:0];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.jsonAllMessage addObject:newMsg];
    [self.messageTableView reloadData];
    [self scrollToTheBottom: YES];
    
    [self.messageTV resignFirstResponder];
    self.messageTV.text = @"";
}


-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    NSLog(@"Did cancel Picker");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

