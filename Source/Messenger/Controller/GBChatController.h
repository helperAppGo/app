//
//  GBChatController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBDataParser.h"
#import "GBChatCell.h"
#import "GBMessageController.h"
#import "EventSource.h"

@interface GBChatController : UIViewController <UITextFieldDelegate>
{
    GBDataParser *dataParser;
    MainDataHolder * dataHolder;
}

@property (weak, nonatomic) IBOutlet UITableView *chatTableView;


@property (nonatomic, strong) NSMutableArray *jsonArray;
@property (nonatomic, strong) NSArray *jsonU;
@property (nonatomic, strong) NSArray *jsonM;
@property (nonatomic, strong) NSArray *jsonSorted;


-(void) getDialogLast;

-(void) refreshChatController;

@end

