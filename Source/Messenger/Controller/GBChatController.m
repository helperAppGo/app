//
//  GBChatController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBChatController.h"

#define dialogLast          [BASE_URL stringByAppendingString:@"/api/dialogs/last"]
#define sseURL              [BASE_URL stringByAppendingString:@"/api/sse"]

@interface GBChatController () <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) NSMutableArray *tableData;

@end

@implementation GBChatController {
    NSString *eventString;
    NSDictionary *dataDict;
}

@synthesize jsonM, jsonU, jsonArray, jsonSorted;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self refreshChatController];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scheduleLocalNotifications) name:@"SnoozeNotifcation" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleRefreshTable:)
                                                 name:@"DataUpdated"
                                               object:nil];
    
    [self sseMethod];
    [self getDialogLast];
    [self setTableView];
    dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
}

- (void) viewDidAppear:(BOOL)animated {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)handleRefreshTable:(NSNotification *)notification {
    NSLog(@"recieved");
//    [self.chatTableView reloadData];
    [self getDialogLast];
}


-(void)setTableView {
    //    self.tableData = [[NSMutableArray alloc] init];
    //    self.tableView.delegate = self;
    //    self.tableView.dataSource = self;
    //    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    //    self.tableView.backgroundColor = [UIColor clearColor];
}

- (void) refreshChatController {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.chatTableView addSubview:refreshControl];
    [self.chatTableView sendSubviewToBack:refreshControl];
}


- (void)handleRefresh:(UIRefreshControl *)refreshControl {
    
    double delayInSeconds = 0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"DONE");
        
        [self getDialogLast];
        
        // When done requesting/reloading/processing invoke endRefreshing, to close the control
        [self.chatTableView reloadData];
        [refreshControl endRefreshing];
    });
}





- (void) sseMethod {
    NSString *requestString = sseURL;
    CDUser *user = [[SettingsManager instance] currentUser];
    NSString *idSSE = [NSString stringWithFormat:@"%@",@(user.objId)];
    NSString *tokenSSE = user.token;
    
    requestString = [requestString stringByAppendingString:@"?id="];
    requestString = [requestString stringByAppendingString:idSSE];
    requestString = [requestString stringByAppendingString:@"&"];
    
    requestString = [requestString stringByAppendingString:@"token="];
    requestString = [requestString stringByAppendingString:tokenSSE];
    
    NSURL *serverURL = [NSURL URLWithString:requestString];
    
    EventSource *source = [EventSource eventSourceWithURL:serverURL];
    [source onMessage:^(Event *e) {
        NSLog(@"SSE EVENT: %@; SSE DATA: %@", e.event, e.data);
        if (![e.data isEqualToString:@"pong"]) {
            NSString *jsonString = e.data;
            eventString = e.event;
            NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
            dataDict = [NSJSONSerialization JSONObjectWithData:data
                                                       options:kNilOptions
                                                         error:nil];
            
            NSLog(@"JSON DICT - %@", dataDict);
            
        }
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            if (settings.authorizationStatus == UNAuthorizationStatusAuthorized) {
                // Notifications allowed
                //                [self scheduleLocalNotifications];
                
                if ([eventString isEqualToString:@"dialog"]) {
                    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
                    
                    //                    UNNotificationAction *snoozeAction = [UNNotificationAction actionWithIdentifier:@"Snooze"
                    //                                                                                              title:@"Snooze" options:UNNotificationActionOptionNone];
                    //                    UNNotificationAction *deleteAction = [UNNotificationAction actionWithIdentifier:@"Delete"
                    //                                                                                              title:@"Delete" options:UNNotificationActionOptionDestructive];
                    //
                    //                    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"UYLReminderCategory"
                    //                                                                                              actions:@[snoozeAction,deleteAction] intentIdentifiers:@[]
                    //                                                                                              options:UNNotificationCategoryOptionNone];
                    //                    NSSet *categories = [NSSet setWithObject:category];
                    
                    //                    [center setNotificationCategories:categories];
                    
                    
                    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
                    content.title = [NSString stringWithFormat:@"id_from: %@", [dataDict valueForKey:@"id_from"]];
                    content.body = [NSString stringWithFormat:@"text: %@", [dataDict valueForKey:@"text"]];
                    content.categoryIdentifier = @"UYLReminderCategory";
                    content.sound = [UNNotificationSound defaultSound];
                    
                    NSString *identifier = @"UYLLocalNotification";
                    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:nil];
                    
                    eventString = @"";
                    
                    [self getDialogLast];
                    
                    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                        if (error != nil) {
                            NSLog(@"Something went wrong: %@",error);
                        }
                    }];
                    
                    //                } else if ([eventString isEqualToString:@""]) {
                    //
                    //                } else if ([eventString isEqualToString:@""]) {
                    
                }
                
                
                
                
                
                
            } else {
                // Notifications not allowed
            }
        }];
    }];
}


//- (void)scheduleLocalNotifications {
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//
//    UNNotificationAction *snoozeAction = [UNNotificationAction actionWithIdentifier:@"Snooze"
//                                                                              title:@"Snooze" options:UNNotificationActionOptionNone];
//    UNNotificationAction *deleteAction = [UNNotificationAction actionWithIdentifier:@"Delete"
//                                                                              title:@"Delete" options:UNNotificationActionOptionDestructive];
//
//    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"UYLReminderCategory"
//                                                                              actions:@[snoozeAction,deleteAction] intentIdentifiers:@[]
//                                                                              options:UNNotificationCategoryOptionNone];
//    NSSet *categories = [NSSet setWithObject:category];
//
//    [center setNotificationCategories:categories];
//
//
//    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
//    content.title = @"Don't forget";
//    content.body = @"Buy some milk";
//    content.categoryIdentifier = @"UYLReminderCategory";
//    content.sound = [UNNotificationSound defaultSound];
//
//    UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:5 repeats:NO];
//
//    NSString *identifier = @"UYLLocalNotification";
//    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger:trigger];
//
//    [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
//        if (error != nil) {
//            NSLog(@"Something went wrong: %@",error);
//        }
//    }];
//}







- (void)getDialogLast {
    NSLog(@"GO TO MESSENGERS");
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodChat params:NULL andURL:dialogLast];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            jsonU = [dataParser parseUsers:[NSJSONSerialization JSONObjectWithData:data
                                                                           options:kNilOptions
                                                                             error:nil]];
            jsonM = [dataParser parseLastMessage:[NSJSONSerialization JSONObjectWithData:data
                                                                                 options:kNilOptions
                                                                                   error:nil]];
            
            NSMutableArray *bigJson = [[NSMutableArray alloc] init];
            
            for (NSArray *object in jsonU) {
                NSLog(@"DDDD");
                
                // - ID USER -------------
                NSNumber *idUser = [object valueForKey:@"idUser"];
                
                // - FIRST + LAST NAME USER ----------------
                NSString *nameUser = [NSString stringWithFormat:@"%@ %@", [object valueForKey:@"firstNameUser"], [object valueForKey:@"lastNameUser"]];
                
                // - AVATAR --------------
                NSString *userAvatar = [object valueForKey:@"pathAvaUser"];
                
                for (NSArray *lastObject in jsonM) {
                    NSNumber *idTO = [lastObject valueForKey:@"idToMessage"];
                    NSNumber *idFrom = [lastObject valueForKey:@"idFromMessage"];
                    
                    if ([[object valueForKey:@"idUser"] isEqual:idTO] || [[object valueForKey:@"idUser"] isEqual:idFrom]) {
                        
                        // - TEXT ---------------
                        NSString *text = [lastObject valueForKey:@"textMessage"];
                        
                        // - DATE ---------------
                        NSDate *date = [lastObject valueForKey:@"dateMessage"];
                        
                        // - NOTIFICATION -------------
                        NSNumber *unread = [lastObject valueForKey:@"countUnread"];
                        
                        NSDictionary *fullArray = @{@"idUser": idUser,
                                                    @"fullNameUser": nameUser,
                                                    @"userAvatar": userAvatar,
                                                    @"lastMessage": text,
                                                    @"date": date,
                                                    @"countUnread": unread};
                        
                        [bigJson addObject:fullArray];
                    }
                }
            }
            
            NSLog(@"BIG JSON: %@", bigJson);
            
            // - COOL SORTED -------------------------------
            NSSortDescriptor *sorter = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
            jsonSorted = [bigJson sortedArrayUsingDescriptors:@[sorter]];
            
        }
        
//        jsonArray = [NSJSONSerialization JSONObjectWithData:data
//                                                    options:kNilOptions
//                                                      error:nil];
//        
//        NSLog(@"JSON ARRAY - %@", jsonArray);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.chatTableView reloadData];
            NSLog(@"otpavivli update table");
        });
    }];
}




#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [jsonSorted count];
}

static NSString *CellIndetifier = @"ChatListCell";
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     NSArray *chat = [jsonSorted objectAtIndex:indexPath.row];
    
    
    GBChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndetifier];
    if (!cell) {
        cell = [[GBChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIndetifier];
    }
    [cell setChat:chat];
    return cell;
}

int indexTable;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    indexTable = indexPath.row;
    
    [  self performSegueWithIdentifier:@"messageSegue1" sender:self];
    
    //    MessageController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Message"];
    //    controller.chat = [self.usersArray objectAtIndex:indexPath.row];
    //
    //    NSLog(@"CHAT - %@", controller.chat);
    //    [self.navigationController pushViewController:controller animated:YES];
    [tableView reloadData];
}




-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqual: @"messageSegue1"] ){
        GBMessageController *cont  = segue.destinationViewController;
        
        NSDictionary *um = [jsonSorted objectAtIndex:indexTable];
        cont.idUserForChat = [um valueForKey:@"idUser"];
        cont.pathAvaForChat = [um valueForKey:@"userAvatar"];
        cont.nameUserForChat = [um valueForKey:@"fullNameUser"];
        NSLog(@"send data to next VC");
        
    }
}



@end



