//
//  GBMessageController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "GBDataParser.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
//#import "UIScrollView+InfiniteScroll.h"
#import "GBServerManager.h"
#import "GBDefines.h"
//#import "ContentView.h"

@interface GBMessageController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>
{
    GBDataParser *dataParser;
    MainDataHolder * dataHolder;
}

@property (weak, nonatomic) IBOutlet UITableView *messageTableView;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;


//@property (strong, nonatomic) NSArray *jsonAllMessages;
@property (weak , nonatomic) NSNumber *idUserForChat;
@property (weak, nonatomic) NSString *pathAvaForChat;
@property (weak, nonatomic) NSString *nameUserForChat;

//-(void) getAllMessanges;
- (void) postMessage;


// send message --------------
- (IBAction)AddButton:(id)sender;





@end


