//
//  GBFriendsGroupCell.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/28/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFriendsGroupCell.h"

@interface GBFriendsGroupCell()
@property (weak, nonatomic) IBOutlet UIImageView *friendsImg;
@property (weak, nonatomic) IBOutlet UILabel *friendsLabel;


@end

@implementation GBFriendsGroupCell

-(void)setFriendsGroupList:(NSArray *)dict {
    
// - AVATAR -----------
    
    NSURL *url = [NSURL URLWithString:[dict valueForKey:@"path_ava"]];
    self.friendsImg.layer.cornerRadius = self.friendsImg.frame.size.width/2;
    self.friendsImg.layer.masksToBounds = YES;
    [self.friendsImg setImageWithURL:url];
    
// - FIRST NAME + LAST NAME --------
    self.friendsLabel.text = [NSString stringWithFormat:@"%@ %@", [dict valueForKey:@"first_name"], [dict valueForKey:@"last_name"]];
    
    
    
    
    
}




//[{"id":1261,"first_name":"Юрій","last_name":"Суворов","id_city":1,"file_ava":3258,"path_ava":"http://helper.darx.net/files/978460a8af5faa99/8d2e0d5a64997b8f/f0a8fdd4c4bdc511/711db1e90ea91f90.jpg","file_cover":0,"path_cover":"","lang":"ru","b_day":1,"b_month":8,"b_year":2017,"status":"","info":"","sex":1,"is_online":false,"time_disconnected":"0001-01-01T00:00:00Z"},
@end
