//
//  GBChatCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBUserModel.h"
#import "GBMessageModel.h"

@interface GBChatCell : UITableViewCell

-(void)setChat:(NSArray *)dict;

@end
