//
//  GBFriendsGroupCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/28/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"

@interface GBFriendsGroupCell : UITableViewCell

-(void)setFriendsGroupList:(NSArray *)dict;

@end
