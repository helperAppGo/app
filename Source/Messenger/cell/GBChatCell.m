//
//  GBChatCell.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBChatCell.h"

@interface GBChatCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picture;
@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *notificationImage;

@end

@implementation GBChatCell

-(void)setChat:(NSArray *)dict {
    
// ---- SHOW NAME USER ----------------------------
    self.nameLabel.text = [dict valueForKey:@"fullNameUser"];
    
// ---- SHOW LAST MESSAGE ----------------------------
    self.messageLabel.text = [dict valueForKey:@"lastMessage"];
    
// ---- SHOW DATE LAST MESSAGE ----------------------------
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *date = [dict valueForKey:@"date"];

    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ;
    
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units fromDate:date toDate:[NSDate date] options:0];
    
    if (components.year > 0) {
        // year ago
        [formatter setDateFormat:@"MM/dd/yyyy"];
        self.timeLabel.text = [formatter stringFromDate:date];
        
    } else if (components.month > 0) {
        // month ago
        [formatter setDateFormat:@"MM/dd/yyyy"];
        self.timeLabel.text = [formatter stringFromDate:date];
        
    } else if (components.weekOfYear > 0) {
        // week of year
        [formatter setDateFormat:@"MM/dd/yyyy"];
        self.timeLabel.text = [formatter stringFromDate:date];
        
    } else if (components.day > 0) {
            // day of week
            [formatter setDateFormat:@"EEEE"];
            NSString *dayDate = [formatter stringFromDate:date];
        
            if ([dayDate isEqualToString:@"Monday"]) {
                self.timeLabel.text = @"Понеділок";
            } else if ([dayDate isEqualToString:@"Tuesday"]) {
                self.timeLabel.text = @"Вівторок";
            } else if ([dayDate isEqualToString:@"Wednesday"]) {
                self.timeLabel.text = @"Середа";
            } else if ([dayDate isEqualToString:@"Thursday"]) {
                self.timeLabel.text = @"Четвер";
            } else if ([dayDate isEqualToString:@"Friday"]) {
                self.timeLabel.text = @"П'ятниця";
            } else if ([dayDate isEqualToString:@"Saturday"]) {
                self.timeLabel.text = @"Субота";
            } else if ([dayDate isEqualToString:@"Sunday"]) {
                self.timeLabel.text = @"Неділя";
            
            } else {
                // yesterday
                self.timeLabel.text = @"Вчора";
            }
        
    } else if(components.hour > 0) {
        // hour
        self.timeLabel.text = [NSString stringWithFormat:@"%ld год.",(long)components.hour];
        
    } else if(components.minute > 0) {
        // minute
        self.timeLabel.text = [NSString stringWithFormat:@"%ld хв.",(long)components.minute];
        
    } else if(components.second > 0) {
        // second
        self.timeLabel.text = [NSString stringWithFormat:@"%ld сек.",(long)components.second];
    } else {
        self.timeLabel.text = @"Цієї миті";
    }
    
    NSLog(@"CHAT CELL Converted String : %@",self.timeLabel.text);
    
    
// ---- SHOW / HIDE NOTIFICATION ----------------------------
    self.notificationImage.layer.cornerRadius = self.notificationImage.frame.size.width/2;
    self.notificationImage.layer.masksToBounds = YES;
    
    if (![[[dict valueForKey:@"countUnread"]stringValue] isEqualToString:@"0"]) {
        self.notificationLabel.text = [[dict valueForKey:@"countUnread"]stringValue];
        self.notificationLabel.hidden = NO;
        self.notificationImage.hidden = NO;
    } else {
    self.notificationLabel.hidden = YES;
    self.notificationImage.hidden = YES;
    }
    
// ---- SHOW IMAGE USER ----------------------------
    NSString *userAvatar = [dict valueForKey:@"userAvatar"];
    self.picture.layer.cornerRadius = self.picture.frame.size.width/2;
    self.picture.layer.masksToBounds = YES;
    
    if ([userAvatar isEqualToString:@""]) {
        self.picture.image = [UIImage imageNamed:@"user_ava"];
    } else {
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:userAvatar]];
        self.picture.image = [UIImage imageWithData:imageData];
    }
}
@end
