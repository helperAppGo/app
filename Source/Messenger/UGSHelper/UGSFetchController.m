//
//  UGSFetchController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "UGSFetchController.h"
//#import "UGSLoginFetch.h"
////#import "UGSFeedFetch.h"
////#import "UGSMessangerFetch.h"
//#import "GBChatController.h"

@implementation UGSFetchController

- (instancetype)initPrivate {
    self = [super init];
    return self;
}

+ (instancetype)sharedInstance {
    static UGSFetchController *uniqueInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        uniqueInstance = [[UGSFetchController alloc] initPrivate];
    });
    
    return uniqueInstance;
}

//- (void) registrationUser:(NSDictionary *)dict {
//    [UGSLoginFetch onRequestUserID:dict];
//    
//}

//- (void)getFeed {
//    [UGSFeedFetch getFeed];
//
//}

//- (void) getLastDialogs {
//    [ChatController getLastDialogs];
//}


@end
