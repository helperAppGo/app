//
//  UGSSuperFetch.m
//  NewMessenger
//
//  Created by Maxim Ohrimenko on 8/19/17.
//  Copyright © 2017 Maxim Ohrimenko. All rights reserved.
//

#import "UGSSuperFetch.h"
#import "UGSFetchController.h"

@implementation UGSSuperFetch

+ (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:completion];
    
    [dataTask resume];
}

+ (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link{
    NSMutableURLRequest *request;
    if (method == postMethod) {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
        }
        
        NSError *err;
        NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:dict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&err];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonDict];
        
        return request;
    } else if (method == getMethodChat){
        NSString *requestString = [NSString stringWithString:link];
        NSArray *k = [dict allKeys];
        NSArray *v = [dict allValues];
        requestString = [requestString stringByAppendingString:@"?data={"];
        for (id key in k) {
            NSString *keyString = [NSString stringWithFormat:@"\"%@\":", key];
            NSString *valueString = [NSString stringWithFormat:@"%@", [v objectAtIndex:[k indexOfObject:key]]];
            keyString = [keyString stringByAppendingString:valueString];
            requestString = [requestString stringByAppendingFormat:@"%@", keyString];
            if (key != k.lastObject)
                requestString = [requestString stringByAppendingString:@","];
        }
        requestString = [requestString stringByAppendingString:@"}"];
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        return request;
    } else if (method == getMethodMessage) {
        NSString *requestString = [NSString stringWithString:link];
        
        NSString *id_user = @"id_user";
        NSString *idUser = @"BLA BLA";
        
        //[NSString stringWithFormat:@"%i", reqUserID];  //
        NSString *time = @"time";
        NSString *kTime = @"null";
        
        NSString *limit = @"limit";
        NSString *kLimit = @"25";
        
        NSString *offset = @"offset";
        NSString *kOffset = @"0";
        //        NSString *kOffset = [NSString stringWithFormat:@"%i",self.tableArray.numberOfSections];
        
        
        requestString = [requestString stringByAppendingString:@"?data={"];
        
        NSString *keyIdUser = [NSString stringWithFormat:@"\"%@\":", id_user];
        requestString = [requestString stringByAppendingString:keyIdUser];
        requestString = [requestString stringByAppendingString:idUser];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyTime = [NSString stringWithFormat:@"\"%@\":", time];
        requestString = [requestString stringByAppendingString:keyTime];
        requestString = [requestString stringByAppendingString:kTime];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyLimit = [NSString stringWithFormat:@"\"%@\":", limit];
        requestString = [requestString stringByAppendingString:keyLimit];
        requestString = [requestString stringByAppendingString:kLimit];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyOffset = [NSString stringWithFormat:@"\"%@\":", offset];
        requestString = [requestString stringByAppendingString:keyOffset];
        requestString = [requestString stringByAppendingString:kOffset];
        
        requestString = [requestString stringByAppendingString:@"}"];
        
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        return request;
    } else if (method == getMethodGroupMOST) {
        NSString *requestString = [NSString stringWithString:link];
        
        NSString *time = @"time";
        NSString *kTime = @"null";
        
        NSString *limit = @"limit";
        NSString *kLimit = @"25";
        
        NSString *offset = @"offset";
//        NSString *kOffset = @"0";
        NSString *kOffset = [dict valueForKey:@"OffSet"];
        //        NSString *kOffset = [NSString stringWithFormat:@"%i",self.tableArray.numberOfSections];
        
        
        requestString = [requestString stringByAppendingString:@"?data={"];
        
        NSString *keyTime = [NSString stringWithFormat:@"\"%@\":", time];
        requestString = [requestString stringByAppendingString:keyTime];
        requestString = [requestString stringByAppendingString:kTime];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyLimit = [NSString stringWithFormat:@"\"%@\":", limit];
        requestString = [requestString stringByAppendingString:keyLimit];
        requestString = [requestString stringByAppendingString:kLimit];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyOffset = [NSString stringWithFormat:@"\"%@\":", offset];
        requestString = [requestString stringByAppendingString:keyOffset];
        requestString = [requestString stringByAppendingString:kOffset];
        
        requestString = [requestString stringByAppendingString:@"}"];
        
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }

        return request;
    } else if (method == getMethodApiGroup) {
        NSString *requestString = [NSString stringWithString:link];
        NSString *zero = @"0";
//        NSString *one = @"1";
        
        if ([[dict valueForKey:@"number"] isEqualToString:zero]) {
            
            NSString *id_group = @"id";
            NSString *idGroup = [dict valueForKey:@"id"];
        
            requestString = [requestString stringByAppendingString:@"?data={"];
        
            NSString *keyIdGroup = [NSString stringWithFormat:@"\"%@\":", id_group];
            requestString = [requestString stringByAppendingString:keyIdGroup];
            requestString = [requestString stringByAppendingString:idGroup];
        
            requestString = [requestString stringByAppendingString:@"}"];
        } else {
            NSString *id_group = @"id_group";
            NSString *idGroup = [dict valueForKey:@"id"];
            
            requestString = [requestString stringByAppendingString:@"?data={"];
            
            NSString *keyIdGroup = [NSString stringWithFormat:@"\"%@\":", id_group];
            requestString = [requestString stringByAppendingString:keyIdGroup];
            requestString = [requestString stringByAppendingString:idGroup];
            
            requestString = [requestString stringByAppendingString:@"}"];
        }
        
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        
        return request;
    } else if (method == getMethodGroupsPostsList) {
        NSString *requestString = [NSString stringWithString:link];
        
        NSString *idGroup = @"id";
        NSString *kIdGroup = [dict valueForKey:@"Id"];
        
        NSString *limit = @"limit";
        NSString *kLimit = @"25";
        
        NSString *offset = @"offset";
        NSString *kOffset = [dict valueForKey:@"OffSet"];
        
        NSString *expand = @"expand";
        NSString *kExpand = @"files";

        
        requestString = [requestString stringByAppendingString:@"?data={"];
        
        NSString *keyIdGroup = [NSString stringWithFormat:@"\"%@\":", idGroup];
        requestString = [requestString stringByAppendingString:keyIdGroup];
        requestString = [requestString stringByAppendingString:kIdGroup];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyLimit = [NSString stringWithFormat:@"\"%@\":", limit];
        requestString = [requestString stringByAppendingString:keyLimit];
        requestString = [requestString stringByAppendingString:kLimit];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyOffset = [NSString stringWithFormat:@"\"%@\":", offset];
        requestString = [requestString stringByAppendingString:keyOffset];
        requestString = [requestString stringByAppendingString:kOffset];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyExpand = [NSString stringWithFormat:@"\"%@\":\"%@\"", expand, kExpand];
        requestString = [requestString stringByAppendingString:keyExpand];
//        requestString = [requestString stringByAppendingString:kExpand];
        
        requestString = [requestString stringByAppendingString:@"}"];
        
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        
        return request;
    } else if (method == getFriendsGroupList) {
        NSString *requestString = [NSString stringWithString:link];
        
        NSString *limit = @"limit";
        NSString *kLimit = @"25";
        
        NSString *offset = @"offset";
        NSString *kOffset = [dict valueForKey:@"OffSet"];
        
        NSString *random = @"random";
        NSString *kRandom = @"0";
        
        
        requestString = [requestString stringByAppendingString:@"?data={"];
        
        NSString *keyLimit = [NSString stringWithFormat:@"\"%@\":", limit];
        requestString = [requestString stringByAppendingString:keyLimit];
        requestString = [requestString stringByAppendingString:kLimit];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyOffset = [NSString stringWithFormat:@"\"%@\":", offset];
        requestString = [requestString stringByAppendingString:keyOffset];
        requestString = [requestString stringByAppendingString:kOffset];
        requestString = [requestString stringByAppendingString:@","];
        
        NSString *keyRandom = [NSString stringWithFormat:@"\"%@\":", random];
        requestString = [requestString stringByAppendingString:keyRandom];
        requestString = [requestString stringByAppendingString:kRandom];
        
        requestString = [requestString stringByAppendingString:@"}"];
        
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        
        return request;

    } else {
        request = [NSMutableURLRequest new];
        
        return request;
        
    }  
}

@end

