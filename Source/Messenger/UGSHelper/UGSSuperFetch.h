//
//  UGSSuperFetch.h
//  NewMessenger
//
//  Created by Maxim Ohrimenko on 8/19/17.
//  Copyright © 2017 Maxim Ohrimenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIOMacro.h"
#import "GBMessageController.h"

@interface UGSSuperFetch : NSObject

typedef NS_ENUM(NSUInteger, method)  {
    postMethod,
    getMethodChat,
    getMethodMessage,
    getMethodGroupMOST,
    getMethodApiGroup,
    getMethodGroupsPostsList,
    getFriendsGroupList,
    deleteMethod,
};

typedef void(^fetchHandleBlock)(NSData *data, NSURLResponse *response, NSError *error);

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

+ (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion;
+ (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link;
@end
