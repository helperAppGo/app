//
//  GBFriendsView.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/23/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFriendsView.h"

#define getFriendsList [BASE_URL stringByAppendingString:@"/api/users/friends/list"]

@interface GBFriendsView ()

@end

@implementation GBFriendsView {
    NSMutableArray *jsonArray;
    NSInteger currentPage;
    NSDictionary *paramDic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currentPage = 0;
    paramDic = [[NSDictionary alloc] init];
    
    [self getDataFriendsList];
    dataParser = [[GBDataParser alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getDataFriendsList {
    NSLog(@"GO TO GROUPS");
    
    paramDic = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%li", (long)currentPage] forKey:@"OffSet"];
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getFriendsGroupList params:paramDic andURL:getFriendsList];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
            
            jsonArray = [array valueForKey:@"response"];
            
            NSLog(@"FRIENDS LIST: %@", jsonArray);
            
//            if (jsonGroup == nil) {
//                jsonGroup = [[newGroup reverseObjectEnumerator] allObjects];
//            } else {
//                [jsonGroup addObjectsFromArray:[[newGroup reverseObjectEnumerator] allObjects]];
//            }
        }
        
        [self.friendsTableView reloadData];
        
//        currentPage += 25;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.allGroupTableView reloadData];
//            NSLog(@"otpavivli update table");
//            
//            [[UIApplication sharedApplication] stopNetworkActivity];
//            
//            if(completion) {
//                completion();
//            }
//        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.friendsTableView reloadData];
            NSLog(@"otpavivli update table");
        });
    }];
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"JSON GROUP COUNT: %lu", (unsigned long)jsonGroup.count);
//    NSLog(@"CURRENT PAGE = %ld", (long)currentPage);
    NSLog(@"JSON FRIENDS COUNT: %lu", (unsigned long)jsonArray.count);
    return [jsonArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    NSArray *allFriends = [jsonArray objectAtIndex:indexPath.row];
    
    static NSString *CellIndetifier = @"FriendsListCell";
    GBFriendsGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndetifier];
    if (!cell) {
        cell = [[GBFriendsGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIndetifier];
    }
    [cell setFriendsGroupList:[jsonArray objectAtIndex:indexPath.row]];
    
    return cell;
}





@end
