//
//  GBSwitchChat.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/23/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBSwitchChat.h"

#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface GBSwitchChat ()

@end

@implementation GBSwitchChat
@synthesize chatSwitch, friendsSwitch;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    self.navigationItem.title = @"Чат";
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    chatSwitch = [storyboard instantiateViewControllerWithIdentifier:@"Chat"];
    friendsSwitch = [storyboard instantiateViewControllerWithIdentifier:@"GBFriendsView"];
    
    [self switchSegmentedControllerChat];
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) switchSegmentedControllerChat {
    [chatSwitch removeFromParentViewController];
    [chatSwitch.view removeFromSuperview];
    [self addChildViewController:chatSwitch];
    [self fullFillToBottomView:chatSwitch.view];
    
    // SWITCHER -----
    self.segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"Діалоги", @"Друзi"]];
    self.segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    self.segmentedControl.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 30);
    self.segmentedControl.backgroundColor = [UIColor whiteColor];
    self.segmentedControl.selectionIndicatorHeight = 2.0f;
    self.segmentedControl.segmentEdgeInset = UIEdgeInsetsMake(0, 10, 0, 10);
    self.segmentedControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedControl.selectionIndicatorColor = Rgb2UIColor(9, 93, 177);
    self.segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor grayColor]};
    self.segmentedControl.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : Rgb2UIColor(9, 93, 177)};
    self.segmentedControl.verticalDividerWidth = 1.0f;
    [self.view addSubview:self.segmentedControl];
//    self.navigationItem.titleView = self.segmentedControl;
    [self.segmentedControl setIndexChangeBlock:^(NSInteger index) {
        if(index == 0){
            NSLog(@"Did press position on switch at index: %lu", (unsigned long)index);
            
//            self.navigationItem.title = @"Діалоги";
            
            [chatSwitch removeFromParentViewController];
            [chatSwitch.view removeFromSuperview];
            [self addChildViewController:chatSwitch];
            [self fullFillToBottomView:chatSwitch.view];
            
        } else {
            NSLog(@"Did press position on switch at index: %lu", (unsigned long)index);
            
//            self.navigationItem.title = @"Друзi";
            
            [friendsSwitch removeFromParentViewController];
            [friendsSwitch.view removeFromSuperview];
            [self addChildViewController:friendsSwitch];
            [self fullFillToBottomView:friendsSwitch.view];
            
//            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
//                                                                           message:@"COMING SOON..."
//                                                                    preferredStyle:UIAlertControllerStyleAlert];
//            
//            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                                              handler:^(UIAlertAction * action) {}];
//            
//            [alert addAction:defaultAction];
//            [self presentViewController:alert animated:YES completion:nil];
            
        }
    }];
}

-(void) fullFillToBottomView:(UIView *)subView {
    [self.switchConteinerView addSubview:subView];
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.switchConteinerView addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.switchConteinerView attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0]];
    [self.switchConteinerView addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.switchConteinerView attribute:NSLayoutAttributeWidth multiplier:1.0 constant:0]];
    [self.switchConteinerView addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.switchConteinerView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [self.switchConteinerView addConstraint:[NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.switchConteinerView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
}



@end
