//
//  GBSwitchChat.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/23/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBChatController.h"
#import "GBFriendsView.h"
#import "HMSegmentedControl.h"

@interface GBSwitchChat : UIViewController <UIAlertViewDelegate>

@property (nonatomic, strong) GBChatController *chatSwitch;
@property (nonatomic, strong) GBFriendsView *friendsSwitch;
@property (nonatomic, strong) HMSegmentedControl *segmentedControl;

@property (weak, nonatomic) IBOutlet UIView *switchConteinerView;

- (void) fullFillToBottomView:(UIView *)subView;

@end
