//
//  GBListMembers.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBServerManager.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBDataParser.h"
#import "UIScrollView+InfiniteScroll.h"
#import "UIApplication+NetworkIndicator.h"
#import "CustomInfiniteIndicator.h"
#import "GBListMembersModel.h"
#import "GBListMembersCell.h"

@interface GBListMembers : UIViewController<UITextViewDelegate, UITableViewDataSource, UIAlertViewDelegate> {
    GBDataParser *dataParser;
}

@property (weak, nonatomic) IBOutlet UITableView *membersTableView;
@property (nonatomic, weak) NSNumber *idGroupForMembers;

-(void) getMembersList:(void(^)(void))completion;
-(void) refresh;

@end
