//
//  GBInGroupViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 26.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBStreamViewController.h"
#import "GBDataParser.h"

@interface GBInGroupViewController : GBStreamViewController{
    GBDataParser *dataParser;
    MainDataHolder *dataHolder;
}

// STATUS BAR VIEW
@property (weak, nonatomic) IBOutlet UIView *statusBarView;

// MAIN STATIC VIEW
@property (weak, nonatomic) IBOutlet UIView *mainStaticView;

// TOP VIEW
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *nameGroup;
@property (weak, nonatomic) IBOutlet UILabel *typeGroup;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeight;

// COUNT VIEW
@property (weak, nonatomic) IBOutlet UIView *countPeopleView;
@property (weak, nonatomic) IBOutlet UILabel *countPeople;
- (IBAction)countPeopleButton:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *countHight;

// ADDRESS VIEW
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UILabel *adress;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addressHight;

// WEB VIEW
@property (weak, nonatomic) IBOutlet UIView *webView;
@property (weak, nonatomic) IBOutlet UILabel *webSite;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *webHight;

// DESCRIPTION VIEW
@property (weak, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionGroup;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHight;

// LIST VIEW
@property (weak, nonatomic) IBOutlet UIView *listView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listHight;


@property (strong,nonatomic) NSNumber* idGroupInWall;
@property (nonatomic, weak) NSNumber *idGroup;

- (void) showMainStaticView;

@end
