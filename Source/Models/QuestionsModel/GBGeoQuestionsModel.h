//  GBGeoQuestionsModel.h
//  GoBeside

#import <Mantle/Mantle.h>

@interface GBGeoQuestionsModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber * gqId;
@property (nonatomic, strong) NSNumber * id_user;
@property (nonatomic, strong) NSNumber * id_category;
@property (nonatomic, strong) NSNumber * count_likes;
@property (nonatomic, strong) NSNumber * count_answers;
@property (nonatomic, strong) NSNumber * status;

@property (nonatomic, strong) NSString * time;
@property (nonatomic, strong) NSString * lon;
@property (nonatomic, strong) NSString * lat;
@property (nonatomic, strong) NSString * distance;
@property (nonatomic, strong) NSString * files;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * address;

+ (NSArray *)getObjectsFromArray:(NSArray *)data;

+ (GBGeoQuestionsModel *)getObjectByDictionary:(NSDictionary *)data;


@end
