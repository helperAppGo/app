//  GBGeoQuestionsModel.m
//  GoBeside

#import "GBGeoQuestionsModel.h"
#import "GBDataFacade.h"

@implementation GBGeoQuestionsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"gqId": @"id",
             @"id_user" : @"id_user",
             @"id_category" : @"id_category",
             @"time" : @"time",
             @"lon" : @"lon",
             @"lat" : @"lat",
             @"distance" : @"distance",
             @"files" : @"files",
             @"text" : @"text",
             @"address" : @"address",
             @"count_likes" : @"count_likes",
             @"count_answers" : @"count_answers",
             @"status" : @"status"
             };
}

#pragma mark - LastQuestions
+ (NSArray *)getObjectsFromArray:(NSArray *)data
{
    if ((id)data == [NSNull null])
    {
        return @[];
    }
    
    NSMutableArray * result = [NSMutableArray array];
    
    for (NSDictionary * dict in data)
    {
        @try
        {
            [result addObject:[GBGeoQuestionsModel getObjectByDictionary:dict]];
        }
        @catch (NSException *exception)
        {
            Error(@"%@", exception);
        }
    }
    return result;
}

+ (GBGeoQuestionsModel *)getObjectByDictionary:(NSDictionary *)data
{
    GBGeoQuestionsModel * lastQuestions = [[GBGeoQuestionsModel alloc] init];
    
    lastQuestions.gqId = data[@"id"];
    lastQuestions.id_user = data[@"id_user"];
    lastQuestions.id_category = data[@"id_category"];
    lastQuestions.time = data[@"time"];
    lastQuestions.lon = data[@"lon"];
    lastQuestions.lat = data[@"lat"];
    lastQuestions.distance = data[@"distance"];
    lastQuestions.files = data[@"files"];
    lastQuestions.text = data[@"text"];
    lastQuestions.address = data[@"address"];
    lastQuestions.count_likes = data[@"count_likes"];
    lastQuestions.count_answers = data[@"count_answers"];
    lastQuestions.status = data[@"status"];
    
    return lastQuestions;
}

@end
