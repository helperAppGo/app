#import <Foundation/Foundation.h>
#import "GBUserModel.h"
#import "GBUser.h"

@interface MainDataHolder : NSObject

@property (nonatomic, strong) GBUserModel * currentUser;

@end
