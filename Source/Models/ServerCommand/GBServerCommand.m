//  GBServerCommand.m
//  GoBeside

#import "GBServerCommand.h"

@implementation GBServerCommand

static NSDictionary *typesDictionary = nil;

+ (void)initialize
{
    [super initialize];
    
    typesDictionary = @{@"categories_subscriptions": @(GBServerCommandTypeSubscribeCategories),
                        @"users_subscriptions": @(GBServerCommandTypeSubscribeUsers)
                        };
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"type" : @"code",
             @"optional" : @"optional",
             @"minCount" : @"count",
             };
}

+ (NSValueTransformer *)typeJSONTransformer
{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:typesDictionary
                                                            defaultValue:@(GBServerCommandTypeUnknown)
                                                     reverseDefaultValue:nil];
}

@end
