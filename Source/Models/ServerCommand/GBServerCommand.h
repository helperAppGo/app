//  GBServerCommand.h
//  GoBeside

#import "GBModel.h"

typedef enum : NSUInteger
{
    GBServerCommandTypeUnknown,
    GBServerCommandTypeSubscribeCategories,
    GBServerCommandTypeSubscribeUsers,
} GBServerCommandType;

@interface GBServerCommand : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign) GBServerCommandType type;
@property (nonatomic, assign) BOOL               optional;
@property (nonatomic, assign) NSUInteger         minCount;

@end
