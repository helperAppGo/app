//// Header

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UserObj : NSObject

@property (nonatomic) NSInteger objId;
@property (nullable, nonatomic, copy) NSString *token;
@property (nullable, nonatomic, copy) NSDate *birthDay;
@property (nonatomic) NSInteger friendsCount;
@property (nonatomic) NSInteger followersCount;
@property (nonatomic) NSInteger postsCount;
@property (nonatomic) NSInteger fileAva;
@property (nonatomic) NSInteger fileCover;
@property (nullable, nonatomic, copy) NSString *firstName;
@property (nonatomic, strong) NSDictionary *dict;
@property (nonatomic) NSInteger cityid;
@property (nullable, nonatomic, copy) NSString *info;
@property (nonatomic) BOOL online;
@property (nullable, nonatomic, copy) NSString *lang;
@property (nullable, nonatomic, copy) NSString *lastName;
@property (nullable, nonatomic, copy) NSString *avatar;
@property (nullable, nonatomic, copy) NSString *cover;
@property (nonatomic, assign) NSInteger relation;
@property (nonatomic) NSInteger sex;
@property (nullable, nonatomic, copy) NSString *status;
@property (nullable, nonatomic, copy) NSDate *disconnectDate;

+(UserObj *)createUser:(NSDictionary *)response;

-(NSString *)nameString;
-(NSString *)birthDayString;
-(NSString *)cityString;
-(NSString *)gender;
-(NSString *)relationString;

-(UIColor *)statusColor;
-(NSString *)statusText;

@end
