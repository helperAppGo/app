//
//  GBPostModel.m
//  GoBeside
//
//  Created by Ruslan Mishin on 15.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBPostModel.h"

@implementation GBPostModel

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"countComments"  : @"count_comments",
                                                                  @"countLikes"     : @"count_likes",
                                                                  @"fileObjects"    : @"file_objects",
                                                                  @"objId"          : @"id",
                                                                  @"userId"         : @"id_user",
                                                                  @"wallId"         : @"id_wall",
                                                                  @"isLiked"        : @"is_liked",
                                                                  @"avatar"         : @"path_ava"
                                                                  }];
}

#pragma mark - Public

- (NSNumber*)ident
{
    return @(self.objId);
}

@end
