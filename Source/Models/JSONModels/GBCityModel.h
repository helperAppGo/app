//
//  GBCityModel.h
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseModel.h"

@interface GBCityModel : GBBaseModel

@property (nonatomic, assign) int64_t objId;
@property (nonatomic, strong) NSString *name;

+ (void) refreshCitiesDataWithCompletion:(GBBoolBlock)aCompletion;

@end
