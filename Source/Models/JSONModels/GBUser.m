//  GBUser.m
//  GoBeside

#import "GBUser.h"
#import "GBServerManager.h"

@implementation GBUser

#pragma mark - Class

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"objId"          : @"id",
                                                                  @"name"           : @"first_name",
                                                                  @"lastName"       : @"last_name",
                                                                  @"avatarFileId"   : @"file_ava",
                                                                  @"avatar"         : @"path_ava",
                                                                  @"birthDay"       : @"b_day",
                                                                  @"birthMonth"     : @"b_month",
                                                                  @"birthYear"      : @"b_year",
                                                                  @"friendsCount"   : @"count_friends",
                                                                  @"postsCount"     : @"count_posts",
                                                                  @"cityId"         : @"id_city",
                                                                  @"isOnline"       : @"is_online",
                                                                  @"coverFileId"    : @"file_cover",
                                                                  @"cover"          : @"path_cover",
                                                                  @"gender"         : @"sex",
                                                                  @"timeDisconnected":@"time_disconnected"
                                                                  }];
}

+ (BOOL) propertyIsOptional:(NSString *)propertyName
{
    return [super propertyIsOptional:propertyName] ||
           [propertyName isEqualToString:@"postsCount"] ||
           [propertyName isEqualToString:@"relation"];
}

+ (GBUser *)userWithNumberId:(NSNumber *)ID from:(NSArray *)users
{
    return [self userWithStringId:ID.description from:users];
}

+ (GBUser *)userWithStringId:(NSString *)ID from:(NSArray *)users
{
    if (ID.integerValue == 0) return nil;
    
    NSArray *filtered = [users filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"objId == %d", ID.integerValue]];
    
    return filtered.firstObject;
}

+ (instancetype)userWithID:(NSNumber *)ID
{
    GBUser *user = [self new];
    user.objId = ID;
    
    return user;
}

//+ (NSString *)parsePhone:(NSString *)phone
//{
//    if (!phone.length)
//    {
//        return phone;
//    }
//
//    NSRange rg = [phone rangeOfString:@"+" options:NSBackwardsSearch];
//
//    if (rg.location == NSNotFound)
//    {
//        return [NSString stringWithFormat:@"+%@",phone];
//    }
//
//    return phone;
//}

+ (void) refreshUserDataWithCompletion:(GBBoolBlock)aCompletion
{
    if ((!SECRET_TOKEN.length) || (USER_ID.longLongValue == 0)) {
        if (aCompletion) aCompletion(NO);
        return;
    }
    CDUser *user = [[SettingsManager instance] currentUser];
    if (!user) {
        return;
    }
    NSDictionary *params= @{@"data":@(user.objId)};
    
    [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            response =response[@"response"];
            [CDUser currentUserUpdate:response];
        }
    }];
    
//     [CDUser currentUserUpdate:response];
//    [[GBServerManager sharedManager] userById:[USER_ID longLongValue] success:^(id responseObject) {
//        NSLog(@"info: %@", responseObject);
//
//        NSDictionary* userInfo = responseObject[@"response"];
//        NSError *error = nil;
//        GBUser * user = [[GBUser alloc] initWithDictionary:userInfo error:&error];
//        CDUser * currentUser = [[SettingsManager instance] currentUser];
//        if (user && currentUser && (currentUser.objId.longLongValue == user.objId.longLongValue)) {
//            if (currentUser.city && (currentUser.cityId == user.cityId)) {
//                user.city = currentUser.city;
//            }
//            if (currentUser.topFriends)
//                user.topFriends = currentUser.topFriends;
//            if (currentUser.topFollowers)
//                user.topFollowers = currentUser.topFollowers;
//        }
//        DLogIf(user == nil, @"Failed to parse user model (2): %@",error);
//        dAssert(user != nil, @"Failed to parse user model (2): %@",error);
//        if (user) {
//            [GBCommonData setUser:user];
//            [user refreshUserTopFriendsWithCompletion:^(BOOL res1) {
//                if (res1)
//                    [GBCommonData setUser:user];
//                [user refreshUserTopFollowersWithCompletion:^(BOOL res2) {
//                    if (res2)
//                        [GBCommonData setUser:user];
//
//                    //[SVProgressHUD showSuccessWithStatus:nil];
//                    if (aCompletion) aCompletion(user != nil);
//                }];
//
//            }];
//        }else{
////            [SVProgressHUD showErrorWithStatus:nil];
//        }
////        if (aCompletion) aCompletion(user != nil);
//
//    } failure:^(NSError *error, NSDictionary *userInfo) {
//        DLog(@"Filed to get user. Error: %@\nuserInfo: %@",error,userInfo);
//        [SVProgressHUD showErrorWithStatus:nil];
//        if (aCompletion) aCompletion(NO);
//    }];
}

+ (void) birthDateComponentsFromDate:(NSDate*)aDate day:(NSInteger*)aDay month:(NSInteger*)aMonth year:(NSInteger*)aYear
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents * components = [calendar components:(NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear) fromDate:aDate];
    if (aDay) *aDay = components.day;
    if (aMonth) *aMonth = components.month;
    if (aYear) *aYear = components.year;
}


#pragma mark - Public

- (NSDate*) birthDate
{
    if ((self.birthDay == 0) || (self.birthMonth < 1) || (self.birthMonth > 12) || (self.birthYear < 1900)) return nil;
    
    NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [NSDateComponents new];
    [components setDay:self.birthDay];
    [components setMonth:self.birthMonth];
    [components setYear:self.birthYear];
    NSDate *date = [calendar dateFromComponents:components];
    if ([date timeIntervalSinceNow] > 0.0) return nil;
    return date;
}

- (void) setBirthDate:(NSDate*)birthDate
{
    if (!birthDate) {
        self.birthDay = 0;
        self.birthMonth = 0;
        self.birthYear = 0;
    }else{
        NSInteger day = 0, month = 0, year = 0;
        [GBUser birthDateComponentsFromDate:birthDate day:&day month:&month year:&year];
        self.birthDay = day;
        self.birthMonth = month;
        self.birthYear = year;
    }
}

- (NSString*)fullName
{
    if (self.name.length && self.lastName.length) {
        return [NSString stringWithFormat:@"%@ %@",self.name,self.lastName];
    }else if (self.name.length) {
        return self.name;
    }else if (self.lastName.length) {
        return self.lastName;
    }else{
        return @"";
    }
}

- (NSNumber*)ident
{
    return self.objId;
}

- (void) refreshUserTopFriendsAndFollowersWithCompletion:(GBBoolBlock)aCompletion
{
    WEAKSELF_DECLARATION;
//    [self refreshUserTopFriendsWithCompletion:^(BOOL res1) {
//        [weakSelf refreshUserTopFollowersWithCompletion:^(BOOL res2) {
//            if (aCompletion) aCompletion(res1 && res2);
//        }];
//    }];
}


#pragma mark - Private

- (void) refreshUserTopFriendsWithCompletion:(GBBoolBlock)aCompletion
{
    WEAKSELF_DECLARATION
    [[GBServerManager sharedManager] friendsForUserId:self.objId.longLongValue limit:6 offset:0 random:0 success:^(id responseObject) {
        STRONGSELF_DECLARATION
        DLog(@"Friends received:\n%@",responseObject);
        NSError *error = nil;
        NSArray *friends = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
        if (error) {
            DLog(@"Failed to parse friends: %@",error);
            if (aCompletion)
                aCompletion(nil);
        }else{
            strongSelf.topFriends = (id)friends;
            if (aCompletion)
                aCompletion(friends);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        if (weakSelf && aCompletion)
            aCompletion(nil);
    }];
}

//- (void) refreshUserTopFollowersWithCompletion:(GBBoolBlock)aCompletion
//{
//    WEAKSELF_DECLARATION
//    [[GBServerManager sharedManager] followersForUserId:CURRENT_USER.objId.longLongValue limit:6 offset:0 random:0 success:^(id responseObject) {
//        STRONGSELF_DECLARATION
//        DLog(@"Followers received:\n%@",responseObject);
//        NSError *error = nil;
//        NSArray *followers = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
//        if (error) {
//            DLog(@"Failed to parse followers: %@",error);
//            if (aCompletion)
//                aCompletion(NO);
//        }else{
//            strongSelf.topFollowers = (id)followers;
//            if (aCompletion)
//                aCompletion(YES);
//        }
//    } failure:^(NSError *error, NSDictionary *userInfo) {
//        if (weakSelf && aCompletion)
//            aCompletion(NO);
//    }];
//}


@end
