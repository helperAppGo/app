//
//  GBBaseModel.h
//  GoBeside
//
//  Created by Ruslan Mishin on 14.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "JSONModel.h"

@interface GBBaseModel : JSONModel

- (NSNumber*)ident;

@end
