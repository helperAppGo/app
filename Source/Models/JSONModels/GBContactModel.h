//
//  GBContactModel.h
//  GoBeside
//
//  Created by Ruslan Mishin on 29.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseModel.h"

@interface GBContactModel : GBBaseModel

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSNumber *phone;

@end
