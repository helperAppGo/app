//  GBUser.h
//  GoBeside

#import "GBBaseModel.h"
#import "GBCityModel.h"

@protocol GBUser;

@interface GBUser : GBBaseModel

@property (nonatomic, strong) NSNumber * objId;

@property (nonatomic, strong) NSString<Optional> * name;
@property (nonatomic, strong) NSString<Optional> * lastName;
@property (nonatomic, assign) int64_t avatarFileId;
@property (nonatomic, strong) NSString<Optional> * avatar;
@property (nonatomic, strong) NSString<Optional> * info;
@property (nonatomic, assign) int64_t cityId;
@property (nonatomic, assign) BOOL isOnline;
@property (nonatomic, strong) NSString<Optional> * lang;
@property (nonatomic, assign) int64_t coverFileId;
@property (nonatomic, strong) NSString<Optional> * cover;
@property (nonatomic, assign) EGender gender;
@property (nonatomic, strong) NSString<Optional> * status;
@property (nonatomic, assign) NSInteger relation;

@property (nonatomic, assign) NSInteger birthDay;
@property (nonatomic, assign) NSInteger birthMonth;
@property (nonatomic, assign) NSInteger birthYear;

@property (nonatomic, assign) int64_t friendsCount;
@property (nonatomic, assign) int64_t postsCount;

@property (nonatomic, strong) NSDate * timeDisconnected;

//attached values - shoulf be copied on user refresh (reload)
@property (nonatomic, strong) GBCityModel<Optional> * city;
@property (nonatomic, strong) NSArray<Optional,GBUser> * topFriends;
@property (nonatomic, strong) NSArray<Optional,GBUser> * topFollowers;


+ (instancetype)userWithID:(NSNumber *)ID;
+ (GBUser *)userWithNumberId:(NSNumber *)ID from:(NSArray *)users;
+ (GBUser *)userWithStringId:(NSString *)ID from:(NSArray *)users;

+ (void) refreshUserDataWithCompletion:(GBBoolBlock)aCompletion;

+ (void) birthDateComponentsFromDate:(NSDate*)aDate day:(NSInteger*)aDay month:(NSInteger*)aMonth year:(NSInteger*)aYear;

//    {
//        response =     {
//            --- "b_day" = 0;
//            --- "b_month" = 0;
//            --- "b_year" = 0;
//            --- "count_friends" = 0;
//            --- "count_posts" = 0;
//            --- "file_ava" = 17402;
//            --- "file_cover" = 0;
//            --- "first_name" = "";
//            --- id = 1738;
//            --- "id_city" = 0;
//            --- info = "";
//            --- "is_online" = 0;
//            --- lang = "";
//            --- "last_name" = "";
//            --- "path_ava" = "http://helper.darx.net/files/613636041e276429/adc0e3610391bb39/15b309ad1f720cab/5d782b486bf7b3f5.png";
//            --- "path_cover" = "";
//            --- sex = 1;
//            --- status = "";
//            "time_disconnected" = "0001-01-01T00:00:00Z";
//        };
//    }

//{
//    "b_day" = 21;
//    "b_month" = 9;
//    "b_year" = 1994;
//    "count_friends" = 0;
//    "file_ava" = 4227;
//    "file_cover" = 0;
//    "first_name" = Yyy;
//    id = 80;
//    "id_city" = 1;
//    info = "";
//    "is_online" = 0;
//    lang = ru;
//    "last_name" = Ooo;
//    "path_ava" = "http://helper.darx.net/files/1196cf3d157a58e0/095a7b4c5dadfd7a/7ed93cc82917ef74/9bdb924efa487cc6.jpg";
//    "path_cover" = "";
//    relation = 1;
//    sex = 1;
//    status = "";
//    "time_disconnected" = "0001-01-01T00:00:00Z";
//}


- (NSDate*) birthDate;
- (void) setBirthDate:(NSDate*)birthDate;

- (NSString*)fullName;
- (void) refreshUserTopFriendsAndFollowersWithCompletion:(GBBoolBlock)aCompletion;

@end
