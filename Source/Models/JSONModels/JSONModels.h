//
//  JSONModels.h
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#ifndef JSONModels_h
#define JSONModels_h

#import "GBCityModel.h"
#import "GBUser.h"
#import "GBFileModel.h"
#import "GBCommentModel.h"
#import "GBPostModel.h"
#import "GBContactModel.h"

#endif /* JSONModels_h */
