//
//  JSONValueTransformer+CustomTransformers.m
//  GoBeside
//
//  Created by Ruslan Mishin on 18.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "JSONValueTransformer+CustomTransformers.h"

static NSString *apiDateFormat1 = @"yyyy-MM-dd'T'HH:mm:ssXXXXX";
static NSString *apiDateFormat2 = @"yyyy-MM-dd'T'HH:mm:ss.SSSSXXXXX";
static NSDateFormatter * jsonValueTransformer_dateFormatter1 = nil;
static NSDateFormatter * jsonValueTransformer_dateFormatter2 = nil;

@implementation JSONValueTransformer (CustomTransformers)



- (NSDate *)NSDateFromNSString:(NSString *)string
{
    if (!string.length) return nil;
    
    if (!jsonValueTransformer_dateFormatter1) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateFormat = apiDateFormat1;
        jsonValueTransformer_dateFormatter1 = formatter;
    }
    NSDate *res = [jsonValueTransformer_dateFormatter1 dateFromString:string];
    if (res) return res;
    
    if (!jsonValueTransformer_dateFormatter2) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateFormat = apiDateFormat2;
        jsonValueTransformer_dateFormatter2 = formatter;
    }
    res = [jsonValueTransformer_dateFormatter2 dateFromString:string];
    
    return res;
}

- (NSString *)JSONObjectFromNSDate:(NSDate *)date
{
    if (!jsonValueTransformer_dateFormatter1) {
        NSDateFormatter *formatter = [NSDateFormatter new];
        formatter.dateFormat = apiDateFormat1;
        jsonValueTransformer_dateFormatter1 = formatter;
    }
    return [jsonValueTransformer_dateFormatter1 stringFromDate:date];
}

@end
