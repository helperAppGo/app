//
//  GBBaseModel.m
//  GoBeside
//
//  Created by Ruslan Mishin on 14.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseModel.h"

@implementation GBBaseModel

- (NSNumber*)ident
{
    dAssert(NO,@"method \"ident\" should be overridden in subclasses");
    return nil;
}

@end
