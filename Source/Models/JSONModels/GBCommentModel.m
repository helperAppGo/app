//
//  GBCommentModel.m
//  GoBeside
//
//  Created by Ruslan Mishin on 18.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCommentModel.h"

@implementation GBCommentModel

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"countLikes"     : @"count_likes",
                                                                  @"fileObjects"    : @"file_objects",
                                                                  @"objId"          : @"id",
                                                                  @"postId"         : @"id_post",
                                                                  @"questionId"     : @"id_question",
                                                                  @"userId"         : @"id_user",
                                                                  @"isLiked"        : @"is_liked",
                                                                  @"avatar"         : @"path_ava"
                                                                  }];
}

+ (BOOL) propertyIsOptional:(NSString *)propertyName
{
    return ([super propertyIsOptional:propertyName] ||
            [propertyName isEqualToString:@"postId"] ||
            [propertyName isEqualToString:@"questionId"]);
}

#pragma mark - Public

- (NSNumber*)ident
{
    return @(self.objId);
}

@end
