//
//  GBCityModel.m
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCityModel.h"
#import "GBServerManager.h"

@implementation GBCityModel

#pragma mark - Class

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"objId"          : @"id"
                                                                  }];
}

+ (void) refreshCitiesDataWithCompletion:(GBBoolBlock)aCompletion
{
    [[GBServerManager sharedManager] citiesSuccess:^(id responseObject) {
        id response = responseObject[@"response"];
        if ([response isKindOfClass:[NSArray class]] && [(NSArray*)response count]) {
            NSError *parseError = nil;
            NSArray *cities = [GBCityModel arrayOfModelsFromDictionaries:(NSArray*)response error:&parseError];
            if (cities)
                CITIES = cities;
            if (aCompletion) aCompletion(cities != nil);
        }else{
            if (aCompletion) aCompletion(NO);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        if (aCompletion) aCompletion(NO);
    }];
}

#pragma mark - Public

- (NSNumber*)ident
{
    return @(self.objId);
}

@end
