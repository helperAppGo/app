//
//  GBFileModel.m
//  GoBeside
//
//  Created by Ruslan Mishin on 15.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBFileModel.h"

@implementation GBFileModel

#pragma mark - JSONModel

+ (JSONKeyMapper *)keyMapper
{
    return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{
                                                                  @"objId"          : @"id",
                                                                  @"sType"          : @"type"
                                                                  }];
}

+ (BOOL) propertyIsIgnored:(NSString *)propertyName
{
    return [super propertyIsIgnored:propertyName] ||
           [propertyName isEqualToString:@"eType"];
}


#pragma mark - Custom Setters

- (void) setSType:(NSString<Optional> *)sType
{
    _sType = sType;
    if ([sType isEqualToString:@"youtube"]) {
        _eType = EAttachmentTypeYoutube;
    }else if ([sType isEqualToString:@"image"]) {
        _eType = EAttachmentTypeImage;
    }else if ([sType isEqualToString:@"video"]) {
        _eType = EAttachmentTypeVideo;
    }else{
        _eType = EAttachmentTypeUnknown;
    }
}


#pragma mark - Public

- (NSNumber*)ident
{
    return @(self.objId);
}

@end
