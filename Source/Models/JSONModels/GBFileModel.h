//
//  GBFileModel.h
//  GoBeside
//
//  Created by Ruslan Mishin on 15.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseModel.h"

@protocol GBFileModel;

@interface GBFileModel : GBBaseModel

@property (nonatomic,assign) int64_t objId;
@property (nonatomic,assign) NSInteger width;
@property (nonatomic,assign) NSInteger height;
@property (nonatomic,strong) NSString<Optional> * path;
@property (nonatomic,strong) NSString<Optional> * preview;
@property (nonatomic,strong) NSString<Optional> * sType;

@property (nonatomic,assign,readonly) EAttachmentType eType;


@end
