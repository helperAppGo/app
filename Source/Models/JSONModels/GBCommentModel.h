//
//  GBCommentModel.h
//  GoBeside
//
//  Created by Ruslan Mishin on 18.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseModel.h"

@interface GBCommentModel : GBBaseModel

@property (nonatomic,assign) int64_t countLikes;
@property (nonatomic,strong) NSArray<GBFileModel,Optional> *fileObjects;
@property (nonatomic,strong) NSString<Optional> *files; //CSV-identifiers
@property (nonatomic,assign) int64_t objId;
@property (nonatomic,assign) int64_t postId;
@property (nonatomic,assign) int64_t questionId;
@property (nonatomic,assign) int64_t userId;
@property (nonatomic,assign) BOOL isLiked;
@property (nonatomic,strong) NSString<Optional> *name;
@property (nonatomic,strong) NSString<Optional> *avatar;
@property (nonatomic,strong) NSString<Optional> *text;
@property (nonatomic,strong) NSDate<Optional> *time;

@end
