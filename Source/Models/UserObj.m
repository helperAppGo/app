//// Header

#import "UserObj.h"
#import "UIFont+Additions.h"
#import "CDCity+CoreDataClass.h"
#import "NSManagedObject+ActiveRecord.h"
#import "Globals.h"

@implementation UserObj


+(UserObj *)createUser:(NSDictionary *)response{
    
    UserObj *user = [[UserObj alloc] init];
    
    NSInteger day = [NULL_TO_NIL(response[@"b_day"]) integerValue];
    NSInteger month = [NULL_TO_NIL(response[@"b_month"]) integerValue];
    NSInteger year = [NULL_TO_NIL(response[@"b_year"]) integerValue];
    NSString *dateString = [NSString stringWithFormat:@"%02ld.%02ld.%ld",(long)day,(long)month,(long)year];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    user.birthDay = [formatter dateFromString:dateString];
    user.objId = [NULL_TO_NIL(response[@"id"]) integerValue];
    user.followersCount = [NULL_TO_NIL(response[@"count_followers"]) integerValue];
    user.friendsCount = [NULL_TO_NIL(response[@"count_friends"]) integerValue];
    user.postsCount = [NULL_TO_NIL(response[@"count_posts"]) integerValue];
    
    user.fileAva = [NULL_TO_NIL(response[@"file_ava"]) integerValue];
    user.fileCover = [NULL_TO_NIL(response[@"file_cover"]) integerValue];
    
    user.firstName = NULL_TO_NIL(response[@"first_name"]);
    user.lastName = NULL_TO_NIL(response[@"last_name"]);
    
    user.cityid = [NULL_TO_NIL(response[@"id_city"]) integerValue];
    user.info = NULL_TO_NIL(response[@"info"]);
    user.online = [NULL_TO_NIL(response[@"is_online"]) integerValue] == 1;
    user.lang = NULL_TO_NIL(response[@"lang"]);
    user.cover = NULL_TO_NIL(response[@"path_cover"]);
    user.sex = [NULL_TO_NIL(response[@"sex"]) integerValue];
    user.status = NULL_TO_NIL(response[@"status"]);
    user.avatar = NULL_TO_NIL(response[@"path_ava"]);
    user.relation = [NULL_TO_NIL(response[@"relation"]) integerValue];
    user.dict = response;
    return user;
}

-(NSString *)birthDayString{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MM.yyyy";
    return [formatter stringFromDate:self.birthDay];
}

-(NSString *)nameString{
    return [NSString stringWithFormat:@"%@ %@", self.firstName, self.lastName];
}

-(NSString *)cityString{
    CDCity *city = [CDCity find:@{@"objId":@(self.cityid)}];
    return city.name;;
}

-(NSString *)relationString{
    /*
     0 = post: api/users/friends?data={"id_user": 1}
     это означает что при нажатии мы можем подать заявку в друзья
     
     1 = delete: api/users/friends?data={"id_user": 1}
     это означает что пользователь наш друг и при нажатии мы можем удалить его из друзей
     
     2 = delete: api/users/friends?data={"id_user": 1}
     это означает что заявка отправлена текущим пользователем при нажатии мы можем отменить запрос
     
     3 = post: api/users/friends?data={"id_user": 1}
     это означает что заявка подана текущему пользователю и при нажатии мы можем принять ее
     
     4 = post: api/users/friends?data={"id_user": 1}
     это означает что другой пользователь подписан на текущего и при нажатии он добавит его в друзья
     
     5 = delete: api/users/friends?data={"id_user": 1}
     это означает что текущий пользователь подписан на другого и при нажатии мы можем отписаться
     */
    NSArray *items = @[@"Добавить в друзья",
                       @"Ваш друг",
                       @"Заявка отправлена",
                       @"Принять заявку",
                       @"Ваш подписчик",
                       @"Вы подписаны"];
    NSInteger langId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"] integerValue];
    if (langId == 1) {
        items = @[@"Додати в друзі",
                  @"Ваш друг",
                  @"Заявку відправлено",
                  @"Прийняти заявку",
                  @"Ваш підписник",
                  @"Ви підписані"];
        
    }
    if (langId == 3) {
        items  = @[@"Add Friend",
                   @"Your friend",
                   @"Request sent",
                   @"Accept request",
                   @"Your follower",
                   @"You are following"];
        
    }
    if (self.relation > 5) {
        return @"Заблокирован»";
    }
    return items[self.relation];
}

-(NSString *)gender{
    return self.sex == 0 ? @"Женский":@"Мужской";
}

-(UIColor *)statusColor{
    return self.online ? appGreenColor : [UIColor grayColor];
}
-(NSString *)statusText{
    return self.online ? @"Онлайн" : @"Оффлайн";
}

@end
