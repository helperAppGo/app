//// Header

#import <Foundation/Foundation.h>

/*
 height = 600;
 id = 11234;
 path = "http://helper.darx.net/files/a99dd89eb644b19d/7d0163fa98c5aa70/1acd157759bb6af6/db5e98a57666d656.jpg";
 preview = "";
 type = image;
 width = 480;
 */

@interface PhotoObj : NSObject

@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) float height;
@property (nonatomic, assign) float width;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *preview;
@property (nonatomic, strong) NSString *typeString;

-(id)initWithDict:(NSDictionary *)dict;


           
@end
