//  GBResponseData.m
//  GoBeside

#import "GBResponseData.h"

@implementation GBResponseData

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"usersSubscriptions" : @"users_subscriptions",
             @"" : @"",
             };
}

@end
