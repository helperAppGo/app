//  GBResponse.h
//  GoBeside

#import <Mantle/Mantle.h>
#import "GBResponseData.h"

@interface GBResponse : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) GBResponseData *data;
@property (nonatomic, strong) NSArray       *errors;

@end
