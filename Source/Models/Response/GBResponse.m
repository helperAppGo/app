//  GBResponse.m
//  GoBeside

#import "GBResponse.h"

@implementation GBResponse

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"data" : @"data",
             @"errors" : @"errors",
             };
}

@end
