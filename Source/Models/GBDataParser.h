//  GBDataParser.h
//  GoBeside

#import <Foundation/Foundation.h>
#import "MainDataHolder.h"
#import "GBUserModel.h"
#import "GBMessageModel.h"
#import "GBAllGroupModel.h"
#import "GBWallModel.h"
#import "GBListMembersModel.h"

@interface GBDataParser : NSObject
{
    __weak MainDataHolder * dataHolder;
}

- (instancetype)initWithDataHolder:(MainDataHolder *)holder;

- (GBUserModel *)parseUser:(NSDictionary *)data;
- (NSArray *)parseUsers:(NSDictionary *)data;

- (NSArray *)parseLastMessage:(NSDictionary *)data;
- (NSArray *)parseMessages:(NSDictionary *)data;

- (NSArray *)parseGroupMost:(NSDictionary *)data;
- (NSArray *)parseGroupGeo:(NSDictionary *)data;

- (NSArray *)parseApiGroup:(NSDictionary *)data;

- (NSArray *)parseGroupPostList:(NSDictionary *)data;

- (NSArray *)parseListMembers:(NSDictionary *)data;

- (NSArray *)parseLastQuestions:(NSDictionary *)data;
- (NSArray *)parseGeoQuestions:(NSDictionary *)data;

@end
