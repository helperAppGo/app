//
//  GBDataHelper.m
//  GoBeside
//
//  Created by Ruslan Mishin on 18.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBDataHelper.h"
#import "GBServerManager.h"

@implementation GBDataHelper

+ (void) likePost:(NSInteger)postId
{
    if(postId>0){
        [[GBServerManager sharedManager] likeWallPostId:postId success:^(id responseObject) {
            DLog(@"like post %ld succes", (long)postId );
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"fail like post %ld", (long)postId );
        }];
    }else{
        [[GBServerManager sharedManager] likeGroupPostId:postId success:^(id responseObject) {
            DLog(@"like post %ld succes", (long)postId );
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"fail like post %ld", (long)postId );
        } ];
    }
}

+ (void) complainPost:(int64_t)postId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] complainPostId:postId success:^(id responseObject) {
        DLog(@"Success to complain post:\n%@",responseObject);
        [SVProgressHUD showSuccessWithStatus:nil];
        if (completion) completion();
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to complain post: %@\n%@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (failure) failure(error);
    }];
}

+ (void) complainQuestion:(int64_t)questionId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] complainQuestionId:questionId success:^(id responseObject) {
        DLog(@"Success to complain question:\n%@",responseObject);
        [SVProgressHUD showSuccessWithStatus:nil];
        if (completion) completion();
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to complain question: %@\n%@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (failure) failure(error);
    }];
}

+ (void) deletePost:(int64_t)postId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] deleteWallPostId:postId success:^(id responseObject) {
        DLog(@"Success to delete post:\n%@",responseObject);
        [SVProgressHUD showSuccessWithStatus:nil];
        if (completion) completion();
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to delete post: %@\n%@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (failure) failure(error);
    }];
}

#pragma mark - User

+ (void) banUser:(int64_t)userId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] banFriendUserId:userId success:^(id responseObject) {
        DLog(@"Success to ban user:\n%@",responseObject);
        [SVProgressHUD showSuccessWithStatus:nil];
        if (completion) completion();
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to ban user: %@\n%@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (failure) failure(error);
    }];
}

+ (void) getUserById:(int64_t)aUserId andCompletion:(GBValueBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] userById:aUserId success:^(id responseObject) {
        DLog(@"Success to get user:\n%@",responseObject);
        
        NSDictionary* userInfo = responseObject[@"response"];
        NSError *error = nil;
        GBUser * user = [[GBUser alloc] initWithDictionary:userInfo error:&error];
        DLogIf(user == nil, @"Failed to parse user model: %@",error);
        if (user) {
            [SVProgressHUD showSuccessWithStatus:nil];
            if (completion) completion(user);
        }else{
            [SVProgressHUD showErrorWithStatus:nil];
            if (failure) failure(error);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to get user: %@\n%@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (failure) failure(error);
    }];
}

#pragma mark - Contacts

+ (void) sendContacts:(NSArray*)aContacts completion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    NSArray *contacts = [JSONModel arrayOfDictionariesFromModels:aContacts];
    
    [[GBServerManager sharedManager] sendContacts:contacts success:^(id responseObject) {
        
        DLog(@"Send Contacts response:\n%@",responseObject);
        NSError *error = nil;
        NSArray *users = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
        if (error) {
            DLog(@"Failed to parse users(contacts): %@",error);
            if (failure)
                failure(error);
        }else{
            if (completion)
                completion(users);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        if (failure) failure(error);
    }];
}

@end
