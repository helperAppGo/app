//
//  GBDataHelper.h
//  GoBeside
//
//  Created by Ruslan Mishin on 18.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBDataHelper : NSObject

+ (void) likePost:(NSInteger)postId;
+ (void) complainPost:(int64_t)postId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure;
+ (void) complainQuestion:(int64_t)questionId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure;
+ (void) deletePost:(int64_t)postId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure;

+ (void) banUser:(int64_t)userId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure;
+ (void) getUserById:(int64_t)aUserId andCompletion:(GBValueBlock)completion failure:(GBErrorBlock)failure;

+ (void) sendContacts:(NSArray*)aContacts completion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;

@end
