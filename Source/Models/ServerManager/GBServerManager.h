//
//  GBServerManager.h
//  GoBeside
//
//  Created by Som Sam on 20.08.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "AFNetworking.h"

typedef void (^GBServerManagerSuccessHandler)(id responseObject);
typedef void (^GBServerManagerFailureHandler)(NSError *error, NSDictionary *userInfo);
typedef void (^GBServerManagerConstructingBodyHandler)(id<AFMultipartFormData> formData);

@interface GBServerManager : NSObject

+ (GBServerManager *)sharedManager;

#pragma mark - Registration
- (void)registrationWithPhone:(NSInteger)phoneInt success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)registrationGetCodeForUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)registrationSendCode:(int)code userId:(NSUInteger)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)registerUserPersonalInfo:(NSString*)firstName sureName:(NSString*)sureName city:(NSUInteger)cityId sex:(NSUInteger)sex success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Users
- (void)userById:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void) updateUserWithFirstName:(NSString*)firstName lastName:(NSString*)lastName avatarId:(int64_t)avatarId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)usersList:(NSArray *)usersArray success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendUserLocationLat:(double)lat lon:(double)lon success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)postForUserId:(int)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)postsForUserId:(int)userId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendPostToUserWallId:(int)wallId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)editWallPostId:(int)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendCommentToWallPostId:(int64_t)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)commentsWallPostId:(int)postId  time:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)likeWallPostId:(NSInteger)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)changeUserInfoStatus:(int)status city:(int)cityId bDay:(int)bDay bMonth:(int)bMonth bYear:(int)bYear success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)updateUserBirthdayWithDay:(NSInteger)aDay month:(NSInteger)aMonth year:(NSInteger)aYear success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)updateUserCityWithId:(int64_t)aCityId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - User Friends
- (void)friendsForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset random:(int)random success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)friendsRequestForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)outgoingFriendsRequestForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)followersForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset random:(int)random success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)followingUsersForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendFriendRequestToUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)deleteFriendUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)banFriendUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)feedTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)searchUserFistName:(NSString *)firstName lastName:(NSString *)lastName  bDay:(int)bDay bMonth:(int)bMonth bYear:(int)bYear success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)notificationLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Questions
- (void)questionById:(int)questionId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)questionsWithLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)questionsGeoTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)questionsMyTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)postQuestion:(NSString *)text category:(int)categoryId lat:(double)lat lon:(double)lon files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)editQuestionId:(int)questionId text:(NSString *)text category:(int)categoryId lat:(double)lat lon:(double)lon files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)deleteQuestionId:(int)questionId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)questionsCategoriesSuccess:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)complainQuestionId:(int64_t)questionId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Answer
- (void)answersForQuestionId:(NSInteger)questionId limit:(NSInteger)limit offset:(NSInteger)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)postAnswerForQuestionId:(int64_t)questionId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)likeAnswerId:(int64_t)answerId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendMessage:(NSString *)text toUserId:(int)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Groups
- (void)groupsMostTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupsGeoTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupById:(int)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupPostsById:(int)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupsCategoriesSuccess:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupsMyTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupsMeAdminTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)createGroup:(NSString *)name description:(NSString *)description fileCover:(int)fileCover type:(int)type lat:(double)lat lon:(double)lon webSite:(NSString *)webSite success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)editGroupId:(int)groupId name:(NSString *)name description:(NSString *)description fileCover:(int)fileCover type:(int)type lat:(double)lat lon:(double)lon webSite:(NSString *)webSite success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)membersGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)membersRequestsGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)membersBannedGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)banUserGroupId:(int)groupId userId:(int)userId ban:(BOOL)needBan success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)joinGroupId:(int64_t)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)unfollowGroupId:(int64_t)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)groupsSearch:(NSString *)text limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)adsForGroupId:(int64_t)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)createAdForGropId:(int)groupId text:(NSString *)text cities:(NSArray *)citiesIdsArray minYears:(int)minYears maxYears:(int)maxYears success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Group Posts
- (void)groupPostId:(int)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)postsGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendPostToGroupId:(int)groupId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)editGroupPostId:(int)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)deleteGroupPostId:(int)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)commentsGroupPostId:(int)postId time:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset  success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendCommentToPostId:(int64_t)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)likeGroupPostId:(NSInteger)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

- (void)likePostCommenId:(int64_t)commentId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)getCommentsPostId:(NSInteger)postId limit:(NSInteger)limit offset:(NSUInteger)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Posts
- (void) complainPostId:(int64_t)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void) sendPostToWallId:(NSNumber*)wallId text:(NSString*)text files:(NSString*)files link:(NSString*)link success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void) deleteWallPostId:(int64_t)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void) postsForWallId:(NSNumber*)wallId  time:(NSTimeInterval)timeInterval limit:(NSInteger)limit offset:(NSUInteger)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Dialogs
- (void)dialogsLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)dialogsMessagesLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)markReadMessageId:(int)messageId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Files
- (void)uploadFile:(NSData*)fileData withName:(NSString*)fileName success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)getFileList:(NSArray *)filesIds success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)addPhoto:(int)fileId toGallery:(int)wallId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Info
- (void)citiesSuccess:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendQrCode:(NSString *)qrToken success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;
- (void)sendContacts:(NSArray*)contacts success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

#pragma mark - Geo
- (void) sendLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure;

@end
