//
//  GBServerManager.m
//  GoBeside
//
//  Created by Som Sam on 20.08.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBServerManager.h"
#import "GBCommonData.h"
#import "AFNetworking.h"
#import "AFJSONRequestSerializer-GB.h"
#import "AFHTTPRequestSerializer-GB.h"

typedef NS_ENUM(NSInteger, RequestManagerResultType) {
    RequestManagerResultTypeJSON,
    RequestManagerResultTypeString
};

typedef NS_ENUM(NSInteger, RequestType) {
    RequestTypePOST,
    RequestTypeMultypartPOST,
    RequestTypeGET,
    RequestTypePUT,
    RequestTypeDELETE,
    RequestTypePATCH,
};

@implementation GBServerManager

+ (GBServerManager *)sharedManager {
    static GBServerManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [self new];
    });
    return sharedManager;
}

#pragma mark - Registration

- (void)registrationWithPhone:(NSInteger)phoneInt success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    DLog(@"%ld", (long)phoneInt);
    NSMutableDictionary *parameters = [self parametersBasic];
    [parameters setObject:@(phoneInt) forKey:@"phone"];
    if (kProductionVersion) {
        [self sendPOSTRequest:@"reg" parameters:parameters success:success failure:failure];
    } else{
        [self sendPOSTRequest:@"registration" parameters:parameters success:success failure:failure];
    }
    
}

- (void)registrationGetCodeForUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"phone":@(userId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        if (kProductionVersion) {
            [self sendGETRequest:@"reg/code" parameters:parameters success:success failure:failure];
        } else{
            [self sendGETRequest:@"registration/code" parameters:parameters success:success failure:failure];
        }
    } else {
        [self jsonFailure:failure];
    }
}

- (void)registrationSendCode:(int)code userId:(NSUInteger)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"phone_code"] = @(code);
    parameters[@"phone"] = @(userId);
    
    [self sendPOSTRequest:@"registration/code" parameters:parameters success:success failure:failure];
}
- (void)registerUserPersonalInfo:(NSString*)firstName sureName:(NSString*)sureName city:(NSUInteger)cityId sex:(NSUInteger)sex success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    
    [parameters setObject:firstName forKey:@"first_name"];
    [parameters setObject:sureName  forKey:@"last_name"];
    [parameters setObject:@(cityId) forKey:@"id_city"];
    [parameters setObject:@(sex) forKey:@"sex"];
    
    [self sendPATCHRequest:@"users/self" parameters:parameters success:success failure:failure];
}

#pragma mark - Users
- (void)userById:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
//    NSMutableDictionary *parameters = [self parametersBasic];
//    [self sendGETRequest:[NSString stringWithFormat:@"users?%d", userId] parameters:parameters success:success failure:failure];
    
    NSString* dataString = [NSString stringWithFormat:@"%lld", userId];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void) updateUserWithFirstName:(NSString*)firstName lastName:(NSString*)lastName avatarId:(int64_t)avatarId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"first_name"] = AS_STRING(firstName);
    parameters[@"last_name"] = AS_STRING(lastName);
    parameters[@"file_ava"] =@(avatarId);
    
    [self sendPUTRequest:@"users" parameters:parameters success:success failure:failure];
}

- (void)usersList:(NSArray *)usersArray success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:usersArray];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)sendUserLocationLat:(double)lat lon:(double)lon success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"lat"] = @(lat);
    parameters[@"lon"] = @(lon);
    
    [self sendPUTRequest:@"users/geo" parameters:parameters success:success failure:failure];
}


- (void)postForUserId:(int)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id":@(userId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/posts" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)postsForUserId:(int)userId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/posts/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)sendPostToUserWallId:(int)wallId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_wall"] = @(wallId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPOSTRequest:@"users/posts" parameters:parameters success:success failure:failure];
}

- (void)editWallPostId:(int)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id"] = @(postId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPUTRequest:@"users/posts" parameters:parameters success:success failure:failure];
}

- (void)sendCommentToWallPostId:(int64_t)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_post"] = @(postId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPOSTRequest:@"users/posts/comments" parameters:parameters success:success failure:failure];
}

- (void)commentsWallPostId:(int)postId  time:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_post":@(postId), /*@"time":@((NSUInteger)timeInterval), */@"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/posts/comments/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)likeWallPostId:(NSInteger)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_post"] = @(postId);
    
    [self sendPOSTRequest:@"users/posts/likes" parameters:parameters success:success failure:failure];
}

//method has stupid syntaxis
- (void)changeUserInfoStatus:(int)status city:(int)cityId bDay:(int)bDay bMonth:(int)bMonth bYear:(int)bYear success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];

    if (status != 0)
        parameters[@"status"] = @(status);
    if (status != 0)
        parameters[@"id_city"] = @(cityId);
    if (status != 0)
        parameters[@"b_day"] = @(bDay);
    if (status != 0)
        parameters[@"b_month"] = @(bMonth);
    if (status != 0)
        parameters[@"b_year"] = @(bYear);
    
    [self sendPATCHRequest:@"users/self" parameters:parameters success:success failure:failure];
}

- (void)updateUserBirthdayWithDay:(NSInteger)aDay month:(NSInteger)aMonth year:(NSInteger)aYear success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    if ((aDay == 0) || (aMonth == 0) || (aYear == 0)) {
        if (failure) failure(nil,nil);
        return;
    }
    
    NSMutableDictionary *parameters = [self parametersBasic];
    
    parameters[@"b_day"] = @(aDay);
    parameters[@"b_month"] = @(aMonth);
    parameters[@"b_year"] = @(aYear);
    
    [self sendPATCHRequest:@"users/self" parameters:parameters success:success failure:failure];
}

- (void) updateUserCityWithId:(int64_t)aCityId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_city"] = @(aCityId);
    
    [self sendPATCHRequest:@"users/self" parameters:parameters success:success failure:failure];
}


#pragma mark - User Friends

- (void)friendsForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset random:(int)random success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId), @"limit":@(limit), @"offset":@(offset), @"random":@(random)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/friends/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)friendsRequestForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/friends/requested" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)outgoingFriendsRequestForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/friends/requesting" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)followersForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset random:(int)random success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId), @"limit":@(limit), @"offset":@(offset), @"random":@(random)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/friends/followers" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)followingUsersForUserId:(int64_t)userId limit:(int64_t)limit offset:(int64_t)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/friends/following" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)sendFriendRequestToUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_user"] = @(userId);
    
    [self sendPOSTRequest:@"users/friends" parameters:parameters success:success failure:failure];
}

- (void)deleteFriendUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendDELETERequest:@"users/friends" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)banFriendUserId:(int64_t)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_user":@(userId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendPOSTRequest:@"users/friends/ban" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)feedTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset), @"expand":@"files"}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/feed" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)searchUserFistName:(NSString *)firstName lastName:(NSString *)lastName  bDay:(int)bDay bMonth:(int)bMonth bYear:(int)bYear success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"first_name":firstName, @"last_name":lastName, @"b_day":@(bDay), @"b_month":@(bMonth), @"b_year":@(bYear)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/search" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)notificationLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"notifications/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}


#pragma mark - Questions
- (void)questionById:(int)questionId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id":@(questionId)}]; // [NSString stringWithFormat:@"%d", questionId]; //
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"questions" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)questionsWithLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSTimeInterval ti = [[NSDate date] timeIntervalSince1970];
    NSString* dataString = [self jsonStringForObject:@{@"time":@(ceil(ti)),@"limit":@(limit), @"offset":@(offset), @"expand":@"files"}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"questions/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)questionsGeoTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset), @"expand":@"files"}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"questions/geo" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)questionsMyTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"questions/self" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)postQuestion:(NSString *)text category:(int)categoryId lat:(double)lat lon:(double)lon files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"text"] = text;
    parameters[@"id_category"] = @(categoryId);
    parameters[@"lat"] = @(lat);
    parameters[@"lon"] = @(lon);
    parameters[@"files"] = files;
    
    [self sendPOSTRequest:@"questions" parameters:parameters success:success failure:failure];
}

- (void)editQuestionId:(int)questionId text:(NSString *)text category:(int)categoryId lat:(double)lat lon:(double)lon files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id"] = @(questionId);
    parameters[@"text"] = text;
    parameters[@"id_category"] = @(categoryId);
    parameters[@"lat"] = @(lat);
    parameters[@"lon"] = @(lon);
    parameters[@"files"] = files;
    
    [self sendPUTRequest:@"questions" parameters:parameters success:success failure:failure];
}

- (void)deleteQuestionId:(int)questionId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id":@(questionId)}]; // [NSString stringWithFormat:@"%d", questionId]; //
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendDELETERequest:@"questions" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)questionsCategoriesSuccess:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    [self sendGETRequest:@"questions/categories" parameters:parameters success:success failure:failure];
}

- (void)complainQuestionId:(int64_t)questionId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_question"] = @(questionId);
    parameters[@"type"] = @(0);
    
    [self sendPOSTRequest:@"questions/report" parameters:parameters success:success failure:failure];
}

#pragma mark - Answers

- (void)answersForQuestionId:(NSInteger)questionId limit:(NSInteger)limit offset:(NSInteger)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_question":@((NSInteger)questionId), /*@"time":@((NSUInteger)timeInterval),*/ @"limit":@((NSUInteger)limit), @"offset":@((NSUInteger)offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"answers/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)postAnswerForQuestionId:(int64_t)questionId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_question"] = @(questionId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPOSTRequest:@"answers" parameters:parameters success:success failure:failure];
}

- (void)likeAnswerId:(int64_t)answerId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_answer"] = @(answerId);
    
    [self sendPOSTRequest:@"answers/likes" parameters:parameters success:success failure:failure];
}




#pragma mark - Groups
- (void)groupsMostTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/most" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)groupsGeoTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval), */@"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/geo" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)groupById:(int)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id":@(groupId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)groupPostsById:(int)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id":@(groupId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/posts" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

// {"name":"Name","short_description":"Text","file_cover":25,"type":1,"lat":1.0,"lon":1.0,"web_site":"site"}
- (void)createGroup:(NSString *)name description:(NSString *)description fileCover:(int)fileCover type:(int)type lat:(double)lat lon:(double)lon webSite:(NSString *)webSite success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"name"] = name;
    parameters[@"short_description"] = description;
    parameters[@"file_cover"] = @(fileCover);
    parameters[@"type"] = @(type);
    parameters[@"lat"] = @(lat);
    parameters[@"lon"] = @(lon);
    parameters[@"web_site"] = webSite;
    
    [self sendPOSTRequest:@"groups" parameters:parameters success:success failure:failure];
}

- (void)editGroupId:(int)groupId name:(NSString *)name description:(NSString *)description fileCover:(int)fileCover type:(int)type lat:(double)lat lon:(double)lon webSite:(NSString *)webSite success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id"] = @(groupId);
    parameters[@"name"] = name;
    parameters[@"short_description"] = description;
    parameters[@"file_cover"] = @(fileCover);
    parameters[@"type"] = @(type);
    parameters[@"lat"] = @(lat);
    parameters[@"lon"] = @(lon);
    parameters[@"web_site"] = webSite;
    
    [self sendPUTRequest:@"groups" parameters:parameters success:success failure:failure];
}

- (void)groupsCategoriesSuccess:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    [self sendGETRequest:@"groups/categories" parameters:parameters success:success failure:failure];
}

- (void)groupsMyTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/groups" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)groupsMeAdminTime:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{/*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"users/groups/admin" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)membersGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_group":@(groupId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/members/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)membersRequestsGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_group":@(groupId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/members/requested" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)membersBannedGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_group":@(groupId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/members/banned" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)banUserGroupId:(int)groupId userId:(int)userId ban:(BOOL)needBan success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id"] = @(groupId);
    parameters[@"id_user"] = @(userId);
    parameters[@"ban"] = @(needBan);
    
    [self sendPUTRequest:@"groups/members/ban" parameters:parameters success:success failure:failure];
}

- (void)joinGroupId:(int64_t)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_group"] = @(groupId);
    
    [self sendPOSTRequest:@"groups/members" parameters:parameters success:success failure:failure];
}

- (void)unfollowGroupId:(int64_t)groupId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_group":@(groupId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendDELETERequest:@"groups/members" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}


- (void)groupsSearch:(NSString *)text limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"text":text, @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/search" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

#warning ??? Backend problems ???
- (void)adsForGroupId:(int64_t)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_group":@(groupId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"ad/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)createAdForGropId:(int)groupId text:(NSString *)text cities:(NSArray *)citiesIdsArray minYears:(int)minYears maxYears:(int)maxYears success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSMutableDictionary *parameters = [self parametersBasic];
    [parameters addEntriesFromDictionary:@{@"id_group":@(groupId), @"text":text, @"id_cities":citiesIdsArray, @"min_years":@(minYears), @"max_years":@(maxYears)}];
    [self sendPOSTRequest:@"ad" parameters:parameters success:success failure:failure];
}

#pragma mark - Group Posts
- (void)groupPostId:(int)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id":@(postId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/posts" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)postsGroupId:(int)groupId limit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id":@(groupId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/posts/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)sendPostToGroupId:(int)groupId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_group"] = @(groupId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPOSTRequest:@"groups/posts" parameters:parameters success:success failure:failure];
}

- (void)editGroupPostId:(int)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id"] = @(postId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPUTRequest:@"groups/posts" parameters:parameters success:success failure:failure];
}

- (void)deleteGroupPostId:(int)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id":@(postId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendDELETERequest:@"groups/members" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)commentsGroupPostId:(int)postId time:(NSTimeInterval)timeInterval limit:(int)limit offset:(int)offset  success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_post":@(postId), /*@"time":@((NSUInteger)timeInterval),*/ @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"groups/posts/comments/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)likeGroupPostId:(NSInteger)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_post"] = @(postId);
    
    [self sendPOSTRequest:@"groups/posts/likes" parameters:parameters success:success failure:failure];
}

#pragma mark - Posts

- (void)complainPostId:(int64_t)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_post"] = @(postId);
    parameters[@"type"] = @(0);
    
    [self sendPOSTRequest:@"posts/report" parameters:parameters success:success failure:failure];
}

- (void)sendPostToWallId:(NSNumber*)wallId text:(NSString*)text files:(NSString*)files link:(NSString*)link success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_wall"] = wallId;
    parameters[@"text"] = AS_STRING(text);
    parameters[@"link"] = AS_STRING(link);
    parameters[@"files"] = AS_STRING(files);
    
    [self sendPOSTRequest:@"posts" parameters:parameters success:success failure:failure];
}

- (void)deleteWallPostId:(int64_t)postId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
//    NSMutableDictionary *parameters = [self parametersBasic];
//    parameters[@"id_post"] = @(postId);
//    [self sendDELETERequest:@"posts" parameters:parameters success:success failure:failure];
    
    NSString* dataString = [self jsonStringForObject:@{@"id":@(postId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendDELETERequest:@"posts" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void) postsForWallId:(NSNumber*)wallId  time:(NSTimeInterval)timeInterval limit:(NSInteger)limit offset:(NSUInteger)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id_wall":wallId, /*@"time":@((NSUInteger)timeInterval), */@"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"posts/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

#pragma mark - Comments

- (void)sendCommentToPostId:(int64_t)postId text:(NSString *)text files:(NSString *)files success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_post"] = @(postId);
    parameters[@"text"] = text;
    parameters[@"files"] = files;
    
    [self sendPOSTRequest:@"posts/comments" parameters:parameters success:success failure:failure];
}

- (void) getCommentsPostId:(NSInteger)postId limit:(NSInteger)limit offset:(NSUInteger)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"id_post":@(postId), @"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"posts/comments/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)likePostCommenId:(int64_t)commentId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_comment"] = @(commentId);
    
    [self sendPOSTRequest:@"groups/posts/comments/likes" parameters:parameters success:success failure:failure];
}


#pragma mark - Dialogs
- (void)dialogsLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"dialogs/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)dialogsMessagesLimit:(int)limit offset:(int)offset success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSString* dataString = [self jsonStringForObject:@{@"limit":@(limit), @"offset":@(offset)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendGETRequest:@"dialogs/messages/last" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

#warning ??? Backend problems ???
- (void)markReadMessageId:(int)messageId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:@{@"id":@(messageId)}];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;
        [self sendPUTRequest:@"dialogs/messages/read" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)sendMessage:(NSString *)text toUserId:(int)userId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"text"] = text;
    parameters[@"id_user"] = @(userId);
    
    [self sendPOSTRequest:@"dialogs/messages" parameters:parameters success:success failure:failure];
}


#pragma mark - Files
- (void)uploadFile:(NSData*)fileData withName:(NSString*)fileName success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    
    NSMutableDictionary *parameters = [self parametersBasic];
    
    NSString* mimeType = @"text/plain";
    NSString* ext = [[fileName pathExtension] lowercaseString];
    if ([ext isEqualToString:@"jpg"] || [ext isEqualToString:@"jpeg"])
        mimeType = @"image/jpg";
    else if ([ext isEqualToString:@"gif"])
        mimeType = @"image/gif";
    else if ([ext isEqualToString:@"png"])
        mimeType = @"image/gif";
    #warning TODO: need to implement more mime types
    
    [self sendMultypartPOSTRequest:@"files" parameters:parameters constructingBody:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:fileData name:@"File" fileName:fileName mimeType:mimeType];
    } success:success failure:failure];
}

- (void)getFileList:(NSArray *)filesIds success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:filesIds];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        parameters[@"data"] = dataString;

        [self sendGETRequest:@"files/list" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void)addPhoto:(int)fileId toGallery:(int)wallId success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure {
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"id_file"] = @(fileId);
    parameters[@"id_wall"] = @(wallId);
    
    [self sendPOSTRequest:@"gallery" parameters:parameters success:success failure:failure];
}

//#warning TODO: need more information 
/*
 @Multipart
 @POST("api/video")
 Call<UploadFileResponse> uploadVideo(@Header("X-USER") int userId, @Header("X-PHONE-TOKEN") String token,
 @PartMap Map<String, RequestBody> file);
 
 @PUT("api/users")
 Call<ApiResponse<Integer>> updateAvatar(@Header("X-USER") int userId, @Header("X-PHONE-TOKEN") String token,
 @Body Map<String, Object> data);
*/

#pragma mark - Info

- (void)citiesSuccess:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    [self sendGETRequest:@"cities" parameters:parameters success:success failure:failure];
}

- (void)sendQrCode:(NSString *)qrToken success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
//    NSMutableDictionary *parameters = [self parametersBasic];
//    parameters[@"token"] = qrToken;
//    
//    [self sendPUTRequest:@"qrcode" parameters:parameters success:success failure:failure];
    
//    NSString* dataString = [self jsonStringForObject:@{@"token":qrToken}];
   NSString* dataString = qrToken;

    DLog(@"DATA QR:%@ ", dataString);
    
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        [parameters setObject:dataString forKey:@"token"];
//        parameters[@"data"] = dataString;
        
        [self sendPUTRequest:@"qrcode" parameters:parameters success:success failure:failure];
    } else {
        [self jsonFailure:failure];
    }
}

- (void) sendContacts:(NSArray*)contacts success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSString* dataString = [self jsonStringForObject:contacts];
    if (dataString) {
        NSMutableDictionary *parameters = [self parametersBasic];
        NSData *contactsData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
        
        [self sendPOSTRequest:@"users/phones" parameters:parameters constructingBody:^(id<AFMultipartFormData> formData) {
            DLog(@"constructing");
            [formData appendPartWithFileData:contactsData name:@"data" fileName:@"data" mimeType:@"application/json"];
//            NSDictionary *headers = @{@"Content-Type" : @"application/json"};
//            [formData appendPartWithHeaders:headers body:contactsData];
        } success:^(id responseObject) {
            if (success)
                success(responseObject);
        } failure:^(NSError *error, NSDictionary *userInfo) {
            if (failure)
                failure(error,userInfo);
        }];
        
    } else {
        [self jsonFailure:failure];
    }
}

#pragma mark - Geo

- (void) sendLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
{
    NSMutableDictionary *parameters = [self parametersBasic];
    parameters[@"lat"] = @(latitude);
    parameters[@"lon"] = @(longitude);
    
    [self sendPUTRequest:@"users/geo" parameters:parameters success:success failure:failure];
}


#pragma mark - AFNetworking Low Level

- (NSMutableDictionary *)parametersBasic
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    // Addiional init place
//    if (SECRET_TOKEN) {
//        parameters[@"Phone_token"] = SECRET_TOKEN;
//        parameters[@"Id_user"] = USER_ID.stringValue;
//    }
    
    return parameters;
}

- (void)sendPOSTRequest:(NSString *)request
             parameters:(NSMutableDictionary *)parameters
       constructingBody:(GBServerManagerConstructingBodyHandler)bodyBlock
                success:(GBServerManagerSuccessHandler)success
                failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypePOST withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:bodyBlock success:success failure:failure];
}

- (void)sendMultypartPOSTRequest:(NSString *)request
             parameters:(NSMutableDictionary *)parameters
       constructingBody:(GBServerManagerConstructingBodyHandler)bodyBlock
                success:(GBServerManagerSuccessHandler)success
                failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypeMultypartPOST withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:bodyBlock success:success failure:failure];
}


- (void)sendPOSTRequest:(NSString *)request
             parameters:(NSMutableDictionary *)parameters
                success:(GBServerManagerSuccessHandler)success
                failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypePOST withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:nil success:success failure:failure];
}

- (void)sendGETRequest:(NSString *)request
            parameters:(NSMutableDictionary *)parameters
               success:(GBServerManagerSuccessHandler)success
               failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypeGET withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:nil success:success failure:failure];
}

- (void)sendPUTRequest:(NSString *)request
            parameters:(NSMutableDictionary *)parameters
               success:(GBServerManagerSuccessHandler)success
               failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypePUT withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:nil success:success failure:failure];
}

- (void)sendDELETERequest:(NSString *)request
               parameters:(NSMutableDictionary *)parameters
                  success:(GBServerManagerSuccessHandler)success
                  failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypeDELETE withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:nil success:success failure:failure];
}

- (void)sendPATCHRequest:(NSString *)request
               parameters:(NSMutableDictionary *)parameters
                  success:(GBServerManagerSuccessHandler)success
                  failure:(GBServerManagerFailureHandler)failure
{
    [self sendRequest:request withType:RequestTypePATCH withResultType:RequestManagerResultTypeJSON parameters:parameters constructingBody:nil success:success failure:failure];
}


- (void)sendRequest:(NSString *)request
           withType:(RequestType)type
     withResultType:(RequestManagerResultType)resultType
         parameters:(NSMutableDictionary *)parameters
   constructingBody:(GBServerManagerConstructingBodyHandler)bodyBlock
            success:(GBServerManagerSuccessHandler)success
            failure:(GBServerManagerFailureHandler)failure
{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer_GB serializer]; // [AFHTTPRequestSerializer serializer];
    
    NSString *url = nil;
    if ([request hasPrefix:@"https://"] || [request hasPrefix:@"http://"])
        url =  request;
    else{
        url = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, request]; 
    }
//        url = [NSString stringWithFormat:@"%@/api/%@", BASE_URL_STRING, request];
        
    DLog(@"%@", url);
    
    switch (resultType) {
        case RequestManagerResultTypeString:
            manager.responseSerializer = [AFHTTPResponseSerializer serializer];
            break;
            
        case RequestManagerResultTypeJSON: {
            AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
            serializer.removesKeysWithNullValues = YES;
            serializer.readingOptions = NSJSONReadingMutableContainers | NSJSONReadingAllowFragments;
            manager.responseSerializer = serializer;
            break;
        }
            
        default:
            break;
    }
    
    switch (type) {
        case RequestTypeGET: {
            AFHTTPRequestSerializer_GB* requestSerializer = [AFHTTPRequestSerializer_GB serializer];
            manager.requestSerializer = requestSerializer;
            
            [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, id responseObject) {
                if ([self checkErrorField:responseObject]) {
                    NSError *error = [self createError:responseObject];
                    [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                } else {
                    
                    
                    [self successWithUrl:url resultType:resultType parameters:parameters responseObject:responseObject success:success failure:failure];
                }
                [manager invalidateSessionCancelingTasks:YES];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                [manager invalidateSessionCancelingTasks:YES];

            }];
            break;
        }
            
        case RequestTypePOST: {

            AFJSONRequestSerializer_GB* requestSerializer = [AFJSONRequestSerializer_GB serializer];
            manager.requestSerializer = requestSerializer;

            [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([self checkErrorField:responseObject]) {
                    NSError *error = [self createError:responseObject];
                    [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                } else {
                    [self successWithUrl:url resultType:resultType parameters:parameters responseObject:responseObject success:success failure:failure];
                }
                [manager invalidateSessionCancelingTasks:YES];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                [manager invalidateSessionCancelingTasks:YES];

            }];
            
            break;
        }
            
        case RequestTypeMultypartPOST: {
            [manager POST:url parameters:parameters constructingBodyWithBlock:bodyBlock progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([self checkErrorField:responseObject]) {
                    NSError *error = [self createError:responseObject];
                    [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                } else {
                    [self successWithUrl:url resultType:resultType parameters:parameters responseObject:responseObject success:success failure:failure];
                }
                [manager invalidateSessionCancelingTasks:YES];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                [manager invalidateSessionCancelingTasks:YES];

            }];
            
            break;
        }
            
        case RequestTypePUT: {
            [manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([self checkErrorField:responseObject]) {
                    NSError *error = [self createError:responseObject];
                    [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                } else {
                    [self successWithUrl:url resultType:resultType parameters:parameters responseObject:responseObject success:success failure:failure];
                }
                [manager invalidateSessionCancelingTasks:YES];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                [manager invalidateSessionCancelingTasks:YES];

            }];
            
            break;
        }

        case RequestTypeDELETE: {
            [manager DELETE:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([self checkErrorField:responseObject]) {
                    NSError *error = [self createError:responseObject];
                    [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                } else {
                    [self successWithUrl:url resultType:resultType parameters:parameters responseObject:responseObject success:success failure:failure];
                }
                [manager invalidateSessionCancelingTasks:YES];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                [manager invalidateSessionCancelingTasks:YES];

            }];
            
            break;
        }

        case RequestTypePATCH: {
            [manager PATCH:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([self checkErrorField:responseObject]) {
                    NSError *error = [self createError:responseObject];
                    [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                } else {
                    [self successWithUrl:url resultType:resultType parameters:parameters responseObject:responseObject success:success failure:failure];
                }
                [manager invalidateSessionCancelingTasks:YES];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self failureWithUrl:url error:error parameters:parameters success:success failure:failure];
                [manager invalidateSessionCancelingTasks:YES];

            }];
            
            
            break;
        }
        
    }
}

- (NSError *)createError:(id)responseObject {
    NSError *error = [NSError errorWithDomain:@"Server error" code:0 userInfo:nil];
    
    if (!responseObject[@"errors"]) return error;
    if (!responseObject[@"errors"][0][@"error_msg"]) return error;
    
    NSString *message = responseObject[@"errors"][0][@"error_msg"];
    NSNumber *code = responseObject[@"errors"][0][@"error_code"];
    
    if ([message isKindOfClass:[NSString class]]) {
        return [NSError errorWithDomain:@"Server error" code:code.intValue userInfo:@{NSLocalizedDescriptionKey: message}];
    }
    
    return error;
}

- (BOOL)checkErrorField:(id)responseObject {
    return responseObject[@"error"] ? YES : NO;
}

- (void)successWithUrl:(NSString *)url
            resultType:(RequestManagerResultType)resultType
            parameters:(NSDictionary *)parameters
        responseObject:(id)responseObject
               success:(GBServerManagerSuccessHandler)success
               failure:(GBServerManagerFailureHandler)failure {
    
    if (resultType == RequestManagerResultTypeString) {
        responseObject = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
    }
    
    
    if (success)
        success(responseObject);
    
}

- (void)failureWithUrl:(NSString *)url
                 error:(NSError *)error
            parameters:(NSDictionary *)parameters
               success:(GBServerManagerSuccessHandler)success
               failure:(GBServerManagerFailureHandler)failure {
    
    DLog(@"error: %@", error);
    
    NSMutableDictionary *userInfo;
    if (error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]) {
        NSError *jsonError;
        userInfo = [NSJSONSerialization JSONObjectWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]
                                                   options:NSJSONReadingMutableContainers | NSJSONReadingAllowFragments
                                                     error:&jsonError];
        if (jsonError) {
            DLog(@"Error in JSON: %@", jsonError);
            
            NSString *string = [[NSString alloc] initWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
            DLog(@"Try to convert JSON to string: %@", string);
            
            userInfo = [NSMutableDictionary dictionary];
            userInfo[@"message"] = @"Server error";
            if (string) {
                userInfo[@"errors"] = string;
            }
            
            DLog(@"Try to convert JSON to image");
            UIImage *image = [[UIImage alloc] initWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey]];
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"Image.png"];
            [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
        }
    } else {
        [self addErrorInLog:error url:url parameters:parameters];
    }
    
    if (failure) failure(error, userInfo);
}

- (void)addErrorInLog:(NSError *)error url:(NSString *)url parameters:(NSDictionary *)parameters {
    DLog(@"Error in request %@: %@\nparameters:\n%@", url, error.localizedDescription, parameters);
}

- (NSString *)jsonStringForObject:(id)object {
    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:0 error:&err];
    NSString* result = nil;
    if (err == nil)
        result = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return result;
}

- (void)jsonFailure:(GBServerManagerFailureHandler)failure {
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Wrond JSON encoding"};
    NSError *error = [NSError errorWithDomain:@"err.error" code:1001 userInfo:userInfo];
    failure(error, userInfo);
}

#pragma marl - QR-Code
//- (void)sendQr:(double)lat lon:(double)lon success:(GBServerManagerSuccessHandler)success failure:(GBServerManagerFailureHandler)failure
//{
//    NSMutableDictionary *parameters = [self parametersBasic];
//    parameters[@"lat"] = @(lat);
//    parameters[@"lon"] = @(lon);
//    
//    [self sendPUTRequest:@"users/geo" parameters:parameters success:success failure:failure];
//}

@end
