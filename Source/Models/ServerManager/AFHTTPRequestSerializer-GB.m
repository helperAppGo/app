//
//  AFHTTPRequestSerializer-GB.m
//  GoBeside
//
//  Created by Serge Moskalenko on 8/21/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "AFHTTPRequestSerializer-GB.h"

@implementation AFHTTPRequestSerializer_GB

- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request withParameters:(id)parameters error:(NSError *__autoreleasing *)error {
    NSMutableURLRequest *mutableRequest = [[super requestBySerializingRequest:request withParameters:parameters error:error] mutableCopy];
    [self.HTTPRequestHeaders enumerateKeysAndObjectsUsingBlock:^(id field, id value, BOOL * __unused stop) {
        if (![request valueForHTTPHeaderField:field]) {
            [mutableRequest setValue:value forHTTPHeaderField:field];
        }
    }];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [mutableRequest setValue:userIdString forHTTPHeaderField:@"X-USER"];
        [mutableRequest setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
    }
    
    return mutableRequest;
}


- (NSMutableURLRequest *)multipartFormRequestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(nullable NSDictionary <NSString *, id> *)parameters constructingBodyWithBlock:(nullable void (^)(id <AFMultipartFormData> formData))block error:(NSError * _Nullable __autoreleasing *)error {
    
    NSMutableURLRequest *mutableRequest = [super multipartFormRequestWithMethod:method URLString:URLString parameters:parameters constructingBodyWithBlock:block error:error];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [mutableRequest setValue:userIdString forHTTPHeaderField:@"X-USER"];
        [mutableRequest setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
    }
    
    return mutableRequest;
}

- (NSMutableURLRequest *)requestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(id)parameters error:(NSError *__autoreleasing *)error
{
    NSMutableURLRequest *mutableRequest = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    
    if ([method isEqualToString:@"GET"] || [method isEqualToString:@"POST"])
        return mutableRequest;
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [mutableRequest setValue:userIdString forHTTPHeaderField:@"X-USER"];
        [mutableRequest setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
    }
    
    return mutableRequest;
}


@end
