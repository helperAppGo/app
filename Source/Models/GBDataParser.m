//  GBDataParser.m
//  GoBeside

#import "GBDataParser.h"

@implementation GBDataParser

- (instancetype)initWithDataHolder:(MainDataHolder *)holder
{
    self = [super init];
    if (self)
    {
        dataHolder = holder;
    }
    return self;
}

- (GBUserModel *)parseUser:(NSDictionary *)data
{
    dataHolder.currentUser = [GBUserModel getObjectByDictionary:data];
    return dataHolder.currentUser;
}

- (NSArray *)parseUsers:(NSDictionary *)data
{
    NSArray * res = nil;
    
    res = [GBUserModel getObjectsFromArray:[[data valueForKey:@"response"] valueForKey:@"users"]];
    NSLog(@"have res = %@" , res);
    return res;
}

- (NSArray *)parseLastMessage:(NSDictionary *)data {
    NSArray *res = nil;
    
    res = [GBMessageModel getObjectsFromArray:[[data valueForKey:@"response"] valueForKey:@"messages"]];
    
    return res;
}

- (NSArray *)parseMessages:(NSDictionary *)data {
    NSArray *res = nil;
    
    res = [GBMessageModel getObjectsFromArray:[data valueForKey:@"response"]];
    
    return res;
}

- (NSArray *)parseGroupMost:(NSDictionary *)data {
    NSArray *res = nil;
    
    res = [GBAllGroupModel getObjectsFromArray:[data valueForKey:@"response"]];
    
    return res;
}

- (NSArray *)parseGroupPostList:(NSDictionary *)data {
    NSArray *res = nil;
    
    res = [GBWallModel getObjectsFromArray:[data valueForKey:@"response"]];
    
    return res;
}

- (NSArray *)parseListMembers:(NSDictionary *)data {
    NSArray *res = nil;
    
    res = [GBListMembersModel getObjectsFromArray:[data valueForKey:@"response"]];
    
    return res;
}



//- (NSArray *)parseApiGroup:(NSArray *)data {
//    NSArray *res = nil;
//    
//    res = [GBAllGroupModel getApiGroupObjectsFromArray:[data valueForKey:@"response"]];
//    
//    return res;
//}


- (NSArray *)parseLastQuestions:(NSDictionary *)data
{
    NSArray * res = nil;
    if (data && [data isKindOfClass:[NSArray class]])
    {
        //        res = [GBLastQuestionsModel getObjectsFromArray:data];
    }
    
    return res;
}

- (NSArray *)parseGeoQuestions:(NSDictionary *)data
{
    NSArray * res = nil;
    if (data && [data isKindOfClass:[NSArray class]])
    {
        //        res = [GBGeoQuestionsModel getObjectsFromArray:data];
    }
    
    return res;
}




@end

