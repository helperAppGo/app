//  GBModel.m
//  GoBeside

#import "GBModel.h"

@implementation GBModel

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary
{
    return [[self alloc] initWithDictionary:dictionary];
}

+ (NSArray *)modelsWithCollection:(NSObject<NSFastEnumeration> *)collection
{
    if (![collection conformsToProtocol:@protocol(NSFastEnumeration)]) return nil;
    
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *dictionary in collection)
    {
        if (![dictionary isKindOfClass:[NSDictionary class]]) continue;
        
        GBModel *model = [self modelWithDictionary:dictionary];
        
        if (model)
        {
            [result addObject:model];
        }
    }
    
    return [result copy];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [self init];
    
    return self;
}

@end
