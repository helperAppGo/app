//  GBModel.h
//  GoBeside

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>

@interface GBModel : NSObject

+ (instancetype)modelWithDictionary:(NSDictionary *)dictionary;
+ (NSArray *)modelsWithCollection:(NSObject<NSFastEnumeration> *)collection;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
