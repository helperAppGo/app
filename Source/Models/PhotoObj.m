//// Header

#import "PhotoObj.h"

@implementation PhotoObj

-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.path = NULL_TO_NIL(dict[@"path"]);
        self.height = [NULL_TO_NIL(dict[@"height"]) floatValue];
        self.width = [NULL_TO_NIL(dict[@"width"]) floatValue];
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.preview = NULL_TO_NIL(dict[@"preview"]);
        self.typeString = NULL_TO_NIL(dict[@"type"]);
    }
    return self;
}
@end
