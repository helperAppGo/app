//
//  GBFollowButton.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/26/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFollowButton.h"

@implementation GBFollowButton

- (instancetype) initWithModel:(GBAllGroupModel *)groupModel {
    self = (GBFollowButton *)[GBFollowButton buttonWithType:UIButtonTypeCustom];
    if (self) {
        self.groupModel = groupModel;
    }
    return self;
}

@end
