//
//  GBLocationManager.h
//  GoBeside
//
//  Created by Ruslan Mishin on 23.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBLocationManagerListener.h"

@interface GBLocationManager : NSObject <CLLocationManagerDelegate>{
    CLLocation *_userLocation;
}

+ (instancetype) sharedManager;

-(CLLocation *)currentLocation;

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,assign,readonly) BOOL isUpdatingLocation;

- (void) addLocationListener:(id<GBLocationManagerListener>)aLocationListener;
- (void) removeLocationListener:(id<GBLocationManagerListener>)aLocationListener;

@end
