//
//  GBLocationManagerListener.h
//  GoBeside
//
//  Created by Ruslan Mishin on 23.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

@protocol GBLocationManagerListener <NSObject>

- (void) didUpdateToLocation:(CLLocation*)location lastKnownLocation:(CLLocation*)lastKnownLocation;

@end
