//
//  GBLocationManager.m
//  GoBeside
//
//  Created by Ruslan Mishin on 23.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBLocationManager.h"
#import "GBWeakifier.h"
#import "FeedListViewController.h"
@import CoreLocation;


static BOOL locationAlertShown = NO;


@interface GBLocationManager()

@property (nonatomic, strong) NSMutableArray<GBWeakifier*> * locationListeners;

@end


@implementation GBLocationManager

#pragma mark - Class

+ (instancetype)sharedManager
{
    static GBLocationManager * sharedLocationManager = nil;
    static dispatch_once_t sharedLocationManager_once;
    dispatch_once(&sharedLocationManager_once, ^{
        sharedLocationManager = [[self alloc] init];
    });
    return sharedLocationManager;
}


#pragma mark - Lifecycle

- (instancetype) init
{
    self = [super init];
    if (self) {
        
        self.locationListeners = [NSMutableArray array];
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.locationManager.distanceFilter = 50.0;//kCLDistanceFilterNone;
        
        [self listenNotifications];
    }
    return self;
}


#pragma mark - Public

- (void) addLocationListener:(id<GBLocationManagerListener>)aLocationListener
{
    if (!aLocationListener) return;
    
    NSArray * filtered = [self.locationListeners filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"obj == %@",aLocationListener]];
    if (!filtered.count) //new listener is absent in listeners list
        [self.locationListeners addObject:[GBWeakifier weakifierWithObject:aLocationListener]];
    
    [self cleanupListenersList];
    
    if (!self.isUpdatingLocation) {
        [self startUpdatingLocation];
        [self locationAvailabilityCheck];
    }else if (self.locationManager.location) {
        CLLocation * location = self.locationManager.location;
        dispatch_async(DISP_MAIN_THREAD, ^{
            if ([aLocationListener respondsToSelector:@selector(didUpdateToLocation:lastKnownLocation:)])
                [aLocationListener didUpdateToLocation:location lastKnownLocation:location];
        });
    }
}

- (void) removeLocationListener:(id<GBLocationManagerListener>)aLocationListener
{
    if (!aLocationListener) return;
    
    NSArray * filtered = [self.locationListeners filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"obj == %@",aLocationListener]];
    if (filtered.count)
        [self.locationListeners removeObjectsInArray:filtered];
    
    [self cleanupListenersList];
}


#pragma mark - Private Methods

- (void) startUpdatingLocation
{
    _isUpdatingLocation = YES;
    [self.locationManager startUpdatingLocation];
}

- (void) stopUpdatingLocation
{
    _isUpdatingLocation = NO;
    [self.locationManager stopUpdatingLocation];
}

- (void) cleanupListenersList
{
    NSArray * filtered = [self.locationListeners filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"obj == nil"]];
    if (filtered.count)
        [self.locationListeners removeObjectsInArray:filtered];
    if (self.isUpdatingLocation && !self.locationListeners.count)
        [self stopUpdatingLocation];
}

- (void) postLocation:(CLLocation*)aLocation lastKnownLocation:(CLLocation*)aLastKnownLocation
{
    [self cleanupListenersList];
    
    for (GBWeakifier * weakifier in self.locationListeners) {
        id<GBLocationManagerListener> listener = weakifier.obj;
        if ([listener respondsToSelector:@selector(didUpdateToLocation:lastKnownLocation:)])
            [listener didUpdateToLocation:aLocation lastKnownLocation:aLastKnownLocation];
    }
}

- (void) locationAvailabilityCheck
{
    if ([CLLocationManager locationServicesEnabled]) {
        CLAuthorizationStatus authorizationStatus = [CLLocationManager authorizationStatus];
        if (authorizationStatus == kCLAuthorizationStatusNotDetermined) {
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
            }
            [self startUpdatingLocation];
        } else if (authorizationStatus == kCLAuthorizationStatusAuthorizedAlways || authorizationStatus == kCLAuthorizationStatusAuthorizedWhenInUse) {
            [self startUpdatingLocation];
        } else if (authorizationStatus == kCLAuthorizationStatusDenied) {
            if (!locationAlertShown) {
                [self showLocationSettingsAlert];
                locationAlertShown = YES;
            }
            [self postLocation:nil lastKnownLocation:self.locationManager.location];
        }
        else if ([Utils isAirplaneModeOn]) {
            [self showLocationAirPlaneModeAlert];
            [self postLocation:nil lastKnownLocation:self.locationManager.location];
        }
    }
}

- (void)showLocationAirPlaneModeAlert
{
    DLog(@"Showing alert (LocationAirPlaneMode): Turn Off Airplane Mode or Use Wi-Fi to Access Data...");
    NSString *title = NSLocalizedString(@"Turn Off Airplane Mode or\n"
                                        "Use Wi-Fi to Access Data", @"turn off air plane mode");
    [GBAlerter show2ButtonsAlertWithTitle:title
                                  message:nil
                             cancelButton:NSLocalizedString(@"Ok", nil)
                              otherButton:NSLocalizedString(@"Settings", nil)
                           viewController:nil
                                 onCancel:nil onOther:^{
                                     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                 }];
}



- (void)showLocationSettingsAlert
{
    DLog(@"Showing alert (Location Manager): %@ cannot retrive your current location",APPNAME);
    
    NSString * ttl = NSLocalizedString(@"App cannot retrieve your\ncurrent location.\n"
                                       "Please make sure location\n services are enabled.",
                                       @"configure location settings");
    [GBAlerter showAlertWithMessage:ttl];
}

- (void) listenNotifications
{
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(applicationDidEnterBackgroundNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [notificationCenter addObserver:self selector:@selector(applicationWillEnterForegroundNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
}


#pragma mark - CLLocationManagerDelegate

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"Error: %@",error.description);
    [self postLocation:nil lastKnownLocation:manager.location];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *location = [locations lastObject];
//    DLog(@"current location: %@", location);
    _userLocation = location;
    [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_UPDATED object:nil];
    [self postLocation:location lastKnownLocation:manager.location];
    
//    if (location) {
//        float distance = [location distanceFromLocation:_userLocation];
//        if (distance > 50) {
//            [[NSNotificationCenter defaultCenter] postNotificationName:LOCATION_UPDATED object:nil];
//            [self postLocation:location lastKnownLocation:manager.location];
//        }
//    }
    
    
}

-(CLLocation *)currentLocation{
    return _userLocation;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            [self postLocation:nil lastKnownLocation:manager.location];
        }
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            NSLog(@"LOCATION");
            [self startUpdatingLocation];
        }
            break;
            
        default:
            break;
    }
//    UITabBarController *tabController  = [[UIApplication sharedApplication] delegate].window.rootViewController;
//    UINavigationController *navController = tabController.selectedViewController;
//    FeedListViewController *controller = [navController viewControllers][0];
//    if ([controller isKindOfClass:[FeedListViewController class]]) {
//        [controller localNext];
//    }
    
}


#pragma mark - Notifications

- (void) applicationDidEnterBackgroundNotification:(NSNotification*)aNotification
{
    [self stopUpdatingLocation];
}

- (void) applicationWillEnterForegroundNotification:(NSNotification*)aNotification
{
    [self cleanupListenersList];
    if (self.locationListeners.count)
        [self startUpdatingLocation];
}

@end
