//
//  GBSynchronizedContacts.h
//  GoBeside
//
//  Created by Ruslan Mishin on 30.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBSynchronizedContacts : NSObject

@property (nonatomic,strong, readonly) NSDictionary *contacts;
@property (nonatomic,strong, readonly) NSDictionary *users;

+ (instancetype) sharedInstance;

- (void) addContacts:(NSDictionary*)contacts;
- (void) addUsers:(NSDictionary*)users;
- (NSDictionary*) unsynchronizedContactsFrom:(NSDictionary*)contacts;

@end
