//
//  GBSynchronizedContacts.m
//  GoBeside
//
//  Created by Ruslan Mishin on 30.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBSynchronizedContacts.h"


#define GB_SYNCHRONIZED_CONTACTS_STORAGE_FILE               [CachedDataPath stringByAppendingPathComponent:@"scs.dat"]
#define GB_SYNCHRONIZED_USERS_STORAGE_FILE                  [CachedDataPath stringByAppendingPathComponent:@"sus.dat"]


@interface GBSynchronizedContacts()

@property (nonatomic,strong, readwrite) NSDictionary *contacts;
@property (nonatomic,strong, readwrite) NSDictionary *users;

@end


@implementation GBSynchronizedContacts

#pragma mark - Class

+ (instancetype) sharedInstance
{
    static GBSynchronizedContacts * sharedSyncronizedContacts = nil;
    static dispatch_once_t sharedSyncronizedContacts_once;
    dispatch_once(&sharedSyncronizedContacts_once, ^{
        sharedSyncronizedContacts = [[self alloc] init];
    });
    return sharedSyncronizedContacts;
}


#pragma mark - Lifecycle

- (instancetype) init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


#pragma mark - Public

- (void) addContacts:(NSDictionary*)contacts
{
    NSMutableDictionary *mcontacts = [self.contacts mutableCopy];
    [mcontacts addEntriesFromDictionary:contacts];
    self.contacts = [mcontacts copy];
    [self saveContacts];
}

- (void) addUsers:(NSDictionary *)users
{
    NSMutableDictionary *musers = [self.users mutableCopy];
    [musers addEntriesFromDictionary:users];
    self.users = [musers copy];
    [self saveUsers];
}


#pragma mark - Private

- (void) saveContacts
{
    NSArray *keys = [self.contacts allKeys];
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    for (id key in keys) {
        GBContactModel *contact = self.contacts[key];
        md[key] = [contact toJSONString];
    }
    [md writeToFile:GB_SYNCHRONIZED_CONTACTS_STORAGE_FILE atomically:YES];
}

- (void) saveUsers
{
    NSArray *keys = [self.users allKeys];
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    for (id key in keys) {
        GBUser *user = self.users[key];
        md[key] = [user toJSONString];
    }
    [md writeToFile:GB_SYNCHRONIZED_USERS_STORAGE_FILE atomically:YES];
}

- (NSDictionary*) unsynchronizedContactsFrom:(NSDictionary*)contacts
{
    if (!self.contacts.count) return contacts;  //nothing is synchronized
    
    NSMutableSet *allKeysSet = [NSMutableSet setWithArray:[contacts allKeys]];
    NSSet *synchronizedKeysSet = [NSSet setWithArray:[self.contacts allKeys]];
    [allKeysSet minusSet:synchronizedKeysSet];
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    for (id key in allKeysSet)
        md[key] = contacts[key];
    
    return [md copy];
}

#pragma mark - Custom Getters

- (NSDictionary*) contacts
{
    if (!_contacts) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:GB_SYNCHRONIZED_CONTACTS_STORAGE_FILE]) {
            NSDictionary *d = [NSDictionary dictionaryWithContentsOfFile:GB_SYNCHRONIZED_CONTACTS_STORAGE_FILE];
            NSMutableDictionary *md = [NSMutableDictionary dictionary];
            NSArray *keys = [d allKeys];
            for (id key in keys) {
                NSString *json = d[key];
                NSError *error = nil;
                GBContactModel *contact = [[GBContactModel alloc] initWithString:json error:&error];
                if (contact) {
                    md[key] = contact;
                }else{
                    DLog(@">>>ERR>>> Cannot parse contact from string: %@. Error: %@",json,error);
                }
            }
            _contacts = [md copy];
        }
        if (!_contacts) _contacts = @{};
    }
    return _contacts;
}

- (NSDictionary*) users
{
    if (!_users) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:GB_SYNCHRONIZED_USERS_STORAGE_FILE]) {
            NSDictionary *d = [NSDictionary dictionaryWithContentsOfFile:GB_SYNCHRONIZED_USERS_STORAGE_FILE];
            NSMutableDictionary *md = [NSMutableDictionary dictionary];
            NSArray *keys = [d allKeys];
            for (id key in keys) {
                NSString *json = d[key];
                NSError *error = nil;
                GBUser *contact = [[GBUser alloc] initWithString:json error:&error];
                if (contact) {
                    md[key] = contact;
                }else{
                    DLog(@">>>ERR>>> Cannot parse user from string: %@. Error: %@",json,error);
                }
            }
            _users = [md copy];
        }
        if (!_users) _users = @{};
    }
    return _users;
}

@end
