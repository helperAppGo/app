//
//  GBContactsManager.h
//  GoBeside
//
//  Created by Ruslan Mishin on 28.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBContactsManager : NSObject

+ (instancetype) sharedManager;
- (void) sync:(BOOL)silent;

@end
