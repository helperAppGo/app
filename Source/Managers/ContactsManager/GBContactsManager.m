//
//  GBContactsManager.m
//  GoBeside
//
//  Created by Ruslan Mishin on 28.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBContactsManager.h"
#import "GBDataHelper.h"
#import "GBSynchronizedContacts.h"

@interface GBContactsManager()

@property (nonatomic,strong) CNContactStore *store;

@property (nonatomic,assign) BOOL syncIsActive;

@end


@implementation GBContactsManager

#pragma mark - Class

+ (instancetype)sharedManager
{
    static GBContactsManager * sharedAddressBookManager = nil;
    static dispatch_once_t sharedAddressBookManager_once;
    dispatch_once(&sharedAddressBookManager_once, ^{
        sharedAddressBookManager = [[self alloc] init];
    });
    return sharedAddressBookManager;
}


#pragma mark - Lifecycle

- (instancetype) init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark - Public

- (void) sync:(BOOL)silent
{
    if (!CONTACTS_SYNC_ENABLED) return;
    
    if ((!SECRET_TOKEN.length) || (USER_ID.longLongValue == 0)) return;
    if (self.syncIsActive) return;
    
    CNAuthorizationStatus authStatus = [CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]; //ABAddressBookGetAuthorizationStatus();
    if (authStatus == CNAuthorizationStatusDenied ||
        authStatus == CNAuthorizationStatusRestricted){
        //1
        DLog(@"ABAuthStatus - Denied");
    } else if (authStatus == CNAuthorizationStatusAuthorized){
        //2
        DLog(@"ABAuthStatus - Authorized");
        [self syncContacts];
    } else if (!silent){ //authStatus == CNAuthorizationStatusNotDetermined
        //3
        DLog(@"ABAuthStatus - Not determined");
        [self requestAccess];
    }
}

#pragma mark - Private

- (void) syncContacts
{
    if (self.syncIsActive) return;
    
    self.syncIsActive = YES;
    //keys with fetching properties
    NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPhoneNumbersKey];
    CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:keys];
    NSError *error = nil;
    __block NSMutableDictionary *contacts = [NSMutableDictionary dictionary];
    [self.store enumerateContactsWithFetchRequest:request error:&error usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop) {
        if (error) {
            DLog(@"error fetching contacts %@", error);
        } else if (contact.phoneNumbers.count) {
            NSString *contactName = [NSString stringWithFormat:@"%@%@%@",AS_STRING(contact.givenName),(contact.givenName.length&&contact.familyName.length)?@" ":@"",AS_STRING(contact.familyName)];
            DLog(@"Name: %@",contactName);
            NSArray *phoneNumbers = contact.phoneNumbers;
            NSCharacterSet *nonNumbers = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
            for (CNLabeledValue *phoneNumberValue in phoneNumbers) {
                CNPhoneNumber *phoneNumber = phoneNumberValue.value;
                DLog(@"PhoneNumber: %@",phoneNumber.stringValue);
//                if (phoneNumber.stringValue.length && ([phoneNumber.stringValue hasPrefix:@"+380"] || [phoneNumber.stringValue hasPrefix:@"380"])) {
                
                    GBContactModel *contactModel = [GBContactModel new];
                    contactModel.name = contactName;
                    contactModel.phone = @([[[phoneNumber.stringValue componentsSeparatedByCharactersInSet:nonNumbers] componentsJoinedByString:@""] longLongValue]);
                    contacts[contactModel.ident] = contactModel;
//                }
            }
        }
    }];
    NSDictionary *unsynchronizedContacts = contacts.count ? [[GBSynchronizedContacts sharedInstance] unsynchronizedContactsFrom:contacts] : nil;
    if (unsynchronizedContacts.count) {
        [self sendContacts:[unsynchronizedContacts allValues] withCompletion:^(BOOL result) {
            if (result) {
                [[GBSynchronizedContacts sharedInstance] addContacts:unsynchronizedContacts];
            }
            self.syncIsActive = NO;
        }];
    }else{
        self.syncIsActive = NO;
    }
}

- (void) requestAccess
{
    [self.store requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted)
            [self syncContacts];
    }];
}

- (void) sendContacts:(NSArray*)aContacts withCompletion:(GBBoolBlock)aCompletion
{
    [GBDataHelper sendContacts:aContacts completion:^(NSArray*users){
        
        NSMutableDictionary *md = [NSMutableDictionary dictionary];
        for (GBUser *user in users) {
            md[user.ident] = user;
        }
        [[GBSynchronizedContacts sharedInstance] addUsers:[md copy]];
        
        if (aCompletion)
            aCompletion(YES);
    } failure:^(NSError *err) {
        if (aCompletion)
            aCompletion(NO);
    }];
}

#pragma mark - Custom Getters

- (CNContactStore*)store
{
    if (!_store)
        _store = [[CNContactStore alloc] init];
    return _store;
}

@end
