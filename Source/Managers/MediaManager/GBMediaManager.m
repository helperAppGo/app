//  GBMediaManager.m
//  GoBeside

#import "GBMediaManager.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <ImageIO/ImageIO.h>
#import <AVFoundation/AVFoundation.h>

static NSString *GBTempDirName = @"UnsentImages";

@implementation GBMediaManager

+ (NSString *)writeImageToFile:(UIImage *)image_
{
    NSData *imageData = UIImageJPEGRepresentation(image_, 1.);
    return [GBMediaManager writeImageDataToFile:imageData extension:@"JPG"];
}

+ (NSString *)writeImageDataToFile:(NSData *)imageData extension:(NSString *)extension
{
    const int NUMBER_OF_CHARS = 40;
    char data[NUMBER_OF_CHARS];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    NSString *dirPath = GBTempDirName.length ? [documentsPath stringByAppendingPathComponent:GBTempDirName] : documentsPath;
    
    NSError * error;
    
    [fm createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
    
    for (int x=0; x < NUMBER_OF_CHARS; data[x++] = (char)('A' + (arc4random_uniform(26))));
    
    NSString *fileName = [[NSString alloc] initWithBytes:data length:NUMBER_OF_CHARS encoding:NSUTF8StringEncoding];
    fileName = [fileName stringByAppendingFormat:@".%@", extension ?: @""];
    
    NSString *filePath = [dirPath stringByAppendingPathComponent:fileName];
    [imageData writeToFile:filePath options:NSDataWritingAtomic error:&error];
    
    if (imageData.length > 1024*1024)
    {
        NSLog(@"TOO BIG IMAGE: %dkB - %@", (int)imageData.length / 1024, fileName);
    }
    return filePath;
}

+ (void)removeTempDir
{
    if (!GBTempDirName.length) return;
    
    NSString *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject;
    NSString *path = [documentsPath stringByAppendingPathComponent:GBTempDirName];
    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
}

@end
