//  GBMediaManager.h
//  GoBeside

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface GBMediaManager : NSObject

+ (NSString *)writeImageToFile:(UIImage *)image_;
+ (NSString *)writeImageDataToFile:(NSData *)imageData extension:(NSString *)extension;

+ (void)removeTempDir;

@end
