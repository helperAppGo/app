//  AFNetworkServer.h
//  GoBeside

#import <Foundation/Foundation.h>
#import "GBNetworkConstants.h"
#import "AFHTTPSessionManager+GBExtensions.h"

@interface AFNetworkServer : NSObject

@property BOOL networkIsReachable;

- (void)registrationWithParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;
- (void)phoneCode:(NSNumber *)userID completionBlock:(GBCompletionBlock)completion;
- (void)phoneToken:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;



- (void)getUserById:(NSNumber *)sbId completionBlock:(GBCompletionBlock)completion;
- (void)getUsers:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;
- (void)updateUser:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;


- (void)getLastQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;
- (void)getGeoQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;
@end
