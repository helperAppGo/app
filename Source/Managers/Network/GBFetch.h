#import <Foundation/Foundation.h>

@interface GBFetch : NSObject

typedef NS_ENUM(NSUInteger, method)  {
    postMethod,
    getMethod,
    getNewMethod,
    deleteMethod,
    putMethod
};

typedef void(^fetchHandleBlock)(NSData *data, NSURLResponse *response, NSError *error);

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

- (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion;
- (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link ;

@end
