//  GBNetworkConstants.m
//  GoBeside

#import "GBNetworkConstants.h"
#import "GBCommonData.h"

NSString * MethodByName(NSString * name)
{
    return [NSString stringWithFormat:@"/api/%@", name];
}

NSString *const METHOD_REG              =   @"registration";
NSString *const METHOD_REG_CODE         =   @"registration/code";

NSString *const METHOD_QUESTIONS_CODE   =   @"questions";
NSString *const METHOD_QUESTIONS_LAST   =   @"questions/last";
NSString *const METHOD_QUESTIONS_GEO    =   @"questions/geo";

NSString *const METHOD_USERS            =   @"users";
NSString *const METHOD_USERS_LIST       =   @"users/list";
NSString *const METHOD_FRIENDS_LIST     =   @"users/friends/list";
NSString *const METHOD_FOLLOWERS        =   @"users/friends/followers";
