//  AFNetworkServer.m
//  GoBeside

#import <AFNetworking/AFNetworking.h>

#import "AFNetworkServer.h"

#import "GBSessionManager.h"
#import "GBNotifications.h"

@interface AFNetworkServer ()
{
    NSString *startUrl;
}
//@property (nonatomic, readonly) GBSessionManager *operationManager;
//@property (nonatomic, readonly) GBSessionManager *serviceManager;
@property (nonatomic, readonly) GBSessionManager *operationManager;

@property (nonatomic, strong)   NSURLSessionDataTask * searchTask;

@end

@implementation AFNetworkServer

#pragma mark - Object life cycle

- (instancetype)init
{
    if (self = [super init])
    {
        AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
        [manager startMonitoring];
        
        __weak typeof (self) weakSelf = self;
        
        [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
        {
            weakSelf.networkIsReachable = status == AFNetworkReachabilityStatusReachableViaWiFi || status == AFNetworkReachabilityStatusReachableViaWWAN;
        }];
        
        [self configureManagers];
    }
    
    return self;
}

- (void)configureManagers
{
    _operationManager = [[GBSessionManager alloc] initWithBaseURL:nil];
    
    [_operationManager setRequestConfigurationBlock:^(GBSessionManager *manager)
    {
//        [manager setupAuthorizationHeaderWithField:];
    }];
    
    [_operationManager setErrorHandlingBlock:^(NSURLSessionDataTask *task, NSError *error)
    {
        [GBAlerter showErrorMessage:error];
    }];
}

- (void)setSearchTask:(NSURLSessionDataTask *)searchTask
{
    if (searchTask == _searchTask) return;
    
    [_searchTask cancel];
    _searchTask = searchTask;
}

#pragma mark - Registration
- (void)registrationWithParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
   [self.operationManager POST:MethodByName(METHOD_REG) parameters:params completion:^(id response, NSError *error)
     {
         completion(response[@"response"], error);
     }];
}

- (void)phoneCode:(NSNumber *)userID completionBlock:(GBCompletionBlock)completion
{
    [self.operationManager GET:MethodByName(METHOD_REG_CODE) parameters:userID completion:^(id response, NSError *error)
    {
        completion(response[@"response"], error);
    }];
}

- (void)phoneToken:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    [self.operationManager POST:MethodByName(METHOD_REG_CODE) parameters:params completion:^(id response, NSError *error)
     {
         completion(response[@"response"], error);
     }];
}

#pragma mark - Users

- (void)getUserById:(NSNumber *)sbId completionBlock:(GBCompletionBlock)completion
{
    NSDictionary * params = sbId ? @{@"id":sbId} : nil;
    [self.operationManager GET:MethodByName(METHOD_USERS)
                    parameters:params
     completion:^(id response, NSError *error) {
         NSLog(@"Users response: %@", error.localizedDescription);
         completion(response[@"data"][METHOD_REG], error);
     }];
}

- (void)getUsers:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    NSDictionary *newParams = [params mtl_dictionaryByRemovingValuesForKeys:@[@"params"]];
    
    NSString *url = [NSString stringWithFormat:@"%@?%@",MethodByName(METHOD_USERS_LIST), params[@"params"] ? : @""];
    [self.operationManager GET:url parameters:newParams completion:^(id response, NSError *error) {
        completion(response[@"data"], error);
    }];
}

- (void)updateUser:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    GBSessionManager *manager = [[GBSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];

//    [manager setupAuthorizationHeaderWithField:];

    [manager PUT:MethodByName(METHOD_REG) parameters:params completion:^(id response, NSError *error) {
        completion(response[@"data"][METHOD_REG], error);
    }];
}

#pragma mark - Questions
- (void)getLastQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    if (SECRET_TOKEN)
    {
        [self.operationManager.requestSerializer setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [self.operationManager.requestSerializer setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
    }
    
    [self.operationManager GET:MethodByName(METHOD_QUESTIONS_LAST) parameters:params completion:^(id response, NSError *error)
    {
        completion(response[@"response"], error);
    }];
}

- (void)getGeoQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    [self.operationManager GET:MethodByName(METHOD_QUESTIONS_GEO) parameters:params completion:^(id response, NSError *error)
     {
         completion(response[@"response"], error);
     }];
}

@end
