#import "GBFetch.h"

@implementation GBFetch

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:completion];

    [dataTask resume];
}

- (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)params andURL:(NSString *)link
{
    NSMutableURLRequest *request;
    if (method == postMethod)
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        if (SECRET_TOKEN)
        {
            [request setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
            [request setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
        }

        NSError *err;
        NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:params
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&err];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonDict];
        
        return request;
    }
    else if (method == getMethod)
    {
        NSString *requestString = [NSString stringWithString:link];
        NSArray *k = [params allKeys];
        NSArray *v = [params allValues];
        NSArray *a = [requestString componentsSeparatedByString:@"/"];
        if ([a.lastObject isEqualToString:@"list"]) {
            requestString = [requestString stringByAppendingString:@"?data="];
            requestString = [requestString stringByAppendingString:[NSString
                                                                    stringWithFormat:@"[%@]", params[@"id_user"]]];
        } else {
            requestString = [requestString stringByAppendingString:@"?data={"];
            for (id key in k) {
                NSString *keyString = [NSString stringWithFormat:@"\"%@\":", key];
                NSString *valueString = [NSString stringWithFormat:@"%@", [v objectAtIndex:[k indexOfObject:key]]];
                keyString = [keyString stringByAppendingString:valueString];
                requestString = [requestString stringByAppendingFormat:@"%@", keyString];
                if (key != k.lastObject)
                    requestString = [requestString stringByAppendingString:@","];
            }
            requestString = [requestString stringByAppendingString:@"}"];
        }

        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if (SECRET_TOKEN) {
            [request setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
            [request setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        
        return request;
    }
        else if (method == putMethod)
    {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        if (SECRET_TOKEN)
        {
            [request setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
            [request setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
        }
        
        NSError *err;
        NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:params
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&err];
        [request setHTTPMethod:@"PUT"];
        [request setHTTPBody:jsonDict];
        
        return request;
    }
    else
    {
        request = [NSMutableURLRequest new];
        
        return request;
    }
}

@end
