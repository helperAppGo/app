//  GBResponseSerializer.h
//  GoBeside

#import "AFURLResponseSerialization.h"

extern NSString *const JSONResponseSerializerWithDataKey;

@interface GBResponseSerializer : AFJSONResponseSerializer

@end
