//  GBResponseSerializer.m
//  GoBeside

#import "GBResponseSerializer.h"

@implementation GBResponseSerializer

- (id)responseObjectForResponse:(NSHTTPURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error
{
    NSDictionary *JSONObject = [super responseObjectForResponse:response data:data error:error];
    if (*error)
    {
        NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:(*error).userInfo];
        userInfo[kGBErrorResponseKey] = JSONObject;
        userInfo[kGBErrorResponseStringKey] = JSONObject ? nil : [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        userInfo[kGBErrorResponseDataKey] = data;
        (*error) = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:[userInfo copy]];
    }
    
    NSArray *errors = JSONObject[@"errors"];
    
    if (errors.count)
    {
        NSDictionary *errorDictionary = errors.firstObject;
        NSString *errorCode = errorDictionary[@"code"];
        NSDictionary *info = @{ kGBErrorResponseKey: JSONObject,
                                kGBErrorResponseDataKey : data
                                };
        (*error) = [NSError errorWithDomain:@"Server"
                                       code:errorCode ? [errorCode intValue] : (*error).code
                                   userInfo:info];
    }
    
    if ([response.allHeaderFields[@"Maintenance"] isEqual:@"true"])
    {
        NSMutableDictionary *info = [NSMutableDictionary dictionaryWithDictionary:(*error).userInfo];
        info[kGBErrorMessageKey] = @"Application is on maintenance, please try again later.";
        *error = [NSError errorWithDomain:@"ServerMaintenance" code:999 userInfo:[info copy]];
    }
    
    return (JSONObject);
}

@end
