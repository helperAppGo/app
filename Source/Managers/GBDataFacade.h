//  GBDataFacade.h
//  GoBeside

#import <Foundation/Foundation.h>
#import "AFNetworkServer.h"
#import "GBDataParser.h"
#import "GBFetch.h"


#define DATA_FACADE [GBDataFacade sharedFacade]
#define DATA_HOLDER [[GBDataFacade sharedFacade] dataHolder]

@interface GBDataFacade : NSObject
{
    AFNetworkServer * afServer;
    GBDataParser * dataParser;
    MainDataHolder * dataHolder;
    GBFetch * fetch;
}

@property (nonatomic, strong, readonly) GBUser * currentUser;

+ (GBDataFacade *)sharedFacade;

- (MainDataHolder *)dataHolder;

- (void)registrationWithParams:(NSDictionary *)params completionBlock:(GBVoidBlock)completion;
//- (void)phoneToken;
- (void)phoneCode;

- (void)getUserById:(NSNumber *)sbId completionBlock:(GBCompletionBlockTest)completion;
- (void)getUsers:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;
- (void)updateUser:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;

- (void) uploadFile:(NSData *)data;

- (void)getLastQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;
- (void)getGeoQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion;

@end
