//  GBCache.m
//  GoBeside

#import "GBCache.h"

NSString *GBCacheRootKey = @"_CacheContainer";

NSString *GBCacheKeyScheme = @"GBCacheKeyScheme";
NSString *GBCacheKeyNewsfeed = @"MChacheKeyNewsfeed";
NSString *GBCacheKeyServices = @"GBCacheKeyServices";
NSString *GBCacheKeyProducts = @"GBCacheKeyProducts";
NSString *GBCacheKeyEvents = @"GBCacheKeyEvents";
NSString *GBCacheKeyGroups = @"GBCacheKeyGroups";

@interface GBCache()
@property (nonatomic, copy) NSString *cacheKey;
@property (nonatomic, strong) NSMutableDictionary *mutableContent;

@end

@implementation GBCache

#pragma mark - Class methods

+ (instancetype)cacheWithRootKey:(NSString *)key {
    static NSMutableDictionary *caches = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        caches = [NSMutableDictionary dictionary];
    });
    
    GBCache *cache = caches[key];
    if (cache) return cache;
    
    @synchronized (caches) {
        cache = caches[key];
        if (!cache) {
            cache = [self new];
            cache.cacheKey = key;
            cache.mutableContent = [[[self cacheContainer] dictionaryForKey:key] mutableCopy];
            caches[key] = cache;
        }
    }
    
    return cache;
}


+ (NSUserDefaults *)cacheContainer {
    static NSUserDefaults *cacheContainer = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cacheContainer = [[NSUserDefaults alloc] initWithSuiteName:GBCacheRootKey];
    });

    return cacheContainer;
}

+ (void)resetAllKnown {
    NSArray *keys = @[GBCacheKeyScheme, GBCacheKeyNewsfeed, GBCacheKeyServices, GBCacheKeyProducts, GBCacheKeyEvents, GBCacheKeyGroups];
    for (NSString *key in keys) {
        GBCache *cache = [GBCache cacheWithRootKey:key];
        [cache reset];
    };
}

#pragma mark - Accessors

- (NSDictionary *)content {
    return [self.mutableContent copy];
}

#pragma mark - Public methods

- (void)cacheObject:(NSDictionary *)object {
    self.mutableContent = [NSMutableDictionary dictionaryWithDictionary:object ?: @{}];
    [self synchronize];
}

- (void)synchronize {
    NSDictionary *object = self.content;
    NSUserDefaults *defaults = [GBCache cacheContainer];
    NSString *key = self.cacheKey;
    if (object) {
        [defaults setObject:object forKey:key];
    }
    else {
        [defaults removeObjectForKey:key];
    }
    
    [defaults synchronize];
}

- (void)reset {
    self.mutableContent = nil;
    [self synchronize];
}

#pragma mark - private methods

@end
