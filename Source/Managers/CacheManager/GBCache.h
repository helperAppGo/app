//  GBCache.h
//  GoBeside

#import <Foundation/Foundation.h>

extern NSString *GBCacheKeyScheme;
extern NSString *GBCacheKeyNewsfeed;
extern NSString *GBCacheKeyServices;
extern NSString *GBCacheKeyProducts;
extern NSString *GBCacheKeyEvents;
extern NSString *GBCacheKeyGroups;

@interface GBCache: NSObject
@property (nonatomic, readonly) NSString *cacheKey;
@property (nonatomic, readonly) NSDictionary *content;

+ (instancetype)cacheWithRootKey:(NSString *)key;
+ (void)resetAllKnown;

- (void)cacheObject:(NSDictionary *)object;
- (void)reset;

@end
