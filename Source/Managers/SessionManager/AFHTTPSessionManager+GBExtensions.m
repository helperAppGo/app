//  AFHTTPSessionManager+GBExtensions.m
//  GoBeside

#import "AFHTTPSessionManager+GBExtensions.h"
#import "NSObject+GBToast.h"

@implementation AFHTTPSessionManager (GBExtensions)

#pragma mark - Public methods

- (void)setupAuthorizationHeaderWithField:(NSString *)field
{
    field = [field isEqual:@"(null):(null)"] ? @":" : field;
    NSData *basicAuthCredentials = [field dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64AuthCredentials = [basicAuthCredentials base64EncodedStringWithOptions:(NSDataBase64EncodingOptions)0];
    NSString *credentials = [NSString stringWithFormat:@"Basic %@", base64AuthCredentials];
    [self.requestSerializer setValue:credentials forHTTPHeaderField:@"Authorization"];
}

#pragma mark - Short m-methods

- (NSURLSessionDataTask *)POST:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion
{
    __weak typeof(self) weakSelf = self;
    return [self POST:url
           parameters:parameters
              success:^(NSURLSessionDataTask *task, id response) {
                  [weakSelf task:task endedWithResponse:response error:nil completion:completion];
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  [weakSelf task:task endedWithResponse:nil error:error completion:completion];
              }];
}

- (NSURLSessionDataTask *)GET:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion {
    __weak typeof(self) weakSelf = self;
    return [self GET:url
          parameters:parameters
             success:^(NSURLSessionDataTask *task, id response) {
                 [weakSelf task:task endedWithResponse:response error:nil completion:completion];
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 [weakSelf task:task endedWithResponse:nil error:error completion:completion];
             }];
}

- (NSURLSessionDataTask *)PUT:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion {
    __weak typeof(self) weakSelf = self;
    return [self PUT:url
          parameters:parameters
             success:^(NSURLSessionDataTask *task, id response) {
                 [weakSelf task:task endedWithResponse:response error:nil completion:completion];
             }
             failure:^(NSURLSessionDataTask *task, NSError *error) {
                 [weakSelf task:task endedWithResponse:nil error:error completion:completion];
             }];
}

- (NSURLSessionDataTask *)DELETE:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion
{
    __weak typeof(self) weakSelf = self;
    return [self DELETE:url
             parameters:parameters
                success:^(NSURLSessionDataTask *task, id response) {
                    [weakSelf task:task endedWithResponse:response error:nil completion:completion];
                }
                failure:^(NSURLSessionDataTask *task, NSError *error) {
                    [weakSelf task:task endedWithResponse:nil error:error completion:completion];
                }];
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
     constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> _Nonnull))block
                    completion:(GBCompletionBlock)completion
{
    __weak typeof(self) weakSelf = self;
    return [self        POST:URLString
                  parameters:parameters
   constructingBodyWithBlock:block
                     success:^(NSURLSessionDataTask *task, id response) {
                         [weakSelf task:task endedWithResponse:response error:nil completion:completion];
                     }
                     failure:^(NSURLSessionDataTask *task, NSError *error) {
                         [weakSelf task:task endedWithResponse:nil error:error completion:completion];
                     }];
}

#pragma mark - Private methods

- (void)task:(NSURLSessionDataTask *)task endedWithResponse:(id)response error:(NSError *)error completion:(GBCompletionBlock)completion
{
    if (completion)
    {
        completion(response, error);
    }
}

@end
