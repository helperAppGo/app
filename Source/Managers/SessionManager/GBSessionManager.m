//  GBSessionManager.m
//  GoBeside

#import "GBSessionManager.h"
#import "GBResponseSerializer.h"

typedef void(^MSuccessBlock)(NSURLSessionDataTask *task, id response);
typedef void(^MFailureBlock)(NSURLSessionDataTask *task, NSError *error);

@interface GBSessionManager()
@property (nonatomic, copy) MSuccessBlock success;
@property (nonatomic, copy) MFailureBlock failure;

@end

@implementation GBSessionManager

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url ? : [NSURL URLWithString:BASE_URL]];
    
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    self.responseSerializer = [GBResponseSerializer serializer];
    
    return self;
}

#pragma mark - Public methods

- (void)setRequestSerializer:(AFHTTPRequestSerializer<AFURLRequestSerialization> *)requestSerializer
{
    [super setRequestSerializer:requestSerializer];
    
    if (SECRET_TOKEN)
    {
        [requestSerializer setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
        [requestSerializer setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
    }
    
    requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    
//    [self setupAuthorizationHeaderWithField:@":"];
}

- (void)setResponseSerializer:(AFHTTPResponseSerializer<AFURLResponseSerialization> *)responseSerializer
{
    [super setResponseSerializer:responseSerializer];
    
    responseSerializer.acceptableContentTypes = [responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
}

#pragma mark - Overrided methods

- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nonnull))success
                      failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    [self prepareManagerForRequest];
    
    return [super GET:URLString
           parameters:parameters
              success:^(NSURLSessionDataTask * task, id response) {
                  success(task, response);
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  failure(task, error);
                  [self handleError:error fromTask:task];
              }];
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nonnull))success
                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    [self prepareManagerForRequest];
    
    return [super POST:URLString
            parameters:parameters
               success:^(NSURLSessionDataTask * task, id response) {
                   success(task, response);
               }
               failure:^(NSURLSessionDataTask *task, NSError *error) {
                   failure(task, error);
                   [self handleError:error fromTask:task];
               }];
}

- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
     constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> _Nonnull))block
                       success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nonnull))success
                       failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    [self prepareManagerForRequest];
    
    return [super       POST:URLString
                  parameters:parameters
   constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> _Nonnull))block
                     success:^(NSURLSessionDataTask * task, id response) {
                         success(task, response);
                     }
                     failure:^(NSURLSessionDataTask *task, NSError *error) {
                         failure(task, error);
                         [self handleError:error fromTask:task];
                     }];
}

- (NSURLSessionDataTask *)PUT:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nonnull))success
                      failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    [self prepareManagerForRequest];
    
    return [super PUT:URLString
           parameters:parameters
              success:^(NSURLSessionDataTask * task, id response) {
                  success(task, response);
              }
              failure:^(NSURLSessionDataTask *task, NSError *error) {
                  failure(task, error);
                  [self handleError:error fromTask:task];
              }];
}

- (NSURLSessionDataTask *)DELETE:(NSString *)URLString
                      parameters:(id)parameters
                         success:(void (^)(NSURLSessionDataTask * _Nonnull, id _Nonnull))success
                         failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure
{
    [self prepareManagerForRequest];
    
    return [super DELETE:URLString
              parameters:parameters
                 success:^(NSURLSessionDataTask * task, id response) {
                     success(task, response);
                 }
                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                     failure(task, error);
                     [self handleError:error fromTask:task];
                 }];
}

#pragma mark - Private methods

- (void)prepareManagerForRequest
{
    GBValueBlock block = self.requestConfigurationBlock;
    if (block)
    {
        block(self);
    }
}

- (void)handleError:(NSError *)error fromTask:(NSURLSessionDataTask *)task
{
    if (error.code == 4)
    {
        
    }
    else
    {
        GBCompletionBlock block = self.errorHandlingBlock;
        
        if (block)
        {
            block(task, error);
        }
    }
}

@end
