//  GBSessionManager.h
//  GoBeside

#import <AFNetworking/AFNetworking.h>

#import "AFHTTPSessionManager+GBExtensions.h"

@interface GBSessionManager : AFHTTPSessionManager

@property (nonatomic, copy) GBValueBlock requestConfigurationBlock;
@property (nonatomic, copy) GBCompletionBlock errorHandlingBlock;// task + error

-(NSURLSessionDataTask *)GET:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * , id ))success failure:(void (^)(NSURLSessionDataTask * , NSError * ))failure;
-(NSURLSessionDataTask *)POST:(NSString *)URLString parameters:(id)parameters constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> ))block success:(void (^)(NSURLSessionDataTask * , id ))success failure:(void (^)(NSURLSessionDataTask * , NSError * ))failure;
-(NSURLSessionDataTask *)POST:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * , id ))success failure:(void (^)(NSURLSessionDataTask * , NSError * ))failure;
-(NSURLSessionDataTask *)PUT:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * , id ))success failure:(void (^)(NSURLSessionDataTask * , NSError * ))failure;
-(NSURLSessionDataTask *)DELETE:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * , id ))success failure:(void (^)(NSURLSessionDataTask * , NSError * ))failure;

@end
