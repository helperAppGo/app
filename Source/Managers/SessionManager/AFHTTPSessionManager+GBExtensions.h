//  AFHTTPSessionManager+GBExtensions.h
//  GoBeside

#import <AFNetworking/AFNetworking.h>

@interface AFHTTPSessionManager (GBExtensions)

- (void)setupAuthorizationHeaderWithField:(NSString *)field;

- (NSURLSessionDataTask *)GET:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion;
- (NSURLSessionDataTask *)POST:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion;
- (NSURLSessionDataTask *)PUT:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion;
- (NSURLSessionDataTask *)DELETE:(NSString *)url parameters:(id)parameters completion:(GBCompletionBlock)completion;
- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
     constructingBodyWithBlock:(void (^)(id<AFMultipartFormData>))block
                    completion:(GBCompletionBlock)completion;
@end
