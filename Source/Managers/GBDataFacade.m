//  GBDataFacade.m
//  GoBeside

#import "GBDataFacade.h"
#import "GBResponse.h"
#import "GBCache.h"

NSString *const ERROR_DOMAIN = @"ERROR_DOMAIN.com";

@interface GBDataFacade ()
@end

@implementation GBDataFacade

@dynamic currentUser;

+ (id)sharedFacade
{
    static GBDataFacade *_dataFacade;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dataFacade = [[super alloc] init];
    });
    
    return _dataFacade;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        afServer = [[AFNetworkServer alloc] init];
        dataHolder = [[MainDataHolder alloc] init];
        dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
        fetch = [GBFetch alloc];
    }
    return self;
}

- (MainDataHolder *)dataHolder
{
    return dataHolder;
}

- (void)handleResponse:(GBResponse *)response error:(NSError *)error completion:(GBCompletionBlock)completion
{
    if (completion)
    {
        completion(error ? nil : response, error);
    }
}

#pragma mark - Reg/Login
- (void)registrationWithParams:(NSDictionary *)params completionBlock:(GBVoidBlock)completion {
/*
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];

    if (SECRET_TOKEN)
    {
        [manager.requestSerializer setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
    }
    
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *path = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_REG];

    [manager POST:path parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
    {
        NSLog(@"operation - %@", operation);
        NSLog(@"responseObject - %@", responseObject);
        
        [GBCommonData setUserId:responseObject[@"response"][@"Id_user"]];
        [self phoneCode];
        completion();
        
    }
          failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
    {
        NSLog(@"operation - %@", operation);
        NSLog(@"error - %@", error);
    }];
 */
}

- (void)phoneCode
{
    NSDictionary *params = @{@"Id_user": USER_ID};
    
    NSURLRequest *request = [fetch makeRequestWithMethod:getMethod params:params
                                                  andURL:[NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_REG_CODE]];
    
    [fetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *response, NSError *error)
    {
        if (data && !error)
        {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:0
                                                                           error:nil];
            if (responseDict[@"response"] != nil) {
                NSNumber *phone_code = responseDict[@"response"][@"phone_code"];
                NSLog(@"Code: %@", phone_code);
                [GBCommonData setPushToken: phone_code];
            }else {
                NSLog(@"%@", responseDict[@"error"]);
            }
        }
        else
        {
            NSLog(@"%@", error);
        }
    }];
}

//- (void)phoneToken {
//    NSDictionary *params = @{@"id_user" : USER_ID,
//                              @"phone_code": PUSH_TOKEN};
//    
//}

#pragma mark - User
- (void)getUserById:(NSNumber *)sbId completionBlock:(GBCompletionBlockTest)completion {
    NSString *path = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_USERS_LIST];
    NSDictionary *params = @{@"id_user":sbId.stringValue};
    
    NSURLRequest *r = [fetch makeRequestWithMethod:getMethod params:params andURL:path];
    [fetch getDataWithRequest:r andCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        completion(data, response, error);
        [self getFollowersWithParams:@{@"id_user": sbId,
                                       @"limit"  : @25,
                                       @"offset" : @0,
                                       @"random" : @0}];
        
        [self getFriendsWithParams:@{@"id_user": sbId,
                                     @"limit"  : @25,
                                     @"offset" : @0,
                                     @"random" : @0}];
    }];
}

- (void)getUsers:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    /*
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.requestSerializer setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
    [manager.requestSerializer setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
    
    NSString *pathForUser = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_USERS_LIST];
    
    [manager GET:pathForUser parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject) {

        }
         failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error) {
             NSLog(@"%@", error);
         }];
     */
}

- (void)updateUser:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    if (![self checkForReachability:completion]) return;
    
    if (!params) return;
    
    __weak typeof(self) weakSelf = self;
    [afServer updateUser:@{@"update":params} completionBlock:^(id resObject, NSError *error)
    {
        if (!error) {
            resObject = [[GBUser alloc] initWithDictionary:resObject[@"user"] error:&error];
            DLogIf(resObject == nil, @"Failed to parse user model (1): %@",error);
            dAssert(resObject != nil, @"Failed to parse user model (1): %@",error);
        }
        
        [weakSelf handleResponse:resObject error:error completion:(completion)];
    }];
}

- (void)getFollowersWithParams:(NSDictionary *)params {
    NSString *path = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_FOLLOWERS];
    NSURLRequest *r = [fetch makeRequestWithMethod:getMethod params:params andURL:path];
    [fetch getDataWithRequest:r andCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:0
                                                                       error:nil];
        NSLog(@"%@", responseDict);
    }];
}

- (void)getFriendsWithParams:(NSDictionary *)params {
    NSString *path = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_FRIENDS_LIST];
    NSURLRequest *r = [fetch makeRequestWithMethod:getMethod params:params andURL:path];
    [fetch getDataWithRequest:r andCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                     options:0
                                                                       error:nil];
        NSLog(@"%@", responseDict);
    }];
}

#pragma mark - Questions

- (void)getLastQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
//    __weak typeof(self) weakSelf = self;
//    [afServer getLastQuestionsByParams:params completionBlock:^(id resObject, NSError *error)
//    {
//        if (!error)
//        {
//            NSArray *lastQuestionsAr = [dataParser parseLastQuestions:resObject];
//            resObject = @{@"questions":lastQuestionsAr};
//        }
//        [weakSelf handleResponse:resObject error:error completion:completion];
//    }];
    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    
//    if (SECRET_TOKEN)
//    {
//        [manager.requestSerializer setValue:SECRET_TOKEN forHTTPHeaderField:@"X-PHONE-TOKEN"];
//        [manager.requestSerializer setValue:USER_ID.stringValue forHTTPHeaderField:@"X-USER"];
//    }
//    
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    NSString *path = [NSString stringWithFormat:@"%@/api/%@", BASE_URL, METHOD_QUESTIONS_LAST];
//    
//    [manager POST:path parameters:params success:^(AFHTTPRequestOperation * _Nonnull operation, id  _Nonnull responseObject)
//     {
//         NSLog(@"operation - %@", operation);
//         NSLog(@"responseObject - %@", responseObject);
//         
////         [GBCommonData setUserId:responseObject[@"response"][@"Id_user"]];
////         [self phoneCode];
//     }
//          failure:^(AFHTTPRequestOperation * _Nullable operation, NSError * _Nonnull error)
//     {
//         NSLog(@"operation - %@", operation);
//         NSLog(@"error - %@", error);
//     }];
    
    NSURLRequest *request = [fetch makeRequestWithMethod:postMethod params:params andURL:path];
    
    [fetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *response, NSError *error)
     {
         if (data && !error)
         {
             NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                          options:0
                                                                            error:nil];
             NSLog(@"responseDict - %@", responseDict);
         }
         else
         {
             NSLog(@"%@", error);
         }
     }];

}

- (void)getGeoQuestionsByParams:(NSDictionary *)params completionBlock:(GBCompletionBlock)completion
{
    __weak typeof(self) weakSelf = self;
    [afServer getGeoQuestionsByParams:params completionBlock:^(id resObject, NSError *error)
     {
         if (!error)
         {
             NSArray *geoQuestionsAr = [dataParser parseGeoQuestions:resObject];
             resObject = @{@"questions":geoQuestionsAr};
         }
         [weakSelf handleResponse:resObject error:error completion:completion];
     }];
}

#pragma mark - Reachability

- (BOOL)checkForReachability:(GBCompletionBlock)completion
{
    if (!afServer.networkIsReachable && completion)
    {
        if (completion)
        {
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey: @"Network not reachable"};
            NSError *error = [NSError errorWithDomain:ERROR_DOMAIN code:666 userInfo:userInfo];
            completion(nil, error);
        }
    }
    return afServer.networkIsReachable;
}

@end
