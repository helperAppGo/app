//
//  GBNotAdminGroupController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/15/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBNotAdminGroupController.h"
#import "GBPostTableViewCell.h"

#define getApiGroup             [BASE_URL stringByAppendingString:@"/api/groups"]
#define getGroupPostsList       [BASE_URL stringByAppendingString:@"/api/groups/posts/list"]

static NSString *wallIdentifier = @"GBtableViewCell";

@interface GBNotAdminGroupController ()

//@property (strong, nonatomic) UIView *head;

@end

@implementation GBNotAdminGroupController {
    NSArray *jsonGroupInside;
    NSMutableDictionary *paramDic;
    NSArray *jsonGIS;
    NSNumber *isMember;
    
    NSInteger currentWall;
    NSMutableDictionary *paramDicWall;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refresh];
    
// - FOR WALL ------------
    currentWall = 0;
    paramDicWall = [[NSMutableDictionary alloc] init];
    [self getWallInfo:^{}];
    
    
//    // - SET CUSTOM BACK BUTTON ------------
//    UIImage *image = [UIImage imageNamed:@"left-arrow-white"];
//    
//    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//    button.bounds = CGRectMake(0, 0, 20, 20);
//    button.layer.shadowOpacity = 0.8;
//    button.layer.shadowColor = [UIColor blackColor].CGColor;
//    button.layer.shadowOffset = CGSizeMake(0.5, 1.0);
//    [button setImage:image forState:UIControlStateNormal];
//    [button addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//    self.navigationItem.leftBarButtonItem = barButtonItem;
//    
//    
    // - SET IMAGE SIZE N GROUP -------------
//    _image.contentMode = UIViewContentModeScaleAspectFill;
    
    
    // - MAKE NAVIGATION CONTROLLER TRANSAPEND --------------------
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    DLog(@"ID GROUP: %@", self.idGroup);
    dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
   
    [self getInfoGroup];
    
}

//- (void)viewWillDisappear:(BOOL)animated {
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = nil;
//    self.navigationController.navigationBar.tintColor = nil;
//    self.navigationController.navigationBar.translucent = false;
//    
//    [super viewWillDisappear:animated];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) refresh {
    
    __weak typeof(self) weakSelf = self;
    
    // Create custom indicator
    CGRect indicatorRect;
    
#if TARGET_OS_TV
    indicatorRect = CGRectMake(0, 0, 64, 64);
#else
    indicatorRect = CGRectMake(0, 0, 24, 24);
#endif
    
    CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
    
    // Set custom indicator
    self.tableView.infiniteScrollIndicatorView = indicator;
    
    // Set custom indicator margin
    self.tableView.infiniteScrollIndicatorMargin = 40;
    
    // Set custom trigger offset
    self.tableView.infiniteScrollTriggerOffset = 0;
    
    [self.tableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
        [weakSelf getWallInfo:^{
            // Finish infinite scroll animations
            [tableView finishInfiniteScroll];
        }];
    }];
    
    [self.tableView beginInfiniteScroll:NO];
}


//- (void)backButtonClicked {
//    DLog(@"going back");
//    [self.navigationController popViewControllerAnimated:YES];
//}


//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:YES];
//    
//    for (id item in self.childViewControllers) {
//        if ([item isKindOfClass:[GBWallController class]]) {
//            GBWallController *controller = item;
//            CGFloat height =  controller.jsonGroup.count *242;
//            [self.wallHeight setConstant:height];
//        }
//    }
//}


-(void) getWallInfo:(void(^)(void))completion {
    DLog(@"GO TO GROUPS");
    
    paramDicWall = [@{@"OffSet":[NSString stringWithFormat:@"%li", (long)currentWall],
                 @"Limit": @"25",
                 @"Id":[NSString stringWithFormat:@"%@", self.idGroup]} mutableCopy];
    
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodGroupsPostsList params:paramDicWall andURL:getGroupPostsList];
    DLog(@"Request url: %@", request);
    DLog(@"HTTP BODY: %@", request.HTTPBody);
    DLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            NSArray *newGroup = [dataParser parseGroupPostList:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            
            if (self.modelArray == nil) {
                self.modelArray = [[[newGroup reverseObjectEnumerator] allObjects] mutableCopy];
            } else {
                [self.modelArray addObjectsFromArray:[[newGroup reverseObjectEnumerator] allObjects]];
            }
        }
//                if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
//                    DLog(@"Response HTTP Status code: %ld\n", (long) [(NSHTTPURLResponse *) urlResponse statusCode]);
//                    DLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *) urlResponse allHeaderFields]);
//                }
        
//                NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//                DLog(@"Responcse Body:\n%@\n", body);
//        
//                jsonGISWall = [NSJSONSerialization JSONObjectWithData:data
//                                                          options:kNilOptions
//                                                            error:nil];
//                DLog(@"JSON ARRAY - %@", jsonGISWall);
        
        
        DLog(@"WALL RESPONSE:%@", self.modelArray);
        
        [self.tableView reloadData];
        
        currentWall += 25;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            DLog(@"otpavivli update table");
            
            [[UIApplication sharedApplication] stopNetworkActivity];
            
            if(completion) {
                completion();
            }
        });
    }];
}



-(void) getInfoGroup
{
    DLog(@"GO TO GROUPS");
    WEAKSELF_DECLARATION
    
    paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", self.idGroup],@"id",@"0",@"number", nil];
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodApiGroup params:paramDic andURL:getApiGroup];
    DLog(@"Request url: %@", request);
    DLog(@"HTTP BODY: %@", request.HTTPBody);
    DLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            jsonGIS = [NSJSONSerialization JSONObjectWithData:data
                                                      options:kNilOptions
                                                        error:nil];
            DLog(@"JSON ARRAY - %@", jsonGIS);
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            DLog(@"otpavivli update table");
            STRONGSELF_DECLARATION
            // NAME GROUP -------
            
            self.nameGroup.layer.shadowOpacity = 0.8;
            self.nameGroup.layer.shadowColor = [UIColor blackColor].CGColor;
            self.nameGroup.layer.shadowOffset = CGSizeMake(0.5, 1.0);
            
            self.nameGroup.text = [[jsonGIS valueForKey:@"response"] valueForKey:@"name"];
            DLog(@"NAME: %@", self.nameGroup.text);
            
            // TYPE GROUP -------
            NSNumber *type = [[jsonGIS valueForKey:@"response"] valueForKey:@"type"];
            
            NSNumber *zero = @(0);
            NSNumber *one = @(1);
            NSNumber *two = @(2);
            NSNumber *three = @(3);
            
            self.typeGroup.layer.shadowOpacity = 0.8;
            self.typeGroup.layer.shadowColor = [UIColor blackColor].CGColor;
            self.typeGroup.layer.shadowOffset = CGSizeMake(0.5, 1.0);
            
            if ([type isEqualToNumber:zero]) {
                self.typeGroup.text = @"Закрита група";
            } else if ([type isEqualToNumber:one]) {
                self.typeGroup.text = @"Відкрита група";
            } else if ([type isEqualToNumber:two]){
                self.typeGroup.text = @"Публічна сторінка";
            } else if ([type isEqualToNumber:three]) {
                self.typeGroup.text = @"Афіша";
            } else {
                self.typeGroup.text = @"";
            }
            DLog(@"TYPE: %@", self.typeGroup.text);
            
            // IMAGE GROUP --------
            NSString *groupAvatar = [[jsonGIS valueForKey:@"response"] valueForKey:@"path_cover"];
            
            if ([groupAvatar isEqualToString:@""]) {
                self.image.image = [UIImage imageNamed:@"camera.png"];
            } else {
                NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:groupAvatar]];
                // self.path_ava.layer.cornerRadius = self.path_ava.frame.size.width/2;
                // self.path_ava.layer.masksToBounds = YES;
                self.image.image = [UIImage imageWithData:imageData];
                DLog(@"IMAGE: %@", self.image.image);
            }
            
            // STATUS GROUP ---------
            
            NSNumber *typeS = [[jsonGIS valueForKey:@"response"] valueForKey:@"type"];
            isMember = [[jsonGIS valueForKey:@"response"] valueForKey:@"is_member"];
            
            if ([isMember isEqualToNumber:zero]){
                self.iconStatusGroup.image = [UIImage imageNamed:@"ic_follow"];
                
                if ([typeS isEqualToNumber:zero]) {
                    self.statusGroup.text = @"Подать заявку";
                } else if ([typeS isEqualToNumber:one]) {
                    self.statusGroup.text = @"Вступить в групу";
                } else if ([typeS isEqualToNumber:two]){
                    self.statusGroup.text = @"Подписаться";
                } else if ([typeS isEqualToNumber:three]) {
                    self.statusGroup.text = @"Афіша";
                } else {
                    self.statusGroup.text = @"error";
                }
                DLog(@"TYPE:%@, MEMBER:%@", self.statusGroup.text, isMember);
            } else {
                self.iconStatusGroup.image = [UIImage imageNamed:@"ic_folling"];
                
                if ([typeS isEqualToNumber:zero]) {
                    self.statusGroup.text = @"Отписаться";
                } else if ([typeS isEqualToNumber:one]) {
                    self.statusGroup.text = @"Выйти с групы";
                } else if ([typeS isEqualToNumber:two]){
                    self.statusGroup.text = @"Отписаться";
                } else if ([typeS isEqualToNumber:three]) {
                    self.statusGroup.text = @"Афіша";
                } else {
                    self.statusGroup.text = @"error";
                }
                DLog(@"TYPE:%@, MEMBER:%@", self.statusGroup.text, isMember);
            }
            
            // COUNT PEOPLE ---------
            NSNumber *countPeople = [NSNumber numberWithInteger: [[[jsonGIS valueForKey:@"response"] valueForKey:@"count_followers"] integerValue]];
            
            if (([countPeople integerValue] % 100 >= 11) && ([countPeople integerValue] % 100 <= 19)){
                self.countPeople.text = [NSString stringWithFormat:@"%@ подписчиков",[[jsonGIS valueForKey:@"response"] valueForKey:@"count_followers"]];
            }else {
                
                if ([countPeople integerValue] % 10 == 1) {
                    self.countPeople.text = [NSString stringWithFormat:@"%@ подписчик",[[jsonGIS valueForKey:@"response"] valueForKey:@"count_followers"]];
                    
                } else if ([countPeople integerValue] % 10 == 2 ||
                           [countPeople integerValue] % 10 == 3 ||
                           [countPeople integerValue] % 10 == 4) {
                    self.countPeople.text = [NSString stringWithFormat:@"%@ подписчика",[[jsonGIS valueForKey:@"response"] valueForKey:@"count_followers"]];
                    
                } else {
                    self.countPeople.text = [NSString stringWithFormat:@"%@ подписчиков",[[jsonGIS valueForKey:@"response"] valueForKey:@"count_followers"]];
                }
            }
            DLog(@"COUNT: %@", self.countPeople.text);
            
            // ADDRESS --------------
            self.adress.text = [[jsonGIS valueForKey:@"response"] valueForKey:@"address"];
            DLog(@"ADDRESS: %@", self.adress.text);
            
            // WEB SITE -------------
            self.webSite.text = [[jsonGIS valueForKey:@"response"] valueForKey:@"web_site"];
            DLog(@"WEB SITE: %@", self.webSite.text);
            
            // DESCRIPTION ----------
            self.descriptionGroup.text = [[jsonGIS valueForKey:@"response"] valueForKey:@"short_description"];
            
            [self hideLocationView];
            
            [strongSelf showMainStaticView];
        });
    }];
}

// - HIDE VIEW ----------
- (void) hideLocationView {
    WEAKSELF_DECLARATION
    dispatch_async(dispatch_get_main_queue(), ^{
        STRONGSELF_DECLARATION
        if ([strongSelf.adress.text isEqualToString:@""]) {
            strongSelf.addressView.hidden = YES;
            strongSelf.addressHight.constant = 0;
        
            dispatch_async(dispatch_get_main_queue(), ^{
                STRONGSELF_DECLARATION
                //This code will run in the main thread:
                CGRect frame = strongSelf.mainStaticView.frame;
                frame.size.height = strongSelf.mainStaticView.frame.size.height - 44;
                strongSelf.mainStaticView.frame = frame;
            });
        }
    
        if ([strongSelf.webSite.text isEqualToString:@""]) {
            strongSelf.webView.hidden = YES;
            strongSelf.webHight.constant = 0;
        
            dispatch_async(dispatch_get_main_queue(), ^{
                STRONGSELF_DECLARATION
                //This code will run in the main thread:
                CGRect frame = strongSelf.mainStaticView.frame;
                frame.size.height = strongSelf.mainStaticView.frame.size.height - 44;
                strongSelf.mainStaticView.frame = frame;
            });
        }
    });
}

- (IBAction)isMemberButton:(id)sender {
    DLog(@"CLICK MEMBERS");
    
    NSNumber *zero = [NSNumber numberWithInt:0];
    //    NSNumber *one = [NSNumber numberWithInt:1];
    
    if ([isMember isEqualToNumber:zero]) {
        
        [[GBServerManager sharedManager] joinGroupId:[self.idGroup longLongValue] success:^(id responseObject) {
            DLog(@"info: %@", responseObject);
            DLog(@"JOIN");
            
            [self getInfoGroup];
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:@"Welcome to the best community"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"error: %@, userInfo: %@", error, userInfo);
        }];
        
    } else {
        
        [[GBServerManager sharedManager] unfollowGroupId:[self.idGroup longLongValue] success:^(id responseObject) {
            DLog(@"info: %@", responseObject);
            DLog(@"UNFOLLOW");
            
            [self getInfoGroup];
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                           message:@"Good luck"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"error: %@, userInfo: %@", error, userInfo);
        }];
    }
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DLog(@"JSON WALL COUNT: %lu", (unsigned long)self.modelArray.count);
    DLog(@"OFF SET = %ld", (long)currentWall);
    return [self.modelArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GBPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[GBPostTableViewCell cellIdentifier]];
    GBWallModel *wall = self.modelArray[indexPath.row];
    [cell configureWithWallItem:wall delegate:self];
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 232.0;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GBPostTableViewCell *wallcell = [tableView cellForRowAtIndexPath:indexPath];
    [self openCommentsForStreamItemId:wallcell.streamItemId];
}

#pragma mark -

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
        [segue.identifier isEqual:@"goToListMembers"];
        GBListMembers *members = segue.destinationViewController;
        members.idGroupForMembers = self.idGroup;
}


- (IBAction)countPeopleButton:(id)sender {
    DLog(@"CLICK COUNT BUTTON");
}
@end

