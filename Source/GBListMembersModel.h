//
//  GBListMembersModel.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBListMembersModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *idMembers;
@property (nonatomic, strong) NSString *first_name;
@property (nonatomic, strong) NSString *last_name;
@property (nonatomic, strong) NSNumber *id_city;
@property (nonatomic, strong) NSNumber *file_ava;
@property (nonatomic, strong) NSURL *path_ava;
@property (nonatomic, strong) NSNumber *file_cover;
@property (nonatomic, strong) NSString *path_cover;
@property (nonatomic, strong) NSString *lang;
@property (nonatomic, strong) NSString *b_day;
@property (nonatomic, strong) NSString *b_month;
@property (nonatomic, strong) NSString *b_year;
@property (nonatomic, strong) NSString *status;
@property (nonatomic, strong) NSString *info;
@property (nonatomic, strong) NSNumber *sex;
@property (nonatomic, assign) BOOL is_online;
@property (nonatomic, strong) NSDate *time_disconnected;

+(NSArray *)getObjectsFromArray:(NSArray *)data;
+(GBListMembersModel *)ListMembersFromDictionary:(NSDictionary *)data;
+(void)changesHeightForAspectRatio;

@end

//{"id":458,"first_name":"Mememe","last_name":"Shka","id_city":1,"file_ava":114,"path_ava":"http://helper.darx.net/./files/26e55af62e969ce5/29923086e2cef193/d6e0f7d131cfadf7/5acc506114db2610.jpg","file_cover":0,"path_cover":"","lang":"ru","b_day":20,"b_month":8,"b_year":1990,"status":"","info":"","sex":1,"is_online":false,"time_disconnected":"0001-01-01T00:00:00Z"},
