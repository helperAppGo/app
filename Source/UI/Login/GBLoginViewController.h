#import <UIKit/UIKit.h>

typedef void (^responseBlock) (NSData *data, NSURLResponse *response, NSError *error);

@interface GBLoginViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *surnameField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *codeField;
@property (weak, nonatomic) IBOutlet UITextField *cityField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sexSegmentedControl;
@property (assign, nonatomic) int64_t userIdInt;
@property (assign, nonatomic) int64_t userPhone;
@property (nonatomic) NSUInteger phoneCodeInt;
@property (assign, nonatomic) BOOL isNewUser;

- (IBAction)onLogin:(id)sender;
- (IBAction)doneNewUserAction:(id)sender;



@end
