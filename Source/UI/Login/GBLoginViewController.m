#import "GBLoginViewController.h"
#import "TabViewController.h"
#import "GBChooseCityViewController.h"

#import "GBDataFacade.h"
#import "GBServerManager.h"
#import "GBContactsManager.h"

#import <AudioToolbox/AudioToolbox.h>

@interface GBLoginViewController () <UITextFieldDelegate>{
    NSString *_tokenString;
    NSInteger _userId;
    BOOL _isNew;
}

@property (nonatomic, strong) GBCityModel *city;

@end

@implementation GBLoginViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (SECRET_TOKEN && !self.isNewUser) {
        self.isNewUser =0;
        [GBCityModel refreshCitiesDataWithCompletion:^(BOOL result) {}];
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.cityField.layer.borderColor = [UIColor redColor].CGColor;
    self.nameField.layer.borderColor = [UIColor redColor].CGColor;
    self.surnameField.layer.borderColor = [UIColor redColor].CGColor;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (self.navigationController)
        [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.codeField.text = (self.phoneCodeInt == 0) ? @"" : [NSString stringWithFormat:@"%lu", (unsigned long)self.phoneCodeInt];
}


#pragma mark - IBActions

- (IBAction)onLogin:(id)sender
{
    /*
     
     380973159485
     sms kod = 93880
     
     */
    NSString *phoneString = [self.phoneField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (phoneString.length != 12) return;
    
    int64_t phoneStringToInt = [phoneString longLongValue];
    DLog(@"Send phone: %@ %@", self.phoneField.text, phoneString);
 
    WEAKSELF_DECLARATION
    [SVProgressHUD show];
    [[GBServerManager sharedManager] registrationWithPhone:phoneStringToInt  success:^(id responseObject) {
        STRONGSELF_DECLARATION
        DLog(@"registrationWithPhone %lld / response:\n%@", phoneStringToInt,responseObject);
        NSNumber* phoneNumber = nil;
        @try {
            phoneNumber = responseObject[@"response"][@"phone"];
        }
        @catch (NSException *exception) {
           DLog(@"%s:%d error: %@", __func__, __LINE__, exception);
        }

        if (phoneNumber) {
            strongSelf.userPhone = [phoneNumber longLongValue];
            if (kProductionVersion) {
                [SVProgressHUD dismiss];
                [self openCodeViewController:0];
            } else{
                __weak typeof(self) weakSelf = self;
                [[GBServerManager sharedManager] registrationGetCodeForUserId:strongSelf.userPhone success:^(id responseObject) {
                    [SVProgressHUD dismiss];
                    STRONGSELF_DECLARATION
                    DLog(@"info: %@", responseObject);
                    NSNumber* phoneCode = nil;
                    @try {
                        phoneCode = responseObject[@"response"][@"phone_code"];
                    }
                    @catch (NSException *exception) {
                        DLog(@"%s:%d error: %@", __func__, __LINE__, exception);
                    }
                    if (phoneCode) {
                        strongSelf.phoneCodeInt = [phoneCode unsignedIntegerValue];
                    }
                    [weakSelf openCodeViewController:[phoneCode integerValue]];
                    
                } failure:^(NSError *error, NSDictionary *userInfo) {
                    DLog(@"error: %@, userInfo: %@", error, userInfo);
                    [SVProgressHUD showErrorWithStatus:nil];
                }];

            }
        } else {
            strongSelf.phoneField.text = @"";
            strongSelf.phoneField.placeholder = @"Error!";
            [SVProgressHUD showErrorWithStatus:nil];
        }
        
    } failure:^(NSError *error, NSDictionary *userInfo) {
        STRONGSELF_DECLARATION
        strongSelf.phoneField.text = @"";
        strongSelf.phoneField.placeholder = @"Error!";
        [SVProgressHUD showErrorWithStatus:nil];
    }];
    
}
-(void)openCodeViewController:(NSInteger )code{
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
    GBLoginViewController *codeLogin = [st instantiateViewControllerWithIdentifier:@"codeViewController"];
    codeLogin.userPhone = self.userPhone;
    codeLogin.phoneCodeInt = code;
    [[[UIApplication sharedApplication] delegate] window].rootViewController = codeLogin;
//    [NAVIGATION_MANAGER setRootControllerWithAnimation:codeLogin];
}

- (IBAction)doneNewUserAction:(id)sender {
    
    BOOL nameOk = FALSE;
    BOOL cityOK = self.city != nil;
    BOOL surname = FALSE;
    
    if (self.nameField.text.length>2) {
        nameOk = TRUE;
        self.nameField.layer.borderWidth = 0.0;
    }else{
        self.nameField.layer.borderWidth = 0.5;
    }
    
    if (self.surnameField.text.length>2){
        surname = TRUE;
        self.surnameField.layer.borderWidth = 0.0;
    }else{
        self.surnameField.layer.borderWidth = 0.5;
    }
    
    if (self.city) {
        self.cityField.layer.borderWidth = 0.0;
    }else{
        self.cityField.layer.borderWidth = 0.5;
    }
    
    if(nameOk && cityOK && surname){
        NSMutableDictionary *parameters = @{}.mutableCopy;
        
        [parameters setObject:self.nameField.text forKey:@"first_name"];
        [parameters setObject:self.surnameField.text  forKey:@"last_name"];
        [parameters setObject:@(self.city.objId) forKey:@"id_city"];
        [parameters setObject:@(self.sexSegmentedControl.selectedSegmentIndex) forKey:@"sex"];
        
        __weak typeof(self) weakSelf = self;
        [weakSelf showLoader];
        [[ApiManager sharedManager] patchRequest:@"users/self" params:parameters callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                [weakSelf getUser];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
        
        
        
//        [SVProgressHUD show];
//        WEAKSELF_DECLARATION
//        [[GBServerManager sharedManager] registerUserPersonalInfo:self.nameField.text
//                                                         sureName:self.surnameField.text
//                                                             city:self.city.objId
//                                                              sex:self.sexSegmentedControl.selectedSegmentIndex
//                                                          success:^(id responseObject) {
//                                                              [SVProgressHUD dismiss];
//                                                              DLog(@"response of user Patch: %@", responseObject);
//                                                              [GBUser refreshUserDataWithCompletion:^(BOOL result) {
//                                                                  if (result) {
//                                                                      STRONGSELF_DECLARATION
//                                                                      GBUser *user = CURRENT_USER;
//                                                                      user.city = strongSelf.city;
//                                                                      [GBCommonData setUser:user];
//                                                                      [strongSelf push];
//                                                                      [[GBContactsManager sharedManager] sync:YES];
//                                                                  }else{
//                                                                      [SVProgressHUD showErrorWithStatus:nil];
//                                                                  }
//                                                              }];
//
//
//
//        } failure:^(NSError *error, NSDictionary *userInfo) {
//            DLog(@"Error %@", error);
//            [SVProgressHUD showErrorWithStatus:nil];
//        } ];

        
        //[self push];
    }
    else{
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}
-(void)sendCode{
    NSMutableDictionary *parameters = @{}.mutableCopy;
    parameters[@"phone_code"] = @([self.codeField.text integerValue]);
    parameters[@"phone"] = @(self.userPhone);
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    NSString *path = kProductionVersion ? @"reg/code" : @"registration/code";
    [[ApiManager sharedManager] postRequest:path params:parameters callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            _isNew = [response[@"is_new"] integerValue] == 1;
            _userId = [response[@"id_user"] integerValue];
            [CDUser createUserRegisterResponse:response];
            [weakSelf getUser];
            
        } else{
            weakSelf.codeField.text = @"";
            weakSelf.codeField.placeholder = @"Код не вiрний";
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getUser{
    
    NSDictionary *params= @{@"data":@(_userId)};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    
    [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            if(_isNew){
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
                GBLoginViewController *nameViewController = [storyboard instantiateViewControllerWithIdentifier:@"nameViewController"];
                UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:nameViewController];
                [nc setNavigationBarHidden:YES animated:NO];
                [[[UIApplication sharedApplication] delegate] window].rootViewController = nc;
//                [NAVIGATION_MANAGER setRootControllerWithAnimation:nc];
                
            } else{
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] openTabbar];
//                [NAVIGATION_MANAGER openMainAppInitializer];
                [CDUser currentUserUpdate:response];
//                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//                UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TabViewController"];
//                [NAVIGATION_MANAGER setRootControllerWithAnimation:controller];
            }

        }else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
    
    
//    [[GBServerManager sharedManager] userById:(int)self.userIdInt success:^(id responseObject) {
//        DLog(@"info: %@", responseObject);
//        NSDictionary* userInfo = nil;
//        @try {
//            userInfo = responseObject[@"response"];
//
//        }
//        @catch (NSException *exception) {
//            DLog(@"%s:%d error: %@", __func__, __LINE__, exception);
//        }
//        if (userInfo) {
//            NSError *error = nil;
//            GBUser * user = [[GBUser alloc] initWithDictionary:userInfo error:&error];
//            dAssert(user != nil, @"Failed to parse user model (3): %@",error);
//            [GBCommonData setUser:user];
//        }
//        if(self.isNewUser){
//
//            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
//            GBLoginViewController *nameViewController = [storyboard instantiateViewControllerWithIdentifier:@"nameViewController"];
//            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:nameViewController];
//            [nc setNavigationBarHidden:YES animated:NO];
//
//            [NAVIGATION_MANAGER setRootControllerWithAnimation:nc];
//        } else{
//            [self push];
//        }
//
//
//
//    } failure:^(NSError *error, NSDictionary *userInfo) {
//        DLog(@"error: %@, userInfo: %@", error, userInfo);
//    }];

}

- (IBAction)getLogedIn:(id)sender {
    if ([self.codeField.placeholder isEqualToString: @"Код не вiрний"]) {
        UIStoryboard *st = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
        GBLoginViewController *loginViewController = [st instantiateViewControllerWithIdentifier:@"loginViewController"];
        [[UIApplication sharedApplication] delegate].window.rootViewController = loginViewController;
//        [NAVIGATION_MANAGER setRootControllerWithAnimation:loginViewController];
    } else {
        [self sendCode];
        
        /*
        DLog(@"%d %lu", [self.codeField.text intValue], (unsigned long)self.userPhone);
        
        [[GBServerManager sharedManager] registrationSendCode:[self.codeField.text intValue] userId:self.userPhone success:^(id responseObject) {
            DLog(@"file: %@", responseObject);
            NSString* phoneToken = nil;
            BOOL isNew = 0;
            @try {
                phoneToken = responseObject[@"response"][@"phone_token"];
//                self.isNewUser = responseObject[@"response"][@"is_new"];
                NSInteger isNewResponceIndicator = [responseObject[@"response"][@"is_new"] integerValue] ;
                if(isNewResponceIndicator == 1){
                    isNew = TRUE;
                }
                NSInteger userID = [responseObject[@"response"][@"id_user"] integerValue];
                self.userIdInt = userID;
                DLog(@"%i", isNew);
                self.isNewUser = isNew;
            }
            @catch (NSException *exception) {
                DLog(@"%s:%d error: %@", __func__, __LINE__, exception);
            }
            [GBCommonData setToken:phoneToken];
            [GBCommonData setUserId:@(self.userIdInt)];
            DLog(@"UserIDToRequest:%ld", (long)self.userIdInt);
            [[GBServerManager sharedManager] userById:(int)self.userIdInt success:^(id responseObject) {
                DLog(@"info: %@", responseObject);
                NSDictionary* userInfo = nil;
                @try {
                    userInfo = responseObject[@"response"];
                    
                }
                @catch (NSException *exception) {
                    DLog(@"%s:%d error: %@", __func__, __LINE__, exception);
                }
                if (userInfo) {
                    NSError *error = nil;
                    GBUser * user = [[GBUser alloc] initWithDictionary:userInfo error:&error];
                    dAssert(user != nil, @"Failed to parse user model (3): %@",error);
                    [GBCommonData setUser:user];
                }
                if(self.isNewUser){
                   
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
                    GBLoginViewController *nameViewController = [storyboard instantiateViewControllerWithIdentifier:@"nameViewController"];
                    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:nameViewController];
                    [nc setNavigationBarHidden:YES animated:NO];
                    
                    [NAVIGATION_MANAGER setRootControllerWithAnimation:nc];
                } else{
                  [self push];
                }
                
                
                
            } failure:^(NSError *error, NSDictionary *userInfo) {
                DLog(@"error: %@, userInfo: %@", error, userInfo);
            }];

            
        } failure:^(NSError *error, NSDictionary *userInfo) {
            self.codeField.text = @"";
            self.codeField.placeholder = @"Код не вiрний";
        }];
         */
    }
    
}


#pragma mark - Private

- (void) prefillPhoneNumberTextFieldIfNeededByText:(NSString*)aText
{
    NSString * pfx = PHONE_PFX_REQUIRED;
    NSString * pfx_plus = @"";
    NSString * pfx_code = [pfx stringByReplacingOccurrencesOfString:pfx_plus withString:@""];
    if (![aText hasPrefix:pfx]) {
        NSString * resultText = [aText stringByReplacingOccurrencesOfString:pfx_plus withString:@""];
        resultText = [([resultText hasPrefix:pfx_code] ? pfx_plus : pfx) stringByAppendingString:resultText];
        self.phoneField.text = resultText;
    }
}

- (void)push {
    [[UGSFetchController sharedInstance] setToken:SECRET_TOKEN];
    [[UGSFetchController sharedInstance] setUserID:USER_ID];
    
    
    DLog(@"TOKEN User: %@", SECRET_TOKEN);
    DLog(@"ID User: %@", USER_ID);
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"TabViewController"];
    [[UIApplication sharedApplication] delegate].window.rootViewController = controller;
//    [NAVIGATION_MANAGER setRootControllerWithAnimation:controller];
}

- (void)dismissKeyboard {
//    [self.nameField resignFirstResponder];
//    [self.sirnameField resignFirstResponder];
    [self.phoneField resignFirstResponder];
}

//- (void) getCitiesList
//{
//    [[GBServerManager sharedManager] citiesSuccess:^(id responseObject) {
//        self.citieListArray = [[NSMutableArray alloc] init];
//        [self.citieListArray removeAllObjects];
//        //        DLog(@"RESPONCE!CITY!! %@", responseObject);
//        NSDictionary *CityDictionary = responseObject;
//        NSArray *dictionariesArray = [[NSArray alloc] init];
//        dictionariesArray = [CityDictionary objectForKey:@"response"];
//        for (NSDictionary *dict in dictionariesArray) {
//            [self.citieListArray addObject:dict];
//
//        }
//        DLog(@"%@", self.citieListArray);
//    } failure:^(NSError *error, NSDictionary *userInfo) {
//        DLog(@"Failed get Cities");
//    }];
//};

- (void) chooseCity
{
    WEAKSELF_DECLARATION
    GBChooseCityViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GBChooseCityViewController"];
    [vc configureWithSelectedCityId:self.city.objId completion:^(id value) {
        STRONGSELF_DECLARATION
        strongSelf.city = value;
    }];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.cityField) {
        [self chooseCity];
        return NO;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.phoneField) {
        [self prefillPhoneNumberTextFieldIfNeededByText:textField.text];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    __block NSString * resultText = [textField.text stringByReplacingCharactersInRange:range
                                                                            withString:string];
    
    if (textField == self.phoneField) {
        if (resultText.length > PHONE_NUMBER_LENGTH) {
            return NO;
        }else if (![resultText hasPrefix:PHONE_PFX_REQUIRED]) {
            if ([textField.text isEqualToString:PHONE_PFX_REQUIRED] && (resultText.length < PHONE_PFX_REQUIRED.length)) return NO;
            WEAKSELF_DECLARATION
            dispatch_async(DISP_MAIN_THREAD, ^{
                STRONGSELF_DECLARATION
                [strongSelf prefillPhoneNumberTextFieldIfNeededByText:resultText];
            });
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    
    return YES;
}

- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if ((textField == self.phoneField) && [self.phoneField.text isEqualToString:PHONE_PFX_REQUIRED])
        self.phoneField.text = @"";
}


#pragma mark - Custom Setters

- (void) setCity:(GBCityModel *)city
{
    _city = city;
    self.cityField.text = city ? city.name : @"";
}

@end
