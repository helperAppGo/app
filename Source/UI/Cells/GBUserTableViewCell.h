//
//  GBUserTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCell.h"

@interface GBUserTableViewCell : GBBaseTableViewCell

@property (nonatomic,strong,readonly) GBUser *user;

- (void) configureWithUser:(GBUser*)aUser;

@end
