#import "GBPostTableViewCell.h"
#import "NSDate+Extention.h"


@interface GBPostTableViewCell()

@end


@implementation GBPostTableViewCell

#pragma mark - GBBaseTableViewCellProtocol

+ (NSString*) cellIdentifier
{
    return @"post";
}


#pragma mark - Lilfecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drawContent];
}


#pragma mark - Public

- (void) configureWithPost:(GBPostModel*)aModel delegate:(id<GBStreamItemCellDelegate>)aDelegate
{
    _model = aModel;
    _delegate = aDelegate;
    [self drawContent];
}

- (void) configureWithFeedItem:(GBFeedModel*)aModel delegate:(id<GBStreamItemCellDelegate>)aDelegate
{
    _fmodel = aModel;
    _delegate = aDelegate;
    [self drawContent];
}

- (void) configureWithWallItem:(GBWallModel*)aModel delegate:(id<GBStreamItemCellDelegate>)aDelegate
{
    _wmodel = aModel;
    _delegate = aDelegate;
    [self drawContent];
}

#pragma mark - Private

- (void) drawContent
{
    if (self.model) {
    
        [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.model.avatar] placeholderImage:[UIImage imageNamed:@"user"]];
        self.nameLabel.text = AS_STRING(self.model.name);
        self.dateLabel.text = self.model.time ? [self.model.time intervalString:NO] : @"";
        self.lblBody.text = AS_STRING(self.model.text);
        
        if (!self.model.fileObjects.count) {
            self.lcImageHeight.constant = 0.0;
            self.lcImageTop.constant = 0.0;
        }else{
            GBFileModel *fmodel = self.model.fileObjects.firstObject;
            self.lcImageHeight.constant = [UIScreen mainScreen].bounds.size.width * ((fmodel.height && fmodel.width) ? (fmodel.height / fmodel.width) : (2.0/3.0));
            NSString *imagePath = (fmodel.eType == EAttachmentTypeImage) ? fmodel.path : fmodel.preview;
            NSURL *url = [NSURL URLWithString:imagePath];
            //__weak typeof (self.ivMedia) weak_ivMedia = self.ivMedia;
            self.ivMedia.contentMode = UIViewContentModeScaleAspectFill;
            [self.ivMedia sd_setImageWithURL:url placeholderImage:nil/*[UIImage imageNamed:@"ic_image"]*/ completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                //if (error) [weak_ivMedia setImage:[UIImage imageNamed:@"imageError"]];
            }];
            self.lcImageTop.constant = (self.model.text.length > 0) ? 4.0 : 0.0;
        }
        
        self.btnLike.selected = self.model.isLiked;
        [self.btnLike setTitle:[NSString stringWithFormat:@"%lld",self.model.countLikes] forState:UIControlStateNormal];
        [self.btnComments setTitle:[NSString stringWithFormat:@"%lld",self.model.countComments] forState:UIControlStateNormal];
        //[self.btnMore setHidden:self.model.userId == CURRENT_USER.objId.longLongValue];
        
    }else if (self.fmodel) {
        GBFeedModel *model = self.fmodel;
        
        [self.avatarImageView sd_setImageWithURL:model.avaPath placeholderImage:[UIImage imageNamed:@"user"]];
        self.nameLabel.text = AS_STRING(model.name);
        self.dateLabel.text = model.time ? [model.time intervalString:NO] : @"";
        self.lblBody.text = AS_STRING(model.text);
        
        if (!model.imageInfo) {
            self.lcImageHeight.constant = 0.0;
            self.lcImageTop.constant = 0.0;
        }else{
            GBFeedImageInfo *iimodel = model.imageInfo;
            self.lcImageHeight.constant = [UIScreen mainScreen].bounds.size.width * ((iimodel.height && iimodel.width) ? (iimodel.height / iimodel.width) : (2.0/3.0));
            NSURL *url = iimodel.Url;
            self.ivMedia.contentMode = UIViewContentModeScaleAspectFill;
            [self.ivMedia sd_setImageWithURL:url];
            self.lcImageTop.constant = (model.text.length > 0) ? 4.0 : 0.0;
        }
        
        self.btnLike.selected = model.isLiked;
        [self.btnLike setTitle:[NSString stringWithFormat:@"%ld",model.countLikes] forState:UIControlStateNormal];
        [self.btnComments setTitle:[NSString stringWithFormat:@"%ld",model.countComments] forState:UIControlStateNormal];
    }else if (self.wmodel) {
        GBWallModel *model = self.wmodel;
        
        [self.avatarImageView sd_setImageWithURL:model.path_ava placeholderImage:[UIImage imageNamed:@"user"]];
        self.nameLabel.text = AS_STRING(model.name);
        self.dateLabel.text = model.date ? [model.date intervalString:NO] : @"";
        self.lblBody.text = AS_STRING(model.text);
        
        if (!model.pathUrl) {
            self.lcImageHeight.constant = 0.0;
            self.lcImageTop.constant = 0.0;
        }else{
            self.lcImageHeight.constant = [UIScreen mainScreen].bounds.size.width * ((model.heightObjects && model.widthObjects) ? (model.heightObjects / model.widthObjects) : (2.0/3.0));
            NSURL *url = model.pathUrl;
            self.ivMedia.contentMode = UIViewContentModeScaleAspectFill;
            [self.ivMedia sd_setImageWithURL:url];
            self.lcImageTop.constant = (model.text.length > 0) ? 4.0 : 0.0;
        }
        
        self.btnLike.selected = model.is_liked;
        [self.btnLike setTitle:[NSString stringWithFormat:@"%ld",model.countLikes] forState:UIControlStateNormal];
        [self.btnComments setTitle:[NSString stringWithFormat:@"%ld",model.countComments] forState:UIControlStateNormal];
    }
}


#pragma mark - GBStreamItemCellProtocol

- (int64_t) streamItemId
{
    if (self.model) return self.model.objId;
    if (self.fmodel) return self.fmodel.justId;
    if (self.wmodel) return self.wmodel.idCountWall;
    return 0;
}

- (int64_t) userId
{
    if (self.model) return self.model.userId;
    if (self.fmodel) return self.fmodel.idUser;
    if (self.wmodel) return (self.wmodel.idUser.longLongValue == self.wmodel.idWall.longLongValue) ? self.wmodel.idUser.longLongValue : 0;
    return 0;
}

- (IBAction) onBtnComment:(id)sender
{
    if (self.actionBlock) {
        self.actionBlock(self.indexPath, FeedActionTypeComment);
    }
    
    if ([self.delegate respondsToSelector:@selector(didPressCommentButtonInCell:)])
        [self.delegate didPressCommentButtonInCell:self];
}

- (IBAction) onBtnMore:(id)sender
{
    if (self.actionBlock) {
        self.actionBlock(self.indexPath, FeedActionTypeMore);
    }
    
    if ([self.delegate respondsToSelector:@selector(didPressMoreButtonInCell:)])
        [self.delegate didPressMoreButtonInCell:self];
}

- (IBAction) onBtnLike:(UIButton *)sender
{
    if (self.actionBlock) {
        self.actionBlock(self.indexPath, FeedActionTypeLike);
    }
    
    if ([self.delegate respondsToSelector:@selector(didChangeLikeValueInCell:)]) {
        sender.selected = !sender.selected;
        if (self.model) {
            self.model.isLiked = sender.selected;
            self.model.countLikes += sender.selected ? 1 : (-1);
            [self.btnLike setTitle:[NSString stringWithFormat:@"%lld",self.model.countLikes] forState:UIControlStateNormal];
        }else if (self.fmodel) {
            self.fmodel.isLiked = sender.selected;
            self.fmodel.countLikes += sender.selected ? 1 : (-1);
            [self.btnLike setTitle:[NSString stringWithFormat:@"%ld",self.fmodel.countLikes] forState:UIControlStateNormal];
        }else if (self.wmodel) {
            self.wmodel.is_liked = sender.selected;
            self.wmodel.countLikes += sender.selected ? 1 : (-1);
            [self.btnLike setTitle:[NSString stringWithFormat:@"%ld",self.wmodel.countLikes] forState:UIControlStateNormal];
        }
        [self.delegate didChangeLikeValueInCell:self];
    }
}

- (IBAction) onBtnShare:(UIButton *)sender
{
    if (self.actionBlock) {
        self.actionBlock(self.indexPath, FeedActionTypeShare);
    }
    
}

- (IBAction) onBtnAvatar:(id)sender
{
    if (self.actionBlock) {
        self.actionBlock(self.indexPath, FeedActionTypeAvatar);
    }
    
    if ([self.delegate respondsToSelector:@selector(didPressAvatarInCell:)])
        [self.delegate didPressAvatarInCell:self];
}

- (IBAction) onBtnLocation:(UIButton*)sender
{
    if (self.actionBlock) {
        self.actionBlock(self.indexPath, FeedActionTypeLocation);
    }
    
}


@end
