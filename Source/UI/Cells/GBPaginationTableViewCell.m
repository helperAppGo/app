//
//  GBPaginationTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 18.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBPaginationTableViewCell.h"


@interface GBPaginationTableViewCell()

@property (nonatomic,weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end


@implementation GBPaginationTableViewCell


#pragma mark - GBBaseTableViewCellProtocol

+ (NSString*) cellIdentifier
{
    return @"paginator";
}

#pragma mark - Public

- (void) configureWithAnimating:(BOOL)isAnimating
{
    self.animating = isAnimating;
}

#pragma mark - Custom Setters

- (void) setAnimating:(BOOL)animating
{
    _animating = animating;
    if (animating) {
        [self.activityIndicator startAnimating];
    }else{
        [self.activityIndicator stopAnimating];
    }
}

@end
