//
//  GBPaginationTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 18.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCell.h"

@interface GBPaginationTableViewCell : GBBaseTableViewCell

@property (nonatomic,assign) BOOL animating;

- (void) configureWithAnimating:(BOOL)isAnimating;

@end
