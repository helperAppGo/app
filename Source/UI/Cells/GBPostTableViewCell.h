#import "GBBaseTableViewCell.h"
#import "GBStreamItemCellDelegate.h"
#import "GBFeedModel.h"
#import "GBWallModel.h"

@interface GBPostTableViewCell : GBBaseTableViewCell <GBStreamItemCellProtocol>

@property (weak, nonatomic) IBOutlet UIImageView        *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel            *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel            *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel            *lblBody;
@property (weak, nonatomic) IBOutlet UIImageView        *ivMedia;
@property (nonatomic, weak) IBOutlet UIButton           *btnLike;
@property (nonatomic, weak) IBOutlet UIButton           *btnComments;
@property (nonatomic, weak) IBOutlet UIButton           *btnMore;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcImageHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lcImageTop;

@property (nonatomic,strong,readonly) GBPostModel *model;
@property (nonatomic,strong,readonly) GBFeedModel *fmodel;
@property (nonatomic,strong,readonly) GBWallModel *wmodel;
@property (nonatomic,weak,readonly) id<GBStreamItemCellDelegate> delegate;

- (void) configureWithPost:(GBPostModel*)aModel delegate:(id<GBStreamItemCellDelegate>)aDelegate;
- (void) configureWithFeedItem:(GBFeedModel*)aModel delegate:(id<GBStreamItemCellDelegate>)aDelegate;
- (void) configureWithWallItem:(GBWallModel*)aModel delegate:(id<GBStreamItemCellDelegate>)aDelegate;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, copy) void(^actionBlock)(NSIndexPath *path, NSInteger action);


@end
