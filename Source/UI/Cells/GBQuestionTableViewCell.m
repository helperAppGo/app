//
//  GBQuestionTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 03.06.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBQuestionTableViewCell.h"
#import "GBQuestionsImageInfo.h"
#import "NSDate+Extention.h"

@interface GBQuestionTableViewCell()

@property (nonatomic, strong) NSDictionary *categories;

@end


@implementation GBQuestionTableViewCell

#pragma mark - GBBaseTableViewCellProtocol

+ (NSString*) cellIdentifier
{
    return @"question";
}


#pragma mark - Lilfecycle

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drawContent];
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    self.btnLocation.selected = NO;
}


#pragma mark - Public

- (void) configureWithQuestion:(GBQuestionsModel*)aModel categories:(NSDictionary*)aCategories delegate:(id<GBStreamItemCellDelegate>)aDelegate;
{
    _model = aModel;
    _delegate = aDelegate;
    _categories = aCategories;
    [self drawContent];
}


#pragma mark - Private

- (void) drawContent
{
    if (self.model) {
        
        [self.avatarImageView sd_setImageWithURL:self.model.avaPath placeholderImage:[UIImage imageNamed:@"user"]];
        self.nameLabel.text = AS_STRING(self.model.name);
        self.dateLabel.text = self.model.time ? [self.model.time intervalString:NO] : @"";
        self.lblBody.text = AS_STRING(self.model.text);
        
        self.lblCategory.text = AS_STRING(self.categories[@(self.model.idCategory)]);
        
        if (!self.model.imageInfo) {
            self.lcImageHeight.constant = 0.0;
            self.lcImageTop.constant = 0.0;
        }else{
            GBQuestionsImageInfo *iimodel = self.model.imageInfo;
            self.lcImageHeight.constant = [UIScreen mainScreen].bounds.size.width * ((iimodel.height && iimodel.width) ? (iimodel.height / iimodel.width) : (2.0/3.0));
            NSURL *url = iimodel.Url;
            //__weak typeof (self.ivMedia) weak_ivMedia = self.ivMedia;
            self.ivMedia.contentMode = UIViewContentModeScaleAspectFill;
            [self.ivMedia sd_setImageWithURL:url placeholderImage:nil/*[UIImage imageNamed:@"ic_image"]*/ completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                //if (error) [weak_ivMedia setImage:[UIImage imageNamed:@"imageError"]];
            }];
            self.lcImageTop.constant = (self.model.text.length > 0) ? 4.0 : 0.0;
        }
        
        [self.btnComments setTitle:[NSString stringWithFormat:@"%ld",self.model.countAnswers] forState:UIControlStateNormal];
        
        [self.btnLocation setTitle:(self.model.address.length ? self.model.address : @"NO DATA") forState:UIControlStateNormal];
        
        if (self.model.distanceQuestions) {
            CGFloat distance = self.model.distanceQuestions;
            NSString * distanceString = nil;;
            if (distance < 1000) {
                distanceString = [NSString stringWithFormat:@"%d м", (int)distance];
            } else {
                distance = distance /1000;
                distanceString = [NSString stringWithFormat:@"%.01f км", distance];
            }
            [self.btnLocation setTitle:distanceString forState:UIControlStateSelected];
        }else{
            [self.btnLocation setTitle:nil forState:UIControlStateSelected];
        }
    }
}


#pragma mark - GBStreamItemCellProtocol

- (int64_t) streamItemId
{
    if (self.model) return self.model.justId;
    return 0;
}

- (int64_t) userId
{
    if (self.model) return self.model.idUser;
    return 0;
}

- (IBAction) onBtnComment:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressCommentButtonInCell:)])
        [self.delegate didPressCommentButtonInCell:self];
}

- (IBAction) onBtnMore:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressMoreButtonInCell:)])
        [self.delegate didPressMoreButtonInCell:self];
}

- (IBAction) onBtnLike:(UIButton *)sender
{
}

- (IBAction) onBtnShare:(UIButton *)sender
{
    
}

- (IBAction) onBtnAvatar:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressAvatarInCell:)])
        [self.delegate didPressAvatarInCell:self];
}

- (IBAction) onBtnLocation:(UIButton*)sender
{
    sender.selected = !sender.selected;
}


@end
