//
//  GBQuestionTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 03.06.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCell.h"
#import "GBStreamItemCellDelegate.h"
#import "GBQuestionsModel.h"

@interface GBQuestionTableViewCell : GBBaseTableViewCell <GBStreamItemCellProtocol>

@property (nonatomic, weak) IBOutlet UIImageView        *avatarImageView;
@property (nonatomic, weak) IBOutlet UILabel            *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel            *lblCategory;
@property (nonatomic, weak) IBOutlet UILabel            *dateLabel;
@property (nonatomic, weak) IBOutlet UILabel            *lblBody;
@property (nonatomic, weak) IBOutlet UIImageView        *ivMedia;
@property (nonatomic, weak) IBOutlet UIButton           *btnLike;
@property (nonatomic, weak) IBOutlet UIButton           *btnComments;
@property (nonatomic, weak) IBOutlet UIButton           *btnMore;
@property (nonatomic, weak) IBOutlet UIButton           *btnLocation;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lcImageHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lcImageTop;

@property (nonatomic, weak) GBQuestionsModel            *model;
@property (nonatomic, weak,readonly) id<GBStreamItemCellDelegate> delegate;

- (void) configureWithQuestion:(GBQuestionsModel*)aModel categories:(NSDictionary*)aCategories delegate:(id<GBStreamItemCellDelegate>)aDelegate;

@end
