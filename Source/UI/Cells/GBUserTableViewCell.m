//
//  GBUserTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBUserTableViewCell.h"


@interface GBUserTableViewCell()

@property (nonatomic,strong,readwrite) GBUser *user;

@property (nonatomic,weak) IBOutlet UIImageView * ivAvatar;
@property (nonatomic,weak) IBOutlet UILabel *lblName;

@end


@implementation GBUserTableViewCell

#pragma mark - GBBaseTableViewCellProtocol

+ (NSString*) cellIdentifier
{
    return @"user";
}


#pragma mark - Lifecycle

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drawContent];
}


#pragma mark - Public

- (void) configureWithUser:(GBUser*)aUser
{
    self.user = aUser;
    [self drawContent];
}


#pragma mark - Private

- (void) drawContent
{
    if (!self.user) return;
    
    [self.ivAvatar sd_setImageWithURL:[NSURL URLWithString:self.user.avatar] placeholderImage:[UIImage imageNamed:@"user"]];
    self.lblName.text = [self.user fullName];
}


@end
