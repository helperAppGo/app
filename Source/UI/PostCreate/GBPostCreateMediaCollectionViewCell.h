//
//  GBPostCreateMediaCollectionViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 13.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

@class GBPostCreateMediaCollectionViewCell;

@protocol GBPostCreateMediaCollectionViewCellDelegate <NSObject>

- (void) didPressRemoveInCell:(GBPostCreateMediaCollectionViewCell*)aCell;

@end


@interface GBPostCreateMediaCollectionViewCell : UICollectionViewCell

+ (NSString*) cellIdentifier;

- (void) configureWithImage:(UIImage*)anImage delegate:(id<GBPostCreateMediaCollectionViewCellDelegate>)aDelegate;

@end
