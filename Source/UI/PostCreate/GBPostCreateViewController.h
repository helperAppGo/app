//
//  GBPostCreateViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 13.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"

@interface GBPostCreateViewController : GBBaseViewController

- (void)configureWithPostAddedBlock:(GBValueBlock)onPostAddedBlock;

@end
