//
//  GBPostCreateMediaCollectionViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 13.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBPostCreateMediaCollectionViewCell.h"


@interface GBPostCreateMediaCollectionViewCell()

@property (nonatomic,weak) IBOutlet UIImageView * ivMedia;

@property (nonatomic,strong) UIImage *image;
@property (nonatomic,weak) id<GBPostCreateMediaCollectionViewCellDelegate> delegate;

@end


@implementation GBPostCreateMediaCollectionViewCell

#pragma mark - Class

+ (NSString*) cellIdentifier
{
    return @"media";
}


#pragma mark - Lifecycle

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    if (self.image)
        [self drawContent];
}

#pragma mark - Public

- (void) configureWithImage:(UIImage*)anImage delegate:(id<GBPostCreateMediaCollectionViewCellDelegate>)aDelegate
{
    self.image = anImage;
    self.delegate = aDelegate;
    [self drawContent];
}


#pragma mark - Private

- (void) drawContent
{
    self.ivMedia.image = self.image;
}


#pragma mark - IBActions

- (IBAction)onBtnDelete:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressRemoveInCell:)]) {
        WEAKSELF_DECLARATION
        dispatch_async(DISP_MAIN_THREAD, ^{
            STRONGSELF_DECLARATION
            [strongSelf.delegate didPressRemoveInCell:self];
        });
    }
}

@end
