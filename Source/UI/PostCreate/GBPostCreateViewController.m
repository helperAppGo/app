//
//  GBPostCreateViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 13.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBPostCreateViewController.h"
#import "GBPostCreateMediaCollectionViewCell.h"
#import "GBServerManager.h"
@import AVFoundation;

@interface GBPostCreateViewController () <UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,GBPostCreateMediaCollectionViewCellDelegate,
                                            UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic,weak) IBOutlet UITextView * tvPost;
@property (nonatomic,weak) IBOutlet UILabel * lblPlaceholder;
@property (nonatomic,weak) IBOutlet UICollectionView * collectionView;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcMediaContainerHeight;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcPlaceholderX;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcPlaceholderY;

@property (nonatomic,strong) NSMutableArray *mediaItems;

@property (nonatomic,copy) GBValueBlock onPostAddedBlock;

@end

@implementation GBPostCreateViewController


#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.tvPost.backgroundColor = [UIColor clearColor];
    
    self.lcPlaceholderX.constant = self.tvPost.textContainerInset.left+4.0;
    self.lcPlaceholderY.constant = self.tvPost.textContainerInset.top;
    self.lcMediaContainerHeight.constant = 0.0;
}


#pragma mark - Public

- (void)configureWithPostAddedBlock:(GBValueBlock)onPostAddedBlock
{
    self.onPostAddedBlock = onPostAddedBlock;
}


#pragma mark - Private

- (void) makeMediaContainerVisible:(BOOL)visible
{
    if (visible) {
        if (self.lcMediaContainerHeight.constant > 0.0) return;
    }else{
        if (self.lcMediaContainerHeight.constant == 0.0) return;
    }
    self.lcMediaContainerHeight.constant = visible ? 104.0 : 0.0;
    [self.view setNeedsUpdateConstraints];
    WEAKSELF_DECLARATION
    [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
        STRONGSELF_DECLARATION
        [strongSelf.view layoutIfNeeded];
    }];
}

- (void) sendPost
{
    NSString * str = [self.tvPost.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ((str.length == 0) && (self.mediaItems.count == 0)) return;
    
    [SVProgressHUD show];
    
    WEAKSELF_DECLARATION
    GBStringBlock sendPostBlock = ^(NSString * mediaIdentifier){
        CDUser *user = [[SettingsManager instance] currentUser];
//        CURRENT_USER.objId
        [[GBServerManager sharedManager] sendPostToWallId:@(user.objId) text:str files:mediaIdentifier link:nil success:^(id responseObject) {
            DLog(@"Post sent: %@", responseObject);
            NSError *error = nil;
            GBPostModel *post = [[GBPostModel alloc] initWithDictionary:responseObject[@"response"] error:&error];
            if (post) {
                [SVProgressHUD showSuccessWithStatus:nil];
                dispatch_async(DISP_MAIN_THREAD, ^{
                    STRONGSELF_DECLARATION
                    if (strongSelf.onPostAddedBlock)
                        strongSelf.onPostAddedBlock(post);
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                });
            }else{
                DLog(@"Failed to parse post: %@",error);
                [SVProgressHUD showErrorWithStatus:nil];
            }
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"Send post: error: %@, userInfo: %@", error, userInfo);
            [SVProgressHUD showErrorWithStatus:nil];
        }];
    };
    
    if (self.mediaItems.count == 0) {
        sendPostBlock(nil);
    }else{
        UIImage * image = [self.mediaItems firstObject];
        NSData *data = UIImagePNGRepresentation(image);
        float k = [data length]/1024;
        if (k > 1) {
            data = UIImageJPEGRepresentation(image, 1/k);
        }
        __weak typeof(self) weakSelf = self;
        [[ApiManager sharedManager] uploadPhoto:data fileName:@"media.png" callback:^(id response, ErrorObj *error) {
            if (response) {
                response = response[@"response"];
                DLog(@"FILE PATH: %@", [response valueForKey:@"path"]);
                int64_t imageId = [[response valueForKey:@"id"] longLongValue];
                sendPostBlock([NSString stringWithFormat:@"%lld",imageId]);
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
//        [[GBServerManager sharedManager] uploadFile:UIImagePNGRepresentation(image) withName:@"media.png" success:^(id responseObject) {
//            DLog(@"file: %@", responseObject);
//            DLog(@"FILE PATH: %@", [[responseObject valueForKey:@"response"] valueForKey:@"path"]);
//            int64_t imageId = [[[responseObject valueForKey:@"response"] valueForKey:@"id"] longLongValue];
//            sendPostBlock([NSString stringWithFormat:@"%lld",imageId]);
//        } failure:^(NSError *error, NSDictionary *userInfo) {
//            DLog(@"Send media for post: error: %@, userInfo: %@", error, userInfo);
//            [SVProgressHUD showErrorWithStatus:nil];
//        }];
        
    }
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString * resultText = [textView.text stringByReplacingCharactersInRange:range withString:text];
    self.lblPlaceholder.hidden = resultText.length > 0;
    return (resultText.length <= MAX_POST_TEXT_LENGTH);
}


#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.mediaItems.count;
}

- (UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GBPostCreateMediaCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:[GBPostCreateMediaCollectionViewCell cellIdentifier] forIndexPath:indexPath];
    [cell configureWithImage:self.mediaItems[indexPath.item] delegate:self];
    return cell;
}


#pragma mark - GBPostCreateMediaCollectionViewCellDelegate

- (void) didPressRemoveInCell:(GBPostCreateMediaCollectionViewCell*)aCell
{
    NSIndexPath * indexPath = [self.collectionView indexPathForCell:aCell];
    if (!indexPath) return;
    if (self.mediaItems.count <= indexPath.item) return;
    
    [self.mediaItems removeObjectAtIndex:indexPath.item];
    [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    if (self.mediaItems.count == 0)
        [self makeMediaContainerVisible:NO];
}


#pragma mark - UIImagePickerControllerDelegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage* image = [info valueForKey:UIImagePickerControllerEditedImage];
    [self.mediaItems addObject:image];
    [self.collectionView insertItemsAtIndexPaths:@[[NSIndexPath indexPathForItem:self.mediaItems.count-1 inSection:0]]];
    [self makeMediaContainerVisible:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - IBActions

- (IBAction)onBtnGallery:(id)sender
{
    //count is limited by 1 item in this version
    if (self.mediaItems.count > 0) return;
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) return;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.navigationBar.barTintColor = self.navigationController.navigationBar.barTintColor;
    [self presentViewController:picker animated:YES completion:nil];
}

- (IBAction)onBtnCamera:(id)sender
{
    //count is limited by 1 item in this version
    if (self.mediaItems.count > 0) return;
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) return;
    
    __weak __typeof(self)weakSelf = self;
    GBVoidBlock grantedAccess = ^{
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        UIImagePickerController * picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
        picker.allowsEditing = YES;
        picker.delegate = self;
        [strongSelf presentViewController:picker animated:YES completion:nil];
    };
    
    GBVoidBlock notGrantedAccess = ^{
        STRONGSELF_DECLARATION
        NSString * msg = [NSString stringWithFormat:NSLocalizedString(@"Please allow %@ to access the camera in device's Settings", nil),APPNAME];
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * enableAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Enable Access", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                              }];
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                style:UIAlertActionStyleCancel
                                                              handler:^(UIAlertAction * _Nonnull action) {}];
        [alert addAction:enableAction];
        [alert addAction:cancelAction];
        [strongSelf presentViewController:alert animated:YES completion:nil];
    };
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (authStatus) {
            
        case AVAuthorizationStatusAuthorized:{
            DLog(@"Access to camera authorized");
            grantedAccess();
        }break;
            
        case AVAuthorizationStatusNotDetermined:{
            [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                if(granted){
                    DLog(@"Access to camera granted (type=%@)",AVMediaTypeVideo);
                    dispatch_async(DISP_MAIN_THREAD, ^{
                        grantedAccess();
                    });
                    
                } else {
                    DLog(@"Access to camera not granted (type=%@)",AVMediaTypeVideo);
                    dispatch_async(DISP_MAIN_THREAD, ^{
                        notGrantedAccess();
                    });
                }
            }];
        }break;
            
        case AVAuthorizationStatusDenied:
        case AVAuthorizationStatusRestricted:{
            DLog(@"Access to camera denied");
            notGrantedAccess();
        }break;
            
        default:
            break;
    }
}

- (IBAction)onBtnPublish:(id)sender
{
    [self sendPost];
}


#pragma mark - Custom Getters

- (NSMutableArray*) mediaItems
{
    if (!_mediaItems)
        _mediaItems = [NSMutableArray array];
    return _mediaItems;
}

@end
