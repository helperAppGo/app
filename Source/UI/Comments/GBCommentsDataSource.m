//
//  GBCommentsDataSource.m
//  GoBeside
//
//  Created by Ruslan Mishin on 17.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCommentsDataSource.h"
#import "GBCommentModel.h"

@implementation GBCommentsDataSource

#pragma mark - Comments

+ (void) getComments:(int64_t)postId limit:(NSInteger)limit offset:(NSUInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure
{
    [[GBServerManager sharedManager] getCommentsPostId:postId
                                                 limit:limit
                                                offset:offset
                                               success:^(id responseObject) {
                                                   DLog(@"Comments responce:\n%@",responseObject);
                                                   NSError *error = nil;
                                                   NSArray *comments = [GBCommentModel arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
                                                   if (error) {
                                                       DLog(@"Failed to parse comments: %@",error);
                                                       if (failure)
                                                           failure(error);
                                                   }else{
                                                       if (completion)
                                                           completion(comments);
                                                   }
                                               }
                                               failure:^(NSError *error, NSDictionary *userInfo) {
                                                   DLog(@"Failed to get comments");
                                                   if (failure)
                                                       failure(error);
                                               }];
}

+ (void) sendCommentToPostId:(int64_t)postId text:(NSString*)text andCompletion:(GBValueBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] sendCommentToPostId:postId
                                                    text:text
                                                   files:@""
                                                 success:^(id responseObject) {
                                                     DLog(@"Comment sent. Response: %@",responseObject);
                                                     NSDictionary *d = responseObject[@"response"];
                                                     NSError *error = nil;
                                                     GBCommentModel *model = [[GBCommentModel alloc] initWithDictionary:d error:&error];
                                                     if (error) {
                                                         DLog(@"Failed to parse comment: %@",error);
                                                         [SVProgressHUD showErrorWithStatus:nil];
                                                         if (failure)
                                                             failure(error);
                                                     }else{
                                                         [SVProgressHUD showSuccessWithStatus:nil];
                                                         if (completion)
                                                             completion(model);
                                                     }
                                                 } failure:^(NSError *error, NSDictionary *userInfo) {
                                                     DLog(@"Failed to send comment: %@",error);
                                                     [SVProgressHUD showErrorWithStatus:nil];
                                                     if (failure)
                                                         failure(error);
                                                 }];
}

+ (void) likeCommentById:(int64_t)commentId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure
{
    [[GBServerManager sharedManager] likePostCommenId:commentId
                                              success:^(id responseObject) {
                                                  DLog(@"Like comment success: %@",responseObject);
                                                  if (completion)
                                                      completion();
                                              } failure:^(NSError *error, NSDictionary *userInfo) {
                                                  DLog(@"Failed to like comments: %@",error);
                                                  [SVProgressHUD showErrorWithStatus:nil];
                                                  if (failure) 
                                                      failure(error);
                                              }];
}

#pragma mark - Answers

+ (void) getAnswers:(int64_t)questionId limit:(NSInteger)limit offset:(NSUInteger)offset andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    [[GBServerManager sharedManager] answersForQuestionId:questionId
                                                    limit:limit
                                                   offset:offset
                                                  success:^(id responseObject) {
                                                      DLog(@"Answers received:\n%@",responseObject);
                                                      NSError *error = nil;
                                                      NSArray *comments = [GBCommentModel arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
                                                      if (error) {
                                                          DLog(@"Failed to parse answers: %@",error);
                                                          if (failure)
                                                              failure(error);
                                                      }else{
                                                          if (completion)
                                                              completion(comments);
                                                      }
                                                  } failure:^(NSError *error, NSDictionary *userInfo) {
                                                      DLog(@"Failed to get answers: %@",error);
                                                      if (failure)
                                                          failure(error);
                                                  }];
}

+ (void) sendAnswerToQuestionId:(int64_t)questionId text:(NSString*)text andCompletion:(GBValueBlock)completion failure:(GBErrorBlock)failure
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] postAnswerForQuestionId:questionId
                                                        text:text
                                                       files:@""
                                                     success:^(id responseObject) {
                                                         DLog(@"Answer sent. Response: %@",responseObject);
                                                         NSDictionary *d = responseObject[@"response"];
                                                         NSError *error = nil;
                                                         GBCommentModel *model = [[GBCommentModel alloc] initWithDictionary:d error:&error];
                                                         if (error) {
                                                             DLog(@"Failed to parse answer: %@",error);
                                                             [SVProgressHUD showErrorWithStatus:nil];
                                                             if (failure)
                                                                 failure(error);
                                                         }else{
                                                             [SVProgressHUD showSuccessWithStatus:nil];
                                                             if (completion)
                                                                 completion(model);
                                                         }
                                                     } failure:^(NSError *error, NSDictionary *userInfo) {
                                                         DLog(@"Failed to send answer: %@",error);
                                                         [SVProgressHUD showErrorWithStatus:nil];
                                                         if (failure)
                                                             failure(error);
                                                     }];
}

+ (void) likeAnswerById:(int64_t)answerId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure
{
    [[GBServerManager sharedManager] likeAnswerId:answerId
                                          success:^(id responseObject) {
                                              DLog(@"Like answer success: %@",responseObject);
                                              if (completion)
                                                  completion();
                                          } failure:^(NSError *error, NSDictionary *userInfo) {
                                              DLog(@"Failed to like answer: %@",error);
                                              [SVProgressHUD showErrorWithStatus:nil];
                                              if (failure)
                                                  failure(error);
                                          }];
}

@end
