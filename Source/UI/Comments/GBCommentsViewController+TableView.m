//
//  GBCommentsViewController+TableView.m
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCommentsViewController+TableView.h"
#import "GBCommentTableViewCell.h"
#import "GBPaginationTableViewCell.h"


@interface GBCommentsViewController()

@end


@implementation GBCommentsViewController (TableView)

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 1;
    }else{
        return self.comments.count;
    }
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        GBPaginationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[GBPaginationTableViewCell cellIdentifier]];
        [cell configureWithAnimating:self.requestIsActive];
        if (!self.requestIsActive) {
            WEAKSELF_DECLARATION
            dispatch_async(DISP_MAIN_THREAD, ^{
                STRONGSELF_DECLARATION
                [strongSelf startRequestIfNeeded];
            });
        }
        return cell;
    }else{
        GBCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[GBCommentTableViewCell cellIdentifier]];
        GBCommentModel *comment = self.comments[indexPath.row];
        __weak typeof(self) weakSelf = self;
        [cell setClickBlock:^(NSInteger objId) {
            [weakSelf getUserProfile:objId];
        }];
        [cell configureWithComment:comment delegate:self];
        return cell;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

@end
