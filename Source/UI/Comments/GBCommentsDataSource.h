//
//  GBCommentsDataSource.h
//  GoBeside
//
//  Created by Ruslan Mishin on 17.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBServerManager.h"

@interface GBCommentsDataSource : NSObject

+ (void) getComments:(int64_t)postId limit:(NSInteger)limit offset:(NSUInteger)offset andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;
+ (void) sendCommentToPostId:(int64_t)postId text:(NSString*)text andCompletion:(GBValueBlock)completion failure:(GBErrorBlock)failure;
+ (void) likeCommentById:(int64_t)commentId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure;

+ (void) getAnswers:(int64_t)questionId limit:(NSInteger)limit offset:(NSUInteger)offset andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;
+ (void) sendAnswerToQuestionId:(int64_t)postId text:(NSString*)text andCompletion:(GBValueBlock)completion failure:(GBErrorBlock)failure;
+ (void) likeAnswerById:(int64_t)commentId andCompletion:(GBVoidBlock)completion failure:(GBErrorBlock)failure;

@end
