//
//  GBCommentsViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"
#import "GBCommentTableViewCellDelegate.h"

@interface GBCommentsViewController : GBBaseViewController <GBCommentTableViewCellDelegate>

@property (nonatomic,assign,readonly) int64_t sourceId;
@property (nonatomic,assign,readonly) ECommentItemType itemType;
@property (nonatomic,strong,readonly) NSMutableArray *comments;

@property (nonatomic,assign) BOOL requestIsActive;
@property (nonatomic,assign) BOOL endReached;

- (void) configureWithSourceId:(int64_t)aSourceId itemType:(ECommentItemType)anItemType;
- (void) startRequestIfNeeded;

@end
