//
//  GBCommentsViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCommentsViewController.h"
#import "GBCommentsViewController+TableView.h"
#import "GBCommentTableViewCell.h"
#import "GBPaginationTableViewCell.h"
#import "GBCommentsDataSource.h"
#import "GBCommentModel.h"

@interface GBCommentsViewController ()

@property (nonatomic,assign,readwrite) int64_t sourceId;
@property (nonatomic,assign,readwrite) ECommentItemType itemType;

@property (nonatomic,weak) IBOutlet UITextField *tfMessage;
@property (nonatomic,weak) IBOutlet UIButton *btnSendMessage;

@property (nonatomic, strong) NSMutableDictionary *commentsDict;

@end


@implementation GBCommentsViewController

@synthesize comments = _comments;

#pragma mark - Class

+ (BOOL) usePullToRefresh
{
    return YES;
}

#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    UIView *containerView  = self.btnSendMessage.superview;
    containerView.layer.borderWidth = 1;
    containerView.layer.borderColor = [UIColor grayColor].CGColor;
    _titleLabel.text = @"Комментарии";
    [GBCommentTableViewCell registerForTableView:self.tableView];
    [self.tableView setTableFooterView:[UIView new]];
    [[SettingsManager instance] anySound];
    [self getCommentsWithOffset:0];
}


#pragma mark - Public

- (void) configureWithSourceId:(int64_t)aSourceId itemType:(ECommentItemType)anItemType
{
    self.sourceId = aSourceId;
    self.itemType = anItemType;
}

- (void) onPullToRefresh:(UIRefreshControl *)sender
{
    [super onPullToRefresh:sender];
    
    WEAKSELF_DECLARATION
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        STRONGSELF_DECLARATION
        [strongSelf getCommentsWithOffset:0];
    });
}

- (void) startRequestIfNeeded
{
    if ((!self.requestIsActive) && (!self.endReached)) {
        [self getCommentsWithOffset:(int)self.comments.count];
    }
}


#pragma mark - Private

- (void) getCommentsWithOffset:(NSUInteger)offset
{
    WEAKSELF_DECLARATION
    self.requestIsActive = YES;
    NSInteger limit = 25;
    if (offset == 0) self.endReached = NO;
    
    GBArrayBlock onSuccess = ^(NSArray *array) {
        //DLog(@"Comments received:%@", array);
        STRONGSELF_DECLARATION
        strongSelf.requestIsActive = NO;
        if (array.count < limit)
            strongSelf.endReached = YES;
        if (offset) {
            NSInteger startIdx = strongSelf.comments.count;
            NSMutableArray *indexPaths = [NSMutableArray array];
            for (NSInteger idx = 0; idx < array.count; idx++) {
                GBCommentModel *comment = array[idx];
                if (!strongSelf.commentsDict[@(comment.objId)]) {
                    strongSelf.commentsDict[@(comment.objId)] = comment;
                    [strongSelf.comments addObject:comment];
                    [indexPaths addObject:[NSIndexPath indexPathForRow:(startIdx+idx) inSection:0]];
                }
            }
            if (startIdx) {
                [strongSelf.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }else{
            [strongSelf.comments removeAllObjects];
            [strongSelf.commentsDict removeAllObjects];
            for (GBCommentModel *comment in array)
                strongSelf.commentsDict[@(comment.objId)] = comment;
            [strongSelf.comments addObjectsFromArray:array];
            [strongSelf.tableView reloadData];
        }
        [strongSelf.tableView reloadData];
    };
    
    if (self.itemType == ECommentItemTypeFeedComment) {
        [GBCommentsDataSource getComments:self.sourceId
                                    limit:limit
                                   offset:offset
                            andCompletion:onSuccess
                                  failure:^(NSError *error) {
                                DLog(@"Failed to receive comments. Error:%@", error);
                                STRONGSELF_DECLARATION
                                strongSelf.requestIsActive = NO;
                            }];
    }else{
        [GBCommentsDataSource getAnswers:self.sourceId
                                   limit:limit
                                  offset:offset
                           andCompletion:onSuccess
                                 failure:^(NSError *error) {
                               DLog(@"Failed to receive comments. Error:%@", error);
                               STRONGSELF_DECLARATION
                               strongSelf.requestIsActive = NO;
                           }];
    }
}

- (void) makeMessageBoxEnabled:(BOOL)enabled
{
    self.btnSendMessage.userInteractionEnabled = enabled;
    self.btnSendMessage.alpha = enabled ? 1.0 : 0.4;
    self.tfMessage.userInteractionEnabled = enabled;
    self.tfMessage.alpha = enabled ? 1.0 : 0.4;
}


#pragma mark - Custom Getters

- (NSMutableArray*)comments
{
    if (!_comments)
        _comments = [NSMutableArray array];
    return _comments;
}

- (NSMutableDictionary*)commentsDict
{
    if (!_commentsDict)
        _commentsDict = [NSMutableDictionary dictionary];
    return _commentsDict;
}


#pragma mark - Custom Setters

- (void) setRequestIsActive:(BOOL)requestIsActive
{
    _requestIsActive = requestIsActive;
    if (!requestIsActive) [self stopRefreshControl];
    NSArray *visible = self.tableView.visibleCells;
    NSArray *filtered = [visible filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"class == %@", [GBPaginationTableViewCell class]]];
    if (filtered.count) {
        GBPaginationTableViewCell *cell = [filtered firstObject];
        cell.animating = requestIsActive;
    }
}


#pragma mark - IBActions

- (IBAction) onBtnSendMessage:(id)sender
{
    NSString *message = [self.tfMessage.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (!message.length) return;
    
    [self makeMessageBoxEnabled:NO];
    WEAKSELF_DECLARATION
    
    GBValueBlock onSuccess = ^(id value){
        STRONGSELF_DECLARATION
        [strongSelf makeMessageBoxEnabled:YES];
        strongSelf.tfMessage.text = @"";
        GBCommentModel *comment = value;
        [strongSelf.comments insertObject:comment atIndex:0];
        strongSelf.commentsDict[@(comment.objId)] = comment;
        [strongSelf.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView reloadData];
    };
    
    if (self.itemType == ECommentItemTypeFeedComment) {
        [GBCommentsDataSource sendCommentToPostId:self.sourceId
                                             text:message
                                    andCompletion:^(id value) {
                                        onSuccess(value);
                                    } failure:^(NSError *err) {
                                        STRONGSELF_DECLARATION
                                        [strongSelf makeMessageBoxEnabled:YES];
                                    }];
    }else{
        [GBCommentsDataSource sendAnswerToQuestionId:self.sourceId
                                                text:message
                                       andCompletion:^(id value) {
                                           onSuccess(value);
                                       } failure:^(NSError *err) {
                                           STRONGSELF_DECLARATION
                                           [strongSelf makeMessageBoxEnabled:YES];
                                       }];
    }
}

#pragma mark - GBCommentTableViewCellDelegate

- (void) didPressLikeForComment:(int64_t)aCommentId
{
    WEAKSELF_DECLARATION
    
    GBVoidBlock onSuccess = ^{
        STRONGSELF_DECLARATION
        GBCommentModel *comment = strongSelf.commentsDict[@(aCommentId)];
        comment.isLiked = YES;
        comment.countLikes += 1;
        if (comment) {
            NSUInteger idx = [strongSelf.comments indexOfObject:comment];
            if (idx != NSNotFound)
                [strongSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        }
    };
    if (self.itemType == ECommentItemTypeFeedComment) {
        [GBCommentsDataSource likeCommentById:aCommentId andCompletion:^{
            DLog(@"Did like comment");
            onSuccess();
        } failure:^(NSError *err) {
            DLog(@"Like comment failed: %@",err);
        }];
    }else{
        [GBCommentsDataSource likeAnswerById:aCommentId andCompletion:^{
            DLog(@"Did like answer");
            onSuccess();
        } failure:^(NSError *err) {
            DLog(@"Like comment failed: %@",err);
        }];
    }
}

@end
