//
//  GBCommentTableViewCellDelegate.h
//  GoBeside
//
//  Created by Ruslan Mishin on 19.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

@protocol GBCommentTableViewCellDelegate <NSObject>

- (void) didPressLikeForComment:(int64_t)aCommentId;

@end
