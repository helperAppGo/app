//
//  GBCommentTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCommentTableViewCell.h"
#import "GBCommentModel.h"
#import "NSDate+Extention.h"

@interface GBCommentTableViewCell()

@property (nonatomic,weak) IBOutlet UIImageView *ivAvatar;
@property (nonatomic,weak) IBOutlet UILabel *lblNameAndMessage;
@property (nonatomic,weak) IBOutlet UILabel *lblTime;
@property (nonatomic,weak) IBOutlet UILabel *lblLikes;
@property (nonatomic,weak) IBOutlet UIButton *btnLike;

@property (nonatomic,strong) GBCommentModel *comment;
@property (nonatomic,weak) id<GBCommentTableViewCellDelegate> delegate;

@end


@implementation GBCommentTableViewCell

#pragma mark - GBBaseTableViewCellProtocol

+ (NSString*) cellIdentifier
{
    return @"comment";
}
-(void)awakeFromNib{
    [super awakeFromNib];
    self.lblTime.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.lblLikes.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.ivAvatar.layer.cornerRadius = 22.5;
    self.ivAvatar.clipsToBounds = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarTap)];
    self.ivAvatar.userInteractionEnabled = YES;
    [self.ivAvatar addGestureRecognizer:tapGesture];
    
    UIFont *fntName = [UIFont defaultFontWithSize:13 bold:YES];
    self.lblNameAndMessage.font = fntName;
}

-(void)avatarTap{
    if (self.clickBlock) {
        self.clickBlock(self.comment.userId);
    }
}

#pragma mark - Lifecycle

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
   // if (self.comment)
     //   [self drawContent];
}


#pragma mark - Public

- (void) configureWithComment:(GBCommentModel*)aComment delegate:(id<GBCommentTableViewCellDelegate>)aDelegate
{
    self.comment = aComment;
    self.delegate = aDelegate;
    [self drawContent];
}


#pragma mark - Private

- (void) drawContent
{
    if (!self.comment) return;
    
    self.ivAvatar.image = nil;
    [self.ivAvatar sd_setImageWithURL:[NSURL URLWithString:self.comment.avatar] placeholderImage:[UIImage imageNamed:@"user"]];
    self.btnLike.selected = self.comment.isLiked;
    self.btnLike.userInteractionEnabled = !self.comment.isLiked;
    
    NSString *name = [AS_STRING(self.comment.name) stringByAppendingString:@" "];;
    UIFont *fntName = [UIFont defaultFontWithSize:13 bold:YES];
    //[UIFont boldSystemFontOfSize:self.lblNameAndMessage.font.pointSize];
    NSMutableAttributedString *asName = [[NSMutableAttributedString alloc] initWithString:name attributes:@{NSFontAttributeName:fntName}];
    UIFont *fntText = [UIFont systemFontOfSize:self.lblNameAndMessage.font.pointSize];
    NSAttributedString *asText = [[NSAttributedString alloc] initWithString:AS_STRING(self.comment.text) attributes:@{NSFontAttributeName:fntText}];
    [asName appendAttributedString:asText];
    self.lblNameAndMessage.attributedText = asName;
    self.lblTime.text = self.comment.time ? [self.comment.time intervalString:NO] : @"";
    self.lblLikes.text = [NSString stringWithFormat:@"%lld",self.comment.countLikes];
}


#pragma mark - IBActions

- (IBAction) onBtnLike:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressLikeForComment:)])
        [self.delegate didPressLikeForComment:self.comment.objId];
}

@end
