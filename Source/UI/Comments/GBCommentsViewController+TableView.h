//
//  GBCommentsViewController+TableView.h
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCommentsViewController.h"

@interface GBCommentsViewController (TableView) <UITableViewDelegate,UITableViewDataSource>

@end
