//
//  GBCommentTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCell.h"
#import "GBCommentTableViewCellDelegate.h"

@class GBCommentModel;

@interface GBCommentTableViewCell : GBBaseTableViewCell

@property (nonatomic, copy) void(^clickBlock)(NSInteger objId);

- (void) configureWithComment:(GBCommentModel*)aComment delegate:(id<GBCommentTableViewCellDelegate>)aDelegate;

@end
