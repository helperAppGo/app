//
//  GBImagePreviewViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 30.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"

@interface GBImagePreviewViewController : GBBaseViewController

- (void) configureWithUrl:(NSURL*)anUrl cacheOptions:(SDWebImageOptions)aCacheOptions;
- (void) configureWithImage:(UIImage*)anImage;

@end
