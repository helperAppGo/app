//
//  GBImagePreviewViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 30.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBImagePreviewViewController.h"

@interface GBImagePreviewViewController () <UIGestureRecognizerDelegate>

@property (nonatomic,weak) IBOutlet UIImageView * ivPreview;

@property (nonatomic,strong) NSURL * url;
@property (nonatomic,assign) SDWebImageOptions cacheOptions;
@property (nonatomic,strong) UIImage * image;
@property (nonatomic,weak) IBOutlet UIView * vPreviewContainer;

@property (nonatomic,weak) IBOutlet UIPanGestureRecognizer * panGestureRecognizer;
@property (nonatomic,weak) IBOutlet UIPinchGestureRecognizer * pinchGestureRecognizer;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcImageWidth;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcImageHeight;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcImageOffsetX;
@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcImageOffsetY;

@end

@implementation GBImagePreviewViewController

#pragma mark - Lifecycle

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.view layoutIfNeeded];
    
    if (self.image) {
        [self.ivPreview setImage:self.image];
    }else if (self.url) {
        WEAKSELF_DECLARATION
        [self.ivPreview sd_setImageWithURL:self.url placeholderImage:nil options:self.cacheOptions completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            if (!weakSelf) return;
            if (!image) return;
            STRONGSELF_DECLARATION
            
            CGSize imgSize = image.size;
            CGFloat scaleX = imgSize.width / strongSelf.vPreviewContainer.bounds.size.width;
            CGFloat scaleY = imgSize.height / strongSelf.vPreviewContainer.bounds.size.height;
            if (scaleX > scaleY) {
                strongSelf.lcImageWidth.constant = strongSelf.vPreviewContainer.bounds.size.width;
                strongSelf.lcImageHeight.constant = strongSelf.vPreviewContainer.bounds.size.height * scaleY/scaleX;
            }else{
                strongSelf.lcImageHeight.constant = strongSelf.vPreviewContainer.bounds.size.height;
                strongSelf.lcImageWidth.constant = strongSelf.vPreviewContainer.bounds.size.width * scaleX/scaleY;
            }
        }];
    }
    appDelegate.shouldRotate = YES;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    appDelegate.shouldRotate = NO;
}

#pragma mark - Public

- (void) configureWithUrl:(NSURL*)anUrl cacheOptions:(SDWebImageOptions)aCacheOptions
{
    self.cacheOptions = aCacheOptions;
    self.url = anUrl;
}

- (void) configureWithImage:(UIImage*)anImage
{
    self.image = anImage;
}

#pragma mark - Private

- (void) fixImageScaleAndPosition
{
    CGFloat scale = self.ivPreview.transform.a;
    BOOL fixScale = NO;
    
    if (scale < 1.0) {
        scale = 1.0;
        fixScale = YES;
        
    }
    
    BOOL fixOffset = NO;
    CGFloat maxOffsetX = (self.lcImageWidth.constant * scale - self.vPreviewContainer.bounds.size.width) * 0.5;
    CGFloat maxOffsetY = (self.lcImageHeight.constant * scale - self.vPreviewContainer.bounds.size.height) * 0.5;
    
    if (maxOffsetX < 0.0) {
        fixOffset = YES;
        self.lcImageOffsetX.constant = 0.0;
    }else if (ABS(maxOffsetX) < ABS(self.lcImageOffsetX.constant)) {
        fixOffset = YES;
        self.lcImageOffsetX.constant = ABS(maxOffsetX) * ((self.lcImageOffsetX.constant>0) ? 1.0 : -1.0);
    }
    
    if (maxOffsetY < 0.0) {
        fixOffset = YES;
        self.lcImageOffsetY.constant = 0.0;
    }else if (ABS(maxOffsetY) < ABS(self.lcImageOffsetY.constant)) {
        fixOffset = YES;
        self.lcImageOffsetY.constant = ABS(maxOffsetY) * ((self.lcImageOffsetY.constant>0) ? 1.0 : -1.0);
    }
    
    if (fixScale || fixOffset) {
        if (fixOffset)
            [self.view setNeedsUpdateConstraints];
        WEAKSELF_DECLARATION
        [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
            STRONGSELF_DECLARATION
            if (fixScale)
                strongSelf.ivPreview.transform = CGAffineTransformIdentity;
            if (fixOffset)
                [strongSelf.view layoutIfNeeded];
        }];
    }
}

#pragma mark - Gestures

- (IBAction)pinchGestureDetected:(UIPinchGestureRecognizer *)recognizer
{
//    CGFloat resScale =
    DLog(@"recognizer scale: %f",recognizer.scale);
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
    
    if ((recognizer.state == UIGestureRecognizerStateEnded) || (recognizer.state == UIGestureRecognizerStateCancelled)) {
        [self fixImageScaleAndPosition];
    }
}

- (IBAction)panGestureDetected:(UIPanGestureRecognizer *)recognizer
{
    CGPoint translation = [recognizer translationInView:self.view];
    
    self.lcImageOffsetX.constant = self.lcImageOffsetX.constant + translation.x;
    self.lcImageOffsetY.constant = self.lcImageOffsetY.constant + translation.y;
    
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];

    if ((recognizer.state == UIGestureRecognizerStateEnded) || (recognizer.state == UIGestureRecognizerStateCancelled)) {
        [self fixImageScaleAndPosition];
    }
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

@end
