//  GBNotificationVC.m
//  GoBeside

#import "GBNotificationVC.h"

@interface GBNotificationVC ()

@end

@implementation GBNotificationVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)backBBAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
