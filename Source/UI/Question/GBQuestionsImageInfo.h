//
//  GBQuestionsImageInfo.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBQuestionsImageInfo : NSObject

@property (strong, nonatomic) NSURL *Url;
@property (assign, nonatomic) NSInteger imageID;
@property (strong, nonatomic) NSString *type;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat loadError;


- (instancetype) initWithDictionaryInArray:(NSArray*) array;
@end
