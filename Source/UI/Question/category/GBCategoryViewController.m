//
//  GBCategoryViewController.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 27.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBCategoryViewController.h"

@interface GBCategoryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong,nonatomic) NSMutableArray *marked;

@end

@implementation GBCategoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.marked = [[NSMutableArray alloc] initWithCapacity:self.categoryList.count];
    for (NSInteger row=0; row<self.categoryList.count; row++){
        self.marked[row] = [NSNumber numberWithBool:YES];
    };
    
    
    NSLog(@"responce %@", self.categoryList);
    // Do any additional setup after loading the view.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"%ld", self.categoryList.count);
    return self.categoryList.count;
}


// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"categoryCell";
    int row = (int)indexPath.row;
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    
    cell.imageView.image = [UIImage imageNamed:@"ic_check"];
    NSDictionary *dict = self.categoryList[row];
    cell.textLabel.text = [dict objectForKey:@"name"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSInteger row = indexPath.row;
    if([self.marked[row] isEqual:[NSNumber numberWithBool:YES]]){
        self.marked[row] = [NSNumber numberWithBool:NO];
      cell.imageView.image = [UIImage imageNamed:@"ic_uncheck"];
    }
    else
    {
        self.marked[row] = [NSNumber numberWithBool:YES];
        cell.imageView.image = [UIImage imageNamed:@"ic_check"];
    }
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)doneAction:(id)sender {
    NSArray *returnArrayToQuestionsController = [[NSArray alloc] init];
    
    [self.delegate addMarkedCategoryArrayViewController:self didFinishEnteringItem:returnArrayToQuestionsController];
}

@end
