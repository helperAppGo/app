//
//  GBCategoryViewController.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 27.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GBCategoryViewController;

@protocol GBCategoryViewControllerDelegate <NSObject>
- (void)addMarkedCategoryArrayViewController:(GBCategoryViewController *)controller didFinishEnteringItem:(NSArray *)array;
@end

@interface GBCategoryViewController : UIViewController

@property (nonatomic, weak) id <GBCategoryViewControllerDelegate> delegate;
@property (strong, nonatomic) NSArray *categoryList;
@property (strong, nonatomic) NSString *checkedCategory;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)doneAction:(id)sender;


@end
