//
//  GBQuestionsModel.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBStreamItemModel.h"
#import "GBQuestionsImageInfo.h"

@interface GBQuestionsModel : GBStreamItemModel

@property (strong, nonatomic) NSString *address;
@property (assign, nonatomic) NSInteger countAnswers;
@property (assign, nonatomic) NSInteger countLikes;
@property (assign, nonatomic) CGFloat distanceQuestions;
@property (strong, nonatomic) NSArray *files;
@property (assign, nonatomic) NSInteger idCategory;
@property (assign, nonatomic) CGFloat lat;
@property (assign, nonatomic) CGFloat lon;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSURL    *avaPath;
@property (assign, nonatomic) NSInteger status;
@property (strong, nonatomic) NSString *text;


//@property (assign, nonatomic) NSInteger isLiked;
//@property (assign, nonatomic) NSInteger isPublic;

@property (strong, nonatomic) NSDate   *time;
@property (assign, nonatomic) BOOL isNoImage;
@property (assign, nonatomic) BOOL isNoText;
//@property (strong, nonatomic) NSDictionary *filesObject;
@property (strong, nonatomic) GBQuestionsImageInfo *imageInfo;

- (instancetype)initWithDictionary:(NSDictionary*) dict;


@end
