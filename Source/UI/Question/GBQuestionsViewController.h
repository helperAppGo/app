//
//  GBQuestionsViewController.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBStreamViewController.h"
#import "GBQuestionsModel.h"
#import "GBQuestionsDataSource.h"
#import "UIImageView+AFNetworking.h"
#import "GBQuestionsImageInfo.h"
#import "GBSwitcher.h"
#import "GBCategoryViewController.h"




@interface GBQuestionsViewController : GBStreamViewController <UITableViewDataSource, UITableViewDelegate, GBCategoryViewControllerDelegate>

@property (strong, nonatomic) GBSwitcher *switcher;

@property (strong, nonatomic) NSDictionary *categoryDictrionary;

- (void)addMarkedCategoryArrayViewController:(GBCategoryViewController *)controller didFinishEnteringItem:(NSArray *)array;

@end
