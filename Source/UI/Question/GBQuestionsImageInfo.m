//
//  GBQuestionsImageInfo.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBQuestionsImageInfo.h"

@implementation GBQuestionsImageInfo

- (instancetype) initWithDictionaryInArray:(NSArray*) array {
    NSDictionary *dictionary = [array objectAtIndex:0];
    NSString *path ;
//    [[BASE_URL stringByAppendingString:@"/"] stringByAppendingString:
    path = [dictionary objectForKey:@"path"];
    self.Url = [NSURL URLWithString:path];
    self.height = [[dictionary objectForKey:@"height"] floatValue];
    self.width = [[dictionary objectForKey:@"width"] floatValue];
    self.imageID = [[dictionary objectForKey:@"width"] intValue];
    self.type = [dictionary objectForKey:@"type"];
    [self changeHeightForAspectRatio];
    //    NSLog(@"%@ \n %f %f", path, height, width);
    return self;
}

- (void) changeHeightForAspectRatio{
    float oldWidth = self.width;
    if (oldWidth == 0.0f) {
        self.loadError = 1;
        UIImage *image = [[UIImage alloc] init];
        image = [UIImage imageNamed:@"imageError.png"];
        self.width = image.size.width;
        self.height = image.size.height;
        oldWidth = self.width;
    }
    
    float scaleFactor = [UIScreen mainScreen].bounds.size.width / oldWidth;
    
    self.height = self.height * scaleFactor;
    self.width = self.width * scaleFactor;
}

@end
