//
//  GBQuestionsViewController.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBQuestionsViewController.h"
#import "GBServerManager.h"
#import "GBQuestionTableViewCell.h"


#define Rgb2UIColor(r, g, b)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]

@interface GBQuestionsViewController ()

@property (strong, nonatomic) NSMutableArray *dataSourceArray;

@property (assign, nonatomic) NSInteger offsetQuestions;
@property (assign, nonatomic) NSInteger limitQuestions;
@property (assign, nonatomic) NSInteger offsetGeo;
@property (assign, nonatomic) NSInteger limitGeo;

@property (strong, nonatomic) NSArray *responceArrayQuestions;
@property (strong, nonatomic) NSArray *responceArrayGeo;
@property (strong, nonatomic) NSMutableArray *modelArrayQuestions;
@property (strong, nonatomic) NSMutableArray *modelArrayGeo;
@property (strong, nonatomic) NSArray *categoryList;

//for IOS 9 like DeploymentTarget thereis no @property .refreshController by default in iOS 9
@property (strong, nonatomic) UIRefreshControl *refreshControl;

//@property (weak, nonatomic) IBOutlet UINavigationItem *textQuestions;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedController;

- (IBAction)segmentControllAction:(id)sender;
@end

@implementation GBQuestionsViewController

#pragma mark - GBStreamViewController

+ (EStreamItemType) streamItemType
{
    return EStreamItemTypeQuestions;
}


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self switchSegmentedController];
    
    [GBQuestionTableViewCell registerForTableView:self.tableView];
    
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [self initClassObjects];
    
    [GBQuestionsDataSource returnCategoriListandCompletion:^(NSArray *array) {
        self.categoryList = array;
        NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
        [tempDict removeAllObjects];
        self.categoryDictrionary = nil;
        for (NSDictionary *dict in self.categoryList){
            
            NSString *key = [dict objectForKey:@"id"];
            DLog(@"%@", key);
            [tempDict setObject:[dict objectForKey:@"name"] forKey:key];
        }
        self.categoryDictrionary = tempDict;
        DLog(@"%@", tempDict);
        DLog(@"%@", self.categoryDictrionary);
        [self.tableView reloadData];
    }
                                                   failure:^(NSError *error) {
        DLog(@"Error Category request %@", error);
    }];
    DLog(@"self.categoryList %@", self.categoryList);
}


- (void) segmentedControllerChangeValue{
    if(self.segmentedController.selectedSegmentIndex == 0){
        DLog(@"%lu", (unsigned long)[self.modelArrayQuestions count]);
        self.dataSourceArray = self.modelArrayQuestions;
    }
    else
    {
        DLog(@"%lu", (unsigned long)[self.modelArrayGeo count]);
        self.dataSourceArray = self.modelArrayGeo;
}
    DLog(@"%lu", (unsigned long)[self.dataSourceArray count]);
    [self.tableView reloadData];
}
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    DLog(@"self.dataSourceArray.count %lu", (unsigned long)self.dataSourceArray.count);
    return self.dataSourceArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = (int)indexPath.row;
    
    GBQuestionsModel *question = self.dataSourceArray[row];
    GBQuestionTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:[GBQuestionTableViewCell cellIdentifier] forIndexPath:indexPath];
    [cell configureWithQuestion:question categories:self.categoryDictrionary delegate:self];
    
    return cell;
    
}

#pragma mark - UITableViewDelegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 232.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;
    GBQuestionsModel *question = self.dataSourceArray[row];
    [self openCommentsForStreamItemId:question.justId];
}
- (void)addMarkedCategoryArrayViewController:(GBCategoryViewController *)controller didFinishEnteringItem:(NSArray *)array
{
    //addcode to category request
}


-(void) getQuestionsDefault
{
    [GBQuestionsDataSource getQuestionsWithLimit:25 offset:0 andCompletion:^(NSArray *questions) {
        self.responceArrayQuestions = questions;
        if(self.offsetQuestions==0){
            [self.modelArrayQuestions removeAllObjects];
        }
        
        for (NSDictionary *dict in self.responceArrayQuestions){
            GBQuestionsModel *model = [[GBQuestionsModel alloc] initWithDictionary:dict];
            [self.modelArrayQuestions addObject:model];
        };

        if(self.segmentedController.selectedSegmentIndex == 0){
            self.dataSourceArray = self.modelArrayQuestions; // becouse allwas on first
        }
        
        DLog(@"Data RELOADED");
        [self.tableView reloadData];
        DLog(@"Model in Questions array:%lu", (unsigned long)self.modelArrayQuestions.count);
        [self stopRefresherController];
        
    } failure:^(NSError *error) {
        DLog(@"Error load modelArrayQuestions");
    }];
}
- (void) stopRefresherController{
    if(self.refreshControl.isRefreshing){
        [self.refreshControl endRefreshing];
    }
}

-(void) getGeoDefault
{
    double timeSince70 = [[NSDate date]timeIntervalSince1970];
    [GBQuestionsDataSource getGeoTime:timeSince70 limit:25 offset:0 andCompletion:^(NSArray *questions){
        self.responceArrayGeo = questions;
        if(self.offsetGeo==0){
            [self.modelArrayGeo removeAllObjects];
        }
        
        for (NSDictionary *dict in self.responceArrayGeo){
            GBQuestionsModel *model = [[GBQuestionsModel alloc] initWithDictionary:dict];
            [self.modelArrayGeo addObject:model];
        };
//         [self.dataSourceArray removeAllObjects];
        if(self.segmentedController.selectedSegmentIndex == 1){
            self.dataSourceArray = self.modelArrayGeo; // becouse allwas on first
        }
        
        DLog(@"Data RELOADED");
        [self.tableView reloadData];
        DLog(@"Model in Questions array:%lu", (unsigned long)self.modelArrayQuestions.count);
        [self stopRefresherController];
        
    } failure:^(NSError *error) {
        DLog(@"Error load modelArrayQuestions");
    }];
}


#pragma mark - Refresh
-(void)refreshTableView
{
//    [self.dataSourceArray removeAllObjects];
    DLog(@"Refresh control execute code");
    
    if (self.segmentedController.selectedSegmentIndex == 0) {
        [self getQuestionsDefault];
    }
    else{
      [self getGeoDefault];
    }
    
}

#pragma mark - API
//- (void)getFileList:(NSArray *)filesIds
//            success:(GBServerManagerSuccessHandler)success
//            failure:(GBServerManagerFailureHandler)failure{
//
//}
-(void) initClassObjects{
    

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(segmentedControllerChangeValue)
                                                 name:@"segmentedControllerChangeValue"
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(categoryListReturned)
//                                                 name:@"categoryListReturned"
//                                               object:nil];
   
    
    // --- SEGMENTED CONTROLLER STYLE --------------------------------------------------------------
    // bubble color
    
    
    
    
    
    self.segmentedController.layer.cornerRadius = 5.0;
    self.segmentedController.layer.borderColor = [UIColor whiteColor].CGColor;
    self.segmentedController.layer.borderWidth = 2.0f;
    //segmentedController.layer.masksToBounds = YES;
    
    // text selected color
    UIColor *blackColor = [UIColor whiteColor];
    NSDictionary *selected = [NSDictionary dictionaryWithObjects:@[blackColor] forKeys:@[NSForegroundColorAttributeName]];
    [self.segmentedController setTitleTextAttributes:selected
                                            forState:UIControlStateSelected];
    
    // text normal color
    UIColor *lightGrayColor = [UIColor lightGrayColor];
    NSDictionary *normal = [NSDictionary dictionaryWithObjects:@[lightGrayColor] forKeys:@[NSForegroundColorAttributeName]];
    [self.segmentedController setTitleTextAttributes:normal
                                            forState:UIControlStateNormal];
    
    self.navigationController.navigationItem.titleView = self.segmentedController;
    //end segmentedControl ctrl+c +ctrl+v
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
//    [self.tableView.refreshControl addTarget:self
//                                      action:@selector(refreshTableView)
//                            forControlEvents:UIControlEventValueChanged];
    
    self.dataSourceArray        = [[NSMutableArray alloc] init];
    self.modelArrayQuestions    = [[NSMutableArray alloc] init];
    self.modelArrayGeo          = [[NSMutableArray alloc] init];
    self.responceArrayQuestions = [[NSArray alloc] init];
    self.responceArrayGeo       = [[NSArray alloc] init];
    self.categoryList           = [[NSArray alloc] init];
    self.categoryDictrionary    = [[NSDictionary alloc] init];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self getQuestionsDefault];
    });
    ////////////////////////////////////ADD CODE FOR 25+
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getGeoDefault];
    });
}

#pragma mark - Actions



-(void) dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"fetchDidCompleteQuestions"
//                                                  object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"segmentedControllerChangeValue"
                                                  object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:@"categoryListReturned"
//                                                  object:nil];
}
-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"categorySegue"]){
        GBCategoryViewController *destViewController = segue.destinationViewController;
        destViewController.categoryList = self.categoryList;
        destViewController.delegate = self;
    }
}

- (IBAction)segmentControllAction:(id)sender {
//    DLog(@"SegmentedControllChangeValue");
//    [[NSNotificationCenter defaultCenter]         postNotificationName:@"segmentedControllerChangeValue"
//                                                                object:self
//                                                              userInfo:nil];
    if(self.segmentedController.selectedSegmentIndex == 0){
        DLog(@"%lud", (unsigned long)[self.modelArrayQuestions count]);
        self.dataSourceArray = self.modelArrayQuestions;
    }
    else
    {
        DLog(@"%lu", [self.modelArrayGeo count]);
        self.dataSourceArray = self.modelArrayGeo;
    }
    DLog(@"%lu", [self.dataSourceArray count]);
    [self.tableView reloadData];

}


-(void) switchSegmentedController {
    // SHOW FIRST VIEW CONTROLLER -----
//    [allGroup removeFromParentViewController];
//    [allGroup.view removeFromSuperview];
//    [self addChildViewController:allGroup];
//    [self fullfillToBottomView:allGroup.view];
//
    // SWITCHER -----
    NSInteger margin = 57;
    UIFont *text2Font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];

    NSString *firstString = @"Питання";
    NSString *secondString = @"Біля мене";
    NSMutableAttributedString *attributedString1 =
    [[NSMutableAttributedString alloc] initWithString:firstString attributes:@{NSFontAttributeName : text2Font }];
    NSMutableAttributedString *attributedString2 =
    [[NSMutableAttributedString alloc] initWithString:secondString attributes:@{NSFontAttributeName : text2Font }];
    
//    self.switcher = [[GBSwitcher alloc] initWithStringsArray:@[, ]];
    self.switcher = [[GBSwitcher alloc] initWithAttributedStringsArray:@[attributedString1,attributedString2 ]];
    self.switcher.frame = CGRectMake(margin, 20, 200, 30);
    self.switcher.backgroundColor =  Rgb2UIColor(214, 214, 214);
    self.switcher.labelTextColorOutsideSlider = Rgb2UIColor(0, 0, 0);
    self.switcher.labelTextColorInsideSlider = Rgb2UIColor(7, 70, 151);
    self.navigationItem.titleView = self.switcher;
    WEAKSELF_DECLARATION
    [self.switcher setPressedHandler:^(NSUInteger index) {
        STRONGSELF_DECLARATION
        if(index == 0){
            DLog(@"Did press position on switch at index: %lu", (unsigned long)index);
            DLog(@"%lu", [strongSelf.modelArrayQuestions count]);
            strongSelf.dataSourceArray = strongSelf.modelArrayQuestions;
          
            
        } else {
            DLog(@"Did press position on switch at index: %lu", (unsigned long)index);
            DLog(@"%lu", [strongSelf.modelArrayGeo count]);
            strongSelf.dataSourceArray = strongSelf.modelArrayGeo;
          
        }
        DLog(@"%lu", [strongSelf.dataSourceArray count]);
        [strongSelf.tableView reloadData];
    }
];
}

#pragma mark - Custom Getters

- (NSMutableArray*)modelArray
{
    return self.dataSourceArray;
}

    
@end
