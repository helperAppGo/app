//
//  GBQuestionsModel.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBQuestionsModel.h"

@implementation GBQuestionsModel

- (instancetype)initWithDictionary:(NSDictionary*) dict
{
    self = [super init];
    if (self) {
//        self.countComments   = [[dict objectForKey:@"count_comments"] intValue];
        self.countLikes      = [[dict objectForKey:@"count_likes"] intValue];
        
        NSString *fileString = [dict objectForKey:@"files"];
//        NSLog(@"Filestring %@", fileString);
        if ([dict objectForKey:@"file_objects"])
        {
            NSArray *responceFileObjects = [dict objectForKey:@"file_objects"];
            self.imageInfo = [[GBQuestionsImageInfo alloc] initWithDictionaryInArray:responceFileObjects];
        }
#pragma - mark Fix#2
            if(self.imageInfo.height == 0){
                self.isNoImage = YES;
            
            
        }
        else
        {
            self.isNoImage = NO;
            
        }
        
        
        if(!self.isNoImage){
            NSInteger fileIdInt = [fileString integerValue];
            self.files = @[@(fileIdInt)];
            
        }
        else if([fileString containsString:@","]){
            self.files = [fileString componentsSeparatedByString:@","];
            NSLog(@"Send File array to VC array:%@", fileString);
        }
        else{
//            NSLog(@"NO file and no array", fileString);
        }
        //        self.files = @[[dict objectForKey:@"files"]];
        ////        [[NSArray alloc] initWithObjects:fileString, nil]; ;
        self.address                = [dict objectForKey:@"address"];
        self.countAnswers           = [[dict objectForKey:@"count_answers"] intValue];
        self.countLikes             = [[dict objectForKey:@"count_likes"] intValue];
        self.distanceQuestions      = [[dict objectForKey:@"distance"] floatValue];
        self.files                  = [dict objectForKey:@"files"];
        self.countLikes             = [[dict objectForKey:@"count_likes"] intValue];
        self.justId                 = [[dict objectForKey:@"id"] intValue];
        self.idCategory             = [[dict objectForKey:@"id_category"] intValue];
        self.idUser                 = [[dict objectForKey:@"id_user"] intValue];
        self.lat                    = [[dict objectForKey:@"lat"] floatValue];
        self.lon                    = [[dict objectForKey:@"lon"] floatValue];
        self.name                   = [dict objectForKey:@"name"];
        self.avaPath                = [NSURL URLWithString:[dict objectForKey:@"path_ava"]];
        self.status                 = [[dict objectForKey:@"status"] intValue];
        self.text                   = [dict objectForKey:@"text"];
//date
        NSString *temp = [dict objectForKey:@"time"];
//        NSLog(@"%@", temp);
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *date = [dateFormat dateFromString:temp];
        self.time           = date;
        //end Dateformating
        
    }
    return self;
}

//-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
//{
//    float oldWidth = sourceImage.size.width;
//    float scaleFactor = i_width / oldWidth;
//
//    float newHeight = sourceImage.size.height * scaleFactor;
//    float newWidth = oldWidth * scaleFactor;
//
//    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
//    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newImage;
//}

//request by id
//[[GBServerManager sharedManager] getFileList:feed.files
//                                             success:^(id responseObject) {
//                                                 NSArray *arrayResponse = [responseObject objectForKey:@"response"];
//                                                 NSDictionary *dictFileProperty = /*[[NSDictionary alloc] init];
//                                                                                   dictFileProperty = */[arrayResponse objectAtIndex:0];
//                                                                           NSLog(@"dictFileProperty:%@", dictFileProperty);
//
//                                                 NSString *endPath = [dictFileProperty objectForKey:@"path"];
//                                                 endPath = [@"/" stringByAppendingString:endPath];
//                                                 NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:endPath]];
//                                                 NSLog(@"URLFull:%@", url);
//       [cell.image setImageWithURL:url];
//                                                 NSLog(@"Cell %d, response OK", row);
//    }
//                                         failure:^(NSError *error, NSDictionary *userInfo) {
//        NSLog(@"error: %@, userInfo: %@", error, userInfo);
//    }];
//
//    }else{
//
//    }


@end
