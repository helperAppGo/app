//
//  GBQuestionsDataSource.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBQuestionsDataSource.h"

@implementation GBQuestionsDataSource

+ (void)getQuestionsWithLimit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure {
    [[GBServerManager sharedManager] questionsWithLimit:(int)limit
                                                 offset:(int)offset
                                                success:^(id responseObject) {
                                                    NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
                                                    //NSLog(@"responce question %@", responseObject);
                                                    NSDictionary *feedDictionary = responseObject;
                                                    NSArray *feedArray = [[NSArray alloc] init];
                                                    feedArray = [feedDictionary objectForKey:@"response"];
                                                    for (NSDictionary *dict in feedArray) {
                                                        [feedArrayForReturn addObject:dict];
                                                    }
                                                    
                                                    if (completion) {
                                                        completion(feedArrayForReturn);
                                                    }
                                                }
                                                failure:^(NSError *error, NSDictionary *userInfo) {
                                                    NSLog(@"Error");
                                                    
                                                    if (failure) {
                                                        failure(error);
                                                    }
                                                }];
}

//questions
+ (void) getGeoTime:(NSTimeInterval)time limit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure {
//    NSMutableArray *geoArrayForReturn = [[NSMutableArray alloc] init];
    GBVoidBlock onGeoSent = ^{
        [[GBServerManager sharedManager] questionsGeoTime:time
                                                    limit:(int)limit
                                                   offset:(int)offset
                                                  success:^(id responseObject) {
                                                   NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
    //                                              NSLog(@"georesponce");
                                                   NSDictionary *feedDictionary = responseObject;
                                                   NSArray *feedArray = [[NSArray alloc] init];
                                                   feedArray = [feedDictionary objectForKey:@"response"];
                                                   for (NSDictionary *dict in feedArray) {
                                                       [feedArrayForReturn addObject:dict];
                                                   }
                                                   
                                                   if (completion) {
                                                       completion(feedArrayForReturn);
                                                   }
                                               }
                                               failure:^(NSError *error, NSDictionary *userInfo) {
                                                   NSLog(@"Error");
                                                   
                                                   if (failure) {
                                                       failure(error);
                                                   }
                                               }];
    };
    if (GBLocationManager.sharedManager.locationManager.location) {
        CLLocation *location = GBLocationManager.sharedManager.locationManager.location;
        [[GBServerManager sharedManager] sendLatitude:location.coordinate.latitude longitude:location.coordinate.longitude success:^(id responseObject) {
            DLog(@"Coordinates are sent:\n%@",responseObject);
            onGeoSent();
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"Failed to send coordibates: %@\n%@",error,userInfo);
            onGeoSent();
        }];
    }else{
        onGeoSent();
    }
    
}
+ (void) getAnswers:(NSInteger)questionId limit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure {
    [[GBServerManager sharedManager] answersForQuestionId:(NSInteger)questionId
                                                    limit:(NSInteger)limit
                                                   offset:(NSInteger)offset
                                                  success:^(id responseObject) {
                                                      NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
                                                      NSLog(@"Comments comes with responce:%@", responseObject);
                                                  NSLog(@"Commment responce");
                                                      NSDictionary *feedDictionary = responseObject;
                                                      NSArray *feedArray = [[NSArray alloc] init];
                                                      feedArray = [feedDictionary objectForKey:@"response"];
                                                      for (NSDictionary *dict in feedArray) {
                                                          [feedArrayForReturn addObject:dict];
                                                      }
                                                      NSLog(@"%ld Commment", feedArrayForReturn.count);
                                                      if (completion) {
                                                          completion(feedArrayForReturn);
                                                      }
                                                  }
                                                  failure:^(NSError *error, NSDictionary *userInfo) {
                                                      NSLog(@"Error");
                                                      if (failure) {
                                                          failure(error);
                                                      }
                                                      }];
    
}

+ (void) returnCategoriListandCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure {
//+ (NSArray*) returnCategoryList{
//
    [[GBServerManager sharedManager] questionsCategoriesSuccess:^(id responseObject) {
//         NSMutableArray *array = [[NSMutableArray alloc] init];
        NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
//        NSLog(@"%@", responseObject);
//        NSLog(@"Commment responce");
        NSDictionary *categoryDictionary = [[NSDictionary alloc] init];
        categoryDictionary = responseObject;
        NSArray *feedArray = [[NSArray alloc] init];
        feedArray = [categoryDictionary objectForKey:@"response"];
        for (NSDictionary *dict in feedArray) {
            [feedArrayForReturn addObject:dict];
        }
        NSLog(@"%ld Commment", feedArrayForReturn.count);
        if (completion) {
            completion(feedArrayForReturn);
        }
        
    } failure:^(NSError *error, NSDictionary *userInfo) {
        NSLog(@"Error can`t get questions category list");
    }];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"categoryListReturned"
//                                                        object:self
//                                                      userInfo:nil];
//    return array;
}


@end
