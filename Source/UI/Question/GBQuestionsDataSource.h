//
//  GBQuestionsDataSource.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 20.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GBServerManager.h"
#import "GBQuestionsModel.h"


@interface GBQuestionsDataSource : NSObject

+ (void)getQuestionsWithLimit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure;
+ (void) getGeoTime:(NSTimeInterval)time limit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure;
+ (void) getAnswers:(NSInteger) questionId limit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure;
+ (void) returnCategoriListandCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure;

//+ (NSArray*) returnCategoryList;

@end
