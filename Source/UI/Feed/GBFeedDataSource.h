//
//  GBFeedDataSource.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 04.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GBServerManager.h"
#import "GBFeedModel.h"


@interface GBFeedDataSource : NSObject

+ (void) getFeedPostAtTime:(NSTimeInterval)time limit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure;


+ (void) likePost:(NSInteger)postId completion:(GBBoolBlock)completion;
+ (void) likePostGroup:(NSInteger)postId completion:(GBBoolBlock)completion;


//+ (void) getCommentsForJustId:(NSInteger)justId andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure;
//+ (NSArray*) getFeedPostsAtTime:(NSTimeInterval) time andLimit:(NSInteger) limit withOffset:(NSInteger) offset;

@end
