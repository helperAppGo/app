//
//  GBFeedModel.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 04.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFeedModel.h"
//#import "GBFeedDataSource.h"




@implementation GBFeedModel

- (instancetype)initWithDictionary:(NSDictionary*) dict
{
    self = [super init];
    if (self) {
        self.countComments   = [[dict objectForKey:@"count_comments"] intValue];
        self.countLikes      = [[dict objectForKey:@"count_likes"] intValue];
        
        NSString *fileString = [dict objectForKey:@"files"];
        if ([dict objectForKey:@"file_objects"])
        {
            NSArray *responceFileObjects = [dict objectForKey:@"file_objects"];
            self.imageInfo = [[GBFeedImageInfo alloc] initWithDictionaryInArray:responceFileObjects];
#pragma - mark Fix#2
            if(self.imageInfo.height == 0){
                self.isNoImage = YES;
            }
            
        }
        else
        {
            self.isNoImage = YES;
            
        }
        
        
        if(!self.isNoImage){
            NSInteger fileIdInt = [fileString integerValue];
            self.files = @[@(fileIdInt)];
            
        }
        else if([fileString containsString:@","]){
            self.files = [fileString componentsSeparatedByString:@","];
            NSLog(@"Send File array to VC array:%@", fileString);
        }
        else{
            //            NSLog(@"NO file and no array", fileString);
        }
        //        self.files = @[[dict objectForKey:@"files"]];
        ////        [[NSArray alloc] initWithObjects:fileString, nil]; ;
        self.justId         = [[dict objectForKey:@"id"] intValue];
        self.idAd           = [[dict objectForKey:@"id_ad"] intValue];
        self.idUser         = [[dict objectForKey:@"id_user"] intValue];
        self.idWall         = [[dict objectForKey:@"id_wall"] intValue];
        self.isLiked        = [[dict objectForKey:@"is_liked"] intValue];
        self.countLikes     = [[dict objectForKey:@"count_likes"] intValue];
        self.isPublic       = [[dict objectForKey:@"is_public"] intValue];
        self.name           = [dict objectForKey:@"name"];
        self.text           = [dict objectForKey:@"text"];
        self.avaPath        = [NSURL URLWithString:[dict objectForKey:@"path_ava"]];
        NSString *temp = [dict objectForKey:@"time"];
        //        NSLog(@"%@", temp);
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
        NSDate *date = [dateFormat dateFromString:temp];
        
        
        
        self.time           = date;
    }
    return self;
}

//-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
//{
//    float oldWidth = sourceImage.size.width;
//    float scaleFactor = i_width / oldWidth;
//
//    float newHeight = sourceImage.size.height * scaleFactor;
//    float newWidth = oldWidth * scaleFactor;
//
//    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
//    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
//    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return newImage;
//}

//request by id
//[[GBServerManager sharedManager] getFileList:feed.files
//                                             success:^(id responseObject) {
//                                                 NSArray *arrayResponse = [responseObject objectForKey:@"response"];
//                                                 NSDictionary *dictFileProperty = /*[[NSDictionary alloc] init];
//                                                                                   dictFileProperty = */[arrayResponse objectAtIndex:0];
//                                                                           NSLog(@"dictFileProperty:%@", dictFileProperty);
//
//                                                 NSString *endPath = [dictFileProperty objectForKey:@"path"];
//                                                 endPath = [@"/" stringByAppendingString:endPath];
//                                                 NSURL *url = [NSURL URLWithString:[BASE_URL stringByAppendingString:endPath]];
//                                                 NSLog(@"URLFull:%@", url);
//       [cell.image setImageWithURL:url];
//                                                 NSLog(@"Cell %d, response OK", row);
//    }
//                                         failure:^(NSError *error, NSDictionary *userInfo) {
//        NSLog(@"error: %@, userInfo: %@", error, userInfo);
//    }];
//
//    }else{
//
//    }

- (void) likeAction {
    
    if(self.isLiked){
        
        self.isLiked = FALSE;
        self.countLikes--;
        //post liked
    }
    else
    {
        self.isLiked = TRUE;
        self.countLikes++;
        //Delete POST
    }
}

@end

