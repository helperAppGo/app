//
//  GBFeedDataSource.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 04.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFeedDataSource.h"

@implementation GBFeedDataSource

+ (void) likePost:(NSInteger)postId completion:(GBBoolBlock)completion
{
    if(postId>0){
        
        [[GBServerManager sharedManager] likeWallPostId:postId success:^(id responseObject) {
            DLog(@"like post %ld succes", postId );
            if (completion) completion(YES);
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"fail like post %ld", postId );
            if (completion) completion(NO);
        }];
    }
    else{
        [[GBServerManager sharedManager] likeGroupPostId:postId success:^(id responseObject) {
            DLog(@"like post %ld succes", postId );
            if (completion) completion(YES);
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"fail like post %ld", postId );
            if (completion) completion(NO);
        } ];
    }
}

+ (void) getFeedPostAtTime:(NSTimeInterval)time limit:(NSInteger)limit offset:(NSInteger)offset andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure {
    [[GBServerManager sharedManager] feedTime:time
                                        limit:(int)limit
                                       offset:(int)offset
                                      success:^(id responseObject) {
                                                      NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
                                                    DLog(@"Feed responce");
//                                          DLog(@"RESPONCE!!!!!!!!!!!!!!!! %@", responseObject);
                                                      NSDictionary *feedDictionary = responseObject;
                                                      NSArray *feedArray = [[NSArray alloc] init];
                                                      feedArray = [feedDictionary objectForKey:@"response"];
                                                      for (NSDictionary *dict in feedArray) {
                                                          [feedArrayForReturn addObject:dict];
                                                      }
                                                      
                                                      if (completion) {
                                                          completion(feedArrayForReturn);
                                                      }
                                                  }
                                                  failure:^(NSError *error, NSDictionary *userInfo) {
                                                      DLog(@"Error");
                                                      if (failure) {
                                                          failure(error);
                                                      }
                                                  }];
    
};

+ (void) likePostGroup:(NSInteger)postId completion:(GBBoolBlock)completion
{
    [[GBServerManager sharedManager] likeGroupPostId:(int)postId success:^(id responseObject) {
        DLog(@"like post %ld succes", postId );
        if (completion) completion(YES);
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"fail like post %ld", postId );
        if (completion) completion(NO);
    } ];
}

//+ (void) getCommentsForJustId:(NSInteger)justId andCompletion:(void(^)(NSArray *))completion failure:(void(^)(NSError *))failure{
//   double timeSince70 = [[NSDate date]timeIntervalSince1970];
//    if (justId>0){
//        [[GBServerManager sharedManager] commentsWallPostId:justId
//                                                       time:timeSince70
//                                                      limit:0 offset:0
//                                                    success:^(id responseObject) {
//
//                                                        NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
//                                                        DLog(@"Feed responce");
//                                                        //                                          DLog(@"RESPONCE!!!!!!!!!!!!!!!! %@", responseObject);
//                                                        NSDictionary *feedDictionary = responseObject;
//                                                        NSArray *feedArray = [[NSArray alloc] init];
//                                                        feedArray = [feedDictionary objectForKey:@"response"];
//                                                        for (NSDictionary *dict in feedArray) {
//                                                            [feedArrayForReturn addObject:dict];
//                                                        }
//
//                                                        if (completion) {
//                                                            completion(feedArrayForReturn);
//                                                        }
//                                                    } failure:^(NSError *error, NSDictionary *userInfo) {
//                                                        DLog(@"Faluer to get user Wall post");
//                                                    }];
//    }
//    else{
//        [[GBServerManager sharedManager] commentsGroupPostId:justId
//                                                        time:timeSince70
//                                                       limit:7
//                                                      offset:0
//                                                     success:^(id responseObject) {
//                                                         NSMutableArray *feedArrayForReturn = [[NSMutableArray alloc] init];
//                                                         DLog(@"Feed responce");
//                                                         //                                          DLog(@"RESPONCE!!!!!!!!!!!!!!!! %@", responseObject);
//                                                         NSDictionary *feedDictionary = responseObject;
//                                                         NSArray *feedArray = [[NSArray alloc] init];
//                                                         feedArray = [feedDictionary objectForKey:@"response"];
//                                                         for (NSDictionary *dict in feedArray) {
//                                                             [feedArrayForReturn addObject:dict];
//                                                         }
//
//                                                         if (completion) {
//                                                             completion(feedArrayForReturn);
//                                                         }
//                                                     }
//                                                     failure:^(NSError *error, NSDictionary *userInfo) {
//                                                         DLog(@"Faluer to get group Post Coments");
//                                                     }];
//    }
//
//
//}





@end
