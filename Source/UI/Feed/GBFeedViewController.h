//
//  GBFeedViewController.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 03.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBStreamViewController.h"
#import "GBFeedModel.h"
#import "GBFeedDataSource.h"
#import "UIImageView+AFNetworking.h"
#import "GBFeedImageInfo.h"


@interface GBFeedViewController : GBStreamViewController <UITableViewDataSource, UITableViewDelegate>

@end
