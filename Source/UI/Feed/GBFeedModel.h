//
//  GBFeedModel.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 04.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBStreamItemModel.h"
#import "GBFeedImageInfo.h"

@interface GBFeedModel : GBStreamItemModel

@property (assign, nonatomic) NSInteger countComments;
@property (assign, nonatomic) NSInteger countLikes;
@property (strong, nonatomic) NSArray *files;
@property (assign, nonatomic) int64_t idAd;
@property (assign, nonatomic) int64_t idWall;
@property (assign, nonatomic) BOOL isLiked;
@property (assign, nonatomic) NSInteger isPublic;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSURL    *avaPath;
@property (strong, nonatomic) NSDate   *time;
@property (strong, nonatomic) NSString *text;
@property (assign, nonatomic) BOOL isNoImage;
@property (assign, nonatomic) BOOL isNoText;
@property (strong, nonatomic) NSDictionary *filesObject;
@property (strong, nonatomic) GBFeedImageInfo *imageInfo;

- (instancetype)initWithDictionary:(NSDictionary*) dict;
- (void) likeAction;


@end

