//
//  GBFeedImageInfo.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 08.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFeedImageInfo.h"

@implementation GBFeedImageInfo

- (instancetype) initWithDictionaryInArray:(NSArray*) array {
    NSDictionary *dictionary = [array objectAtIndex:0];
    NSString *path ;
    path = [dictionary objectForKey:@"path"];
    self.Url = [NSURL URLWithString:path];
    self.height = [[dictionary objectForKey:@"height"] floatValue];
    self.width = [[dictionary objectForKey:@"width"] floatValue];
    self.imageID = [[dictionary objectForKey:@"id"] intValue];
    self.type = [dictionary objectForKey:@"type"];
    [self changeHeightForAspectRatio];
    //    NSLog(@"%@ \n %f %f", path, height, width);
    return self;
}

- (void) changeHeightForAspectRatio{
    float oldWidth = self.width;
    if (oldWidth == 0.f) {
        self.loadError = 1;
        UIImage *image = [[UIImage alloc] init];
        image = [UIImage imageNamed:@"imageError.png"];
        self.width = image.size.width;
        self.height = image.size.height;
        oldWidth = self.width;
    }
    
    float scaleFactor = [UIScreen mainScreen].bounds.size.width / oldWidth;
    
    self.height = self.height * scaleFactor;
    self.width = self.width * scaleFactor;
}

#pragma mark - Custom Setters

- (void) setType:(NSString<Optional> *)sType
{
    _type = sType;
    if ([sType isEqualToString:@"youtube"]) {
        _eType = EAttachmentTypeYoutube;
    }else if ([sType isEqualToString:@"image"]) {
        _eType = EAttachmentTypeImage;
    }else if ([sType isEqualToString:@"video"]) {
        _eType = EAttachmentTypeVideo;
    }else{
        _eType = EAttachmentTypeUnknown;
    }
}

@end

