//
//  GBFeedImageInfo.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 08.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBFeedImageInfo : NSObject

@property (strong, nonatomic) NSURL *Url;
@property (assign, nonatomic) NSInteger imageID;
@property (strong, nonatomic) NSString *type;
@property (assign, nonatomic) CGFloat height;
@property (assign, nonatomic) CGFloat width;
@property (assign, nonatomic) CGFloat loadError;

@property (nonatomic,assign,readonly) EAttachmentType eType;

- (instancetype) initWithDictionaryInArray:(NSArray*) array;
@end


