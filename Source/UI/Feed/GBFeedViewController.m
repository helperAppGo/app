//
//  GBFeedViewController.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 03.09.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFeedViewController.h"
#import "GBServerManager.h"
#import "GBStreamItemCellDelegate.h"

#import "GBCommentsViewController.h"

#import "GBPostTableViewCell.h"

@interface GBFeedViewController ()

@property (assign, nonatomic) NSInteger offset;
@property (assign, nonatomic) NSInteger limit;
@property (strong, nonatomic) NSArray *responceArray;
@property (weak, nonatomic) IBOutlet UINavigationItem *text;
@property (assign, nonatomic) double timeForOffset;

//for IOS 9 like DeploymentTarget thereis no @property .refreshController by default in iOS 9
@property (strong, nonatomic) UIRefreshControl *refreshControl;

@end

@implementation GBFeedViewController

#pragma mark - GBStreamViewController

+ (EStreamItemType) streamItemType
{
    return EStreamItemTypeFeed;
}


#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initClassObjects];
    
    [GBPostTableViewCell registerForTableView:self.tableView];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelArray.count;
}


- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GBPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[GBPostTableViewCell cellIdentifier]];
    GBFeedModel *feed = self.modelArray[indexPath.row];
    [cell configureWithFeedItem:feed delegate:self];
    if (indexPath.row == self.offset-5) {
        [self getFeedDefaultNewTime:NO];
    }
    return cell;
}


#pragma mark - UITableViewDelegate

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 232.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger row = indexPath.row;

    GBFeedModel *feed = self.modelArray[row];
    [self openCommentsForStreamItemId:feed.justId];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void) getFeedDefaultNewTime:(BOOL) timeNew{
    
    double time = [[NSDate date] timeIntervalSince1970];
    if(timeNew){
        self.offset = 0;
        self.timeForOffset = time;
       [self.modelArray removeAllObjects];
    }
    
    [GBFeedDataSource getFeedPostAtTime:self.timeForOffset limit:25 offset:self.offset andCompletion:^(NSArray *feed) {
        self.responceArray = feed;

        for (NSDictionary *dict in self.responceArray){
            GBFeedModel *model = [[GBFeedModel alloc] initWithDictionary:dict];
            [self.modelArray addObject:model];
        };
        self.offset = self.modelArray.count;
        
        NSLog(@"Data RELOADED");
        [self.tableView reloadData];
        NSLog(@"Model in Feed array:%lu", (unsigned long)self.modelArray.count);
        /////////////////////////////
        
        if(self.refreshControl.isRefreshing){
            [self.refreshControl endRefreshing];
    }
    }failure:^(NSError *error) {
         NSLog(@"Error load modelArrayFeed");
   
}];
};
#pragma mark - Refresh
-(void)refreshTableView{
    NSLog(@"Refresh control execute code");
    //    self.responceArray =nil;
    [self getFeedDefaultNewTime:YES];
}

-(void) initClassObjects{
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    
//    self.tableView.refreshControl = [[UIRefreshControl alloc] init];
//    [self.tableView.refreshControl addTarget:self
//                                      action:@selector(refreshTableView)
//                            forControlEvents:UIControlEventValueChanged];
    
    
    
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshTableView) forControlEvents:UIControlEventValueChanged];
 
    
    
    self.modelArray = [[NSMutableArray alloc] init];
    
    
    
    self.responceArray = [[NSArray alloc] init];
    [self getFeedDefaultNewTime:YES];
    
};


#pragma mark - GBStreamItemCellDelegate

- (void) didChangeLikeValueInCell:(id<GBStreamItemCellProtocol>)aCell
{
    [super didChangeLikeValueInCell:aCell];
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:(UITableViewCell*)aCell];
    GBFeedModel *model = [(GBPostTableViewCell*)aCell fmodel];
    WEAKSELF_DECLARATION
    [GBFeedDataSource likePost:model.justId completion:^(BOOL result) {
        if (!result) {
            STRONGSELF_DECLARATION
            model.isLiked = !model.isLiked;
            model.countLikes += model.isLiked ? 1 : (-1);
            if (indexPath)
                [strongSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}





@end

