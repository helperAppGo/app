//
//  GBContact.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBContact.h"
#import "GBLocalStorage.h"

@implementation GBContact

+(GBContact *)contactFromDictionary:(NSDictionary *)dict
{
    GBContact *contact = [[GBContact alloc] init];
    contact.name = dict[@"name"];
    contact.identifier = dict[@"id"];
    contact.image_id = dict[@"image_id"];
    return contact;
}
-(BOOL)hasImage
{
    return ![self.image_id isEqualToString:@""];
}
-(void)save
{
    [GBLocalStorage storeContact:self];
}

@end
