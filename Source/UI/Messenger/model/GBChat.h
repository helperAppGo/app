//
//  GBChat.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GBMessage.h"
#import "GBContact.h"
//
// This class is responsable to store information
// displayed in ChatController
//

@interface GBChat : NSObject
@property (strong, nonatomic) GBMessage *last_message;
@property (strong, nonatomic) GBContact *contact;
@property (assign, nonatomic) NSInteger numberOfUnreadMessages;
-(NSString *)identifier;
-(void)save;
@end
