//
//  GBChat.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBChat.h"
#import "GBLocalStorage.h"

@implementation GBChat

-(NSString *)identifier
{
    return _contact.identifier;
}
-(void)setLast_message:(GBMessage *)last_message
{
    if (!_last_message)
    {
        _last_message = last_message;
    }
    else
    {
        if([_last_message.date earlierDate:last_message.date] == _last_message.date)
            _last_message = last_message;
    }
}
-(void)save
{
    [GBLocalStorage storeChat:self];
}

@end
