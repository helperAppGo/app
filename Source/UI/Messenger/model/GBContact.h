//
//  GBContact.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBContact : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *image_id;
-(BOOL)hasImage;
-(void)save;
+(GBContact *)contactFromDictionary:(NSDictionary *)dict;
+(GBContact *)queryForName:(NSString *)name;

@end
