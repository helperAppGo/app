//
//  GBChatController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBChatController.h"
#import "GBMessageController.h"
#import "GBChatCell.h"
#import "GBChat.h"
#import "GBLocalStorage.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"

#import "GIOMacro.h"

@interface GBChatController() <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData;
@end

@implementation GBChatController

@synthesize fullURL;
@synthesize dicResponse;
@synthesize usersArray, messagesArray;

-(void)viewDidLoad {
    [super viewDidLoad];
    [self setTableView];
    [self setTest];
    [self retrieveData];
    
    self.title = @"Chats";
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


//-(void)viewDidAppear:(BOOL)animated {
//    [self parseLastDialogs];
//}


-(void)setTableView {
    self.tableData = [[NSMutableArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f,self.view.frame.size.width, 10.0f)];
    self.tableView.backgroundColor = [UIColor clearColor];
}





//
// PARSE DIALOGS LAST ---------------------------------------------
//
- (void) retrieveData {
    usersArray = [[NSMutableArray alloc] init];
    messagesArray = [[NSMutableArray alloc] init];
    
//    for (int i = 0; i < dicResponse.count; i++) {
//        NSString *bDayU = [[[[dicResponse objectAtIndex:i] objectForKey:@"response"] objectForKey:@"users"]objectForKey:@"b_day"];
    
        
        
        
    
    
        
//                         valueForKey:@"response"]valueForKey:@"messages"] valueForKey:@"id"];
        
//        bDayUser;
//        bMonthUser;
//        bYearUser;
//        fileAvaUser;
//        fileCoverUser;
//        firstNameUser;
//        idUser;
//        idCityUser;
//        infoUser;
//        langUser;
//        lastNameUser;
//        pathAvaUser;
//        pathCoverUser;
//        statusUser;
        
    
    
//    }
}



//
// TEST (SHOW IN VIEW)---------------------------------------------
//

- (void)setTest {
    GBContact *contact = [[GBContact alloc] init];
    contact.name = @"Player 1";
    contact.identifier = @"12345";
    
    GBChat *chat = [[GBChat alloc] init];
    chat.contact = contact;
    
    NSArray *texts = @[@"Hello!",
                       @"This project try to implement a chat UI similar to Whatsapp app.",
                       @"Is it close enough?"];
    
    GBMessage *last_message = nil;
    for (NSString *text in texts)
    {
        GBMessage *message = [[GBMessage alloc] init];
        message.text = text;
        message.sender = MessageSenderSomeone;
        message.status = MessageStatusReceived;
        message.chat_id = chat.identifier;
        
        [[GBLocalStorage sharedInstance] storeMessage:message];
        last_message = message;
    }
    
    chat.numberOfUnreadMessages = texts.count;
    chat.last_message = last_message;
    
    [self->_tableData addObject:chat];
}

#pragma mark - TableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tableData count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ChatListCell";
    GBChatCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell)
    {
        cell = [[GBChatCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.chat = [self.tableData objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GBMessageController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Message"];
    controller.chat = [self.tableData objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
