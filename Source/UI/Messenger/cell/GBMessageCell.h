//
//  GBMessageCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBMessage.h"

@interface GBMessageCell : UITableViewCell

@property (strong, nonatomic) GBMessage *message;
@property (strong, nonatomic) UIButton *resendButton;

-(void)updateMessageStatus;

//Estimate BubbleCell Height
-(CGFloat)height;

@end
