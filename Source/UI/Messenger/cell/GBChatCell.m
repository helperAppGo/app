//
//  GBChatCell.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBChatCell.h"
#import "GBLocalStorage.h"

@interface GBChatCell()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *picture;
@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;
@end


@implementation GBChatCell

-(void)awakeFromNib
{
    self.picture.layer.cornerRadius = self.picture.frame.size.width/2;
    self.picture.layer.masksToBounds = YES;
    self.notificationLabel.layer.cornerRadius = self.notificationLabel.frame.size.width/2;
    self.notificationLabel.layer.masksToBounds = YES;
    self.nameLabel.text = @"";
    self.messageLabel.text = @"";
    self.timeLabel.text = @"";
}
-(void)setChat:(GBChat *)chat
{
    _chat = chat;
    self.nameLabel.text = chat.contact.name;
    self.messageLabel.text = chat.last_message.text;
    [self updateTimeLabelWithDate:chat.last_message.date];
    [self updateUnreadMessagesIcon:chat.numberOfUnreadMessages];
}
-(void)updateTimeLabelWithDate:(NSDate *)date
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.timeStyle = NSDateFormatterShortStyle;
    df.dateStyle = NSDateFormatterNoStyle;
    df.doesRelativeDateFormatting = NO;
    self.timeLabel.text = [df stringFromDate:date];
}
-(void)updateUnreadMessagesIcon:(NSInteger)numberOfUnreadMessages
{
    self.notificationLabel.hidden = numberOfUnreadMessages == 0;
    self.notificationLabel.text = [NSString stringWithFormat:@"%ld", (long)numberOfUnreadMessages];
}
-(UIImageView *)imageView
{
    return _picture;
}

@end



