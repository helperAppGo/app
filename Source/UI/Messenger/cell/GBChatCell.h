//
//  GBChatCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBChat.h"

//
// This class is the custom cell in
// ChatController

@interface GBChatCell : UITableViewCell
@property (strong, nonatomic) GBChat *chat;
-(UIImageView *)imageView;

@end
