//
//  GBChatController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGSSuperFetch.h"
#import "GBModelUser.h"
#import "GBModelMessage.h"

@interface GBChatController : UIViewController

@property (nonatomic, strong) NSString *fullURL;


- (void) retriveData;
- (void) setTest;

@property (nonatomic, weak) NSMutableArray *dicResponse;
@property (nonatomic, strong) NSMutableArray *usersArray;
@property (nonatomic, strong) NSMutableArray *messagesArray;


@end
