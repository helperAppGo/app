//
//  GBTestMessage.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/24/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBModelMessage : NSObject

@property (nonatomic, strong) NSString *filesMessage;
@property (nonatomic, strong) NSString *idMessage;
@property (nonatomic, strong) NSString *idFromMessage;
@property (nonatomic, strong) NSString *idToMessage;
@property (nonatomic, strong) NSString *isReadMessage;
@property (nonatomic, strong) NSString *textMessage;
@property (nonatomic, strong) NSString *timeMessage;

@end
