//
//  GBTestUser.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/24/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBModelUser : NSObject

@property (nonatomic, strong) NSNumber *bDayUser;
@property (nonatomic, strong) NSNumber *bMonthUser;
@property (nonatomic, strong) NSString *bYearUser;
@property (nonatomic, strong) NSString *fileAvaUser;
@property (nonatomic, strong) NSString *fileCoverUser;
@property (nonatomic, strong) NSString *firstNameUser;
@property (nonatomic, strong) NSString *idUser;
@property (nonatomic, strong) NSString *idCityUser;
@property (nonatomic, strong) NSString *infoUser;
@property (nonatomic, strong) NSString *langUser;
@property (nonatomic, strong) NSString *lastNameUser;
@property (nonatomic, strong) NSString *pathAvaUser;
@property (nonatomic, strong) NSString *pathCoverUser;
@property (nonatomic, strong) NSString *statusUser;

@end
