//
//  GBFakeLogin.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UGSSuperFetch.h"
#import "GBChatController.h"


@interface GBFakeLogin : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSNumber *idUserInt;
@property (nonatomic, strong) NSNumber *phoneCodeInt;

@property (nonatomic, strong) NSString *phone_token;

//-----chat controller----------------------------------------------------------------------------------------

@property (nonatomic, strong) NSMutableArray *jsonArray;

@property (nonatomic, strong) NSArray *userArray;
@property (nonatomic, strong) NSNumber *userID;
@property (nonatomic, strong) NSString *userFirstName;
@property (nonatomic, strong) NSString *userAvatar;

//----------------------------------------------------------------------------------------------

- (IBAction)clickButton:(id)sender;

-(void) getToken;

@property (class, readonly, strong) NSURLSession *sharedSession;

typedef void(^fetchHandleBlock)(NSData *data, NSURLResponse *response, NSError *error);

- (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion;

- (void)onRequestToken;

- (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link;

@end

