//
//  GIOGCD.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GIOGCD.h"
#pragma mark -
#pragma mark Private declarations

void GIOPerformBlockOnMainQueueWithTypeAndBlock(GIOBlockExecutionType type, GIOGCDBlock block);

void GIOPefromBlockWithQueueAndType(GIOBlockExecutionType type, dispatch_queue_t queue, GIOGCDBlock block);

#pragma mark -
#pragma mark Public implementations

void GIOPerformAsyncBlockOnMainQueue(GIOGCDBlock block) {
    GIOPerformBlockOnMainQueueWithTypeAndBlock(GIOBlockExecutionAsynchronous, block);
}

void GIOPerformSyncBlockOnMainQueue(GIOGCDBlock block) {
    GIOPerformBlockOnMainQueueWithTypeAndBlock(GIOBlockExecutionSynchronous, block);
}

void GIOPerformAsyncBlockOnBackgroundQueue(GIOGCDBlock block) {
    GIOPefromBlockWithQueueAndType(GIOBlockExecutionAsynchronous,
                                   GIODisptchQueueWithPriorityType(GIODispatchQueuePriorityBackground),
                                   block);
}

void GIOPerformSyncBlockOnBackgroundQueue(GIOGCDBlock block) {
    GIOPefromBlockWithQueueAndType(GIOBlockExecutionSynchronous,
                                   GIODisptchQueueWithPriorityType(GIODispatchQueuePriorityBackground),
                                   block);
}

void GIOPerformAsyncBlockOnLowQueue(GIOGCDBlock block) {
    GIOPefromBlockWithQueueAndType(GIOBlockExecutionAsynchronous,
                                   GIODisptchQueueWithPriorityType(GIODispatchQueuePriorityLow),
                                   block);
}

void GIOPerformSyncBlockOnLowQueue(GIOGCDBlock block) {
    GIOPefromBlockWithQueueAndType(GIOBlockExecutionSynchronous,
                                   GIODisptchQueueWithPriorityType(GIODispatchQueuePriorityLow),
                                   block);
}

dispatch_queue_t GIODisptchQueueWithPriorityType(GIODispatchQueuePriorityType type) {
    return dispatch_get_global_queue(type, 0);
}

#pragma mark -
#pragma mark Private implementations

void GIOPerformBlockOnMainQueueWithTypeAndBlock(GIOBlockExecutionType type, GIOGCDBlock block) {
    if ([NSThread isMainThread]) {
        block();
    } else {
        GIOPefromBlockWithQueueAndType(type, dispatch_get_main_queue(), block);
    }
}

void GIOPefromBlockWithQueueAndType(GIOBlockExecutionType type, dispatch_queue_t queue, GIOGCDBlock block) {
    if (!block) {
        return;
    }
    
    switch (type) {
        case GIOBlockExecutionSynchronous:
            dispatch_sync(queue, block);
            break;
            
        case GIOBlockExecutionAsynchronous:
            dispatch_async(queue, block);
            break;
            
        default:
            break;
    }
}
