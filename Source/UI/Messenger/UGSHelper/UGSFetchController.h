//
//  UGSFetchController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UGSFetchController : NSObject
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSNumber *phoneCode;
@property (nonatomic, strong) NSNumber *userID;
@property (nonatomic, strong) NSArray *feedPosts;
@property (nonatomic, strong) NSDictionary *dialogLast;


- (instancetype)init UNAVAILABLE_ATTRIBUTE;
+ (instancetype)sharedInstance;

- (void) registrationUser:(NSDictionary *)dict;

- (void) getFeed;

- (void) getLastDialogs;

@end
