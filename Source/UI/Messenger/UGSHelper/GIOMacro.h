//
//  GIOMacro.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#define GIOEmpty

#define GIOViewControllerBaseViewPropertyWithGetter(viewControllerClass, baseViewClass, propertyName) \
@interface viewControllerClass (__GIOPrivatBaseView) \
GIODefineBaseViewProrety(propertyName, baseViewClass)\
\
@end \
\
@implementation viewControllerClass (__GIOPrivatBaseView) \
\
@dynamic propertyName; \
\
GIOBaseViewGetterSyntesize(propertyName, baseViewClass)\
\
@end

#define GIODefineBaseViewProrety(propertyName, viewClass) \
@property (nonatomic, readonly) viewClass *propertyName;

#define GIOBaseViewGetterSyntesize(selector, viewClass) \
- (viewClass *)selector { \
if ([self isViewLoaded] && [self.view isKindOfClass:[viewClass class]]) { \
return (viewClass *)self.view; \
} \
\
return nil; \
}

#define kGIOStringConstantWithValue(name, value) \
static NSString * const name = @value

#define GIOPerformBlock(block, ...) \
do { \
if (block) { \
block(__VA_ARGS__); \
} \
} while(0) \

#define GIOStrongifyAndReturnIfNil(variable) \
GIOStrongifyAndReturnResultIfNil(variable, GIOEmpty)

#define GIOStrongifyAndReturnNilIfNil(variable) \
GIOStrongifyAndReturnResultIfNil(variable, nil)

#define GIOStrongifyAndReturnResultIfNil(variable, result) \
GIOStrongify(variable); \
if (!variable) { \
return result; \
}
#define GIOWeakify(variable) \
__weak __typeof(variable) __GIOWeakified_##variable = variable;

#define GIOStrongify(variable) \
__strong __typeof(variable) variable = __GIOWeakified_##variable;

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)




