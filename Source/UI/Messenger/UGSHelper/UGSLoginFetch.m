//
//  UGSLoginFetch.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "UGSLoginFetch.h"
#import "UGSFetchController.h"

kGIOStringConstantWithValue(registrationURL, "http://helper.darx.net/api/registration");
kGIOStringConstantWithValue(registrationGetCode, "http://helper.darx.net/api/registration/code");

@implementation UGSLoginFetch

+ (void)onRequestUserID:(NSDictionary *) dict {
    NSURLRequest *request = [self makeRequestWithMethod:postMethod params:dict andURL:registrationURL];
    [self getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data && !error) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:0
                                                                           error:nil];
            NSNumber *userID = responseDict[@"response"][@"id_user"];
            [[UGSFetchController sharedInstance] setUserID:userID];
            
            [self onRequestPhoneCode];
        } else {
            NSLog(@"%@", error);
        }
    }];
}

+ (void)onRequestPhoneCode {
    NSDictionary *dict = @{@"id_user": [[UGSFetchController sharedInstance] userID]};
    NSURLRequest *request = [self makeRequestWithMethod:getMethod params:dict andURL:registrationGetCode];
    [self getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data && !error) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:0
                                                                           error:nil];
            NSNumber *i = responseDict[@"response"][@"phone_code"];
            [[UGSFetchController sharedInstance] setPhoneCode:i];
            
            [self onRequestToken];
        } else {
            NSLog(@"%@", error);
        }
    }];
}

+ (void)onRequestToken {
    NSDictionary *dict = @{@"id_user" : [[UGSFetchController sharedInstance] userID],
                           @"phone_code": [[UGSFetchController sharedInstance] phoneCode]};
    
    NSURLRequest *request = [self makeRequestWithMethod:postMethod params:dict andURL:registrationGetCode];
    
    [self getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (data && !error) {
            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data
                                                                         options:0
                                                                           error:nil];
            [[UGSFetchController sharedInstance] setToken:responseDict[@"response"][@"phone_token"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"token" object:nil];
        } else {
            NSLog(@"%@", error);
        }
    }];
}


@end
