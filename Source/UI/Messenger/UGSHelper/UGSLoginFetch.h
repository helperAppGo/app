//
//  UGSLoginFetch.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "UGSSuperFetch.h"

@interface UGSLoginFetch : UGSSuperFetch

+ (void)onRequestUserID:(NSDictionary *) dict;
+ (void)onRequestPhoneCode;
+ (void)onRequestToken;

@end
