//
//  UGSSuperFetch.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "UGSSuperFetch.h"
#import "UGSFetchController.h"

@implementation UGSSuperFetch

+ (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:completion];
    
    [dataTask resume];
}

+ (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link {
    NSMutableURLRequest *request;
    if (method == postMethod) {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
        }
        
        NSError *err;
        NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:dict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&err];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonDict];
        
        return request;
    } else if (method == getMethod){
        NSArray *k = [dict allKeys];
        NSArray *v = [dict allValues];
        NSString *requestString = [NSString stringWithString:link];
        
        if (k) {
            requestString = [requestString stringByAppendingString:@"?data={"];
            for (id key in k) {
                NSString *keyString = [NSString stringWithFormat:@"\"%@\":", key];
                NSString *valueString = [NSString stringWithFormat:@"%@", [v objectAtIndex:[k indexOfObject:key]]];
                keyString = [keyString stringByAppendingString:valueString];
                requestString = [requestString stringByAppendingFormat:@"%@", keyString];
                if (key == k.lastObject) {
                    requestString = [requestString stringByAppendingString:@"}"];
                } else {
                    requestString = [requestString stringByAppendingString:@","];
                }
            }
        }
        NSLog(@"Request string: %@", requestString);
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        return request;
    } else {
        request = [NSMutableURLRequest new];
        return request;
    }
}

@end
