//
//  UGSSuperFetch.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GIOMacro.h"

@interface UGSSuperFetch : NSObject

typedef NS_ENUM(NSUInteger, method)  {
    postMethod,
    getMethod,
    deleteMethod,
};

typedef void(^fetchHandleBlock)(NSData *data, NSURLResponse *response, NSError *error);

- (instancetype)init UNAVAILABLE_ATTRIBUTE;

+ (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion;
+ (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link ;

@end


