//
//  GBFakeLogin.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBFakeLogin.h"
#import "UGSLoginFetch.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"

#import "GIOMacro.h"

#define getDataURL @"http://helper.darx.net/api/registration/code"
kGIOStringConstantWithValue(getDataURLDialogLast, "http://helper.darx.net/api/dialogs/last");

#define ID_USER 292
#define PHONE_CODE 6197

@interface GBFakeLogin ()

@end

@implementation GBFakeLogin

@synthesize phoneCodeInt, idUserInt;
@synthesize phone_token;

@synthesize jsonArray, userID, userArray, userAvatar, userFirstName;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self getToken];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getDataWithRequest:(NSURLRequest *)request andCompletion:(fetchHandleBlock)completion {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:completion];
    
    [dataTask resume];
}

- (NSURLRequest *)makeRequestWithMethod:(method)method params:(NSDictionary *)dict andURL:(NSString *)link {
    NSMutableURLRequest *request;
    if (method == postMethod) {
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:link]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
        }
        
        NSError *err;
        NSData *jsonDict = [NSJSONSerialization dataWithJSONObject:dict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&err];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:jsonDict];
        
        return request;
    } else if (method == getMethod){
        NSString *requestString = [NSString stringWithString:link];
        NSArray *k = [dict allKeys];
        NSArray *v = [dict allValues];
        requestString = [requestString stringByAppendingString:@"?data={"];
        for (id key in k) {
            NSString *keyString = [NSString stringWithFormat:@"\"%@\":", key];
            NSString *valueString = [NSString stringWithFormat:@"%@", [v objectAtIndex:[k indexOfObject:key]]];
            keyString = [keyString stringByAppendingString:valueString];
            requestString = [requestString stringByAppendingFormat:@"%@", keyString];
            if (key != k.lastObject)
                requestString = [requestString stringByAppendingString:@","];
        }
        requestString = [requestString stringByAppendingString:@"}"];
        requestString = [requestString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestString]];
        if ([[UGSFetchController sharedInstance] token]) {
            [request setValue:[[[UGSFetchController sharedInstance] userID] stringValue] forHTTPHeaderField:@"X-USER"];
            [request setValue:[[UGSFetchController sharedInstance] token] forHTTPHeaderField:@"X-PHONE-TOKEN"];
        }
        return request;
    } else {
        request = [NSMutableURLRequest new];
        
        return request;
    }
}



- (void) getToken {
    idUserInt = [NSNumber numberWithInteger:ID_USER];
    phoneCodeInt = [NSNumber numberWithInteger:PHONE_CODE];
    
    NSDictionary *parameters = @{@"id_user": idUserInt, @"phone_code": phoneCodeInt};
    
    NSLog(@"PARAMETERS = %@", parameters);
    
    NSString *post = [NSString stringWithFormat:@"%@", parameters];
    NSLog(@"POST = %@", post);
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    NSLog(@"JSON = %@", jsonData);
    NSString *postLength = [NSString stringWithFormat:@"lu", (unsigned long)[jsonData length]];
    NSURL *url = [NSURL URLWithString:getDataURL];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // get the value for the CONTENT-TYPE from postman
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:jsonData];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                if (!error) {
                                                    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    
                                                    [[UGSFetchController sharedInstance] setToken:responseDict[@"response"][@"phone_token"]];
                                                    
                                                    NSLog(@"TOKEN: %@", responseDict);
                                                    
                                                    NSString *TOKENSTRING = [[responseDict objectForKey:@"response"]objectForKey:@"phone_token"];
                                                    
                                                    NSLog(@"TOKEN STRING: %@", TOKENSTRING);
                                                } else {
                                                    NSLog(@"%@", error);
                                                    return;
                                                }
                                                //                                                if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                                                //                                                    NSLog(@"Response HTTP Status code: %ld\n", (long) [(NSHTTPURLResponse *) response statusCode]);
                                                //                                                    NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *) response allHeaderFields]);
                                                //                                                }
                                                //
                                                //                                                NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                                //                                                NSLog(@"Responcse Body:\n%@\n", body);
                                                
                                            }];
    
    [task resume];
    
    //
    // send userID and phoneCode to FetchController
    //
    [[UGSFetchController sharedInstance] setUserID:idUserInt];
    [[UGSFetchController sharedInstance] setPhoneCode:phoneCodeInt];
    
    NSLog(@"SET USER ID - %@", [[UGSFetchController sharedInstance] userID]);
    NSLog(@"SET PHONE CODE - %@", [[UGSFetchController sharedInstance] phoneCode]);
}

- (IBAction)clickButton:(id)sender {
    NSLog(@"GO TO MESSENGERS");
    
    NSURLRequest *request = [self makeRequestWithMethod:getMethod params:NULL andURL:getDataURLDialogLast];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
        
    [self getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                        options:kNilOptions
                                                          error:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSegueWithIdentifier:@"goToChatController" sender:self];
        }
                       );
        NSLog(@"JSON ARRAY - %@", jsonArray);
        
        
//        userArray = [[[jsonArray valueForKey:@"response"]valueForKey:@"messages"] valueForKey:@"id"];
//            
//        NSLog(@"1234567 %@", userArray);
        
        
        
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"goToChatController"]) {
        GBChatController *GBCC = [segue destinationViewController];
        
        GBCC.dicResponse = jsonArray;
    }
}


@end
