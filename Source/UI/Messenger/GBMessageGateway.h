//
//  GBMessageGateway.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBMessageGateway.h"
#import "GBMessage.h"
#import "GBChat.h"

@protocol GBMessageGatewayDelegate;

//
// this class is responsable to send message
// to server and notify status. It's also responsable
// to get messages in local storage.
//
@interface GBMessageGateway : NSObject
@property (assign, nonatomic) id<GBMessageGatewayDelegate> delegate;
@property (strong, nonatomic) GBChat *chat;
+(id)sharedInstance;
-(void)loadOldMessages;
-(void)sendMessage:(GBMessage *)message;
-(void)news;
-(void)dismiss;
@end


@protocol GBMessageGatewayDelegate <NSObject>
-(void)gatewayDidUpdateStatusForMessage:(GBMessage *)message;
-(void)gatewayDidReceiveMessages:(NSArray *)array;
@end
