//
//  GBMessageController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBChat.h"

//
// This class control chat exchange message itself
// It creates the bubble UI
//

@interface GBMessageController : UIViewController
@property (strong, nonatomic) GBChat *chat;
@end

