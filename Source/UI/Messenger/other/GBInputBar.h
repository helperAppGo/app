//
//  GBInputBar.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GBInputBarDelegate;

@interface GBInputBar : UIToolbar

@property (nonatomic, assign) id<GBInputBarDelegate>delegate;
@property (nonatomic) NSString *placeholder;
@property (nonatomic) UIImage *leftButtonImage;
@property (nonatomic) NSString *rightButtonText;
@property (nonatomic) UIColor  *rightButtonTextColor;

-(void)resignFirstResponder;
-(NSString *)text;

@end


@protocol GBInputBarDelegate <NSObject>
-(void)inputbarDidPressRightButton:(GBInputBar *)inputbar;
-(void)inputbarDidPressLeftButton:(GBInputBar *)inputbar;
@optional
-(void)inputbarDidChangeHeight:(CGFloat)new_height;
-(void)inputbarDidBecomeFirstResponder:(GBInputBar *)inputbar;
@end

