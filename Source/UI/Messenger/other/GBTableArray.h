//
//  GBTableArray.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GBMessage.h"

//
// This class teaches tableView how to interact with
// dictionaty. Basically it's mimics the array use in
// a tableView.
//

@interface GBTableArray : NSObject

-(void)addObject:(GBMessage *)message;
-(void)addObjectsFromArray:(NSArray *)messages;
-(void)removeObject:(GBMessage *)message;
-(void)removeObjectsInArray:(NSArray *)messages;
-(void)removeAllObjects;
-(NSInteger)numberOfMessages;
-(NSInteger)numberOfSections;
-(NSInteger)numberOfMessagesInSection:(NSInteger)section;
-(NSString *)titleForSection:(NSInteger)section;
-(GBMessage *)objectAtIndexPath:(NSIndexPath *)indexPath;
-(GBMessage *)lastObject;
-(NSIndexPath *)indexPathForLastMessage;
-(NSIndexPath *)indexPathForMessage:(GBMessage *)message;

@end
