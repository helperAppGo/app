//
//  GBLocalStorage.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBLocalStorage.h"

@interface GBLocalStorage ()
@property (strong, nonatomic) NSMutableDictionary *mapChatToMessages;
@end

@implementation GBLocalStorage

+(id)sharedInstance
{
    static GBLocalStorage *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
-(id)init
{
    self = [super init];
    if (self)
    {
        self.mapChatToMessages = [[NSMutableDictionary alloc] init];
    }
    return self;
}
-(void)storeMessage:(GBMessage *)message
{
    [self storeMessages:@[message]];
}
-(void)storeMessages:(NSArray *)messages
{
    if (messages.count == 0) return;
    GBMessage *message = messages[0];
    NSString *chat_id = message.chat_id;
    NSMutableArray *array = (NSMutableArray *)[self queryMessagesForChatID:chat_id];
    if (array)
    {
        [array addObjectsFromArray:messages];
    }
    else
    {
        array = [[NSMutableArray alloc] initWithArray:messages];
    }
    [self.mapChatToMessages setValue:array forKey:chat_id];
}
-(NSArray *)queryMessagesForChatID:(NSString *)chat_id
{
    return [self.mapChatToMessages valueForKey:chat_id];
}

+(void)storeChat:(GBChat *)chat
{
    //TODO
}
+(void)storeChats:(NSArray *)chats
{
    //TODO
}
+(void)storeContact:(GBContact *)contact
{
    //TODO
}
+(void)storeContacts:(NSArray *)contacts
{
    //TODO
}

@end
