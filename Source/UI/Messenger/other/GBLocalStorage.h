//
//  GBLocalStorage.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 7/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GBMessage.h"
#import "GBChat.h"

//
// This class is responsable to store messages
// For now, it stores in memory only
//

@interface GBLocalStorage : NSObject
+(id)sharedInstance;
+(void)storeChat:(GBChat *)chat;
+(void)storeChats:(NSArray *)chats;
+(void)storeContact:(GBContact *)contact;
+(void)storeContacts:(NSArray *)contacts;
+(void)storeMessage:(GBMessage *)message;
+(void)storeMessages:(NSArray *)messages;
-(NSArray *)queryMessagesForChatID:(NSString *)chat_id;
@end
