//
//  GBContactsViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 30.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBContactsViewController.h"
#import "GBContactsManager.h"


@interface GBContactsViewController ()

@end

@implementation GBContactsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[GBContactsManager sharedManager] sync:NO];
}



@end
