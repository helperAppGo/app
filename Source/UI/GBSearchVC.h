//  Created by Sergey Gaponov on 7/26/17.
//  Copyright © 2017 SG. All rights reserved.

#import <UIKit/UIKit.h>

@interface GBSearchVC : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBB;
- (IBAction)backBBAction:(id)sender;

@end
