//  TabViewController.h
//  GoBeside

#import <UIKit/UIKit.h>
#import "GBUser.h"

@interface TabViewController :UITabBarController  <UITabBarDelegate, UITabBarControllerDelegate>

@property (nonatomic, strong) GBUser *user;

@end
