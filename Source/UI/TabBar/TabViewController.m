//  TabViewController.m
//  GoBeside

#import "TabViewController.h"
#import "GBDataFacade.h"
#import "GBNavigationManager.h"
#import "GBProfileViewController.h"

@interface TabViewController ()

@end

@implementation TabViewController


- (void)viewDidLoad	
{
    [super viewDidLoad];
    
    self.delegate = self;
    
//    NAVIGATION_MANAGER.tabViewController = self;

//    [self registration]; /*Sample requests registration*/
//    [self getUserSamle]; /*Sample request getUser*/
//    [self loadData];
}


- (void)registration
{
//    [GBCommonData setUserId:@"5"];
//    [GBCommonData setToken:@"d9317b862d38ee1b84d5c303e454415775803989b28da255272460c5c9f3176d"];
    
//    NSDictionary *testRegParamsDict = @{@"qweqweqwe":@"First_name",
//                                        @"asdasdasd":@"Last_name",
//                                        @"16273512653":@"Phone"
//                                        };
//    
//    [DATA_FACADE registrationWithParams:testRegParamsDict completionBlock:^(NSData *data, NSURLResponse *response, NSError *error)
//    {
//        if (data && !error)
//        {
//            NSLog(@"%@", response);
//        }
//        else
//        {
//            NSLog(@"%@", error);
//        }
//    }];
}

//- (void)getUserSamle
//{
//    __weak typeof(self) weakSelf = self;
//    [DATA_FACADE getUserById:@"userID" completionBlock:^(id resObject, NSError *error)
//     {
//         GBUser *loaded = [resObject firstObject];
//         weakSelf.user = loaded;
//     }];
//}

- (void)loadData
{
    
    NSDictionary *dict = @{@"1500950239":@"time",
                           @"25":@"limit",
                           @"0":@"offset"};
    
    [DATA_FACADE getLastQuestionsByParams:dict completionBlock:^(id response, NSError *error)
     {
         
     }];
    //    [DATA_FACADE getNewsfeedPage:page.intValue onPage:20 completionBlock:^(id resObject, NSError *error)
    //     {
    //         [weakSelf handleResponseWithModels:resObject[@"news"] error:error ofPage:page];
    //     }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

#pragma mark - TabBar delegate

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(nonnull UIViewController *)viewController
{
    UINavigationController *nav = (id)viewController;
    
    if ([nav isKindOfClass:[UINavigationController class]])
    {
        viewController = nav.viewControllers.firstObject;
    }

    return YES;
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(nonnull UIViewController *)viewController
{
    UINavigationController *nav = (id)viewController;
    
    if ([nav isKindOfClass:[UINavigationController class]])
    {
        viewController = nav.viewControllers.firstObject;
    }
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    
}

@end
