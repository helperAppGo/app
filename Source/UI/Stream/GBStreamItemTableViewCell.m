//
//  GBStreamItemTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 21.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBStreamItemTableViewCell.h"

@implementation GBStreamItemTableViewCell

#pragma mark - GBStreamItemCellProtocol

- (int64_t) streamItemId
{
    dAssert(NO, @"Method should be overridden");
    return 0;
}

- (int64_t) userId
{
    dAssert(NO, @"Method should be overridden");
    return 0;
}

- (void) onBtnComment:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressCommentButtonInCell:)])
        [self.delegate didPressCommentButtonInCell:self];
}

- (void) onBtnMore:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressMoreButtonInCell:)])
        [self.delegate didPressMoreButtonInCell:self];
}

- (IBAction) onBtnLike:(UIButton*)sender
{
    
}

- (IBAction) onBtnShare:(UIButton*)sender
{
    
}

- (IBAction) onBtnAvatar:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(didPressAvatarInCell:)])
        [self.delegate didPressAvatarInCell:self];
}

@end
