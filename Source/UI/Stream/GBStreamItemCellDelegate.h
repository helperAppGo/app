//
//  GBStreamItemCellDelegate.h
//  GoBeside
//
//  Created by Ruslan Mishin on 21.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

@class GBStreamItemTableViewCell;

@protocol GBStreamItemCellProtocol <NSObject>

- (int64_t) streamItemId;
- (int64_t) userId;

- (IBAction) onBtnComment:(id)sender;
- (IBAction) onBtnMore:(id)sender;
- (IBAction) onBtnShare:(UIButton*)sender;
- (IBAction) onBtnAvatar:(id)sender;

- (IBAction) onBtnLike:(UIButton*)sender;
- (IBAction) onBtnLocation:(UIButton*)sender;

@end

@protocol GBStreamItemCellDelegate <NSObject>

- (void) didPressAvatarInCell:(id<GBStreamItemCellProtocol>)aCell;
- (void) didPressCommentButtonInCell:(id<GBStreamItemCellProtocol>)aCell;
- (void) didPressMoreButtonInCell:(id<GBStreamItemCellProtocol>)aCell;
- (void) didChangeLikeValueInCell:(id<GBStreamItemCellProtocol>)aCell;

@end
