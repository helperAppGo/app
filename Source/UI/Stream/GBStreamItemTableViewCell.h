//
//  GBStreamItemTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 21.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBStreamItemCellDelegate.h"

@interface GBStreamItemTableViewCell : UITableViewCell <GBStreamItemCellProtocol>

@property (nonatomic,weak) id<GBStreamItemCellDelegate> delegate;

@end
