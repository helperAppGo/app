//
//  GBStreamViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 21.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBStreamItemCellDelegate.h"

@interface GBStreamViewController : UIViewController <GBStreamItemCellDelegate>

@property (nonatomic, strong) NSMutableArray *modelArray;
@property (nonatomic, weak) IBOutlet UITableView *tableView;

+ (EStreamItemType) streamItemType;

- (void) openCommentsForStreamItemId:(int64_t)aStreamItemId;

@end
