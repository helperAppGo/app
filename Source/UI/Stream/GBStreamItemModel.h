//
//  GBStreamItemModel.h
//  GoBeside
//
//  Created by Ruslan Mishin on 21.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBStreamItemModel : NSObject

@property (assign, nonatomic) int64_t justId;
@property (assign, nonatomic) int64_t idUser;

@end
