//
//  GBStreamViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 21.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBStreamViewController.h"
#import "GBCommentsViewController.h"
#import "GBInGroupViewController.h"
#import "GBProfileViewController.h"
#import "GBStreamItemTableViewCell.h"
#import "GBStreamItemModel.h"
#import "GBFeedModel.h"
#import "GBWallModel.h"
#import "GBQuestionsModel.h"
#import "GBDataHelper.h"


@interface GBStreamViewController ()

@end

@implementation GBStreamViewController

#pragma mark - Class

+ (EStreamItemType) streamItemType
{
    return EStreamItemTypeUnknown;
}


#pragma mark - Public

- (void) openCommentsForStreamItemId:(int64_t)aStreamItemId
{
    GBCommentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:aStreamItemId itemType: ([[self class] streamItemType] == EStreamItemTypeQuestions) ? ECommentItemTypeAnswer : ECommentItemTypeFeedComment];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Private

- (void) deletePostById:(int64_t)anItemId
{
    WEAKSELF_DECLARATION
    [GBDataHelper deletePost:anItemId andCompletion:^{
        STRONGSELF_DECLARATION
        NSArray *filtered = [strongSelf.modelArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"justId == %lld",anItemId]];
        if (filtered.count) {
            [strongSelf.modelArray removeObjectsInArray:filtered];
            [strongSelf.tableView reloadData];
        }
    } failure:^(NSError *err) {}];
    
}

- (void) abusePostById:(int64_t)aStreamItemId
{
    WEAKSELF_DECLARATION
    GBVoidBlock onSuccess = ^{
        STRONGSELF_DECLARATION
        NSArray *filtered = [strongSelf.modelArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"justId == %lld",aStreamItemId]];
        if (filtered.count) {
            [strongSelf.modelArray removeObjectsInArray:filtered];
            [strongSelf.tableView reloadData];
        }
    };
    
    
    EStreamItemType streamItemType = [[self class] streamItemType];
    if (streamItemType == EStreamItemTypeFeed) {
        [GBDataHelper complainPost:aStreamItemId andCompletion:^{
            onSuccess();
        } failure:^(NSError *err) {}];
    }else if (streamItemType == EStreamItemTypeQuestions) {
        [GBDataHelper complainQuestion:aStreamItemId andCompletion:^{
            onSuccess();
        } failure:^(NSError *err) {}];
    }
}

- (void) banUserById:(int64_t)userId
{
    WEAKSELF_DECLARATION
    [GBDataHelper banUser:userId andCompletion:^{
        STRONGSELF_DECLARATION
        NSArray *filtered = [strongSelf.modelArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"idUser == %lld",userId]];
        if (filtered.count) {
            [strongSelf.modelArray removeObjectsInArray:filtered];
            [strongSelf.tableView reloadData];
        }
    } failure:^(NSError *err) {}];
}

- (void) showMoreMenuForStreamItemId:(int64_t)aStreamItemId
{
    NSArray *filtered = nil;
    if ([self isKindOfClass:[GBInGroupViewController class]]) {
        filtered = [self.modelArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"idCountWall == %lld",aStreamItemId]];
    }else{
        filtered = [self.modelArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"justId == %lld",aStreamItemId]];
    }
    if (!filtered.count) return;
    id model = [filtered firstObject];
//    WEAKSELF_DECLARATION
    
    int64_t userId = 0;
    BOOL showBanUser = NO;
    if ([model isKindOfClass:[GBQuestionsModel class]]) {
        showBanUser = YES;
        userId = [(GBQuestionsModel*)model idUser];
    }else if ([model isKindOfClass:[GBFeedModel class]]) {
        GBFeedModel *feedModel = (GBFeedModel*)model;
        showBanUser = (feedModel.idUser == feedModel.idWall);
        userId = feedModel.idUser;
    }else if ([model isKindOfClass:[GBWallModel class]]) {
        GBWallModel *wallModel = (GBWallModel*)model;
        showBanUser = ([wallModel.idUser longLongValue] == [wallModel.idWall longLongValue]);
        userId = [wallModel.idUser longLongValue];
    }
    
//    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
//    if (userId == CURRENT_USER.objId.longLongValue) {
//        UIAlertAction *actionDeletePost = [UIAlertAction actionWithTitle:NSLocalizedString(@"Видалити пост", nil)
//                                                                   style:UIAlertActionStyleDestructive
//                                                                 handler:^(UIAlertAction * _Nonnull action) {
//                                                                     STRONGSELF_DECLARATION
//                                                                     [strongSelf deletePostById:aStreamItemId];
//                                                                 }];
//        [alertController addAction:actionDeletePost];
//    }else{
//        UIAlertAction *actionReportFeed = [UIAlertAction actionWithTitle:NSLocalizedString(@"Поскаржитись на пост", nil)
//                                                                   style:UIAlertActionStyleDefault
//                                                                 handler:^(UIAlertAction * _Nonnull action) {
//                                                                     STRONGSELF_DECLARATION
//                                                                     [strongSelf abusePostById:aStreamItemId];
//                                                                 }];
//        [alertController addAction:actionReportFeed];
    
        
        
//        if (showBanUser) {
//            UIAlertAction *actionBanUser = [UIAlertAction actionWithTitle:NSLocalizedString(@"Заблокувати користувача", nil)
//                                                                    style:UIAlertActionStyleDefault
//                                                                  handler:^(UIAlertAction * _Nonnull action) {
//                                                                      STRONGSELF_DECLARATION
//                                                                      [strongSelf banUserById:userId];
//                                                                  }];
//            [alertController addAction:actionBanUser];
//        }
//    }
    
//    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Відміна", nil)
//                                                           style:UIAlertActionStyleCancel
//                                                         handler:^(UIAlertAction * _Nonnull action) {}];
//    [alertController addAction:actionCancel];
//
//    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - GBStreamItemCellDelegate

- (void) didPressAvatarInCell:(id<GBStreamItemCellProtocol>)aCell
{
    WEAKSELF_DECLARATION
    int64_t userId = aCell.userId;
    if (userId == 0) return;
    [GBDataHelper getUserById:userId andCompletion:^(id value) {
        STRONGSELF_DECLARATION
        GBUser *user = value;
        GBProfileViewController * vc = [strongSelf.storyboard instantiateViewControllerWithIdentifier:@"GBProfileViewController"];
        //vc.hidesBottomBarWhenPushed = YES;
        [vc configureWithUser:user];
        [strongSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *err) {
        //nothing
    }];
}

- (void) didPressCommentButtonInCell:(GBStreamItemTableViewCell*)aCell
{
    [self openCommentsForStreamItemId:aCell.streamItemId];
}

- (void) didPressMoreButtonInCell:(GBStreamItemTableViewCell*)aCell
{
    [self showMoreMenuForStreamItemId:aCell.streamItemId];
}

- (void)didChangeLikeValueInCell:(id<GBStreamItemCellProtocol>)aCell {
    
}


#pragma mark - CustomGetters

- (NSMutableArray*) modelArray
{
    if (!_modelArray)
        _modelArray = [NSMutableArray array];
    return _modelArray;
}

@end
