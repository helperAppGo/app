//
//  GBBaseTableViewCellProtocol.h
//  GoBeside
//
//  Created by Ruslan Mishin on 18.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

@protocol GBBaseTableViewCellProtocol <NSObject>

@required

+ (NSString*) cellIdentifier;
+ (NSString*) nibName;

- (instancetype) initFromNib;

- (void) stopActivity;

@optional

+ (CGFloat) cellHeight;

@end
