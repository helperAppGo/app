//
//  GBBaseTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCellProtocol.h"

@interface GBBaseTableViewCell : UITableViewCell <GBBaseTableViewCellProtocol>

+ (void) registerForTableView:(UITableView*)aTableView;
- (UITableView*) tableView;

@end
