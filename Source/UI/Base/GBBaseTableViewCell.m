//
//  GBBaseTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCell.h"
#import "DAssert.h"

@implementation GBBaseTableViewCell

#pragma mark - GBBaseCellProtocol

+ (NSString*) cellIdentifier
{
    dAssert(NO, @"Method 'cellIdentifier' need to be overriden in BaseTableViewCell subclass");
    return nil;
}

+ (NSString*) nibName
{
    NSString * nibName = NSStringFromClass([self class]);
    return nibName;
}

- (instancetype) initFromNib
{
    NSArray *allComponents = [[NSBundle bundleForClass:[self class]] loadNibNamed:[[self class] nibName] owner:nil options:nil];
    for (id obj in allComponents) {
        if ( [obj isKindOfClass:[self class]] ) {
            self = obj;
            break;
        }
    }
    return self;
}

- (void) stopActivity
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (CGFloat) cellHeight
{
    return UITableViewAutomaticDimension;
}


#pragma mark - Public Class methods

+ (void) registerForTableView:(UITableView*)aTableView
{
    NSString * cellIdentifier = [self cellIdentifier];
    [aTableView registerClass:[self class] forCellReuseIdentifier:cellIdentifier];
    UINib *cellNib = [UINib nibWithNibName:[self nibName] bundle:nil];
    [aTableView registerNib:cellNib forCellReuseIdentifier:cellIdentifier];
}

#pragma mark - Public

- (UITableView*) tableView
{
    UITableView * tableView = nil;
    UIView * superView = self.superview;
    while (YES) {
        if ([superView isKindOfClass:[UITableView class]]) {
            tableView = (UITableView*)superView;
            break;
        }
        superView = superView.superview;
        if (!superView) break;
    }
    
    return tableView;
}

@end
