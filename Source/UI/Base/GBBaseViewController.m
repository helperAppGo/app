//
//  GBBaseViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"
#import "GBBaseViewController+GestureRecognizer.h"
#import "GBBaseTableViewCell.h"
#import "GBPaginationTableViewCell.h"

@interface GBBaseViewController ()

@property (nonatomic,weak,readwrite) UITapGestureRecognizer * bgndTapGestureRecognizer;
@property (nonatomic,weak,readwrite) UITapGestureRecognizer * navBarTapGestureRecognizer;

@end

@implementation GBBaseViewController

#pragma mark - Class

+ (BOOL) usePullToRefresh
{
    return NO;
}

#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    _didLoad = YES;
    
    if (_tableView)
        [GBPaginationTableViewCell registerForTableView:_tableView];
    if (_tableView && [[self class] usePullToRefresh] && [self usePullToRefreshInstance]) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc]init];
        [self.tableView addSubview:refreshControl];
        [refreshControl addTarget:self action:@selector(onPullToRefresh:) forControlEvents:UIControlEventValueChanged];
        self.refreshControl = refreshControl;
    }
    
    [self addNavBarNegativeSpacing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackgroundNotification:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForegroundNotification:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActiveNotification:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActiveNotification:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self initializeBgndTapGestureRecognizer];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self listenNotifications];
    
    if (!_didAppearOnce)
        _didAppearOnce = YES;
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeNotifications];
    [self.view endEditing:YES];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.lcBaseContentBottom.constant = 0.0;
    [self removeBgndTapGestureRecognizer];
}

- (void) dealloc
{
    [self stopActivity];
}

#pragma mark - Public

- (void) stopActivity
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    if (_tableView) {
        NSArray * visibleCells = [_tableView visibleCells];
        for (UITableViewCell * cell in visibleCells) {
            if ([cell isKindOfClass:[GBBaseTableViewCell class]])
                [(GBBaseTableViewCell*)cell stopActivity];
        }
    }
}


- (void) onPullToRefresh:(UIRefreshControl *)sender {}

- (void) stopRefreshControl
{
    if(self.refreshControl.isRefreshing)
        [self.refreshControl endRefreshing];
}

- (void) toolbarDonePressed
{
    [self.view endEditing:YES];
}

- (BOOL) usePullToRefreshInstance
{
    return YES;
}

#pragma mark - IBActions



#pragma mark - Private Methods

- (void) listenNotifications
{
    NSNotificationCenter * notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [notificationCenter addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
}

- (void) removeNotifications
{
    NSNotificationCenter * notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [notificationCenter removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)updateBottomMarginByKeyboardWithBottom:(CGFloat)aBottomMargin userInfo:(NSDictionary*)anUserInfo
{
    NSTimeInterval duration = [anUserInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIViewAnimationCurve animationCurve = [anUserInfo[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    self.lcBaseContentBottom.constant = aBottomMargin;
    [self.view setNeedsUpdateConstraints];
    WEAKSELF_DECLARATION
    [UIView animateWithDuration:duration
                          delay:0.0
                        options:animationCurve | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         STRONGSELF_DECLARATION
                         [strongSelf.view layoutIfNeeded];
                     } completion:^(BOOL finished) {}];
}

- (void) addNavBarNegativeSpacing
{
    if (!self.navigationItem) return;
    
    UIBarButtonItem * spacingLeft = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacingLeft.width = -16.0;
    NSMutableArray * leftItems = [self.navigationItem.leftBarButtonItems mutableCopy];
    [leftItems insertObject:spacingLeft atIndex:0];
    self.navigationItem.leftBarButtonItems = [leftItems copy];
    
    UIBarButtonItem * spacingRight = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    spacingRight.width = -16.0;
    NSMutableArray * rightItems = [self.navigationItem.rightBarButtonItems mutableCopy];
    [rightItems insertObject:spacingRight atIndex:0];
    self.navigationItem.rightBarButtonItems = [rightItems copy];
}

#pragma mark - NSNotifications

- (void)keyboardWillHide:(NSNotification *)aNotification
{
    NSDictionary * userInfo = aNotification.userInfo;
    [self updateBottomMarginByKeyboardWithBottom:0.0 userInfo:userInfo];
}

- (void)keyboardWillShow:(NSNotification *)aNotification
{
    NSDictionary * userInfo = aNotification.userInfo;
    CGRect frame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat height = frame.size.height - (self.tabBarController.tabBar.hidden?0.0:self.tabBarController.tabBar.bounds.size.height);
    [self updateBottomMarginByKeyboardWithBottom:height userInfo:userInfo];
}

- (void) applicationDidEnterBackgroundNotification:(NSNotification*)aNotification
{
}

- (void) applicationWillEnterForegroundNotification:(NSNotification*)aNotification
{
}

- (void) applicationWillResignActiveNotification:(NSNotification*)aNotification
{
}

- (void) applicationDidBecomeActiveNotification:(NSNotification*)aNotification
{
}

@end
