//
//  GBBasePaginationViewController+TableView.h
//  GoBeside
//
//  Created by Ruslan Mishin on 14.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBasePaginationViewController.h"

@interface GBBasePaginationViewController (TableView) <UITableViewDelegate,UITableViewDataSource>

@end
