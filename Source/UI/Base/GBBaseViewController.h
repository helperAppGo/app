//
//  GBBaseViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//


@interface GBBaseViewController : BaseViewController

@property (nonatomic,copy) GBVoidBlock onDismissCompletion;

@property (nonatomic,assign,readonly) BOOL didLoad;
@property (nonatomic,assign,readonly) BOOL didAppearOnce;

@property (nonatomic,weak) IBOutlet UITableView * tableView;
@property (nonatomic,weak) UIRefreshControl * refreshControl;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint * lcBaseContentBottom;

@property (nonatomic,weak,readonly) UITapGestureRecognizer * bgndTapGestureRecognizer;
@property (nonatomic,weak,readonly) UITapGestureRecognizer * navBarTapGestureRecognizer;

- (void) stopActivity;

//need to be overriden in subclasses. Have sense only if tableView used
//default - NO
+ (BOOL) usePullToRefresh;
- (BOOL) usePullToRefreshInstance;

- (void) stopRefreshControl;

#pragma mark - IBActions

- (void) onPullToRefresh:(UIRefreshControl *)sender;

#pragma mark - NSNotifications

- (void) keyboardWillHide:(NSNotification *)aNotification;
- (void) keyboardWillShow:(NSNotification *)aNotification;

- (void) applicationDidEnterBackgroundNotification:(NSNotification*)aNotification;
- (void) applicationWillEnterForegroundNotification:(NSNotification*)aNotification;
- (void) applicationWillResignActiveNotification:(NSNotification*)aNotification;
- (void) applicationDidBecomeActiveNotification:(NSNotification*)aNotification;

@end
