//
//  GBBasePaginationViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 14.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"

@interface GBBasePaginationViewController : GBBaseViewController

@property (nonatomic,strong) NSMutableArray *items;

@property (nonatomic,assign) BOOL requestIsActive;
@property (nonatomic,assign) BOOL endReached;

- (void) startRequestIfNeeded;
- (void) getDataWithOffset:(NSUInteger)offset;
- (void) sendDataRequestWithOffset:(NSUInteger)offset limit:(NSInteger)limit onSuccess:(GBArrayBlock)aSuccessBlock onError:(GBErrorBlock)anErrorBlock;

@end
