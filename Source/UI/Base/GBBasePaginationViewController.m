//
//  GBBasePaginationViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 14.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBasePaginationViewController.h"
#import "GBPaginationTableViewCell.h"
#import "GBBaseModel.h"

@interface GBBasePaginationViewController ()

@property (nonatomic, strong) NSMutableDictionary *itemsDict;

@end


@implementation GBBasePaginationViewController

#pragma mark - Class

+ (BOOL) usePullToRefresh
{
    return YES;
}


#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [self getDataWithOffset:0];
}


#pragma mark - Public

- (void) onPullToRefresh:(UIRefreshControl *)sender
{
    [super onPullToRefresh:sender];
    
    WEAKSELF_DECLARATION
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        STRONGSELF_DECLARATION
        [strongSelf getDataWithOffset:0];
    });
}

- (void) startRequestIfNeeded
{
    if ((!self.requestIsActive) && (!self.endReached)) {
        [self getDataWithOffset:self.items.count];
    }
}


- (void) sendDataRequestWithOffset:(NSUInteger)offset limit:(NSInteger)limit onSuccess:(GBArrayBlock)aSuccessBlock onError:(GBErrorBlock)anErrorBlock
{
    //should be implemented in subclasses
    dAssert(NO,@"method \"sendDataRequestWithOffset:limit:onSuccess:onError:\" should be overridden in subclasses");
}

- (void) getDataWithOffset:(NSUInteger)offset
{
    WEAKSELF_DECLARATION
    self.requestIsActive = YES;
    NSInteger limit = 25;
    if (offset == 0) self.endReached = NO;
    
    GBArrayBlock onSuccess = ^(NSArray *array) {
        //DLog(@"Comments received:%@", array);
        STRONGSELF_DECLARATION
        strongSelf.requestIsActive = NO;
        if (array.count < limit)
            strongSelf.endReached = YES;
        if (offset) {
            NSInteger startIdx = strongSelf.items.count;
            NSMutableArray *indexPaths = [NSMutableArray array];
            for (NSInteger idx = 0; idx < array.count; idx++) {
                GBBaseModel *model = array[idx];
                if (!strongSelf.itemsDict[model.ident]) {
                    strongSelf.itemsDict[model.ident] = model;
                    [strongSelf.items addObject:model];
                    [indexPaths addObject:[NSIndexPath indexPathForRow:(startIdx+idx) inSection:0]];
                }
            }
            if (startIdx) {
                [strongSelf.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationAutomatic];
            }else{
                [strongSelf.tableView reloadData];
            }
        }else{
            [strongSelf.items removeAllObjects];
            [strongSelf.itemsDict removeAllObjects];
            for (GBBaseModel *model in array)
                strongSelf.itemsDict[model.ident] = model;
            [strongSelf.items addObjectsFromArray:array];
            [strongSelf.tableView reloadData];
        }
    };
    
    [self sendDataRequestWithOffset:offset limit:limit onSuccess:onSuccess onError:^(NSError *error) {
        DLog(@"Failed to receive comments (%@). Error:%@", [self class], error);
        STRONGSELF_DECLARATION
        strongSelf.requestIsActive = NO;
    }];
}


#pragma mark - Custom Getters

- (NSMutableArray*)items
{
    if (!_items)
        _items = [NSMutableArray array];
    return _items;
}

- (NSMutableDictionary*)itemsDict
{
    if (!_itemsDict)
        _itemsDict = [NSMutableDictionary dictionary];
    return _itemsDict;
}


#pragma mark - Custom Setters

- (void) setRequestIsActive:(BOOL)requestIsActive
{
    _requestIsActive = requestIsActive;
    if (!requestIsActive) [self stopRefreshControl];
    NSArray *visible = self.tableView.visibleCells;
    NSArray *filtered = [visible filteredArrayUsingPredicate:[NSPredicate predicateWithFormat: @"class == %@", [GBPaginationTableViewCell class]]];
    if (filtered.count) {
        GBPaginationTableViewCell *cell = [filtered firstObject];
        cell.animating = requestIsActive;
    }
}

@end
