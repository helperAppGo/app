//
//  GBBaseViewController+GestureRecognizer.m
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController+GestureRecognizer.h"

@interface GBBaseViewController()

@property (nonatomic,weak) UITapGestureRecognizer * bgndTapGestureRecognizer;
@property (nonatomic,weak,readwrite) UITapGestureRecognizer * navBarTapGestureRecognizer;

@end

@implementation GBBaseViewController (GestureRecognizer)

#pragma mark - Public

- (void) initializeBgndTapGestureRecognizer
{
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBgndTap:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate = self;
    [self.view/*navigationController.navigationBar*/ addGestureRecognizer:tapGesture];
    self.bgndTapGestureRecognizer = tapGesture;
    
    if (self.navigationController) {
        UITapGestureRecognizer * tapGestureNavBar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBgndTap:)];
        tapGestureNavBar.cancelsTouchesInView = NO;
        tapGestureNavBar.delegate = self;
        [self.navigationController.navigationBar addGestureRecognizer:tapGestureNavBar];
        self.navBarTapGestureRecognizer = tapGestureNavBar;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onBgndTappedNotification:) name:@"BgndTappedNotification" object:nil];
}

- (void) removeBgndTapGestureRecognizer
{
    if (self.bgndTapGestureRecognizer)
        [self.view removeGestureRecognizer:self.bgndTapGestureRecognizer];
    if (self.navBarTapGestureRecognizer)
        [self.navigationController.navigationBar removeGestureRecognizer:self.navBarTapGestureRecognizer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"BgndTappedNotification" object:nil];
}

#pragma mark - Private

- (void) handleBgndTap:(UITapGestureRecognizer *)aRecognizer
{
    DLog(@"Handle tap in %@",self.class);
    WEAKSELF_DECLARATION
    dispatch_async(DISP_MAIN_THREAD, ^{
        STRONGSELF_DECLARATION
        [strongSelf.view endEditing:YES];
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:@"BgndTappedNotification" object:self];
}

- (void) onBgndTappedNotification:(NSNotification*)aNotification
{
    //DLog(@"Handle tap notification: %@",self.class);
    if (aNotification.object == self) {
        //DLog(@"ignore");
        return;
    }
    [self.view endEditing:YES];
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return (![touch.view isKindOfClass:[UIControl class]]);
}

@end
