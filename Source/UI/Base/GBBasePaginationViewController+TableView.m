//
//  GBBasePaginationViewController+TableView.m
//  GoBeside
//
//  Created by Ruslan Mishin on 14.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBasePaginationViewController+TableView.h"
#import "GBPaginationTableViewCell.h"

@implementation GBBasePaginationViewController (TableView)


#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 1) {
        return 1;
    }else{
        return self.items.count;
    }
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        GBPaginationTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[GBPaginationTableViewCell cellIdentifier]];
        [cell configureWithAnimating:self.requestIsActive];
        if (!self.requestIsActive) {
            WEAKSELF_DECLARATION
            dispatch_async(DISP_MAIN_THREAD, ^{
                STRONGSELF_DECLARATION
                [strongSelf startRequestIfNeeded];
            });
        }
        return cell;
    }else{
        dAssert(NO,@"method \"tableView:tableView cellForRowAtIndexPath:\" should be overridden in subclasses");
        return nil;
    }
}

@end
