//
//  GBBaseViewController+GestureRecognizer.h
//  GoBeside
//
//  Created by Ruslan Mishin on 16.04.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"

@interface GBBaseViewController (GestureRecognizer) <UIGestureRecognizerDelegate>

- (void) initializeBgndTapGestureRecognizer;
- (void) removeBgndTapGestureRecognizer;
- (void) handleBgndTap:(UITapGestureRecognizer *)aRecognizer;

@end
