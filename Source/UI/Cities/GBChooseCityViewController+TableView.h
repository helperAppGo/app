//
//  GBChooseCityViewController+TableView.h
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBChooseCityViewController.h"

@interface GBChooseCityViewController (TableView) <UITableViewDelegate,UITableViewDataSource>

@end
