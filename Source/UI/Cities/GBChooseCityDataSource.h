//
//  GBChooseCityDataSource.h
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBChooseCityDataSource : NSObject

- (void) requestCitiesListWithCompletion:(GBArrayBlock)aCompletion;

@end
