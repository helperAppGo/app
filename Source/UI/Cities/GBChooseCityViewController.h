//
//  GBChooseCityViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"

@interface GBChooseCityViewController : GBBaseViewController

@property (nonatomic,strong) NSArray * cities;
@property (nonatomic,assign) int64_t selectedCityId;

- (void) configureWithSelectedCityId:(int64_t)cityId completion:(GBValueBlock)aCompletion;

@end
