//
//  GBChooseCityViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBChooseCityViewController.h"
#import "GBChooseCityDataSource.h"
#import "GBCityTableViewCell.h"

@interface GBChooseCityViewController ()

@property (nonatomic, strong) GBChooseCityDataSource *dataSource;
@property (nonatomic, copy) GBValueBlock onChooseCityBlock;

@end


@implementation GBChooseCityViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    self.cities = CITIES;
    self.dataSource = [GBChooseCityDataSource new];
    [super viewDidLoad];
    
    [GBCityTableViewCell registerForTableView:self.tableView];
    self.tableView.tableFooterView = [UIView new];
    
    [self getCitiesList];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}


#pragma mark - Public

- (void) configureWithSelectedCityId:(int64_t)cityId completion:(GBValueBlock)aCompletion
{
    self.selectedCityId = cityId;
    self.onChooseCityBlock = aCompletion;
}


#pragma mark - Private

- (void)getCitiesList
{
    WEAKSELF_DECLARATION
    
    
    [self.dataSource requestCitiesListWithCompletion:^(NSArray *arr) {
        STRONGSELF_DECLARATION
        if (arr.count) {
            CITIES = arr;
            strongSelf.cities = arr;
            [strongSelf.tableView reloadData];
        }
    }];
}


#pragma mark - IBActions

- (IBAction)onBtnDone:(id)sender
{
    NSIndexPath * indexPath = [self.tableView indexPathForSelectedRow];
    if (!indexPath) return;
    
    GBCityTableViewCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
    GBCityModel * city = cell.model;
    if (self.onChooseCityBlock)
        self.onChooseCityBlock(city);
    [self.navigationController popViewControllerAnimated:YES];
    
}


@end
