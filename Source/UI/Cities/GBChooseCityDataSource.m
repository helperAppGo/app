//
//  GBChooseCityDataSource.m
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBChooseCityDataSource.h"
#import "GBServerManager.h"

@implementation GBChooseCityDataSource

- (void) requestCitiesListWithCompletion:(GBArrayBlock)aCompletion
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] citiesSuccess:^(id responseObject) {
        id response = responseObject[@"response"];
        if ([response isKindOfClass:[NSArray class]] && [(NSArray*)response count]) {
            NSError *parseError = nil;
            NSArray *cities = [GBCityModel arrayOfModelsFromDictionaries:(NSArray*)response error:&parseError];
            [SVProgressHUD dismiss];
            if (aCompletion) aCompletion(cities);
        }else{
            [SVProgressHUD showErrorWithStatus:nil];
            if (aCompletion) aCompletion(nil);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        [SVProgressHUD showErrorWithStatus:nil];
        if (aCompletion) aCompletion(nil);
    }];
}

@end
