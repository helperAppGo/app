//
//  GBChooseCityViewController+TableView.m
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBChooseCityViewController+TableView.h"
#import "GBCityTableViewCell.h"

@implementation GBChooseCityViewController (TableView)

#pragma mark - UITableViewDelegate,UITableViewDataSource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cities.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GBCityTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[GBCityTableViewCell cellIdentifier]];
    GBCityModel *city = self.cities[indexPath.row];
    [cell configureWithModel:city];
    [cell setSelected:((self.selectedCityId != 0) && (self.selectedCityId == city.objId))];
    
    return cell;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [GBCityTableViewCell cellHeight];
}

@end
