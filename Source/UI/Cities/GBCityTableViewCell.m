//
//  GBCityTableViewCell.m
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBCityTableViewCell.h"

@interface GBCityTableViewCell()

@property (nonatomic,weak) IBOutlet UILabel * lblName;
@property (nonatomic,weak) IBOutlet UIImageView * ivSelection;

@property (nonatomic,strong,readwrite) GBCityModel * model;

@end

@implementation GBCityTableViewCell

#pragma mark - GBBaseTableViewCellProtocol

+ (NSString*) cellIdentifier
{
    return @"city";
}

+ (CGFloat) cellHeight
{
    return 44.0;
}

#pragma mark - Lifecycle

- (void) drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [self drawContent];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    self.ivSelection.hidden = !selected;
}

#pragma mark - Public

- (void) configureWithModel:(GBCityModel*)aModel
{
    self.model = aModel;
    [self drawContent];
}

#pragma mark - Private

- (void) drawContent
{
    self.lblName.text = self.model.name;
    self.ivSelection.hidden = !self.selected;
}

@end
