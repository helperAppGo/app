//
//  GBCityTableViewCell.h
//  GoBeside
//
//  Created by Ruslan Mishin on 05.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseTableViewCell.h"

@interface GBCityTableViewCell : GBBaseTableViewCell

@property (nonatomic,strong,readonly) GBCityModel * model;

- (void) configureWithModel:(GBCityModel*)aModel;

@end
