//
//  GBProfileViewController+GBStreamItemCellDelegate.m
//  GoBeside
//
//  Created by Ruslan Mishin on 18.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBProfileViewController+GBStreamItemCellDelegate.h"
#import "GBCommentsViewController.h"
#import "GBStreamItemTableViewCell.h"
#import "GBDataHelper.h"

@implementation GBProfileViewController (GBStreamItemCellDelegate)


#pragma mark - Private

- (void) openCommentsForStreamItemId:(int64_t)aStreamItemId
{
    GBCommentsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:aStreamItemId itemType:ECommentItemTypeFeedComment];
    vc.type =1;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) showMoreMenuForStreamItemId:(int64_t)anItemId
{
    NSArray *filtered = [self.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"objId == %lld",anItemId]];
    if (!filtered.count) return;
    GBPostModel * model = [filtered firstObject];
    WEAKSELF_DECLARATION
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    CDUser *user = [[SettingsManager instance] currentUser];
    if (model.userId == user.objId) {
        UIAlertAction *actionDeletePost = [UIAlertAction actionWithTitle:NSLocalizedString(@"Видалити пост", nil)
                                                                   style:UIAlertActionStyleDestructive
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     STRONGSELF_DECLARATION
                                                                     [strongSelf deletePostById:anItemId];
                                                                 }];
        [alertController addAction:actionDeletePost];
    }else{
        UIAlertAction *actionСomplainPost = [UIAlertAction actionWithTitle:NSLocalizedString(@"Поскаржитись на пост", nil)
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     STRONGSELF_DECLARATION
                                                                     [strongSelf complainPostById:anItemId];
                                                                 }];
        [alertController addAction:actionСomplainPost];
        
        if (model.userId == model.wallId) {
            UIAlertAction *actionBanUser = [UIAlertAction actionWithTitle:NSLocalizedString(@"Заблокувати користувача", nil)
                                                                    style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * _Nonnull action) {
                                                                      STRONGSELF_DECLARATION
                                                                      [strongSelf banUserById:model.userId];
                                                                  }];
            [alertController addAction:actionBanUser];
        }
    }
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Відміна", nil)
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {}];
    [alertController addAction:actionCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) deletePostById:(int64_t)anItemId
{
    WEAKSELF_DECLARATION
    [GBDataHelper deletePost:anItemId andCompletion:^{
        STRONGSELF_DECLARATION
        NSArray *filtered = [strongSelf.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"objId == %lld",anItemId]];
        if (filtered.count) {
            [strongSelf.items removeObjectsInArray:filtered];
            [strongSelf.tableView reloadData];
        }
    } failure:^(NSError *err) {}];
    
}

- (void) complainPostById:(int64_t)anItemId
{
    WEAKSELF_DECLARATION
    [GBDataHelper complainPost:anItemId andCompletion:^{
        STRONGSELF_DECLARATION
        NSArray *filtered = [strongSelf.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"objId == %lld",anItemId]];
        if (filtered.count) {
            [strongSelf.items removeObjectsInArray:filtered];
            [strongSelf.tableView reloadData];
        }
    } failure:^(NSError *err) {}];
    
}

- (void) banUserById:(int64_t)userId
{
    WEAKSELF_DECLARATION
    [GBDataHelper banUser:userId andCompletion:^{
        STRONGSELF_DECLARATION
        NSArray *filtered = [strongSelf.items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"idUser == %lld",userId]];
        if (filtered.count) {
            [strongSelf.items removeObjectsInArray:filtered];
            [strongSelf.tableView reloadData];
        }
    } failure:^(NSError *err) {}];
}


#pragma mark - GBStreamItemCellDelegate

- (void) didPressAvatarInCell:(id<GBStreamItemCellProtocol>)aCell
{
    WEAKSELF_DECLARATION
    int64_t userId = aCell.userId;
    if (userId == 0) return;
    [GBDataHelper getUserById:userId andCompletion:^(id value) {
        STRONGSELF_DECLARATION
        GBUser *user = value;
        GBProfileViewController * vc = [strongSelf.storyboard instantiateViewControllerWithIdentifier:@"GBProfileViewController"];
        //vc.hidesBottomBarWhenPushed = YES;
        [vc configureWithUser:user];
        [strongSelf.navigationController pushViewController:vc animated:YES];
    } failure:^(NSError *err) {
        //nothing
    }];
    
    
}

- (void) didPressCommentButtonInCell:(GBStreamItemTableViewCell*)aCell
{
    [self openCommentsForStreamItemId:aCell.streamItemId];
}

- (void) didPressMoreButtonInCell:(GBStreamItemTableViewCell*)aCell
{
    [self showMoreMenuForStreamItemId:aCell.streamItemId];
}

- (void) didChangeLikeValueInCell:(id<GBStreamItemCellProtocol>)aCell
{
    [GBDataHelper likePost:aCell.streamItemId];
}

@end
