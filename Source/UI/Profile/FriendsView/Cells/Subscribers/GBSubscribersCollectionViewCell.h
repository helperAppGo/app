#import <UIKit/UIKit.h>

@interface GBSubscribersCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@end
