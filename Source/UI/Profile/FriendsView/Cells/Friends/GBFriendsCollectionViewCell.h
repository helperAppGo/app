#import <UIKit/UIKit.h>

@interface GBFriendsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

@end
