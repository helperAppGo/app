#import "GBFriendsViewController.h"
#import "GBFriendsCollectionViewCell.h"
#import "GBSubscribersCollectionViewCell.h"

static NSString *friendsIdentifier = @"friendsCollectionCellIdentifier";
static NSString *subscribersIdentifier = @"subscribersCollectionCellIdentifier";

@interface GBFriendsViewController ()

@property (nonatomic,strong) NSArray * friends;
@property (nonatomic,strong) NSArray * followers;
@property (nonatomic,strong) GBUser * user;

@end

@implementation GBFriendsViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.friendsCollection registerClass:[GBFriendsCollectionViewCell class]
                forCellWithReuseIdentifier:friendsIdentifier];
    [self.friendsCollection registerNib:[UINib nibWithNibName:@"GBFriendsCollectionViewCell"
                                                        bundle:nil]
                forCellWithReuseIdentifier:friendsIdentifier];
        
    [self.subscribersCollection registerClass:[GBSubscribersCollectionViewCell class]
                    forCellWithReuseIdentifier:subscribersIdentifier];
    [self.subscribersCollection registerNib:[UINib nibWithNibName:@"GBSubscribersCollectionViewCell"
                                                            bundle:nil]
                 forCellWithReuseIdentifier:subscribersIdentifier];
    
    [self refreshWithUser:self.user];
}


#pragma mark - Public

- (void) refreshWithUser:(GBUser*)aUser
{
    if (!aUser) return;
    self.user = aUser;
    
    self.friends = aUser.topFriends;
    self.followers = aUser.topFollowers;
    
    [self.friendsCollection reloadData];
    [self.subscribersCollection reloadData];
}


#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.friendsCollection) {
        return MIN(self.friends.count,6);
    }else{
        return MIN(self.followers.count,6);
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.friendsCollection) {
        GBFriendsCollectionViewCell *friendsCell = [self.friendsCollection dequeueReusableCellWithReuseIdentifier:@"friendsCollectionCellIdentifier" forIndexPath:indexPath];
        [friendsCell.avatar sd_setImageWithURL:[NSURL URLWithString:[(GBUser*)self.friends[indexPath.item] avatar]] placeholderImage:[UIImage imageNamed:@"user"]];
        return friendsCell;
    } else {
        GBSubscribersCollectionViewCell *followersCell = [self.subscribersCollection dequeueReusableCellWithReuseIdentifier:@"subscribersCollectionCellIdentifier" forIndexPath:indexPath];
        [followersCell.avatar sd_setImageWithURL:[NSURL URLWithString:[(GBUser*)self.followers[indexPath.item] avatar]] placeholderImage:[UIImage imageNamed:@"user"]];
        return followersCell;
    }
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}

@end
