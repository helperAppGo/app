
@interface GBFriendsViewController : UIViewController <UICollectionViewDataSource,
                                                       UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *friendsCollection;
@property (weak, nonatomic) IBOutlet UICollectionView *subscribersCollection;

- (void) refreshWithUser:(GBUser*)aUser;

@end
