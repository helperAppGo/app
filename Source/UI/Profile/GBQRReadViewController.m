//
//  GBQRReadViewController.m
//  GoBeside
//
//  Created by Vyacheslav Varusha on 22.10.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBQRReadViewController.h"

@interface GBQRReadViewController ()

@property (strong, nonatomic) MTBBarcodeScanner *scanner;
@property (weak, nonatomic) IBOutlet UIView *preview;

@end

@implementation GBQRReadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
    self.scanner = [[MTBBarcodeScanner alloc] initWithMetadataObjectTypes:@[AVMetadataObjectTypeQRCode]
                                                                            previewView:self.preview];
    [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
        if (success) {
            
            NSError *error = nil;
            [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
                AVMetadataMachineReadableCodeObject *code = [codes firstObject];
                NSLog(@"Found code: %@", code.stringValue);
                [self.scanner stopScanning];
                [[GBServerManager sharedManager] sendQrCode:code.stringValue
                                                    success:^(id responseObject) {
                                                        NSLog(@"QR Send");
                                                    } failure:^(NSError *error, NSDictionary *userInfo) {
                                                        NSLog(@"QR Error");
                                                    }];
                
               [self.navigationController popViewControllerAnimated:YES];
                
            } error:&error];
            
        } else {
            // The user denied access to the camera
        }
    }];
    
    }
//- (void)viewDidAppear:YES {
//
//}


@end
