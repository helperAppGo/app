
@interface GBProfileDataSource : NSObject

//@property (strong, nonatomic) GBUser *user;

+ (id)dataSource;
- (void)uploadAvatarToServer:(UIImage *)image completion:(GBBoolBlock)aCompletion;
- (void)updateUserBirthdayWithDay:(NSInteger)aDay month:(NSInteger)aMonth year:(NSInteger)aYear completion:(GBBoolBlock)aCompletion;
- (void)updateUserCityWithId:(int64_t)aUserId completion:(GBBoolBlock)aCompletion;

+ (void)getPosts:(NSNumber*)wallId limit:(NSInteger)limit offset:(NSUInteger)offset andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;

@end
