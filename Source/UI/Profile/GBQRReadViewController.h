//
//  GBQRReadViewController.h
//  GoBeside
//
//  Created by Vyacheslav Varusha on 22.10.17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "MTBBarcodeScanner.h"
#import "GBServerManager.h"

@interface GBQRReadViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@end
