#import "GBBasePaginationViewController.h"
#import "GBBasePaginationViewController+TableView.h"
#import "GBUser.h"
#import "GBProfileDataSource.h"

#import "GBDataFacade.h"

@interface GBProfileViewController : GBBasePaginationViewController <UINavigationControllerDelegate>

@property (weak, nonatomic) GBProfileDataSource *dataSource;

- (void) configureWithUser:(GBUser*)aUser;

@end
