#import "GBProfileDataSource.h"
#import "GBServerManager.h"

@interface GBProfileDataSource()

@end

@implementation GBProfileDataSource

+ (id)dataSource {
    static GBProfileDataSource *profileDataSource;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        profileDataSource = [[super alloc] init];
    });
    
    return profileDataSource;
}

- (instancetype)init {
    self = [super init];
    if (self)
    {
//        self.user = CURRENT_USER;
    }
    
    return self;
}

- (void)uploadAvatarToServer:(UIImage *)image completion:(GBBoolBlock)aCompletion
{
    if (!image) return;
    [SVProgressHUD show];
    NSData *data = UIImagePNGRepresentation(image);
    
    [[GBServerManager sharedManager] uploadFile:data withName:@"avatar.png" success:^(id responseObject) {
        DLog(@"Avatar uploaded. Response:\n%@",responseObject);
        NSDictionary *resp = responseObject[@"response"];
        int64_t fileId = [resp[@"id"] longLongValue];
        
        CDUser *user = [[SettingsManager instance] currentUser];
        [[GBServerManager sharedManager] updateUserWithFirstName:user.firstName lastName:user.lastName avatarId:fileId success:^(id responseObject) {
            
            DLog(@"User updated. Response:\n%@",responseObject);
            [GBUser refreshUserDataWithCompletion:^(BOOL result) {
                if (result) {
                    [SVProgressHUD showSuccessWithStatus:nil];
                }else{
                    [SVProgressHUD showErrorWithStatus:nil];
                }
                if (aCompletion) aCompletion(result);
            }];
            
        } failure:^(NSError *error, NSDictionary *userInfo) {
            DLog(@"Filed to update user. Error: %@\nuserInfo: %@",error,userInfo);
            [SVProgressHUD showErrorWithStatus:nil];
            if (aCompletion) aCompletion(NO);
        }];
        
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Filed to upload avatar image. Error: %@\nuserInfo: %@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (aCompletion) aCompletion(NO);
    }];
}

- (void) updateUserBirthdayWithDay:(NSInteger)aDay month:(NSInteger)aMonth year:(NSInteger)aYear completion:(GBBoolBlock)aCompletion
{
    [SVProgressHUD show];
    
    [[GBServerManager sharedManager] updateUserBirthdayWithDay:aDay month:aMonth year:aYear success:^(id responseObject) {
        DLog(@"Birthday date is sent. Response:\n%@",responseObject);
        [SVProgressHUD showSuccessWithStatus:nil];
        if (aCompletion) aCompletion(YES);
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Filed to send birthday date. Error: %@\nuserInfo: %@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (aCompletion) aCompletion(NO);
    }];
}

- (void) updateUserCityWithId:(int64_t)aUserId completion:(GBBoolBlock)aCompletion
{
    [SVProgressHUD show];
    [[GBServerManager sharedManager] updateUserCityWithId:aUserId success:^(id responseObject) {
        DLog(@"City for user is sent. Response:\n%@",responseObject);
        [SVProgressHUD showSuccessWithStatus:nil];
        if (aCompletion) aCompletion(YES);
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Filed to send city for user. Error: %@\nuserInfo: %@",error,userInfo);
        [SVProgressHUD showErrorWithStatus:nil];
        if (aCompletion) aCompletion(NO);
    }];
}


+ (void)getPosts:(NSNumber*)wallId limit:(NSInteger)limit offset:(NSUInteger)offset andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    [[GBServerManager sharedManager] postsForWallId:wallId
                                               time:[[NSDate date] timeIntervalSince1970]
                                              limit:limit
                                             offset:offset
                                            success:^(id responseObject) {
                                                 DLog(@"Posts responce:\n%@",responseObject);
                                                 NSError *error = nil;
                                                 NSArray *posts = [GBPostModel arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
                                                 if (error) {
                                                     DLog(@"Failed to parse posts: %@",error);
                                                     if (failure)
                                                         failure(error);
                                                 }else{
                                                     if (completion)
                                                         completion(posts);
                                                 }
                                             } failure:^(NSError *error, NSDictionary *userInfo) {
                                                 DLog(@"Failed to get posts");
                                                 if (failure)
                                                     failure(error);
                                             }];
}

@end
