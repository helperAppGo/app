//
//  GBSubscribersViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBSubscribersViewController.h"
#import "GBFriendsListDataSource.h"

@interface GBSubscribersViewController ()

@end

@implementation GBSubscribersViewController

#pragma mark - Public

- (void) sendDataRequestWithOffset:(NSUInteger)offset limit:(NSInteger)limit onSuccess:(GBArrayBlock)aSuccessBlock onError:(GBErrorBlock)anErrorBlock
{
    [GBFriendsListDataSource getFollowersForUserId:self.user.objId.longLongValue withOffset:offset limit:limit andCompletion:aSuccessBlock failure:anErrorBlock];
}

@end
