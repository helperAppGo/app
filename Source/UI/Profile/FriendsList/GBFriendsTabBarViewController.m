//
//  GBFriendsTabBarViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 11.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBFriendsTabBarViewController.h"
#import "GBFriendsTabFriendsViewController.h"
#import "GBFriendsTabRequestsReceivedViewController.h"
#import "GBFriendsTabRequestsSentViewController.h"
#import <MaterialControls/MaterialControls.h>

@interface GBFriendsTabBarViewController () <MDTabBarDelegate>

@property (nonatomic,weak) GBFriendsTabFriendsViewController * friendsViewController;
@property (nonatomic,weak) GBFriendsTabRequestsReceivedViewController * requestsReceivedViewController;
@property (nonatomic,weak) GBFriendsTabRequestsSentViewController * requestsSentViewController;

@property (nonatomic,weak) IBOutlet MDTabBar * tabBar;
@property (nonatomic,weak) IBOutlet UIScrollView * scrollView;

@property (nonatomic,weak) IBOutlet NSLayoutConstraint *lcTabBarHeight;

@property (nonatomic,assign) BOOL scrollViewDragging;

@property (nonatomic,strong) GBUser *user;

@end

@implementation GBFriendsTabBarViewController

#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
//    BOOL thisUser = self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue;
//    self.tabBar.delegate = self;
//    if (thisUser) {
//        [self.tabBar setItems:@[@"Друзі",@"Заявки",@"Вихідні"]];
//    }else{
//        [self.tabBar setItems:@[@""]];
//    }
//    self.tabBar.selectedIndex = 0;
//    UIFont * tabsFont = [UIFont systemFontOfSize:14.0 weight:UIFontWeightMedium];
//    self.tabBar.textFont = tabsFont;
//    self.tabBar.normalTextFont = tabsFont;
//
//    [self updateTabBar:self.tabBar withSelectedIndex:self.tabBar.selectedIndex animated:NO];
//    if (!thisUser) {
//        self.tabBar.clipsToBounds = YES;
//        self.lcTabBarHeight.constant = 0.0;
//    }
}


#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
//    BOOL thisUser = self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue;
//    if ([segue.identifier isEqualToString:@"TabFriendsSegue"]) {
//        self.friendsViewController = segue.destinationViewController;
//        [self.friendsViewController configureWithUser:self.user];
//    }else if ([segue.identifier isEqualToString:@"TabRequestsReceivedSegue"]) {
//        self.requestsReceivedViewController = segue.destinationViewController;
//        if (thisUser)
//            [self.requestsSentViewController configureWithUser:self.user];
//    }else if ([segue.identifier isEqualToString:@"TabRequestsSentSegue"]) {
//        self.requestsSentViewController = segue.destinationViewController;
//        if (thisUser)
//            [self.requestsSentViewController configureWithUser:self.user];
//    }
}


#pragma mark - Public

- (void)configureWithUser:(GBUser*)aUser
{
    self.user = aUser;
}


#pragma mark - Private

- (void) updateTabBar:(MDTabBar*)tabBar withSelectedIndex:(NSUInteger)selectedIndex animated:(BOOL)animated
{
    [self.scrollView setContentOffset:CGPointMake(self.scrollView.frame.size.width * selectedIndex, 0.0) animated:animated];
}


#pragma mark - MDTabBarDelegate

- (void)tabBar:(MDTabBar *)tabBar didChangeSelectedIndex:(NSUInteger)selectedIndex
{
    [self updateTabBar:tabBar withSelectedIndex:selectedIndex animated:YES];
}


#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (self.scrollViewDragging) {
        NSInteger targetIdx = round(scrollView.contentOffset.x/scrollView.frame.size.width);
        self.tabBar.selectedIndex = targetIdx;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.scrollViewDragging = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    self.scrollViewDragging = NO;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    NSInteger targetIdx = round((*targetContentOffset).x/scrollView.frame.size.width);
    self.tabBar.selectedIndex = targetIdx;
}


@end
