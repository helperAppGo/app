//
//  GBFriendsListDataSource.m
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBFriendsListDataSource.h"
#import "GBServerManager.h"

@implementation GBFriendsListDataSource

+ (void) getFriendsForUserId:(int64_t)aUserId withOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    [[GBServerManager sharedManager] friendsForUserId:aUserId limit:limit offset:offset random:0 success:^(id responseObject) {
        DLog(@"Friends responce:\n%@",responseObject);
        NSError *error = nil;
        NSArray *friends = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
        if (error) {
            DLog(@"Failed to parse friends: %@",error);
            if (failure)
                failure(error);
        }else{
            if (completion)
                completion(friends);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to get friends");
        if (failure)
            failure(error);
    }];
}

+ (void) getFriendRequestsReceivedWithOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    CDUser *user = [[SettingsManager instance] currentUser];
    [[GBServerManager sharedManager] friendsRequestForUserId:user.objId limit:limit offset:offset success:^(id responseObject) {
        DLog(@"Friend Requests (Incoming) responce:\n%@",responseObject);
        NSError *error = nil;
        NSArray *friends = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
        if (error) {
            DLog(@"Failed to parse Friend Requests (Incoming): %@",error);
            if (failure)
                failure(error);
        }else{
            if (completion)
                completion(friends);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to get Friend Requests (Incoming)");
        if (failure)
            failure(error);
    }];
}

+ (void) getFriendRequestsSentWithOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    CDUser *user = [[SettingsManager instance] currentUser];
    [[GBServerManager sharedManager] outgoingFriendsRequestForUserId:user.objId  limit:limit offset:offset success:^(id responseObject) {
        DLog(@"Friend Requests (Outgoing) responce:\n%@",responseObject);
        NSError *error = nil;
        NSArray *friends = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
        if (error) {
            DLog(@"Failed to parse Friend Requests (Outgoing): %@",error);
            if (failure)
                failure(error);
        }else{
            if (completion)
                completion(friends);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to get Friend Requests (Outgoing)");
        if (failure)
            failure(error);
    }];
}

+ (void) getFollowersForUserId:(int64_t)aUserId withOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure
{
    [[GBServerManager sharedManager] followersForUserId:aUserId limit:limit offset:offset random:0 success:^(id responseObject) {
        DLog(@"Followers responce:\n%@",responseObject);
        NSError *error = nil;
        NSArray *followers = [GBUser arrayOfModelsFromDictionaries:responseObject[@"response"] error:&error];
        if (error) {
            DLog(@"Failed to parse Followers: %@",error);
            if (failure)
                failure(error);
        }else{
            if (completion)
                completion(followers);
        }
    } failure:^(NSError *error, NSDictionary *userInfo) {
        DLog(@"Failed to get Followers");
        if (failure)
            failure(error);
    }];
}


@end
