//
//  GBFriendsTabBarViewController.h
//  GoBeside
//
//  Created by Ruslan Mishin on 11.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBBaseViewController.h"

@interface GBFriendsTabBarViewController : GBBaseViewController

- (void)configureWithUser:(GBUser*)aUser;

@end
