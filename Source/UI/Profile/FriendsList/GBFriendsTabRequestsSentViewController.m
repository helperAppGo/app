//
//  GBFriendsTabRequestsSentViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBFriendsTabRequestsSentViewController.h"
#import "GBFriendsListDataSource.h"

@interface GBFriendsTabRequestsSentViewController ()

@end

@implementation GBFriendsTabRequestsSentViewController

#pragma mark - Public

- (void) sendDataRequestWithOffset:(NSUInteger)offset limit:(NSInteger)limit onSuccess:(GBArrayBlock)aSuccessBlock onError:(GBErrorBlock)anErrorBlock
{
    if (self.user)
        [GBFriendsListDataSource getFriendRequestsSentWithOffset:offset limit:limit andCompletion:aSuccessBlock failure:anErrorBlock];
}

@end
