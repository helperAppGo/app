//
//  GBFriendsListDataSource.h
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

@interface GBFriendsListDataSource : NSObject

+ (void) getFriendsForUserId:(int64_t)aUserId withOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;
+ (void) getFriendRequestsReceivedWithOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;
+ (void) getFriendRequestsSentWithOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;
+ (void) getFollowersForUserId:(int64_t)aUserId withOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;
//+ (void) getFollowingWithOffset:(NSUInteger)offset limit:(NSInteger)limit andCompletion:(GBArrayBlock)completion failure:(GBErrorBlock)failure;

@end
