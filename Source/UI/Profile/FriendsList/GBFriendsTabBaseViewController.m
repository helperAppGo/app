//
//  GBFriendsTabBaseViewController.m
//  GoBeside
//
//  Created by Ruslan Mishin on 12.05.18.
//  Copyright © 2018 SG. All rights reserved.
//

#import "GBFriendsTabBaseViewController.h"
#import "GBBasePaginationViewController+TableView.h"
#import "GBProfileViewController.h"
#import "GBUserTableViewCell.h"
#import "GBPaginationTableViewCell.h"

@interface GBFriendsTabBaseViewController ()

@property (nonatomic,strong,readwrite) GBUser *user;

@end


@implementation GBFriendsTabBaseViewController

#pragma mark - Lifecycle

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [GBUserTableViewCell registerForTableView:self.tableView];
    [self.tableView setTableFooterView:[UIView new]];
}


#pragma mark - Public

- (void)configureWithUser:(GBUser*)aUser
{
    self.user = aUser;
}


#pragma mark - UITableViewDelegate,UITableViewDataSource

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }else{
        GBUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[GBUserTableViewCell cellIdentifier]];
        GBUser *user = self.items[indexPath.row];
        [cell configureWithUser:user];
        return cell;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GBUserTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    GBUser *user = cell.user;
    GBProfileViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GBProfileViewController"];
    //vc.hidesBottomBarWhenPushed = YES;
    [vc configureWithUser:user];
    [self.navigationController pushViewController:vc animated:YES];
}

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44.0;
}

@end
