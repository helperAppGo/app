#import "GBProfileViewController.h"
#import "GBProfileViewController+GBStreamItemCellDelegate.h"

#import "GBFriendsViewController.h"
#import "GBFriendsTabBarViewController.h"
#import "GBSubscribersViewController.h"
#import "GBChooseCityViewController.h"
#import "GBPostCreateViewController.h"
#import "GBImagePreviewViewController.h"

#import "GBPostTableViewCell.h"

#import "GIOGCD.h"
#import "MainDataHolder.h"

#import "GBLoginViewController.h"
#import "GBCommonData.h"

@interface GBProfileViewController () <UIImagePickerControllerDelegate,UITextFieldDelegate>

@property (nonatomic, weak) GBFriendsViewController *friendsViewController;

@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *genderTextField;

@property (weak, nonatomic) IBOutlet UIView *buttonsView;
@property (weak, nonatomic) IBOutlet UIView *friendsCollection;

@property (weak, nonatomic) IBOutlet UILabel *birthday;
@property (weak, nonatomic) IBOutlet UILabel *city;
@property (weak, nonatomic) IBOutlet UILabel *gender;

@property (nonatomic, weak) IBOutlet UIView *vDatePickerContainer;
@property (nonatomic, weak) IBOutlet UIView *vDatePicker;
@property (nonatomic, weak) IBOutlet UIDatePicker *datePicker;

@property (nonatomic, weak) IBOutlet UIButton *btnLogoff;

@property (nonatomic,weak) IBOutlet UIStackView *stvGroups;
@property (nonatomic,weak) IBOutlet UIStackView *stvContacts;
@property (nonatomic,weak) IBOutlet UIStackView *stvAddFriend;
@property (nonatomic,weak) IBOutlet UIStackView *stvMessage;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lcDatePickerContainerHeight;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lcAddPostTop;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *lcAddPostHeight;

@property (nonatomic, strong) GBUser * user;

//- (IBAction)qrAction:(id)sender;

@end

@implementation GBProfileViewController


#pragma mark - LifeCycle

- (void)viewDidLoad
{
    self.dataSource = [GBProfileDataSource dataSource];
    [super viewDidLoad];
    // [self makeProfile];
    
//    [self updateUI];
    
    [GBPostTableViewCell registerForTableView:self.tableView];
    self.tableView.tableFooterView =[UIView new];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    WEAKSELF_DECLARATION
    
//    BOOL thisUser = self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue;
//    self.stvGroups.hidden = !thisUser;
//    self.stvContacts.hidden = !thisUser;
//    self.stvAddFriend.hidden = thisUser;
//    self.stvMessage.hidden = thisUser;
    
    [GBUser refreshUserDataWithCompletion:^(BOOL result) {
        if (!result) return;
        STRONGSELF_DECLARATION
//        [strongSelf updateUI];
        [strongSelf getDataWithOffset:0];
    }];
}


#pragma mark - Navigation

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    WEAKSELF_DECLARATION
    if ([segue.identifier isEqualToString:@"FriendsSegue"]) {
        self.friendsViewController = segue.destinationViewController;
    }else if ([segue.identifier isEqualToString:@"ChooseCitySegue"]) {
        GBChooseCityViewController * vc  =(GBChooseCityViewController*)segue.destinationViewController;
        [vc configureWithSelectedCityId:self.user.cityId completion:^ (id value){
            if (!value) return;
            STRONGSELF_DECLARATION
            GBCityModel *city = value;
            
//            [strongSelf.dataSource updateUserCityWithId:city.objId completion:^(BOOL result) {
//                if (!result) return;
//                STRONGSELF_DECLARATION
//                strongSelf.user.city = city;
//                strongSelf.user.cityId = strongSelf.user.city.objId;
//                [strongSelf updateCity];
//                if (strongSelf.user.objId == CURRENT_USER.objId) {
//                    //force to store updated value
//                    [GBCommonData setUser:strongSelf.user];
//                }
//            }];
        }];
    }else if ([segue.identifier isEqualToString:@"PostCreateSegue"]) {
        GBPostCreateViewController *vc = segue.destinationViewController;
        [vc configureWithPostAddedBlock:^(id value) {
//            if (![value isKindOfClass:[GBPostModel class]]) return;
//            GBPostModel * post = value;
//            STRONGSELF_DECLARATION
//            if (strongSelf.items.count) {
//                [strongSelf.items insertObject:post atIndex:0];
//                [strongSelf.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
//            }else{
//                [strongSelf.items addObject:post];
//                [strongSelf.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
//            }
        }];
    }
}


#pragma mark - Public

//- (BOOL) usePullToRefreshInstance
//{
//    return (self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue);
//}

- (void) configureWithUser:(GBUser*)aUser
{
    self.user = aUser;
    WEAKSELF_DECLARATION
    [self.user refreshUserTopFriendsAndFollowersWithCompletion:^(BOOL result) {
        STRONGSELF_DECLARATION
        [strongSelf.friendsViewController refreshWithUser:strongSelf.user];
    }];
}

- (void) sendDataRequestWithOffset:(NSUInteger)offset limit:(NSInteger)limit onSuccess:(GBArrayBlock)aSuccessBlock onError:(GBErrorBlock)anErrorBlock
{
    CDUser *user = [[SettingsManager instance] currentUser];
    [GBProfileDataSource getPosts:@(user.objId) limit:limit offset:offset andCompletion:aSuccessBlock failure:anErrorBlock];
}

#pragma mark -

- (void) unlogin {
    [[SettingsManager instance] logOut];
     
    [GBCommonData setToken:nil];
    [GBCommonData setUserId:nil];
    
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
    GBLoginViewController *loginViewController = [st instantiateViewControllerWithIdentifier:@"loginViewController"];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] openLogin];
//    [NAVIGATION_MANAGER setRootControllerWithAnimation:loginViewController];
}

#pragma mark Make profile 
#pragma TODO: Profile data source

//- (void) makeProfile {
//    [self makeBirthday];
//    
//    self.isStretched = YES;
//    self.nameLabel.text = self.dataSource.user.fullName;
//    self.statusTextField.text = self.dataSource.user.status;
//    
//    GIOPerformAsyncBlockOnBackgroundQueue(^{
//        NSData *ad = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.dataSource.user.file_url_avatar]];
//        UIImage *a = [UIImage imageWithData:ad];
//        GIOPerformSyncBlockOnMainQueue(^{
//            self.avatar.image = a;
//        });
//    });
//}

//- (IBAction)logout:(id)sender {
//    
//}



#pragma mark - UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
    [picker dismissViewControllerAnimated:YES completion:nil];
    WEAKSELF_DECLARATION
    [self.dataSource uploadAvatarToServer:image completion:^(BOOL result) {
        STRONGSELF_DECLARATION
        strongSelf.avatar.image = image;
    }];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

//- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    if (textField == self.birthdayTextField) {
//        if (self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue)
//            [self showDatePicker];
//        return NO;
//    }else if (textField == self.cityTextField) {
//        if (self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue)
//            [self chooseCity];
//        return NO;
//    }else if (textField == self.genderTextField) {
//        if (self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue)
//        {
//            //TODO: choose gender
//        }
//        return NO;
//    }
//    return YES;
//}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//        if (textField == self.contactsTextField) {
//                //[self.dataSource updateContacts: textField.text];
//            }
//}


#pragma mark - UITableViewDataSource, UITableViewDelegate

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 232.0;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        return [super tableView:tableView cellForRowAtIndexPath:indexPath];
    }else{
        GBPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[GBPostTableViewCell cellIdentifier]];
        GBPostModel *post = self.items[indexPath.row];
        [cell configureWithPost:post delegate:self];
        return cell;
    }
}


#pragma mark - Buttons clicked

//- (IBAction)questionsButtonClicked:(id)sender {
//        UIViewController *questionsViewController = [[UIViewController alloc] init];
//        questionsViewController.title = @"My Questions";
//        UITextView *tv = [[UITextView alloc] initWithFrame:questionsViewController.view.frame];
//        [questionsViewController.view addSubview:tv];
//        GIOPerformAsyncBlockOnBackgroundQueue(^(void) {
//                [DATA_FACADE getLastQuestionsByParams:@{@"limit": @25} completionBlock:^(id response, NSError *error) {
//                        NSString *text = [NSString stringWithFormat:@"%@", [NSJSONSerialization JSONObjectWithData:response options:kNilOptions error:nil]];
//                        GIOPerformSyncBlockOnMainQueue(^(void) {
//                                tv.text = text;
//                                [self.navigationController pushViewController:questionsViewController animated:YES];
//                            });
//                    }];
//            });
//    
//    
//    }

//- (IBAction)groupsButtonClicked:(id)sender {
//
//}
//
//- (IBAction)eventsButtonClicked:(id)sender {
//}
//

#pragma mark - Private

//- (void) updateUI
//{
//    self.nameLabel.text = self.user.fullName;
//    [self.avatar sd_setImageWithURL:[NSURL URLWithString:self.user.avatar] placeholderImage:[UIImage imageNamed:@"user"]];
//    [self updateBirthDate];
//    [self updateCity];
//    [self updateGender];
//
//    BOOL isCurrentUser = self.user.objId.longLongValue == CURRENT_USER.objId.longLongValue;
//    self.btnLogoff.hidden = !isCurrentUser;
//    self.lcAddPostTop.constant = isCurrentUser ? 8.0 : 0.0;
//    self.lcAddPostHeight.constant = isCurrentUser ? 44.0 : 0.0;
//    CGRect frame = self.tableView.tableHeaderView.frame;
//    frame.size.height = isCurrentUser ? 557.0 : (557.0-44.0-8.0);
//    self.tableView.tableHeaderView.frame = frame;
//
//    [self.friendsViewController refreshWithUser:self.user];
//}

- (void) updateBirthDate
{
    NSDate * birthDate = self.user.birthDate;
    if (birthDate) {
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd/MM/YYYY";
        self.birthdayTextField.text = [dateFormatter stringFromDate:birthDate];
    }else{
        self.birthdayTextField.text = @"";
    }
}

- (void) updateCity
{
    GBCityModel * city = nil;
    if (self.user.city) {
        city = self.user.city;
    }else if (self.user.cityId) {
        if (CITIES) {
            NSArray * filtered = [CITIES filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"objId == %lld",self.user.cityId]];
            if (filtered.count)
                city = [filtered firstObject];
        }
    }
    self.cityTextField.text = city ? city.name : @"";
}

- (void) updateGender
{
    self.genderTextField.text = (self.user.gender == EGenderFemale) ? @"Жін." : @"Чол.";
}

- (void) showDatePicker
{
    if (self.user.birthDate) {
        self.datePicker.date = self.user.birthDate;
    }else{
        self.datePicker.date = [NSDate date];
    }
    [self.datePicker setMaximumDate:[NSDate date]];
    
    self.lcDatePickerContainerHeight.constant = self.vDatePicker.frame.size.height;
    [self.view setNeedsUpdateConstraints];
    WEAKSELF_DECLARATION
    [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
        STRONGSELF_DECLARATION
        [strongSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (!finished) return;
        STRONGSELF_DECLARATION
        strongSelf.vDatePickerContainer.userInteractionEnabled = YES;
    }];
}

- (void) hideDatePicker
{
    self.lcDatePickerContainerHeight.constant = 0.0;
    [self.view setNeedsUpdateConstraints];
    self.vDatePickerContainer.userInteractionEnabled = NO;
    WEAKSELF_DECLARATION
    [UIView animateWithDuration:DEFAULT_ANIMATION_DURATION animations:^{
        STRONGSELF_DECLARATION
        [strongSelf.view layoutIfNeeded];
    }];
}

- (void) chooseCity
{
    [self performSegueWithIdentifier:@"ChooseCitySegue" sender:self];
}

//- (IBAction)shortnessInfoView:(id)sender {
//    CGFloat height;
//    CGFloat heightForLabels;
//    CGFloat fontSize;
//    
//    if (!self.isStretched) {
//        height = 184;
//        heightForLabels = 15;
//        fontSize = 14;
//        self.isStretched = YES;
//    } else {
//        height = 0;
//        heightForLabels = 0;
//        fontSize = 0;
//        self.isStretched = NO;
//    }
//    
//    [self.infoHeight setConstant:height];
//    [self.infoView setFrame:CGRectMake(0,
//                                       self.infoView.frame.origin.y,
//                                       self.view.frame.size.width,
//                                       height)];
//    
//    NSArray *arrayOfLabels = @[self.contacts, self.contactsTextField, self.status, self.statusTextField,
//                               self.birthday, self.birthdayDateLabel, self.city, self.cityTextField];
//    
//    for (UILabel *item in arrayOfLabels) {
//        [item setFrame:CGRectMake(item.frame.origin.x,
//                                  item.frame.origin.y,
//                                  item.frame.size.width,
//                                  heightForLabels)];
//        [item setFont:[UIFont systemFontOfSize:fontSize]];
//    }
//    
//    [self.view setNeedsLayout];
//}

//- (IBAction)qrAction:(id)sender {
//}

#pragma mark - IBActions

- (IBAction)onBtnAvatar:(id)sender
{
//    if (self.user.objId.longLongValue != CURRENT_USER.objId.longLongValue) {
//        if (self.user.avatar.length) {
//            GBImagePreviewViewController *vc = [self .storyboard instantiateViewControllerWithIdentifier:@"GBImagePreviewViewController"];
//            [vc configureWithUrl:[NSURL URLWithString:self.user.avatar] cacheOptions:SDWebImageCacheMemoryOnly];
//            [self.navigationController pushViewController:vc animated:YES];
//        }
//    }else{
//        UIImagePickerController *picker = [UIImagePickerController new];
//        picker.navigationBar.translucent = NO;
//        picker.navigationBar.barTintColor = self.navigationController.navigationBar.barTintColor;
//        picker.navigationBar.tintColor = self.navigationController.navigationBar.tintColor;
//        picker.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName : self.navigationController.navigationBar.tintColor};
//        picker.delegate = self;
//        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//
//        [self.navigationController presentViewController:picker
//                                                animated:YES
//                                              completion:nil];
//    }
}

- (IBAction) onBtnOpenFriends:(id)sender
{
    GBFriendsTabBarViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GBFriendsTabBarViewController"];
    dAssert(vc,@"GBFriendsListViewController not found in Storyboard");
    [vc configureWithUser:self.user];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction) onBtnOpenSubscribers:(id)sender
{
    GBSubscribersViewController * vc = [self.storyboard instantiateViewControllerWithIdentifier:@"GBSubscribersViewController"];
    dAssert(vc,@"GBSubscribersViewController not found in Storyboard");
    [vc configureWithUser:self.user];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)logoffAction:(id)sender
{
    [self unlogin];
}

- (IBAction)onBtnDatePickerCancel:(id)sender
{
    [self hideDatePicker];
}

- (IBAction)onBtnDatePickerConfirm:(id)sender
{
    NSDate * date = self.datePicker.date;
    NSInteger day = 0, month = 0, year = 0;
    [GBUser birthDateComponentsFromDate:date day:&day month:&month year:&year];
    
    WEAKSELF_DECLARATION
    [self.dataSource updateUserBirthdayWithDay:day month:month year:year completion:^(BOOL result) {
        if (result) {
            STRONGSELF_DECLARATION
            strongSelf.user.birthDate = date;
            [strongSelf updateBirthDate];
            [strongSelf hideDatePicker];
//            if (strongSelf.user.objId == CURRENT_USER.objId) {
//                //force to store updated value
//                [GBCommonData setUser:strongSelf.user];
//            }
        }
    }];
    
    
}


#pragma mark - Custom Getters

- (GBUser*)user
{
    return nil;
}


@end
