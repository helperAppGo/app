//
//  GBListMembersModel.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBListMembersModel.h"

#define   SHOW_LOGS             NO
#define   Error(format, ...)    if (SHOW_LOGS) NSLog(@"ERROR: %@", [NSString stringWithFormat:format, ## __VA_ARGS__]);

@implementation GBListMembersModel

+ (NSArray *) getObjectsFromArray:(NSArray *)data {
    if ((id)data == [NSNull null]) {
        return @[];
    }
    
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSDictionary *dict in data) {
        @try {
            [result insertObject:[GBListMembersModel ListMembersFromDictionary:dict] atIndex:0];
        }
        @catch (NSException *exception) {
            Error(@"%@", exception);
        }
    }
    return result;
}


+(GBListMembersModel *)ListMembersFromDictionary:(NSDictionary *)data {
    GBListMembersModel *listModel = [[GBListMembersModel alloc] init];

    listModel.idMembers = data[@"id"];
    listModel.first_name = data[@"first_name"];
    listModel.last_name = data[@"last_name"];
    listModel.id_city = data[@"id_city"];
    listModel.file_ava = data[@"file_ava"];
    listModel.path_ava = [NSURL URLWithString:data[@"path_ava"]];
    listModel.file_cover = data[@"file_cover"];
    listModel.path_cover = data[@"path_cover"];
    listModel.lang = data[@"lang"];
    listModel.b_day = data[@"b_day"];
    listModel.b_month = data[@"b_month"];
    listModel.b_year = data[@"b_year"];
    listModel.status = data[@"status"];
    listModel.info = data[@"info"];
    listModel.sex = data[@"sex"];
    listModel.is_online = data[@"id"];
    
    NSString *time = data[@"time_disconnected"];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *date = [formatter dateFromString:time];
    listModel.time_disconnected = date;
    
    return listModel;
}


@end
