//
//  GBAllGroupController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/6/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBAllGroupController.h"

#define groupMost [BASE_URL stringByAppendingString:@"/api/groups/most"]

@interface GBAllGroupController ()

@end

@implementation GBAllGroupController {
    NSMutableArray *jsonArray;
    NSMutableArray *jsonGroup;
    NSInteger currentPage;
    NSMutableDictionary *paramDic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refresh];
    
//#if USE_AUTOSIZING_CELLS
//    // enable auto-sizing cells on iOS 8
//    if([self.allGroupTableView respondsToSelector:@selector(layoutMargins)]) {
//        self.allGroupTableView.estimatedRowHeight = 88.0;
//        self.allGroupTableView.rowHeight = UITableViewAutomaticDimension;
//    }
//#endif
    
    currentPage = 0;
    paramDic = [[NSMutableDictionary alloc] init];
    
    [self getAllGroup:^{}];
    
    dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
    
    // reload data in GBAllGroupCell
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"reload_data" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) refresh {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    [self.allGroupTableView addSubview:refreshControl];
    [self.allGroupTableView sendSubviewToBack:refreshControl];
    
    __weak typeof(self) weakSelf = self;
    
    // Create custom indicator
    CGRect indicatorRect;
    
#if TARGET_OS_TV
    indicatorRect = CGRectMake(0, 0, 64, 64);
#else
    indicatorRect = CGRectMake(0, 0, 24, 24);
#endif
    
    CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
    
    // Set custom indicator
    self.allGroupTableView.infiniteScrollIndicatorView = indicator;
    
    // Set custom indicator margin
    self.allGroupTableView.infiniteScrollIndicatorMargin = 40;
    
    // Set custom trigger offset
    self.allGroupTableView.infiniteScrollTriggerOffset = 500;
    
    [self.allGroupTableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
        [weakSelf getAllGroup:^{
            // Finish infinite scroll animations
            [tableView finishInfiniteScroll];
        }];
    }];
    
    [self.allGroupTableView beginInfiniteScroll:YES];
}

- (void)handleRefresh:(UIRefreshControl *)refreshControl {
    
    double delayInSeconds = 0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"DONE");
        
        currentPage = 0;
        jsonGroup = nil;
        [self getAllGroup:^{}];
        
        // When done requesting/reloading/processing invoke endRefreshing, to close the control
        [self.allGroupTableView reloadData];
        [refreshControl endRefreshing];
    });
}

// - WHEN CLICK TO IMAGE FOLLOW/UNFOLLOW
- (void) reloadTableView:(NSNotificationCenter *)reloadData{
    double delayInSeconds = 0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"DONE");
        
        currentPage = 0;
        jsonGroup = nil;
        [self getAllGroup:^{}];
        
        // When done requesting/reloading/processing invoke endRefreshing, to close the control
        [self.allGroupTableView reloadData];
    });
    
}


-(void) getAllGroup:(void(^)(void))completion {
    NSLog(@"GO TO GROUPS");
    
    paramDic = [@{@"OffSet":[NSString stringWithFormat:@"%li", (long)currentPage]} mutableCopy];
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodGroupMOST params:paramDic andURL:groupMost];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            NSArray *newGroup = [dataParser parseGroupMost:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            
            if (jsonGroup == nil) {
                jsonGroup = [[[newGroup reverseObjectEnumerator] allObjects] mutableCopy];
            } else {
                [jsonGroup addObjectsFromArray:[[newGroup reverseObjectEnumerator] allObjects]];
            }
        }
        
        [self.allGroupTableView reloadData];
        
        currentPage += 25;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.allGroupTableView reloadData];
            NSLog(@"otpavivli update table");
            
            [[UIApplication sharedApplication] stopNetworkActivity];
            
            if(completion) {
                completion();
            }
        });
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"JSON GROUP COUNT: %lu", (unsigned long)jsonGroup.count);
    NSLog(@"CURRENT PAGE = %ld", (long)currentPage);
    return [jsonGroup count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GBAllGroupModel *allGroup = [jsonGroup objectAtIndex:indexPath.row];
    
    static NSString *CellIndetifier = @"AllGroupListCell";
    GBAllGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndetifier];
    if (!cell) {
        cell = [[GBAllGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIndetifier];
    }
    [cell setGroup:allGroup];
    
    return cell;
}


NSInteger indexTableAllGroup;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *zero = [NSNumber numberWithInt:0];
    GBAllGroupModel *model = [jsonGroup objectAtIndex:indexTableAllGroup];
    
    indexTableAllGroup = indexPath.row;
    
    if ([model.is_admin isEqualToNumber:zero]) {
        [self performSegueWithIdentifier:@"goToNotAdmin" sender:self];
    } else {
        [self performSegueWithIdentifier:@"goToAdmin" sender:self];
    }
    
    [tableView reloadData];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSNumber *zero = [NSNumber numberWithInt:0];
    GBAllGroupModel *model = [jsonGroup objectAtIndex:indexTableAllGroup];
    
    if ([model.is_admin isEqualToNumber:zero]) {
        NSLog(@"NOT ADMIN");
        [segue.identifier isEqual: @"goToNotAdmin"];
        
        GBNotAdminGroupController *cont = segue.destinationViewController;
        
        cont.idGroup = model.idGroup;
        NSLog(@"all group - group id: %@", model.idGroup);
        
    } else {
        
        NSLog(@"ADMIN");
        [segue.identifier isEqual: @"goToAdmin"];
        
        GBAdminGroupController *cont = segue.destinationViewController;
        
        cont.idGroup = model.idGroup;
        NSLog(@"all group - group id: %@", model.idGroup);
    }
}

/*
 {
 address = "";
 "can_post" = 0;
 "count_followers" = 17;
 distance = 0;
 "file_ava" = 3302;
 "file_cover" = 3301;
 id = 35;
 "id_category" = 0;
 "id_city" = 0;
 "is_admin" = 0;
 "is_banned" = 0;
 "is_member" = 0;
 lat = "50.512283";
 lon = "30.622667";
 name = "\U041f\U0440\U0438\U043a\U043e\U043b\U044c\U043d\U044b\U0435 \U043a\U0430\U0440\U0442\U0438\U043d\U043a\U0438 ";
 "path_ava" = "http://helper.darx.net/files/19de0faae244bf06/4401d4eb04f41641/95d8a0cb5d164beb/31518afd14fbd3d6.jpg";
 "path_cover" = "http://helper.darx.net/files/c972f87241e9e654/0bc38aaa83ffe76b/5e6beb1b1ef6d429/88b8b79cc7d52b37.jpg";
 "short_description" = "\U0421\U0430\U043c\U044b\U0435 \U043b\U0443\U0447\U0448\U0438\U0435 \U043f\U043e\U0441\U0442\U044b ";
 type = 2;
 "web_site" = "";
 },
 */


@end
