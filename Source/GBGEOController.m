//
//  GBGEOController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/6/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBGEOController.h"

#define groupGEO [BASE_URL stringByAppendingString:@"/api/groups/geo"]

@interface GBGEOController ()

@end

@implementation GBGEOController{
    NSMutableArray *jsonArray;
    NSMutableArray *jsonGroup;
    NSInteger currentPage;
    NSMutableDictionary *paramDic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self refresh];

    currentPage = 0;
    paramDic = [[NSMutableDictionary alloc] init];
    
    [self getGeoGroup:^{}];
    
    dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
    
    // reload data in GBAllGroupCell
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView:) name:@"reload_data" object:nil];
}

- (void) refresh {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(handleRefreshs:) forControlEvents:UIControlEventValueChanged];
    [self.geoGroupTableView addSubview:refreshControl];
    [self.geoGroupTableView sendSubviewToBack:refreshControl];
    
    __weak typeof(self) weakSelf = self;
    
    // Create custom indicator
    CGRect indicatorRect;
    
#if TARGET_OS_TV
    indicatorRect = CGRectMake(0, 0, 64, 64);
#else
    indicatorRect = CGRectMake(0, 0, 24, 24);
#endif
    
    CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
    
    // Set custom indicator
    self.geoGroupTableView.infiniteScrollIndicatorView = indicator;
    
    // Set custom indicator margin
    self.geoGroupTableView.infiniteScrollIndicatorMargin = 40;
    
    // Set custom trigger offset
    self.geoGroupTableView.infiniteScrollTriggerOffset = 500;
    
    [self.geoGroupTableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
        [weakSelf getGeoGroup:^{
            // Finish infinite scroll animations
            [tableView finishInfiniteScroll];
        }];
    }];

    [self.geoGroupTableView beginInfiniteScroll:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleRefreshs:(UIRefreshControl *)refreshControl {
    
    //    [self getAllGroup:^{}];
    double delayInSeconds = 0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"DONE");
        
        currentPage = 0;
        jsonGroup = nil;
        [self getGeoGroup:^{}];
        
        // When done requesting/reloading/processing invoke endRefreshing, to close the control
        [self.geoGroupTableView reloadData];
        //        [self.allGroupTableView layoutIfNeeded];
        [refreshControl endRefreshing];
    });
}

// - WHEN CLICK TO IMAGE FOLLOW/UNFOLLOW
- (void) reloadTableView:(NSNotificationCenter *)reloadData{
    double delayInSeconds = 0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"DONE");
        
        currentPage = 0;
        jsonGroup = nil;
        [self getGeoGroup:^{}];
        
        // When done requesting/reloading/processing invoke endRefreshing, to close the control
        [self.geoGroupTableView reloadData];
    });
    
}

-(void) getGeoGroup:(void(^)(void))completion {
    NSLog(@"GO TO GROUPS");
    
    paramDic = [NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%li", (long)currentPage] forKey:@"OffSet"];
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodGroupMOST params:paramDic andURL:groupGEO];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if (!error) {
            NSArray *newGroup = [dataParser parseGroupMost:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
            
            if (jsonGroup == nil) {
                jsonGroup = [[newGroup reverseObjectEnumerator] allObjects];
            } else {
                [jsonGroup addObjectsFromArray:[[newGroup reverseObjectEnumerator] allObjects]];
            }
        }
        
        [self.geoGroupTableView reloadData];
        
        currentPage += 1;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.geoGroupTableView reloadData];
            NSLog(@"otpavivli update table");
            
            [[UIApplication sharedApplication] stopNetworkActivity];
            
            if(completion) {
                completion();
            }
        });
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"JSON GROUP COUNT: %lu", (unsigned long)jsonGroup.count);
    return [jsonGroup count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GBAllGroupModel *allGroup = [jsonGroup objectAtIndex:indexPath.row];
    
    static NSString *CellIndetifier = @"AllGroupListCell";
    GBAllGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndetifier];
    if (!cell) {
        cell = [[GBAllGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIndetifier];
    }
    [cell setGroup:allGroup];
    
    return cell;
}


int indexTableGeoGroup;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSNumber *zero = [NSNumber numberWithInt:0];
    GBAllGroupModel *model = [jsonGroup objectAtIndex:indexTableGeoGroup];
    
    indexTableGeoGroup = indexPath.row;
    
    if ([model.is_admin isEqualToNumber:zero]) {
        [self performSegueWithIdentifier:@"goToNotAdmin" sender:self];
    } else {
        [self performSegueWithIdentifier:@"goToAdmin" sender:self];
    }
    
    [tableView reloadData];
    
    
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    NSNumber *zero = [NSNumber numberWithInt:0];
    GBAllGroupModel *model = [jsonGroup objectAtIndex:indexTableGeoGroup];
    
    if ([model.is_admin isEqualToNumber:zero]) {
        NSLog(@"NOT ADMIN");
        [segue.identifier isEqual: @"goToNotAdmin"];
        
        GBNotAdminGroupController *cont = segue.destinationViewController;
        
        cont.idGroup = model.idGroup;
        NSLog(@"all group - group id: %@", model.idGroup);
        
    } else {
        
        NSLog(@"ADMIN");
        [segue.identifier isEqual: @"goToAdmin"];
        
        GBAdminGroupController *cont = segue.destinationViewController;
        
        cont.idGroup = model.idGroup;
        NSLog(@"all group - group id: %@", model.idGroup);
    }
}

@end

