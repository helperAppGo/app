//
//  GBListMembersCell.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBListMembersCell.h"

@interface GBListMembersCell()

@property (weak, nonatomic) IBOutlet UIImageView *memberImage;
@property (weak, nonatomic) IBOutlet UILabel *memberLabel;

@end



@implementation GBListMembersCell {
    GBListMembersModel *listMembersModel;
}



- (void) setListMember:(GBListMembersModel *)listModel {
    listMembersModel = listModel;
    
// - AVA PATH ------------
    self.memberImage.layer.cornerRadius = self.memberImage.frame.size.width/2;
    self.memberImage.layer.masksToBounds = YES;
    [self.memberImage setImageWithURL:listModel.path_ava];
    
// - NAME MEMBERS ------------
    self.memberLabel.text = [NSString stringWithFormat:@"%@ %@", listModel.first_name, listModel.last_name];
    
    
    
    
    
    
    
    
}



@end
