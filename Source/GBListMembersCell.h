//
//  GBListMembersCell.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImageView+AFNetworking.h"
#import "GBServerManager.h"

#import "GBListMembersModel.h"
#import "GBListMembers.h"

@interface GBListMembersCell : UITableViewCell

- (void) setListMember:(GBListMembersModel *)listModel;

@end
