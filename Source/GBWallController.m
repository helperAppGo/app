//
//  GBWallController.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/16/17.
//  Copyright © 2017 SG. All rights reserved.
//

//#import "GBWallController.h"

//#define getGroupPostsList [BASE_URL stringByAppendingString:@"/api/groups/posts/list"]
//
//static NSString *wallIdentifier = @"GBWallTableViewCell";
//static NSString *collectionViewCell = @"ProfileCollectionViewCell";

//@interface GBWallController ()
//
//@end
//
//@implementation GBWallController {
//    NSMutableArray *jsonArray;
//    NSInteger currentPage;
//    NSMutableDictionary *paramDic;
//    NSArray *jsonGIS;
    
//}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    
//    [self refresh];
//    
//    currentPage = 0;
//    
//    paramDic = [[NSMutableDictionary alloc] init];
//    paramDic = @{@"OffSet": @"0",
//                 @"Limit": @"25"};
//    
//    [self getAllGroup:^{}];
//    
//    dataParser = [[GBDataParser alloc] initWithDataHolder:dataHolder];
//}
//
//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}


//- (void) refresh {
////    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
////    [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
////    [self.wallTableView addSubview:refreshControl];
////    [self.wallTableView sendSubviewToBack:refreshControl];
//    
//    __weak typeof(self) weakSelf = self;
//    
//    // Create custom indicator
//    CGRect indicatorRect;
//    
//#if TARGET_OS_TV
//    indicatorRect = CGRectMake(0, 0, 64, 64);
//#else
//    indicatorRect = CGRectMake(0, 0, 24, 24);
//#endif
//    
//    CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
//    
//    // Set custom indicator
//    self.wallTableView.infiniteScrollIndicatorView = indicator;
//    
//    // Set custom indicator margin
//    self.wallTableView.infiniteScrollIndicatorMargin = 40;
//    
//    // Set custom trigger offset
//    self.wallTableView.infiniteScrollTriggerOffset = 500;
//    
//    [self.wallTableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
//        [weakSelf getAllGroup:^{
//            // Finish infinite scroll animations
//            [tableView finishInfiniteScroll];
//        }];
//    }];
//    
//    [self.wallTableView beginInfiniteScroll:YES];
//}
//
//- (void)handleRefresh:(UIRefreshControl *)refreshControl {
//    
//    double delayInSeconds = 0;
//    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        NSLog(@"DONE");
//        
//        currentPage = 0;
//        _jsonGroup = nil;
//        [self getAllGroup:^{}];
//        
//        // When done requesting/reloading/processing invoke endRefreshing, to close the control
//        [self.wallTableView reloadData];
//        [refreshControl endRefreshing];
//    });
//}


//-(void) getAllGroup:(void(^)(void))completion {
//    NSLog(@"GO TO GROUPS");
//    
//    paramDic = @{@"OffSet":[NSString stringWithFormat:@"%li", (long)currentPage],
//                 @"Limit": @"25",
//                 @"Id":[NSString stringWithFormat:@"%@", _idGroupInWall]};
//    
//    
//    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodGroupsPostsList params:paramDic andURL:getGroupPostsList];
//    NSLog(@"Request url: %@", request);
//    NSLog(@"HTTP BODY: %@", request.HTTPBody);
//    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
//    
//    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
//        
//        if (!error) {
//            NSArray *newGroup = [dataParser parseGroupPostList:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]];
//            
//            if (_jsonGroup == nil) {
//                _jsonGroup = [[newGroup reverseObjectEnumerator] allObjects];
//            } else {
//                [_jsonGroup addObjectsFromArray:[[newGroup reverseObjectEnumerator] allObjects]];
//            }
//        }
////        if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
////            NSLog(@"Response HTTP Status code: %ld\n", (long) [(NSHTTPURLResponse *) urlResponse statusCode]);
////            NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *) urlResponse allHeaderFields]);
////        }
////        
////        NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
////        NSLog(@"Responcse Body:\n%@\n", body);
////        
////        jsonGIS = [NSJSONSerialization JSONObjectWithData:data
////                                                  options:kNilOptions
////                                                    error:nil];
////        NSLog(@"JSON ARRAY - %@", jsonGIS);
////        
////        
////        NSLog(@"WALL RESPONSE:%@", _jsonGroup);
//        
//        [self.wallTableView reloadData];
//        
//        currentPage += 1;
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [self.wallTableView reloadData];
//            NSLog(@"otpavivli update table");
//            
//            [[UIApplication sharedApplication] stopNetworkActivity];
//            
//            if(completion) {
//                completion();
//            }
//        });
//    }];
//}


//#pragma mark - Table view data source
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(@"JSON WALL COUNT: %lu", (unsigned long)_jsonGroup.count);
//    return [_jsonGroup count];
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    GBWallModel *wallModel = [_jsonGroup objectAtIndex:indexPath.row];
//    
//    
//    GBWallCell *cell = [tableView dequeueReusableCellWithIdentifier:wallIdentifier];
//    if (!cell) {
//        cell = [[GBWallCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:wallIdentifier];
//    }
//    [cell setWall:wallModel];
//    
//    return cell;
//}
//
//
//#pragma mark - UITableViewDelegate
//
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSInteger row = indexPath.row;
//    GBWallModel *wall = _jsonGroup[row];
//    CGFloat height = 95.0;
//    if (!wall.isNoImageGroup){
//        
//        height = height + wall.heightObjects;
//        
//    }
//    if (wall.text){
//        height = height + 25.f; //////kostul!!!!!!!!!!!!!!!!
//    }
//    //    NSLog(@"Cell:%d, h:%f", indexPath.row, height);
//    
//    return height;
//}

//@end
