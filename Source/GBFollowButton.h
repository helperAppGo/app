//
//  GBFollowButton.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/26/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GBAllGroupModel.h"

@interface GBFollowButton : UIButton

@property (strong, nonatomic) GBAllGroupModel *groupModel;

- (instancetype)initWithModel:(GBAllGroupModel*)groupModel;

@end

