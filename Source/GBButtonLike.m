//
//  GBButtonLike.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/18/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBButtonLike.h"

@implementation GBButtonLike

- (instancetype) initWithModel:(GBWallModel *)model {
    self = (GBButtonLike *)[GBButtonLike buttonWithType:UIButtonTypeCustom];
    if (self) {
        self.model = model;
    }
    return self;
}

@end
