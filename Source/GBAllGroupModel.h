//
//  GBAllGroupModel.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/7/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GBAllGroupModel : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSNumber *can_post;
@property (nonatomic, strong) NSNumber *count_followers;
@property (nonatomic, strong) NSNumber *distance;
@property (nonatomic, strong) NSString *file_ava;
@property (nonatomic, strong) NSString *file_cover;
@property (nonatomic, strong) NSNumber *idGroup;
@property (nonatomic, strong) NSNumber *id_category;
@property (nonatomic, strong) NSNumber *id_city;
@property (nonatomic, strong) NSNumber *is_admin;
@property (nonatomic, strong) NSNumber *is_banned;
@property (nonatomic, assign) BOOL is_member;
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lon;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSURL *path_ava;
@property (nonatomic, strong) NSString *path_cover;
@property (nonatomic, strong) NSString *short_description;
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSString *web_site;

+(NSArray *)getObjectsFromArray:(NSArray *)data;
+(GBAllGroupModel *)allGroupFromDictionary:(NSDictionary *)data;
-(void) followAction;


//+(NSArray *)getApiGroupObjectsFromArray:(NSArray *)data;
//+(GBAllGroupModel *)allApiGroupFromDictionary:(NSDictionary *)data;



@end
