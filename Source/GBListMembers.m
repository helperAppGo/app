//
//  GBListMembers.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 10/17/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBListMembers.h"

#define getListMembers [BASE_URL stringByAppendingString:@"/api/groups/members/list"]

@interface GBListMembers ()

@end

@implementation GBListMembers {
    NSMutableArray *jsonArray;
    NSMutableArray *jsonMembers;
//    NSInteger currentPage;
    NSMutableDictionary *paramDic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    self.navigationItem.title = @"Підписники";
//    self.navigationItem.title = @"Піпіскіни";
    
    // - SET CUSTOM BACK BUTTON ------------
    UIImage *image = [UIImage imageNamed:@"left-arrow-white"];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.bounds = CGRectMake(0, 0, 20, 20);
    button.layer.shadowOpacity = 0.8;
    button.layer.shadowColor = [UIColor blackColor].CGColor;
    button.layer.shadowOffset = CGSizeMake(0.5, 1.0);
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(backButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;

    
    [self getMembersList:^{}];
    
    NSLog(@"THIS IS: %@", _idGroupForMembers);
    dataParser = [[GBDataParser alloc] init];
    
    [self.membersTableView reloadData];
}


//- (void)viewWillDisappear:(BOOL)animated {
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                  forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.translucent = YES;
//    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//    
//    [super viewWillDisappear:animated];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)backButtonClicked {
    NSLog(@"going back");
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) getMembersList:(void(^)(void))completion {
    NSLog(@"GO TO MEMBERS");
    
    paramDic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@", _idGroupForMembers],@"id",@"1",@"number", nil];
    
    NSURLRequest *request = [UGSSuperFetch makeRequestWithMethod:getMethodApiGroup params:paramDic andURL:getListMembers];
    NSLog(@"Request url: %@", request);
    NSLog(@"HTTP BODY: %@", request.HTTPBody);
    NSLog(@"ALL HTTP HEADER: %@", request.allHTTPHeaderFields);
    
    
    [UGSSuperFetch getDataWithRequest:request andCompletion:^(NSData *data, NSURLResponse *urlResponse, NSError *error) {
        
        if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]]) {
            NSLog(@"Response HTTP Status code: %ld\n", (long) [(NSHTTPURLResponse *) urlResponse statusCode]);
            NSLog(@"Response HTTP Headers:\n%@\n", [(NSHTTPURLResponse *) urlResponse allHeaderFields]);
        }
        
        NSString* body = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"Responcse Body:\n%@\n", body);

        
        if (!error) {
            jsonMembers = [[dataParser parseListMembers:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil]] mutableCopy];
            
            NSLog(@"JSON MEMBERS :%@", jsonMembers);
        }
        
        [self.membersTableView reloadData];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self.membersTableView reloadData];
            NSLog(@"otpavivli update table");
            
//            [[UIApplication sharedApplication] stopNetworkActivity];
//            
//            if(completion) {
//                completion();
//            }
        });
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"JSON MEMBERS COUNT: %lu", (unsigned long)jsonMembers.count);
    return [jsonMembers count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    GBListMembersModel *listMembers = [jsonMembers objectAtIndex:indexPath.row];
    
    static NSString *CellIndetifier = @"listMemberCell";
    GBListMembersCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIndetifier];
    if (!cell) {
        cell = [[GBListMembersCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIndetifier];
    }
    [cell setListMember:listMembers];
    
    return cell;
}


int indexTableListMembers;
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                       message:@"COMMING SOON..."
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
    
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    
//    NSNumber *zero = [NSNumber numberWithInt:0];
//    GBListMembersModel *model = [jsonMembers objectAtIndex:indexTableListMembers];
//    
//    indexTableListMembers = indexPath.row;
    
//    if ([model.is_admin isEqualToNumber:zero]) {
//        [self performSegueWithIdentifier:@"goToNotAdmin" sender:self];
//    } else {
//        [self performSegueWithIdentifier:@"goToAdmin" sender:self];
//    }
    
    [tableView reloadData];
}




@end
