//
//  GBAllGroupCell.m
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/7/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBAllGroupCell.h"

@interface GBAllGroupCell()

@property (strong, nonatomic) UIView *head;
@property (strong, nonatomic) UIImageView *avatarImg;
@property (strong, nonatomic) UILabel *nameGroupLabel;
@property (strong, nonatomic) UILabel *typeGroupLabel;
@property (strong, nonatomic) UILabel *shortDescLabel;
@property (strong, nonatomic) UIImageView *locationImg;
@property (strong, nonatomic) UILabel *locationLabel;
@property (strong, nonatomic) UIImageView *countPeopleImage;
@property (strong, nonatomic) UILabel *countPeopleLabel;
@property (strong, nonatomic) UIImageView *isMemberImage;
@property (strong, nonatomic) GBFollowButton *followButton;
@property (strong, nonatomic) UIView *bottomView;

@end

@implementation GBAllGroupCell{
    GBAllGroupController *controller;
    GBAllGroupModel *model;
}

- (void)setGroup:(GBAllGroupModel *)allGroup {
    model = allGroup;
    
    [self.contentView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    
// - HEAD -------------
    self.head = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 116)];
    self.head.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.head];
    
    
    

// - AVATAR -------------
    self.avatarImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 40, 40)];
    self.avatarImg.layer.cornerRadius = self.avatarImg.frame.size.width/2;
    self.avatarImg.layer.masksToBounds = YES;
    NSURL *thumburl = [NSURL URLWithString:@""];
    if ([allGroup.path_ava isEqual:thumburl]) {
        self.avatarImg.image = [UIImage imageNamed:@"team"];
    } else {
        [self.avatarImg setImageWithURL:allGroup.path_ava];
    }
    [self.head addSubview:self.avatarImg];

    
// - NAME GROUP --------
    self.nameGroupLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.avatarImg.bounds.size.width+25, 12, [UIScreen mainScreen].bounds.size.width-110, 21)];
    self.nameGroupLabel.text = allGroup.name;
    [self.nameGroupLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:14]];
    [self.head addSubview:self.nameGroupLabel];
    
    
// - TYPE GROUP --------
    self.typeGroupLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.avatarImg.bounds.size.width+25,
                                                                    33, 100, 14)];
    self.typeGroupLabel.textColor = [UIColor lightGrayColor];
    
    NSNumber *zero = [NSNumber numberWithInt:0];
    NSNumber *one = [NSNumber numberWithInt:1];
    NSNumber *two = [NSNumber numberWithInt:2];
    NSNumber *three = [NSNumber numberWithInt:3];
    if ([allGroup.type isEqualToNumber:zero]) {
        self.typeGroupLabel.text = @"Закрита група";
    } else if ([allGroup.type isEqualToNumber:one]) {
        self.typeGroupLabel.text = @"Відкрита група";
    } else if ([allGroup.type isEqualToNumber:two]){
        self.typeGroupLabel.text = @"Публічна сторінка";
    } else if ([allGroup.type isEqualToNumber:three]) {
        self.typeGroupLabel.text = @"Афіша";
    } else {
        self.typeGroupLabel.text = @"";
    }
    
    [self.typeGroupLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    [self.head addSubview:self.typeGroupLabel];
    
    
// - SHORT DESCRIPTION --------
    self.shortDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 55, [UIScreen mainScreen].bounds.size.width-24, 25)];
    self.shortDescLabel.text = allGroup.short_description;
    
    [self.shortDescLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    [self.head addSubview:self.shortDescLabel];
    
    
// - LOCATION IMAGE -------
    self.locationImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 84, 20, 20)];
    self.locationImg.image = [UIImage imageNamed:@"ic_placeholder"];
    [self.head addSubview:self.locationImg];
    
    
// - LOCATION LABEL -------
    self.locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(38, 84, 208, 23)];
    [self.locationLabel setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    self.locationLabel.textColor = [UIColor darkGrayColor];
    
    
    if (!allGroup.address) {
        self.locationLabel.text = allGroup.address;
    } else if (!allGroup.distance) {
        self.locationLabel.text = [allGroup.distance stringValue];
    } else {
        [self.locationLabel setHidden:YES];
        self.locationImg.hidden = YES;
    }
    
    [self.head addSubview:self.locationLabel];

    
// - COUNT PEOPLE IMAGE ---------
    self.countPeopleImage = [[UIImageView alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-53, 84, 20, 20)];
    self.countPeopleImage.image = [UIImage imageNamed:@"ic_publics"];
    [self.head addSubview:self.countPeopleImage];
    
    
// - COUNT PEOPLE LABEL ---------
    self.countPeopleLabel = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-30, 85, 22, 18)];
    [self.countPeopleLabel setFont:[UIFont fontWithName:@"Helvetica" size:10]];
    self.countPeopleLabel.textColor = [UIColor darkGrayColor];
    self.countPeopleLabel.text = [allGroup.count_followers stringValue];
    [self.head addSubview:self.countPeopleLabel];
    
 
// - IS MEMBER IMAGE + BUTTON ---------
    self.followButton = [[GBFollowButton alloc] initWithModel:model];
    self.followButton.groupModel = model;
    [self.followButton setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-37, 10, 25, 25)];
    NSString *is_member_icon;
    if (model.is_member) {
        is_member_icon = @"ic_folling";
        self.followButton.tag = 1;
    } else {
        is_member_icon = @"ic_follow";
        self.followButton.tag = 2;
    }
    
    [self.followButton setImage:[UIImage imageNamed:is_member_icon] forState:UIControlStateNormal];
    [self.followButton addTarget:self action:@selector(followAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.followButton];
    

// - VIEW BOTTOM ----------
    self.bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 112, [UIScreen mainScreen].bounds.size.width, 4)];
    self.bottomView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.head addSubview:self.bottomView];

}

-(void) followAction:(GBFollowButton *)sender{
    NSString *is_member_icon;
    NSLog(@"%ld", (long)sender.groupModel.is_member);

        
    if (sender.groupModel.is_member) {
        is_member_icon = @"ic_follow";
            
        [[GBServerManager sharedManager] unfollowGroupId:[model.idGroup integerValue] success:^(id responseObject) {
            NSLog(@"info: %@", responseObject);
                
            NSLog(@"UNFOLLOW");
        } failure:^(NSError *error, NSDictionary *userInfo) {
            NSLog(@"error: %@, userInfo: %@", error, userInfo);
        }];

    } else {
            
        is_member_icon = @"ic_folling";
            
        [[GBServerManager sharedManager] joinGroupId:[model.idGroup integerValue] success:^(id responseObject) {
            NSLog(@"info: %@", responseObject);
                
            NSLog(@"JOIN");
        } failure:^(NSError *error, NSDictionary *userInfo) {
            NSLog(@"error: %@, userInfo: %@", error, userInfo);
        }];
    }
    
    [sender.groupModel followAction];
    [sender setImage:[UIImage imageNamed:is_member_icon] forState:UIControlStateNormal];
}
    
@end
