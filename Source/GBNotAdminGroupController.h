//
//  GBNotAdminGroupController.h
//  GoBeside
//
//  Created by Maxim Ohrimenko on 9/15/17.
//  Copyright © 2017 SG. All rights reserved.
//

#import "GBInGroupViewController.h"
#import "UGSFetchController.h"
#import "UGSSuperFetch.h"
#import "GBDataParser.h"
#import "GBAllGroupModel.h"
#import "GBWallController.h"
#import "AppDelegate.h"
#import "GBListMembers.h"
#import "GBWallCell.h"

@interface GBNotAdminGroupController : GBInGroupViewController <UITableViewDelegate, UITableViewDataSource> 

// STATUS VIEW
@property (weak, nonatomic) IBOutlet UIView *statusGroupView;
@property (weak, nonatomic) IBOutlet UIImageView *iconStatusGroup;
@property (weak, nonatomic) IBOutlet UILabel *statusGroup;
- (IBAction)isMemberButton:(id)sender;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusHeight;


@end
