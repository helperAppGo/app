//
//  UIView+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 26.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

+ (instancetype)loadFromNib;
-(CGFloat)height;
-(void)setHeight:(CGFloat)height;
-(CGFloat)width;
-(void)setWidth:(CGFloat)width;

- (void)drawDashedBorderAroundView:(UIView *)v;

//-(CGFloat)x;
//-(CGFloat)y;

-(CGFloat)top;
-(void)setTop:(CGFloat)top;

-(CGFloat)bottom;
-(void)setBottom:(CGFloat)bottom;

-(CGFloat)left;
-(void)setLeft:(CGFloat)left;

-(CGFloat)right;
-(void)setRight:(CGFloat)right;

-(CGFloat)centerX;
-(void)setCenterX:(CGFloat)centerX;

-(CGFloat)centerY;
-(void)setCenterY:(CGFloat)centerY;

-(void)defaultShadow;
-(void)bottomShadow;
-(void)defaultCornerRadius;
-(void)defaultBorder;
-(void)roundView;

-(void)addRoundedCorners:(UIRectCorner)corners withRadii:(CGSize)radii;
@end
