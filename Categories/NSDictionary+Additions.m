//
//  NSDictionary+Additions.m
//  TaxiPriz
//
//  Created by Nodir on 29.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import "NSDictionary+Additions.h"

@implementation NSDictionary (Additions)

- (NSString *)convertToJsonString {
    if(self) {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        return jsonString;
    } else {
        return @"";
    }
}

@end
