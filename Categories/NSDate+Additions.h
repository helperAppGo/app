//
//  NSDate+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 26.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)
+(NSString *)todayWithMinute:(BOOL)minute;
-(NSString *)timeString;
@end
