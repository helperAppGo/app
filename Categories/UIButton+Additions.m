//
//  UIButton+Additions.m
//  TaxiPriz
//
//  Created by Nodir on 27.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import "UIButton+Additions.h"

@implementation UIButton (Additions)
//-(void)defaultCornerRadius{
//    self.layer.cornerRadius = 4;
//    self.layer.masksToBounds = YES;
//}

-(void)setTitle:(NSString *)title{
    [self setTitle:title forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateSelected];
    [self setTitle:title forState:UIControlStateHighlighted];
}

-(void)setTitleColor:(UIColor *)color{
    [self setTitleColor:color forState:UIControlStateNormal];
    [self setTitleColor:[color colorWithAlphaComponent:0.7] forState:UIControlStateSelected];
    [self setTitleColor:[color colorWithAlphaComponent:0.7] forState:UIControlStateHighlighted];
}
@end
