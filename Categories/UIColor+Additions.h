//
//  UIColor+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 27.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Additions)

+(UIColor *)appYellowColor;

+(UIColor *)appOrangeColor;

+(UIColor *)appBlueColor;

+(UIColor *)appBackgroundBlueColor;

+(UIColor *)appBrownColor;

+ (UIColor *)colorFromHex:(NSString *)hexString;

@end
