//
//  UIPrintPageRenderer+PDF.h
//  Milliy
//
//  Created by Jurayev Nodir on 7/4/17.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPrintPageRenderer (PDF)

- (NSData*) printToPDF;

@end
