//
//  UIColor+Additions.m
//  TaxiPriz
//
//  Created by Nodir on 27.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import "UIColor+Additions.h"

@implementation UIColor (Additions)

+(UIColor *)appYellowColor{
    return COLOR(255, 193, 8);
}

+(UIColor *)appOrangeColor{
    return COLOR(254, 159, 10);
}

+(UIColor *)appBackgroundBlueColor{
    return COLOR(241, 245, 247);
}

+(UIColor *)appBrownColor{
    return COLOR(116, 116, 116);
}

+ (UIColor *)colorFromHex:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
