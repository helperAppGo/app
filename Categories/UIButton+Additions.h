//
//  UIButton+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 27.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Additions)
//-(void)defaultCornerRadius;
-(void)setTitle:(NSString *)title;
-(void)setTitleColor:(UIColor *)color;
@end
