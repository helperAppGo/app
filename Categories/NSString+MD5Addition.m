//
//  NSString+MD5Addition.m
//  UIDeviceAddition
//
//  Created by Georg Kitz on 20.08.11.
//  Copyright 2011 Aurora Apps. All rights reserved.
//

#import "NSString+MD5Addition.h"
#import <CommonCrypto/CommonDigest.h>
#import <Foundation/NSString.h>

@implementation NSString (MD5Addition)

-(NSString *)toWords{
    NSNumber *numberValue = [NSNumber numberWithLongLong:[self longLongValue]]; //needs to be NSNumber!
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    numberFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"ru"];
    [numberFormatter setNumberStyle:NSNumberFormatterSpellOutStyle];
    NSString *wordNumber = [numberFormatter stringFromNumber:numberValue];
    return wordNumber;
}

-(NSString *)maskFormat{
    if (self.length < 5) {
        return @"";
    }
    NSString *first = [self substringWithRange:NSMakeRange(0, 4)];
    NSString *last = [self substringFromIndex:self.length -4];
    return [NSString stringWithFormat:@"%@ **** **** %@",first, last];
}
-(NSString *)numberToString:(float)number{
    
//    NSDictionary *dig1 = @{@"0":@[@"одна", @"две", @"три", @"четыре", @"пять", @"шесть", @"семь", @"восемь", @"девять"],@"1":@[@"один", @"два"]};
//    
//    NSArray *dig10 = @[@"десять", @"одиннадцать", @"двенадцать", @"тринадцать", @"четырнадцать",
//                       @"пятнадцать", @"шестнадцать", @"семнадцать", @"восемнадцать", @"девятнадцать"];
//    
//    NSArray *dig20 = @[@"двадцать", @"тридцать", @"сорок", @"пятьдесят",
//                       @"шестьдесят", @"семьдесят", @"восемьдесят", @"девяносто"];
//    
//    NSArray *dig100 = @[@"сто", @"двести", @"триста", @"четыреста", @"пятьсот",
//                        @"шестьсот", @"семьсот", @"восемьсот", @"девятьсот"];
//    
//    NSDictionary *leword = @{@"0":@[@"тийин", @"тийины", @"тийинов", @"0"],
//                             @"1":@[@"сум", @"сума", @"сумов", @"1"],
//                             @"2":@[@"тысяча", @"тысячи", @"тысяч", @"0"],
//                             @"3":@[@"миллион", @"миллиона", @"миллионов", @"1"],
//                             @"4":@[@"миллиард", @"миллиарда", @"миллиардов", @"1"],
//                             @"5":@[@"триллион", @"триллиона", @"триллионов", @"1"]};
    
    
    
    return nil;
    
    //рекурсивная функция преобразования целого числа num в рубли
    //        private static String num2words(long num, int level) {
    ////            StringBuilder words = new StringBuilder(50);
    //            NSMutableString *words = [NSMutableString new];
    //            if (num == 0) words append("ноль ");         //исключительный случай
    //            int sex = leword[level][3].indexOf("1") + 1; //не красиво конечно, но работает
    //            int h = (int) (num % 1000);    //текущий трехзначный сегмент
    //            int d = h / 100;              //цифра сотен
    //            if (d > 0) words.append(dig100[d - 1]).append(" ");
    //            int n = h % 100;
    //            d = n / 10;                   //цифра десятков
    //            n = n % 10;                   //цифра единиц
    //            switch (d) {
    //                case 0:
    //                    break;
    //                case 1:
    //                    words.append(dig10[n]).append(" ");
    //                    break;
    //                default:
    //                    words.append(dig20[d - 2]).append(" ");
    //            }
    //            if (d == 1){
    //                n = 0;              //при двузначном остатке от 10 до 19, цифра едициц не должна учитываться
    //            }
    //            switch (n) {
    //                case 0:
    //                    break;
    //                case 1:
    //                case 2:
    //                    words.append(dig1[sex][n - 1]).append(" ");
    //                    break;
    //                default:
    //                    words.append(dig1[0][n - 1]).append(" ");
    //            }
    //
    //            switch (n) {
    //                case 1:
    //                    words.append(leword[level][0]);
    //                    break;
    //                case 2:
    //                case 3:
    //                case 4:
    //                    words.append(leword[level][1]);
    //                    break;
    //                default:
    //                    if ((h != 0) || ((h == 0) && (level == 1)))  //если трехзначный сегмент = 0, то добавлять нужно только "рублей"
    //                        words.append(leword[level][2]);
    //            }
    //            long nextnum = num / 1000;
    //            if (nextnum > 0) {
    //                return (num2words(nextnum, level + 1) + " " + words.toString()).trim();
    //            } else {
    //                return words.toString().trim();
    //            }
    //        }
    //
    //        //функция преобразования вещественного числа в рубли-копейки
    //        //при значении money более 50-70 триллионов рублей начинает искажать копейки, осторожней при работе такими суммами
    //        public static String inwords(double money) {
    //            if (money < 0.0) return "error: отрицательное значение";
    //            String sm = String.format("%.2f", money);
    //            String skop = sm.substring(sm.length() - 2, sm.length());    //значение копеек в строке
    //            int iw;
    //            switch (skop.substring(1)) {
    //                case "1":
    //                    iw = 0;
    //                    break;
    //                case "2":
    //                case "3":
    //                case "4":
    //                    iw = 1;
    //                    break;
    //                default:
    //                    iw = 2;
    //            }
    //            long num = (long) Math.floor(money);
    //            if (num < 1000000000000000l) {
    //                return num2words(num, 1) + " " + skop + " " + leword[0][iw];
    //            } else
    //                return "error: слишком много рублей " + skop + " " + leword[0][iw];
    //        }
    //    }
}

-(NSString *)moneyFormat {
    
    NSString *sum = self;
    NSArray *items = [sum componentsSeparatedByString:@"."];
    NSString *kopeyka = @"";
    if (items.count == 2) {
        kopeyka = [sum componentsSeparatedByString:@"."][1];
    }
    
    sum = [sum componentsSeparatedByString:@"."][0];
    NSMutableArray *array = @[].mutableCopy;
    NSString *greeting;
    if (sum.length <= 3) {
        return self;
    } else{
        while (sum.length > 3) {
            
            NSString *result = [sum substringFromIndex:[sum length] - 3];
            
            sum = [sum substringWithRange:NSMakeRange(0,[sum length]-[result length])];
            [array insertObject:result atIndex:0];
            greeting = [array componentsJoinedByString:@" "];
            
        }
    }
    if (kopeyka.length) {
        sum = [NSString stringWithFormat:@"%@ %@.%@",sum,greeting,kopeyka];
    } else{
        sum = [NSString stringWithFormat:@"%@ %@",sum,greeting];
    }
    
    return sum;
}

-(NSString *)cardNumberFormat{
    if (self.length != 16) {
        return self;
    }
    NSMutableString *result = @"".mutableCopy;
    NSInteger index = 0;
    for (NSInteger k = 0; k<4; k++) {
        NSString *str = [self substringWithRange:NSMakeRange(index, 4)];
        [result appendFormat:@"%@ ",str];
        index += 4;
    }
    return result;
}

-(NSString *)fio{
    NSArray *items = [self componentsSeparatedByString:@" "];
    NSString *first = [[items[0] substringToIndex:1] uppercaseString];
    NSString *str = [[items[0] substringFromIndex:1] lowercaseString];
    NSString *family = [NSString stringWithFormat:@"%@%@",first,str];
    if (items.count == 3) {
        NSString *name = [[items[1] substringToIndex:1] uppercaseString];
        NSString *father = [[items[2] substringToIndex:1] uppercaseString];
        return [NSString stringWithFormat:@"%@ %@.%@",family,name,father];
    }
    if (items.count == 2) {
        NSString *name = [[items[1] substringToIndex:1] uppercaseString];
        return [NSString stringWithFormat:@"%@ %@",family,name];
    }
    
    return self;
}
- (NSString *)MD5 {
    
    if(self == nil || [self length] == 0)
        return nil;
    
    const char *value = [self UTF8String];
    
    unsigned char outputBuffer[CC_MD5_DIGEST_LENGTH];
    CC_MD5(value, (CC_LONG)strlen(value), outputBuffer);
    
    NSMutableString *outputString = [[NSMutableString alloc] initWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(NSInteger count = 0; count < CC_MD5_DIGEST_LENGTH; count++){
        [outputString appendFormat:@"%02x",outputBuffer[count]];
    }
    
    return outputString;
}

- (NSString *) reverseString {
	NSMutableString *reversedStr;
	NSInteger len = [self length];
    
	// auto released string
	reversedStr = [NSMutableString stringWithCapacity:len];
    
	// quick-and-dirty implementation
	while ( len > 0 )
		[reversedStr appendString:[NSString stringWithFormat:@"%C",[self characterAtIndex:--len]]];
    
	return reversedStr;
}

- (NSString *)hexString
{
    NSUInteger len = [self length];
    unichar *chars = malloc(len * sizeof(unichar));
    [self getCharacters:chars];
    
    NSMutableString *hexString = [[NSMutableString alloc] init];
    
    for (NSUInteger i = 0; i < len; i++ ) {
        [hexString appendFormat:@"%02x", chars[i]]; /*EDITED PER COMMENT BELOW*/
    }
    free(chars);    
    return hexString;
}

- (NSString *)base64String
{
    NSData *data = [NSData dataWithBytes:[self UTF8String] length:[self lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    NSUInteger length = [data length];
    NSMutableData *mutableData = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    
    uint8_t *input = (uint8_t *)[data bytes];
    uint8_t *output = (uint8_t *)[mutableData mutableBytes];
    
    for (NSUInteger i = 0; i < length; i += 3) {
        NSUInteger value = 0;
        for (NSUInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        static uint8_t const kAFBase64EncodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        
        NSUInteger idx = (i / 3) * 4;
        output[idx + 0] = kAFBase64EncodingTable[(value >> 18) & 0x3F];
        output[idx + 1] = kAFBase64EncodingTable[(value >> 12) & 0x3F];
        output[idx + 2] = (i + 1) < length ? kAFBase64EncodingTable[(value >> 6)  & 0x3F] : '=';
        output[idx + 3] = (i + 2) < length ? kAFBase64EncodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:mutableData encoding:NSASCIIStringEncoding];
}
//
//- (NSString *)xorStringWithKey:(NSString *)key
//{
//    // Create data object from the string
//    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
//    
//    // Get pointer to data to obfuscate
////    char *dataPtr = (char *) [data bytes];
//    
//    // Get pointer to key data
//    char *keyData = (char *) [[key dataUsingEncoding:NSUTF8StringEncoding] bytes];
//    
//    // Points to each char in sequence in the key
//    char *keyPtr = keyData;
//    int keyIndex = 0;
//    
//    // For each character in data, xor with current value in key
//    for (int x = 0; x < [data length]; x++) {
//        // Replace current character in data with
//        // current character xor'd with current key value.
//        // Bump each pointer to the next character
////        *dataPtr = *dataPtr++ ^ *keyPtr++;
//        
//        // If at end of key data, reset count and
//        // set key pointer back to start of key value
//        if (++keyIndex == [key length])
//            keyIndex = 0, keyPtr = keyData;
//    }
//    
//    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
//
//}


- (BOOL)isEmailValid {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isLinkValid {
    
    NSString *urlRegEx = @"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    
    return [urlTest evaluateWithObject:self];
}
- (NSString *)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}
-(BOOL)isPhoneValid{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    BOOL phoneValidates = [phoneTest evaluateWithObject:self];
    return phoneValidates;
}


//-(NSString *)stringForTag:(NSString*)htmlTag {
//    //    NSRange r;
//    //    NSString *s = [[self copy] autorelease];
//    //    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
//    //        s = [s stringByReplacingCharactersInRange:r withString:@""];
//    //    return [s stringByRemovingNewLinesAndWhitespace];
//    NSError *err;
//    HTMLParser *parser = [[HTMLParser alloc] initWithString:self error:&err];
//    if (!err) {
//        NSString *str = [[[[parser body] findChildTag:htmlTag] contents] trim];
//        return str;
//    }
//    
//    return nil;
//}
//
//- (NSString *)stringTagValueWithName:(NSString *)name
//{
//    NSError *err = NULL;
//    HTMLParser *parser = [[HTMLParser alloc] initWithString:self error:&err];
//    if (!err) {
//        HTMLNode *node = [[parser body] findChildWithAttribute:@"name" matchingName:name allowPartial:YES];
//        NSString *str = [node getAttributeNamed:@"value"];
//        return str;
//    }
//    
//    return nil;
//}

-(NSDictionary*) JSONValue{
    
    NSError *error;
    NSString *str = [NSString stringWithFormat:@"%@",self];
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return dict;
}

+(NSString*) convertDictionaryToString:(NSMutableDictionary*) dict {
    return @"";
//    NSError* error;
//    NSDictionary* tempDict = [dict copy]; // get Dictionary from mutable Dictionary
//    //giving error as it takes dic, array,etc only. not custom object.
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:tempDict
//                                                       options:NSJSONReadingMutableLeaves error:&error];
//    NSString* nsJson=  [[NSString alloc] initWithData:jsonData
//                                             encoding:NSUTF8StringEncoding];
//    return nsJson;
}


@end
