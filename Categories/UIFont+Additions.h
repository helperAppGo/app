//
//  UIFont+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 26.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

#define NULL_TO_NIL(obj)({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj;})

typedef NS_ENUM(NSInteger, FontType){
    Regular = 1,
    Bold,
    Medium
};

@interface UIFont (Additions)

//+(UIFont *)defaultFontWithSize:(CGFloat)size withFontType:(FontType)fontType;

+(UIFont *)defaultFontWithSize:(CGFloat)size bold:(BOOL)bold;
+(UIFont *)visaCard:(CGFloat)size;

@end
