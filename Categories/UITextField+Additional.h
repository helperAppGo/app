//
//  UITextField+Additional.h
//  Milliy
//
//  Created by Admin on 23/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Additional)

-(void)addLeftView:(float)width;
-(void)add998:(UIColor *)color;

@end
