//
//  UIImageView+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 28.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Additions)

-(void)setAvatarWithURL:(NSString *)url;
-(void)setAvatarWithImage:(UIImage *)image;

+ (UIImage *)imageFromColor:(UIColor *)color;
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end

