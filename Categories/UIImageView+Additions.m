//
//  UIImageView+Additions.m
//  TaxiPriz
//
//  Created by Nodir on 28.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import "UIImageView+Additions.h"

@implementation UIImageView (Additions)

-(void)setAvatarWithURL:(NSString *)url{
    NSURL *photoURL = [NSURL URLWithString:url];
    UIImage *placeholder = [UIImage imageNamed:@"avatar_wh"];
    
    self.image = placeholder;
    self.contentMode = UIViewContentModeCenter;
    
    [self sd_setImageWithURL:photoURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if(image){
            self.image = image;
            self.contentMode = UIViewContentModeScaleAspectFill;
        }
    }];
}

-(void)setAvatarWithImage:(UIImage *)image{
    if(image){
        self.image = image;
    } else{
        UIImage *placeholder = [UIImage imageNamed:@"avatar_wh"];
        self.image = placeholder;
    }
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
