//// Header

#import "UIImage+Helper.h"

@implementation UIImage (Helper)

+ (UIImage *)GMSMarkerImageWithString:(NSString *)string color:(UIColor *)color{
    
    UIImage *icon = [UIImage imageNamed:@"new_marker.png"];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, icon.size.width,icon.size.height)];
    [label setText:string];
    [label setTextColor:color];
    [label setFont:[UIFont defaultFontWithSize:14 bold:NO]];
    label.textAlignment = NSTextAlignmentCenter;
    //start drawing
    UIGraphicsBeginImageContext(icon.size);
    
    //draw image
    [icon drawInRect:CGRectMake(0, 0, icon.size.width, icon.size.height)];
    
    //draw label
    [label drawTextInRect:CGRectMake((icon.size.width - label.frame.size.width)/2, -5, label.frame.size.width, label.frame.size.height)];
    
    //get the final image
    UIImage *resultImage  = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}

+(UIImage *)imageWithCorner:(UIImage *)image{
    
    //    UIImage *image = [UIImage imageNamed:@"avatar.png"];
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(50, 50), NO, [UIScreen mainScreen].scale);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 50, 50)
                                cornerRadius:20.0] addClip];
    // Draw your image
    [image drawInRect:CGRectMake(0, 0, 50, 50)];
    
    // Get the image, here setting the UIImageView image
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return result;
}

+ (UIImage *)GMSMarkerImage:(UIImage *)image {
    UIImage  *fgImage = [self imageWithCorner:[UIImage imageNamed:@"avatar.png"]];
    UIImage  *bgImage = [UIImage imageNamed:@"profileMarkericon.png"];
    UIGraphicsBeginImageContextWithOptions(bgImage.size, FALSE, 0.0);
    [bgImage drawInRect:CGRectMake(0, 0, bgImage.size.width, bgImage.size.height)];
    
    [fgImage drawInRect:CGRectMake(2, 2, 40, 40)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
