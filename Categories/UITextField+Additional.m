//
//  UITextField+Additional.m
//  Milliy
//
//  Created by Admin on 23/05/2017.
//  Copyright © 2017 Admin. All rights reserved.
//

#import "UITextField+Additional.h"

@implementation UITextField (Additional)

//- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    if (action == @selector(selectAll:))
//        return YES;
//    else
//        return NO;
//    
//    return [super canPerformAction:action withSender:sender];
//}

-(void)add998:(UIColor *)color{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, self.height)];
    label.font = self.font;
    label.textColor = color;
    label.text = @"998";
    self.leftView = label;
    self.leftViewMode = UITextFieldViewModeAlways;
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    
}

-(void)addLeftView:(float)width{
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, self.height)];
    self.leftView = leftView;
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.leftViewMode = UITextFieldViewModeAlways;
}

-(void)addRightView:(float)width image:(UIImage*)image{
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, width)];
    UIImageView *view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width - 10, width - 10)];
    view.image = image;
    view.contentMode = UIViewContentModeScaleAspectFit;
    view.center = CGPointMake(rightView.width/2, rightView.height/2);
    [rightView addSubview:view];
    self.rightView = rightView;
    self.rightViewMode = UITextFieldViewModeAlways;
}

@end
