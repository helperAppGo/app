//// Header

#import <UIKit/UIKit.h>

@interface UIImage (Helper)
+ (UIImage *)GMSMarkerImageWithString:(NSString *)string color:(UIColor *)color;
+(UIImage *)imageWithCorner:(UIImage *)image;
+ (UIImage *)GMSMarkerImage:(UIImage *)image;
@end
