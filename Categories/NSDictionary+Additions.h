//
//  NSDictionary+Additions.h
//  TaxiPriz
//
//  Created by Nodir on 29.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Additions)

- (NSString *)convertToJsonString;

@end
