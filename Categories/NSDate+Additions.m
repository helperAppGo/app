//
//  NSDate+Additions.m
//  TaxiPriz
//
//  Created by Nodir on 26.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)

+(NSString *)todayWithMinute:(BOOL)minute{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    if (minute) {
        formatter.dateFormat = @"dd-MM-yyyy HH:mm";
    } else{
        formatter.dateFormat = @"dd-MM-yyyy";
    }
    
    return [formatter stringFromDate:[NSDate date]];
}
-(NSString *)timeString{
    NSDate *date = self;
    NSDate *today = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd.MMMM.yyyy";
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ru"];
    NSString *todayString = [formatter stringFromDate:today];
    NSString *dateString = [formatter stringFromDate:date];
    if ([todayString isEqualToString:dateString]) {
        formatter.dateFormat = @"HH:mm";
        return [formatter stringFromDate:date];
    }
    double seconds = [today timeIntervalSince1970] - [date timeIntervalSince1970];
    if (seconds < 24 * 3600) {
        formatter.dateFormat = @"HH:mm";
        return [NSString stringWithFormat:@"Вчера в %@",[formatter stringFromDate:date]];
    }
    if (seconds < 365 * 24 * 3600) {
        formatter.dateFormat = @"dd.MMM HH:mm";
        return [formatter stringFromDate:date];
    }
    formatter.dateFormat = @"dd.MMMM.yyyy";
    return [formatter stringFromDate:date];
}

/*
 № 9 Время  - с этим у нас большие проблемы
 
 Мы показываем сейчас только цыфри 23:34 и и кто его знает когда это было
 
 
 Нам нужно
 
 Если запись было сделана сегодня с 00:00 по 23:59:59 тогда мы пишем только время ( например 22:34)
 Если запись было сделана вчера мы пишем « Вчера в 12:45»
 Если запись была сделана позавчера мы пишем число и месяц и время (год не указываем)
 Если запись была сделала больше года назад мы указываем время - число- месяц-год
 
 Все проверки делаем в зависимости от времени на девайсе

 */
@end
