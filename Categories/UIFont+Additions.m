//
//  UIFont+Additions.m
//  TaxiPriz
//
//  Created by Nodir on 26.03.17.
//  Copyright © 2017 edu.selfCS193p. All rights reserved.
//

#import "UIFont+Additions.h"
/*
 "Helvetica-BoldOblique",
 "Helvetica-Bold",
 "Helvetica-Light"
 */


@implementation UIFont (Additions)

+(UIFont *)defaultFontWithSize:(CGFloat)size withFontType:(FontType)fontType{

    return nil;
}

+(UIFont *)defaultFontWithSize:(CGFloat)size bold:(BOOL)bold{
    float w = [[UIScreen mainScreen] bounds].size.width;
    if (w<414 && w>320) {
        size = size + 1;
    } if(w > 375){
        size = size + 2;
    }
    return bold ? [UIFont fontWithName:@"Helvetica-Bold" size:size] :  [UIFont fontWithName:@"Helvetica-Light" size:size];
}

+(UIFont *)visaCard:(CGFloat)size{
  return [UIFont fontWithName:@"OCRAStd" size:size];
}


@end
