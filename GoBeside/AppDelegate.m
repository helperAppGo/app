//  AppDelegate.m
//  GoBeside

#import "AppDelegate.h"
#import "GBDataFacade.h"
#import "GBServerManager.h"
#import "GBContactsManager.h"
#import "CoreDataManager.h"
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>
#import "MessageViewController.h"
#import "ChatDialogListViewController.h"
#import "FeedListViewController.h"
#import "ProfileViewController.h"

@import Firebase;

#import <GoogleMaps/GoogleMaps.h>


#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#ifdef TEST_SERVER

//#define ID_USER 1261  //0966073490
//#define TOKEN_USER @"fa74c4e026ac960bf6f4712760ca8043bcaf640d74e2c324a98331e97a382cb1"

#define ID_USER 1544    //380966073490
#define TOKEN_USER @"935ce6a66c0894fcc5e4863fb65d7cd7d27e6d8353380a920987552570bdfc13"

//#define ID_USER 1738
//#define TOKEN_USER @"fba4ebf589ecbd89c43754dff178dfc7c45436b2c16e6db8dc03484f3225ac50"

#else
#define ID_USER 1540
#define TOKEN_USER @"5454077821793b5fb5a489fe6acb936d4b40056217352c421c04eade29de1f02"
#endif

@interface AppDelegate () <GBLocationManagerListener,UNUserNotificationCenterDelegate,FIRMessagingDelegate, UITabBarControllerDelegate>{
    NSInteger _currentUserId;
    
}

@property (nonatomic,assign) BOOL locationSent;
@property (nonatomic,assign) BOOL didRefreshUserData;
@property (nonatomic,assign) BOOL shouldSyncContacts;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [FIRApp configure];
    [FIRMessaging messaging].delegate = self;

    [GMSServices provideAPIKey:kGoogleMapsKey];
    [Fabric with:@[[Crashlytics class]]];
    
    [CoreDataManager sharedManager].modelName = @"DataBase";
    [CoreDataManager sharedManager].databaseName = @"DataBase";
    
    
    [[UINavigationBar appearance] setBackgroundColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setTranslucent:NO];
    
    
//    [self configureNotifications];
    self.shouldSyncContacts = YES;
    
//    NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
//    if (![[languages firstObject] isEqualToString:@"ru"]) {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"AppleLanguages"];
        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    [[SettingsManager instance] saveObject:@(NO) key:@"_UIConstraintBasedLayoutLogUnsatisfiable"];
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSInteger k = [[[SettingsManager instance] objForKey:@"uncompleteRegister"] integerValue];
        if (k == 1) {
            [self openRegister];
        } else{
            _currentUserId = user.objId;
            [self openTabbar];
        }
        
    } else{
        [self openLogin];
    }

// -- -- -- -- -- -- -- -- --
//    [GBCommonData setToken:nil];
//    [GBCommonData setUserId:nil];
    
//    [GBCommonData initCommon];

    
    [self configureApperance];
    [SVProgressHUD setMinimumDismissTimeInterval:PROGRESS_HUD_MIN_DISMISS_INTERVAL];
    
//    [GBLocationManager sharedManager];
//    [GBCityModel refreshCitiesDataWithCompletion:nil];
    
    
    [self getUserData];
    
    return YES;
}

-(void)openRegister{
    BaseViewController *obj = [[BaseViewController alloc] init];
    BaseViewController *controller = [obj createController:@"RegisterUserViewController"];
    CustomNavigationController *navController = [[CustomNavigationController alloc] initWithRootViewController:controller];
    self.window = [[UIWindow alloc] init];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}

-(void)openLogin{
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"GBLoginViewController" bundle:nil];
//    UIViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"loginViewController"];
//    self.window = [[UIWindow alloc] init];
//    self.window.backgroundColor = [UIColor whiteColor];
//    self.window.rootViewController = controller;
//    [self.window makeKeyAndVisible];
    
    BaseViewController *obj = [[BaseViewController alloc] init];
    BaseViewController *controller = [obj createController:@"SignViewController"];
    CustomNavigationController *navController = [[CustomNavigationController alloc] initWithRootViewController:controller];
    self.window = [[UIWindow alloc] init];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = navController;
    [self.window makeKeyAndVisible];
}

#pragma mark - User methods
-(void)getUserData{
    
    [[ApiManager sharedManager] getRequest:@"cities" params:@{} callback:^(id response, ErrorObj *error) {
        if (response) {
            response =response[@"response"];
            for (NSDictionary *dict in response) {
                [CDCity createCity:dict];
            }
        }
    }];

    CDUser *user = [[SettingsManager instance] currentUser];
    if (!user) {
        return;
    }
    NSDictionary *params= @{@"data":@(user.objId)};
    
    [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            response =response[@"response"];
            [CDUser currentUserUpdate:response];
        }
    }];
    
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application{
    self.didRefreshUserData = NO;
    self.shouldSyncContacts = YES;
    [UIApplication sharedApplication].applicationIconBadgeNumber = [self appBadgeCount];
}

-(void)activeActions{
    CDUser *user = [[SettingsManager instance] currentUser];
    if (!user) {
        return;
    }
    NSInteger k = [[[SettingsManager instance] objForKey:@"uncompleteRegister"] integerValue];
    if (k == 1) {
        return;
    }
    
    k = [[[SettingsManager instance] objForKey:@"contactPermission"] integerValue];
    if (k == 1) {
        [[ContactSyncManager sharedManager] startSync];
    }
    
    k = [[[SettingsManager instance] objForKey:@"locationPermission"] integerValue];
    if (k == 1) {
        self.locationSent = NO;
        [[GBLocationManager sharedManager] addLocationListener:self];
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application{
    [self activeActions];
}

#pragma mark - Custom Methods

-(void)pushNext{
    UITabBarController *tabbarController =  (UITabBarController *)self.window.rootViewController;
    UINavigationController *navController = tabbarController.viewControllers[0];
    FeedListViewController *controller = navController.viewControllers[0];
    [controller pushNext];
    
}

- (void)configureNotifications{
    if ([UNUserNotificationCenter class] != nil) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [center requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                    [self pushNext];
                });
                
            }
        }];
    } else {
        UIUserNotificationType allNotificationTypes = (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    
}
-(void)application:(UIApplication *)application didRegisterUserNotificationSettings:(nonnull UIUserNotificationSettings *)notificationSettings{
    NSLog(@"Register");
    
}

- (void) configureApperance
{
    [[UINavigationBar appearance] setTintColor:[UIColor blueColor]];
    NSDictionary * titleTextAttrs = @{
                                      NSForegroundColorAttributeName    :   [UIColor blackColor],
                                      //NSFontAttributeName               :   [UIFont systemFontOfSize:17.0 weight:UIFontWeightMedium]
                                      };
    [[UINavigationBar appearance] setTitleTextAttributes:titleTextAttrs];
}



- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    // Print full message.
    DLog(@"application:didReceiveRemoteNotification: %@", userInfo);
}

-(void)sendNotification{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateBadge" object:nil];
}

-(void)soundForNotification{
    NSString *path = [NSString stringWithFormat:@"%@/notifSound.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    // Create audio player object and initialize with URL to sound
    //    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [_audioPlayer play];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    if (state != UIApplicationStateActive) {
        [self openNotification:userInfo];
        return;
    }
    NSString *event = userInfo[@"event"];
    if ([event isEqualToString:@"dialog"]) {
        NSDictionary *messageDict = [userInfo[@"data"] JSONValue];
        MessageModel *message = [[MessageModel alloc] initWithDict:messageDict];
        UITabBarController *tabController = (UITabBarController *)self.window.rootViewController;
        
        UINavigationController *navController = tabController.viewControllers[2];
        ChatDialogListViewController *dialog = navController.viewControllers[0];
        if (navController.viewControllers.count > 1) {
            MessageViewController *controller = navController.viewControllers[1];
            if ([controller chatUserId] == message.fromId) {
                [controller receiveMessage:message];
                return;
            }
        }
        [dialog receiveMessage:message];
        NSInteger  index = tabController.selectedIndex;
        if (index != 2 ) {
            NSString *str =[[SettingsManager instance] objForKey:@"dialog"];
            NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithDictionary:[str JSONValue]];
            NSString *key = [NSString stringWithFormat:@"%@",@(message.fromId)];
            if (!dict[key]) {
                dict[key] = @"1";
            }
            [[SettingsManager instance] saveObject:[dict convertToJsonString] key:@"dialog"];
            [self changeBadge:2 count:dict.allKeys.count];
        }
    } else if([event isEqualToString:@"notification"]){
//        NSDictionary *dict = [userInfo[@"data"] JSONValue];
//        NSArray *notifications = (dict[@"notifications"]);
//        NSArray *answers =  (dict[@"answers"]);
//        NSArray *groups =  (dict[@"groups"]);
//        NSArray *comments =  (dict[@"comments"]);
//        NSArray *posts =  (dict[@"posts"]);
//        NSArray *questions = (dict[@"questions"]);
//        NSArray *users = (dict[@"users"]);
//        NotificationObj *result;
//        for (NSDictionary *nDict in notifications) {
//            NotificationObj *not = [[NotificationObj alloc] initWithDict:nDict];
//            if (not.globalType == 0) {
//                continue;
//            }
//            result = [NotificationObj parsing:nDict users:users groups:groups answers:answers comments:comments posts:posts questions:questions];
//            break;
//        }
        NotificationObj *result = [self notificationWithUserInfo:userInfo];
        if (result) {
            NSString *key = @"tab1";
            NSString *key2 = @"notification1";
            NSInteger index = 0;
            if (result.globalType == 2) {
                key = @"tab3";
                key2 = @"notification3";
                index = 3;
            } else if(result.globalType == 3){
                key = @"tab2";
                key2 = @"notification2";
                index = 1;
            }
            NSInteger count = [[[SettingsManager instance] objForKey:key] integerValue];
            NSInteger count2 = [[[SettingsManager instance] objForKey:key2] integerValue];
            count ++;
            count2 ++;
            [[SettingsManager instance] saveObject:@(count) key:key];
            [[SettingsManager instance] saveObject:@(count2) key:key2];
            [self changeBadge:index count:count];
            [self soundForNotification];
            [self performSelector:@selector(sendNotification) withObject:nil afterDelay:0.4];
        }
    }
    
    if ([event isEqualToString:@"dialog_read"]) {
        NSDictionary *messageDict = [userInfo[@"data"] JSONValue];
        MessageModel *message = [[MessageModel alloc] initWithDict:messageDict];
        UITabBarController *tabController = (UITabBarController *)self.window.rootViewController;
        UINavigationController *navController = tabController.viewControllers[2];
        if (navController.viewControllers.count > 1) {
            MessageViewController *controller = navController.viewControllers[1];
            if (message.fromId == _currentUserId) {
                [controller readMessage:message];
            }
        }
        
    }
    completionHandler(UIBackgroundFetchResultNewData);
}

-(NotificationObj *)notificationWithUserInfo:(NSDictionary *)userInfo{
    NSDictionary *dict = [userInfo[@"data"] JSONValue];
    NSArray *notifications = (dict[@"notifications"]);
    NSArray *answers =  (dict[@"answers"]);
    NSArray *groups =  (dict[@"groups"]);
    NSArray *comments =  (dict[@"comments"]);
    NSArray *posts =  (dict[@"posts"]);
    NSArray *questions = (dict[@"questions"]);
    NSArray *users = (dict[@"users"]);
    NotificationObj *result;
    for (NSDictionary *nDict in notifications) {
        NotificationObj *not = [[NotificationObj alloc] initWithDict:nDict];
        if (not.globalType == 0) {
            continue;
        }
        result = [NotificationObj parsing:nDict users:users groups:groups answers:answers comments:comments posts:posts questions:questions];
        break;
    }
    return result;
}

-(void)openNotification:(NSDictionary *)userInfo{
    NSString *event = userInfo[@"event"];
    if ([event isEqualToString:@"notification"]) {
        NotificationObj *result = [self notificationWithUserInfo:userInfo];
        NSString *type = @"user";
        NSInteger index = 0;
        if (result.globalType == 2) {
            // Groups
            type = @"group";
            index = 3;
        } else if(result.globalType == 3){
            // Question
            type = @"question";
            index = 1;
            
        }  else if(result.globalType == 1){
            type = @"user";
            index = 0;
            
        }
        UITabBarController *tabbarController = (UITabBarController *)self.window.rootViewController;
        [tabbarController setSelectedIndex:index];
        [self changeBadge:index count:0];
        BaseViewController *obj = [[BaseViewController alloc] init];
        BaseViewController *controller = [obj createController:@"NotificationListViewController"];
        controller.type = 1;
        controller.info = @{@"type":type};
        controller.hidesBottomBarWhenPushed = YES;
        UINavigationController *navController = tabbarController.viewControllers[index];
        [navController pushViewController:controller animated:YES];
    }
    if ([event isEqualToString:@"dialog_read"]) {
        UITabBarController *tabbarController = (UITabBarController *)self.window.rootViewController;
        [tabbarController setSelectedIndex:2];
    }
}

- (void) userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(nonnull UNNotificationResponse *)response withCompletionHandler:(nonnull void (^)(void))completionHandler{
    DLog(@"User Info = %@",response.notification.request.content.userInfo);
    [self openNotification:response.notification.request.content.userInfo];
}

-(void)changeBadge:(NSInteger)index count:(NSInteger)count{
    UITabBarController *tabbarController = (UITabBarController *)self.window.rootViewController;
    if (tabbarController.selectedIndex == index) {
        return;
    }
    UITabBarItem *item = tabbarController.tabBar.items[index];
    item.badgeValue = [NSString stringWithFormat:@"%@",@(count)];
}

-(NSInteger)appBadgeCount{
    NSInteger tab1Count = [[[SettingsManager instance] objForKey:@"notification1"] integerValue];
    NSInteger tab2Count = [[[SettingsManager instance] objForKey:@"notification2"] integerValue];
    NSInteger tab3Count = [[[SettingsManager instance] objForKey:@"dialog"] integerValue];
    NSInteger tab4Count = [[[SettingsManager instance] objForKey:@"notification3"] integerValue];
    
    return tab1Count + tab2Count + tab3Count + tab4Count;
    
}

#pragma mark - UITabBarController delegate
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    NSArray *items = tabBarController.viewControllers;
    NSInteger index= [items indexOfObject:viewController];
    if (index == 0) {
        UITabBarItem *item = tabBarController.tabBar.items[0];
        item.badgeValue = nil;
        [[SettingsManager instance] saveObject:nil key:@"tab1"];
        
    }
    
    if (index == 1) {
        UITabBarItem *item = tabBarController.tabBar.items[1];
        item.badgeValue = nil;
        [[SettingsManager instance] saveObject:nil key:@"tab2"];
    }
    
    if (index == 2) {
        UITabBarItem *item = tabBarController.tabBar.items[2];
        item.badgeValue = nil;
        [[SettingsManager instance] saveObject:nil key:@"dialog"];
//        UINavigationController *navController = (UINavigationController *)viewController;
//        ChatDialogListViewController *controller = navController.viewControllers[0];
//        [controller getDialogList];
    }
    
    if (index == 3) {
        UITabBarItem *item = tabBarController.tabBar.items[3];
        item.badgeValue = nil;
        [[SettingsManager instance] saveObject:nil key:@"tab3"];
    }
    
    if (index == 4) {
//        UINavigationController *navController = (UINavigationController *)viewController;
//        ProfileViewController *controller = navController.viewControllers[0];
//        [controller getNow];
    }
    [[SettingsManager instance] anySound];
}

-(void)openTabbar{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    BaseViewController *a = [[BaseViewController alloc] init];
    
    UITabBarController *tabbarController = [[UITabBarController alloc] init];
    tabbarController.delegate = self;
    //    UIViewController *feeds = [storyboard instantiateViewControllerWithIdentifier:@"GBFeedViewController"];
    
    BaseViewController *feeds = [a createController:@"FeedListViewController"];
    //    BaseViewController *feeds = [a createController:@"ChatDialogListViewController"];
    CustomNavigationController *navFeedsController = [[CustomNavigationController alloc] initWithRootViewController:feeds];
    
    
    UIViewController *questionController = [storyboard instantiateViewControllerWithIdentifier:@"GBQuestionsViewController"];
    questionController = [a createController:@"QuestionListViewController"];
    
    CustomNavigationController *navQuestionController = [[CustomNavigationController alloc] initWithRootViewController:questionController];
    
    UIViewController *chatViewController = [storyboard instantiateViewControllerWithIdentifier:@"GBSwitchChat"];
    chatViewController = [a createController:@"ChatDialogListViewController"];
    
    CustomNavigationController *navChatController = [[CustomNavigationController alloc] initWithRootViewController:chatViewController];
    
    UIViewController *groupsController = [storyboard instantiateViewControllerWithIdentifier:@"GBGroupController"];
    groupsController = [a createController:@"GroupListViewController"];
    CustomNavigationController *navGroupController = [[CustomNavigationController alloc] initWithRootViewController:groupsController];
    
    //    UIViewController *profileController = [storyboard instantiateViewControllerWithIdentifier:@"GBProfileViewController"];
    
    
    UIViewController *profileController = [a createController:@"ProfileViewController"];
    
    CustomNavigationController *navProfileController = [[CustomNavigationController alloc] initWithRootViewController:profileController];
    
//    UITabBarController *tabbarController = [[UITabBarController alloc] init];
    [tabbarController.tabBar setBackgroundImage:[UIImage new]];
    [tabbarController setViewControllers:@[navFeedsController,
                                           navQuestionController,
                                           navChatController,
                                           navGroupController,
                                           navProfileController] animated:YES];
    self.window = [[UIWindow alloc] init];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = tabbarController;
    [self.window makeKeyAndVisible];
    
    NSArray *images = @[@"ic_poster", @"ic_questions", @"ic_messager", @"ic_publics", @"ic_acount"];
    
    NSArray *titles = @[@"", @"", @"", @"", @""];
    
    tabbarController.tabBar.backgroundColor = COLOR(255, 255, 255);
    //    tabbarController.tabBar.selectionIndicatorImage = whiteImage;
    //    [[UITabBar appearance] setTintColor:COLOR(255, 255, 255)];
    //    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : appColor} forState:UIControlStateNormal];
    //    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateSelected];
    //    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateHighlighted];
    NSInteger k = 0;
    for (UITabBarItem *item in tabbarController.tabBar.items) {
        UIImage *tabImage = [UIImage imageNamed:images[k]];
        [item setImage:tabImage];
        //        UIImage *tabSelectImage = [UIImage imageNamed:selectImages[k]];
        //        [item setSelectedImage:tabSelectImage];
        item.title =titles[k];
        k++;
    }
    
    [self sendFcmToken];
}

-(void)sendFcmToken{
    NSString *fcmToken = [[SettingsManager instance] objForKey:@"fcmToken"];
    if (fcmToken.length == 0) {
        return;
    }
    NSDictionary *params = @{@"token":fcmToken};
    [[ApiManager sharedManager] putRequest:@"users/firebase" params:params callback:^(id response, ErrorObj *error) {
        NSLog(@"%@",response);
    }];
}
#pragma mark - UNUserNotificationCenterDelegate Methods

//- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
//    // Play sound and show alert to the user
//    // APP IS RUN!
//    DLog(@"User Info = %@",notification.request.content.userInfo);
//    
//    completionHandler(UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge | UNNotificationPresentationOptionSound);
//}

// The method will be called on the delegate when the user responded to the notification by opening the application, dismissing the notification or choosing a UNNotificationAction. The delegate must be set before the application returns from applicationDidFinishLaunching:.



- (void)application:(UIApplication *)application didUpdateUserActivity:(NSUserActivity *)userActivity {
    
}

#pragma mark - FIRMessagingDelegate
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[FIRMessaging messaging] setAPNSToken:deviceToken type:FIRMessagingAPNSTokenTypeProd];
//    [[FIRInstanceID instanceID] setAPNSToken:deviceToken type:FIRMessagingAPNSTokenTypeProd];
//    [FIRMessaging messaging].APNSToken = deviceToken;
}

- (void)messaging:(nonnull FIRMessaging *)messaging didReceiveRegistrationToken:(nonnull NSString *)fcmToken
{
    DLog(@"messaging:didReceiveRegistrationToken: %@",fcmToken);
    [[SettingsManager instance] saveObject:fcmToken key:@"fcmToken"];
    if ([[SettingsManager instance] currentUser]) {
        NSDictionary *params = @{@"token":fcmToken};
        [[ApiManager sharedManager] putRequest:@"users/firebase" params:params callback:^(id response, ErrorObj *error) {
            NSLog(@"%@",response);
        }];
    }
}

- (void)messaging:(nonnull FIRMessaging *)messaging didReceiveMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage
{
    DLog(@"messaging:idReceiveMessage: %@",remoteMessage.appData);
}

// IOS 10 and above
- (void)applicationReceivedRemoteMessage:(nonnull FIRMessagingRemoteMessage *)remoteMessage{
    DLog(@"messaging:idReceiveMessage: %@",remoteMessage.appData);
}




#pragma mark - GBLocationManagerListener

- (void) didUpdateToLocation:(CLLocation*)location lastKnownLocation:(CLLocation*)lastKnownLocation
{
//    if ((!SECRET_TOKEN.length) || (USER_ID.longLongValue == 0)) return;
    CDUser *user = [[SettingsManager instance] currentUser];
    if (!user) {
        return;
    }
    self.locationSent = YES;
    NSMutableDictionary *parameters = @{}.mutableCopy;
    parameters[@"lat"] = @(lastKnownLocation.coordinate.latitude);
    parameters[@"lon"] = @(lastKnownLocation.coordinate.longitude);
    [[ApiManager sharedManager] putRequest:@"users/geo" params:parameters callback:^(id response, ErrorObj *error) {
        if (response) {
            NSLog(@"%@",response);
        }
    }];
    /*
     NSMutableDictionary *parameters = [self parametersBasic];
     parameters[@"lat"] = @(latitude);
     parameters[@"lon"] = @(longitude);
     
     [self sendPUTRequest:@"users/geo" parameters:parameters success:success failure:failure];
     */
//    [[GBServerManager sharedManager] sendLatitude:location.coordinate.latitude longitude:location.coordinate.longitude success:^(id responseObject) {
//        DLog(@"Coordinates are sent:\n%@",responseObject);
//    } failure:^(NSError *error, NSDictionary *userInfo) {
//        DLog(@"Failed to send coordibates: %@\n%@",error,userInfo);
//        self.locationSent = NO;
//    }];
}

//- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    return self.shouldRotate ? UIInterfaceOrientationMaskAllButUpsideDown : UIInterfaceOrientationMaskPortrait;
//}


@end
