//  AppDelegate.h
//  GoBeside

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <AVFoundation/AVFoundation.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>{
    
    AVPlayer *_audioPlayer;
}

@property (strong, nonatomic) UIWindow *window;

@property (assign, nonatomic) BOOL shouldRotate;
-(void)openTabbar;
-(void)openLogin;
-(NSInteger)appBadgeCount;
-(void)configureNotifications;

-(void)activeActions;
@end

