//// Header

#import "NotificationService.h"
//#import "DialogModel.h"
#import "UIFont+Additions.h"
#import "NotificationObj.h"
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#define NULL_TO_NIL2 (obj)({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj;})

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
//    [[request.content mutableCopy] userInfo];
//
    // Modify the notification content here...
//    self.bestAttemptContent.title = @"Title";
//    self.bestAttemptContent.subtitle = @"sd";
    
    NSDictionary *info = request.content.userInfo;
    NSString *event = info[@"event"];
    NSDictionary *messageObj = [self dictWIthString:info[@"data"]];
    if ([event isEqualToString:@"dialog"]) {
//        MessageModel *message = [[MessageModel alloc] initWithDict:info[@"data"]];
        
        NSInteger files = [messageObj[@"files"] integerValue];
        if (files > 0) {
            self.bestAttemptContent.title = messageObj[@"name"];
            self.bestAttemptContent.body = @"Фото";
        } else{
            self.bestAttemptContent.title = messageObj[@"name"];
            self.bestAttemptContent.body = messageObj[@"text"];
        }
    } else if([event isEqualToString:@"notification"]){
        NSDictionary *dict = messageObj;
        NSArray *notifications = (dict[@"notifications"]);
        NSArray *answers =  (dict[@"answers"]);
        NSArray *groups =  (dict[@"groups"]);
        NSArray *comments =  (dict[@"comments"]);
        NSArray *posts =  (dict[@"posts"]);
        NSArray *questions = (dict[@"questions"]);
        NSArray *users = (dict[@"users"]);
        NotificationObj *result;
        for (NSDictionary *nDict in notifications) {
            NotificationObj *not = [[NotificationObj alloc] initWithDict:nDict];
            if (not.globalType == 0) {
                continue;
            }
            result = [NotificationObj parsing:nDict users:users groups:groups answers:answers comments:comments posts:posts questions:questions];
            break;
        }
        if (result) {
            self.bestAttemptContent.title = [result.topString string];
            self.bestAttemptContent.body = result.descString;
        }
        
    } else if([event isEqualToString:@"dialog_read"]){
        /*
         Имя Фамилия
         Прочитал ваше сообщения
         */
        self.bestAttemptContent.title = messageObj[@"name"];
        self.bestAttemptContent.body = @"Прочитал ваше сообщения!";
        
    }
    NSInteger  count = [(AppDelegate *)[UIApplication sharedApplication] appBadgeCount];
    count ++;
    [UIApplication sharedApplication].applicationIconBadgeNumber = count;
    self.bestAttemptContent.sound = [UNNotificationSound soundNamed:@"pushSound.wav"];
    self.contentHandler(self.bestAttemptContent);
}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

-(NSDictionary *)dictWIthString:(NSString *)str{
    NSError *error;
    NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&error];
    if (error) {
        return nil;
    }
    return dict;
}

@end
