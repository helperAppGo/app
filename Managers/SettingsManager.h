

#import <Foundation/Foundation.h>
#import "NSString+MD5Addition.h"
#include "TargetConditionals.h"


#define appLanguages            @[@"",@"uk",@"ru",@"en"]

#define kUserToken              @"autToken"
#define kDeviceToken            [@"deviceToken" MD5]
#define kAppLanguage            @"appLanguage"
#define kAppMenuImage           [@"manuImage" MD5]
#define kPasswordLength         5

#define kUserId              [@"userId" MD5]
#define kUserName            [@"userName" MD5]
#define kUserPhone           [@"userPhone" MD5]

#define kSessionId           [@"sessionId" MD5]

#define kPaperSizeA4 CGSizeMake(595.2,841.8)
#define kPaperSizeA5 CGSizeMake(420.0, 595.0)


#define kAlphabet @[@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z"]

#define kGoogleMapsKey   @"AIzaSyD4Dbl7NMqqeRxTi2W2bYWvNDpNFT3PQDg"
#define kApiServer       [@"apiServer" MD5]
#define kBasketCount     [@"basketCount" MD5]
#define kNBUCardCode     @"02"

#define appPopubBgColor      COLOR(240, 240, 240)
#define appPopubTextColor    COLOR(100, 100, 100)
#define appPopubActionColor     COLOR(120, 120, 120)
#define appBlueColor     COLOR(44, 124, 246)

#define serverUrl  [[SettingsManager instance] apiUrl]

#define iTunesUrl @""

#define NBULogo @""

@class CDUser;

@interface SettingsManager : NSObject
@property (nonatomic , strong) NSDictionary *orderTypes;
@property (nonatomic) BOOL isLastVersion;

@property (nonatomic, strong) NSArray *customers;

+ (SettingsManager *)instance;

-(CDUser *)currentUser;

- (void)saveObject:(id)obj key:(NSString*)key;
- (id)objForKey:(NSString*)key;

// Language methods
- (NSInteger)appLanguage;
- (NSLocale *)appLocale;
- (NSInteger)internetStatus;
- (void)changeLanguage:(NSInteger)index;
- (NSString *)translate:(NSString*)str;


-(NSArray *)cardColors;

- (float)offset;
- (void)logOut;
- (BOOL)isLogin;
-(NSString *)apiUrl;

-(void)anySound;

@end
