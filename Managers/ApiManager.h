//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

#define productionVersion 1
#define usingRSA productionVersion
#define usingYurRSA 0

@interface ErrorObj : NSObject
@property(nonatomic, strong)NSString *message;
@property(nonatomic, assign)NSInteger code;
@end

typedef void (^Callback)(id response, ErrorObj *error);

@interface ApiManager : NSObject


+ (ApiManager *)sharedManager;

-(void)postRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback;
-(void)getRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback;
-(void)patchRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback;
-(void)putRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback;
-(void)deleteRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback;
-(void)uploadPhoto:(NSData *)data fileName:(NSString *)fileName callback:(Callback)callback;
-(void)postPhonesRequest:(NSString *)path params:(NSArray *)params callback:(Callback)callback;

- (void)youtubeList:(NSDictionary *)params callback:(Callback)callback;

@end
