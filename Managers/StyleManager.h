
#import <Foundation/Foundation.h>

@interface StyleManager : NSObject

#pragma mark - LeftMenu style


+ (void) styleBtn:(UIButton*)btn image:(UIImage *)image;
+ (void) styleBtn:(UIButton*)btn title:(NSString*)title;
+ (void) styleBtn:(UIButton*)btn bgImage:(UIImage *)image;
+ (void) styleBtn:(UIButton*)btn titleColor:(UIColor*)color;

+ (void) styleView:(UIView*)view cornerR:(float)cornerR border:(UIColor *)color borderW:(float)borderW bgColor:(UIColor*)bgColor;

+ (void) styleLabel:(UILabel*)label font:(UIFont*)font color:(UIColor*)color text:(NSString *)text;

+ (NSArray *) styleBackItem:(id)controller selector:(SEL)selector;

+ (UIImage*) styleImageWithColor:(UIColor*)color size:(CGSize)size;

+ (CGSize)size:(NSString*)string with:(CGFloat)width fontSize:(CGFloat)fontsize;

+ (void)addShadow:(UIView *)view radius:(float)radius;
@end
