

#import "StyleManager.h"

@interface BarButton : UIButton
@end

@implementation BarButton

- (UIEdgeInsets)alignmentRectInsets {
    UIEdgeInsets insets;
//    if (IF_ITS_A_LEFT_BUTTON) {
        insets = UIEdgeInsetsMake(0, 9.0f, 0, 0);
//    }
//    else { // IF_ITS_A_RIGHT_BUTTON
//        insets = UIEdgeInsetsMake(0, 0, 0, 9.0f);
//    }
    return insets;
}
@end


@implementation StyleManager

+(void)styleBtn:(UIButton*)btn bgImage:(UIImage *)image{
    [btn setBackgroundImage:image forState:UIControlStateNormal];
    [btn setBackgroundImage:image forState:UIControlStateSelected];
    [btn setBackgroundImage:image forState:UIControlStateHighlighted];
    
}

+(void)styleBtn:(UIButton*)btn image:(UIImage *)image{
    [btn setImage:image forState:UIControlStateNormal];
    [btn setImage:image forState:UIControlStateSelected];
    [btn setImage:image forState:UIControlStateHighlighted];
}

+(void)styleBtn:(UIButton *)btn title:(NSString *)title{
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateSelected];
    [btn setTitle:title forState:UIControlStateHighlighted];
//    btn.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    btn.titleLabel.adjustsFontSizeToFitWidth = YES;
}

+(void)styleBtn:(UIButton*)btn titleColor:(UIColor*)color {
    [btn setTitleColor:color forState:UIControlStateNormal];
    [btn setTitleColor:color forState:UIControlStateSelected];
    [btn setTitleColor:color forState:UIControlStateHighlighted];
}

+(UIImage*)styleImageWithColor:(UIColor*)color size:(CGSize)size{
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (void) styleView:(UIView*)view cornerR:(float)cornerR border:(UIColor *)color borderW:(float)borderW bgColor:(UIColor*)bgColor{
	view.layer.cornerRadius = cornerR;
	view.layer.borderWidth = borderW;
	view.layer.borderColor = color.CGColor;
	view.backgroundColor = bgColor;
}


+(NSArray *)styleBackItem:(id)controller selector:(SEL)selector{
    BarButton *btn = [[BarButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    btn.backgroundColor = [UIColor clearColor];
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 23)];
    iconView.image = [[UIImage imageNamed:@"backIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    iconView.tintColor = COLOR(150, 150, 150);
    iconView.center = CGPointMake(8, btn.height/2);
    [btn addSubview:iconView];
    [btn addTarget:controller action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -20;
    return @[item];
}

+ (CGSize)size:(NSString*)string with:(CGFloat)width fontSize:(CGFloat)fontsize{
    NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
    style.lineBreakMode = NSLineBreakByWordWrapping;
    CGRect rect = [string ? string : @"0" boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO], NSParagraphStyleAttributeName : style} context:nil];
    if (rect.size.height<15) {
        rect.size.height = 15;
    }
    return rect.size;
}

+ (void) styleLabel:(UILabel*)label font:(UIFont*)font color:(UIColor*)color text:(NSString *)text{
    label.font = font;
    label.textColor = color;
    label.text = text;
    label.backgroundColor = [UIColor clearColor];
}

+ (void)addShadow:(UIView *)view radius:(float)radius{
    view.layer.cornerRadius = radius;
    view.backgroundColor = [UIColor whiteColor];
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(-1, 1);
    view.layer.shadowColor = [[UIColor blackColor] colorWithAlphaComponent:0.4].CGColor;
    view.layer.shadowOpacity = 0.5;
    view.layer.shadowPath = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
}

@end
