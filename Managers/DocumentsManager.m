//


#import "DocumentsManager.h"
#import <AFNetworking/AFNetworking.h>
#import "AppDelegate.h"

@implementation PDFFileItem

@end

@interface DocumentsManager(){
    NSMutableDictionary *_languageDict;
    NSMutableDictionary *_localizationDict;
	NSInteger _internetStatus;
    NSArray *_cardColors;
}

@end

@implementation DocumentsManager

- (instancetype)init {
    self = [super init];
    if (self) {
    
    }
    return self;
}

+ (DocumentsManager *)instance {
    static DocumentsManager *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}

-(NSInteger )lastDocumentId{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd-MM-yyyy";
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSString *key = [[[[dateString MD5] MD5] MD5] MD5];
    
    NSInteger objId = [[[SettingsManager instance] objForKey:key] integerValue];
    [[SettingsManager instance] saveObject:@(objId + 1) key:key];
    return objId + 1;
}

-(NSString *)documentName{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"dd-MM-yyyy";
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    return [NSString stringWithFormat:@"%@-%@",@([self lastDocumentId]), dateString];
}

-(NSString *)documentDirectory{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/PdfDocuments"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:nil];
    }
    
    return dataPath;
}

-(NSArray *)fileList{
    NSString *folderPath = [self documentDirectory];
    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self documentDirectory] error:NULL];
    NSMutableArray *sortedPaths = [NSMutableArray new];
    for (NSString *path in directoryContent) {
        NSString *fullPath = [folderPath stringByAppendingPathComponent:path];
        
        NSDictionary *attr = [NSFileManager.defaultManager attributesOfItemAtPath:fullPath error:nil];
        NSDate *modDate = [attr objectForKey:NSFileCreationDate];
        
        PDFFileItem *pathWithDate = [[PDFFileItem alloc] init];
        pathWithDate.path = path;
        pathWithDate.date = modDate;
        [sortedPaths addObject:pathWithDate];
    }
    
    [sortedPaths sortUsingComparator:^(PDFFileItem *path1, PDFFileItem *path2) {
        return [path2.date compare:path1.date];
    }];
    return sortedPaths;
    
//    NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self documentDirectory] error:NULL];
//    return directoryContent;
}

-(NSString *)filePath:(NSString *)fileName{
    NSString *path = [self documentDirectory];
    return [NSString stringWithFormat:@"%@/%@.pdf",path,fileName];
//    [path stringByAppendingPathComponent:fileName];
//    return path;
}

@end
