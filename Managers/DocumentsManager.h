//


#import <Foundation/Foundation.h>

@interface PDFFileItem : NSObject
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSDate *date;
@end


@interface DocumentsManager : NSObject

+ (DocumentsManager *)instance;

-(NSString *)documentName;
-(NSString *)filePath:(NSString *)fileName;
-(NSArray *)fileList;

@end
