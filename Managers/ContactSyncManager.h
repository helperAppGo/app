//// Header

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>


@interface ContactSyncManager : NSObject{
    ABAddressBookRef _adressBook;
}

+ (ContactSyncManager *)sharedManager;
-(void)startSync;

@end
