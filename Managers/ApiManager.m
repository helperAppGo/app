
#import "ApiManager.h"
#import <AFNetworking/AFNetworking.h>

#import <AFNetworking/AFURLRequestSerialization.h>
#import <AFNetworking/AFURLResponseSerialization.h>
#import "AFNetworkReachabilityManager.h"
#import "AFHTTPRequestSerializer-GB.h"


@implementation ErrorObj
@end

typedef NS_ENUM(NSInteger, HMPApiMethodType) {
    GET = 0,
    POST,
    DELETE,
    PUT
};

@implementation ApiManager 

+ (ApiManager *)sharedManager {
    static ApiManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[ApiManager alloc] init];
    });
    
    return sharedManager;
}
-(NSString *)mobilDevice{
    NSString *deviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *model = [[UIDevice currentDevice] model];
    NSString *device = [NSString stringWithFormat:@"%@|%@|%@",model,@"iOS", deviceId];
    return device;
}

-(void)postRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback{

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    
    NSMutableDictionary *parameters = @{}.mutableCopy;
    [parameters addEntriesFromDictionary:params];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,path];
    [manager POST:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        id result = nil;
        ErrorObj *errObj = [[ErrorObj alloc] init];
        errObj.message = @"Server error";
        if (responseObject[@"error"]) {
            
        } else{
            result = responseObject;
            errObj = nil;
        }
        
        if (callback) {
            callback(result, errObj);
        }
        [manager invalidateSessionCancelingTasks:YES];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }];
    
    
}

-(void)getRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
//    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
//    serializer.removesKeysWithNullValues = YES;
//    serializer.readingOptions = NSJSONReadingMutableContainers | NSJSONReadingAllowFragments;
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
//    AFHTTPRequestSerializer_GB* requestSerializer = [AFHTTPRequestSerializer_GB serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    NSMutableDictionary *parameters = @{}.mutableCopy;
    [parameters addEntriesFromDictionary:params];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,path];
        
    [manager GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        id result = nil;
        ErrorObj *errObj = [[ErrorObj alloc] init];
        errObj.message = @"Server error";
        if (responseObject[@"error"]) {
            
        } else{
            result = responseObject;
            errObj = nil;
        }
        
        if (callback) {
            callback(result, errObj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }];
}

-(void)putRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    NSMutableDictionary *parameters = @{}.mutableCopy;
    [parameters addEntriesFromDictionary:params];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,path];
    
    [manager PUT:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        id result = nil;
        ErrorObj *errObj = [[ErrorObj alloc] init];
        errObj.message = @"Server error";
        if (responseObject[@"error"]) {
            
        } else{
            result = responseObject;
            errObj = nil;
        }
        
        if (callback) {
            callback(result, errObj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }];
}


-(void)deleteRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    NSMutableDictionary *parameters = @{}.mutableCopy;
    [parameters addEntriesFromDictionary:params];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,path];
    
    [manager DELETE:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        id result = nil;
        ErrorObj *errObj = [[ErrorObj alloc] init];
        errObj.message = @"Server error";
        if (responseObject[@"error"]) {
            
        } else{
            result = responseObject;
            errObj = nil;
        }
        
        if (callback) {
            callback(result, errObj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    }];
}
-(void)patchRequest:(NSString *)path params:(NSDictionary *)params callback:(Callback)callback{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    NSMutableDictionary *parameters = @{}.mutableCopy;
    [parameters addEntriesFromDictionary:params];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,path];
    
    [manager PATCH:urlString parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        id result = nil;
        ErrorObj *errObj = [[ErrorObj alloc] init];
        errObj.message = @"Server error";
        if (responseObject[@"error"]) {
            
        } else{
            result = responseObject;
            errObj = nil;
        }
        
        if (callback) {
            callback(result, errObj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    }];
}

-(void)uploadPhoto:(NSData *)data fileName:(NSString *)fileName callback:(Callback)callback{
    NSMutableDictionary *parameters = @{}.mutableCopy;
    
    NSString *mimeType = @"text/plain";
    NSString *ext = [[fileName pathExtension] lowercaseString];
    if ([ext isEqualToString:@"jpg"] || [ext isEqualToString:@"jpeg"])
        mimeType = @"image/jpg";
    else if ([ext isEqualToString:@"gif"])
        mimeType = @"image/gif";
    else if ([ext isEqualToString:@"png"])
        mimeType = @"image/gif";
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [manager.requestSerializer setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [manager.requestSerializer setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,@"files"];
    
    [manager POST:urlString parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
         [formData appendPartWithFileData:data name:@"File" fileName:fileName mimeType:mimeType];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (callback) {
            callback(responseObject,nil);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    }];
}

-(void)postPhonesRequest:(NSString *)path params:(NSArray *)params callback:(Callback)callback{
    NSString *urlString = [NSString stringWithFormat:@"%@/api/%@",BASE_URL,path];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
    NSString* jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:120];
    
    [request setHTTPMethod:@"POST"];
    [request setValue: @"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue: @"application/json" forHTTPHeaderField:@"Accept"];
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user) {
        NSString *userIdString = [NSString stringWithFormat:@"%@",@(user.objId)];
        [request setValue:user.token forHTTPHeaderField:@"X-PHONE-TOKEN"];
        [request setValue:userIdString forHTTPHeaderField:@"X-USER"];
    }
    
    [request setHTTPBody: [jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionTask *task = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        ErrorObj *obj = nil;
        if (error) {
            obj = [[ErrorObj alloc] init];
            NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
            obj.message = str;
            if (callback) {
                callback(nil, obj);
            }
        } else{
            if (callback) {
                callback(responseObject,nil);
            }
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    }];
    
    [task resume];
}

#pragma mark - Youtube list methods

- (void)youtubeList:(NSDictionary *)params callback:(Callback)callback{
//    https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&chart=mostPopular&regionCode=UA&key=AIzaSyBez4-AE-41aNdFzATun891O_DuAKJLv4k
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSString *key = @"AIzaSyBez4-AE-41aNdFzATun891O_DuAKJLv4k";
//    part=snippet,contentDetails&chart=mostPopular&regionCode=UA&key=
    //search?pageToken
    
    NSMutableDictionary *parameters = @{@"chart":@"mostPopular",
                                        @"part":@"snippet,contentDetails",
                                        @"regionCode":@"UA",
                                        @"maxResults":@"10",
                                        @"key":key}.mutableCopy;
    if (params[@"q"]) {
        parameters = @{@"type":@"video",
                       @"part":@"snippet",
                       @"maxResults":@"10",
                       @"key":key}.mutableCopy;
    }
    
    [parameters addEntriesFromDictionary:params];
    NSString *urlString = @"https://www.googleapis.com/youtube/v3/videos";
    if (params[@"q"]) {
        urlString = @"https://www.googleapis.com/youtube/v3/search";
    }
    
    [manager GET:urlString parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        id result = nil;
        result = responseObject;
        if (callback) {
            callback(result, nil);
        }
        [manager invalidateSessionCancelingTasks:YES];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        ErrorObj *obj = [[ErrorObj alloc] init];
        NSString *str = [NSString stringWithFormat:@"%@(%@)",error.userInfo[@"NSLocalizedDescription"], @(error.code)];
        obj.message = str;
        if (callback) {
            callback(nil, obj);
        }
        [manager invalidateSessionCancelingTasks:YES];
    }];
    
    
}


@end
