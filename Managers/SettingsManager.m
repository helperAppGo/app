

#import "SettingsManager.h"
#import <AFNetworking/AFNetworking.h>
#import "AppDelegate.h"
#import "CDUser+CoreDataClass.h"
#import <AVFoundation/AVFoundation.h>

@interface SettingsManager(){
    NSMutableDictionary *_languageDict;
    NSMutableDictionary *_localizationDict;
	NSInteger _internetStatus;
    NSArray *_cardColors;
    AVPlayer *_player;
}

@end

@implementation SettingsManager

- (instancetype)init {
    self = [super init];
    if (self) {
        _localizationDict = @{}.mutableCopy;
        _languageDict = @{}.mutableCopy;
        [self loadLanguageDict];
//        id item = [self objForKey:kAppLanguage];
        
        NSString *deviceLanguage = [[NSLocale preferredLanguages] firstObject];
        if ([deviceLanguage containsString:@"uk"]) {
            [self changeLanguage:1];
        } else if([deviceLanguage containsString:@"ru"]){
            [self changeLanguage:2];
        } else{
            [self changeLanguage:3];
        }
//        [self changeLanguage:2];
        
        

    }

    return self;
}

-(float)offset{
    float k = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ? 0.55 : 0.2;
    return [UIScreen mainScreen].bounds.size.width *k;
}

+ (SettingsManager *)instance {
    static SettingsManager *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}

-(void)anySound{
    NSString *path = [NSString stringWithFormat:@"%@/any_buttom.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    _player.volume = 0.25;
    [_player play];
}

-(void)saveObject:(id)obj key:(NSString *)key{
    if (obj) {
        [[NSUserDefaults standardUserDefaults] setObject:obj forKey:key];
    } else{
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(id)objForKey:(NSString *)key{
    if (key.length) {
        return [[NSUserDefaults standardUserDefaults] objectForKey:key];
    }
    return nil;
}

-(void)logOut{
    [CDUser deleteAll];
    [[CoreDataManager sharedManager] saveContext];
    
}

-(BOOL)isLogin{
    id token = [self objForKey:kUserId];
    return token ? YES : NO;
}

-(NSString *)apiUrl{
    return @"";
}

#pragma mark - Language methods

-(NSInteger)appLanguage{
	NSString *firstLanguage = [[NSLocale preferredLanguages] firstObject];
	NSString *language = [[firstLanguage componentsSeparatedByString:@"-"] firstObject];
	NSInteger languageId = 2;
	if([language isEqualToString:@"ru"]){
		languageId = 1;
	}
	if([self objForKey:kAppLanguage]){
		languageId = [[self objForKey:kAppLanguage] integerValue];
	}
	return languageId;
}

-(NSLocale *)appLocale{
	NSInteger langIndex = [self appLanguage];
	NSString *identifier = appLanguages[langIndex];
	return [NSLocale localeWithLocaleIdentifier:identifier];
}

-(void)changeInternetConnection:(AFNetworkReachabilityStatus)status{
	AFNetworkReachabilityManager *reachabilityManager = [AFNetworkReachabilityManager sharedManager];
	BOOL reachable = reachabilityManager.reachable;
	_internetStatus = reachable  ? 1 : 0;
	[[NSNotificationCenter defaultCenter] postNotificationName:@"changeInternetConnection" object:nil];
}

-(NSInteger)internetStatus{
	AFNetworkReachabilityManager *reachabilityManager = [AFNetworkReachabilityManager sharedManager];
	BOOL reachable = reachabilityManager.reachable;
	_internetStatus = reachable  ? 1 : 0;

	return _internetStatus;
}

-(NSString *)translate:(NSString*)str{
    if (_languageDict[str]) {
        return _languageDict[str];
    }
    return str;
}

-(void)changeLanguage:(NSInteger)index{
    [self saveObject:@(index) key:kAppLanguage];
    [self changeLanguageDict];
}

-(void)loadLanguageDict{
    NSString *fileName = @"translate";
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:fileName ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *langDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    if (langDict) {
        [_localizationDict setDictionary:langDict];
        NSString *langKey = appLanguages[[self appLanguage]];
        [_languageDict setDictionary:_localizationDict[langKey]];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"lang" object:nil];
}


-(void)changeLanguageDict{
    NSString *langKey = appLanguages[[self appLanguage]];
    [_languageDict setDictionary:_localizationDict[langKey]];
}

-(NSArray *)cardColors{
    if (_cardColors.count) {
        return _cardColors;
    }

    _cardColors = @[];
    return _cardColors;
    
}

-(CDUser *)currentUser{
    NSPredicate *tokenPredicate = [NSPredicate predicateWithFormat:@"token.length > 0"];
    CDUser *user = [CDUser find:tokenPredicate];
    return user;
}

@end
