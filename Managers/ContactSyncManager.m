//// Header

#import "ContactSyncManager.h"

@implementation ContactSyncManager

+ (ContactSyncManager *)sharedManager {
    static ContactSyncManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[ContactSyncManager alloc] init];
    });
    
    return sharedManager;
}

-(void)startSync{
    [self checkAccessToContacts];
//    [self contactScan];
}

- (void) contactScan{
    if ([CNContactStore class]) {
        //ios9 or later
        CNEntityType entityType = CNEntityTypeContacts;
        if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
        {
            CNContactStore * contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if(granted){
                    [self getAllContact];
                }
            }];
        }
        else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
        {
            [self getAllContact];
        }
    }
}

-(void)getAllContact{
    
    if([CNContactStore class]) {
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc]init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey,CNContactDatesKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            [self parseContactWithContact:contact];
        }];
    }
}

- (void)parseContactWithContact :(CNContact* )contact {
//    NSString * firstName =  contact.givenName;
//    NSString * lastName =  contact.familyName;
//
////    CNPhoneNumber
//    NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
//    NSString * email = [contact.emailAddresses valueForKey:@"value"];
//    NSArray * addrArr = [self parseAddressWithContac:contact];
}

- (NSMutableArray *)parseAddressWithContac: (CNContact *)contact{
//    NSMutableArray * addrArr = [[NSMutableArray alloc]init];
//    CNPostalAddressFormatter * formatter = [[CNPostalAddressFormatter alloc]init];
//    NSArray * addresses = (NSArray*)[contact.postalAddresses valueForKey:@"value"];
//    if (addresses.count > 0) {
//        for (CNPostalAddress* address in addresses) {
//            [addrArr addObject:[formatter stringFromPostalAddress:address]];
//        }
//    }
//    return addrArr;
    return @[].mutableCopy;
}



#pragma mark - Old framework
- (void) checkAccessToContacts {
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined) {
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
            if (granted) {
                [self startSync:addressBook];
            }
        });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        [self startSync:addressBook];
    }
}

-(ABAddressBookRef)addressBook{
    return _adressBook;
}

-(NSString *)clearSymbolsNumber:(NSString *)phoneString{
    if (!phoneString) {
        return nil;
    }
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@"+" withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@"-" withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@")" withString:@""];
    phoneString = [phoneString stringByReplacingOccurrencesOfString:@"(" withString:@""];
    phoneString =  [[phoneString componentsSeparatedByCharactersInSet:NSCharacterSet.whitespaceCharacterSet] componentsJoinedByString:@""];
    return phoneString;
    
}

-(void)startSync:(ABAddressBookRef)addressBook {
    [[SettingsManager instance] saveObject:@(1) key:@"contactPermission"];
    _adressBook = addressBook;
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    NSTimeInterval lastSyncTime = 0;
    if ([[SettingsManager instance] objForKey:kContactSyncDate]) {
        lastSyncTime = [[[SettingsManager instance] objForKey:kContactSyncDate] floatValue];
    } else{
        
    }
    
//    if (currentTime - lastSyncTime < 3600) {
//        return;
//    }
    
    if(addressBook != nil) {
        CFIndex nPeople = ABAddressBookGetPersonCount(addressBook);
        if(nPeople > 0){
            CFArrayRef allPeople = ABAddressBookCopyArrayOfAllPeople(addressBook);
            
            NSMutableArray *actualContacts = @[].mutableCopy;
            
            for (int index = 0; index < nPeople; ++index) {
                ABRecordRef person = CFArrayGetValueAtIndex(allPeople, index);
                
                NSDate *modificationDate = (__bridge NSDate*) ABRecordCopyValue(person, kABPersonModificationDateProperty);
                NSTimeInterval mDate = [modificationDate timeIntervalSince1970];
                BOOL aa = mDate > lastSyncTime && mDate>0;
                if (aa) {
                    ABMultiValueRef phones =(__bridge ABMultiValueRef)((__bridge NSString*)ABRecordCopyValue(person, kABPersonPhoneProperty));
                    NSNumber *contactID = [NSNumber numberWithInt:ABRecordGetRecordID(person)];
                    CFStringRef firstName = ABRecordCopyValue(person, kABPersonFirstNameProperty);
                    CFStringRef lastName = ABRecordCopyValue(person, kABPersonLastNameProperty);
                    NSString  *lName = lastName ? [NSString stringWithFormat:@"%@",lastName] : @"";
                    NSString  *fName = firstName ? [NSString stringWithFormat:@"%@",firstName] : @"";
                    NSString* phoneString = nil;
                    for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++) {
                        NSString *mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
                        
                        if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel]) {
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                            
                        }
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel]) {
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        }
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        if ([mobileLabel isEqualToString:(NSString*)CNLabelPhoneNumberMain]) {
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        }
                        
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneHomeFAXLabel]) {
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        }
                        
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneWorkFAXLabel]) {
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        }
                        
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneOtherFAXLabel]) {
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        }
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        if([mobileLabel isEqualToString:(NSString *)kABPersonPhonePagerLabel]){
                            phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        }
                        if (phoneString.length>0) {
                            break;
                        }
                        
                        phoneString = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
                        if (phoneString.length>0) {
                            break;
                        }
                    }
                    
                    if (!phoneString) {
                        continue;
                    }
                    if (!contactID) {
                        continue;
                    }
                    phoneString = [self clearSymbolsNumber:phoneString];
                    if ([phoneString hasPrefix:@"380"]) {
                        NSString *name = nil;
                        if (fName.length && lName.length) {
                            name = [NSString stringWithFormat:@"%@ %@",fName, lName];
                        } else{
                            name = fName.length ? fName : lName;
                        }
                        NSDictionary *contactDict = @{@"phone":@([phoneString longLongValue]),
                                                      @"name":name};
                        
                        [actualContacts addObject:contactDict];
                    }
                }
            }
            if(allPeople)
                CFRelease(allPeople);
            
            allPeople = nil;
            if (actualContacts.count) {
                [self sendContacts:actualContacts];
            }
            
        }
    }
}

- (NSString *)jsonStringForObject:(id)object {
    NSError *err;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:0 error:&err];
    NSString* result = nil;
    if (err == nil)
        result = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return result;
}

-(void)sendContacts:(NSArray *)contactList{
    [[ApiManager sharedManager] postPhonesRequest:@"users/phones" params:contactList callback:^(id response, ErrorObj *error) {
        if (response) {
            NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
            [[SettingsManager instance] saveObject:@(currentTime) key:kContactSyncDate];
        }
        
    }];
}




@end
