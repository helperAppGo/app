//// Header

#import "ChatDialogListViewController.h"
#import "DialogListTableViewCell.h"
#import "LoaderCell.h"
#import "MessageViewController.h"
#import "FDTakeController.h"

@interface ChatDialogListViewController (){
    NSMutableArray *_list;
    AVAudioPlayer *_audioPlayer;
    BOOL _isLastLoader;
    UIRefreshControl *_refreshControl;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) FDTakeController *takeController;

@end

@implementation ChatDialogListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _list = @[].mutableCopy;
    _titleLabel.text = trChatTitle;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UINib *nib = [UINib nibWithNibName:@"DialogListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"loader"];
    _refreshControl =  [[UIRefreshControl alloc] init];
    [self.tableView  addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(getNow) forControlEvents:UIControlEventValueChanged];
    
    [self getDialogList];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getNow{
    [_refreshControl beginRefreshing];
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(0)};
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"dialogs/last" params:@{@"data":[dict convertToJsonString]} callback:^(id response, ErrorObj *error) {
        _isLastLoader = YES;
        [_refreshControl endRefreshing];
        [_list removeAllObjects];
        if (response) {
            NSDictionary *json = response[@"response"];
            NSMutableArray *messages = [NSMutableArray arrayWithArray:json[@"messages"]];
            NSArray *userList = json[@"users"];
            NSMutableArray *dialogList = @[].mutableCopy;
            CDUser *currentUser = [[SettingsManager instance] currentUser];
            for (NSDictionary *userDict in userList) {
                if ([userDict[@"id"] integerValue] == currentUser.objId) {
                    continue;
                }
                UserObj *user = [UserObj createUser:userDict];
                MessageModel *message = nil;
                
                for (NSDictionary *messageDict in messages) {
                    NSInteger fromId = [messageDict[@"id_from"] integerValue];
                    NSInteger toId = [messageDict[@"id_to"] integerValue];
                    if (fromId == user.objId || toId == user.objId) {
                        message = [[MessageModel alloc] initWithDict:messageDict];
                        break;
                    }
                }
                DialogModel *dialog = [[DialogModel alloc] init];
                dialog.messageObj = message;
                dialog.user = user;
                [dialogList addObject:dialog];
            }
            [_list addObjectsFromArray:dialogList];
            [_list sortUsingComparator:^NSComparisonResult(DialogModel *obj1, DialogModel *obj2) {
                return ([obj2.messageObj.date compare:obj1.messageObj.date]);
            }];
            [weakSelf.tableView reloadData];
        }
    }];
}
-(void)receiveMessage:(MessageModel *)model{
    for (DialogModel *dialog in _list) {
        if (dialog.messageObj.fromId == model.fromId) {
            NSInteger count = dialog.messageObj.unReadCount;
            count ++;
            model.unReadCount = count;
            dialog.messageObj = model;
            [self.tableView reloadData];
            break;
        }
    }
    [self soundForReceive];
}

-(void)soundForReceive{
    NSString *path = [NSString stringWithFormat:@"%@/r_message.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    [_audioPlayer play];
}

-(void)getDialogList{
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(_list.count)};
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"dialogs/last" params:@{@"data":[dict convertToJsonString]} callback:^(id response, ErrorObj *error) {
        _isLastLoader = YES;
        if (response) {
            NSDictionary *json = response[@"response"];
            NSMutableArray *messages = [NSMutableArray arrayWithArray:json[@"messages"]];
            NSArray *userList = json[@"users"];
            NSMutableArray *dialogList = @[].mutableCopy;
            CDUser *currentUser = [[SettingsManager instance] currentUser];
            for (NSDictionary *userDict in userList) {
                if ([userDict[@"id"] integerValue] == currentUser.objId) {
                    continue;
                }
                UserObj *user = [UserObj createUser:userDict];
                MessageModel *message = nil;
               
                for (NSDictionary *messageDict in messages) {
                    NSInteger fromId = [messageDict[@"id_from"] integerValue];
                    NSInteger toId = [messageDict[@"id_to"] integerValue];
                    if (fromId == user.objId || toId == user.objId) {
                        message = [[MessageModel alloc] initWithDict:messageDict];
                        break;
                    }
                }
                DialogModel *dialog = [[DialogModel alloc] init];
                dialog.messageObj = message;
                dialog.user = user;
                [dialogList addObject:dialog];
            }
            [_list addObjectsFromArray:dialogList];
            [_list sortUsingComparator:^NSComparisonResult(DialogModel *obj1, DialogModel *obj2) {
                return ([obj2.messageObj.date compare:obj1.messageObj.date]);
            }];
            [weakSelf.tableView reloadData];
        }
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_list sortUsingComparator:^NSComparisonResult(DialogModel *obj1, DialogModel *obj2) {
        return ([obj2.messageObj.date compare:obj1.messageObj.date]);
    }];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"addIcon"] style:UIBarButtonItemStyleDone target:self action:@selector(addFriend)];
    item.tintColor = appBlueColor;
    self.navigationItem.rightBarButtonItem = item;
    [self.tableView reloadData];
    if (_isLastLoader) {
        [self getNow];
    }
}

-(void)addFriend{
    BaseViewController *controller = [self createController:@"ChatFriendListViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)setableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    DialogListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    DialogModel *model = _list[indexPath.row];
    
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.nameLabel.text = [model.user nameString];
    cell.descLabel.text = model.messageObj.files ? @"Фото" : model.messageObj.text;
    cell.timeLabel.text = model.messageObj.dateString;
    cell.badgeLabel.text = [NSString stringWithFormat:@"%@",@(model.messageObj.unReadCount)];
    cell.badgeLabel.superview.hidden = model.messageObj.unReadCount == 0;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageViewController *controller = (MessageViewController *)[self createController:@"MessageViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    controller.info = @{@"dialog":_list[indexPath.row]};
    
    [controller setChangeBlock:^(JSQMessage *message) {
        DialogModel *model = _list[indexPath.row];
        model.messageObj.text = message.text;
        model.messageObj.date = message.date;
        model.messageObj.unReadCount = 0;
//        model.messageObj = message.messageModel;
    }];
    [controller setReadBlock:^(DialogModel *model) {
        DialogModel *model2 = _list[indexPath.row];
        model2.messageObj.unReadCount = 0;
        model2.messageObj.isRead = 1;
        
    }];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
