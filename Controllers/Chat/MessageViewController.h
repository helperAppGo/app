//// Header

#import "JSQMessages.h"

#import "DemoModelData.h"
#import "NSUserDefaults+DemoSettings.h"


@class MessageViewController;

@protocol JSQDemoViewControllerDelegate <NSObject>

- (void)didDismissJSQDemoViewController:(MessageViewController *)vc;

@end




@interface MessageViewController : JSQMessagesViewController <UIActionSheetDelegate, JSQMessagesComposerTextViewPasteDelegate>




@property (weak, nonatomic) id<JSQDemoViewControllerDelegate> delegateModal;
@property (strong, nonatomic) DemoModelData *demoData;

- (void)receiveMessagePressed:(UIBarButtonItem *)sender;
- (void)closePressed:(UIBarButtonItem *)sender;
-(void)readMessage:(MessageModel *)model;
@property (copy, nonatomic) void(^changeBlock)(JSQMessage *message);
@property (copy, nonatomic) void(^readBlock)(DialogModel *model);

-(NSInteger)chatUserId;
-(void)receiveMessage:(MessageModel *)model;

@end
