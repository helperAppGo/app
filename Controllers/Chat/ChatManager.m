//// Header

#import "ChatManager.h"

@implementation ChatManager
+ (ChatManager *)instance {
    static ChatManager *_instance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[self alloc] init];
    });
    
    return _instance;
}



@end
