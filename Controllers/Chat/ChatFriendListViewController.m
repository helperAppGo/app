//// Header

#import "ChatFriendListViewController.h"
#import "FriendListTableViewCell.h"
#import "LoaderCell.h"

@interface ChatFriendListViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *_list;
    UIRefreshControl *_refreshControl;
    BOOL _complete;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ChatFriendListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleLabel.text = trChatFriendTitle;
    _list = @[].mutableCopy;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UINib *nib = [UINib nibWithNibName:@"FriendListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"loader"];
    
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(getListNow) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView addSubview:_refreshControl];
    
    [self getListNow];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)getList{
    if (_complete) {
        return;
    }
    NSDictionary *params = @{@"limit":@(25),
                             @"offset":@(_list.count)};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"users/friends/mixed" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            response = response[@"response"];
            for (NSDictionary *dict in response) {
                UserObj *user = [UserObj createUser:dict];
                [_list addObject:user];
            }
            _complete = [response count] < 25;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
    
}
-(void)getListNow{
    NSDictionary *params = @{@"limit":@(25),
                             @"offset":@(0)};
    __weak typeof(self) weakSelf = self;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:@"users/friends/mixed" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            response = response[@"response"];
            [_list removeAllObjects];
            for (NSDictionary *dict in response) {
                UserObj *user = [UserObj createUser:dict];
                [_list addObject:user];
            }
            _complete = [response count] < 25;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return 1;
    }
    return _list.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        } else{
            [cell.loader startAnimating];
            cell.loader.hidden = NO;
        }
        return cell;
    }
    FriendListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserObj *user = _list[indexPath.row];
    cell.nameLabel.text = user.nameString;
    [cell.iconView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserObj *obj =_list[indexPath.row];
    DialogModel *model = [[DialogModel alloc] init];
    model.user = obj;
    NSArray *items = self.navigationController.viewControllers;
    BaseViewController *controller = [self createController:@"MessageViewController"];
    controller.type = 1;
    controller.info = @{@"dialog":model};
    [self.navigationController setViewControllers:@[items[0],controller] animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == _list.count  - 4) {
            [self getList];
        }
        
    }
}

@end
