//// Header

#import <Foundation/Foundation.h>
#import "JSQPhotoMediaItem.h"

@interface ChatAsyncPhoto : JSQPhotoMediaItem

@property (nonatomic, strong) UIImageView *asyncImageView;

- (instancetype)initWithURL:(NSURL *)URL;

@end
