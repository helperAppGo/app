//// Header

#import "BaseViewController.h"

@interface ChatDialogListViewController : BaseViewController

-(void)receiveMessage:(MessageModel *)model;
-(void)getDialogList;

@end
