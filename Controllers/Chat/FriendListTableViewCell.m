//// Header

#import "FriendListTableViewCell.h"

@implementation FriendListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.iconView.layer.cornerRadius = 20;
    self.iconView.contentMode = UIViewContentModeScaleAspectFill;
    self.iconView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
