//// Header

#import <Foundation/Foundation.h>


@interface MessageModel : NSObject
@property (nonatomic, assign) NSInteger unReadCount;
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger fromId;
@property (nonatomic, assign) NSInteger files;
@property (nonatomic, assign) NSInteger toId;
@property (nonatomic, assign) NSInteger isRead;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) FeedMediaObject *media;

-(id)initWithDict:(NSDictionary *)dict;
  
@end


@interface DialogModel : NSObject
@property (nonatomic, strong) UserObj *user;
@property (nonatomic, strong) MessageModel *messageObj;
@end
