//// Header

#import "DialogListTableViewCell.h"

@implementation DialogListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = 25;
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.descLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.timeLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.timeLabel.textColor = COLOR(180, 180, 180);
    self.descLabel.textColor = COLOR(180, 180, 180);
    self.descLabel.numberOfLines = 2;
    
    self.badgeLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.badgeLabel.textColor = [UIColor whiteColor];
    
    self.badgeLabel.superview.backgroundColor = appBlueColor;
    self.badgeLabel.superview.layer.cornerRadius  = self.badgeLabel.superview.height/2;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
