//// Header
//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//
#import "ChatAsyncPhoto.h"
#import "FDTakeController.h"
#import <AVFoundation/AVFoundation.h>

#import "MessageViewController.h"
#import "JSQMessagesViewAccessoryButtonDelegate.h"
#import "GroupNavView.h"
#import "UserPhotoView.h"
#import "PhotoViewController.h"

@interface MessageViewController () <JSQMessagesViewAccessoryButtonDelegate, UICollectionViewDelegate, FDTakeDelegate, NYTPhotosViewControllerDelegate>{
    NSMutableArray *_list;
    DialogModel *_model;
    CDUser *_currentUser;
    NSAttributedString *_sentAtr;
    NSAttributedString *_deliverAtr;
    UIImage *_attachImage;
    GroupNavView *_navView;
}
@property (nonatomic) NYTPhotoViewerArrayDataSource *dataSource;
@property (nonatomic, strong) FDTakeController *takeController;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@end

@implementation MessageViewController

#pragma mark - View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"doubleTick"];
    NSTextAttachment *delAttach = [[NSTextAttachment alloc] init];
    delAttach.image = [UIImage imageNamed:@"deliverIcon"];
    NSTextAttachment *sentAttach = [[NSTextAttachment alloc] init];
    sentAttach.image = [UIImage imageNamed:@"sentMessageIcon"];
    _sentAtr = [NSAttributedString attributedStringWithAttachment:sentAttach];
    _deliverAtr = [NSAttributedString attributedStringWithAttachment:delAttach];
    
    
    _currentUser = [[SettingsManager instance] currentUser];
    _model = self.info[@"dialog"];
    [self changeNavigation];
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    /**
     *  Load up our fake data for the demo
     */
    self.demoData = [[DemoModelData alloc] init];
    /**
     *  Set up message accessory button delegate and configuration
     */
    self.collectionView.accessoryDelegate = self;
    
    /**
     *  You can set custom avatar sizes
     */
    if (![NSUserDefaults incomingAvatarSetting]) {
        self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    }
    
    if (![NSUserDefaults outgoingAvatarSetting]) {
        self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    }
    
    self.showLoadEarlierMessagesHeader = YES;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(receiveMessagePressed:)];
    
    /**
     *  Register custom menu actions for cells.
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    
    
    /**
     *  OPT-IN: allow cells to be deleted
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    
    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
    [self getMessages];
    
}

-(void)changeNavigation{
    _navView = [GroupNavView loadFromNib];
//    _navView.nameCenter.constant = 0;
    _navView.nameLabel.text = _model.user.nameString;
    _navView.descLabel.text = _model.user.statusText;
    _navView.descLabel.textColor = _model.user.statusColor;
    _navView.descLabel.font = [UIFont defaultFontWithSize:10 bold:NO];
    [_navView.avatar sd_setImageWithURL:[NSURL URLWithString:_model.user.avatar] placeholderImage:[UIImage imageNamed:@"user"]];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer  alloc] initWithTarget:self action:@selector(userClicked)];
    [_navView.heightAnchor constraintEqualToConstant:44].active  = YES;
    tapGesture.numberOfTapsRequired = 1;
    _navView.userInteractionEnabled = YES;
    self.navigationItem.titleView = _navView;
    [_navView addGestureRecognizer:tapGesture];
    
}

-(void)userClicked{
    [self openUserProfile:_model.user];
}

-(void)clearNotifications{
    NSArray *items = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *notif in items) {
        NSDictionary *dict = notif.userInfo;
        if([dict[@"fromId"] integerValue] == _model.user.objId){
            [[UIApplication sharedApplication] cancelLocalNotification:notif];
        }
    }
}


-(void)getMessages{
    NSDictionary *params = @{@"limit":@(25),
                             @"expand":@"files",
                             @"offset":@(self.demoData.messages.count),
                             @"id_user":@(_model.user.objId)
                             };
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"dialogs/messages/last" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            [weakSelf clearNotifications];
            response = response[@"response"];
            NSMutableArray *result = @[].mutableCopy;
            for (NSDictionary *dict in response) {
                MessageModel *obj = [[MessageModel alloc] initWithDict:dict];
                NSString *senderName = obj.fromId == _currentUser.objId ? _currentUser.nameString : _model.user.nameString;
                JSQMessage *message = [JSQMessage messageWithServerMessage:obj senderName: senderName];
                message.messageModel = obj;
                [result addObject:message];
            }
            [result sortUsingComparator:^NSComparisonResult(JSQMessage *obj1, JSQMessage *obj2) {
                return [obj2.date compare:obj1.date];
            }];
            for (JSQMessage *message in result) {
                [weakSelf.demoData.messages insertObject:message atIndex:0];
            }
            [weakSelf.collectionView reloadData];
            if (weakSelf.demoData.messages.count <= 25) {
                [weakSelf finishSendingMessageAnimated:YES];
            }
            [weakSelf readMessage];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)readMessage{
    NSInteger senderId = _model.messageObj.fromId;
    if (senderId == _currentUser.objId) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] putRequest:@"dialogs/messages/read" params:@{@"id":@(_model.messageObj.objId)} callback:^(id response, ErrorObj *error) {
        if (response) {
            _model.messageObj.isRead = 0;
            _model.messageObj.unReadCount = 0;
            if (weakSelf.readBlock) {
                weakSelf.readBlock(_model);
            }
        }
    }];
}

-(void)uploadPhoto:(UIImage *)image{
    NSData *data = UIImagePNGRepresentation(image);
    _attachImage = image;
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(image, 1/k);
    }
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] uploadPhoto:data fileName:@"media.png" callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            NSInteger fileId = [response[@"id"] integerValue];
            NSString *text = self.inputToolbar.contentView.textView.text;
            [weakSelf sendMessage:text ? text : @"" fileId:fileId];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
        
    }];
}

-(void)sendPhoto{
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    self.takeController.delegate = self;
    [self.takeController takePhotoOrChooseFromLibrary];
                           
//    ChatAsyncPhoto *photoItem = [[ChatAsyncPhoto alloc] initWithURL:[NSURL URLWithString:_currentUser.avatar]];
//    //[NSString stringWithFormat:@"%@",@(_model.user.objId)]
//    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
//                                             senderDisplayName:_currentUser.nameString
//                                                          date:[NSDate date]
//                                                         media:photoItem];
//    [self.demoData.messages addObject:message];
//    [self finishSendingMessageAnimated:YES];
}

-(void)sendMessage:(NSString *)text fileId:(NSInteger) fileId{
    
    NSMutableDictionary *params = @{@"id_user":@(_model.user.objId),
                                    @"text":text}.mutableCopy;;
    
    MessageModel *newMessage = [[MessageModel alloc] init];
    newMessage.objId = -1;
    newMessage.text = text;
    newMessage.fromId = _currentUser.objId;
    newMessage.toId = _model.user.objId;
    newMessage.name = _currentUser.nameString;
    newMessage.date = [NSDate date];
    
    JSQMessage *message;
    if (fileId) {
        params[@"files"] = [NSString stringWithFormat:@"%@",@(fileId)];// @(fileId);
        NSString *senderId = [NSString stringWithFormat:@"%@",@(_currentUser.objId)];
        
//        ChatAsyncPhoto *photoItem = [[ChatAsyncPhoto alloc] initWithImage:_attachImage];
//        message =  [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:_currentUser.nameString date:newMessage.date media:photoItem];
        
        {
            JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:_attachImage];
            message = [JSQMessage messageWithSenderId:senderId
                                                           displayName:_currentUser.nameString
                                                                 media:photoItem];
        }
        
    } else{
        message = [JSQMessage messageWithServerMessage:newMessage senderName: [_currentUser nameString]];
    }
    message.messageModel = newMessage;
    [self.demoData.messages addObject:message];
    [self finishSendingMessage];
    [self soundForSend];
//    [self.demoData.messages addObject:message];
//    [self finishSendingMessageAnimated:YES];
    
    __weak typeof(self) weakSelf = self;
    __block JSQMessage *m = message;
    __block NSInteger lasId = self.demoData.messages.count - 1;
    
    [[ApiManager sharedManager] postRequest:@"dialogs/messages" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            MessageModel *messageModel = [[MessageModel alloc] initWithDict:response[@"response"]];
            NSIndexPath *path = [NSIndexPath indexPathForItem:lasId inSection:0];
            m.messageModel = messageModel;
            [weakSelf.collectionView reloadItemsAtIndexPaths:@[path]];
        }
    }];
}

-(void)soundForSend{
    NSString *path = [NSString stringWithFormat:@"%@/s_message.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    _audioPlayer.volume = 0.25;
    [_audioPlayer play];
}

-(void)soundForReceive{
    NSString *path = [NSString stringWithFormat:@"%@/r_message.mp3", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    // Create audio player object and initialize with URL to sound
    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    _audioPlayer.volume = 0.25;
    [_audioPlayer play];
}

-(void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info{
    [self uploadPhoto:photo];
}

#pragma mark - CHAT

-(NSInteger)chatUserId{
    return _model.user.objId;
}

-(void)receiveMessage:(MessageModel *)model{
    JSQMessage *message = [JSQMessage messageWithServerMessage:model senderName: model.name];
    message.messageModel = model;
    [self.demoData.messages addObject:message];
    [self finishReceivingMessageAnimated:YES];
    [self soundForReceive];
}

-(void)readMessage:(MessageModel *)model{
    NSInteger messageId = model.objId;
    for (JSQMessage *jsq in self.demoData.messages) {
        NSInteger objId = jsq.messageModel.objId;
        if (objId == messageId) {
            jsq.messageModel.isRead = 1;
            [self finishReceivingMessageAnimated:YES];
            break;
        }
    }
//    JSQMessage *message = [JSQMessage messageWithServerMessage:model senderName: model.name];
//    message.messageModel = model;
//    [self.demoData.messages addObject:message];
//    [self finishReceivingMessageAnimated:YES];
//    [self soundForReceive];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.delegateModal) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop
                                                                                              target:self
                                                                                              action:@selector(closePressed:)];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
    self.collectionView.collectionViewLayout.springinessEnabled = [NSUserDefaults springinessSetting];
}



#pragma mark - Custom menu actions for cells

- (void)didReceiveMenuWillShowNotification:(NSNotification *)notification
{
    /**
     *  Display custom menu actions for cells.
     */
    UIMenuController *menu = [notification object];
    menu.menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action" action:@selector(customAction:)] ];
    
    [super didReceiveMenuWillShowNotification:notification];
}



#pragma mark - Testing

- (void)pushMainViewController
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nc = [sb instantiateInitialViewController];
    [self.navigationController pushViewController:nc.topViewController animated:YES];
}


#pragma mark - Actions

- (void)receiveMessagePressed:(UIBarButtonItem *)sender
{
    /**
     *  DEMO ONLY
     *
     *  The following is simply to simulate received messages for the demo.
     *  Do not actually do this.
     */
    
    
    /**
     *  Show the typing indicator to be shown
     */
    self.showTypingIndicator = !self.showTypingIndicator;
    
    /**
     *  Scroll to actually view the indicator
     */
    [self scrollToBottomAnimated:YES];
    
    /**
     *  Copy last sent message, this will be the new "received" message
     */
    JSQMessage *copyMessage = [[self.demoData.messages lastObject] copy];
    
    if (!copyMessage) {
        copyMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdJobs
                                          displayName:kJSQDemoAvatarDisplayNameJobs
                                                 text:@"First received!"];
    }
    
    /**
     *  Allow typing indicator to show
     */
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        NSMutableArray *userIds = [[self.demoData.users allKeys] mutableCopy];
        [userIds removeObject:self.senderId];
        NSString *randomUserId = userIds[arc4random_uniform((int)[userIds count])];
        
        JSQMessage *newMessage = nil;
        id<JSQMessageMediaData> newMediaData = nil;
        id newMediaAttachmentCopy = nil;
        
        if (copyMessage.isMediaMessage) {
            /**
             *  Last message was a media message
             */
            id<JSQMessageMediaData> copyMediaData = copyMessage.media;
            
            if ([copyMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                JSQPhotoMediaItem *photoItemCopy = [((JSQPhotoMediaItem *)copyMediaData) copy];
                photoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [UIImage imageWithCGImage:photoItemCopy.image.CGImage];
                
                /**
                 *  Set image to nil to simulate "downloading" the image
                 *  and show the placeholder view
                 */
                photoItemCopy.image = nil;
                
                newMediaData = photoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                JSQLocationMediaItem *locationItemCopy = [((JSQLocationMediaItem *)copyMediaData) copy];
                locationItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [locationItemCopy.location copy];
                
                /**
                 *  Set location to nil to simulate "downloading" the location data
                 */
                locationItemCopy.location = nil;
                
                newMediaData = locationItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                JSQVideoMediaItem *videoItemCopy = [((JSQVideoMediaItem *)copyMediaData) copy];
                videoItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [videoItemCopy.fileURL copy];
                
                /**
                 *  Reset video item to simulate "downloading" the video
                 */
                videoItemCopy.fileURL = nil;
                videoItemCopy.isReadyToPlay = NO;
                
                newMediaData = videoItemCopy;
            }
            else if ([copyMediaData isKindOfClass:[JSQAudioMediaItem class]]) {
                JSQAudioMediaItem *audioItemCopy = [((JSQAudioMediaItem *)copyMediaData) copy];
                audioItemCopy.appliesMediaViewMaskAsOutgoing = NO;
                newMediaAttachmentCopy = [audioItemCopy.audioData copy];
                
                /**
                 *  Reset audio item to simulate "downloading" the audio
                 */
                audioItemCopy.audioData = nil;
                
                newMediaData = audioItemCopy;
            }
            else {
                NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
            }
            
            newMessage = [JSQMessage messageWithSenderId:randomUserId
                                             displayName:self.demoData.users[randomUserId]
                                                   media:newMediaData];
        }
        else {
            /**
             *  Last message was a text message
             */
            newMessage = [JSQMessage messageWithSenderId:randomUserId
                                             displayName:self.demoData.users[randomUserId]
                                                    text:copyMessage.text];
        }
        
        /**
         *  Upon receiving a message, you should:
         *
         *  1. Play sound (optional)
         *  2. Add new id<JSQMessageData> object to your data source
         *  3. Call `finishReceivingMessage`
         */
        
        // [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
        
//        [self.demoData.messages addObject:newMessage];
        
        [self finishReceivingMessageAnimated:YES];
        
        
        if (newMessage.isMediaMessage) {
            /**
             *  Simulate "downloading" media
             */
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                /**
                 *  Media is "finished downloading", re-display visible cells
                 *
                 *  If media cell is not visible, the next time it is dequeued the view controller will display its new attachment data
                 *
                 *  Reload the specific item, or simply call `reloadData`
                 */
                
                if ([newMediaData isKindOfClass:[JSQPhotoMediaItem class]]) {
                    ((JSQPhotoMediaItem *)newMediaData).image = newMediaAttachmentCopy;
                    [self.collectionView reloadData];
                }
                else if ([newMediaData isKindOfClass:[JSQLocationMediaItem class]]) {
                    [((JSQLocationMediaItem *)newMediaData)setLocation:newMediaAttachmentCopy withCompletionHandler:^{
                        [self.collectionView reloadData];
                    }];
                }
                else if ([newMediaData isKindOfClass:[JSQVideoMediaItem class]]) {
                    ((JSQVideoMediaItem *)newMediaData).fileURL = newMediaAttachmentCopy;
                    ((JSQVideoMediaItem *)newMediaData).isReadyToPlay = YES;
                    [self.collectionView reloadData];
                }
                else if ([newMediaData isKindOfClass:[JSQAudioMediaItem class]]) {
                    ((JSQAudioMediaItem *)newMediaData).audioData = newMediaAttachmentCopy;
                    [self.collectionView reloadData];
                }
                else {
                    NSLog(@"%s error: unrecognized media item", __PRETTY_FUNCTION__);
                }
                
            });
        }
        
    });
}

- (void)closePressed:(UIBarButtonItem *)sender
{
    [self.delegateModal didDismissJSQDemoViewController:self];
}




#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date {
    /**
     *  Sending a message. Your implementation of this method should do *at least* the following:
     *
     *  1. Play sound (optional)
     *  2. Add new id<JSQMessageData> object to your data source
     *  3. Call `finishSendingMessage`
     */
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    [self sendMessage:text fileId:0];
}



- (void)didPressAccessoryButton:(UIButton *)sender{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:trChatMediaTitle message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:trChatMediaPhoto style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self sendPhoto];
    }];
    [alertController addAction:photoAction];

    UINavigationController *navController = (UINavigationController *)[[[[UIApplication sharedApplication] delegate] window] rootViewController];
    [navController presentViewController:alertController animated:YES completion:nil];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        [self.inputToolbar.contentView.textView becomeFirstResponder];
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self.demoData addPhotoMediaMessage];
            break;
            
        case 1:
        {
            __weak UICollectionView *weakView = self.collectionView;
            
            [self.demoData addLocationMediaMessageCompletion:^{
                [weakView reloadData];
            }];
        }
            break;
            
        case 2:
            [self.demoData addVideoMediaMessage];
            break;
            
        case 3:
            [self.demoData addVideoMediaMessageWithThumbnail];
            break;
            
        case 4:
            [self.demoData addAudioMediaMessage];
            break;
    }
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    [self finishSendingMessageAnimated:YES];
}

-(void)back{
    if (self.changeBlock) {
        if (self.demoData.messages.count) {
            self.changeBlock([self.demoData.messages lastObject]);
        }
    }
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - JSQMessages CollectionView DataSource

- (NSString *)senderId {
    return [NSString stringWithFormat:@"%@",@(_currentUser.objId)];
}

- (NSString *)senderDisplayName {
    return kJSQDemoAvatarDisplayNameSquires;
}

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.demoData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if (message.senderId.integerValue == _currentUser.objId) {
        return self.demoData.outgoingBubbleImageData;
    }
    
    return self.demoData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if (message.senderId.integerValue == _currentUser.objId) {
        if (![NSUserDefaults outgoingAvatarSetting]) {
            return nil;
        }
    }
    else {
        if (![NSUserDefaults incomingAvatarSetting]) {
            return nil;
        }
    }
    
    
    return [self.demoData.avatars objectForKey:message.senderId];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if (message.senderId.integerValue == _currentUser.objId) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 15;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([self.senderId isEqualToString:currentMessage.senderId]) {
        //status could be: 'sent', 'sending', etc
        if (currentMessage.messageModel.isRead) {
            return _deliverAtr;
//           return [[NSAttributedString alloc] initWithString:@"deliver"];
        } else{
            if (currentMessage.messageModel.objId < 0) {
                NSLog(@"%@",currentMessage);
            }
            return currentMessage.messageModel.objId < 0 ? _sentAtr : _sentAtr;
//            NSString *str = currentMessage.messageModel.objId < 0 ? @"sending" : @"sent";
//            return [[NSAttributedString alloc] initWithString:str];
        }
    } else{
        NSString *str = currentMessage.messageModel.dateString;
        return [[NSAttributedString alloc] initWithString:str ? str : @""];
    }
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if (msg.senderId.integerValue != _currentUser.objId) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    cell.accessoryButton.hidden = ![self shouldShowAccessoryButtonForMessage:msg];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    if(msg.senderId.integerValue == _currentUser.objId){
        return;
    }
    
    if (msg.messageModel.isRead == 0) {
        __block JSQMessage *message = msg;
        NSDictionary *params = @{@"id":@(msg.messageModel.objId)};
        [[ApiManager sharedManager] putRequest:@"dialogs/messages/read" params:params callback:^(id response, ErrorObj *error) {
            if (response) {
                message.messageModel.isRead = 1;
            }
        }];
    }
}


- (BOOL)shouldShowAccessoryButtonForMessage:(id<JSQMessageData>)message
{
    return ([message isMediaMessage] && [NSUserDefaults accessoryButtonForMediaMessages]);
}


#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Custom Action", nil)
                                message:nil
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}



#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}


#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    [self getMessages];
     
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");
    JSQMessage *message = self.demoData.messages[indexPath.row];
    if (message.media) {
        [self showMessageAttachments:message];
    }
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}



#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
//        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
//        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
//                                                 senderDisplayName:self.senderDisplayName
//                                                              date:[NSDate date]
//                                                             media:item];
//        [self.demoData.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}

#pragma mark - JSQMessagesViewAccessoryDelegate methods

- (void)messageView:(JSQMessagesCollectionView *)view didTapAccessoryButtonAtIndexPath:(NSIndexPath *)path
{
    NSLog(@"Tapped accessory button!");
}


#pragma mark - PhotosView
-(void)showMessageAttachments:(JSQMessage *)message{
    NSMutableArray *items = @[].mutableCopy;
    NSMutableArray *urlItems = @[].mutableCopy;
    NSString  *current = message.messageModel.media.imgUrl ? message.messageModel.media.imgUrl : @"";
    
    for (JSQMessage *message in self.demoData.messages) {
        if (message.isMediaMessage) {
            ChatAsyncPhoto *photoObj =(ChatAsyncPhoto *)message.media;
            UserPhotoView *view = [[UserPhotoView alloc] init];
            NSString  *urlString = message.messageModel.media.imgUrl ? message.messageModel.media.imgUrl : @"";
            view.urlString = urlString;
            if (urlString.length) {
                [view sd_setImageWithURL:[NSURL URLWithString:view.urlString]];
            } else{
                view.image = photoObj.image;
            }
            [items addObject:view];
            [urlItems addObject:urlString];
        }
    }
    NSInteger index = [urlItems indexOfObject:current];
    
    self.dataSource = [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:items];
    PhotoViewController *photosViewController = [[PhotoViewController alloc] initWithDataSource:self.dataSource initialPhoto:self.dataSource[index] delegate:self];
    photosViewController.view.tintColor = [UIColor whiteColor];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController referenceViewForPhoto:(id <NYTPhoto>)photo {
    return nil;
    //    NSInteger index = [self.dataSource indexOfPhoto:photo];
    //    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    //    UICollectionViewCell *cell = [_photoCollectionView cellForItemAtIndexPath:path];
    //    return cell.contentView;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController loadingViewForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController captionViewForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (CGFloat)photosViewController:(NYTPhotosViewController *)photosViewController maximumZoomScaleForPhoto:(id <NYTPhoto>)photo {
    return 0.5f;
}

- (NSDictionary *)photosViewController:(NYTPhotosViewController *)photosViewController overlayTitleTextAttributesForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (NSString *)photosViewController:(NYTPhotosViewController *)photosViewController titleForPhoto:(id<NYTPhoto>)photo atIndex:(NSInteger)photoIndex totalPhotoCount:(nullable NSNumber *)totalPhotoCount {
    return [NSString stringWithFormat:@"%lu/%lu", (unsigned long)photoIndex+1, (unsigned long)totalPhotoCount.integerValue];
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController didNavigateToPhoto:(id <NYTPhoto>)photo atIndex:(NSUInteger)photoIndex {
    NSLog(@"Did Navigate To Photo: %@ identifier: %lu", photo, (unsigned long)photoIndex);
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController actionCompletedWithActivityType:(NSString *)activityType {
    NSLog(@"Action Completed With Activity Type: %@", activityType);
}

- (void)photosViewControllerDidDismiss:(NYTPhotosViewController *)photosViewController {
    NSLog(@"Did Dismiss Photo Viewer: %@", photosViewController);
}


@end

