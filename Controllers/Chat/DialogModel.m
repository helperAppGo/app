//// Header

#import "DialogModel.h"


@implementation MessageModel
-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.unReadCount = [NULL_TO_NIL(dict[@"count_unread"]) integerValue];
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.fromId = [NULL_TO_NIL(dict[@"id_from"]) integerValue];
        self.toId = [NULL_TO_NIL(dict[@"id_to"]) integerValue];
        self.isRead = [NULL_TO_NIL(dict[@"is_read"]) integerValue];
        self.name = NULL_TO_NIL(dict[@"name"]);
        self.text = NULL_TO_NIL(dict[@"text"]);
        
        NSString *dateString = NULL_TO_NIL(dict[@"time"]);
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = [dateString rangeOfString:@"."].location != NSNotFound ? @"yyyy-MM-dd'T'HH:mm:ss.SSSZ" : @"yyyy-MM-dd'T'HH:mm:ssZ";
        self.date = [formatter dateFromString:dateString];
        formatter.dateFormat = @"dd-MM-yyyy HH:mm";
        self.dateString = [formatter stringFromDate:self.date];
        NSArray *items = NULL_TO_NIL(dict[@"file_objects"]);
        if (items.count) {
            NSDictionary *dict = items[0];
            FeedMediaObject *obj = [[FeedMediaObject alloc] initWithDict:dict];
            self.media = obj;
        }
        self.files = [NULL_TO_NIL(dict[@"files"]) integerValue];
    }
    return self;
}
-(NSString *)convertDate{
    
    double now = [[NSDate date] timeIntervalSince1970];
    double seconds = [self.date timeIntervalSince1970];
    double result = now - seconds;
    if (result < 60) {
        return @"1 минут назад";
    }
    NSInteger min = result/60;
    if (result < 50 ) {
        return [NSString stringWithFormat:@"%@ назад",@(min)];
    }
    NSInteger hours = min/60;
    if (hours == 0) {
        hours = 1;
    }
    if (hours < 2) {
        return [NSString stringWithFormat:@"%@ час назад",@(hours)];
    }
    
//    время
//    10 минут
//    30 минут
//    час назад
//    сегодня
//    вчера
    return @"";
}
@end


@implementation DialogModel

@end
