//// Header

#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface BaseViewController (){
    AVPlayer *_player;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont defaultFontWithSize:15 bold:NO];
    _titleLabel.textColor = COLOR(100, 100, 100);
    self.navigationItem.titleView = _titleLabel;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleDefault;
}

-(void)soundForLike{
    NSString *path = [NSString stringWithFormat:@"%@/likeSound.wav", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    // Create audio player object and initialize with URL to sound
    //    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:nil];
    _player.volume = 0.5;
    [_player play];
}

-(void)addBadge:(UIBarButtonItem *)item count:(NSInteger) count{
    [item pp_setBadgeLabelAttributes:^(PPBadgeLabel *badgeLabel) {
        badgeLabel.backgroundColor = [UIColor redColor];
    }];
    
    // 1.2 右边
    [item pp_changeBadge:count];
    [item pp_moveBadgeWithX:-10 Y:0];
    [item pp_setBadgeFlexMode:PPBadgeViewFlexModeHead];
}



-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.type == 1) {
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
        btn.backgroundColor = [UIColor clearColor];
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 23)];
        iconView.image = [[UIImage imageNamed:@"backIcon"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        iconView.tintColor = appBlueColor;
        iconView.center = CGPointMake(8, btn.height/2);
        [btn addSubview:iconView];
        [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        negativeSpacer.width = -20;
        self.navigationItem.leftBarButtonItems = @[item];
//        [StyleManager styleBackItem:self selector:@selector(back)];
        self.navigationItem.rightBarButtonItem = nil;
        
    }
}

-(void)reloadTableView{
    
}
-(void)back{
    [[SettingsManager instance] anySound];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showErrorMessage:(NSString *)message{
    [SVProgressHUD showErrorWithStatus:message];
}

-(void)showLoader{
    [SVProgressHUD show];
}

-(void)hideLoader{
    [SVProgressHUD dismiss];
}

-(BaseViewController *)createController:(NSString *)controllerName{
    if([[NSBundle mainBundle] pathForResource:controllerName ofType:@"nib"] != nil)
    {
        BaseViewController *controller = [(BaseViewController*)[NSClassFromString(controllerName) alloc] initWithNibName:controllerName bundle:nil];
        return controller;
    }
    
    return nil;
}

- (UIView*)findFirstResponderBeneathView:(UIView*)view {
    for ( UIView *childView in view.subviews ) {
        if ( [childView respondsToSelector:@selector(isFirstResponder)] && [childView isFirstResponder] ) return childView;
        UIView *result = [self findFirstResponderBeneathView:childView];
        if ( result ) return result;
    }
    return nil;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self findFirstResponderBeneathView:self.view] resignFirstResponder];
    [super touchesEnded:touches withEvent:event];
}


#pragma mark - Relation methods
-(void)getUserProfile:(NSInteger)userId{
    [self showLoader];
    NSDictionary *params = @{@"data":@(userId)};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            UserObj *user = [UserObj createUser:response];
            [weakSelf openUserProfile:user];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)openUserProfile:(UserObj*)user{
    BaseViewController *controller = [self createController:@"ProfileViewController"];
    controller.info = @{@"user":user};
    controller.type = 1;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)addRecomendationFriend:(UserObj *)user tableView:(UITableView *)tableView{
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    NSDictionary  *params = @{@"id_user":@(user.objId)};
    [[ApiManager sharedManager] postRequest:@"users/friends" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            user.relation = 2;
            [tableView  reloadData];
            //                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)changeRelation:(UserObj *)user tableView:(UITableView*)tableView index:(NSIndexPath *)indexPath{
    NSDictionary *params = @{@"data":[@{@"id_user":@(user.objId)} convertToJsonString]};
    NSString *path = @"users/friends";
   
    if (user.relation == 0) {
        [self addRecomendationFriend:user tableView:tableView];
    }
    if (user.relation == 1) {
        __weak typeof(self) weakSelf = self;
        [self showAlert:@"Удалить из друзей ?" callback:^(id responseAlert, ErrorObj *errorAler) {
            if (responseAlert) {
                [weakSelf showLoader];
                [[ApiManager sharedManager] deleteRequest:path params:params callback:^(id response, ErrorObj *error) {
                    [weakSelf hideLoader];
                    if (response) {
                        user.relation = 4;
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    } else{
                        [weakSelf showErrorMessage:error.message];
                    }
                }];
            }
        }];
        
        
    }
    if (user.relation == 3) {
        __weak typeof(self) weakSelf = self;
        [self showAlert:@"Отменить заявку?" callback:^(id responseAlert, ErrorObj *errorAlert) {
            if (responseAlert) {
                [weakSelf showLoader];
                [[ApiManager sharedManager] deleteRequest:path params:params callback:^(id response, ErrorObj *error) {
                    [weakSelf hideLoader];
                    if (response) {
                        user.relation = 0;
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    } else{
                        [weakSelf showErrorMessage:error.message];
                    }
                }];
            }
        }];
    }
    
    if (user.relation == 2) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *btn1 = [UIAlertAction actionWithTitle:@"Принять заявку" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            __weak typeof(self) weakSelf = self;
            [self showLoader];
            [[ApiManager sharedManager] postRequest:path params:params callback:^(id response, ErrorObj *error) {
                [weakSelf hideLoader];
                if (response) {
                    user.relation = 1;
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                } else{
                    [weakSelf showErrorMessage:error.message];
                }
            }];
            
        }];
        [alert addAction:btn1];
        
        UIAlertAction *btn2 = [UIAlertAction actionWithTitle:@"Удалить заявку" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            __weak typeof(self) weakSelf = self;
            [self showLoader];
            [[ApiManager sharedManager] deleteRequest:path params:params callback:^(id response, ErrorObj *error) {
                [weakSelf hideLoader];
                if (response) {
                    user.relation = 4;
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                } else{
                    [weakSelf showErrorMessage:error.message];
                }
            }];
            
        }];
        [alert addAction:btn2];
        
        UIAlertAction *btn3 = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:btn3];
        
        
        //        alert.view.tintColor = COLOR(120, 120, 120);
        alert.modalPresentationStyle = UIModalPresentationPopover;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:YES completion:nil];
        });
        UIPopoverPresentationController *popController = [alert popoverPresentationController];
        popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
        UIView *view = self.navigationController.view;
        popController.sourceView = view;
        popController.sourceRect = CGRectMake(view.width/2, view.height, 0, 0);
    }
    
    if (user.relation == 4) {
        __weak typeof(self) weakSelf = self;
        [self showAlert:@"Хотите добавить в друзья ?" callback:^(id responseAlert, ErrorObj *errorAlert) {
            if (responseAlert) {
                [weakSelf showLoader];
                [[ApiManager sharedManager] postRequest:path params:params callback:^(id response, ErrorObj *error) {
                    [weakSelf hideLoader];
                    if (response) {
                        user.relation = 1;
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    } else{
                        [weakSelf showErrorMessage:error.message];
                    }
                }];
            }
        }];
        
    }
    if (user.relation == 5) {
        __weak typeof(self) weakSelf = self;
        [self showAlert:@"Хотите отписаться ?" callback:^(id responseAlert, ErrorObj *errorAlert) {
            if (responseAlert) {
                [weakSelf showLoader];
                [[ApiManager sharedManager] deleteRequest:path params:params callback:^(id response, ErrorObj *error) {
                    [weakSelf hideLoader];
                    if (response) {
                        user.relation = 0;
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    } else{
                        [weakSelf showErrorMessage:error.message];
                    }
                }];
            }
        }];
        
        
    }
}


-(void)showAlert:(NSString *)message callback:(Callback)callback{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *btn1 = [UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (callback) {
            callback(@{},nil);
        }
    }];
    [alert addAction:btn1];
    
    UIAlertAction *btn2 = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (callback) {
            callback(nil,nil);
        }
    }];
    [alert addAction:btn2];
    
    alert.view.tintColor = COLOR(120, 120, 120);
    alert.modalPresentationStyle = UIModalPresentationPopover;
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
    UIPopoverPresentationController *popController = [alert popoverPresentationController];
    popController.permittedArrowDirections = UIPopoverArrowDirectionAny;
    UIView *view = self.navigationController.view;
    popController.sourceView = view;
    popController.sourceRect = CGRectMake(view.width/2, view.height, 0, 0);
}




@end
