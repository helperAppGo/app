//// Header

#import "BaseViewController.h"

@interface CityListViewController : BaseViewController
@property (nonatomic, copy) void(^clickBlock)(NSInteger objId);
@end
