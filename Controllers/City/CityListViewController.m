//// Header

#import "CityListViewController.h"

@interface CityListViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *_list;
    NSInteger _selectId;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CityListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectId = [self.info[@"id"] integerValue];
    NSArray *items = [CDCity allWithOrder:@"name"];
    _list = @[].mutableCopy;
    [_list setArray:items];
    _titleLabel.text = trCityList;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [self changeRightItem];
}

-(void)changeRightItem{
    if (_selectId > 0) {
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(done)];
        self.navigationItem.rightBarButtonItem = item;
    } else{
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)done{
    if (self.clickBlock) {
        self.clickBlock(_selectId);
    }
    
    [self back];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"list"];
        cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    }
    CDCity *city = _list[indexPath.row];
    cell.textLabel.text = city.name;
    cell.accessoryType = (city.objId == _selectId) ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CDCity *city = _list[indexPath.row];
    if (city.objId == _selectId) {
        return;
    }
    _selectId = city.objId;
    [self changeRightItem];
    [self.tableView reloadData];
//    if (self.clickBlock) {
//        self.clickBlock(city.objId);
//    }
}


@end
