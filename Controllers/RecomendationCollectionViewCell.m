//// Header

#import "RecomendationCollectionViewCell.h"

@implementation RecomendationCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = 8;
    
    self.cityLabel.textColor = [UIColor grayColor];
    float w = [UIScreen mainScreen].bounds.size.width;
    if (w > 320) {
        self.cityLabel.font = [UIFont defaultFontWithSize:13 bold:YES];
        self.relationLabel.font = [UIFont defaultFontWithSize:13 bold:YES];
        self.nameLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    } else{
        self.cityLabel.font = [UIFont defaultFontWithSize:7 bold:YES];
        self.nameLabel.font = [UIFont defaultFontWithSize:8 bold:YES];
        self.relationLabel.font = [UIFont defaultFontWithSize:7 bold:YES];
    }
    
    self.relationLabel.textColor = appBlueColor;
    self.relationBtn.backgroundColor = COLOR(248, 248, 248);
    self.relationBtn.layer.borderColor = COLOR(235, 235, 235).CGColor;
    self.relationBtn.layer.borderWidth = 0.5;
    
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.containerView.backgroundColor = [UIColor whiteColor];
    self.containerView.layer.cornerRadius = 8;
    self.relationBtn.layer.cornerRadius = 4;
    self.deleteBtn.backgroundColor = [UIColor clearColor];
    [self.deleteBtn addTarget:self action:@selector(deleteClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.relationBtn addTarget:self action:@selector(relationClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.relationBtn.layer.cornerRadius = 5;
    self.relationBtn.layer.masksToBounds = false;
    self.relationBtn.layer.shadowOffset = CGSizeMake(0, 0);
    self.relationBtn.layer.shadowColor = COLOR(220, 220, 220).CGColor;
    self.relationBtn.layer.shadowOpacity = 0.8;
    self.relationBtn.layer.shadowRadius = 5;
}

-(void)deleteClicked{
    if (self.clickBlock) {
        self.clickBlock(2, self.indexPath.row);
    }
}

-(void)relationClicked{
    if (self.clickBlock) {
        self.clickBlock(1, self.indexPath.row);
    }
}

@end
