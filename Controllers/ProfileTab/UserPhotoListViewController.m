//// Header

#import "UserPhotoListViewController.h"
#import "AvatarCollectionViewCell.h"

@interface UserPhotoListViewController ()<UICollectionViewDelegate, UICollectionViewDataSource>{
    float _cellH;
    NSMutableArray *_photoItems;
}
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation UserPhotoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   _cellH =([[UIScreen mainScreen] bounds].size.width - 4 * 10)/3;
    
    UICollectionViewFlowLayout * _verticalLayout = [[UICollectionViewFlowLayout alloc] init];
    _verticalLayout.minimumInteritemSpacing = 10;
    _verticalLayout.minimumLineSpacing = 10;
    _verticalLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _verticalLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _verticalLayout.itemSize = CGSizeMake(_cellH, _cellH);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    
    [self.collectionView setCollectionViewLayout:_verticalLayout animated:YES];
    [self.collectionView reloadData];
    
    UINib *nib = [UINib nibWithNibName:@"AvatarCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"list"];
    
    
    
}

#pragma mark -  CollectionView delegate methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _photoItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AvatarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"list" forIndexPath:indexPath];
    cell.avatarView.backgroundColor = [UIColor clearColor];
    NSDictionary *dict = _photoItems[indexPath.row];
    NSURL *url = [NSURL URLWithString:dict[@"path_ava"]];
    [cell.avatarView sd_setImageWithURL:url placeholderImage:nil];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(_cellH, _cellH);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}



@end
