//// Header

#import "PhotoViewController.h"
#import <NYTPhotoViewer/NYTPhotosOverlayView.h>
#import <NYTPhotoViewer/NSBundle+NYTPhotoViewer.h>

@interface PhotoViewController ()
@property (nonatomic,readwrite) NYTPhotosOverlayView *overlayView;
@end

@implementation PhotoViewController
@synthesize overlayView;

-(void)viewDidLoad{
    self.overlayView = ({
        NYTPhotosOverlayView *v = [[NYTPhotosOverlayView alloc] initWithFrame:CGRectZero];
        UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"NYTPhotoViewerCloseButtonX" inBundle:[NSBundle nyt_photoViewerResourceBundle] compatibleWithTraitCollection:nil] landscapeImagePhone:[UIImage imageNamed:@"NYTPhotoViewerCloseButtonXLandscape" inBundle:[NSBundle nyt_photoViewerResourceBundle] compatibleWithTraitCollection:nil] style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonTapped:)];
        leftItem.tintColor = [UIColor whiteColor];
        v.leftBarButtonItem = leftItem;
        v.leftBarButtonItem.imageInsets = UIEdgeInsetsMake(3, 0, -3, 0);
        
        UIBarButtonItem *item =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonTapped:)];
        item.tintColor = [UIColor whiteColor];
        v.rightBarButtonItem = item;
        v;
    });
    [super viewDidLoad];
    
    
    
}

@end
