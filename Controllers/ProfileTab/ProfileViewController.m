//// Header

#import "ProfileViewController.h"
#import "ProfileAvatarCell.h"
#import "ProfileListTableViewCell.h"
#import "CenterLabelTableViewCell.h"
#import "CollectionViewTableViewCell.h"
#import "ProfilePhotosTableViewCell.h"
#import "GBPostTableViewCell.h"
#import "LoaderCell.h"
#import "ProfileBtnTableViewCell.h"
#import <NYTPhotoViewer/NYTPhotosViewController.h>
#import <NYTPhotoViewer/NYTPhoto.h>
#import <NYTPhotoViewer/NYTPhotoViewerArrayDataSource.h>
#import "PhotoViewController.h"
#import "FDTakeController.h"
#import "SearchHeaderCell.h"
#import "CityListViewController.h"
#import "UserListTableViewCell.h"
#import "SearchSettingsTableViewCell.h"
#import "GBPostCreateViewController.h"
#import "GBCommentsViewController.h"

typedef NS_ENUM(NSInteger, ProfileSectionType) {
    ProfileSectionAvatar  = 0,
    ProfileSectionInfo,
    ProfileSectionGroups,
    ProfileSectionFriends,
    ProfileSectionFollowers,
    ProfileSectionPhotos,
    ProfileSectionCreateFeed,
    ProfileSectionFeeds
};



@interface ProfileViewController ()<UITableViewDelegate, UITableViewDataSource, NYTPhotosViewControllerDelegate, FDTakeDelegate, UISearchResultsUpdating, UISearchBarDelegate, UISearchControllerDelegate>{
    CDUser *_user;
    UserObj  *_otherUser;
    NSInteger _userId;
    
    NSMutableArray *_friendList;
    NSMutableArray *_photoList;
    NSMutableArray *_userFeeds;
    BOOL _postComplete;
    
    UICollectionView *_photoCollectionView;
    
    NSInteger _searchIndex;
    NSString *_serchText;
    NSMutableArray *_searchList;
    UITableView *_searchTableView;
    
    BOOL _isSearch;
    BOOL _searchComplete;
    BOOL _isLastLoader;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) FDTakeController *takeController;

//@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (nonatomic, strong) UISearchController *searchController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchH;

@property (weak, nonatomic) IBOutlet UIView *searchContainer;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleLabel.text = trProfileTitle;
    _otherUser = self.info[@"user"];
    if (_otherUser) {
        _userId = _otherUser.objId;
        CDUser *user = [[SettingsManager instance] currentUser];
        if (_userId == user.objId) {
            _user = user;
        } else{
            [[SettingsManager instance] anySound];
        }
        
    } else{
        _user = [[SettingsManager instance] currentUser];
        _userId = _user.objId;
    }
    
    self.searchH.constant = 0;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [UIView new];
    
    UINib *avatarNib = [UINib nibWithNibName:@"ProfileAvatarCell" bundle:nil];
    [self.tableView registerNib:avatarNib forCellReuseIdentifier:@"avatar"];
    
    UINib *listNib = [UINib nibWithNibName:@"ProfileListTableViewCell" bundle:nil];
    [self.tableView registerNib:listNib forCellReuseIdentifier:@"listInfo"];
    
    UINib *emptyNib = [UINib nibWithNibName:@"CenterLabelTableViewCell" bundle:nil];
    [self.tableView registerNib:emptyNib forCellReuseIdentifier:@"empty"];
    
    UINib *collectionCell = [UINib nibWithNibName:@"CollectionViewTableViewCell" bundle:nil];
    [self.tableView registerNib:collectionCell forCellReuseIdentifier:@"collection"];
    
    UINib *photoNib = [UINib nibWithNibName:@"ProfilePhotosTableViewCell" bundle:nil];
    [self.tableView registerNib:photoNib forCellReuseIdentifier:@"photos"];
    
    UINib *loaderNib = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:loaderNib forCellReuseIdentifier:@"loader"];
    
    UINib *btnNib = [UINib nibWithNibName:@"ProfileBtnTableViewCell" bundle:nil];
    [self.tableView registerNib:btnNib forCellReuseIdentifier:@"btn"];
    
    
    _friendList = @[].mutableCopy;
    [self getFriendList];
    
    _photoList = @[].mutableCopy;
    [self getPhotoList];
    _userFeeds = @[].mutableCopy;
    
    
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.delegate = self;
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    self.searchController.hidesNavigationBarDuringPresentation = YES;
    self.searchController.searchBar.frame = CGRectMake(self.searchController.searchBar.frame.origin.x, self.searchController.searchBar.frame.origin.y, self.searchController.searchBar.frame.size.width, 44.0);
    
    self.extendedLayoutIncludesOpaqueBars = YES;
    
    self.definesPresentationContext = YES;
    _searchList = @[].mutableCopy;
    [self.searchController.searchBar setValue:trCancel forKey:@"_cancelButtonText"];
    self.searchController.searchBar.placeholder = trProfileSearch;
    if (_user) {
        self.tableView.tableHeaderView = self.searchController.searchBar;
    }
    
    UINib *settingNib = [UINib nibWithNibName:@"SearchSettingsTableViewCell" bundle:nil];
    [self.tableView registerNib:settingNib forCellReuseIdentifier:@"settings"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNow) name:@"createPost" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_isLastLoader) {
        [self getNow];
    }
}

#pragma mark - Server Methods

-(void)getFollowerList{}

-(void)getPhotoList{
    NSDictionary *params = @{@"id_wall":@(_userId), @"limit":@(100), @"offset":@(0)};
    NSString *str = [params convertToJsonString];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"gallery/last" params:@{@"data":str} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = [NSArray arrayWithArray:response[@"response"]];
            NSMutableArray *photos = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                PhotoObj *obj = [[PhotoObj alloc] initWithDict:dict];
                [photos addObject:obj];
            }
            [_photoList setArray:photos];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}


-(void)getFriendList{
    NSDictionary *params = @{@"id_user":@(_userId), @"limit":@(20), @"offset":@(0), @"random":@(0)};
    NSString *str = [params convertToJsonString];
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"users/friends/list" params:@{@"data":str} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = [NSArray arrayWithArray:response[@"response"]];
            NSMutableArray *userList = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [userList addObject:user];
            }
            [_friendList setArray:userList];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}
-(void)getNow{
//    [_userFeeds removeAllObjects];
//    if (_userFeeds != nil) {
//        _postComplete = NO;
//        [self getUserPosts];
//    }
    NSDictionary *params = @{@"offset":@(0),
                             @"limit":@(25),
                             @"id_wall":@(_userId)};
    
    NSString *dataString = [params convertToJsonString];
    NSDictionary *paramaters = @{@"data":dataString};
    __weak typeof(self) weakSelf = self;
    _postComplete = YES;
    [[ApiManager sharedManager] getRequest:@"posts/last" params:paramaters callback:^(id response, ErrorObj *error) {
        _isLastLoader = YES;
        [_userFeeds removeAllObjects];
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary *dict in items) {
                FeedModel *model = [[FeedModel alloc] initWithDict:dict];
                [_userFeeds addObject:model];
            }
            _postComplete = items.count < 25;
            [weakSelf.tableView reloadData];
        }
    }];
    
}


-(void)getUserPosts{
    if (_postComplete) {
        return;
    }
    NSDictionary *params = @{@"offset":@(_userFeeds.count),
                             @"limit":@(25),
                             @"id_wall":@(_userId)};
    
    NSString *dataString = [params convertToJsonString];
    NSDictionary *paramaters = @{@"data":dataString};
    __weak typeof(self) weakSelf = self;
    _postComplete = YES;
    [[ApiManager sharedManager] getRequest:@"posts/last" params:paramaters callback:^(id response, ErrorObj *error) {
        _isLastLoader = YES;
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary *dict in items) {
                FeedModel *model = [[FeedModel alloc] initWithDict:dict];
                [_userFeeds addObject:model];
            }
            _postComplete = items.count < 25;
            [weakSelf.tableView reloadData];
        } else{
            
        }
        
    }];
};


#pragma mark - Actions
-(void)sendMessage{
    if (_otherUser.objId == _user.objId) {
        return;
    }
    UserObj *obj = _otherUser;
    DialogModel *model = [[DialogModel alloc] init];
    model.user = obj;
    BaseViewController *controller = [self createController:@"MessageViewController"];
    controller.type = 1;
    controller.info = @{@"dialog":model};
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeCityId:(NSInteger)cityId{
    if (_user.objId == cityId) {
        return;
    }
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] patchRequest:@"users/self" params:@{@"id_city":@(cityId)} callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            _user.cityid = cityId;
            [_user save];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)takePhoto{
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    self.takeController.delegate = self;
    self.takeController.type = 1;
    [self.takeController takePhotoOrChooseFromLibrary];
}

-(void)changeAvatar{
    if (!_user) {
        [self  openAvatar:_otherUser.avatar];
        return;
    }
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    self.takeController.delegate = self;
    self.takeController.type = 2;
    [self.takeController takePhotoOrChooseFromLibrary];
}

- (void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info{
    
    NSData *data = UIImagePNGRepresentation(photo);
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(photo, 1/k);
    }
    if (controller.type == 1) {
        /// Gallery
        __weak typeof(self) weakSelf = self;
        [self showLoader];
        
        [[ApiManager sharedManager] uploadPhoto:data fileName:@"galery.png" callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                response = response[@"response"];
                NSInteger fileId = [response[@"id"] integerValue];
                [weakSelf addGallery:fileId];
                
            } else{
                [weakSelf showErrorMessage:error.message];
            }
            
        }];
    } else if(controller.type == 2){
        __weak typeof(self) weakSelf = self;
        [self showLoader];
        [[ApiManager sharedManager] uploadPhoto:data fileName:@"avatar.png" callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                response = response[@"response"];
                NSInteger fileId = [response[@"id"] integerValue];
                [weakSelf updateAvatar:fileId];
                
            } else{
                [weakSelf showErrorMessage:error.message];
            }
            
        }];
    }
}

-(void)addGallery:(NSInteger)fileId{
    /*
     Id_wall int64 `json:"id_wall"`
     Id_file uint64 `json:"id_file"`
     */
    CDUser *user = [[SettingsManager instance] currentUser];
    NSMutableDictionary *params = @{}.mutableCopy;
    params[@"id_wall"] = @(user.objId);
    params[@"id_file"] = @(fileId);
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] postRequest:@"gallery" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf getPhotoList];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
    
}

-(void)updateAvatar:(NSInteger)fileId{
    CDUser *user = [[SettingsManager instance] currentUser];
    NSMutableDictionary *params = @{}.mutableCopy;
    params[@"first_name"] = user.firstName;
    params[@"last_name"] = user.lastName;
    params[@"file_ava"] = @(fileId);
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] putRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            NSDictionary *params= @{@"data":@(user.objId)};
            [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
                if (response) {
                    response =response[@"response"];
                    _user =  [CDUser currentUserUpdate:response];
                    [weakSelf.tableView reloadData];
                }
            }];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}


-(void)openFriendList{
    if (_user) {
        UIStoryboard *main = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        BaseViewController *controller = [main instantiateViewControllerWithIdentifier:@"FriendsPageViewController"];
        controller.type = 1;
        [self.navigationController pushViewController:controller animated:YES];
        return;
    }
    BaseViewController *controller = [self createController:@"UserListViewController"];
    controller.info = @{@"userId":@(_userId),@"listType":@"1"};
    controller.type = 1;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)openFriendProfile:(NSInteger)index{
    UserObj *user = _friendList[index];
    [self openUserProfile:user];
}

-(void)openFollowerList{
    BaseViewController *controller = [self createController:@"UserListViewController"];
    controller.info = @{@"userId":@(_userId),@"listType":@"2"};
    controller.type = 1;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)relationClicked2:(UIButton *)sender{
    if (_user) {
        return;
    }
    NSDictionary *params = @{@"data":[@{@"id_user":@(_otherUser.objId)} convertToJsonString]};
    NSString *path =@"users/friends";
    __weak typeof(self) weakSelf = self;
    
    if (_otherUser.relation == 0) {
        [self showLoader];
        NSDictionary  *params = @{@"id_user":@(_otherUser.objId)};
        [[ApiManager sharedManager] postRequest:@"users/friends" params:params callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                _otherUser.relation = 2;
                [weakSelf.tableView  reloadData];
            }
        }];
    }
    if (_otherUser.relation == 1) {
        [self showAlert:trRelationDelete callback:^(id responseAlert, ErrorObj *errorAler) {
            if (responseAlert) {
                [weakSelf hideLoader];
                [[ApiManager sharedManager] deleteRequest:path params:params callback:^(id response, ErrorObj *error) {
                    [weakSelf hideLoader];
                    if (response) {
                        _otherUser.relation = 0;
                    } else{
                        [weakSelf showErrorMessage:error.message];
                    }
                    [weakSelf.tableView reloadData];
                }];
            }
        }];
    }
    
    if (_otherUser.relation == 2) {
        [self showAlert:trRelationCancelRequest callback:^(id responseAlert, ErrorObj *errorAler) {
            if (responseAlert) {
                [weakSelf showLoader];
                [[ApiManager sharedManager] deleteRequest:path params:params callback:^(id response, ErrorObj *error) {
                    [weakSelf hideLoader];
                    if (response) {
                        _otherUser.relation = 0;
                    } else{
                        [weakSelf showErrorMessage:error.message];
                    }
                    [weakSelf.tableView reloadData];
                }];
            }
        }];
    }
    
    
//    0 = post: api/users/friends?data={"id_user": 1}
//    это означает что при нажатии мы можем подать заявку в друзья
//
//    1 = delete: api/users/friends?data={"id_user": 1}
//    это означает что пользователь наш друг и при нажатии мы можем удалить его из друзей
//
//    2 = delete: api/users/friends?data={"id_user": 1}
//    это означает что заявка отправлена текущим пользователем при нажатии мы можем отменить запрос
//
//    3 = post: api/users/friends?data={"id_user": 1}
//    это означает что заявка подана текущему пользователю и при нажатии мы можем принять ее
//
//    4 = post: api/users/friends?data={"id_user": 1}
//    это означает что другой пользователь подписан на текущего и при нажатии он добавит его в друзья
//
//    5 = delete: api/users/friends?data={"id_user": 1}
//    это означает что текущий пользователь подписан на другого и при нажатии мы можем отписаться
//    UserSearchObj *obj = _searchList[sender.tag];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:1];
//    [self changeRelation:(UserObj *)obj tableView:self.tableView index:indexPath];
    
}


-(void)relationClicked:(UIButton *)sender{
    UserSearchObj *obj = _searchList[sender.tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:1];
    [self changeRelation:(UserObj *)obj tableView:self.tableView index:indexPath];
    
}
#pragma mark - Feed methods

-(UITableView *)table{
    return self.tableView;
}

-(NSMutableArray *)feedItems{
    return _userFeeds;
}


-(void)feedAction:(NSInteger)action indexPath:(NSIndexPath *)indexPath{
    if (action == FeedActionTypeLike) {
        GBPostModel *model = _userFeeds[indexPath.row];
        if (model.objId > 0) {
            __weak typeof(self) weakSelf = self;
            NSDictionary *params = @{@"id_post":@(model.objId)};
            [[ApiManager sharedManager] postRequest:@"users/posts/likes" params:params callback:^(id response, ErrorObj *error) {
                if (response){
                    model.isLiked = !model.isLiked;
                    model.countLikes += model.isLiked ? 1 : (-1);
                    if (indexPath){
                        [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                }
            }];
        }
    } else if(action == FeedActionTypeComment){
        GBPostModel *model = _userFeeds[indexPath.row];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
        [vc configureWithSourceId:model.objId itemType:ECommentItemTypeFeedComment];
        vc.type = 1;
        [self.navigationController pushViewController:vc animated:YES];
    }
}


-(void)logOut{
    [[SettingsManager instance] logOut];
    [GBCommonData setToken:nil];
    [GBCommonData setUserId:nil];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] openLogin];
}


#pragma mark - UITableView dataSource methods

-(BOOL) isSearchTableView:(UITableView *)tableView{
    return _isSearch;
    return [[[tableView class] description] isEqualToString:@"UISearchResultsTableView"];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([self isSearchTableView:tableView]) {
        return 2;
    }
    
    return ProfileSectionFeeds + 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([self isSearchTableView:tableView]) {
        return section == 0 ? 2 : _searchList.count;
    }
    if (section == ProfileSectionInfo) {
        return 3;
    }
    if (section == ProfileSectionGroups) {
        if(_user){
            return 3;
        }
        return 1;
    }
    if (section == ProfileSectionFeeds) {
        if (_postComplete) {
            return _userFeeds.count;
        }
        return _userFeeds.count + 1;
    }
    if (section == ProfileSectionCreateFeed) {
        return _user ? 1 : 0;
    }
    if (section == ProfileSectionPhotos) {
        return 1;//_photoList.count ? 1 : 0;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = COLOR(230, 230, 230);
    
    if ([self isSearchTableView:tableView]) {
        return section == 1 ? view : nil;
    }
    
    if (section == ProfileSectionCreateFeed) {
        return _user ? view : nil;
    }
    
    if (section == ProfileSectionPhotos) {
        return _photoList.count ? view : nil;
    }
    if (section == ProfileSectionGroups) {
        if (!_user) {
            return nil;
        }
    }
    
    if (section == ProfileSectionFriends) {
        if (!_user) {
            return nil;
        }
    }
    
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if ([self isSearchTableView:tableView]) {
        return section == 1 ? 10 : 0;
    }
    
    if (section == ProfileSectionCreateFeed) {
        return _user ? 10 : 0;
    }
    if (section == ProfileSectionGroups) {
        if (!_user) {
            return 0;
        }
    }
    if (section == ProfileSectionFriends) {
        if (!_user) {
            return 0;
        }
    }
    if (section == ProfileSectionPhotos) {
        return _photoList.count ? 10 : 0;
    }
    
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self isSearchTableView:tableView]) {
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            if (!cell) {
                UINib *nib = [UINib nibWithNibName:@"SearchHeaderCell" bundle:nil];
                [tableView registerNib:nib forCellReuseIdentifier:@"headerCell"];
                
                UINib *nib2 = [UINib nibWithNibName:@"UserListTableViewCell" bundle:nil];
                [tableView registerNib:nib2 forCellReuseIdentifier:@"userList"];
            }
            
        }
        if (indexPath.section == 0) {
            if (indexPath.row == 1) {
                SearchSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"settings"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            SearchHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"headerCell"];
            cell.selectionStyle =UITableViewCellSelectionStyleNone;
            cell.segmentView.selectedSegmentIndex = _searchIndex;
            [cell.segmentView addTarget:self action:@selector(changeFilterSegment:) forControlEvents:UIControlEventValueChanged];
            return cell;
        }
        
        UserListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"userList"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UserSearchObj *obj = _searchList[indexPath.row];
        cell.statusView.backgroundColor = appStatusColor(obj.online);
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@", obj.firstName, obj.lastName];
        [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:obj.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
        cell.actionLabel.text = [obj relationString];
        cell.actionBtn.tag = indexPath.row;
        [cell.actionBtn addTarget:self action:@selector(relationClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        
        return cell;
        
    }
    
    if (indexPath.section == ProfileSectionAvatar) {
        ProfileAvatarCell *cell = [tableView dequeueReusableCellWithIdentifier:@"avatar"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_user) {
            cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",_user.firstName, _user.lastName];
            [cell.iconView sd_setImageWithURL:[NSURL URLWithString:_user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
            cell.logOutBtn.hidden = NO;
            cell.statusView.backgroundColor = appStatusColor(1);
            
        } else{
            cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",_otherUser.firstName, _otherUser.lastName];
            cell.statusView.backgroundColor = _otherUser.statusColor;
            [cell.iconView sd_setImageWithURL:[NSURL URLWithString:_otherUser.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
            cell.logOutBtn.hidden = YES;
            
        }
        [cell.logOutBtn addTarget:self action:@selector(logOut) forControlEvents:UIControlEventTouchUpInside];
        [cell.avatarBtn addTarget:self action:@selector(changeAvatar) forControlEvents:UIControlEventTouchUpInside];
        return cell;
        
    }
    if (indexPath.section == ProfileSectionInfo) {
        ProfileListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listInfo"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (indexPath.row == 0) {
            cell.leftLabel.text = trCity;
            cell.rightLabel.text = _user ? [_user cityString] : [_otherUser cityString];;
            if (_user) {
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
        } else if (indexPath.row == 1){
            cell.leftLabel.text = @"Дата рождения";
            cell.rightLabel.text = _user ? [_user birthDayString] : [_otherUser birthDayString];;
            
        } else if (indexPath.row == 2){
            cell.leftLabel.text = @"Пол";
            cell.rightLabel.text = _user ? [_user gender] : [_otherUser gender];;
        }
        return cell;
    }
    
    if (indexPath.section == ProfileSectionGroups) {
        if (_user) {
            CenterLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"empty"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.btn.tag = indexPath.row;
            NSString *str = @"";
            
            if (indexPath.row == 0) {
//                cell.label.text = @"Мои группы";
                str = trProfileGroups;
            } else if (indexPath.row == 1){
                cell.label.text = @"Мои контакты";
                str = trProfileContacts;
            } else {
                cell.label.text = @"Мои вопросы";
                str = trProfileQuestions;
            }
            [StyleManager styleBtn:cell.btn title:str];
            [cell.btn addTarget:self action:@selector(groupsClicked:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
            
        } else{
            ProfileBtnTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"btn"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.contentView.backgroundColor = COLOR(230, 230, 230);
            [StyleManager styleBtn:cell.leftBtn title:[_otherUser relationString]];
            cell.leftLabel.text = [_otherUser relationString];
            [cell.leftBtn addTarget:self action:@selector(relationClicked2:) forControlEvents:UIControlEventTouchUpInside];
            [cell.rightBtn addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
    }
    // Friends
    if (indexPath.section == ProfileSectionFriends) {
        CollectionViewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"collection"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSInteger count = _user ? _user.friendsCount : _otherUser.friendsCount;
        if (_friendList.count == 0) {
            cell.collectionH.constant = 0;
        } else if(_friendList.count <= 3){
            cell.collectionH.constant =  itemSizeWidth + 10;
        } else{
            cell.collectionH.constant = (itemSizeWidth + 10) * 2;
        }
        __weak typeof(self) weakSelf = self;
        [cell setClickBlock:^(NSInteger index) {
            [weakSelf openFriendProfile:index];
        }];
        
        cell.titleLabel.text = trFriends;
        cell.countLabel.text = [NSString stringWithFormat:@"%@",count ? @(count) : @""];
        cell.items = _friendList;
        [cell.titleBtn addTarget:self action:@selector(openFriendList) forControlEvents:UIControlEventTouchUpInside];
        cell.allBtn.hidden = NO;
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
    }
    
    if (indexPath.section == ProfileSectionFollowers) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"some"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"some"];
            cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSString *str = trProfileFollowers;
        NSInteger count = _user ? _user.followersCount : _otherUser.followersCount;
        cell.textLabel.text = [NSString stringWithFormat:@"%@ \t%@",str,@(count)];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    if (indexPath.section == ProfileSectionPhotos) {
        ProfilePhotosTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"photos"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.titleLabel.text = _otherUser ? trProfilePhoto : trProfilePhotoSelf;
        cell.seeLabel.text = trProfileTakePhoto;
        cell.seeBtn.hidden = _otherUser ? YES : NO;
        cell.countLabel.text = [NSString stringWithFormat:@"%@",@(_photoList.count)];
        cell.items = _photoList;
        [cell.seeBtn addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];
        __weak typeof(self) weakSelf = self;
        [cell setClickBlock:^(NSInteger index) {
            [weakSelf showPhotos:index];
        }];
        _photoCollectionView = cell.collectionView;
        return cell;
    }
    
    if (indexPath.section == ProfileSectionCreateFeed) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"some"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"some"];
            cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = trProfileAddFeed;
        cell.accessoryType = UITableViewCellAccessoryNone;
        return cell;
        
    }
    
    if (indexPath.section == ProfileSectionFeeds) {
        if (indexPath.row == _userFeeds.count) {
            LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        FeedModel *post = _userFeeds[indexPath.row];
        return [self feedCell:post indexPath:indexPath tableView:tableView];
    }
    CenterLabelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"empty"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.label.text = @"Some text";
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_isSearch) {
        if (indexPath.section == 1) {
            UserSearchObj *obj = _searchList[indexPath.row];
            [self getUserProfile:obj.objId];
        }
        return;
    }
    
    if (indexPath.section == ProfileSectionCreateFeed) {
        BaseViewController *controller  = [self createController:@"AddQuestionViewController"];
        controller.info = @{@"post":@(1)};
        controller.type = 1;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        GBPostCreateViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"GBPostCreateViewController"];
//        [self.navigationController pushViewController:vc animated:YES];
        return;
        
    }
    if (indexPath.section == ProfileSectionGroups) {
        return;
    }
    
    if (indexPath.section == ProfileSectionFollowers) {
        [self openFollowerList];
    }
    
    if(indexPath.section == ProfileSectionInfo){
        if (indexPath.row == 0) {
            if (_user) {
                CityListViewController *controller = (CityListViewController *)[self createController:@"CityListViewController"];
                controller.info = @{@"id":@(_user.cityid)};
                controller.type = 1;
                __weak typeof(self) weakSelf = self;
                [controller setClickBlock:^(NSInteger objId) {
                    [weakSelf changeCityId:objId];
                }];
                [self.navigationController pushViewController:controller animated:YES];
            }
            
        }
    }
}

-(void)groupsClicked:(UIButton *)sender{
    if (sender.tag == 0) {
        [self openGroups];
    }
    if (sender.tag == 1) {
        [self getContacts];
    }
    if (sender.tag == 2) {
        [self openQuestions];
    }

}

-(void)openGroups{
    BaseViewController *controller = [self createController:@"GroupListViewController"];
    controller.type =1;
    controller.info = @{};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)openQuestions{
    BaseViewController *controller = [self createController:@"QuestionListViewController"];
    controller.type = 1;
    controller.info = @{};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)getContacts{
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] getRequest:@"users/phones" params:@{} callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if(response){
            NSArray *items = response[@"response"];
            if (items.count) {
                BaseViewController *controller = [self createController:@"UserListViewController"];
                controller.type = 1;
                controller.info = @{@"items":items};
                [weakSelf.navigationController pushViewController:controller animated:YES];
            } else{
                [weakSelf showErrorMessage:@"No contacts found"];
            }
            
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == ProfileSectionFeeds) {
        if (indexPath.row == _userFeeds.count ) {
            [self getUserPosts];
        }
        
    }
    
    if ([self isSearchTableView:tableView]) {
        if (indexPath.row == _searchList.count - 1) {
            [self searchUsersLoader];
        }
        return;
    }
    
}

#pragma mark - PhotoView methods
-(void)showPhotos:(NSInteger)index{
    NSMutableArray *items = @[].mutableCopy;
    for (PhotoObj *obj in _photoList) {
        UserPhotoView *view = [[UserPhotoView alloc] init];
        view.urlString = obj.path;
        [view sd_setImageWithURL:[NSURL URLWithString:obj.path]];
        [items addObject:view];
    }
    self.dataSource = [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:items];
    PhotoViewController *photosViewController = [[PhotoViewController alloc] initWithDataSource:self.dataSource initialPhoto:self.dataSource[index] delegate:self];
    photosViewController.indexType = 1;
    photosViewController.view.tintColor = [UIColor whiteColor];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

- (UIView *)photosViewController:(PhotoViewController *)photosViewController referenceViewForPhoto:(id <NYTPhoto>)photo {
    if (photosViewController.indexType == 0) {
        return nil;
    }
    NSInteger index = [self.dataSource indexOfPhoto:photo];
    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
    UICollectionViewCell *cell = [_photoCollectionView cellForItemAtIndexPath:path];
    return cell.contentView;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController loadingViewForPhoto:(id <NYTPhoto>)photo {
    //    if ([photo isEqual:self.dataSource.photos[NYTViewControllerPhotoIndexCustomEverything]]) {
    //        UILabel *loadingLabel = [[UILabel alloc] init];
    //        loadingLabel.text = @"Custom Loading...";
    //        loadingLabel.textColor = [UIColor greenColor];
    //        return loadingLabel;
    //    }
    //
    //    return nil;
    return nil;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController captionViewForPhoto:(id <NYTPhoto>)photo {
    //    if ([photo isEqual:self.dataSource.photos[NYTViewControllerPhotoIndexCustomEverything]]) {
    //        UILabel *label = [[UILabel alloc] init];
    //        label.text = @"Custom Caption View";
    //        label.textColor = [UIColor whiteColor];
    //        label.backgroundColor = [UIColor redColor];
    //        return label;
    //    }
    //
    return nil;
}

- (CGFloat)photosViewController:(NYTPhotosViewController *)photosViewController maximumZoomScaleForPhoto:(id <NYTPhoto>)photo {
    return 0.5f;
}

- (NSDictionary *)photosViewController:(NYTPhotosViewController *)photosViewController overlayTitleTextAttributesForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (NSString *)photosViewController:(PhotoViewController *)photosViewController titleForPhoto:(id<NYTPhoto>)photo atIndex:(NSInteger)photoIndex totalPhotoCount:(nullable NSNumber *)totalPhotoCount {
    if (photosViewController.indexType == 0) {
        return nil;
    }
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:photoIndex inSection:0];
    [_photoCollectionView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    return [NSString stringWithFormat:@"%lu/%lu", (unsigned long)photoIndex+1, (unsigned long)totalPhotoCount.integerValue];
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController didNavigateToPhoto:(id <NYTPhoto>)photo atIndex:(NSUInteger)photoIndex {
//    NSLog(@"Did Navigate To Photo: %@ identifier: %lu", photo, (unsigned long)photoIndex);
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController actionCompletedWithActivityType:(NSString *)activityType {
//    NSLog(@"Action Completed With Activity Type: %@", activityType);
}

- (void)photosViewControllerDidDismiss:(NYTPhotosViewController *)photosViewController {
//    NSLog(@"Did Dismiss Photo Viewer: %@", photosViewController);
}

#pragma mark - Search methods

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController{
//    [self.searchDisplayController.searchResultsTableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    self.searchController.active = YES;
    _isSearch = YES;
    [self.tableView reloadData];
    [self searchUsers];
    [searchBar setValue:trCancel forKey:@"_cancelButtonText"];
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar{
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _isSearch = NO;
    [self.tableView reloadData];
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    [self.tableView  setContentOffset:CGPointMake(0, 0)];
    
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    _serchText = searchText;
    _searchComplete = NO;
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(searchUsers) object:nil];
    [self performSelector:@selector(searchUsers) withObject:nil afterDelay:0.5];
}

-(void)searchUsers{
    //users/search
    if (_searchComplete) {
        return;
    }
    NSString *searchText = _serchText;
    NSMutableDictionary *params = @{}.mutableCopy;
    NSArray *arr = [searchText componentsSeparatedByString:@" "];
    
    if (arr.count>1) {
        params[@"first_name"] = arr[0];
        params[@"last_name"] = arr[1];
    } else if (searchText.length) {
        if (searchText.length) {
            params[@"first_name"] = searchText;
        }
    }
    if (_searchIndex) {
        params[@"id_city"] = @(_user.cityid);
        params[@"type"] = @"geo";
    }
    params[@"limit"] = @(30);
    params[@"offset"] = @(0);
    NSInteger time = (NSInteger)[[NSDate date] timeIntervalSince1970];
    params[@"time"] = @(time);
    NSDictionary *dict = @{@"data":[params convertToJsonString]};
    
    /*
     Time       uint64 `json:"time"`
     Limit      int64  `json:"limit"`
     Offset     int64  `json:"offset"`
     First_name string `json:"first_name"`
     Last_name  string `json:"last_name"`
     Id_city    uint64 `json:"id_city"`
     B_day      uint64 `json:"b_day"`
     B_month    uint64 `json:"b_month"`
     B_year     uint64 `json:"b_year"`
     Type       string `json:"type"`
     */
    _searchComplete = YES;
    [[ApiManager sharedManager] getRequest:@"users/search" params:dict callback:^(id response, ErrorObj *error) {
        if (response) {
            [_searchList removeAllObjects];
            NSArray *items = response[@"response"];
            NSMutableArray *results = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserSearchObj *obj = [[UserSearchObj alloc] initWithdict:dict];
                [results addObject:obj];
            }
            _searchComplete  = results.count < 30;
            [_searchList setArray:results];
            [_tableView reloadData];
        }
    }];
}
-(void)searchUsersLoader{
    //users/search
    if (_searchComplete) {
        return;
    }
    NSString *searchText = _serchText;
    NSMutableDictionary *params = @{}.mutableCopy;
    NSArray *arr = [searchText componentsSeparatedByString:@" "];
    
    if (arr.count>1) {
        params[@"first_name"] = arr[0];
        params[@"last_name"] = arr[1];
    } else if (searchText.length) {
        if (searchText.length) {
            params[@"first_name"] = searchText;
        }
    }
    if (_searchIndex) {
        params[@"id_city"] = @(_user.cityid);
        params[@"type"] = @"geo";
    }
    params[@"limit"] = @(30);
    params[@"offset"] = @(_searchList.count);
    NSInteger time = (NSInteger)[[NSDate date] timeIntervalSince1970];
    params[@"time"] = @(time);
    NSDictionary *dict = @{@"data":[params convertToJsonString]};
    
    /*
     Time       uint64 `json:"time"`
     Limit      int64  `json:"limit"`
     Offset     int64  `json:"offset"`
     First_name string `json:"first_name"`
     Last_name  string `json:"last_name"`
     Id_city    uint64 `json:"id_city"`
     B_day      uint64 `json:"b_day"`
     B_month    uint64 `json:"b_month"`
     B_year     uint64 `json:"b_year"`
     Type       string `json:"type"`
     */
    
    [[ApiManager sharedManager] getRequest:@"users/search" params:dict callback:^(id response, ErrorObj *error) {
        if (response) {
//            [_searchList removeAllObjects];
            NSArray *items = response[@"response"];
            NSMutableArray *results = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserSearchObj *obj = [[UserSearchObj alloc] initWithdict:dict];
                [results addObject:obj];
            }
            _searchComplete = results.count < 30;
            [_searchList addObjectsFromArray:results];
            [_tableView reloadData];
        }
    }];
}

-(void)changeFilterSegment:(UISegmentedControl *)segmentControl{
    _searchIndex = segmentControl.selectedSegmentIndex;
    [self searchUsers];
}

@end
