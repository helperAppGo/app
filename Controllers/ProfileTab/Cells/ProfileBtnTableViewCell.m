//// Header

#import "ProfileBtnTableViewCell.h"
#import "Globals.h"
@implementation ProfileBtnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftBtn.superview.backgroundColor = [UIColor clearColor];
    self.leftBtn.backgroundColor = [UIColor whiteColor];
    self.leftLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.leftLabel.text = trAddFriend;
    
    self.rightBtn.superview.backgroundColor = [UIColor clearColor];
    self.rightBtn.backgroundColor = [UIColor whiteColor];
    self.rightLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.rightLabel.text = trWrite;
    
    self.rightBtn.layer.cornerRadius =  self.leftBtn.layer.cornerRadius = 20;
    
    self.rightBtn.backgroundColor = self.leftBtn.backgroundColor = [UIColor whiteColor];
    
    self.rightBtn.layer.borderColor = [UIColor grayColor].CGColor;
    self.rightBtn.layer.borderWidth = 1;
    [StyleManager styleBtn:self.rightBtn titleColor:appBlueColor];
    [self.rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];

    [StyleManager styleBtn:self.rightBtn title:trWrite];
    
    self.leftBtn.layer.borderColor = [UIColor grayColor].CGColor;
    self.leftBtn.layer.borderWidth = 1;
    [StyleManager styleBtn:self.leftBtn titleColor:appBlueColor];
    [self.leftBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                                       
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
