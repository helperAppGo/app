//// Header

#import "ProfileListTableViewCell.h"

@implementation ProfileListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.leftLabel.textColor = COLOR(60, 60, 60);
    self.leftLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    
    self.rightLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
