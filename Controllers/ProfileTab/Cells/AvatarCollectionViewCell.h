//// Header

#import <UIKit/UIKit.h>

@interface AvatarCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@end
