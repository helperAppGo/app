//// Header

#import "ProfilePhotosTableViewCell.h"
#import "AvatarCollectionViewCell.h"

@implementation ProfilePhotosTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.countLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.seeLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.seeLabel.textColor = [UIColor blueColor];
    self.countLabel.textColor = COLOR(200, 200, 200);
    
    
    UICollectionViewFlowLayout * _verticalLayout = [[UICollectionViewFlowLayout alloc] init];
    _verticalLayout.minimumInteritemSpacing = 10;
    _verticalLayout.minimumLineSpacing = 10;
    _verticalLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _verticalLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    float w = [UIScreen mainScreen].bounds.size.width;
    if (w > 320) {
        self.collectionH.constant = 100;
        _verticalLayout.itemSize = CGSizeMake(100, 100);
    } else{
        _verticalLayout.itemSize = CGSizeMake(80, 80);
    }
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView setCollectionViewLayout:_verticalLayout animated:YES];
    
    UINib *nib = [UINib nibWithNibName:@"AvatarCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"list"];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView reloadData];
    
}
-(void)setItems:(NSArray *)items{
    _items = items;
    [self.collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AvatarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"list" forIndexPath:indexPath];
    cell.avatarView.backgroundColor = [UIColor clearColor];
    PhotoObj *photo = _items[indexPath.row];
    NSURL *url = [NSURL URLWithString:photo.path];
    [cell.avatarView sd_setImageWithURL:url placeholderImage:nil];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float w = self.collectionView.frame.size.height;
    return CGSizeMake(w, w);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (self.clickBlock) {
        self.clickBlock(indexPath.row);
    }
}

@end
