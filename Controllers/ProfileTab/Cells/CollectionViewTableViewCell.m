//// Header

#import "CollectionViewTableViewCell.h"
#import "AvatarCollectionViewCell.h"



@implementation CollectionViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    UICollectionViewFlowLayout * _verticalLayout = [[UICollectionViewFlowLayout alloc] init];
    _verticalLayout.minimumInteritemSpacing = 10;
    _verticalLayout.minimumLineSpacing = 10;
    _verticalLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _verticalLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    _verticalLayout.itemSize = CGSizeMake(itemSizeWidth, itemSizeWidth);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [StyleManager styleBtn:self.titleBtn title:trFriends];
    [StyleManager styleBtn:self.titleBtn titleColor:appBlueColor];
    [self.titleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.titleBtn.titleLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    
    [self.collectionView setCollectionViewLayout:_verticalLayout animated:YES];
    [self.collectionView reloadData];
    
    UINib *nib = [UINib nibWithNibName:@"AvatarCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"list"];
    

    self.titleLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    
    self.countLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    self.countLabel.textColor = COLOR(120, 120, 120);
    
    self.allLabel.text = @"";//@"See all";
    self.allLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.allLabel.hidden = YES;
    self.allBtn.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setItems:(NSArray *)items{
    _items = items;
    [self.collectionView reloadData];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_items.count <= 6) {
        return _items.count;
    }
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AvatarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"list" forIndexPath:indexPath];
    cell.avatarView.backgroundColor = [UIColor clearColor];
    UserObj *user = _items[indexPath.row];
    NSURL *url = [NSURL URLWithString:user.avatar];
    [cell.avatarView sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"user_ava"]];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(itemSizeWidth, itemSizeWidth);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (self.clickBlock) {
        self.clickBlock(indexPath.row);
    }
}


@end
