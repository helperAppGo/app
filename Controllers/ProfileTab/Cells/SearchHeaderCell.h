//// Header

#import <UIKit/UIKit.h>

@interface SearchHeaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentView;

@end
