//// Header

#import "SearchSettingsTableViewCell.h"

@implementation SearchSettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.label.textColor = COLOR(24, 124, 246);
    self.label.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.label.text = trSearchSettings;//@"Настройка поиска";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
