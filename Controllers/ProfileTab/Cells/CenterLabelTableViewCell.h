//// Header

#import <UIKit/UIKit.h>

@interface CenterLabelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end
