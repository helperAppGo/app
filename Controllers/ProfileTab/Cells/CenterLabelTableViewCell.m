//// Header

#import "CenterLabelTableViewCell.h"

@implementation CenterLabelTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.label.font = [UIFont defaultFontWithSize:14 bold:NO];
    
    [StyleManager styleBtn:self.btn titleColor:appBlueColor];
    [self.btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.btn.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.label.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
