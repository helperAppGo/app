//// Header

#import <UIKit/UIKit.h>

@interface ProfileBtnTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;

@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;

@end
