//// Header

#import "AvatarCollectionViewCell.h"

@implementation AvatarCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = 6;
}

@end
