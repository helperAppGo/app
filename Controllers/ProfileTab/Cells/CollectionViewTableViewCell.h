//// Header

#import <UIKit/UIKit.h>

#define itemSizeWidth ([[UIScreen mainScreen] bounds].size.width - 2 *(10 + 10))/3.f - 0.05

@interface CollectionViewTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionH;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) NSArray *items;

@property (weak, nonatomic) IBOutlet UIButton *allBtn;
@property (weak, nonatomic) IBOutlet UILabel *allLabel;

@property (nonatomic, copy) void(^clickBlock)(NSInteger index);
@property (weak, nonatomic) IBOutlet UIButton *titleBtn;


@end
