//// Header

#import "SearchHeaderCell.h"

@implementation SearchHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateNormal];
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateSelected];
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateHighlighted];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
