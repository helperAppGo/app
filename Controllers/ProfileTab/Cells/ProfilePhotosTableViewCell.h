//// Header

#import <UIKit/UIKit.h>

@interface ProfilePhotosTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>{
    
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *seeBtn;
@property (weak, nonatomic) IBOutlet UILabel *seeLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSArray *items;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionH;
@property (copy, nonatomic) void(^clickBlock)(NSInteger index);


@end
