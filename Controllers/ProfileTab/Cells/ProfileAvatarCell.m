//// Header

#import "ProfileAvatarCell.h"

@implementation ProfileAvatarCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.iconView.contentMode = UIViewContentModeScaleAspectFill;
    self.iconView.clipsToBounds = YES;
    self.iconView.layer.cornerRadius = 7;
    
    self.nameLabel.font = [UIFont defaultFontWithSize:16 bold:YES];
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    self.nameLabel.textColor = COLOR(150, 150, 150);
    
    self.logOutBtn.backgroundColor = [UIColor clearColor];
    self.logOutLabel.text = @"Log out";
    self.logOutLabel.textColor = [UIColor whiteColor];
    self.logOutLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.statusView.layer.cornerRadius = 5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
