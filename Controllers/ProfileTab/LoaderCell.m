//// Header

#import "LoaderCell.h"

@implementation LoaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.loader startAnimating];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
