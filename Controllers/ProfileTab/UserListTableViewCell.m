//// Header

#import "UserListTableViewCell.h"

@implementation UserListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = 25;
    
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.actionLabel.font = [UIFont defaultFontWithSize:10 bold:NO];
    self.actionBtn.backgroundColor = COLOR(245, 245, 245);
    self.actionBtn.layer.cornerRadius = 2;
    self.actionBtn.layer.borderColor = COLOR(235, 235, 235).CGColor;
    self.actionBtn.layer.borderWidth = 0.5;
    self.statusView.layer.cornerRadius = 5;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.actionBtn.layer.cornerRadius = 2;
    self.actionBtn.layer.masksToBounds = true;
    self.actionBtn.layer.masksToBounds = false;
    self.actionBtn.layer.shadowOffset = CGSizeMake(0, 0);
    self.actionBtn.layer.shadowColor = COLOR(150, 150, 150).CGColor;
    self.actionBtn.layer.shadowOpacity = 0.23;
    self.actionBtn.layer.shadowRadius = 4;
}

@end
