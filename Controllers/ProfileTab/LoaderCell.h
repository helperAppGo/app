//// Header

#import <UIKit/UIKit.h>

@interface LoaderCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;

@end
