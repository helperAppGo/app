//// Header

#import "UserListViewController.h"
#import "UserListTableViewCell.h"
#import "LoaderCell.h"


@interface UserListViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *_list;
    UIRefreshControl *_refreshControl;
//    BOOL _friendList;
    NSInteger _userId;
    BOOL _complete;
    NSInteger _currentUserId;
    
    NSInteger _listType; // 0- contacts, 1-friends. 2- followers  3-likes
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation UserListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UINib *nib = [UINib nibWithNibName:@"UserListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *loaderNib = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:loaderNib forCellReuseIdentifier:@"loader"];
    CDUser  *user = [[SettingsManager instance] currentUser];
    _currentUserId = user.objId;
    
    _list = @[].mutableCopy;
    if (self.info[@"items"]) {
        _listType = 0;
        [_list setArray:self.info[@"items"]];
    } else{
        _listType = [self.info[@"listType"] integerValue];
        _userId = [self.info[@"userId"] integerValue];
        
        _refreshControl = [[UIRefreshControl alloc] init];
        [self.tableView addSubview:_refreshControl];
        [_refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];

    }
    _titleLabel.text = [self titleString];
    
}
-(NSString *)titleString{
    if (_listType == 0) {
        return @"Контакты";
    }
    if (_listType == 1) {
        return @"Друзья";
    }
    if (_listType == 2) {
        return @"Подписчики";
    }
    if (_listType == 3) {
        return @"Нравиться";
    }
    if (_listType == 4) {
        return @"Администраторы";
    }
    return @"";
}

-(void)refresh{
    if (!_complete) {
        return;
    }
    NSDictionary *params = @{@"id_user":@(_userId), @"limit":@(20), @"offset":@(0), @"random":@(0)};
    NSString *path = @"";
    if (_listType == 1) {
        path = @"users/friends/list";
    }
    if (_listType == 2) {
        path = @"users/friends/followers";
    }
    
    if (_listType == 3) {
        path = @"posts/likes";
        params = @{@"id_post":@([self.info[@"postId"] integerValue])};
    }
    if (_listType == 4) {
        path = @"groups/members/admin/list";
        NSInteger groupId = [self.info[@"groupId"] integerValue];
        params = @{@"id_group":@(groupId), @"limit":@(20), @"offset":@(0)};
    }
    NSString *str = [params convertToJsonString];
    NSDictionary *p  = @{@"data":str};
    
    __weak typeof(self) weakSelf = self;
    _complete = NO;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:path params:p callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            NSArray *items = [NSArray arrayWithArray:response[@"response"]];
            NSMutableArray *userList = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [userList addObject:user];
            }
            _complete = userList.count < 20;
            [_list setArray:userList];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getList{
    if (_complete) {
        return;
    }
    NSDictionary *params = @{@"id_user":@(_userId), @"limit":@(20), @"offset":@(_list.count), @"random":@(0)};
    
    NSString *path = @"";
    if (_listType == 1) {
        path = @"users/friends/list";
    }
    if (_listType == 2) {
        path = @"users/friends/followers";
    }
    
    if (_listType == 3) {
        path = @"posts/likes";
        params = @{@"id_post":@([self.info[@"postId"] integerValue])};
    }
    
    if (_listType == 4) {
        path = @"groups/members/admin/list";
        NSInteger groupId = [self.info[@"groupId"] integerValue];
        params = @{@"id_group":@(groupId), @"limit":@(20), @"offset":@(_list.count)};
    }
    NSString *str = [params convertToJsonString];
    NSDictionary *p  = @{@"data":str};
    _complete = YES;
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:path params:p callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = [NSArray arrayWithArray:response[@"response"]];
            NSMutableArray *userList = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [userList addObject:user];
            }
            _complete = (userList.count < 20);
            [_list addObjectsFromArray:userList];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



-(void)relationClicked:(UIButton *)sender{
    UserObj *user = _list[sender.tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    [self changeRelation:user tableView:self.tableView index:indexPath];
}
#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_complete){
        return _list.count;
    }
    if (self.info[@"items"]) {
        return _list.count;
    }
    return _list.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        }
        return cell;
    }
    UserListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserObj *user = _list[indexPath.row];
    if (![user isKindOfClass:[UserObj class]]) {
        NSDictionary *dict = _list[indexPath.row];
        [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:dict[@"path_ava"]] placeholderImage:[UIImage imageNamed:@"user_ava"]];
        cell.nameLabel.text = dict[@"name"];
        NSInteger userId = [dict[@"id"] integerValue];
        cell.actionBtn.hidden = _currentUserId == userId;
        cell.statusView.backgroundColor = appStatusColor(0);
        cell.actionLabel.text = [NSString stringWithFormat:@"%@",dict[@"phone"]];
        cell.actionBtn.hidden = YES;
        return cell;
    }
    
    cell.actionBtn.tag = indexPath.row;
    [cell.actionBtn addTarget:self action:@selector(relationClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.nameLabel.text = [user nameString];
    cell.actionLabel.text = [user relationString];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserObj *user = _list[indexPath.row];
    if ([user isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dict = _list[indexPath.row];
        NSInteger userId  = [dict[@"id"] integerValue];
        [self getUserProfile:userId];
    } else{
        UserObj *user = _list[indexPath.row];
        [self openUserProfile:user];
    }
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        [self getList];
    }
}



@end
