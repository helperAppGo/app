//// Header

#import "UserSearchObj.h"

@implementation UserSearchObj

-(id)initWithdict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.fileAva = [NULL_TO_NIL(dict[@"file_ava"]) integerValue];
        self.firstName = NULL_TO_NIL(dict[@"first_name"]);
        self.lastName = NULL_TO_NIL(dict[@"last_name"]);
        self.isFriend = [NULL_TO_NIL(dict[@"is_friend"]) integerValue];
        self.avatar = NULL_TO_NIL(dict[@"path_ava"]);
        self.relation = self.isFriend ? 1 : 0;
    }
    return self;
}

-(NSString *)relationString{
    NSArray *items = @[@"Добавить в друзья", @"Ваш друг", @"Заявка отправлена"];
    if (self.relation >= items.count) {
        return @"Заблокирован";
    }
    return items[self.relation];
}
@end
