//// Header

#import <Foundation/Foundation.h>

@interface UserSearchObj : NSObject

@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger fileAva;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, assign) NSInteger isFriend;
@property (nonatomic, assign) NSInteger relation;
@property (nonatomic, assign) NSInteger online;
@property (nonatomic, strong) NSString *avatar;

-(id)initWithdict:(NSDictionary *)dict;
-(NSString *)relationString;

@end
