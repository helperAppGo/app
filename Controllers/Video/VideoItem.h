//// Header

#import <Foundation/Foundation.h>

@interface VideoItem : NSObject
@property (nonatomic, strong) NSString *youtubeId;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *thumb;
-(id)initWithDict:(NSDictionary *)dict;
@end
