/*
 * This file is part of the JPVideoPlayer package.
 * (c) NewPan <13246884282@163.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Click https://github.com/newyjp
 * or http://www.jianshu.com/users/e2f2d779c022/latest_articles to contact me.
 */

#import "JPVideoPlayerWeiBoEqualHeightCell.h"

@interface JPVideoPlayerWeiBoEqualHeightCell()

@end

@implementation JPVideoPlayerWeiBoEqualHeightCell

-(void)awakeFromNib{
    [super awakeFromNib];
    self.videoPlayView.superview.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.shareBtn addTarget:self action:@selector(shareClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)shareClicked{
    if (self.shareBlock) {
        self.shareBlock(self.indexPath.row);
    }
}
-(void)layoutSubviews{
    [super layoutSubviews];
    {
        self.containerView.layer.cornerRadius = 8;
        self.containerView.layer.masksToBounds = false;
        self.containerView.layer.shadowOffset = CGSizeMake(0, 2);
        self.containerView.layer.shadowColor = [UIColor whiteColor].CGColor;
        self.containerView.layer.shadowOpacity = 1;
        self.containerView.layer.shadowRadius = 4;
    }
    {
//        self.videoContainerView.layer.cornerRadius = 8;
//        self.videoContainerView.layer.masksToBounds = false;
//        self.videoContainerView.layer.shadowOffset = CGSizeMake(0, 1.5);
//        self.videoContainerView.layer.shadowColor = [UIColor whiteColor].CGColor;
//        self.videoContainerView.layer.shadowOpacity = 1;
//        self.videoContainerView.layer.shadowRadius = 4;
    }
}

@end
