/*
 * This file is part of the JPVideoPlayer package.
 * (c) NewPan <13246884282@163.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * Click https://github.com/newyjp
 * or http://www.jianshu.com/users/e2f2d779c022/latest_articles to contact me.
 */

#import "JPVideoPlayerWeiBoListViewController.h"
#import "JPVideoPlayerKit.h"
#import "JPVideoPlayerWeiBoEqualHeightCell.h"
#import "JPVideoPlayerDetailViewController.h"
#import "JPVideoPlayerWeiBoUnequalHeightCell.h"
#import "VideoItem.h"
#import "HCYoutubeParser.h"
#import "ShareViewController.h"


@interface JPVideoPlayerWeiBoListViewController ()<JPTableViewPlayVideoDelegate, JPVideoPlayerDelegate>{
    NSMutableArray *_list;
    NSString *_token;
    NSInteger _maxCount;
    UIRefreshControl *_refreshControl;
    BOOL _complete;
    BOOL _loading;
    NSString *_query;
}
@property (weak, nonatomic) IBOutlet UIView *searchContent;
@property (weak, nonatomic) IBOutlet UIButton *searchReady;
@property (weak, nonatomic) IBOutlet UITextField *searchField;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchWidth;


/**
 * Arrary of video paths.
 * 播放路径数组集合.
 */
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITableView *table;
@property(nonatomic, assign, readwrite) JPScrollPlayStrategyType scrollPlayStrategyType;

//@property(nonatomic, strong, nonnull)NSArray *pathStrings;
@property (nonatomic, strong) JPVideoPlayerWeiBoEqualHeightCell *playingCell;

@end

#define JPVideoPlayerDemoRowHei ([UIScreen mainScreen].bounds.size.width*9.0/16.0)
@implementation JPVideoPlayerWeiBoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [self.table addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(getNow) forControlEvents:UIControlEventValueChanged];
    [self.backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.backBtn.superview.backgroundColor = [UIColor blackColor];
    self.topLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.topLabel.textColor = [UIColor whiteColor];
    self.topLabel.text = @"Видео";
    
    _list = @[].mutableCopy;
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.separatorInset  = UIEdgeInsetsZero;
    self.table.separatorColor = [UIColor clearColor];
    self.table.backgroundColor = [UIColor blackColor];
    self.view.backgroundColor = [UIColor blackColor];
    
    _titleLabel.text = @"Video";
    UINib *nib = [UINib nibWithNibName:@"JPVideoPlayerWeiBoEqualHeightCell" bundle:nil];
    [self.tableView registerNib:nib
         forCellReuseIdentifier:@"JPVideoPlayerWeiBoEqualHeightCell"];
//    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([JPVideoPlayerWeiBoUnequalHeightCell class]) bundle:nil]
//         forCellReuseIdentifier:NSStringFromClass([JPVideoPlayerWeiBoUnequalHeightCell class])];
    
    self.scrollPlayStrategyType = JPScrollPlayStrategyTypeBestVideoView;
    self.tableView.jp_delegate = self;
    self.tableView.jp_scrollPlayStrategyType = self.scrollPlayStrategyType;
    
//    self.pathStrings = @[
//                         @"https://www.youtube.com/watch?v=_PbNqiWNlzQ",
//                         @"http://www.w3school.com.cn/example/html5/mov_bbb.mp4",
//                         @"https://www.w3schools.com/html/movie.mp4",
//                         @"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4",
//                         @"https://media.w3.org/2010/05/sintel/trailer.mp4",
//                         @"http://mvvideo2.meitudata.com/576bc2fc91ef22121.mp4",
//                         @"http://mvvideo10.meitudata.com/5a92ee2fa975d9739_H264_3.mp4",
//                         @"http://mvvideo11.meitudata.com/5a44d13c362a23002_H264_11_5.mp4",
//                         @"http://mvvideo10.meitudata.com/572ff691113842657.mp4",
//                         @"https://api.tuwan.com/apps/Video/play?key=aHR0cHM6Ly92LnFxLmNvbS9pZnJhbWUvcGxheWVyLmh0bWw%2FdmlkPXUwNjk3MmtqNWV6JnRpbnk9MCZhdXRvPTA%3D&aid=381374",
//                         @"https://api.tuwan.com/apps/Video/play?key=aHR0cHM6Ly92LnFxLmNvbS9pZnJhbWUvcGxheWVyLmh0bWw%2FdmlkPWswNjk2enBud2xvJnRpbnk9MCZhdXRvPTA%3D&aid=381395",
//                         ];
    
    self.searchField.backgroundColor = COLOR(110, 110, 110);
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Искать...." attributes:@{NSFontAttributeName : [UIFont defaultFontWithSize:14 bold:NO],NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.searchField.attributedPlaceholder = str;
    self.searchContent.backgroundColor = COLOR(81, 81, 81);
    self.searchContent.layer.cornerRadius = 4;
    [self.searchReady addTarget:self action:@selector(searchReadyClicked) forControlEvents:UIControlEventTouchUpInside];
    self.searchField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.searchField.font = [UIFont defaultFontWithSize:14 bold:NO];
    [self.searchBtn addTarget:self action:@selector(searchClicked) forControlEvents:UIControlEventTouchUpInside];
    self.searchField.textColor = [UIColor whiteColor];
    self.searchWidth.constant = 0;

    [self setup];
    [self getItems];
}



-(void)searchClicked{
    self.searchField.text  = nil;
    [UIView animateWithDuration:0.4 animations:^{
        self.searchWidth.constant = 270;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.searchField becomeFirstResponder];
        }
    }];
    
}

-(void)searchReadyClicked{
    _query = self.searchField.text;
    [self.searchField resignFirstResponder];
    [UIView animateWithDuration:0.4 animations:^{
        self.searchWidth.constant = 0;
    } completion:^(BOOL finished) {
        self.searchField.text = nil;
    }];
    [self getNow];
}



-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)getNow{
    if (_loading) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    _loading  = YES;
    NSDictionary *params = @{};
    if (_query.length) {
        params = @{@"q":_query};
    }
    [[ApiManager sharedManager] youtubeList:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [_list removeAllObjects];
            _token = response[@"nextPageToken"];
            _maxCount = [response[@"pageInfo"][@"totalResults"] integerValue];
            NSArray *items = response[@"items"];
            [weakSelf getLinks:items];
        }
    }];
}

-(void)getItems{
    if (_loading) {
        return;
    }
    __weak typeof(self) weakSelf = self;
    if (_list.count == 0) {
        [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:^(void){
            self.tableView.contentOffset = CGPointMake(0, -_refreshControl.frame.size.height);
        } completion:^(BOOL finished) {
            [_refreshControl beginRefreshing];
        }];
       
    }
    NSMutableDictionary *params = _token ? @{@"pageToken":_token}.mutableCopy : @{}.mutableCopy;
    if (_query.length) {
        params[@"q"] = _query;
    }
    _loading  = YES;
    [[ApiManager sharedManager] youtubeList:params callback:^(id response, ErrorObj *error) {
        if (response) {
            _token = response[@"nextPageToken"];
            _maxCount = [response[@"pageInfo"][@"totalResults"] integerValue];
            NSArray *items = response[@"items"];
            [weakSelf getLinks:items];
        }
    }];
}
-(void)getLinks:(NSArray *)items{
    dispatch_group_t group = dispatch_group_create();
//    [self showLoader];
    
    for (NSDictionary *dict in items) {
        
        id objId = NULL_TO_NIL(dict[@"id"]);
        NSString *youtubeId;
        if ([objId isKindOfClass:[NSDictionary class]]) {
            youtubeId = objId[@"videoId"];
        } else{
            youtubeId = objId;
        }
        
        NSString *title = dict[@"snippet"][@"title"];
        NSDictionary *thumb = dict[@"snippet"][@"thumbnails"][@"medium"];
        NSString *thumbLink =  thumb[@"url"];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",youtubeId]];
        dispatch_group_enter(group);
        [HCYoutubeParser h264videosWithYoutubeURL:url completeBlock:^(NSDictionary *pars, NSError *error) {
            NSString *URLString = nil;
            if (pars[@"medium"]) {
                URLString = pars[@"medium"];
            } else if(pars[@"small"]) {
                URLString = pars[@"small"];
            } else if (pars[@"live"]) {
                URLString = pars[@"live"];
            }
            if (URLString.length) {
                NSString *videoString = URLString;
                VideoItem *item = [[VideoItem alloc] init];
                item.url = videoString;
                item.thumb  = thumbLink;
                item.title = title;
                item.youtubeId = youtubeId;
                [_list addObject:item];
            }
            dispatch_group_leave(group);
        }];
    }
    dispatch_group_notify(group,dispatch_get_main_queue(),^{
        _loading = NO;
        [_refreshControl endRefreshing];
        [self.table reloadData];
        [self.tableView jp_handleCellUnreachableTypeInVisibleCellsAfterReloadData];
        [self.tableView jp_playVideoInVisibleCellsIfNeed];
        

    });
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGRect tableViewFrame = self.tableView.frame;
    tableViewFrame.size.height -= self.tabBarController.tabBar.bounds.size.height;
    self.tableView.jp_tableViewVisibleFrame = tableViewFrame;
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView jp_handleCellUnreachableTypeInVisibleCellsAfterReloadData];
    [self.tableView jp_playVideoInVisibleCellsIfNeed];
    self.tableView.delegate = self;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.tableView.delegate = nil;
    self.navigationController.navigationBar.hidden = NO;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}


#pragma mark - Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return _list.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *reuseIdentifier = NSStringFromClass([JPVideoPlayerWeiBoEqualHeightCell class]);
    JPVideoPlayerWeiBoEqualHeightCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.indexPath = indexPath;
    cell.videoPlayView.backgroundColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    VideoItem *video =  _list[indexPath.row];
    [cell.videoPlayView sd_setImageWithURL:[NSURL URLWithString:video.thumb]];
    cell.jp_videoURL = [NSURL URLWithString:video.url];
    cell.jp_videoPlayView = cell.videoPlayView;
    cell.jp_videoPlayerDelegate = self;
    cell.contentView.backgroundColor = [UIColor blackColor];
    cell.titleLabel.text = video.title;
    [tableView jp_handleCellUnreachableTypeForCell:cell
                                       atIndexPath:indexPath];
    __weak typeof(self) weakSelf = self;
    [cell setShareBlock:^(NSInteger index) {
        [weakSelf shareClicked:index];
    }];
    return cell;
}

-(void)shareClicked:(NSInteger)index{
    VideoItem *item = _list[index];
    ShareViewController *controller = (ShareViewController *)[self createController:@"ShareViewController"];
    __weak typeof(self) weakSelf = self;
    [controller setBlock:^(NSInteger type, NSString *shareText) {
        if (type == 1) {
            [weakSelf sharePost:item text:shareText fileId:0];
        }
    }];
    controller.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.preferredContentSize = CGSizeMake(self.view.width, self.view.height);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    });
}
-(void)postLink:(VideoItem *)item{
//    youtube
   

}

-(void)sharePost:(VideoItem *)model text:(NSString *)shareText fileId:(NSInteger)fileId{
    
    if (fileId) {
        CDUser *user = [[SettingsManager instance] currentUser];
        NSString *fileString = [NSString stringWithFormat:@"%@",@(fileId)];
        NSDictionary *params = @{@"id_wall":@(user.objId),
                                 @"files":fileString};
        if (model.title) {
            params = @{@"id_wall":@(user.objId),
                       @"files":fileString,
                       @"text":shareText};
        }
        __weak typeof(self) weakSelf = self;
        [[ApiManager sharedManager] postRequest:@"posts" params:params callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];

    } else{
        __weak typeof(self) weakSelf = self;
        [self showLoader];
        NSString *link = [NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",model.youtubeId];
        [[ApiManager sharedManager] postRequest:@"youtube" params:@{@"link":link} callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                response = response[@"response"];
                NSInteger objId = [response[@"id"] integerValue];
                [weakSelf sharePost:model text:shareText fileId:objId];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    }
   
//    [self showLoader];
    /*
     
     POST@api/posts
     передаем
     id_wall пользователя который делат репост
     text
     repost передаем  id_post которого делаем репост
     */
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_list.count >0 && indexPath.row > _list.count - 2) {
        if(_list.count >= _maxCount){
            return;
        }
        
        [self getItems];
    }
}


#pragma mark - TableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    JPVideoPlayerDetailViewController *single = [JPVideoPlayerDetailViewController new];
//    single.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:single animated:YES];
//    JPVideoPlayerWeiBoEqualHeightCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    single.videoPath = cell.jp_videoURL.absoluteString;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
//    if(self.scrollPlayStrategyType == JPScrollPlayStrategyTypeBestCell){
//        return JPVideoPlayerDemoRowHei;
//    }
//    return JPVideoPlayerDemoRowHei;
//    
//    return indexPath.row % 2 == 0 ? JPVideoPlayerDemoRowHei : (JPVideoPlayerDemoRowHei + 160);
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}

/**
 * Called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
 * 松手时已经静止, 只会调用scrollViewDidEndDragging
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.tableView jp_scrollViewDidEndDraggingWillDecelerate:decelerate];
}

/**
 * Called on tableView is static after finger up if the user dragged and tableView is scrolling.
 * 松手时还在运动, 先调用scrollViewDidEndDragging, 再调用scrollViewDidEndDecelerating
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self.tableView jp_scrollViewDidEndDecelerating];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.tableView jp_scrollViewDidScroll];
}


#pragma mark - JPTableViewPlayVideoDelegate

- (void)tableView:(UITableView *)tableView willPlayVideoOnCell:(JPVideoPlayerWeiBoEqualHeightCell *)cell {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    VideoItem *video = _list[indexPath.row];
    [cell.videoPlayView jp_resumePlayWithURL:[NSURL URLWithString:video.url]
                                     bufferingIndicator:[JPVideoPlayerBufferingIndicator new]
                                            controlView:[[JPVideoPlayerControlView alloc] initWithControlBar:nil blurImage:nil]
                                           progressView:nil
                                          configuration:nil];
    
}


#pragma mark - Setup
- (void)dealloc {
    if (self.tableView.jp_playingVideoCell) {
        [self.tableView.jp_playingVideoCell.jp_videoPlayView jp_stopPlay];
    }
}

- (instancetype)initWithPlayStrategyType:(JPScrollPlayStrategyType)playStrategyType {
    self = [super init];
    if(self){
        _scrollPlayStrategyType = playStrategyType;
    }
    return self;
}

-(UITableView *)tableView{
    return self.table;
}

- (void)setup{
  
}

- (BOOL)shouldShowBlackBackgroundWhenPlaybackStart {
    return YES;
}

- (BOOL)shouldShowBlackBackgroundBeforePlaybackStart {
    return YES;
}

- (BOOL)shouldAutoHideControlContainerViewWhenUserTapping {
    return YES;
}

- (BOOL)shouldShowDefaultControlAndIndicatorViews {
    return NO;
}


@end
