//// Header

#import "VideoListTableViewCell.h"

@implementation VideoListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor  = [UIColor blackColor];
    self.containerView.backgroundColor = [UIColor blackColor];
    
    self.titleLabel.font = [UIFont defaultFontWithSize:15 bold:NO];
    self.titleLabel.textColor = [UIColor whiteColor];
    
    
    [self.shareBtn addTarget:self action:@selector(shareClicked) forControlEvents:UIControlEventTouchUpInside];
    {
        self.videoView.layer.masksToBounds = false;
        self.videoView.layer.shadowOffset = CGSizeMake(0, 2);
        self.videoView.layer.shadowColor = [UIColor grayColor].CGColor;
        self.videoView.layer.shadowOpacity = 1;
        self.videoView.layer.shadowRadius = 4;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.containerView.layer.cornerRadius = 8;
    self.containerView.layer.masksToBounds = false;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 2);
    self.containerView.layer.shadowColor = [UIColor whiteColor].CGColor;
    self.containerView.layer.shadowOpacity = 1;
    self.containerView.layer.shadowRadius = 4;
}

-(void)shareClicked{
    
}

@end
