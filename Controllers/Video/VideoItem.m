//// Header

#import "VideoItem.h"
#import "HCYoutubeParser.h"

@implementation VideoItem
-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.youtubeId = NULL_TO_NIL(dict[@"id"]);
        self.title = dict[@"snippet"][@"title"];
        NSDictionary *thumb = dict[@"snippet"][@"thumbnails"][@"medium"];
        self.thumb =  thumb[@"url"];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            NSDictionary *pars = [HCYoutubeParser h264videosWithYoutubeID:self.youtubeId];
            NSString *URLString = nil;
            if ([pars objectForKey:@"small"] != nil) {
                URLString = [pars objectForKey:@"small"];
            } else if ([pars objectForKey:@"live"] != nil) {
                URLString = [pars objectForKey:@"live"];
            }
            self.url = URLString;
        });
        
    }
    return self;
}
@end
