//// Header

#import "BaseViewController.h"


@interface ShareViewController : BaseViewController

@property (nonatomic, copy) void(^block)(NSInteger type, NSString *shareText);

@end
