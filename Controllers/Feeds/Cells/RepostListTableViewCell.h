//// Header

#import <UIKit/UIKit.h>

@interface RepostListTableViewCell : UITableViewCell{
    NSTimer *_timer;
}
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *avatarBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet UIView *childContainer;

@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIImageView *likeIcon;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleH;

@property (weak, nonatomic) IBOutlet UIView *childTopView;
@property (weak, nonatomic) IBOutlet UIButton *childAvatarBtn;
@property (weak, nonatomic) IBOutlet UIImageView *childAvatarView;
@property (weak, nonatomic) IBOutlet UILabel *childNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *childTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *childTitleText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *childTitleH;
@property (weak, nonatomic) IBOutlet UIImageView *childImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *childImageX;
@property (weak, nonatomic) IBOutlet UIButton *childPlayBtn;

@property (weak, nonatomic) IBOutlet UIButton *gradientBtn;

@property (weak, nonatomic) IBOutlet UIImageView *instagramView;

@property (nonatomic, copy) void(^clickBlock)(NSIndexPath *indexPath, FeedActionType type, UIButton *sender);
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (weak, nonatomic) IBOutlet UIButton *actionBtn;

@property (weak, nonatomic) IBOutlet UIButton *titleBtn;
@property (weak, nonatomic) IBOutlet UIView *statusView;


@end
