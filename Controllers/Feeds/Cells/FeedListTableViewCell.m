//// Header

#import "FeedListTableViewCell.h"

@implementation FeedListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.backgroundColor = [UIColor whiteColor];

    self.avatarView.layer.cornerRadius = 25;
    self.avatarView.clipsToBounds = YES;
    
    self.timeLabel.font = [UIFont defaultFontWithSize:10 bold:NO];
    self.timeLabel.textColor = COLOR(120, 120, 120);
    
    
    self.nameLabel.font = [UIFont defaultFontWithSize:15 bold:YES];

    self.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    
    self.attachView.layer.borderColor = [UIColor grayColor].CGColor;
    self.attachView.layer.borderWidth = 0.4;
    
    UIColor *countColor = COLOR(100, 100, 100);
    self.likeBtn.backgroundColor = [UIColor clearColor];
    self.likeCountLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.likeCountLabel.textColor = countColor;
    
    self.commentBtn.backgroundColor = [UIColor clearColor];
    self.commentLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.commentLabel.textColor = countColor;
    self.shareBtn.backgroundColor = [UIColor clearColor];
    
    self.gradientView.backgroundColor = [UIColor clearColor];
    self.gradientView.clipsToBounds = YES;
    self.playBtn.backgroundColor = [UIColor clearColor];
    
    self.attachView.userInteractionEnabled = YES;
    self.attachView.layer.borderColor  = [UIColor grayColor].CGColor;
    self.attachView.layer.borderWidth = 0.9;
    
    [self.playBtn addTarget:self action:@selector(playClicked) forControlEvents:UIControlEventTouchUpInside];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:    self action:@selector(gradientClicked)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.delaysTouchesEnded = YES;
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:   self action:@selector(animateLike)];
    doubleTap.numberOfTapsRequired = 2;
    [self.containerView addGestureRecognizer:doubleTap];
    
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    [self.gradientView addGestureRecognizer:singleTap];
    
 
    [self.likeBtn addTarget:self action:@selector(likeClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.commentBtn addTarget:self action:@selector(commentClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.actionBtn addTarget:self action:@selector(actionClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.shareBtn addTarget:self action:@selector(shareClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.instagramLikeView.alpha = 0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] init];
    tap.numberOfTapsRequired = 1;
    [tap addTarget:self action:@selector(nameCLicked)];
    self.nameLabel.userInteractionEnabled = YES;
    [self.nameLabel addGestureRecognizer:tap];
    
    [self.avatarBtn addTarget:self action:@selector(avatarClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [StyleManager styleBtn:self.titleBtn titleColor:appBlueColor];
    [self.titleBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.titleBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleBtn.titleLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    self.titleBtn.titleLabel.numberOfLines = 2;
    [self.titleBtn addTarget:self action:@selector(nameCLicked) forControlEvents:UIControlEventTouchUpInside];
    self.nameLabel.hidden = YES;
    self.statusView.layer.cornerRadius = 5;
    
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPress.minimumPressDuration = 1;
    [self.likeBtn addGestureRecognizer:longPress];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    self.containerView.layer.cornerRadius = 8;
    self.containerView.layer.masksToBounds = false;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 0);
    self.containerView.layer.shadowColor = COLOR(150, 150, 150).CGColor;
    self.containerView.layer.shadowOpacity = 0.7;
    self.containerView.layer.shadowRadius = 4;
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        // Create a timer that calls cancel: 2.5 second after the
        // gesture begins (i.e. 3 seconds after the button press if
        // if lpgr.minimumPressDuration = .5;. Pass the gesture
        // recognizer along within the user info dictionary parameter.
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(cancel:) userInfo:[NSDictionary dictionaryWithObjectsAndKeys:gestureRecognizer, @"gestureRecognizer", nil] repeats:NO];
        
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        // Assuming you still want to execute the end code
        // if the user lifts their finger before the 3 seconds
        // is complete, use the same method called in the timer.
        [self cancel:nil];
    }
}

- (void)cancel:(NSTimer*)timerObject {
    NSLog(@"%@",[_timer.userInfo objectForKey:@"gestureRecognizer"]);
    // Get the gesture recognizer from the info dictionary
    UIGestureRecognizer *gestureRecognizer = [_timer.userInfo objectForKey:@"gestureRecognizer"];
    gestureRecognizer.enabled = NO;
    gestureRecognizer.enabled = YES;
    
    // Invalidate the timer
    [_timer invalidate];
    _timer = nil;
    
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeLongPress, nil);
    }
}

- (void) animateLike {
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeDoubleClick, (UIButton *)self.instagramLikeView);
        self.clickBlock(self.indexPath, FeedActionTypeLike, self.likeBtn);
    }
    
}
-(void)nameCLicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeName, self.avatarBtn);
    }
}

-(void)avatarClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeAvatar, self.avatarBtn);
    }

}

-(void)playClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeVideo, self.playBtn);
    }
}

-(void)gradientClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeImage, self.gradientView);
    }
}

-(void)likeClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeLike, self.likeBtn);
    }
}

-(void)commentClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeComment, self.commentBtn);
    }
}

-(void)actionClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeAction, self.actionBtn);
    }
}

-(void)shareClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeShare, self.shareBtn);
    }
}



@end
