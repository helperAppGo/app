//// Header

#import "FeedTitleView.h"

@implementation FeedTitleView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.iconView.image =  [UIImage imageNamed:@"bottomArrow"];
}
-(CGSize)intrinsicContentSize{
   return CGSizeMake(150, 35);
}


@end
