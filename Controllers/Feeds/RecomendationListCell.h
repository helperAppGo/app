//// Header

#import <UIKit/UIKit.h>

@interface RecomendationListCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource>{
    float _w;
}

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionH;
@property (nonatomic, strong) NSArray *items;

@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, copy) void (^clickBlock)(NSInteger type, NSInteger row);

@end
