//
//  PlayerViewController.h
//  RssFeed
//
//  Created by Jurayev Nodir on 12/11/15.
//  Copyright (c) 2015 Jurayev Nodir. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

@interface PlayerViewController : MPMoviePlayerViewController

@end
