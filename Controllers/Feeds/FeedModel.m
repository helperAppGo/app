//// Header

#import "FeedModel.h"
#import "UIFont+Additions.h"

@implementation FeedMediaObject
-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.height = [NULL_TO_NIL(dict[@"height"]) floatValue];
        self.width = [NULL_TO_NIL(dict[@"width"]) floatValue];
        if (self.height == 0 || self.width == 0) {
            return nil;
        }
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.preview = NULL_TO_NIL(dict[@"preview"]);
        self.type = NULL_TO_NIL(dict[@"type"]);
        self.path = NULL_TO_NIL(dict[@"path"]);
        self.h = [self viewH:NO];
        self.childH = [self viewH:YES];
        
        self.videoK = MAX(self.width, self.height)/MIN(self.width, self.height);
        self.videoH = [[UIScreen mainScreen] bounds].size.width / self.videoK;
        self.childiVideoH = ([[UIScreen mainScreen] bounds].size.width - 20) / self.videoK;
        
    }
    return self;
}

-(NSString *)imgUrl{
    if ([self.type isEqualToString:@"image"]) {
        return self.path;
    }
    if ([self.type isEqualToString:@"video"]) {
        return self.preview;
    }
    if ([self.type isEqualToString:@"youtube"]) {
        return self.preview;
    }
    return self.path;
}

-(float)viewH:(BOOL)child{
    BOOL horizontal = self.width > self.height;
    float k = child ? 20 : 0;
    float deviceWidth = [[UIScreen mainScreen] bounds].size.width - k;
    if (horizontal) {
       return deviceWidth * (self.height/self.width);
    }
    
    return deviceWidth * (self.height/self.width);
}
@end

@implementation FeedModel

-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.repost = NULL_TO_NIL(dict[@"repost"]) ? 1 : 0;
        NSDictionary *repostDict = NULL_TO_NIL(dict[@"repost"]);
        if (repostDict) {
            self.repost = 1;
            self.repostFeed = [[FeedModel alloc] initWithDict:repostDict];
        }
        
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.wallId = [NULL_TO_NIL(dict[@"id_wall"]) integerValue];
        self.files = [NULL_TO_NIL(dict[@"files"]) integerValue];
        self.userId = [NULL_TO_NIL(dict[@"id_user"]) integerValue];
        
        self.commentsCount = [NULL_TO_NIL(dict[@"count_comments"]) integerValue];
        self.likesCount = [NULL_TO_NIL(dict[@"count_likes"]) integerValue];
        self.liked = [NULL_TO_NIL(dict[@"is_liked"]) integerValue];
        
        self.text = NULL_TO_NIL(dict[@"text"]);
        self.name = NULL_TO_NIL(dict[@"name"]);
        self.pathAvatar = NULL_TO_NIL(dict[@"path_ava"]);
    
        
        NSArray *items = [NSArray arrayWithArray:NULL_TO_NIL(dict[@"file_objects"])];
        NSMutableArray *objects = @[].mutableCopy;
        for (NSDictionary *mediaDict in items) {
            FeedMediaObject *media = [[FeedMediaObject alloc] initWithDict:mediaDict];
            if (media) {
                [objects addObject:media];
            }
            
        }
        self.mediaObjects = objects;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        NSDate *date = [formatter dateFromString:NULL_TO_NIL(dict[@"time"])];
        self.createDate = date;
        
        formatter.dateFormat = @"HH:mm";
        self.timeString = [formatter stringFromDate:self.createDate];
        
        formatter.dateFormat = @"dd-MM-yyyy";
        self.dateString = [formatter stringFromDate:self.createDate];
        self.dateFullString = [NSString stringWithFormat:@"%@ %@",self.dateString,self.timeString];
    
        self.dict = dict;
    }
    return self;
}

-(NSString *)likeCountString{
    return [NSString stringWithFormat:@"%@",@(self.likesCount)];
}

-(NSString *)commentCountString{
    return [NSString stringWithFormat:@"%@",@(self.commentsCount)];
}

-(UIImage *)likeIcon{
    return self.liked ? [UIImage imageNamed:@"postLiked"] : [UIImage imageNamed:@"postUnlike"];
}

-(BOOL)isEqual:(FeedModel *)object{
    return self.objId == object.objId;
}

@end
