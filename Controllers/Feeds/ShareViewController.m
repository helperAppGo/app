//// Header

#import "ShareViewController.h"
#import "SAMTextView.h"

@interface ShareViewController ()
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet SAMTextView *textView;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UILabel *leftLabel;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;





@end

@implementation ShareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[SettingsManager instance] anySound];
    
    self.btn.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
    [self.btn addTarget:self action:@selector(dissmiss) forControlEvents:UIControlEventTouchUpInside];
    self.containerView.layer.cornerRadius = 8;
    self.containerView.backgroundColor = [UIColor whiteColor];
    self.containerView.clipsToBounds = YES;
    
    self.topView.backgroundColor = COLOR(240, 240, 240);
    self.topLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.topLabel.text = trShare;
    
    self.textView.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textView.placeholder = trShareQ;
    
    self.bottomView.backgroundColor = self.topView.backgroundColor;
    self.leftBtn.backgroundColor = [UIColor clearColor];
    self.leftLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.leftLabel.text = trCancel;
    
    self.rightBtn.backgroundColor = [UIColor clearColor];
    self.rightLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.rightLabel.text = trReady;
    
    
    
    
    [self.rightBtn addTarget:self action:@selector(rightClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.leftBtn addTarget:self action:@selector(leftClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}

-(void)dissmiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)rightClicked{
    [self.view endEditing:YES];
    if (self.block) {
        self.block(1, self.textView.text);
    }
    [self dissmiss];
}

-(void)leftClicked{
    [self dissmiss];
}

@end
