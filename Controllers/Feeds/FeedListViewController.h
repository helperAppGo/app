//// Header

#import "BaseFeedViewController.h"

@interface FeedListViewController : BaseFeedViewController

-(void)pushNext;
-(void)localNext;
-(void)contactNext;

@end
