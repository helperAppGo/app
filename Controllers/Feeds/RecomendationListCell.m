//// Header

#import "RecomendationListCell.h"
#import "RecomendationCollectionViewCell.h"
@implementation RecomendationListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.titleLabel.text = trFeedRecomendation;
    
    self.contentView.backgroundColor = COLOR(240, 240, 240);
    UICollectionViewFlowLayout * _verticalLayout = [[UICollectionViewFlowLayout alloc] init];
    _verticalLayout.minimumInteritemSpacing = 10;
    _verticalLayout.minimumLineSpacing = 10;
    _verticalLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _verticalLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    float w = [UIScreen mainScreen].bounds.size.width;
    if (w > 320) {
        self.collectionH.constant = 275;
        _w = 180;
        _verticalLayout.itemSize = CGSizeMake(180, 265);
        
    } else{
        self.collectionH.constant = 160;
        _w = 90;
        _verticalLayout.itemSize = CGSizeMake(90, 140);
    }
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = self.contentView.backgroundColor;
    [self.collectionView setCollectionViewLayout:_verticalLayout animated:YES];
    
    UINib *nib = [UINib nibWithNibName:@"RecomendationCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"list"];
    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView reloadData];
    
}
-(void)setItems:(NSArray *)items{
    _items = items;
    [self.collectionView reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecomendationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"list" forIndexPath:indexPath];
    cell.containerView.backgroundColor = [UIColor whiteColor];
    UserObj *user = _items[indexPath.row];
    cell.nameLabel.text = [user nameString];
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.cityLabel.text =[NSString stringWithFormat:@"%@:%@",trCity, [user cityString]];
    [StyleManager styleBtn:cell.relationBtn title:[user relationString]];
    [StyleManager styleBtn:cell.relationBtn titleColor:[UIColor grayColor]];
    [cell.relationBtn setTitleColor:appBlueColor forState:UIControlStateNormal];
    cell.relationBtn.titleLabel.font = cell.relationLabel.font;
    
    cell.relationLabel.hidden = YES;
    __weak typeof(self) weakSelf = self;
    cell.indexPath = indexPath;
    [cell setClickBlock:^(NSInteger type, NSInteger row) {
        [weakSelf cellClicked:type index:row];
    }];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float h = self.collectionView.frame.size.height;
    return CGSizeMake(_w, h);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    if (self.clickBlock) {
        self.clickBlock(0, indexPath.row);
    }
}

-(void)cellClicked:(NSInteger)type index:(NSInteger)index{
    if (self.clickBlock) {
        self.clickBlock(type, index);
    }
}
@end
