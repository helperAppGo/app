//// Header

#import "FeedListViewController.h"
#import "FeedListTableViewCell.h"
#import "GBCommentsViewController.h"
#import "HCYoutubeParser.h"
#import "PlayerViewController.h"
#import "RepostListTableViewCell.h"
#import "LoaderCell.h"
#import "RecomendationListCell.h"
#import "ShareViewController.h"
#import "JPVideoPlayerWeiBoListViewController.h"
#import "FTPopOverMenu.h"
#import "FeedTitleView.h"
#import "GroupNavView.h"

@interface FeedListViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *_list;
    UIRefreshControl *_refreshControl;
    BOOL _complete;
    NSMutableArray *_recomendationList;
    FeedModel *_topModel;
    RecomendationListCell *_recomendationCell;
    
    FeedTitleView *_topView;
    GroupNavView *_navView;
    NSArray *_filterArray;
    NSArray *_filterTypes;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;



@end

@implementation FeedListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel.text = trFeedTitle;
    _recomendationList = @[].mutableCopy;
    _filterArray =  @[trFilterNews,trFilterFriend,trFilterGroup,trFilterNearby,trFilterVideo,trFilterRecomendation];
    _filterTypes  = @[@"", @"friends", @"publics", @"geo", @"video", @"recommendation"];
//    передаем type:
//    publics, группы
//    friends, друзья
//    recommendation, рекомендации
//    geo , рядом
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UINib *nib = [UINib nibWithNibName:@"FeedListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"RepostListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"list2"];
    
    UINib *loaderNib = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:loaderNib forCellReuseIdentifier:@"loader"];
    
    UINib *recNib = [UINib nibWithNibName:@"RecomendationListCell" bundle:nil];
    [self.tableView registerNib:recNib forCellReuseIdentifier:@"rec"];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(getNow) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:_refreshControl];
    _list = @[].mutableCopy;
    [self getRecommendation];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateNotification) name:@"updateBadge" object:nil];

//    _navView = [GroupNavView loadFromNib];
//    [_navView.heightAnchor constraintEqualToConstant:44].active  = YES;
    
    _topView = [FeedTitleView loadFromNib];
    [_topView.heightAnchor constraintEqualToConstant:44].active = YES;
    self.navigationItem.titleView = _topView;
    
    _topView.backgroundColor = [UIColor clearColor];
    _topView.containerBtn.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self     action:@selector(openFilter:)];
    _topView.containerBtn.userInteractionEnabled = YES;
    [_topView.containerBtn addGestureRecognizer:tapGesture];
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.isQuestion = NO;
    configuration.menuWidth = 200;
    configuration.textColor = [UIColor blackColor];
    configuration.textFont = [UIFont defaultFontWithSize:12 bold:NO];
    configuration.tintColor = [COLOR(190, 190, 190) colorWithAlphaComponent:0.5];
    configuration.borderWidth = 0;
}

#pragma mark - Filter methods
-(void)openFilter:(UITapGestureRecognizer *)gesture{
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.isQuestion = NO;
    
    [UIView animateWithDuration:0.5 animations:^{
        _topView.iconView.image = [UIImage imageNamed:@"topArrow"];
    }];
    [FTPopOverMenu showForSender:_topView.iconView
                   withMenuArray:_filterArray
                      imageArray:nil
                       doneBlock:^(NSInteger selectedIndex) {
                           NSInteger old = [FTPopOverMenuConfiguration defaultConfiguration].selectIndex;
                           if (old == selectedIndex) {
                               return;
                           }
                           [FTPopOverMenuConfiguration defaultConfiguration].selectIndex = selectedIndex;
                           [self getNow];
                           [UIView animateWithDuration:0.5 animations:^{
                               _topView.iconView.image = [UIImage imageNamed:@"bottomArrow"];
                               NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].selectIndex;
                               _topView.titleLabel.text = _filterArray[index];
                           }];
                           
                       } dismissBlock:^{
                           [UIView animateWithDuration:0.5 animations:^{
                               _topView.iconView.image = [UIImage imageNamed:@"bottomArrow"];
                               NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].selectIndex;
                               _topView.titleLabel.text = _filterArray[index];
                           }];
                       }];
}

-(void)getRecommendation{
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(_recomendationList.count)};
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"users/friends/recommendation" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            response = NULL_TO_NIL(response[@"response"]);
            NSMutableArray *items = @[].mutableCopy;
            for (NSDictionary *userDict in response) {
                UserObj *user = [UserObj createUser:userDict];
                [items addObject:user];
            }
            [_recomendationList addObjectsFromArray:items];
            [weakSelf.tableView reloadData];
        }
        
    }];

}

-(void)getNow{
    NSDictionary *dict = @{@"offset":@(0),
                           @"limit":@(30),
                           @"expand":@"files"};
    
    NSInteger filterIndex = [FTPopOverMenuConfiguration defaultConfiguration].selectIndex;
    NSString  *path  = @"posts/feed";
    if (filterIndex > 0) {
        dict = @{@"offset":@(0),
                 @"limit":@(30),
                 @"expand":@"files",
                 @"type":_filterTypes[filterIndex]};
    }
    if ([FTPopOverMenuConfiguration defaultConfiguration].selectIndex == 4) {
        path = @"posts/video";
        dict = @{@"offset":@(0),
                 @"limit":@(30)};
                 
    }
    
    
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:path params:params callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            [_list removeAllObjects];
            NSArray *items = response[@"response"];
            for (NSDictionary * item in items) {
                FeedModel *model = [[FeedModel alloc] initWithDict:item];
                [_list addObject:model];
            }
            if (_list.count) {
                _topModel = _list[0];
                [_list removeObjectAtIndex:0];
            }
            
            _complete = items.count < 30;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];

}

-(void)getList{
    if (_complete) {
        return;
    }
    
    NSDictionary *dict = @{@"offset":@(_list.count),
                           @"limit":@(30),
                           @"expand":@"files"};
    NSInteger filterIndex = [FTPopOverMenuConfiguration defaultConfiguration].selectIndex;
    NSString  *path  = @"posts/feed";
    if (filterIndex > 0) {
        dict = @{@"offset":@(_list.count),
                 @"limit":@(30),
                 @"expand":@"files",
                 @"type":_filterTypes[filterIndex]};
    }
    if ([FTPopOverMenuConfiguration defaultConfiguration].selectIndex == 4) {
        path = @"posts/video";
        dict = @{@"offset":@(_list.count),
                 @"limit":@(30)};
        
    }
    
    
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    
    [[ApiManager sharedManager] getRequest:path params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary * item in items) {
                FeedModel *model = [[FeedModel alloc] initWithDict:item];
                [_list addObject:model];
            }
            if (_topModel == nil && _list.count) {
                _topModel = _list[0];
                [_list removeObjectAtIndex:0];
            }
            _complete = items.count < 30;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];

    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateNotification];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_notification"] style:UIBarButtonItemStyleDone target:self action:@selector(openNotifications)];
    item.tintColor = appBlueColor;
    self.navigationItem.leftBarButtonItem = item;
    [self updateNotification];
    [self showPushPermission];
    NSInteger k = [[[SettingsManager instance] objForKey:@"localPermission"] integerValue];
    if (k==1) {
        [GBLocationManager sharedManager];
    }
    {
        UIImage *image = [[UIImage imageNamed:@"youtube_feed"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:image style:UIBarButtonItemStyleDone target:self action:@selector(openVideo)];
//        item.tintColor = []
        self.navigationItem.rightBarButtonItem = item;
        
    }
    
    
    
    
}
-(void)openVideo{
    BaseViewController  *viewController = [self createController:@"JPVideoPlayerWeiBoListViewController"];
    //[[JPVideoPlayerWeiBoListViewController alloc] initWithPlayStrategyType:JPScrollPlayStrategyTypeBestVideoView];
    viewController.type = 1;
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
    
}

-(void)showPushPermission{
    NSInteger k = [[[SettingsManager instance] objForKey:@"pushPermission"] integerValue];
    if (k ==  1) {
        [self pushNext];
        return;
    }
    BaseViewController *controller = [self createController:@"PermissionViewController"];
    controller.info = @{@"type":@"push"};
    controller.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.preferredContentSize = CGSizeMake(self.view.width, self.view.height);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    });
    
}

-(void)showLocalPermission{
    NSInteger k = [[[SettingsManager instance] objForKey:@"localPermission"] integerValue];
    if (k ==  1) {
        [self localNext];
        return;
    }
    BaseViewController *controller = [self createController:@"PermissionViewController"];
    controller.info = @{@"type":@"location"};
    controller.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.preferredContentSize = CGSizeMake(self.view.width, self.view.height);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    });
}

-(void)pushNext{
    [[SettingsManager instance] saveObject:@(1) key:@"pushPermission"];
    [self performSelector:@selector(showLocalPermission) withObject:nil afterDelay:0.4];
}

-(void)localNext{
    [[SettingsManager instance] saveObject:@(1) key:@"localPermission"];
    [self performSelector:@selector(showContactsPermission) withObject:nil afterDelay:0.4];
}

-(void)contactNext{
    [[SettingsManager instance] saveObject:@(1) key:@"contactPermission"];
    [(AppDelegate *)[[UIApplication sharedApplication] delegate] activeActions];
}

-(void)showContactsPermission{
    NSInteger k = [[[SettingsManager instance] objForKey:@"contactPermission"] integerValue];
    if (k ==  1) {
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] activeActions];
        return;
    }
    BaseViewController *controller = [self createController:@"PermissionViewController"];
    controller.info = @{@"type":@"contact"};
    controller.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.preferredContentSize = CGSizeMake(self.view.width, self.view.height);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    });
    
}


-(void)updateNotification{
    
    NSInteger count = [[[SettingsManager instance] objForKey:@"notification1"] integerValue];
    if ([UIDevice currentDevice].systemVersion.doubleValue >= 11) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self addBadge:self.navigationItem.leftBarButtonItem count:count];
        });
        return;
    }
    [self addBadge:self.navigationItem.leftBarButtonItem count:count];
}


-(void)openNotifications{
    BaseViewController *controller = [self createController:@"NotificationListViewController"];
    controller.type = 1;
    controller.info = @{@"type":@"user"};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UITableView *)table{
    return self.tableView;
}

#pragma mark - Action methods
-(void)feedClicked:(NSIndexPath *)indexPath action:(FeedActionType)type sender:(UIButton *)sender{
    FeedModel *model;
    if (indexPath.section == 0) {
        model = _topModel;
    } else{
        model = _list[indexPath.row];
    }
    [self feedAction:indexPath model:model action:type sender:sender];
}

-(NSMutableArray *)feedItems{
    return _list;
}

-(void)deletePost:(FeedModel *)model{
    
    NSDictionary *params = @{@"data":[@{@"id":@(model.objId)} convertToJsonString]};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] deleteRequest:@"posts" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            if ([model isEqual:_topModel]) {
                _topModel = [_list firstObject];
                [_list removeObjectAtIndex:0];
            } else{
                [_list removeObject:model];
            }
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)reportPost:(FeedModel *)model{
    NSDictionary *params = @{@"id_post":@(model.objId),
                             @"type":@(0)};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] postRequest:@"posts/report" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            if ([model isEqual:_topModel]) {
                _topModel = [_list firstObject];
                [_list removeObjectAtIndex:0];
            } else{
                [_list removeObject:model];
            }
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)addRecomendationFriend:(UserObj *)user tableView:(UITableView *)tableView{
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    NSDictionary  *params = @{@"id_user":@(user.objId)};
    [[ApiManager sharedManager] postRequest:@"users/friends" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            user.relation = 2;
            [_recomendationList removeObject:user];
            
            if (_recomendationList.count) {
                _recomendationCell.items = _recomendationList;
            } else{
                NSIndexPath *p = [NSIndexPath indexPathForRow:1 inSection:0];
                [tableView deleteRowsAtIndexPaths:@[p] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}


-(void)recomendationClicked:(NSInteger)type index:(NSInteger)row{
    UserObj *user = _recomendationList[row];
    if (type == 0) {
        // open user profile
        [self openUserProfile:user];
    }
    
    if (type == 1) {
        // relationClicked
        [self changeRelation:user tableView:self.tableView index:[NSIndexPath indexPathForRow:row inSection:0]];
    }
    
    if (type == 2) {
        // delete clicked
    }
}

-(void)banUser:(FeedModel *)post{
    NSDictionary *params = @{@"data":[@{@"id_user":@(post.userId)} convertToJsonString]};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] postRequest:@"users/friends/ban" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            if ([_topModel isEqual:post]) {
                _topModel = nil;
            }
            NSInteger userId = post.userId;
            for (FeedModel *model in _list) {
                if (model.userId == userId) {
                    [_list removeObject:model];
                }
            }
            if (_list.count && (_topModel == nil)) {
                _topModel = [_list objectAtIndex:0];
                [_list removeObjectAtIndex:0];
            }
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}


#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        if (_recomendationList.count) {
            return _topModel ? 2 : 1;
        }
        return _topModel ? 1 : 0;
    }
    if (_complete) {
        return _list.count;
    }
    if (_list.count > 0) {
        return _list.count + 1;
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 50;
    }
    
    if (indexPath.row == _list.count) {
        return 50;
    }
    if (_list.count > 0) {
        FeedModel *model = _list[indexPath.row];
        if (model.mediaObjects.count) {
            return 100;
        }
    }
    
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        if (_topModel && indexPath.row == 0) {
            return [self feedCell:_topModel indexPath:indexPath tableView:tableView];
        } else{
            RecomendationListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rec"];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setItems:_recomendationList];
            __weak typeof(self) weakSelf = self;
            [cell setClickBlock:^(NSInteger type, NSInteger row) {
                [weakSelf recomendationClicked:type index:row];
            }];
            _recomendationCell = cell;
            return cell;
        }
        
    }
    if (indexPath.row == _list.count) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        }
        return cell;
    }
    
    FeedModel *model = _list[indexPath.row];
    return [self feedCell:model indexPath:indexPath tableView:tableView];
}



-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        [self getList];
    }
}




@end
