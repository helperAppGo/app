//// Header

#import "FilterBtnTableViewCell.h"

@implementation FilterBtnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btnView.backgroundColor  =COLOR(247, 247, 247);
    self.btnView.layer.cornerRadius =  15;
    self.label.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.label.textColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setIsSelect:(NSInteger)isSelect{
    _isSelect = isSelect;
    if (_isSelect) {
        [self selectMode];
    } else{
        [self unselectMode];
    }
}

-(void)selectMode{
    self.btnView.backgroundColor = appBlueColor;
    self.label.textColor = [UIColor whiteColor];
}

-(void)unselectMode{
    self.btnView.backgroundColor = COLOR(247, 247, 247);
    self.label.textColor = [UIColor blackColor];
}

@end
