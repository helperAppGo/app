//// Header

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface FeedMediaObject : NSObject

@property (nonatomic, assign) float width;
@property (nonatomic, assign) float height;
@property (nonatomic, assign) float h;
@property (nonatomic, assign) float childH;
@property (nonatomic, assign) float videoK;
@property (nonatomic, assign) float videoH;
@property (nonatomic, assign) float childiVideoH;
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSString *preview;
@property (nonatomic, strong) NSString *type;

-(NSString *)imgUrl;

-(id)initWithDict:(NSDictionary *)dict;

@end


@interface FeedModel : NSObject

@property (nonatomic, assign) NSInteger repost;
@property (nonatomic, strong) FeedModel *repostFeed;
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger commentsCount;
@property (nonatomic, assign) NSInteger likesCount;
@property (nonatomic, assign) NSInteger files;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) NSInteger wallId;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger liked;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *pathAvatar;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSDate   *createDate;

@property (nonatomic, strong) NSString *timeString;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSString *dateFullString;

@property (nonatomic, strong) NSArray *mediaObjects;
@property (nonatomic, strong) NSDictionary *dict;

-(NSString *)likeCountString;
-(NSString *)commentCountString;
-(UIImage *)likeIcon;

-(id)initWithDict:(NSDictionary *)dict;

@end
