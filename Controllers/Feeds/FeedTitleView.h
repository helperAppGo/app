//// Header

#import <UIKit/UIKit.h>

@interface FeedTitleView : UIView
@property (weak, nonatomic) IBOutlet UIButton *containerBtn;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIView *btn;

@end
