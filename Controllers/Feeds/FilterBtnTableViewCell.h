//// Header

#import <UIKit/UIKit.h>

@interface FilterBtnTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *btnView;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (nonatomic, assign) NSInteger isSelect;
@end
