//// Header
#import "FriendsPageViewController.h"

@interface FriendsPageViewController () <KHTabPagerDataSource, KHTabPagerDelegate>{
    NSArray *_categoryList;
    NSInteger _catId;
    
    NSMutableDictionary *_allCategoryItems;
}


@property (assign, nonatomic) BOOL isProgressive;

@end

@implementation FriendsPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    _titleLabel.text = trFriends;
    _categoryList = self.info[@"items"];
    _catId = [self.info[@"categoryId"] integerValue];
    _allCategoryItems = @{}.mutableCopy;
    self.isProgressive = YES;
    self.dataSource = self;
    self.delegate = self;
}

-(void)getAllItems{

}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)switchProgressive {
    //    [self.navigationItem.rightBarButtonItem setTitle: self.isProgressive ?  @"Not Progressive" : @"Progressive" ];
    //    self.isProgressive = !self.isProgressive;
}

#pragma mark - KHTabPagerDataSource

- (NSInteger)numberOfViewControllers {
    return 3;
}

- (UIViewController *)viewControllerForIndex:(NSInteger)index {
    BaseViewController *controller = [self createController:@"MyFriendsListViewController"];
    controller.info = @{@"type":@(index)};
    return controller;
}


- (NSString *)titleForTabAtIndex:(NSInteger)index {
    return @[trFriends, trRequests, trOutgoing][index];
    
}

- (CGFloat)tabHeight {
    return 40.0f;
}

- (CGFloat)tabBarTopViewHeight {
    return 0.0f;
}

- (UIView *)tabBarTopView {
    return nil;
    //    UIView *view = [[[NSBundle mainBundle] loadNibNamed:@"tabBarTopView" owner:self options:nil] objectAtIndex:0];
    //    view.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y , self.view.frame.size.width, view.frame.size.height);
    //    view.autoresizingMask = UIViewAutoresizingNone;
    //    return view;
}

- (UIColor *)tabColor {
    return [UIColor redColor];
}

-(UIColor *)tabBackgroundColor {
    return [UIColor whiteColor];
}

-(UIColor *)titleColor {
    return COLOR(200, 200, 200);
}

-(UIFont *)titleFont {
    return [UIFont defaultFontWithSize:14 bold:NO];
}

-(BOOL)isProgressiveTabBar{
    return self.isProgressive;
}

#pragma mark - Tab Pager Delegate

- (void)tabPager:(KHTabPagerViewController *)tabPager willTransitionToTabAtIndex:(NSInteger)index {
}

- (void)tabPager:(KHTabPagerViewController *)tabPager didTransitionToTabAtIndex:(NSInteger)index {
}

@end
