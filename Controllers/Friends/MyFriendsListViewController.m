//// Header

#import "MyFriendsListViewController.h"
#import "UserListTableViewCell.h"
#import "LoaderCell.h"


@interface MyFriendsListViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *_list;
    UIRefreshControl *_refreshControl;
    NSInteger _userId;
    BOOL _complete;
    NSInteger _friendType;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation MyFriendsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CDUser *user = [[SettingsManager instance] currentUser];
    _userId = user.objId;
    
    _friendType = [self.info[@"type"] integerValue];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UINib *nib = [UINib nibWithNibName:@"UserListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *loaderNib = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:loaderNib forCellReuseIdentifier:@"loader"];
    
    _list = @[].mutableCopy;
    _userId = [self.info[@"userId"] integerValue];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:_refreshControl];
    
    [_refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    
}

//users/friends/requested  GET
//users/friends/requesting  GET

-(NSDictionary *)startRequestParams{
    NSDictionary *params = @{@"id_user":@(_userId), @"limit":@(20), @"offset":@(0), @"random":@(0)};
    return params;
}

-(NSString *)requestPath{
    
    if (_friendType == 0) {
        return @"users/friends/list";
    }
    
    if (_friendType == 1) {
        return @"users/friends/requested";
    }
    return  @"users/friends/requesting";
}

-(void)refresh{
    NSDictionary *params = [self startRequestParams];
    NSString *str = [params convertToJsonString];
    __weak typeof(self) weakSelf = self;
    _complete = NO;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:[self requestPath] params:@{@"data":str} callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            NSArray *items = [NSArray arrayWithArray:response[@"response"]];
            NSMutableArray *userList = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [userList addObject:user];
            }
            _complete = userList.count < 20;
            [_list setArray:userList];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getList{
    if (_complete) {
        return;
    }
    NSDictionary *params = @{@"id_user":@(_userId), @"limit":@(20), @"offset":@(_list.count), @"random":@(0)};
    NSString *str = [params convertToJsonString];
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:[self requestPath] params:@{@"data":str} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = [NSArray arrayWithArray:response[@"response"]];
            NSMutableArray *userList = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [userList addObject:user];
            }
            _complete = (userList.count < 20);
            [_list addObjectsFromArray:userList];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)relationClicked:(UIButton *)sender{
    UserObj *user = _list[sender.tag];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    [self changeRelation:user tableView:self.tableView index:indexPath];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(_complete){
        return _list.count;
    }
    return _list.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        }
        return cell;
    }
    UserListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserObj *user = _list[indexPath.row];
    cell.statusView.backgroundColor = appStatusColor(user.online);
    cell.actionBtn.tag = indexPath.row;
    [cell.actionBtn addTarget:self action:@selector(relationClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.nameLabel.text = [user nameString];
    cell.actionLabel.text = [user relationString];
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UserObj *user = _list[indexPath.row];
    [self openUserProfile:user];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        [self getList];
    }
}

@end
