//// Header

#import "RegisterTableViewCell.h"

@implementation RegisterTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.field.layer.backgroundColor = [UIColor whiteColor].CGColor;
    self.field.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.field.placeholderColor = appGreyTextColor;
    self.field.textAlignment  = NSTextAlignmentLeft;
    self.field.floatingLabelActiveTextColor = appGreyTextColor;
    self.field.autocorrectionType = UITextAutocorrectionTypeNo;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
