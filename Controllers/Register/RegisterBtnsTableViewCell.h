//// Header

#import <UIKit/UIKit.h>

@interface RegisterBtnsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentView;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end
