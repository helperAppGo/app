//// Header

#import "RegisterUserViewController.h"
#import "GBChooseCityViewController.h"
#import "CityListViewController.h"
#import "RegisterTableViewCell.h"
#import "RegisterBtnsTableViewCell.h"


@interface RegisterUserViewController ()<UITableViewDelegate,  UITableViewDataSource, UITextFieldDelegate>{
    NSArray *_placeHolders;
    NSString *_lastName;
    NSString *_firstName;
    NSInteger _cityId;
    NSInteger _genderIndex;
    NSString *_cityName;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation RegisterUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _placeHolders = @[trSignLastname, trSignFirstname, trCity];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource =  self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UINib *nib = [UINib nibWithNibName:@"RegisterTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"RegisterBtnsTableViewCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"btn"];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openCityList{
    CityListViewController *controller = (CityListViewController *)[self createController:@"CityListViewController"];
    controller.info = @{@"id":@(_cityId)};
    controller.type = 1;
    __weak typeof(self) weakSelf = self;
    [controller setClickBlock:^(NSInteger objId) {
        [weakSelf changeCityId:objId];
    }];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeCityId:(NSInteger)cityId{
    _cityId = cityId;
    CDCity *city = [CDCity find:@{@"objId":@(cityId)}];
    _cityName = city.name;
    [self.tableView reloadData];
}

//- (void) chooseCity
//{
//    WEAKSELF_DECLARATION
//    GBChooseCityViewController *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"GBChooseCityViewController"];
//    [vc configureWithSelectedCityId:self.city.objId completion:^(id value) {
//        STRONGSELF_DECLARATION
//        strongSelf.city = value;
//    }];
//    [self.navigationController pushViewController:vc animated:YES];
//}


/*
 [parameters setObject:self.nameField.text forKey:@"first_name"];
 [parameters setObject:self.surnameField.text  forKey:@"last_name"];
 [parameters setObject:@(self.city.objId) forKey:@"id_city"];
 [parameters setObject:@(self.sexSegmentedControl.selectedSegmentIndex) forKey:@"sex"];
 
*/

-(void)changeGender:(UISegmentedControl *)control{
    _genderIndex = control.selectedSegmentIndex;
}

-(void)send{
    [self.view endEditing:YES];
 
    if (_firstName.length == 0) {
        [self showErrorMessage:trSignLastnameError];
        return;
    }
    if (_lastName.length == 0) {
        [self showErrorMessage:trSignFirstname];
        return;
    }
    if (_cityId == 0) {
        [self showErrorMessage:trSignCityError];
        return;
    }
    NSMutableDictionary *parameters = @{}.mutableCopy;
    parameters[@"first_name"]  = _firstName;
    parameters[@"last_name"]  = _lastName;
    parameters[@"id_city"]  = @(_cityId);
    parameters[@"sex"]  = @(_genderIndex);
    __weak typeof(self) weakSelf = self;
    [weakSelf showLoader];
    [[ApiManager sharedManager] patchRequest:@"users/self" params:parameters callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf getUser];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getUser{
    CDUser *user = [[SettingsManager instance] currentUser];
    NSInteger userId = user.objId;
    NSDictionary *params= @{@"data":@(userId)};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [[SettingsManager instance] saveObject:@(0) key:@"uncompleteRegister"];
            response = response[@"response"];
            [CDUser currentUserUpdate:response];
            [(AppDelegate *)[[UIApplication sharedApplication] delegate] openTabbar];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

#pragma mark -  UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return _placeHolders.count;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        RegisterBtnsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"btn"];
        [cell.segmentView addTarget:self action:@selector(changeGender:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btn addTarget:self action:@selector(send) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    
    RegisterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.topH.constant = indexPath.row == 0 ? 100 : 0;
    cell.field.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSString *placeholder = _placeHolders[indexPath.row];
    [cell.field setPlaceholder:placeholder floatingTitle:placeholder];
    
    cell.field.enabled = indexPath.row != 2;
    cell.accessoryType = indexPath.row == 2 ? UITableViewCellAccessoryDisclosureIndicator : UITableViewCellAccessoryNone;
    
    if (indexPath.row == 2) {
        cell.field.text = _cityName;
    }
    if (indexPath.row == 0) {
        cell.field.text = _lastName;
    }
    if (indexPath.row == 1) {
        cell.field.text = _firstName;
    }
    
    cell.field.delegate = self;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row ==  2) {
        [self openCityList];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 0) {
        _lastName = textField.text;
    }
    if (textField.tag == 1) {
        _firstName = textField.text;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.view  endEditing:YES];
    return YES;
}

@end
