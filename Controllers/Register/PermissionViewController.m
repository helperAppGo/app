//// Header

#import "PermissionViewController.h"

@interface PermissionViewController (){
    NSString *_permisionType;
}
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (weak, nonatomic) IBOutlet UILabel *centerLabel;
@end

@implementation PermissionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _permisionType  = self.info[@"type"];
    if ([_permisionType isEqualToString:@"push"]) {
        self.topLabel.text = trPermPushTop;
        self.centerLabel.text = trPermPushCenter;
        [StyleManager styleBtn:self.btn title:trPermOK];
    }
    
    if ([_permisionType isEqualToString:@"contact"]) {
        self.topLabel.text = trPermContactTop;
        self.centerLabel.text = nil;
        [StyleManager styleBtn:self.btn title:trPermOK];
    }
    
    if ([_permisionType isEqualToString:@"location"]) {
        self.topLabel.text = trPermLocationTop;
        self.centerLabel.text = trPermLocationCenter;
        [StyleManager styleBtn:self.btn title:trPermOK];
    }
    self.topLabel.font = [UIFont defaultFontWithSize:25 bold:NO];
    self.centerLabel.font = [UIFont defaultFontWithSize:25 bold:NO];
    self.topLabel.textColor = self.centerLabel.textColor = [UIColor whiteColor];
    
    self.btn.layer.cornerRadius = 6;
    [StyleManager styleBtn:self.btn bgImage:[UIImage imageNamed:@"permissionBtn"]];
    self.btn.backgroundColor = [UIColor clearColor];
    [StyleManager styleBtn:self.btn titleColor:[UIColor whiteColor]];
    

    
    [self.btn addTarget:self action:@selector(btnClicked) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)btnClicked{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([_permisionType isEqualToString:@"push"]) {
        [(AppDelegate *)[[UIApplication sharedApplication] delegate] configureNotifications];
    }
    if ([_permisionType isEqualToString:@"location"]) {
        [[SettingsManager instance] saveObject:@(1) key:@"localPermission"];
        [GBLocationManager sharedManager];
    }
    if ([_permisionType isEqualToString:@"contact"]) {
        [[ContactSyncManager sharedManager] startSync];
    }
}

@end
