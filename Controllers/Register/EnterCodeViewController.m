//// Header

#import "EnterCodeViewController.h"
#import "SHSPhoneTextField.h"
#import "GBServerManager.h"

@interface EnterCodeViewController (){
    BOOL _isNew;
    NSInteger _userId;
    NSTimer *_timer;
    NSInteger _seconds;
}

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet SHSPhoneTextField *numberField;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;


@end

@implementation EnterCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.numberField.font  = [UIFont defaultFontWithSize:14 bold:NO];
    _seconds = 30;
    self.descLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.descLabel.text = trSignEnterCode;
    self.timeLabel.font = [UIFont defaultFontWithSize:25 bold:NO];
    self.timeLabel.text = @"30";

    __weak typeof(self) weakSelf = self;
    [self.numberField setTextDidChangeBlock:^(UITextField *textField) {
        [weakSelf changeText:textField];
    }];

    self.numberField.backgroundColor = [UIColor whiteColor];
    self.numberField.layer.borderWidth = 1;
    self.numberField.textAlignment = NSTextAlignmentCenter;
    self.numberField.layer.borderColor = COLOR(200, 200, 200).CGColor;
    self.numberField.layer.cornerRadius = 5;

    self.btn.backgroundColor = registerBtnColor;
    self.btn.layer.cornerRadius =  5;
    [StyleManager styleBtn:self.btn title:trSend];
    [StyleManager  styleBtn:self.btn titleColor:[UIColor whiteColor]];
    [self.btn addTarget:self action:@selector(sendCode) forControlEvents:UIControlEventTouchUpInside];
    
    self.headerLabel.font = [UIFont defaultFontWithSize:16 bold:NO];
    self.headerLabel.text = trSignSmsTime;
    
    [self resetTimer];
    [self getCode];
}

-(void)resetTimer{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeTime) userInfo:nil repeats:YES];
//    _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(changeTime) userInfo:nil repeats:YES];
}

-(void)stopTimer{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
}

-(void)changeTime{
    _seconds --;
    if (_seconds == 0) {
        [self stopTimer];
        [self back];
        return;
    }
    self.timeLabel.text = [NSString stringWithFormat:@"%@",@(_seconds)];
}


-(void)changeText:(UITextField *)field{
    NSLog(@"%@",field.text);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}


-(void)getCode{
    if(kProductionVersion){
        return;
    }
    NSInteger phone = [self.info[@"phone"] integerValue];
    NSString *dataString = [@{@"phone":@(phone)} convertToJsonString];
    NSDictionary *params = @{@"data":dataString};
    __weak typeof(self) weakSelf = self;
    NSString *path = kProductionVersion  ? @"reg/code"  : @"registration/code";
    [[ApiManager sharedManager] getRequest:path params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            weakSelf.numberField.text = [NSString stringWithFormat:@"%@",response[@"response"][@"phone_code"]];
        }
    }];
}

-(void)sendCode{
    [self.view endEditing:YES];
    NSInteger code = [self.numberField.phoneNumber integerValue];
    if (code <= 0) {
        [self showErrorMessage:@"Вводите код"];
        return;
    }
    
    NSMutableDictionary *parameters = @{}.mutableCopy;
    NSInteger phone = [self.info[@"phone"] integerValue];
    
    parameters[@"phone_code"] = @(code);
    parameters[@"phone"] = @(phone);
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    NSString *path = kProductionVersion ? @"reg/code" : @"registration/code";
    [[ApiManager sharedManager] postRequest:path params:parameters callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf stopTimer];
            response = response[@"response"];
            _isNew = [response[@"is_new"] integerValue] == 1;
            _userId = [response[@"id_user"] integerValue];
            [CDUser createUserRegisterResponse:response];
            [weakSelf getUser];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getUser{
    
    NSDictionary *params= @{@"data":@(_userId)};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    
    [[ApiManager sharedManager] getRequest:@"users" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            if(_isNew){
                [weakSelf openRegister];
            } else{
                [(AppDelegate *)[[UIApplication sharedApplication] delegate] openTabbar];
                [CDUser currentUserUpdate:response];
            }
            
        }else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)openRegister{
    [[SettingsManager instance] saveObject:@(1) key:@"uncompleteRegister"];
    BaseViewController *controller = [self createController:@"RegisterUserViewController"];
    controller.type = 1;
    [self.navigationController pushViewController:controller animated:YES];
}


@end
