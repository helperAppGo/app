//// Header

#import <UIKit/UIKit.h>
#import "JVFloatLabeledTextField.h"

@interface RegisterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *field;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topH;


/*
 [cell.field setPlaceholder:kCrediteAmount floatingTitle:kCrediteAmount];
 cell.field.delegate = self;
 */
@end
