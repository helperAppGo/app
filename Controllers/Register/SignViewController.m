//// Header

#import "SignViewController.h"
#import "SHSPhoneTextField.h"

@interface SignViewController (){
    
}

@property (weak, nonatomic) IBOutlet SHSPhoneTextField *phoneField;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation SignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.phoneField.keyboardType = UIKeyboardTypeNumberPad;
    [self.phoneField.formatter setDefaultOutputPattern:@"## ### ## ##"];
    self.phoneField.formatter.prefix = @"380";
    self.phoneField.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.phoneField.textAlignment = NSTextAlignmentCenter;
    self.phoneField.layer.borderColor = COLOR(200, 200, 200).CGColor;
    self.phoneField.layer.borderWidth = 1;
    self.phoneField.backgroundColor = [UIColor whiteColor];
    self.phoneField.layer.cornerRadius = 5;
    
    self.btn.layer.cornerRadius = 5;
    self.btn.backgroundColor = registerBtnColor;
    
    NSString *title = trSign;
    [StyleManager styleBtn:self.btn title:title];
    
    [StyleManager styleBtn:self.btn titleColor:[UIColor whiteColor]];
    self.btn.titleLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    [self.btn addTarget:self action:@selector(loginClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.label.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.label.text = trSignSmsDesc;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

-(void)loginClicked{
    [self.view endEditing:YES];
    NSString *path = kProductionVersion  ? @"reg" : @"registration";
    [self showLoader];
    NSInteger phoneNumber = [self.phoneField.phoneNumber integerValue];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] postRequest:path params:@{@"phone":@(phoneNumber)} callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            NSInteger phone = [response[@"phone"] integerValue];
            [weakSelf openEnterCode:phone];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)openEnterCode:(NSInteger)phoneNumber{
    BaseViewController *controller = [self createController:@"EnterCodeViewController"];
    controller.type  = 1;
    controller.info = @{@"phone":@(phoneNumber)};
    [self.navigationController  pushViewController:controller animated:YES];
}

@end
