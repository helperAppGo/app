//// Header

#import "RegisterBtnsTableViewCell.h"

@implementation RegisterBtnsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btn.backgroundColor = registerBtnColor;
    
    [StyleManager styleBtn:self.btn title:trSend];
    [StyleManager styleBtn:self.btn titleColor:[UIColor whiteColor]];
    self.btn.titleLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.btn.layer.cornerRadius = 5;
    
    [self.segmentView setTitle:trSignFemale  forSegmentAtIndex:0];
    [self.segmentView setTitle:trSignMale forSegmentAtIndex:1];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
