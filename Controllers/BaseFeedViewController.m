//// Header

#import "BaseFeedViewController.h"
#import "FeedListTableViewCell.h"
#import "RepostListTableViewCell.h"
#import "ShareViewController.h"
#import "GBCommentsViewController.h"
#import "PlayerViewController.h"
#import "HCYoutubeParser.h"
#import "GroupFeedsViewController.h"


@interface BaseFeedViewController ()<UITableViewDelegate, UITableViewDataSource, NYTPhotosViewControllerDelegate>{
    
}

@end

@implementation BaseFeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UINib *nib5 = [UINib nibWithNibName:@"FeedListTableViewCell" bundle:nil];
    [[self table] registerNib:nib5 forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"RepostListTableViewCell" bundle:nil];
    [[self table] registerNib:nib2 forCellReuseIdentifier:@"list2"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getGroup:(NSInteger)grId{
    grId = ABS(grId);
    if ([self isKindOfClass:[GroupFeedsViewController class]]) {
        return;
    }
    NSDictionary *params = @{@"data": [@{@"id":@(grId)} convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] getRequest:@"groups" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            GroupListModel *group = [[GroupListModel alloc] initWithDict:response[@"response"]];
            [weakSelf openGroupInfo:group];
        }
    }];
}

-(void)openGroupInfo:(GroupListModel *)model{
    GroupFeedsViewController *controller = (GroupFeedsViewController *)[self createController:@"GroupFeedsViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    controller.info = @{@"group":model};
    [self.navigationController pushViewController:controller animated:YES];
    
}

-(void)feedClicked:(NSIndexPath *)indexPath action:(FeedActionType)type sender:(UIButton *)sender{
    FeedModel *model = [self feedItems][indexPath.row];
    [self feedAction:indexPath model:model action:type sender:sender];
}


-(void)feedAction:(NSIndexPath *)path model:(FeedModel *)model action:(FeedActionType)type sender:(UIButton *)sender{
   // __weak typeof(self) weakSelf = self;
    if (type == FeedActionTypeLike) {
        UIImageView *likeView = nil;
        UILabel *countLabel = nil;
        for (UIView *subView in sender.subviews) {
            if ([subView isKindOfClass:[UIImageView class]]) {
                likeView = (UIImageView *)subView;
            }
            if ([subView isKindOfClass:[UILabel class]]) {
                countLabel = (UILabel *)subView;
            }
        }
        if (model.liked) {
            model.likesCount --;
        } else{
            model.likesCount ++;
            [self soundForLike];
        }
        model.liked = !model.liked;
        countLabel.text = [NSString stringWithFormat:@"%@",@(model.likesCount)];
        likeView.image = model.liked ? [UIImage imageNamed:@"postLiked"] : [UIImage imageNamed:@"postUnlike"];
        NSDictionary *parameters = @{@"id_post":@(model.objId)};
        
        [[ApiManager sharedManager] postRequest:@"users/posts/likes" params:parameters callback:^(id response, ErrorObj *error) {
            if (response) {

            } else{
                if (model.liked) {
                    model.likesCount --;
                } else{
                    model.likesCount ++;
                }
                model.liked = !model.liked;
                countLabel.text = [NSString stringWithFormat:@"%@",@(model.likesCount)];
                likeView.image = model.liked ? [UIImage imageNamed:@"postLiked"] : [UIImage imageNamed:@"postUnlike"];
                
            }
        }];
    }
    
    if (type == FeedActionTypeComment) {
        [self openComments:model];
    }
    
    if (type == FeedActionTypeVideo) {
        [self openVideo:model.repostFeed ? model.repostFeed : model];
    }
    
    if (type == FeedActionTypeImage) {
        [self openImage:model.repostFeed ? model.repostFeed : model];
    }
    
    if (type == FeedActionTypeLongPress) {
        [self openLikeList:model.repostFeed ? model.repostFeed : model];
    }
    
    if (type == FeedActionTypeAction) {
        CDUser *user = [[SettingsManager instance] currentUser];
        UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        if (model.userId == user.objId) {
            UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:trFeedDelete style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self deletePost:model];
            }];
            [alertController addAction:deleteAction];
            
        } else{
            
            UIAlertAction *complainPost = [UIAlertAction actionWithTitle:trFeedReport style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self reportPost:model];
            }];
            [alertController addAction:complainPost];
            
            if (model.userId == model.wallId) {
                UIAlertAction *blockAlert = [UIAlertAction actionWithTitle:trFeedBlockUser style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self banUser:model];
                }];
                [alertController addAction:blockAlert];
            }
            
        }
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:trCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
    if (type == FeedActionTypeShare) {
        [self showSharePost:model];
    }
    if (type == FeedActionTypeAvatar) {
        if ([[self.class description] containsString:@"FeedListViewController"]) {
            if (model.wallId == model.userId) {
                [self getUserProfile:model.userId];
            }
            if (model.wallId < 0 && model.userId > 0) {
                [self getUserProfile:model.userId];
            }
            if (model.userId == 0 && model.wallId < 0) {
                // open Group
                [self getGroup:model.wallId];
            }
        } else{
            [self showPhotos:model];
        }
       
    }
    if (type == FeedActionTypeName) {
        if (model.wallId == model.userId) {
            [self getUserProfile:model.userId];
        }
        if (model.userId == 0 && model.wallId < 0) {
            // open Group
            [self getGroup:model.wallId];
        }
        if (model.wallId < 0 && model.userId > 0) {
            // open group
            if ([[self.class description] isEqualToString:@"GroupFeedsViewController"]) {
                [self getUserProfile:model.userId];
            } else{
                [self getGroup:model.wallId];
            }
            
            
        }
        
    }
    
    if (type == FeedActionTypeDoubleClick) {
        UIImageView * heartPopup = (UIImageView *)sender;
        if (model.liked) {
            heartPopup.image = nil;// [UIImage imageNamed:@"postUnlike"];
        } else{
            heartPopup.image = [UIImage imageNamed:@"postLiked"];
        }
        [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            heartPopup.transform = CGAffineTransformMakeScale(1.3, 1.3);
            heartPopup.alpha = 1.0;
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                heartPopup.transform = CGAffineTransformMakeScale(1.0, 1.0);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                    heartPopup.transform = CGAffineTransformMakeScale(1.3, 1.3);
                    heartPopup.alpha = 0.0;
                } completion:^(BOOL finished) {
                    heartPopup.transform = CGAffineTransformMakeScale(1.0, 1.0);
                }];
            }];
        }];
    }
}

-(void)openLikeList:(FeedModel *)model{
    BaseViewController *controller  = [self createController:@"UserListViewController"];
    controller.type = 1;
    NSString *postId = [NSString stringWithFormat:@"%@",@(model.objId)];
    controller.info = @{@"listType":@"3",@"postId":postId};
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)showSharePost:(FeedModel *)model{
    ShareViewController *controller = (ShareViewController *)[self createController:@"ShareViewController"];
    __weak typeof(self) weakSelf = self;
    [controller setBlock:^(NSInteger type, NSString *shareText) {
        if (type == 1) {
            [weakSelf sharePost:model text:shareText];
        }
    }];
    controller.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    controller.preferredContentSize = CGSizeMake(self.view.width, self.view.height);
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController presentViewController:controller animated:YES completion:nil];
    });
}

-(void)sharePost:(FeedModel *)model text:(NSString *)shareText{
    [self showLoader];
    /*
     
     POST@api/posts
     передаем
     id_wall пользователя который делат репост
     text
     repost передаем  id_post которого делаем репост
     */
    CDUser *user = [[SettingsManager instance] currentUser];
    
    NSDictionary *params = @{@"id_wall":@(user.objId),
                             @"id_repost":@(model.objId)};
    if (shareText.length) {
       params = @{@"id_wall":@(user.objId),
                  @"text":shareText,
                  @"id_repost":@(model.objId)};
    }
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] postRequest:@"posts" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)deletePost:(FeedModel *)model{
    
    NSDictionary *params = @{@"data":[@{@"id":@(model.objId)} convertToJsonString]};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] deleteRequest:@"posts" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf.feedItems removeObject:model];
            [weakSelf.table reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)reportPost:(FeedModel *)model{
    NSDictionary *params = @{@"id_post":@(model.objId),
                             @"type":@(0)};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] postRequest:@"posts/report" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf.feedItems removeObject:model];
            [weakSelf.table reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)banUser:(FeedModel *)post{
    NSDictionary *params = @{@"data":[@{@"id_user":@(post.userId)} convertToJsonString]};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] postRequest:@"users/friends/ban" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            NSInteger userId = post.userId;
            NSMutableArray *arr = weakSelf.feedItems;
            for (FeedModel *model in arr) {
                if (model.userId == userId) {
                    [weakSelf.feedItems removeObject:model];
                }
            }
            [weakSelf.table reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)openComments:(FeedModel *)model{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:model.objId itemType:ECommentItemTypeFeedComment];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)openVideo:(FeedModel *)model{
    FeedMediaObject *obj = model.mediaObjects[0];
    NSString *type = obj.type;
    if ([type isEqualToString:@"video"]) {
        NSURL *videoURL = [NSURL URLWithString:obj.path];
        PlayerViewController *playController = [[PlayerViewController alloc] initWithContentURL:videoURL];
        [self presentViewController:playController animated:YES completion:NULL];
    } else if([type isEqualToString:@"youtube"]){
        [self playYotubeVideo:obj.path];
    }
}

-(void)openImage:(FeedModel *)model{
    NSMutableArray *items = @[].mutableCopy;
    if (model.mediaObjects.count == 0) {
        return;
    }
    FeedMediaObject *media = model.mediaObjects[0];
    if (![media.type isEqualToString:@"image"]) {
        return;
    }
    UserPhotoView *view = [[UserPhotoView alloc] init];
    view.urlString =  media.imgUrl;
    [view sd_setImageWithURL:[NSURL URLWithString:view.urlString]];
    [items addObject:view];
    
    //    [items addObject:view];
    
    self.dataSource = [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:items];
    PhotoViewController *photosViewController = [[PhotoViewController alloc] initWithDataSource:self.dataSource initialPhoto:self.dataSource[0] delegate:self];
    photosViewController.view.tintColor = [UIColor whiteColor];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

-(void)openAvatar:(NSString *)model{
    if (model.length == 0) {
        return;
    }
    NSMutableArray *items = @[].mutableCopy;
    UserPhotoView *view = [[UserPhotoView alloc] init];
    view.urlString =  model;
    [view sd_setImageWithURL:[NSURL URLWithString:view.urlString]];
    [items addObject:view];
    
    self.dataSource = [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:items];
    PhotoViewController *photosViewController = [[PhotoViewController alloc] initWithDataSource:self.dataSource initialPhoto:self.dataSource[0] delegate:self];
    photosViewController.view.tintColor = [UIColor whiteColor];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

- (void)playYotubeVideo:(NSString*)youtubeId{
    NSDictionary *dict = [HCYoutubeParser h264videosWithYoutubeID:youtubeId];
    NSString *URLString = nil;
    if ([dict objectForKey:@"small"] != nil) {
        URLString = [dict objectForKey:@"small"];
    } else if ([dict objectForKey:@"live"] != nil) {
        URLString = [dict objectForKey:@"live"];
    } else {
        [[[UIAlertView alloc] initWithTitle:trError message:trYoutubeError delegate:nil cancelButtonTitle:trCancel otherButtonTitles: nil] show];
        return;
    }
    NSURL *videoURL = [NSURL URLWithString:URLString];
    PlayerViewController *playController = [[PlayerViewController alloc] initWithContentURL:videoURL];
    [self presentMoviePlayerViewControllerAnimated:playController];
}

#pragma mark - Cell  methods
-(UITableViewCell *)feedCell:(FeedModel *)model indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView{
    
    if (model.repostFeed) {
        RepostListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list2"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.separatorInset = UIEdgeInsetsMake(0, tableView.width, 0, 0);
        [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.pathAvatar]  placeholderImage:[UIImage imageNamed:@"user_ava"]];
        cell.statusView.backgroundColor = appStatusColor(model.status);
        [StyleManager styleBtn:cell.titleBtn title:model.name];
        cell.timeLabel.text = [model.createDate timeString];
        cell.indexPath = indexPath;
        __weak typeof(self) weakSelf = self;
        [cell setClickBlock:^(NSIndexPath *path, FeedActionType type, UIButton *sender) {
             [weakSelf feedClicked:path action:type sender:sender];
        }];
         
        if (model.text.length) {
            cell.titleLabel.text = model.text;
        } else{
            cell.titleLabel.text = nil;
            cell.titleH.constant = 0;
        }
        cell.likeCountLabel.text = [model likeCountString];
        cell.commentCountLabel.text = [model commentCountString];
        cell.likeIcon.image = [model likeIcon];
        
        FeedModel *repostModel = model.repostFeed;
        
        [cell.childAvatarView sd_setImageWithURL:[NSURL URLWithString:repostModel.pathAvatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
        cell.childNameLabel.text = repostModel.name;
        cell.childTimeLabel.text = repostModel.timeString;
        cell.indexPath = indexPath;
        if (repostModel.text.length) {
            cell.childTitleText.text = repostModel.text;
        } else{
            cell.childTitleText.text = nil;
            cell.childTitleH.constant = 0;
        }
        
        
        if (repostModel.mediaObjects.count) {
            FeedMediaObject *obj = repostModel.mediaObjects[0];
            [cell.childImageView sd_setImageWithURL:[NSURL URLWithString:[obj imgUrl]]];
            
            if ([obj.type isEqualToString:@"image"]) {
                cell.childPlayBtn.hidden = YES;
                cell.gradientBtn.hidden = NO;
                cell.childImageX.constant = obj.childH;
            }
            
            if ([obj.type isEqualToString:@"video"]) {
                cell.childPlayBtn.hidden = NO;
                cell.gradientBtn.hidden = YES;
                cell.childImageX.constant = obj.childiVideoH;
            }
            
            if ([obj.type isEqualToString:@"youtube"]) {
                cell.childPlayBtn.hidden = NO;
                cell.gradientBtn.hidden = YES;
                cell.childImageX.constant = obj.childiVideoH;
            }
        } else {
            cell.childImageX.constant = 0;
        }
        
        return cell;
    }
    
    FeedListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.separatorInset = UIEdgeInsetsMake(0, tableView.width, 0, 0);
    
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.pathAvatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.statusView.backgroundColor = appStatusColor(model.status);
    [StyleManager styleBtn:cell.titleBtn title:model.name];
    cell.timeLabel.text = [model.createDate timeString];
    cell.indexPath = indexPath;
    __weak typeof(self) weakSelf = self;
    [cell setClickBlock:^(NSIndexPath *indexPath, FeedActionType type, UIButton *sender) {
        [weakSelf feedClicked:indexPath action:type sender:sender];
    }];
    if (model.text.length) {
        cell.titleLabel.text = model.text;
    } else{
        cell.titleLabel.text = nil;
        cell.titleH.constant = 0;
    }
    cell.likeCountLabel.text = [model likeCountString];
    cell.commentLabel.text = [model commentCountString];
    cell.likeIconView.image = [model likeIcon];
    
    if (model.mediaObjects.count) {
        FeedMediaObject *obj = model.mediaObjects[0];
        if ([obj.type isEqualToString:@"image"]) {
            cell.playBtn.hidden = YES;
            cell.attachView.backgroundColor = [UIColor blackColor];
            cell.attachH.constant = obj.h;
            cell.attachView.backgroundColor = [UIColor clearColor];
            [cell.attachView sd_setImageWithURL:[NSURL URLWithString:[obj imgUrl]]];
        }
        
        if ([obj.type isEqualToString:@"video"]) {
            [cell.attachView sd_setImageWithURL:[NSURL URLWithString:[obj imgUrl]]];
            cell.playBtn.hidden = NO;
            cell.attachView.backgroundColor = [UIColor blackColor];
            cell.gradientView.backgroundColor = [UIColor clearColor];
            cell.attachH.constant = obj.videoH;
        }
        
        if ([obj.type isEqualToString:@"youtube"]) {
            [cell.attachView sd_setImageWithURL:[NSURL URLWithString:[obj imgUrl]]];
            cell.playBtn.hidden = NO;
            cell.attachView.backgroundColor = [UIColor blackColor];
            cell.gradientView.backgroundColor = [UIColor clearColor];
            cell.attachH.constant = obj.videoH;
        }
        
    } else {
        cell.attachH.constant = 0;
    }
    
    return cell;
    
}

-(void)showPhotos:(FeedModel *)model{
    [self openAvatar:model.pathAvatar];
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController referenceViewForPhoto:(id <NYTPhoto>)photo {
    return nil;
//    NSInteger index = [self.dataSource indexOfPhoto:photo];
//    NSIndexPath *path = [NSIndexPath indexPathForRow:index inSection:0];
//    UICollectionViewCell *cell = [_photoCollectionView cellForItemAtIndexPath:path];
//    return cell.contentView;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController loadingViewForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (UIView *)photosViewController:(NYTPhotosViewController *)photosViewController captionViewForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (CGFloat)photosViewController:(NYTPhotosViewController *)photosViewController maximumZoomScaleForPhoto:(id <NYTPhoto>)photo {
    return 0.5f;
}

- (NSDictionary *)photosViewController:(NYTPhotosViewController *)photosViewController overlayTitleTextAttributesForPhoto:(id <NYTPhoto>)photo {
    return nil;
}

- (NSString *)photosViewController:(NYTPhotosViewController *)photosViewController titleForPhoto:(id<NYTPhoto>)photo atIndex:(NSInteger)photoIndex totalPhotoCount:(nullable NSNumber *)totalPhotoCount {
    return [NSString stringWithFormat:@"%lu/%lu", (unsigned long)photoIndex+1, (unsigned long)totalPhotoCount.integerValue];
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController didNavigateToPhoto:(id <NYTPhoto>)photo atIndex:(NSUInteger)photoIndex {
    NSLog(@"Did Navigate To Photo: %@ identifier: %lu", photo, (unsigned long)photoIndex);
}

- (void)photosViewController:(NYTPhotosViewController *)photosViewController actionCompletedWithActivityType:(NSString *)activityType {
    NSLog(@"Action Completed With Activity Type: %@", activityType);
}

- (void)photosViewControllerDidDismiss:(NYTPhotosViewController *)photosViewController {
    NSLog(@"Did Dismiss Photo Viewer: %@", photosViewController);
}

@end
