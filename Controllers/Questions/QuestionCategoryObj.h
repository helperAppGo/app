//// Header

#import <Foundation/Foundation.h>

@interface QuestionCategoryObj : NSObject
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger order;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *name;

-(id)initWithDict:(NSDictionary *)dict;

@end
