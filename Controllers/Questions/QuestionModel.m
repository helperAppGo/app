//// Header

#import "QuestionModel.h"
#import "UIFont+Additions.h"
#import "FeedModel.h"

@implementation QuestionAnswerModel

-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        NSString *dateString = NULL_TO_NIL(dict[@"time"]);
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        self.date = [formatter dateFromString:dateString];
        formatter.dateFormat = @"HH:mm";
        self.dateString = [formatter stringFromDate:self.date];
        
        self.likeCount = [NULL_TO_NIL(dict[@"count_likes"]) integerValue];
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.questionId = [NULL_TO_NIL(dict[@"id_question"]) integerValue];
        self.userId = [NULL_TO_NIL(dict[@"id_user"]) integerValue];
        self.liked = [NULL_TO_NIL(dict[@"is_liked"]) integerValue];
        self.userName = NULL_TO_NIL(dict[@"name"]);
        self.avatar = NULL_TO_NIL(dict[@"path_ava"]);
        self.text = NULL_TO_NIL(dict[@"text"]);
        self.postId = [NULL_TO_NIL(dict[@"id_post"]) integerValue];
        
      
        /*    {
         "count_likes" = 0;
         files = "";
         id = 93;
         "id_question" = 116;
         "id_user" = 181;
         "is_liked" = 0;
         name = "\U0416\U0435\U043d\U044f \U0422\U0435\U0441\U0442";
         "path_ava" = "http://helper.darx.net/./files/17d64bd8ac8cf292/1a91647416f0ac09/0d69b8df360238ae/9b74fa63e52e35b4.jpg";
         text = "\U0440\U0440\U0440";
         time = "2017-07-06T21:29:48+03:00";
         */

       
    }
    return self;
}

-(NSString *)likeCountString{
    if (self.likeCount) {
        return [NSString stringWithFormat:@"%@",@(self.likeCount)];
    }
    return @"";
}


@end



@implementation QuestionModel

-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.answerCount = [NULL_TO_NIL(dict[@"count_answers"]) integerValue];
        self.likesCount = [NULL_TO_NIL(dict[@"count_likes"]) integerValue];
        self.distance = [NULL_TO_NIL(dict[@"distance"]) doubleValue];
        self.userId = [NULL_TO_NIL(dict[@"id_user"]) integerValue];
        self.geoLat = [NULL_TO_NIL(dict[@"lat"]) doubleValue];
        self.geoLon = [NULL_TO_NIL(dict[@"lon"]) doubleValue];
        self.status = [NULL_TO_NIL(dict[@"status"]) integerValue];
        
        self.address = NULL_TO_NIL(dict[@"address"]);
        self.text = NULL_TO_NIL(dict[@"text"]);
        self.avatar = NULL_TO_NIL(dict[@"path_ava"]);
        self.name = NULL_TO_NIL(dict[@"name"]);
        
        NSString *dateString = NULL_TO_NIL(dict[@"time"]);
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
//        NSDate *date = [formatter dateFromString:dateString];
        self.date = [formatter dateFromString:dateString];
        
        NSArray *files = NULL_TO_NIL(dict[@"file_objects"]);
        NSMutableArray *items = @[].mutableCopy;
        if (files.count) {
            for (NSDictionary *fileDict in files) {
                FeedMediaObject *obj = [[FeedMediaObject alloc] initWithDict:fileDict];
                if (obj) {
                    [items addObject:obj];
                }
                
            }
            self.mediaObjects = items;
        }
        
        formatter.dateFormat = @"HH:mm";
        self.timeString = [formatter stringFromDate:self.date];
        
    }
    return self;
}

-(NSString *)answerString{
    NSInteger langId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"] integerValue];
    NSString *str = @"";
    if (langId == 1) {
        str = @"Відповіді";
    }
    str = (langId == 2) ? @"Ответы" : @"Answers";
    if (self.answerCount == 0) {
        return str;
    }
    return [NSString stringWithFormat:@"%@ %@", str, @(self.answerCount)];
}

-(NSString *)distanceString{
    if (self.distance == 0) {
        return @"";
    }
    if (self.distance > 1000) {
        return [NSString stringWithFormat:@"%.02f км",self.distance/1000.f];
    }
    return [NSString stringWithFormat:@"%.02f m",self.distance];
}

@end
