//// Header

#import <UIKit/UIKit.h>

@interface ToolBarView : UIView

@property (weak, nonatomic) IBOutlet UIButton *mapBtn;
@property (weak, nonatomic) IBOutlet UIButton *mediaBtn;
@property (weak, nonatomic) IBOutlet UILabel *adressLabel;

@end
