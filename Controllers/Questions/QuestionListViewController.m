//// Header

#import "QuestionListViewController.h"
#import "QuestionListTableViewCell.h"
#import <GoogleMaps/GoogleMaps.h>
#import "GBCommentsViewController.h"
#import "PlayerViewController.h"
#import "HCYoutubeParser.h"
#import "LoaderCell.h"

#import <NYTPhotoViewer/NYTPhotosViewController.h>
#import <NYTPhotoViewer/NYTPhoto.h>
#import <NYTPhotoViewer/NYTPhotoViewerArrayDataSource.h>
#import "UserPhotoView.h"
#import "PhotoViewController.h"
#import "FeedTitleView.h"
#import "FTPopOverMenu.h"
#import "QuestionCategoryObj.h"

@interface QuestionListViewController ()<UITableViewDelegate, UITableViewDataSource, GMSMapViewDelegate,NYTPhotosViewControllerDelegate>{
    NSMutableArray *_list;
    NSMutableArray *_geoList;
    NSMutableArray *_disPlayList;
    NSMutableArray *_mapList;
    NSMutableArray *_markerList;
    
    UIRefreshControl *_refreshControl;
    BOOL _showMyLocation;
    
    BOOL _questionComplete;
    BOOL _geoComplete;
    BOOL _completeMap;
    
    QuestionModel *_selectModel;
    GMSMarker *_selectMarker;
    NSInteger _segmentIndex;
    FeedTitleView *_filterView;
    
    NSMutableArray *_categoryList;
    
    
}
@property (nonatomic) NYTPhotoViewerArrayDataSource *dataSource;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet GMSMapView *mapsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topH;

@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIButton *avatartBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *questionBtn;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIButton *btn;

@end

@implementation QuestionListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    _titleLabel.text = trQuestionTitle;
    
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateNormal];
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateSelected];
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateHighlighted];
    self.topView.backgroundColor = COLOR(248, 248, 248);
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UINib *nib = [UINib nibWithNibName:@"QuestionListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"loader"];
    
    _refreshControl= [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
    
    [self.segmentView addTarget:self action:@selector(changeIndex) forControlEvents:UIControlEventValueChanged];
    
    _list = @[].mutableCopy;
    _geoList = @[].mutableCopy;
    _categoryList = @[].mutableCopy;
    QuestionCategoryObj *obj = [[QuestionCategoryObj alloc] init];
    obj.name = trQuestionTitle;
    [_categoryList addObject:obj];
    
    if (self.info) {
        self.topH.constant = 0;
        self.topView.hidden = YES;
    }
    [self getQuestionList];
    self.mapsView.hidden = YES;
    _markerList = @[].mutableCopy;
    _mapList = @[].mutableCopy;
    
    self.detailView.backgroundColor = [UIColor whiteColor];
    self.detailView.layer.cornerRadius = 5;
    self.detailView.hidden = YES;
    self.avatarView.layer.cornerRadius = 25;
    self.avatarView.clipsToBounds = YES;
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.nameLabel.textColor = COLOR(56, 56, 56);
    self.questionLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    
    self.answerLabel.textColor = appBlueColor;
    self.answerLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    
    [self.questionBtn addTarget:self action:@selector(openMapAnswers) forControlEvents:UIControlEventTouchUpInside];
    [self.btn addTarget:self action:@selector(openMap) forControlEvents:UIControlEventTouchUpInside];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateNotification) name:@"updateBadge" object:nil];
    
    _filterView = [FeedTitleView loadFromNib];
    [_filterView.heightAnchor constraintEqualToConstant:44].active = YES;
    _filterView.titleLabel.text = trQuestionTitle;
    self.navigationItem.titleView = _filterView;
    
    _filterView.backgroundColor = [UIColor clearColor];
    _filterView.containerBtn.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self     action:@selector(openFilter:)];
    _filterView.containerBtn.userInteractionEnabled = YES;
    [_filterView.containerBtn addGestureRecognizer:tapGesture];
    [self getCategories];
    
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.menuWidth = 200;
    configuration.isQuestion = YES;
    configuration.textColor = [UIColor blackColor];
    configuration.textFont = [UIFont defaultFontWithSize:12 bold:NO];
    configuration.tintColor = [COLOR(190, 190, 190) colorWithAlphaComponent:0.5];
    configuration.borderWidth = 0;
}

-(void)getCategories{
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"questions/categories" params:@{} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            NSMutableArray *objects = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                QuestionCategoryObj *obj = [[QuestionCategoryObj alloc] initWithDict:dict];
                [objects addObject:obj];
            }
            [_categoryList addObjectsFromArray:objects];
            NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
            if (index <= _categoryList.count) {
                _filterView.iconView.image = [UIImage imageNamed:@"bottomArrow"];
                QuestionCategoryObj *obj = _categoryList[index];
                _filterView.titleLabel.text = obj.name;
            }
        }
        [weakSelf.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)openFilter:(UIGestureRecognizer *)gesture{
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.isQuestion = YES;
    
    [UIView animateWithDuration:0.5 animations:^{
        _filterView.iconView.image = [UIImage imageNamed:@"topArrow"];
    }];
    [FTPopOverMenu showForSender:_filterView.iconView
                   withMenuArray:_categoryList
                      imageArray:nil
                       doneBlock:^(NSInteger selectedIndex) {
                           NSInteger old = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
                           if (old == selectedIndex) {
                               return;
                           }
                           [FTPopOverMenuConfiguration defaultConfiguration].questionIndex = selectedIndex;
                           [self refresh];
                           [UIView animateWithDuration:0.5 animations:^{
                               _filterView.iconView.image = [UIImage imageNamed:@"bottomArrow"];
                               NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
                               QuestionCategoryObj *obj = _categoryList[index];
                               _filterView.titleLabel.text = obj.name;
                           }];
                           
                       } dismissBlock:^{
                           [UIView animateWithDuration:0.5 animations:^{
                               _filterView.iconView.image = [UIImage imageNamed:@"bottomArrow"];
                               NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
                               QuestionCategoryObj *obj = _categoryList[index];
                               _filterView.titleLabel.text = obj.name;
                           }];
                       }];
}

-(void)openMap{
    BaseViewController *controller = [self createController:@"MapQuestionViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [[SettingsManager instance] anySound];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (!self.info) {
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:trCreate style:UIBarButtonItemStylePlain target:self action:@selector(createQuestion)];
        item.tintColor = appBlueColor;
        self.navigationItem.rightBarButtonItem = item;
        {
            UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_notification"] style:UIBarButtonItemStyleDone target:self action:@selector(openNotifications)];
            item.tintColor = appBlueColor;
            self.navigationItem.leftBarButtonItem = item;
            
            [self updateNotification];
        }
    }
}

-(void)updateNotification{
    NSInteger count = [[[SettingsManager instance] objForKey:@"notification2"] integerValue];
    if ([UIDevice currentDevice].systemVersion.doubleValue >= 11) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self addBadge:self.navigationItem.leftBarButtonItem count:count];
        });
        return;
    }
    [self addBadge:self.navigationItem.leftBarButtonItem count:count];
}


-(void)openNotifications{
    BaseViewController *controller = [self createController:@"NotificationListViewController"];
    controller.type = 1;
    controller.info = @{@"type":@"question"};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}


-(void)createQuestion{
    if (_categoryList.count < 2) {
        return;
    }
    BaseViewController *controller = [self createController:@"CategoryListViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    controller.info = @{@"category":_categoryList};
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeIndex{
    [[SettingsManager instance] anySound];
    if (self.segmentView.selectedSegmentIndex == 2) {
        self.segmentView.selectedSegmentIndex = _segmentIndex;
        [self openMap];
        return;
    }
    _segmentIndex = self.segmentView.selectedSegmentIndex;
    if (_segmentIndex == 1) {
        [self getGeoList];
    }
    [self.tableView reloadData];
}


-(void)refresh{
    if (self.segmentView.selectedSegmentIndex == 0) {
        [self getQuestionListNow];
    } else if(self.segmentView.selectedSegmentIndex == 1){
        [self getGeoListNow];
    }
}

-(void)getQuestionListNow{
    NSInteger time = (NSInteger)[[NSDate date] timeIntervalSince1970];
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(0),
                           @"expand":@"files",
                           @"time":@(time)};
    NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
    if (index > 0 && _categoryList.count) {
        QuestionCategoryObj *ob =  _categoryList[index];
        dict = @{@"limit":@(25),
                 @"offset":@(0),
                 @"expand":@"files",
                 @"category":@(ob.objId),
                 @"time":@(time)};
    }
    
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    NSString *path = self.info ? @"questions/self" : @"questions/last";
    __weak typeof(self) weakSelf = self;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:path params:params callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            _questionComplete = NO;
            [_list removeAllObjects];
            NSArray *items = NULL_TO_NIL(response[@"response"]);
            for (NSDictionary *dict in items) {
                QuestionModel *model = [[QuestionModel alloc] initWithDict:dict];
                [_list addObject:model];
            }
            [weakSelf.tableView reloadData];
        }
    }];
}

-(void)getQuestionList{
    if (_questionComplete) {
        return;
    }
    NSInteger time = (NSInteger)[[NSDate date] timeIntervalSince1970];
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(_list.count),
                           @"expand":@"files",
                           @"time":@(time)};
    NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
    if (index >0 && _categoryList.count) {
        QuestionCategoryObj *obj =  _categoryList[index];
        dict = @{@"limit":@(25),
                 @"offset":@(_list.count),
                 @"expand":@"files",
                 @"time":@(time),
                 @"category":@(obj.objId)};
    }
    
    NSString *path = self.info ? @"questions/self" : @"questions/last";
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:path params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = NULL_TO_NIL(response[@"response"]);
            for (NSDictionary *dict in items) {
                QuestionModel *model = [[QuestionModel alloc] initWithDict:dict];
                [_list addObject:model];
            }
            _questionComplete = items.count < 25;
            [weakSelf.tableView reloadData];
        }
    }];
}

-(void)getGeoList{
    //questions/geo
    if (_geoComplete) {
        return;
    }
    NSInteger time = (NSInteger)[[NSDate date] timeIntervalSince1970];
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(_geoList.count),
                           @"expand":@"files",
                           @"time":@(time)};
    NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
    if (index > 0 && _categoryList.count) {
        NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
        QuestionCategoryObj *obj =  _categoryList[index];
        dict =@{@"limit":@(25),
                @"offset":@(_geoList.count),
                @"expand":@"files",
                @"category":@(obj.objId),
                @"time":@(time)};
    }
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"questions/geo" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = NULL_TO_NIL(response[@"response"]);
            for (NSDictionary *dict in items) {
                QuestionModel *model = [[QuestionModel alloc] initWithDict:dict];
                [_geoList addObject:model];
            }
            _geoComplete = items.count < 25;
            [weakSelf.tableView reloadData];
        }
    }];
}

-(void)getGeoListNow{
    NSInteger time = (NSInteger)[[NSDate date] timeIntervalSince1970];
    NSDictionary *dict = @{@"limit":@(25),
                           @"offset":@(0),
                           @"expand":@"files",
                           @"time":@(time)};
    NSInteger index = [FTPopOverMenuConfiguration defaultConfiguration].questionIndex;
    if (index > 0 && _categoryList.count) {
        QuestionCategoryObj *obj =  _categoryList[index];
        dict =@{@"limit":@(25),
                @"offset":@(0),
                @"expand":@"files",
                @"category":@(obj.objId),
                @"time":@(time)};
    }
    
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:@"questions/geo" params:params callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            _geoComplete = NO;
            [_geoList removeAllObjects];
            NSArray *items = NULL_TO_NIL(response[@"response"]);
            for (NSDictionary *dict in items) {
                QuestionModel *model = [[QuestionModel alloc] initWithDict:dict];
                [_geoList addObject:model];
            }
            [weakSelf.tableView reloadData];
        }
    }];
}

-(void)questionAction:(NSIndexPath *)indexPath action:(FeedActionType )type{
    QuestionModel *model = self.segmentView.selectedSegmentIndex == 0 ?  _list[indexPath.row] : _geoList[indexPath.row];
    
    if (type == FeedActionTypeName) {
        [self getUserProfile:model.userId];
    }
    if (type == FeedActionTypeAvatar) {
        [self openImage:model avatar:YES];
    }
    if (type == FeedActionTypeAnswers) {
        [self openAnswers:model];
    }
    
    if (type == FeedActionTypeImage) {
        [self openImage:model avatar:NO];
    }
    
    if (type == FeedActionTypeLocation) {
        [self locationClicked:model indexPath:indexPath];
    }
    
    if (type == FeedActionTypeVideo) {
        [self openVideo:model];
    }
    
    if (type == FeedActionTypeAction) {
        CDUser *user = [[SettingsManager instance] currentUser];
        UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        if (model.userId == user.objId) {
            UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:trQuestionDelete style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                [self deletePost:model];
            }];
            [alertController addAction:deleteAction];
            
        } else{
            UIAlertAction *complainPost = [UIAlertAction actionWithTitle:trQuestionReport style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self reportPost:model];
            }];
            [alertController addAction:complainPost];
            
        }
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:trCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)deletePost:(QuestionModel *)model{
    
    NSDictionary *params = @{@"data":[@{@"id":@(model.objId)} convertToJsonString]};
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] deleteRequest:@"questions" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [_list removeObject:model];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)reportPost:(QuestionModel *)model{
    NSDictionary *params = @{@"id_question":@(model.objId),
                             @"type":@(0)};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] postRequest:@"questions/report" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [_list removeObject:model];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}


-(void)openVideo:(QuestionModel *)model{
    FeedMediaObject *obj = model.mediaObjects[0];
    NSString *type = obj.type;
    if ([type isEqualToString:@"video"]) {
        NSURL *videoURL = [NSURL URLWithString:obj.path];
        PlayerViewController *playController = [[PlayerViewController alloc] initWithContentURL:videoURL];
        [self presentViewController:playController animated:YES completion:NULL];
    } else if([type isEqualToString:@"youtube"]){
        [self playYotubeVideo:obj.path];
    }
}

- (void)playYotubeVideo:(NSString*)youtubeId{
    NSDictionary *dict = [HCYoutubeParser h264videosWithYoutubeID:youtubeId];
    NSString *URLString = nil;
    if ([dict objectForKey:@"small"] != nil) {
        URLString = [dict objectForKey:@"small"];
    } else if ([dict objectForKey:@"live"] != nil) {
        URLString = [dict objectForKey:@"live"];
    } else {
        [[[UIAlertView alloc] initWithTitle:trError message:trYoutubeError delegate:nil cancelButtonTitle:trCancel otherButtonTitles: nil] show];
        return;
    }
    NSURL *videoURL = [NSURL URLWithString:URLString];
    PlayerViewController *playController = [[PlayerViewController alloc] initWithContentURL:videoURL];
    [self presentMoviePlayerViewControllerAnimated:playController];
}


-(void)deleteModel{
    
}

-(void)openImage:(QuestionModel *)model avatar:(BOOL)avatar{
    if (avatar) {
        UserPhotoView *view = [[UserPhotoView alloc] init];
        view.urlString =  model.avatar;
        [view sd_setImageWithURL:[NSURL URLWithString:view.urlString]];
        
        self.dataSource = [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:@[view]];
        PhotoViewController *photosViewController = [[PhotoViewController alloc] initWithDataSource:self.dataSource initialPhoto:self.dataSource[0] delegate:self];
        photosViewController.view.tintColor = [UIColor whiteColor];
        [self presentViewController:photosViewController animated:YES completion:nil];
        
        return;
    }
    NSMutableArray *items = @[].mutableCopy;
    if (model.mediaObjects.count == 0) {
        return;
    }
    FeedMediaObject *media = model.mediaObjects[0];
    if (![media.type isEqualToString:@"image"]) {
        return;
    }
    UserPhotoView *view = [[UserPhotoView alloc] init];
    view.urlString =  media.imgUrl;
    [view sd_setImageWithURL:[NSURL URLWithString:view.urlString]];
    [items addObject:view];
    
    self.dataSource = [NYTPhotoViewerArrayDataSource dataSourceWithPhotos:items];
    PhotoViewController *photosViewController = [[PhotoViewController alloc] initWithDataSource:self.dataSource initialPhoto:self.dataSource[0] delegate:self];
    photosViewController.view.tintColor = [UIColor whiteColor];
    [self presentViewController:photosViewController animated:YES completion:nil];
}

-(void)locationClicked:(QuestionModel *)model indexPath:(NSIndexPath *)indexPath{
    model.hideLocation = !model.hideLocation;
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = self.segmentView.selectedSegmentIndex == 0 ?  _list.count : _geoList.count;
    if (count) {
        return count + 1;
    }
    return count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger count = self.segmentView.selectedSegmentIndex == 0 ?  _list.count : _geoList.count;
    if (count == indexPath.row) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (self.segmentView.selectedSegmentIndex == 0) {
            if (_questionComplete) {
                [cell.loader stopAnimating];
                cell.loader.hidden = YES;
            }
        }
        if (self.segmentView.selectedSegmentIndex == 1) {
            if (_geoComplete) {
                [cell.loader stopAnimating];
                cell.loader.hidden = YES;
            }
        }
        
        return cell;
    }
    QuestionListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    QuestionModel *model = self.segmentView.selectedSegmentIndex == 0 ? _list[indexPath.row] : _geoList[indexPath.row];
    
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.nameLabel.text = model.name;
    cell.statusView.backgroundColor = appStatusColor(model.status);
    cell.timeLabel.text = [model.date timeString];
    cell.answerLabel.text = model.answerString;
    if (model.distance) {
        cell.locationBtn.hidden = NO;
        cell.locationLabel.hidden = NO;
        cell.locationLabel.text = model.distanceString;
    } else{
        cell.locationBtn.hidden = YES;
        cell.locationLabel.text = model.distanceString;
        cell.locationLabel.hidden = YES;
    }
    if (model.hideLocation) {
        cell.locationBtn.hidden = YES;
        cell.locationLabel.text = model.address;
        cell.locationW.constant = 0;
    }  else{
        cell.locationW.constant = 25;
        cell.locationLabel.text = model.distanceString;
        cell.locationBtn.hidden = NO;
    }
    cell.categoryLabel.text = [NSString stringWithFormat:@"%@",@(model.categoryId)];
    cell.indexPath = indexPath;
    __weak typeof(self) weakSelf = self;
    [cell setClickBlock:^(NSIndexPath *path, FeedActionType type) {
        [weakSelf questionAction:path action:type];
    }];
    if (model.text.length) {
        cell.titleLabel.text = model.text;
    } else{
        cell.titleLabel.text = nil;
        cell.titleH.constant = 0;
    }
    
    if (model.mediaObjects.count) {
        FeedMediaObject *obj = model.mediaObjects[0];
        [cell.mediaView sd_setImageWithURL:[NSURL URLWithString:[obj imgUrl]]];
        
        if ([obj.type isEqualToString:@"image"]) {
            cell.playBtn.hidden = YES;
            cell.mediaH.constant = obj.h;
        }
        
        if ([obj.type isEqualToString:@"video"]) {
            cell.playBtn.hidden = NO;
            cell.mediaH.constant = obj.h;
        }
        
        if ([obj.type isEqualToString:@"youtube"]) {
            cell.playBtn.hidden = NO;
            cell.mediaH.constant = obj.h;
        }
    } else {
        cell.mediaH.constant = 0;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_segmentView.selectedSegmentIndex == 0) {
        if (indexPath.row == _list.count - 4) {
            [self getQuestionList];
        }
    }
    if (_segmentView.selectedSegmentIndex == 1) {
        if (indexPath.row == _geoList.count - 4) {
            [self getGeoList];
        }
    }
}

-(void)openMapAnswers{
    if (_selectModel) {
        [self openAnswers:_selectModel];
    }
}

-(void)openAnswers:(QuestionModel *)model{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:model.objId itemType:ECommentItemTypeAnswer];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
    [[SettingsManager instance] anySound];
}

#pragma mark - MapView delegate methods

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self hideDetails];
}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    if (position.zoom < 4) {
        _completeMap = YES;
    }
    
    if (!_completeMap) {
        [self changePosition];
    }
    
}

-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    _selectModel = marker.userData;
    [self generateMarker:marker url:_selectModel.avatar selected:YES];
    
    [self showDetails];
    return nil;
}

-(void)userLocationClicked{
    [_mapsView animateToLocation:_mapsView.myLocation.coordinate];
    [self hideDetails];
}

-(BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView{
    [self userLocationClicked];
    return YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (_showMyLocation) {
        if (_mapList.count == 0) {
            [self getMapsItems];
        }
        
        return;
    }
    if ([keyPath isEqualToString:@"myLocation"]) {
        _showMyLocation = YES;
        GMSCameraPosition *position = [GMSCameraPosition cameraWithTarget:_mapsView.myLocation.coordinate zoom:15];
        [_mapsView setCamera:position];
    }
}



-(void)showMapItems{
    for (GMSMarker *marker in _markerList) {
        marker.map = nil;
    }
    [_markerList removeAllObjects];
    for (QuestionModel *model in _mapList) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(model.geoLat, model.geoLon);
        marker.map = _mapsView;
        marker.userData = model;
        [self generateMarker:marker url:model.avatar selected:NO];
        [_markerList addObject:marker];
    }
}

-(void)hideDetails{
    [UIView animateWithDuration:0.4 animations:^{
        self.detailView.alpha = 0;
    }];
    [self generateMarker:_selectMarker url:_selectModel.avatar selected:NO];
}

-(void)showDetails{
    
    [UIView animateWithDuration:0.6 animations:^{
        self.detailView.alpha = 1;
    }];
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:_selectModel.avatar]];
    self.nameLabel.text = _selectModel.name;
    self.questionLabel.text = _selectModel.text;
    self.answerLabel.text = _selectModel.answerString;
}

-(void)getMapsItems{
    [self changePosition];
}


-(void)changePosition{
    CLLocationCoordinate2D center = [self getCenterCoordinate];
    NSDictionary *params = @{@"Lat":@(center.latitude),
                             @"Lon":@(center.longitude),
                             @"Radius":@([self getRadius])};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"questions/map" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            response = response[@"response"];
            [_mapList removeAllObjects];
            for (NSDictionary *dict in response) {
                QuestionModel *model = [[QuestionModel alloc] initWithDict:dict];
                [_mapList addObject:model];
            }
            [weakSelf showMapItems];
        } else{
            
        }
    }];
}


-(void)generateMarker:(GMSMarker *)marker url:(NSString *)url selected:(BOOL)select{
    UIView *gasView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 64, 81)];
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 60, 60)];
    logo.layer.cornerRadius = 30;
    logo.clipsToBounds = YES;
    [logo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"avatar_on_map"]];
    [gasView addSubview:logo];
    if (select) {
        gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_when_tab"]];
    }else{
        gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_when_tabWhite"]];
        
    }
    marker.icon = [self imageWithView:gasView];
}

-(UIImage *)imageWithView:(UIView *)view{
    if ([UIScreen.mainScreen respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen.scale);
        
    } else{
        UIGraphicsBeginImageContext(view.frame.size);
        
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(CLLocationCoordinate2D)getCenterCoordinate {
    CGPoint centerPoint = self.mapsView.center;
    CLLocationCoordinate2D centerCoordinate = [self.mapsView.projection coordinateForPoint:centerPoint];
    return centerCoordinate;
}

-(CLLocationCoordinate2D)getTopCenterCoordinate{
    
    //    CGPoint topCenterCoor = self.mapView.convertPoint(CGPointMake(self.mapView.frame.size.width / 2.0, 0), fromView: self.mapView)
    CGPoint topCenterCoor = [self.mapsView convertPoint:CGPointMake(self.mapsView.frame.size.width / 2.0, 0) fromView:self.mapsView];
    //    (, fromView: self.mapView)
    CLLocationCoordinate2D point = [self.mapsView.projection coordinateForPoint:topCenterCoor];
    return point;
}

-(float) getRadius{
    
    CLLocationCoordinate2D centerCoordinate = [self getCenterCoordinate];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
    
    CLLocationCoordinate2D topCenterCoordinate = [self getTopCenterCoordinate];
    CLLocation * topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoordinate.latitude longitude:topCenterCoordinate.longitude];
    float radius = [centerLocation distanceFromLocation:topCenterLocation];
    return round(radius);
}

-(void)dealloc{
    //    [_mapsView removeObserver:self forKeyPath:@"myLocation"];
}


@end
