//// Header

#import "BaseViewController.h"

@interface MapsViewController : BaseViewController

@property (nonatomic, copy) void(^block)(NSString *address, CLLocationCoordinate2D coordinate);

@end
