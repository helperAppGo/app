//// Header

#import "AnswerTableViewCell.h"

@implementation AnswerTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.topView.backgroundColor = [UIColor whiteColor];
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = 17.5;
    
    self.label.font = [UIFont defaultFontWithSize:15 bold:NO];
    self.timeLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.timeLabel.textColor = [UIColor grayColor];
    self.likeLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    self.likeLabel.textColor = [UIColor grayColor];
    self.likeImageView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.botttomView.backgroundColor = [UIColor whiteColor];
                                        
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
