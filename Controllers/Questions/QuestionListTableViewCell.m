//// Header

#import "QuestionListTableViewCell.h"

@implementation QuestionListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.backgroundColor = [UIColor whiteColor];
    
    self.topView.backgroundColor = [UIColor whiteColor];
    self.avatarView.layer.cornerRadius = 25;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    
    self.nameLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    self.categoryLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.categoryLabel.textColor = COLOR(180, 180, 180);
    
    self.timeLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.timeLabel.textColor = COLOR(180, 180, 180);
    
    [self.actionBtn addTarget:self action:@selector(actionClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [self.avatarBtn addTarget:self action:@selector(avatarClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.titleLabel.font = [UIFont defaultFontWithSize:15 bold:NO];
    
    self.mediaView.contentMode = UIViewContentModeScaleAspectFill;
    self.mediaView.clipsToBounds = YES;
    [self.playBtn addTarget:self action:@selector(playClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self.locationBtn addTarget:self action:@selector(locationClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.locationLabel.font = [UIFont defaultFontWithSize:10 bold:NO];
    self.locationLabel.numberOfLines = 3;
    self.locationLabel.textColor = COLOR(180, 180, 180);
    
    self.answerLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.answerLabel.textColor = appBlueColor;
    self.answerLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(answerClicked)];
    [self.answerLabel addGestureRecognizer:tap];
    self.statusView.layer.cornerRadius  = 5;
    self.statusView.clipsToBounds = YES;
    
    self.nameLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nameClicked)];
    [self.nameLabel addGestureRecognizer:tapGesture];

    {
        self.locationLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationClicked)];
        [self.locationLabel addGestureRecognizer:tapGesture];

    }
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mediaClicked)];
    self.mediaView.userInteractionEnabled = YES;
    [self.mediaView addGestureRecognizer:tap2];
}

-(void)actionClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeAction);
    }
}

-(void)avatarClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeAvatar);
    }
}

-(void)mediaClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeImage);
    }
}

-(void)nameClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeName);
    }
}

-(void)locationClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeLocation);
    }
}

-(void)playClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeVideo);
    }
}
-(void)answerClicked{
    if (self.clickBlock) {
        self.clickBlock(self.indexPath, FeedActionTypeAnswers);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


-(void)layoutSubviews{
    [super layoutSubviews];
    self.containerView.layer.cornerRadius = 8;
    self.containerView.layer.masksToBounds = false;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 0);
    self.containerView.layer.shadowColor = COLOR(150, 150, 150).CGColor;
    self.containerView.layer.shadowOpacity = 0.7;
    self.containerView.layer.shadowRadius = 4;
}

@end
