//// Header

#import "ToolBarView.h"

@implementation ToolBarView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.adressLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.adressLabel.numberOfLines = 2;
    
}

@end
