//// Header

#import "MapQuestionViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "QuestionListTableViewCell.h"
#import "HCYoutubeParser.h"
#import "PlayerViewController.h"
#import "AnswerTableViewCell.h"
#import "CommentTableViewCell.h"
#import "GBCommentsViewController.h"


@interface MapQuestionViewController ()<GMSMapViewDelegate>{
    NSMutableArray *_markerList;
    NSMutableArray *_mapList;
    BOOL _showMyLocation;
    BOOL _completeMap;
    QuestionModel *_selectModel;
    GMSMarker *_selectMarker;
    float _zooming;
    NSMutableArray *_comments;
}

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *bgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topH;
@property (weak, nonatomic) IBOutlet UIButton *hidebtn;


@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIButton *avatartBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;
@property (weak, nonatomic) IBOutlet UIButton *btn;


@end

@implementation MapQuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _titleLabel.text = trMap;
    
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.settings.compassButton = YES;
    _mapView.delegate = self;
    [_mapView addObserver:self
                forKeyPath:@"myLocation"
                   options:(NSKeyValueObservingOptionNew |
                            NSKeyValueObservingOptionOld)
                   context:NULL];
    _markerList = @[].mutableCopy;
    _mapList = @[].mutableCopy;
    
    self.detailView.backgroundColor = [UIColor whiteColor];
    self.detailView.layer.cornerRadius = 5;
    self.detailView.alpha = 0;
    
    self.detailView.layer.cornerRadius = 8;
    self.detailView.layer.masksToBounds = false;
    self.detailView.layer.shadowOffset = CGSizeMake(0, 0);
    self.detailView.layer.shadowColor = COLOR(150, 150, 150).CGColor;
    self.detailView.layer.shadowOpacity = 0.7;
    self.detailView.layer.shadowRadius = 4;
    
    
    
    self.avatarView.layer.cornerRadius = 25;
    self.avatarView.clipsToBounds = YES;
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.nameLabel.textColor = COLOR(56, 56, 56);
    self.questionLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    
    self.answerLabel.textColor = appBlueColor;
    self.answerLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    [self.btn addTarget:self action:@selector(openAnswerList) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)openAnswerList{
    if (_selectModel) {
        [self openAnswers:_selectModel];
    }
}

-(void)openAnswers:(QuestionModel *)model{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:model.objId itemType:ECommentItemTypeAnswer];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - MapView delegate methods

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self hideDetails];
}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    if (position.zoom >= _zooming  && _zooming > 0) {
        return;
    }
    if (position.zoom < 4) {
        _completeMap = YES;
    }
    
    if (!_completeMap) {
        [self changePosition];
    }
    
}

-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    if ([marker isEqual:_selectMarker]) {
        return nil;
    }
    [self generateMarker:_selectMarker url:_selectModel.avatar selected:NO];
    _selectMarker = marker;
    _selectModel = marker.userData;
    [self generateMarker:marker url:_selectModel.avatar selected:YES];
    [self showDetails];
    return nil;
}

-(void)userLocationClicked{
    [_mapView animateToLocation:_mapView.myLocation.coordinate];
    [self hideDetails];
}

-(BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView{
    [self userLocationClicked];
    return YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (_showMyLocation) {
        if (_mapList.count == 0) {
            [self getMapsItems];
        }
        
        return;
    }
    if ([keyPath isEqualToString:@"myLocation"]) {
        _showMyLocation = YES;
        GMSCameraPosition *position = [GMSCameraPosition cameraWithTarget:_mapView.myLocation.coordinate zoom:15];
        [_mapView setCamera:position];
    }
}

-(void)hideDetails{
    [UIView animateWithDuration:0.4 animations:^{
        self.detailView.alpha = 0;
    }];
    [self generateMarker:_selectMarker url:_selectModel.avatar selected:NO];
}

-(void)showDetails{
    [[SettingsManager instance] anySound];
    [self.avatarView sd_setImageWithURL:[NSURL URLWithString:_selectModel.avatar]];
    self.nameLabel.text = _selectModel.name;
    self.questionLabel.text = _selectModel.text;
    self.answerLabel.text =  _selectModel.answerString;
    
    [UIView animateWithDuration:0.6 animations:^{
        self.detailView.alpha = 1;
        [self.view layoutIfNeeded];
    }];
}

-(void)showMapItems{
    for (GMSMarker *marker in _markerList) {
        marker.map = nil;
    }
    [_markerList removeAllObjects];
    for (QuestionModel *model in _mapList) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(model.geoLat, model.geoLon);
        marker.map = _mapView;
        marker.userData = model;
        [self generateMarker:marker url:model.avatar selected:NO];
        [_markerList addObject:marker];
    }
}


-(void)getMapsItems{
    [self changePosition];
}


-(void)changePosition{
    CLLocationCoordinate2D center = [self getCenterCoordinate];
    NSDictionary *params = @{@"Lat":@(center.latitude),
                             @"Lon":@(center.longitude),
                             @"Radius":@([self getRadius])};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"questions/map" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            response = response[@"response"];
            [_mapList removeAllObjects];
            for (NSDictionary *dict in response) {
                QuestionModel *model = [[QuestionModel alloc] initWithDict:dict];
                [_mapList addObject:model];
            }
            _zooming = _mapView.camera.zoom;
            [weakSelf showMapItems];
        } else{
            
        }
    }];
}


-(void)generateMarker:(GMSMarker *)marker url:(NSString *)url selected:(BOOL)select{
    UIView *gasView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 64, 81)];
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 60, 60)];
    logo.layer.cornerRadius = 30;
    logo.clipsToBounds = YES;
    [logo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"avatar_on_map"]];
    [gasView addSubview:logo];
    if (select) {
        gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_when_tab"]];
    }else{
        gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_when_tabWhite"]];
        
    }
    marker.icon = [self imageWithView:gasView];
}

-(UIImage *)imageWithView:(UIView *)view{
    if ([UIScreen.mainScreen respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen.scale);
        
    } else{
        UIGraphicsBeginImageContext(view.frame.size);
        
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(CLLocationCoordinate2D)getCenterCoordinate {
    CGPoint centerPoint = self.mapView.center;
    CLLocationCoordinate2D centerCoordinate = [self.mapView.projection coordinateForPoint:centerPoint];
    return centerCoordinate;
}

-(CLLocationCoordinate2D)getTopCenterCoordinate{
    CGPoint topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0, 0) fromView:self.mapView];
    
    CLLocationCoordinate2D point = [self.mapView.projection coordinateForPoint:topCenterCoor];
    return point;
}

-(float) getRadius{
    
    CLLocationCoordinate2D centerCoordinate = [self getCenterCoordinate];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
    
    CLLocationCoordinate2D topCenterCoordinate = [self getTopCenterCoordinate];
    CLLocation * topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoordinate.latitude longitude:topCenterCoordinate.longitude];
    float radius = [centerLocation distanceFromLocation:topCenterLocation];
    return round(radius);
}

-(void)dealloc{
    [_mapView removeObserver:self forKeyPath:@"myLocation"];
}

@end
