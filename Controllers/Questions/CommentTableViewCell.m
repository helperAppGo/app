//// Header

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.textField.font = [UIFont defaultFontWithSize:13 bold:NO];
    self.textField.layer.cornerRadius = 3;
    self.textField.layer.borderColor = COLOR(230, 230, 230).CGColor;
    self.textField.layer.borderWidth = 0.6;
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.sendbtn addTarget:self action:@selector(sendClicked) forControlEvents:UIControlEventTouchUpInside];
    self.textField.placeholder = @"Your answer";
}

-(void)sendClicked{
    if (self.sendBlock) {
        self.sendBlock(self.textField.text);
    }
    self.textField.text = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
