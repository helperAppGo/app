//// Header

#import <Foundation/Foundation.h>

@interface QuestionAnswerModel : NSObject

@property (nonatomic, assign) NSInteger likeCount;
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger questionId;
@property (nonatomic, assign) NSInteger postId;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger liked;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *dateString;
-(NSString *)likeCountString;

-(id)initWithDict:(NSDictionary *)dict;
@end


@interface QuestionModel : NSObject

@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger hideLocation;
@property (nonatomic, assign) NSInteger answerCount;
@property (nonatomic, assign) NSInteger likesCount;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) NSInteger userId;
@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, strong) NSArray *mediaObjects;

@property (nonatomic, assign) double geoLat;
@property (nonatomic, assign) double geoLon;

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *timeString;

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *address;

-(NSString *)answerString;
-(NSString *)distanceString;

-(id)initWithDict:(NSDictionary *)dict;
  



@end
