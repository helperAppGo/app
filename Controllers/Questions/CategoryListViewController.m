//// Header

#import "CategoryListViewController.h"

@interface CategoryListViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *_list;
    UIRefreshControl *_refreshControl;
    NSMutableDictionary *_actions;
    NSMutableDictionary *_openSections;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CategoryListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _openSections  = @{}.mutableCopy;
    _titleLabel.text = trQuestionSelCat;
    _list = @[].mutableCopy;
    [_list setArray:self.info[@"category"]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [UIView new];
    
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(getList) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
    if (_list.count ==  0) {
        [self getList];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getList{
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"questions/categories" params:@{} callback:^(id response, ErrorObj *error) {
        if (response) {
            [_list removeAllObjects];
            NSArray *items = response[@"response"];
            NSMutableArray *objects = @[].mutableCopy;
            for (NSDictionary *dict in items) {
                QuestionCategoryObj *obj = [[QuestionCategoryObj alloc] initWithDict:dict];
                [objects addObject:obj];
            }
            [_list setArray:objects];
            [weakSelf.tableView reloadData];
        }
    }];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _list.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_openSections[@(section)]) {
        return 2;
    } else{
        return 1;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"type"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"type"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.numberOfLines  = 10;
        cell.textLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
        QuestionCategoryObj *obj = _list[indexPath.section];
        cell.textLabel.text = obj.desc ? obj.desc : @"Эта каегория подходит для\n- поиска друзей\n- знакомства\n- поиска информации\n- просбы о помощи";
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"list"];
    }
    QuestionCategoryObj *obj = _list[indexPath.section];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.textLabel.text = obj.name;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        NSIndexPath *path = [NSIndexPath indexPathForRow:1 inSection:indexPath.section];
        if (_openSections[@(indexPath.section)]) {
            [_openSections removeObjectForKey:@(indexPath.section)];
            [tableView deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
        } else{
            _openSections[@(indexPath.section)] = @(1);
            [tableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        return;
    }
    BaseViewController *controller  = [self createController:@"AddQuestionViewController"];
    controller.info = @{@"obj":_list[indexPath.section]};
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    
}
@end
