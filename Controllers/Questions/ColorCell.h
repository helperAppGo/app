//// Header

#import <UIKit/UIKit.h>

@interface ColorCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *colorView;
@end
