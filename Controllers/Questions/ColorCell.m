//// Header

#import "ColorCell.h"

@implementation ColorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.colorView.layer.borderColor = appGreenColor.CGColor;
}

@end
