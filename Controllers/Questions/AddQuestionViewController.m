//// Header

#import "AddQuestionViewController.h"
#import "ColorCell.h"
#import "SAMTextView.h"
#import "UIImageView+Additions.h"
#import "FDTakeController.h"
#import "MapsViewController.h"
#import "QuestionCategoryObj.h"

@interface AddQuestionViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, FDTakeDelegate, UITextViewDelegate>{
    NSInteger _colorIndex;
    CLLocationCoordinate2D _coordinate;
    UIColor *_contentTextColor;
    QuestionCategoryObj *_category;
    BOOL _isPost;
}

@property (weak, nonatomic) IBOutlet SAMTextView *samTextView;
@property (nonatomic, strong) FDTakeController *takeController;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIView *fileView;
@property (weak, nonatomic) IBOutlet UIButton *deleteIcon;
@property (weak, nonatomic) IBOutlet UIImageView *attachView;
@property (weak, nonatomic) IBOutlet UIButton *fileBtn;
@property (weak, nonatomic) IBOutlet UIButton *locationBtn;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIButton *blackBtn;
@property (weak, nonatomic) IBOutlet UIButton *whiteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *categoryTop;


@end

@implementation AddQuestionViewController

- (void)viewDidLoad {
    self.view.backgroundColor = COLOR(240, 240, 240);
    [self.samTextView addObserver:self forKeyPath:@"contentSize" options:(NSKeyValueObservingOptionNew) context:NULL];
    [super viewDidLoad];
    _titleLabel.text = trCreate;
    self.samTextView.backgroundColor = [UIColor clearColor];
    _locationLabel.text = nil;
    
    _contentTextColor = [UIColor blackColor];
    NSInteger post = [self.info[@"post"] integerValue];
    _isPost = post == 1;
    if (_isPost) {
        self.categoryTop.constant = -40;
        self.categoryLabel.hidden = YES;
        self.attachTop.constant = -30;
        self.locationLabel.hidden = self.locationBtn.hidden =  YES;
    } else{
        self.categoryLabel.font =  [UIFont defaultFontWithSize:14 bold:NO];
        self.categoryLabel.text = _category.name; //self.info[@"name"];
        _category = self.info[@"obj"];
    }
    self.samTextView.textContainer.maximumNumberOfLines = 7;
    self.samTextView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;

    
    
    UINib *nib = [UINib nibWithNibName:@"ColorCell" bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:@"list"];
    UICollectionViewFlowLayout * _verticalLayout = [[UICollectionViewFlowLayout alloc] init];
    _verticalLayout.minimumInteritemSpacing = 10;
    _verticalLayout.minimumLineSpacing = 10;
    _verticalLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _verticalLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.samTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.samTextView.textColor = [UIColor whiteColor];
    self.samTextView.font = [UIFont defaultFontWithSize:25 bold:YES];
    self.samTextView.textAlignment = NSTextAlignmentCenter;
    self.samTextView.delegate = self;
    self.samTextView.scrollEnabled = NO;
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView setCollectionViewLayout:_verticalLayout animated:YES];

    self.collectionView.showsHorizontalScrollIndicator = NO;
    [self.collectionView reloadData];
    [self changeTextColor];
    
    _attachView.superview.hidden = YES;
    self.deleteIcon.backgroundColor = [UIColor clearColor];
    [self.deleteIcon addTarget:self action:@selector(deleteClicked) forControlEvents:UIControlEventTouchUpInside];
    self.samTextView.alwaysBounceHorizontal = YES;
    
    UIColor *color = [UIColor whiteColor];
    UIColor *border = COLOR(120, 120, 120);
    self.locationLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.locationBtn.backgroundColor = color;
    self.locationBtn.layer.borderColor = border.CGColor;
    self.locationBtn.layer.borderWidth = 0.7;
    self.locationBtn.layer.cornerRadius = 4;
    [StyleManager styleBtn:self.locationBtn titleColor:appBlueColor];
    [StyleManager styleBtn:self.locationBtn title:trAddQuestionMap];
    [self.locationBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.locationBtn.titleLabel.font = [UIFont defaultFontWithSize:12 bold:YES];
    [self.locationBtn addTarget:self action:@selector(openMap) forControlEvents:UIControlEventTouchUpInside];
    
    self.fileBtn.backgroundColor = color;
    self.fileBtn.layer.cornerRadius = 4;
    self.fileBtn.layer.borderColor = border.CGColor;
    self.fileBtn.layer.borderWidth = 0.9;
    [StyleManager styleBtn:self.fileBtn titleColor:appBlueColor];
    [StyleManager styleBtn:self.fileBtn title:trUploadFile];
    [self.fileBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.fileBtn.titleLabel.font = [UIFont defaultFontWithSize:12 bold:YES];
    [self.fileBtn addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [self.blackBtn addTarget:self action:@selector(blackClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.whiteBtn addTarget:self action:@selector(whiteClicked) forControlEvents:UIControlEventTouchUpInside];
    
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    UITextView *tv = object;
    CGFloat topCorrect = ([tv bounds].size.height - [tv contentSize].height * [tv zoomScale])/2.0;
    topCorrect = ( topCorrect < 0.0 ? 0.0 : topCorrect );
    tv.contentOffset = (CGPoint){.x = 0, .y = -topCorrect};
}

-(void)blackClicked{
    _contentTextColor = [UIColor blackColor];
    [self changeTextColor];
}

-(void)whiteClicked{
    _contentTextColor = [UIColor whiteColor];
    [self changeTextColor];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self changeNavbar];
}

-(void)changeBgColor{
    NSString *color = [NSString  stringWithFormat:@"color%@",@(_colorIndex)];
    UIImageView *imageView = (UIImageView *)self.samTextView.superview;
    UIImage *image = [UIImage imageNamed:color];
    image = [self imageWithImage:image scaledToSize:CGSizeMake(imageView.width, imageView.height)];
    imageView.backgroundColor = (_colorIndex == 0) ? [UIColor whiteColor] : [UIColor colorWithPatternImage:image];
    [self changeTextColor];
}

-(void)changeTextColor{
    
    NSString *placeholder = _isPost ? trCreatePost : trCreateQuestion;
    UIColor *color = _colorIndex == 0 ? [UIColor blackColor] : [UIColor whiteColor];
    self.blackBtn.hidden = self.whiteBtn.hidden = _colorIndex == 0;
    _contentTextColor = _colorIndex == 0 ? [UIColor blackColor] : _contentTextColor;
    
    NSMutableParagraphStyle *centeredParagraphStyle = [[NSMutableParagraphStyle alloc] init];
    centeredParagraphStyle.alignment = NSTextAlignmentCenter;
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName:color, NSFontAttributeName: [UIFont defaultFontWithSize:25 bold:YES],NSParagraphStyleAttributeName:centeredParagraphStyle}];
    self.samTextView.textColor = _contentTextColor;
    self.samTextView.attributedPlaceholder  = str;
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self changeBgColor];
}

-(void)takePhoto{
    if (_colorIndex >0) {
        UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:nil message:@"Удалить фон ?" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:cancelAction];
        
        UIAlertAction *done = [UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self take];
        }];
        [alertController addAction:done];
        [self presentViewController:alertController animated:YES completion:nil];
        return;
    }
    [self take];
}

-(void)take{
    _colorIndex = 0;
    [self.collectionView reloadData];
    [self changeBgColor];
    
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    self.takeController.delegate = self;
    self.takeController.type = 1;
    [self.takeController takePhotoOrChooseFromLibrary];
}

-(void)textViewDidChange:(UITextView *)textView{
    [self changeNavbar];
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    
    //NSLog(@"%@",textView.text);
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    [self updateTextFont:self.samTextView];
    return textView.text.length + (text.length - range.length) <= 120;
    
}

-(void)updateTextFont:(UITextView *)textView{
    if (self.samTextView.text.length == 0) {
        return;
    }
    
//    CGSize textViewSize = textView.frame.size;
//    CGFloat fixedWidth = textViewSize.width;
//    CGSize expectSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//
//    UIFont *expectFont = textView.font;
//    CGSize size = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//
//    if (expectSize.height > textViewSize.height) {
//
//
//        while (size.height > textViewSize.height) {
//           float fontSize = textView.font.pointSize - 1;
//            expectFont = [UIFont defaultFontWithSize:fontSize bold:NO];
//            textView.font = expectFont;
//        }
//    } else {
//        while (size.height < textViewSize.height) {
//            float fontSize = textView.font.pointSize + 1;
//            expectFont = [UIFont defaultFontWithSize:fontSize bold:NO];
//            textView.font = expectFont;
//        }
//        textView.font = expectFont;
//    }
}

-(UIImage *)imageWithView:(UIView *)view{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return img;
}

-(void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info{
    NSData *data = UIImagePNGRepresentation(photo);
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(photo, 1/k);
    }
    UIImage *image = [UIImage imageWithData:data];
    _attachView.image = image;
    _attachView.superview.hidden = NO;
    [self changeNavbar];
}

-(void)changeNavbar{
    if (_isPost) {
        if (self.samTextView.text.length || _attachView.image) {
            UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:trReady style:UIBarButtonItemStylePlain target:self action:@selector(send)];
            self.navigationItem.rightBarButtonItems = @[item];
        } else{
            self.navigationItem.rightBarButtonItems = nil;
        }
        return;
    }
    
    if (self.samTextView.text.length) {
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:trReady style:UIBarButtonItemStylePlain target:self action:@selector(send)];
        self.navigationItem.rightBarButtonItems = @[item];
    } else{
        self.navigationItem.rightBarButtonItems = nil;
    }
}
-(void)send{
    [self.view endEditing:YES];
    if (_colorIndex == 0) {
        if (_attachView.image) {
            [self uploadFile:_attachView.image];
        } else{
            [self createQuestion:0];
        }
        return;
    }
    self.whiteBtn.alpha = self.blackBtn.alpha = 0;
    UIImage *image = [self imageWithView:self.samTextView.superview];
    self.whiteBtn.alpha = self.blackBtn.alpha = 1;
    [self uploadFile:image];
    
}

-(void)createQuestion:(NSInteger)fileId{
    NSInteger categotyId = _category.objId;
    NSString *text = self.samTextView.text ? self.samTextView.text : @"";
    if (_colorIndex > 0) {
        text = @"";
    }
    if (_isPost) {
        NSMutableDictionary  *dict = @{}.mutableCopy;
        CDUser *user = [[SettingsManager instance] currentUser];
        dict[@"id_wall"] = @(user.objId);
        dict[@"text"] = text;
        dict[@"link"] = @"";
        if (fileId > 0) {
            dict[@"files"] = [NSString stringWithFormat:@"%@",@(fileId)];
        }
        __weak typeof(self) weakSelf = self;
        [self showLoader];
        [[ApiManager sharedManager] postRequest:@"posts" params:dict callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
        
//        [self sendPOSTRequest:@"posts" parameters:parameters success:success failure:failure];
    } else{
        NSDictionary *dict = @{@"Lat":@(_coordinate.latitude),
                               @"Lon":@(_coordinate.longitude),
                               @"Text":text,
                               @"Id_category":@(categotyId)};
        if (fileId > 0) {
            dict = @{@"Lat":@(_coordinate.latitude),
                     @"Lon":@(_coordinate.longitude),
                     @"Text":text,
                     @"Id_category":@(categotyId),
                     @"files":[NSString stringWithFormat:@"%@",@(fileId)]};
            
            
        }
        
        __weak typeof(self) weakSelf = self;
        [self showLoader];
        [[ApiManager sharedManager] postRequest:@"questions" params:dict callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                [weakSelf.navigationController popToRootViewControllerAnimated:YES];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
            
        }];
    }
    
    
}

-(void)uploadFile:(UIImage*)image{
    
    NSData *data = UIImagePNGRepresentation(image);
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] uploadPhoto:data fileName:@"media.png" callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            NSInteger fileId = [response[@"id"] integerValue];
            [weakSelf createQuestion:fileId];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
        
    }];
    
}


-(void)deleteClicked{
    _attachView.image = nil;
    _attachView.superview.hidden = YES;
}

-(void)openMap{
    MapsViewController *controller = (MapsViewController *)[self createController:@"MapsViewController"];
    controller.type = 1;
    controller.info = @{};
    __weak typeof(self) weakSelf = self;
    [controller setBlock:^(NSString *address, CLLocationCoordinate2D coordinate) {
        [weakSelf changeAddress:address coordinate:coordinate];
    }];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeAddress:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate{
    _locationLabel.text = address;
    _coordinate = coordinate;
}

#pragma mark - UICollectionView delegate methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 19;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ColorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"list" forIndexPath:indexPath];
    NSString *color = [NSString stringWithFormat:@"color%@",@(indexPath.row)];
    cell.colorView.layer.borderWidth = (indexPath.row == _colorIndex) ? 2 : 0;
    cell.contentView.backgroundColor = self.view.backgroundColor;
    if (indexPath.row > 0) {
        cell.colorView.layer.contents = (id)[UIImage imageNamed:color].CGImage;
    } else{
        cell.colorView.layer.contents = nil;
        cell.colorView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float h = self.collectionView.frame.size.height;
    return CGSizeMake(h, h);
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self changeFonClicked:indexPath.row];
}

-(void)changeFonClicked:(NSInteger)index{
    if (_attachView.superview.hidden == NO) {
        UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:nil message:trDeleteFile preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:trCancel style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:cancelAction];
        
        UIAlertAction *done = [UIAlertAction actionWithTitle:trYes style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self changeFon:index];
        }];
        [alertController addAction:done];
        
        [self presentViewController:alertController animated:YES completion:nil];
        return;
        
    } else{
        [self changeFon:index];
    }
}

-(void)changeFon:(NSInteger)index{
    [self deleteClicked];
    _colorIndex = index;
    [self.collectionView reloadData];
    [self changeBgColor];
}

@end
