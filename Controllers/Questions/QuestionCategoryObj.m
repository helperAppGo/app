//// Header

#import "QuestionCategoryObj.h"

@implementation QuestionCategoryObj

-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if (self) {
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.order = [NULL_TO_NIL(dict[@"order"]) integerValue];
        self.name = NULL_TO_NIL(dict[@"name"]);
        self.desc = NULL_TO_NIL(dict[@"descr"]);
    }
    return self;
}

@end
