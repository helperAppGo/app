//// Header

#import "CreateQuestionsViewController.h"
#import "SAMTextView.h"
#import "ToolBarView.h"
#import "MapsViewController.h"
#import "FDTakeController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface CreateQuestionsViewController ()<FDTakeDelegate, UITextViewDelegate>{
    CLLocationCoordinate2D _coordinate;
    ToolBarView *_toolView;
}

@property (weak, nonatomic) IBOutlet SAMTextView *textView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteIcon;
@property (weak, nonatomic) IBOutlet UIImageView *preView;
@property (strong, nonatomic) FDTakeController *takeController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topH;

@end

@implementation CreateQuestionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel.text = trCreate;
    self.view.backgroundColor = COLOR(251, 251, 251);
    self.textView.backgroundColor = [UIColor whiteColor];
    
    self.categoryLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.categoryLabel.text = self.info[@"name"];
    
    self.textView.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.textView.placeholder = trQuestionCreatePlaceholder;
    self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.textView.layer.cornerRadius = 8;
    self.textView.layer.borderColor = [UIColor grayColor].CGColor;
    self.textView.layer.borderWidth = 0.4;
    self.textView.delegate = self;
    
    _toolView = [ToolBarView loadFromNib];
    _toolView.adressLabel.text = nil;
    [_toolView.mapBtn addTarget:self action:@selector(openMap) forControlEvents:UIControlEventTouchUpInside];
    [_toolView.mediaBtn addTarget:self action:@selector(takePhoto) forControlEvents:UIControlEventTouchUpInside];
    self.textView.inputAccessoryView = _toolView;
    _coordinate = [[GBLocationManager sharedManager] currentLocation].coordinate;
    self.deleteIcon.backgroundColor = [UIColor clearColor];
    [self.deleteIcon addTarget:self action:@selector(deleteClicked) forControlEvents:UIControlEventTouchUpInside];
    self.preView.superview.hidden = YES;
    self.preView.backgroundColor = [UIColor clearColor];
    [self getAddress];
    if (IS_IOS11) {
        self.topH.constant = 5;
        //        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        // Fallback on earlier versions
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getAddress{
    float lat = _coordinate.latitude;
    float lon = _coordinate.longitude;
    __weak typeof(self) weakSelf = self;
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(lat, lon) completionHandler:^(GMSReverseGeocodeResponse * response, NSError * error) {
        if (response.results.count) {
            GMSAddress *address = response.results[0];
            _toolView.adressLabel.text = [address.lines componentsJoinedByString:@","];
        } else{
            [weakSelf showErrorMessage:error.localizedDescription];
        }
        
    }];
}

-(void)changeNavbar{
    if (self.textView.text.length) {
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:trReady style:UIBarButtonItemStylePlain target:self action:@selector(send)];
        self.navigationItem.rightBarButtonItems = @[item];
    } else{
        self.navigationItem.rightBarButtonItems = nil;
    }
}

-(void)send{
    [self.view endEditing:YES];
    if (self.preView.image) {
        [self uploadFile:self.preView.image];
    } else{
        [self createQuestion:0];
    }
}

-(void)createQuestion:(NSInteger)fileId{
    //questions
    NSInteger categotyId = [self.info[@"id"] integerValue];
    //Id_category
    /*
     Text
     Lon
     Lat
     */
    NSDictionary *dict = @{@"Lat":@(_coordinate.latitude),
                           @"Lon":@(_coordinate.longitude),
                           @"Text":self.textView.text,
                           @"Id_category":@(categotyId)};
    if (fileId > 0) {
        dict = @{@"Lat":@(_coordinate.latitude),
                 @"Lon":@(_coordinate.longitude),
                 @"Text":self.textView.text,
                 @"Id_category":@(categotyId),
                 @"files":[NSString stringWithFormat:@"%@",@(fileId)]};
        
        
    }
    
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] postRequest:@"questions" params:dict callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
        
    }];
}

-(void)uploadFile:(UIImage*)image{
    
    NSData *data = UIImagePNGRepresentation(image);
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] uploadPhoto:data fileName:@"media.png" callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            NSInteger fileId = [response[@"id"] integerValue];
            [weakSelf createQuestion:fileId];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
        
    }];
    
}


-(void)deleteClicked{
    self.preView.image = nil;
    self.preView.superview.hidden = YES;
}

-(void)openMap{
    MapsViewController *controller = (MapsViewController *)[self createController:@"MapsViewController"];
    controller.type = 1;
    controller.info = @{};
    __weak typeof(self) weakSelf = self;
    [controller setBlock:^(NSString *address, CLLocationCoordinate2D coordinate) {
        [weakSelf changeAddress:address coordinate:coordinate];
    }];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeAddress:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate{
    _toolView.adressLabel.text = address;
    UIImageView *imageView = [_toolView.mapBtn subviews][0];
    imageView.tintColor = [UIColor redColor];
    imageView.image= [[UIImage imageNamed:@"ic_placeholder"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    _coordinate = coordinate;
}

-(void)takePhoto{
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    self.takeController.delegate = self;
    self.takeController.type = 1;
    [self.takeController takePhotoOrChooseFromLibrary];
}

-(void)textViewDidChange:(UITextView *)textView{
    [self changeNavbar];
    NSLog(@"%@",textView.text);
}


- (void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info{
    
    NSData *data = UIImagePNGRepresentation(photo);
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(photo, 1/k);
    }
    UIImage *image = [UIImage imageWithData:data];
    self.preView.image = image;
    self.preView.superview.hidden = NO;
}




@end
