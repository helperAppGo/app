//// Header

#import "MapsViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "SMCalloutView.h"
#import "AFHTTPRequestSerializer-GB.h"

#define Height 70

@interface MapsViewController ()<GMSMapViewDelegate>{
    BOOL _isFirst;
    NSMutableArray *_searchItems;
    BOOL _search;
    BOOL _showMyLocation;
    
}
@property (weak, nonatomic) IBOutlet GMSMapView *mapView;
@property (nonatomic, strong) GMSMarker *marker;
@property (nonatomic, strong) SMCalloutView *tempView;

@end

@implementation MapsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirst = YES;
    _searchItems = @[].mutableCopy;
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self addMap];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:LOCATION_UPDATED object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStylePlain target:self action:@selector(readyAction)];
    self.navigationItem.rightBarButtonItems = @[item];
}

-(void)readyAction{
    [self.view endEditing:YES];
    if (self.block) {
        self.block(_tempView.title, _marker.position);
    }
    [self back];
}


-(void)receiveNotification:(NSNotification *)notification{
    if([notification.name isEqualToString:LOCATION_UPDATED]){
        if(_isFirst){
            _isFirst = NO;
            [self showCurrentLocation];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (_showMyLocation) {
        return;
    }
    
    if ([keyPath isEqualToString:@"myLocation"]) {
        _showMyLocation = YES;
        GMSCameraPosition *position = [GMSCameraPosition cameraWithTarget:_mapView.myLocation.coordinate zoom:12];
        [_mapView setCamera:position];
    }
}

-(void)showCurrentLocation{
    CLLocation *location = [[GBLocationManager sharedManager] currentLocation];
    CLLocationCoordinate2D target = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
    _mapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:12];
    _marker.position = target;
}

-(void)addMap{
    _mapView.delegate = self;

    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.settings.compassButton = YES;
    _mapView.delegate = self;
    
    [_mapView addObserver:self
                forKeyPath:@"myLocation"
                   options:(NSKeyValueObservingOptionNew |
                            NSKeyValueObservingOptionOld)
                   context:NULL];
    _marker = [[GMSMarker alloc] init];
    _marker.map = _mapView;
 
    _tempView = [[SMCalloutView alloc] init];
    _tempView.hidden = YES;
    _tempView.title = @"";
    
//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [btn setImage:[UIImage imageNamed:@"plusWhite"] forState:UIControlStateNormal];
//    [btn roundView];
//    btn.backgroundColor = [UIColor blueColor];
//    [btn addTarget:self action:@selector(changeAddress) forControlEvents:UIControlEventTouchUpInside];
//    _tempView.rightAccessoryView = btn;
}

-(void)changeAddress{
    
}

-(void)mapView:(GMSMapView *)phomapView didChangeCameraPosition:(GMSCameraPosition *)position{
    _marker.position = position.target;
}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    [self changePostitionLat:position.target.latitude long:position.target.longitude];
}

-(void)mapView:(GMSMapView *)mapView willMove:(BOOL)gesture{
    _tempView.hidden = YES;
}

-(void)changePostitionLat:(double)lat long:(double)geoLong {
    __weak typeof(self) weakSelf = self;
    [[GMSGeocoder geocoder] reverseGeocodeCoordinate:CLLocationCoordinate2DMake(lat, geoLong) completionHandler:^(GMSReverseGeocodeResponse * response, NSError * error) {
        if (response.results.count) {
            CGPoint point = [_mapView.projection pointForCoordinate:CLLocationCoordinate2DMake(lat, geoLong)];
            GMSAddress *address = response.results[0];
            _tempView.title = [address.lines componentsJoinedByString:@","];
            [_tempView presentCalloutFromRect:CGRectMake(point.x, point.y - 45, 1, 1) inView:_mapView constrainedToView:_mapView animated:NO];
            _tempView.hidden = NO;
        } else{
            [weakSelf showErrorMessage:error.localizedDescription];
        }
        
    }];
    
    
    //    NSString *path = [NSString stringWithFormat:@"https://geocode-maps.yandex.ru/1.x/?format=json&kind=house&geocode=%@,%@",@(position.target.longitude), @(position.target.latitude)];
//    NSString *path = [NSString stringWithFormat:@"https://geocode-maps.yandex.ru/1.x/?format=json&kind=house&geocode=%@,%@",@(geoLong), @(lat)];
//
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.requestSerializer.timeoutInterval = 30;
//    [manager GET:path parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSDictionary *response = responseObject[@"response"];
//        for(NSDictionary *geoObject in response[@"GeoObjectCollection"][@"featureMember"]){
//            _address = [[AddressModel alloc] initWithYandex:geoObject];
//            CGPoint point = [_mapView.projection pointForCoordinate:CLLocationCoordinate2DMake(lat, geoLong)];
//            if (_address.streetName.length && _address.houseNumber>0) {
//                _tempView.title = [NSString stringWithFormat:@"%@, дом %@", _address.streetName, @(_address.houseNumber)];
//            } else{
//                _tempView.title = geoObject[@"GeoObject"][@"description"];
            
//            }
//            [_tempView presentCalloutFromRect:CGRectMake(point.x, point.y - 45, 1, 1) inView:_mapView constrainedToView:_mapView animated:NO];
//            _tempView.hidden = NO;
//            break;
//        }
    
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"%@",error);
//    }];
}

-(void)dealloc{
    [_mapView removeObserver:self forKeyPath:@"myLocation"];
}

@end
