//// Header

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *sendbtn;
@property (nonatomic, copy) void(^sendBlock)(NSString *comment);
@end
