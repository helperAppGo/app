//// Header

#import <UIKit/UIKit.h>

@interface RecomendationCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *relationBtn;
@property (weak, nonatomic) IBOutlet UILabel *relationLabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, copy) void(^clickBlock)(NSInteger type, NSInteger row);
@end
