//// Header

#import "GroupListTableViewCell.h"

@implementation GroupListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.backgroundColor = [UIColor whiteColor];
    self.containerView.layer.borderColor = COLOR(230, 230, 230).CGColor;
    self.containerView.layer.borderWidth = 0.8;
    self.containerView.layer.cornerRadius = 6;
    
    
    self.topView.backgroundColor = [UIColor whiteColor];
    self.avatarView.layer.cornerRadius = self.avatarView.width/2;
    self.avatarView.contentMode = UIViewContentModeScaleAspectFill;
    self.avatarView.clipsToBounds = YES;
    
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.categoryLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    
    self.label.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    
    self.locationLabel.font = [UIFont defaultFontWithSize:8 bold:NO];
    
    self.memberLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.memberLabel.textColor = [UIColor grayColor];
    self.memberView.image = [UIImage imageNamed:@"followersIcon"];
    self.locationIcon.image = [UIImage imageNamed:@"Location"];

    [self.memberBtn addTarget:self action:@selector(memberClicked) forControlEvents:UIControlEventTouchUpInside];

    [self.rightBtn addTarget:self action:@selector(rightClicked) forControlEvents:UIControlEventTouchUpInside];
}

-(void)rightClicked{
    if (self.block) {
        self.block(self.indexPath, 1);
    }
}

-(void)memberClicked{
    if (self.block) {
        self.block(self.indexPath, 2);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
