//// Header

#import "GroupListModel.h"
#import "UIFont+Additions.h"
#import "CDCity+CoreDataClass.h"
#import "NSManagedObject+ActiveRecord.h"

@implementation GroupListModel
-(id)initWithDict:(NSDictionary *)dict{
    self = [super init];
    if(self){
        self.address = NULL_TO_NIL(dict[@"address"]);
        self.canPost = [NULL_TO_NIL(dict[@"can_post"]) integerValue];
        self.followersCount = [NULL_TO_NIL(dict[@"count_followers"]) integerValue];
        
        self.distance = [NULL_TO_NIL(dict[@"can_post"]) doubleValue];
        self.fileAva = [NULL_TO_NIL(dict[@"can_post"]) integerValue];
        self.fileCover = [NULL_TO_NIL(dict[@"can_post"]) integerValue];
        
        self.objId = [NULL_TO_NIL(dict[@"id"]) integerValue];
        self.categoryId = [NULL_TO_NIL(dict[@"id_category"]) integerValue];
        self.cityId = [NULL_TO_NIL(dict[@"id_city"]) integerValue];
        self.isRequested = [NULL_TO_NIL(dict[@"is_requested"]) integerValue];
        
        self.isAdmin = [NULL_TO_NIL(dict[@"is_admin"]) integerValue];
        self.isBanned = [NULL_TO_NIL(dict[@"is_banned"]) integerValue];
        self.isMember = [NULL_TO_NIL(dict[@"is_member"]) integerValue];
        
        self.geoLat = [NULL_TO_NIL(dict[@"lat"]) doubleValue];
        self.geoLong = [NULL_TO_NIL(dict[@"lon"]) doubleValue];
        
        self.name = NULL_TO_NIL(dict[@"name"]);
        self.avatar = NULL_TO_NIL(dict[@"path_ava"]);
        self.cover = NULL_TO_NIL(dict[@"path_cover"]);
        if (self.avatar.length == 0) {
            self.avatar = self.cover;
        }
        self.desc = NULL_TO_NIL(dict[@"short_description"]);
        self.fullDesc = NULL_TO_NIL(dict[@"full_description"]);
        self.type = [NULL_TO_NIL(dict[@"type"]) integerValue];
        self.web = NULL_TO_NIL(dict[@"web_site"]);
        
        if (self.type == 0){
            self.typeString = @"Закрита група";
        } else if (self.type == 1) {
            self.typeString = @"Відкрита група";
        } else if (self.type == 2){
            self.typeString = @"Публічна сторінка";
        } else if (self.type == 3) {
            self.typeString = @"Афіша";
        } else {
            self.typeString = @"";
        }
    }
    
    return self;
}

-(BOOL)isEqual:(GroupListModel *)object{
    return object.objId == self.objId;
}

-(NSString *)folowerCountString{
//    return [NSString stringWithFormat:@"%@",@(self.type)];
    if (self.followersCount > 1000) {
        return [NSString stringWithFormat:@"%.02fK",self.followersCount/1000.f];
    }
    return [NSString stringWithFormat:@"%@",@(self.followersCount)];
}

-(NSString *)cityString{
    CDCity *city = [CDCity find:@{@"objId":@(self.cityId)}];
    if (city) {
        return city.name;
    }
    return @"";
}


@end
