//// Header

#import "GroupMapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "QuestionListTableViewCell.h"
#import "AnswerTableViewCell.h"
#import "HCYoutubeParser.h"
#import "PlayerViewController.h"
#import "CommentTableViewCell.h"
#import "GroupFeedsViewController.h"


@interface GroupMapViewController ()<GMSMapViewDelegate>{
    
    NSMutableArray *_markerList;
    NSMutableArray *_mapList;
    
    float _zooming;
    BOOL _completeMap;
    GroupListModel *_selectModel;
    GMSMarker *_selectMarker;
    
    BOOL _showMyLocation;
    NSMutableArray *_comments;
}
@property (weak, nonatomic) IBOutlet UIView *bgView;



@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;


@property (weak, nonatomic) IBOutlet UIView *detailView;
@property (weak, nonatomic) IBOutlet UIButton *avatartBtn;
@property (weak, nonatomic) IBOutlet UIImageView *avatarView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UILabel *followerLabel;
@property (weak, nonatomic) IBOutlet UIButton *memberBtn;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;



@end

@implementation GroupMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[SettingsManager instance] anySound];
    _titleLabel.text = trMap;
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    self.mapView.settings.compassButton = YES;
    _mapView.delegate = self;
    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:(NSKeyValueObservingOptionNew |
                           NSKeyValueObservingOptionOld)
                  context:NULL];
    _markerList = @[].mutableCopy;
    _mapList = @[].mutableCopy;
    
    self.detailView.backgroundColor = [UIColor whiteColor];
    self.detailView.layer.cornerRadius = 5;
    self.detailView.alpha = 0;
    
    self.detailView.layer.cornerRadius = 8;
    self.detailView.layer.masksToBounds = false;
    self.detailView.layer.shadowOffset = CGSizeMake(0, 0);
    self.detailView.layer.shadowColor = COLOR(150, 150, 150).CGColor;
    self.detailView.layer.shadowOpacity = 0.7;
    self.detailView.layer.shadowRadius = 4;
    
    self.avatarView.layer.cornerRadius = 25;
    self.avatarView.clipsToBounds = YES;
    self.nameLabel.font = [UIFont defaultFontWithSize:14 bold:YES];
    self.nameLabel.textColor = COLOR(56, 56, 56);
    self.questionLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    
    self.typeLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.followerLabel.textColor = appBlueColor;
    self.followerLabel.font = [UIFont defaultFontWithSize:13 bold:NO];
    
    [self.btn addTarget:self action:@selector(openGroupFeed) forControlEvents:UIControlEventTouchUpInside];
    [self getMapsItems];
    
    //Записи
    [self.btn addTarget:self action:@selector(openGroupFeed) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openGroupFeed{
    if (_selectModel) {
        [self openFeeds:_selectModel];
    }
}

-(void)openFeeds:(GroupListModel *)model{
    GroupFeedsViewController *controller = (GroupFeedsViewController *)[self createController:@"GroupFeedsViewController"];
    controller.type = 1;
    __weak typeof(self) weakSelf = self;
    [controller setChangeBlock:^(GroupListModel *model2) {
        [weakSelf replace:model2];
    }];
    controller.info = @{@"group":model};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)replace:(GroupListModel *)model{
    NSInteger index = [_mapList indexOfObject:model];
    [_mapList replaceObjectAtIndex:index withObject:model];
}


#pragma mark - MapView delegate methods
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self hideDetails];
}

-(void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position{
    if (position.zoom >= _zooming  && _zooming > 0) {
        return;
    }
    if (position.zoom < 4) {
        _completeMap = YES;
    }
    
    if (!_completeMap) {
        [self changePosition];
    }
    
}

-(UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker{
    if ([marker isEqual:_selectMarker]) {
        return nil;
    }
    [self generateMarker:_selectMarker url:_selectModel.avatar selected:NO];
    _selectMarker = marker;
    _selectModel = marker.userData;
    
    [self generateMarker:marker url:_selectModel.avatar selected:YES];
    [self showDetails];
    return nil;
}

-(void)userLocationClicked{
    [_mapView animateToLocation:_mapView.myLocation.coordinate];
    [self hideDetails];
}

-(BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView{
    [self userLocationClicked];
    return YES;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (_showMyLocation) {
        if (_mapList.count == 0) {
            [self getMapsItems];
        }
        
        return;
    }
    if ([keyPath isEqualToString:@"myLocation"]) {
        _showMyLocation = YES;
        GMSCameraPosition *position = [GMSCameraPosition cameraWithTarget:_mapView.myLocation.coordinate zoom:15];
        [_mapView setCamera:position];
    }
}


-(void)showMapItems{
    for (GMSMarker *marker in _markerList) {
        marker.map = nil;
    }
    [_markerList removeAllObjects];
    for (GroupListModel *model in _mapList) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.position = CLLocationCoordinate2DMake(model.geoLat, model.geoLong);
        marker.map = _mapView;
        marker.userData = model;
        [self generateMarker:marker url:model.avatar selected:NO];
        [_markerList addObject:marker];
    }
}

-(void)getMapsItems{
    [self changePosition];
}

-(void)hideDetails{
    [UIView animateWithDuration:0.4 animations:^{
        self.detailView.alpha = 0;
    }];
    [self generateMarker:_selectMarker url:_selectModel.avatar selected:NO];
}

-(void)showDetails{
    [[SettingsManager instance] anySound];
    if(_selectModel.type == 0){
        [self.avatarView sd_setImageWithURL:[NSURL URLWithString:_selectModel.avatar] placeholderImage:[UIImage imageNamed:@"closeGroupPlaceholder"]];
    } else{
        [self.avatarView sd_setImageWithURL:[NSURL URLWithString:_selectModel.avatar] placeholderImage:[UIImage imageNamed:@"publicGroupPlaceholder"]];
    }
    
    self.nameLabel.text = _selectModel.name;
    self.questionLabel.text = _selectModel.desc;
    self.typeLabel.text = _selectModel.typeString;
    self.followerLabel.text = trRecods;
    
    [UIView animateWithDuration:0.6 animations:^{
        self.detailView.alpha = 1;
        [self.view layoutIfNeeded];
    }];
}



-(void)changePosition{
    CLLocationCoordinate2D center = [self getCenterCoordinate];
    NSDictionary *params = @{@"Lat":@(center.latitude),
                             @"Lon":@(center.longitude),
                             @"Radius":@([self getRadius])};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"groups/map" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            response = response[@"response"];
            [_mapList removeAllObjects];
            for (NSDictionary *dict in response) {
                GroupListModel *model = [[GroupListModel alloc] initWithDict:dict];
                [_mapList addObject:model];
            }
            _zooming = _mapView.camera.zoom;
            [weakSelf showMapItems];
        } else{
            
        }
    }];
}


-(void)generateMarker:(GMSMarker *)marker url:(NSString *)url selected:(BOOL)select{
    
    UIView *gasView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 69, 94)];
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 16, 34, 34)];
    logo.layer.cornerRadius = 17;
    logo.contentMode = UIViewContentModeScaleAspectFill;
    logo.clipsToBounds = YES;
    GroupListModel *model = marker.userData;
    if (model.type == 0) {
        [logo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"closeGroupPlaceholder"]];
        [gasView addSubview:logo];
        if (select) {
            gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"grMapIcon"]];
        }else{
            gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"grRedMapPin"]];
            
        }
    } else{
        logo.frame = CGRectMake(11.5, 17, 33, 33);
        logo.layer.cornerRadius = 16.5;
        [logo sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@"publicGroupPlaceholder"]];
        [gasView addSubview:logo];
        if (select) {
            gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"grSelectIcon"]];
        }else{
            gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"grNormalIcon"]];
//            gasView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"grSelectIcon"]];
        }
    }
    
    marker.icon = [self imageWithView:gasView];
}

-(UIImage *)imageWithView:(UIView *)view{
    if ([UIScreen.mainScreen respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, false, UIScreen.mainScreen.scale);
        
    } else{
        UIGraphicsBeginImageContext(view.frame.size);
        
    }
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(CLLocationCoordinate2D)getCenterCoordinate {
    CGPoint centerPoint = self.mapView.center;
    CLLocationCoordinate2D centerCoordinate = [self.mapView.projection coordinateForPoint:centerPoint];
    return centerCoordinate;
}

-(CLLocationCoordinate2D)getTopCenterCoordinate{
    CGPoint topCenterCoor = [self.mapView convertPoint:CGPointMake(self.mapView.frame.size.width / 2.0, 0) fromView:self.mapView];
    
    CLLocationCoordinate2D point = [self.mapView.projection coordinateForPoint:topCenterCoor];
    return point;
}

-(float) getRadius{
    CLLocationCoordinate2D centerCoordinate = [self getCenterCoordinate];
    CLLocation *centerLocation = [[CLLocation alloc] initWithLatitude:centerCoordinate.latitude longitude:centerCoordinate.longitude];
    
    CLLocationCoordinate2D topCenterCoordinate = [self getTopCenterCoordinate];
    CLLocation * topCenterLocation = [[CLLocation alloc] initWithLatitude:topCenterCoordinate.latitude longitude:topCenterCoordinate.longitude];
    float radius = [centerLocation distanceFromLocation:topCenterLocation];
    return round(radius);
}

-(void)dealloc{
    [_mapView removeObserver:self forKeyPath:@"myLocation"];
}


@end
