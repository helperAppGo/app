//// Header

#import "GroupListViewController.h"
#import "GroupFeedsViewController.h"
#import "GroupListTableViewCell.h"
#import "GroupListModel.h"
#import "LoaderCell.h"

@interface GroupListViewController ()<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *_list;
    NSInteger _segmentIndex;
    NSMutableArray *_geoList;
    BOOL _geoComplete;
    BOOL _complete;
    
    UIRefreshControl *_refreshControl;
}
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topH;

@end

@implementation GroupListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel.text = trGroupListTitle;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = COLOR(251, 251, 251);
    self.view.backgroundColor = COLOR(251, 251, 251);
    
    UINib *nib = [UINib nibWithNibName:@"GroupListTableViewCell" bundle:nil];;
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"loader"];
    
    _list = @[].mutableCopy;
    _geoList = @[].mutableCopy;
    
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateNormal];
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateSelected];
    [self.segmentView setTitleTextAttributes:@{NSFontAttributeName:[UIFont defaultFontWithSize:14 bold:NO]} forState:UIControlStateHighlighted];
    
    [self.segmentView addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
    _refreshControl = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    self.segmentView.backgroundColor = [UIColor whiteColor];
    
    if (self.info) {
        [self.segmentView removeSegmentAtIndex:2 animated:YES];
        [self.segmentView setTitle:trGroupListSelf forSegmentAtIndex:0];
        [self.segmentView setTitle:trGroupListSettings forSegmentAtIndex:1];
    } else{
        [self.segmentView setTitle:trAll forSegmentAtIndex:0];
        [self.segmentView setTitle:trNearby forSegmentAtIndex:1];
        [self.segmentView setTitle:trMap forSegmentAtIndex:2];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateNotification) name:@"updateBadge" object:nil];

    [self getGeo];
    [self getGroups];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.type == 1) {
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"Создать" style:UIBarButtonItemStyleDone target:self action:@selector(createGroup)];
        item.tintColor = appBlueColor;
        self.navigationItem.rightBarButtonItem = item;
    } else{
        UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_notification"] style:UIBarButtonItemStyleDone target:self action:@selector(openNotifications)];
        item.tintColor = appBlueColor;
        self.navigationItem.leftBarButtonItem = item;
    }
    [self updateNotification];
}

-(void)createGroup{
    BaseViewController *controller = [self createController:@"CreateGroupViewController"];
    controller.type = 1;
    
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)updateNotification{
    NSInteger count = [[[SettingsManager instance] objForKey:@"notification3"] integerValue];
    if ([UIDevice currentDevice].systemVersion.doubleValue >= 11) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.001 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self addBadge:self.navigationItem.leftBarButtonItem count:count];
        });
        return;
    }
    [self addBadge:self.navigationItem.leftBarButtonItem count:count];
}

-(void)openNotifications{
    BaseViewController *controller = [self createController:@"NotificationListViewController"];
    controller.type = 1;
    controller.info = @{@"type":@"group"};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)segmentChanged:(UISegmentedControl *)sender{
    [[SettingsManager instance] anySound];
    if (sender.selectedSegmentIndex == 2) {
        sender.selectedSegmentIndex = _segmentIndex;
        [self openMap];
        return;
    }
    _segmentIndex = sender.selectedSegmentIndex;
    [self.tableView reloadData];
    
}

-(void)refresh{
    if (self.segmentView.selectedSegmentIndex) {
        [self getGeoNow];
    } else{
        [self getGroupsNow];
    }
}

-(void)openMap{
    
    BaseViewController *controller = [self createController:@"GroupMapViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [[SettingsManager instance] anySound];
}


-(void)getGroups{
    if (_complete) {
        return;
    }
    NSDictionary *params = @{@"offset":@(_list.count),
                             @"limit":@(25)};
    __weak typeof(self) weakSelf = self;
    NSString *path = self.info ? @"users/groups" : @"groups/most";
    [[ApiManager sharedManager] getRequest:path params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary *dict in items) {
                GroupListModel *model = [[GroupListModel alloc] initWithDict:dict];
                [_list addObject:model];
            }
            _complete = items.count < 25;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getGroupsNow{
    NSDictionary *params = @{@"offset":@(0),
                             @"limit":@(25)};
    __weak typeof(self) weakSelf = self;
    NSString *path = self.info ? @"users/groups" : @"groups/most";
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:path params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            NSArray *items = response[@"response"];
            [_list removeAllObjects];
            for (NSDictionary *dict in items) {
                GroupListModel *model = [[GroupListModel alloc] initWithDict:dict];
                [_list addObject:model];
            }
            _complete = items.count < 25;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getGeo{
    //groups/geo
    if (_geoComplete) {
        return;
    }
    NSDictionary *params = @{@"offset":@(_geoList.count),
                             @"limit":@(25)};
    __weak typeof(self) weakSelf = self;
    NSString *path = self.info  ? @"users/groups/admin" : @"groups/geo";
    [[ApiManager sharedManager] getRequest:path params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary *dict in items) {
                GroupListModel *model = [[GroupListModel alloc] initWithDict:dict];
                [_geoList addObject:model];
            }
            _geoComplete = items.count < 25;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)getGeoNow{
    NSDictionary *params = @{@"offset":@(0),
                             @"limit":@(25)};
    __weak typeof(self) weakSelf = self;
    NSString *path = self.info  ? @"users/groups/admin" : @"groups/geo";
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:path params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            NSArray *items = response[@"response"];
            [_geoList removeAllObjects];
            for (NSDictionary *dict in items) {
                GroupListModel *model = [[GroupListModel alloc] initWithDict:dict];
                [_geoList addObject:model];
            }
            _geoComplete = items.count < 25;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)cellBtnClicked:(NSIndexPath *)indexPath type:(NSInteger)type{
    if (type == 1) {
        [self memberClicked:indexPath];
    }
    if (type == 2) {
        [self followersClicked:indexPath];
    }
}

-(void)followersClicked:(NSIndexPath *)indexPath{
    GroupListModel *model = self.segmentView.selectedSegmentIndex ? _geoList[indexPath.row] :  _list[indexPath.row];
    BaseViewController *controller = [self createController:@"GroupMemberListViewController"];
    controller.type = 1;
    controller.info = @{@"group":model};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)memberClicked:(NSIndexPath *)indexPath{
    GroupListModel *model = self.segmentView.selectedSegmentIndex ? _geoList[indexPath.row] :  _list[indexPath.row];
    NSDictionary *params = @{@"id_group":@(model.objId)};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    if (model.isRequested || model.isMember) {
        NSDictionary *dict = @{@"data":[params convertToJsonString]};
        [[ApiManager sharedManager] deleteRequest:@"groups/members" params:dict callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                model.isMember = 0;
                model.isRequested = 0;
                if (model.type != 0) {
                    model.followersCount -= 1;
                }
                [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    } else{
        [[ApiManager sharedManager] postRequest:@"groups/members" params:params callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                model.isMember = 1;
                if (model.type == 0) {
                    model.isRequested = 1;
                } else{
                    model.followersCount += 1;
                }
                [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];

            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count = self.segmentView.selectedSegmentIndex ? _geoList.count : _list.count;
    return count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger count = self.segmentView.selectedSegmentIndex ? _geoList.count : _list.count;
    if (indexPath.row == count) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loader.hidden = YES;
        [cell.loader startAnimating];
        if (self.segmentView.selectedSegmentIndex) {
            if (_geoComplete) {
                [cell.loader stopAnimating];
                cell.loader.hidden = YES;
            }
        } else{
            if (_complete) {
                [cell.loader stopAnimating];
                cell.loader.hidden = YES;
            }
        }
        return cell;
    }
    
    GroupListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contentView.backgroundColor = self.view.backgroundColor;
    GroupListModel *model = self.segmentView.selectedSegmentIndex ? _geoList[indexPath.row] : _list[indexPath.row];
    
    [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"team"]];
    cell.nameLabel.text = model.name;
    cell.label.text = model.desc;
    cell.indexPath = indexPath;
    __weak typeof(self) weakSelf = self;
    [cell setBlock:^(NSIndexPath *path, NSInteger type) {
        [weakSelf cellBtnClicked:path type:type];
    }];
    cell.locationLabel.text = model.address;
    cell.categoryLabel.text = model.typeString;
    cell.memberLabel.text = model.folowerCountString;
    cell.locationBtn.hidden = model.address.length == 0;
    if (model.isMember || model.isRequested) {
        [cell.rightBtn setImage:[UIImage imageNamed:@"isMember"] forState:UIControlStateNormal];
    } else{
        [cell.rightBtn setImage:[UIImage imageNamed:@"addMember"] forState:UIControlStateNormal];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.segmentView.selectedSegmentIndex) {
        if (indexPath.row == _geoList.count - 4) {
            [self getGeo];
        }
    } else{
        if (indexPath.row == _list.count - 4) {
            [self getGroups];
        }
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    GroupListModel *model = self.segmentView.selectedSegmentIndex ? _geoList[indexPath.row] : _list[indexPath.row];
    
    GroupFeedsViewController *controller = (GroupFeedsViewController *)[self createController:@"GroupFeedsViewController"];
    controller.type = 1;
    __weak typeof(self) weakSelf = self;
    [controller setChangeBlock:^(GroupListModel *model2) {
        [weakSelf replace:model2];
    }];
    controller.info = @{@"group":model};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)replace:(GroupListModel *)model{
    if (self.segmentView.selectedSegmentIndex) {
        NSInteger index = [_geoList indexOfObject:model];
        [_geoList replaceObjectAtIndex:index withObject:model];
    } else{
        NSInteger index = [_list indexOfObject:model];
        [_list replaceObjectAtIndex:index withObject:model];
    }
    [self.tableView reloadData];
}



@end
