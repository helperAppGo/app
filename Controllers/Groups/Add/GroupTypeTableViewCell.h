//// Header

#import <UIKit/UIKit.h>

@interface GroupTypeTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *leftBtn;
@property (weak, nonatomic) IBOutlet UIButton *centerBtn;
@property (weak, nonatomic) IBOutlet UIButton *rightBtn;
@property (nonatomic, assign) NSInteger type;
@end
