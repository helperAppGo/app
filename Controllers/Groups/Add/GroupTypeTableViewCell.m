//// Header

#import "GroupTypeTableViewCell.h"

@implementation GroupTypeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [StyleManager styleBtn:self.leftBtn title:@"Публичную\nстраницу"];
    [StyleManager styleBtn:self.centerBtn title:@"Открытую\nгруппу"];
    [StyleManager styleBtn:self.rightBtn title:@"Закрытую\nгруппу"];
    
    [StyleManager styleBtn:self.leftBtn titleColor:appBlueColor];
    [self.leftBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [StyleManager styleBtn:self.centerBtn titleColor:appBlueColor];
    [self.centerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    [StyleManager styleBtn:self.rightBtn titleColor:appBlueColor];
    [self.rightBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    self.leftBtn.titleLabel.numberOfLines = 2;
    self.leftBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.rightBtn.titleLabel.numberOfLines = 2;
    self.rightBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.centerBtn.titleLabel.numberOfLines = 2;
    self.centerBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.leftBtn.backgroundColor = [UIColor whiteColor];
    
    self.leftBtn.tag = 0;
    self.centerBtn.tag = 1;
    self.rightBtn.tag = 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    if (self.type == 0) {
        self.leftBtn.titleLabel.textColor = appBlueColor;
        self.centerBtn.titleLabel.textColor = [UIColor blackColor];
        self.rightBtn.titleLabel.textColor = [UIColor  blackColor];
        
    } else if(self.type == 1){
        self.centerBtn.titleLabel.textColor = appBlueColor;
        self.leftBtn.titleLabel.textColor = [UIColor blackColor];
        self.rightBtn.titleLabel.textColor = [UIColor  blackColor];
        
    } else{
        self.rightBtn.titleLabel.textColor = appBlueColor;
        self.leftBtn.titleLabel.textColor = [UIColor blackColor];
        self.centerBtn.titleLabel.textColor = [UIColor  blackColor];
    }
}

@end
