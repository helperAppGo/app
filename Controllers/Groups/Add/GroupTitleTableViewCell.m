//// Header

#import "GroupTitleTableViewCell.h"

@implementation GroupTitleTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textField.placeholder = @"Введите название";
    self.textField.layer.cornerRadius = 4;
    self.textField.clipsToBounds = YES;
    self.textField.font = [UIFont defaultFontWithSize:14 bold:NO];
    [self.textField addLeftView:5];
    self.textField.layer.borderColor = COLOR(150, 150, 150).CGColor;
    self.textField.layer.borderWidth = 0.8;
    self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
