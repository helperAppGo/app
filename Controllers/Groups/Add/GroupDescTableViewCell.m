//// Header

#import "GroupDescTableViewCell.h"

@implementation GroupDescTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textView.backgroundColor = [UIColor clearColor];
    self.textView.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.textView.placeholder = @"Краткое описание";
    self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.textView.scrollEnabled = NO;
    
    self.label.font = [UIFont defaultFontWithSize:12 bold:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)layoutSubviews{
    [super layoutSubviews];
    UIView *view = self.textView.superview;
    view.layer.cornerRadius = 8;
    view.layer.masksToBounds = false;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowColor = COLOR(150, 150, 150).CGColor;
    view.layer.shadowOpacity = 0.7;
    view.layer.shadowRadius = 4;
}

@end
