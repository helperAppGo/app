//// Header

#import <UIKit/UIKit.h>
#import "SAMTextView.h"

@interface GroupDescTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SAMTextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
