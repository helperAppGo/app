//// Header

#import "CreateGroupViewController.h"
#import "GroupDescTableViewCell.h"
#import "GroupTitleTableViewCell.h"
#import "GroupImageTableViewCell.h"
#import "CityListViewController.h"
#import "FDTakeController.h"
#import "MapsViewController.h"
#import "GroupTypeTableViewCell.h"

@interface CreateGroupViewController ()<UITextViewDelegate, UITextFieldDelegate, FDTakeDelegate>{
    CDCity *_city;
    UILabel *_descLabel;
    NSInteger _descCount;
    UILabel *_fullLabel;
    NSInteger _fullCount;
    
    NSString *_title;
    NSString *_desc;
    NSString *_fullDesc;
    NSInteger _groupType;
    NSString *_address;
    UIImage *_image;
    CLLocationCoordinate2D _coordinate;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) FDTakeController *takeController;
@end

@implementation CreateGroupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _titleLabel.text = @"Создать";
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [UIView new];
    
    UINib *nib = [UINib nibWithNibName:@"GroupDescTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"desc"];
    
    UINib *nib2 = [UINib nibWithNibName:@"GroupTitleTableViewCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"title"];
    
    UINib *nib3 = [UINib nibWithNibName:@"GroupImageTableViewCell" bundle:nil];
    [self.tableView registerNib:nib3 forCellReuseIdentifier:@"image"];
    
    UINib *nib4 = [UINib nibWithNibName:@"GroupImageTableViewCell" bundle:nil];
    [self.tableView registerNib:nib4 forCellReuseIdentifier:@"image"];
    
    UINib  *nib5  = [UINib nibWithNibName:@"GroupTypeTableViewCell" bundle:nil];
    [self.tableView registerNib:nib5 forCellReuseIdentifier:@"type"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self changeNav];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 1) {
        return _address.length ?  2 : 1;
    }
    if (section == 2) {
        return _image ? 2 : 1;;
    }
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            GroupTypeTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"type"];
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.separatorInset = UIEdgeInsetsMake(0, tableView.width, 0, 0);
            cell.type = _groupType;
            [cell.leftBtn addTarget:self action:@selector(changeType:) forControlEvents:UIControlEventTouchUpInside];
            [cell.centerBtn addTarget:self action:@selector(changeType:) forControlEvents:UIControlEventTouchUpInside];
            [cell.rightBtn addTarget:self action:@selector(changeType:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        if (indexPath.row == 1) {
            GroupTitleTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"title"];
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.separatorInset = UIEdgeInsetsMake(0, tableView.width, 0, 0);
            cell.textField.delegate = self;
            [cell.textField addTarget:self action:@selector(titleChange:) forControlEvents:UIControlEventEditingChanged];
            return cell;
        }
        
        if (indexPath.row == 2) {
            GroupDescTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"desc"];
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.separatorInset = UIEdgeInsetsMake(0, tableView.width, 0, 0);
            cell.textView.placeholder = @"Краткое описание";
            cell.label.text = [NSString stringWithFormat:@"%@/50",@(_descCount)];
            _descLabel = cell.label;
            cell.textView.tag = 0;
            cell.textView.delegate = self;
            return cell;
        }
        
        if (indexPath.row == 3) {
            GroupDescTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"desc"];
            cell.selectionStyle = UITableViewCellSeparatorStyleNone;
            cell.separatorInset = UIEdgeInsetsMake(0, tableView.width, 0, 0);
            cell.textView.placeholder = @"Полное описание";
            cell.label.text = [NSString stringWithFormat:@"%@/200",@(_fullCount)];
            cell.textView.delegate = self;
            cell.textView.tag = 1;
            _fullLabel = cell.label;
            return cell;
        }
        UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"city"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"city"];
        }
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
        cell.textLabel.text = _city ? _city.name : @"Укажите город";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"city"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"city"];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    cell.textLabel.textColor = [UIColor blackColor];
    if (indexPath.row == 0) {
        cell.textLabel.text = indexPath.section == 2 ? @"Загрузить фото" : @"Добавить адрес";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    if (indexPath.section == 1) {
        cell.textLabel.text = _address;
        cell.textLabel.textColor = appBlueColor;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.numberOfLines = 10;
        return cell;
    }
    {
        GroupImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"image"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.deleteBtn addTarget:self action:@selector(deleteImage) forControlEvents:UIControlEventTouchUpInside];
        cell.iconView.image = _image;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if (indexPath.section == 1) {
            [self openMap];
        } else if(indexPath.section == 2){
            [self take];
        }
    } else if(indexPath.row == 3){
        CityListViewController *controller = (CityListViewController *)[self createController:@"CityListViewController"];
        controller.info = @{@"id":@(_city.objId)};
        controller.type = 1;
        __weak typeof(self) weakSelf = self;
        [controller setClickBlock:^(NSInteger objId) {
            [weakSelf changeCityId:objId];
        }];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
-(void)openMap{
    MapsViewController *controller = (MapsViewController *)[self createController:@"MapsViewController"];
    controller.type = 1;
    controller.info = @{};
    __weak typeof(self) weakSelf = self;
    [controller setBlock:^(NSString *address, CLLocationCoordinate2D coordinate) {
        [weakSelf changeAddress:address coordinate:coordinate];
    }];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeAddress:(NSString *)address coordinate:(CLLocationCoordinate2D)coordinate{
    _address = address;
    _coordinate = coordinate;
    [self.tableView reloadData];
}

-(void)take{
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    self.takeController.delegate = self;
    self.takeController.type = 1;
    [self.takeController takePhotoOrChooseFromLibrary];
}

-(void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info{
    NSData *data = UIImagePNGRepresentation(photo);
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(photo, 1/k);
    }
    _image = [UIImage imageWithData:data];
    [self.tableView reloadData];
}

-(void)changeCityId:(NSInteger)cityId{
    CDCity *city = [CDCity find:@{@"objId":@(cityId)}];
    _city = city;
    [self.tableView reloadData];
}

-(void)deleteImage{
    _image = nil;
    [self.tableView reloadData];
}

#pragma mark - UITextView delegate methods
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    [self textViewFitToContent:textView];
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    if (textView.tag == 0) {
        return (textView.text.length + (text.length - range.length)) <= 50;
    }
    return (textView.text.length + (text.length - range.length)) <= 200;
}


-(void)textViewDidChange:(UITextView *)textView{
    if (textView.tag == 0) {
        _desc = textView.text;
        _descCount = textView.text.length;
        _descLabel.text = [NSString stringWithFormat:@"%@/50",@(textView.text.length)];
    }
    if (textView.tag == 1) {
        _fullDesc = textView.text;
        _fullCount = textView.text.length;
        _fullLabel.text = [NSString stringWithFormat:@"%@/200",@(textView.text.length)];
    }
    [self changeNav];
}


-(void)changeNav{
    if (_desc.length > 0 && _title.length > 0) {
        UIBarButtonItem *item  = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStylePlain target:self action:@selector(create)];
        item.tintColor = appBlueColor;
        self.navigationItem.rightBarButtonItem = item;
    } else{
        self.navigationItem.rightBarButtonItem = nil;
    }
}

-(void)create{
    /*
     name
     file_cover
     file_ava
     short_description
     full_description
     type
     id_city
     lon
     lat
     web_site
     */
    if (_image) {
        [self upload];
    } else{
        [self createGroup:0];
    }
}

-(void)upload{
    NSData *data = UIImagePNGRepresentation(_image);
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(_image, 1/k);
    }
    
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] uploadPhoto:data fileName:@"media.png" callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            NSInteger fileId = [response[@"response"][@"id"] integerValue];
            [weakSelf createGroup:fileId];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)createGroup:(NSInteger)fileId{
    NSMutableDictionary *params = @{@"name":_title,
                                    @"type":@(_groupType),
                                    @"short_description":_desc}.mutableCopy;
    if (_fullDesc.length) {
        params[@"full_description"] = _fullDesc;
    }
    if (_coordinate.latitude > 0) {
        params[@"lat"] = @(_coordinate.latitude);
        params[@"lon"] = @(_coordinate.longitude);
    }
    if (_city) {
        params[@"id_city"] = @(_city.objId);
    }
    if (fileId > 0) {
        params[@"file_ava"] = @(fileId);//[NSString stringWithFormat:@"%@",@(fileId)];
    }
    
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] postRequest:@"groups" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

- (void)textViewFitToContent:(UITextView *)textView{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    textView.scrollEnabled = NO;
}

-(void)changeType:(UIButton *)sender{
    _groupType = sender.tag;
    [self.tableView reloadData];
}

#pragma mark - UITextField delegate methods

-(void)textFieldDidEndEditing:(UITextField *)textField{
    _title = textField.text;
}

-(void)titleChange:(UITextField *)field{
    _title = field.text;
    [self changeNav];
}



@end
