//// Header

#import "GroupFeedsViewController.h"
#import "PrivateGroupTableViewCell.h"
#import "LoaderCell.h"
#import "PlayerViewController.h"
#import "GroupNavView.h"
#import "FDTakeController.h"
#import "PHFComposeBarView.h"

@interface GroupFeedsViewController ()<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, FDTakeDelegate, UITextViewDelegate, PHFComposeBarViewDelegate, UIGestureRecognizerDelegate>{
    NSMutableArray *_list;
    GroupListModel *_model;
    BOOL _complete;
    UIRefreshControl *_refreshControl;
    GroupNavView *_navView;
}


@property (weak, nonatomic) IBOutlet PHFComposeBarView *barVIew;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *composeH;
@property (weak, nonatomic) IBOutlet UIView *attachView;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *composeBottom;

@property (weak, nonatomic) IBOutlet UIButton *bottomBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomH;
@property (weak, nonatomic) IBOutlet UILabel *privateBottomLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *bottomPrivateIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomCons;


@property (strong, nonatomic) FDTakeController *takeController;
@property (weak, nonatomic) IBOutlet UIButton *memberBtn;
@property (weak, nonatomic) IBOutlet UILabel *memberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *memberIcon;

@end

@implementation GroupFeedsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.backgroundColor = COLOR(252, 251, 251);
    self.view.backgroundColor = COLOR(251, 251, 251);
   
    [[SettingsManager instance] anySound];
    _model = self.info[@"group"];
    _navView = [GroupNavView loadFromNib];
    [_navView.heightAnchor constraintEqualToConstant:40].active  = YES;

    self.navigationItem.titleView = _navView;
    _navView.nameLabel.text = _model.name;
    [_navView.avatar sd_setImageWithURL:[NSURL URLWithString:_model.avatar] placeholderImage:[UIImage imageNamed:@"team"]];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self     action:@selector(showAvatar)];
    _navView.avatar.userInteractionEnabled = YES;
    [_navView.avatar addGestureRecognizer:tapGesture];
    
    [self changTopText];
    
    UINib *nib = [UINib nibWithNibName:@"PrivateGroupTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"private"];
    
    UINib *nib5 = [UINib nibWithNibName:@"FeedListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib5 forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"RepostListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"list2"];
    
    UINib *loaderNib = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:loaderNib forCellReuseIdentifier:@"loader"];
    
    
    _list = @[].mutableCopy;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomBtn.superview.superview.backgroundColor = [UIColor clearColor];
    
    
    [self.memberBtn addTarget:self action:@selector(addMember) forControlEvents:UIControlEventTouchUpInside];
    self.memberBtn.backgroundColor = [UIColor clearColor];
    self.memberBtn.superview.backgroundColor = [UIColor whiteColor];
    self.memberBtn.superview.layer.cornerRadius = 25;
    self.memberBtn.superview.clipsToBounds = YES;
    self.memberBtn.superview.layer.masksToBounds = false;
    self.memberBtn.superview.superview.backgroundColor = [UIColor clearColor];
    self.memberBtn.superview.layer.shadowOffset = CGSizeMake(0, 0);
    self.memberBtn.superview.layer.shadowColor = COLOR(180, 180, 180).CGColor;
    self.memberBtn.superview.layer.shadowOpacity = 0.45;
    self.memberBtn.superview.layer.shadowRadius = 4;
    self.memberLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.memberLabel.text = trSubs; //@"Подписаться";
    self.memberLabel.textColor = appBlueColor;
    
    self.privateBottomLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.bottomBtn.superview.backgroundColor = [UIColor whiteColor];
    self.bottomBtn.superview.superview.backgroundColor = [UIColor clearColor];
    
    self.bottomBtn.superview.layer.cornerRadius = self.bottomBtn.superview.height/2;
    self.bottomBtn.superview.layer.masksToBounds = false;
    self.bottomBtn.superview.layer.shadowOffset = CGSizeMake(0, 0);
    self.bottomBtn.superview.layer.shadowColor = COLOR(180, 180, 180).CGColor;
    self.bottomBtn.superview.layer.shadowOpacity = 0.45;
    self.bottomBtn.superview.layer.shadowRadius = 4;
    [self.bottomBtn addTarget:self action:@selector(sendInvite) forControlEvents:UIControlEventTouchUpInside];
    [self changePrivateStatus];
    
    if (_model.type == 0) {
        [self closeMode];
    } 
    _refreshControl = [[UIRefreshControl alloc] init];
    [_refreshControl addTarget:self action:@selector(getNow) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:_refreshControl];
    
    if (_model.type == 1) {
        [self createMode];
    }
    
    if (_model.type == 2) {
        [self channelMode];
    }
    
    if (_model.type == 3) {
        [self noneMode];
    }
    
    [self.barVIew setMaxCharCount:160];
    self.barVIew.delegate = self;
    [self.barVIew setMaxLinesCount:10];
    [self.barVIew setPlaceholder:@"Type something..."];
    [self.barVIew setUtilityButtonImage:[UIImage imageNamed:@"addMember"]];
    
    [[self.barVIew placeholderLabel] setAccessibilityIdentifier:@"Placeholder"];
    [[self.barVIew textView] setAccessibilityIdentifier:@"Input"];
    self.barVIew.textView.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [[self.barVIew button] setAccessibilityIdentifier:@"Submit"];
    [[self.barVIew utilityButton] setAccessibilityIdentifier:@"Utility"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    [self.deleteBtn addTarget:self action:@selector(deleteAttach) forControlEvents:UIControlEventTouchUpInside];
    self.photoView.layer.cornerRadius = 5;
    self.photoView.clipsToBounds = YES;
    self.attachView.backgroundColor = COLOR(240, 240, 240);
    self.attachView.hidden = YES;
}
//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
//    return (![[[touch view] class] isSubclassOfClass:[UIControl class]]);
//}

-(void)showAvatar{
    NSString *urlString = _model.avatar;
    [self openAvatar:urlString];
}

-(void)createMode{
    if (_model.canPost) {
        self.bottomView.hidden = YES;
    } else{
        
    }
    self.bottomView.hidden = YES;
    self.barVIew.hidden = NO;
    
}
-(void)noneMode{
    self.memberBtn.superview.hidden = YES;
    self.bottomView.hidden = YES;
    self.barVIew.hidden = YES;
}

-(void)closeMode{
    self.memberBtn.superview.hidden = YES;
    self.bottomView.hidden = NO;
    self.barVIew.hidden = YES;
    
    self.tableView.scrollEnabled = YES;
    
    if (_model.isAdmin) {
        self.bottomView.hidden = YES;
        self.barVIew.hidden = NO;
        self.photoView.hidden = YES;
        self.privateBottomLabel.superview.hidden = YES;
        self.memberLabel.superview.hidden = YES;
    }
}

-(void)channelMode{
    self.bottomView.hidden = NO;
    self.barVIew.hidden = YES;
    self.photoView.hidden = YES;
    
    self.privateBottomLabel.superview.hidden = YES;
    self.memberLabel.superview.hidden = NO;
    [self changeChanel];
    
}

-(void)changTopText{
    NSString *str = @"Подписчиков";
    if (_model.type < 2) {
        str = @"Участников";
    }
    _navView.descLabel.text = [NSString stringWithFormat:@"%@:%@", str, @(_model.followersCount)];
}

#pragma mark - Private groups
-(void)changePrivateStatus{
    self.bottomPrivateIcon.image = _model.isRequested ? [UIImage imageNamed:@"isMember"] : [UIImage imageNamed:@"addMember"];
    self.privateBottomLabel.text = _model.isRequested ? @"Заявка отправлен" : @"Подать заявку";
    if(self.changeBlock){
        self.changeBlock(_model);
    }
}

-(void)sendInvite{
    NSDictionary *params = @{@"id_group":@(_model.objId)};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    if (_model.isRequested) {
        [[ApiManager sharedManager] postRequest:@"groups/members" params:params callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                _model.isRequested = 0;
                [weakSelf changePrivateStatus];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    } else{
        NSDictionary *dict = @{@"data":[params convertToJsonString]};
        [[ApiManager sharedManager] deleteRequest:@"groups/members" params:dict callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                _model.isRequested = 1;
                [weakSelf changePrivateStatus];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    }
    
}

#pragma mark - Public groups

-(void)addMember{
    
    [[SettingsManager instance] anySound];
    NSDictionary *params = @{@"id_group":@(_model.objId)};
    __weak typeof(self) weakSelf = self;
    if (_model.isMember) {
        [self showLoader];
        NSDictionary *dict = @{@"data":[params convertToJsonString]};
        [[ApiManager sharedManager] deleteRequest:@"groups/members" params:dict callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                _model.isMember = 0;
                _model.followersCount --;
                [weakSelf changeChanel];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    } else{
        [self showLoader];
        [[ApiManager sharedManager] postRequest:@"groups/members" params:params callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                _model.isMember = 1;
                _model.followersCount ++;
                [weakSelf changeChanel];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    }
}

-(void)changeChanel{
    if (_model.isMember) {
        self.memberLabel.text = trUnSubs;
        self.memberIcon.image = [UIImage imageNamed:@"isMember"];
    } else{
        self.memberLabel.text = trSubs;
        self.memberIcon.image = [UIImage imageNamed:@"addMember"];
    }
    [self changTopText];
    if (_model.isAdmin) {
        self.bottomView.hidden = YES;
        self.barVIew.hidden = NO;
        self.photoView.hidden = YES;
        
        self.privateBottomLabel.superview.hidden = YES;
        self.memberLabel.superview.hidden = YES;
    }
    if(self.changeBlock){
        self.changeBlock(_model);
    }
    
}

-(void)sendCommentClicked{
    [self.view endEditing:YES];
    if (self.attachView.hidden) {
        [self createPost:0];
    } else{
        [self uploadPhoto];
    }
}


-(void)attachClicked{
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.delegate = self;
    self.takeController.viewControllerForPresentingImagePickerController = self.tabBarController;
    [self.takeController takePhotoOrChooseFromLibrary];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"about_group"] style:UIBarButtonItemStyleDone target:self action:@selector(openInfo)];
    self.navigationItem.rightBarButtonItem = item;
}

-(void)openInfo{
    BaseViewController *controller = [self createController:@"GroupInfoViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    controller.info = @{@"group":_model};
    [self.navigationController pushViewController:controller animated:YES];
    [[SettingsManager instance] anySound];
}

-(void)getNow{
    NSDictionary *dict = @{@"offset":@(0),
                           @"limit":@(30),
                           @"id_wall":@(-_model.objId),
                           @"expand":@"files"};
    
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    [_refreshControl beginRefreshing];
    [[ApiManager sharedManager] getRequest:@"posts/last" params:params callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            [_list removeAllObjects];
            NSArray *items = response[@"response"];
            for (NSDictionary * item in items) {
                FeedModel *model = [[FeedModel alloc] initWithDict:item];
                [_list addObject:model];
            }
            _complete = items.count < 30;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
    
}

-(void)getList{
    if (_complete) {
        return;
    }
    
    NSDictionary *dict = @{@"offset":@(_list.count),
                           @"limit":@(30),
                           @"id_wall":@(-_model.objId),
                           @"expand":@"files"};
    NSDictionary *params = @{@"data":[dict convertToJsonString]};
    
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"posts/last" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary * item in items) {
                FeedModel *model = [[FeedModel alloc] initWithDict:item];
                [self->_list addObject:  model];
            }
            self->_complete = items.count < 30;
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
    
}

-(UITableView *)table{
    return self.tableView;
}

-(NSMutableArray *)feedItems{
    return _list;
}

#pragma mark - Action methods
- (void)feedClicked:(NSIndexPath *)indexPath action:(FeedActionType)type sender:(UIButton *)sender{
    FeedModel *model = _list[indexPath.row];
    [self feedAction:indexPath model:model action:type sender:sender];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    if (_model.type == 0) {
//        return 1;
//    }
    if (_complete) {
        return _list.count;
    }
    if (_list.count > 0) {
        return _list.count + 1;
    }
    return 1;
    
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 50;
    }
    
    if (indexPath.row == _list.count) {
        return 50;
    }
    if (_list.count > 0) {
        FeedModel *model = _list[indexPath.row];
        if (model.mediaObjects.count) {
            return 100;
        }
    }
    
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (_model.type == 0) {
//        PrivateGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"private"];
//        cell.label2.text = _model.desc;
//        cell.backgroundColor = self.view.backgroundColor;
//        cell.contentView.backgroundColor = self.view.backgroundColor;
//        return cell;
//    }
    
    if (indexPath.row == _list.count) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        }
        return cell;
    }
    
    FeedModel *model = _list[indexPath.row];
    return [self feedCell:model indexPath:indexPath tableView:tableView];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        [self getList];
    }
}

#pragma mark - UITextField delegate methods
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    [self.view endEditing:YES];
////    [self textFieldDidEndEditing:self.field];
//}

-(void)keyboardDidShow:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.composeBottom.constant = -214;
        [self.view layoutIfNeeded];
    });
}

-(void)keyboardDidHide:(id)sender{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.composeBottom.constant = 0;
        [self.view layoutIfNeeded];
    });
    
}

- (void)composeBarViewDidPressButton:(PHFComposeBarView *)composeBarView{
    [self sendCommentClicked];
}

- (void)composeBarViewDidPressUtilityButton:(PHFComposeBarView *)composeBarView{
    [self attachClicked];
}

-(void)composeBarView:(PHFComposeBarView *)composeBarView willChangeFromFrame:(CGRect)startFrame toFrame:(CGRect)endFrame duration:(NSTimeInterval)duration animationCurve:(UIViewAnimationCurve)animationCurve{
    float h = endFrame.size.height;
    if (h<45) {
        h = 45;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        self.composeH.constant = endFrame.size.height;
        [self.view layoutIfNeeded];
    });
    
}

-(void)deleteAttach{
    self.photoView.image = nil;
    self.attachView.hidden = YES;
}

-(void)createPost:(NSInteger)mediaId{
    NSString *text = self.barVIew.textView.text;
    if (text.length == 0 && mediaId == 0) {
        return;
    }
    NSMutableDictionary *params = @{@"id_wall":@(-_model.objId),
                                    @"text":text ? text : @"",
                                    @"link":@""}.mutableCopy;
    
    if (mediaId > 0) {
        params[@"files"] = [NSString stringWithFormat:@"%@",@(mediaId)];
    }
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] postRequest:@"posts" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            [weakSelf.barVIew setText:@"" animated:YES];
            [weakSelf.barVIew resignFirstResponder];
            weakSelf.attachView.hidden = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"createPost" object:nil];
            [weakSelf getNow];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)uploadVideo{
    
}


-(void)uploadPhoto{
    NSData *data = UIImagePNGRepresentation(self.photoView.image);
    float k = [data length]/1024;
    if (k > 1) {
        data = UIImageJPEGRepresentation(self.photoView.image, 1/k);
    }
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] uploadPhoto:data fileName:@"media.png" callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            response = response[@"response"];
            NSInteger fileId = [response[@"id"] integerValue];
            [weakSelf createPost:fileId];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
        
    }];}

#pragma mark - FDTakeController delegate

-(void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info{
    self.attachView.hidden = NO;
    self.photoView.image = photo;
}

- (void)takeController:(FDTakeController *)controller gotVideo:(NSURL *)video withInfo:(NSDictionary *)info{
    
}



@end
