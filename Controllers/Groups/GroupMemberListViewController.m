//// Header

#import "GroupMemberListViewController.h"
#import "MemberListTableViewCell.h"
#import "LoaderCell.h"

@interface GroupMemberListViewController ()<UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *_list;
    BOOL _complete;
    GroupListModel *_model;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GroupMemberListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _model = self.info[@"group"];
    _titleLabel.text = trMembers;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.tableFooterView = [UIView new];
    
    UINib *nib = [UINib nibWithNibName:@"MemberListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *nib2 = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"loader"];

    _list = @[].mutableCopy;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)getList{
    if (_complete) {
        return;
    }
    NSDictionary *params = @{@"id_group":@(_model.objId),
                             @"offset":@(_list.count),
                             @"limit":@(50)};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"groups/members/list" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [_list addObject:user];
            }
            _complete = items.count < 50;
            [weakSelf.tableView reloadData];
        } else{
            
        }
    }];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        } else{
            [cell.loader startAnimating];
            cell.loader.hidden = NO;
        }
        return cell;
    }
    
    MemberListTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserObj *user = _list[indexPath.row];
    [cell.iconView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.label.text = user.nameString;
    return cell;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < _list.count) {
        [self openUserProfile:_list[indexPath.row]];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        [self getList];
    }
}

@end
