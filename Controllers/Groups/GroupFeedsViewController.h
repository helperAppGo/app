//// Header

#import "BaseFeedViewController.h"

@interface GroupFeedsViewController : BaseFeedViewController

@property (nonatomic, copy) void(^changeBlock)(GroupListModel *model);

@end
