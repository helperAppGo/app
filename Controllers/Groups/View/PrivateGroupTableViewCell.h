//// Header

#import <UIKit/UIKit.h>

@interface PrivateGroupTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

@end
