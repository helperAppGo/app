//// Header

#import "GroupNavView.h"

@implementation GroupNavView

-(void)awakeFromNib{
    [super awakeFromNib];
    self.nameLabel.font = [UIFont defaultFontWithSize:15 bold:YES];
    self.descLabel.font = [UIFont defaultFontWithSize:8 bold:NO];
    self.avatar.layer.cornerRadius = 15;
    self.avatar.clipsToBounds = YES;
}

@end
