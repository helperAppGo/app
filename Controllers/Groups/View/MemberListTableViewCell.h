//// Header

#import <UIKit/UIKit.h>

@interface MemberListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@end
