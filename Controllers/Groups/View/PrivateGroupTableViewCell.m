//// Header

#import "PrivateGroupTableViewCell.h"

@implementation PrivateGroupTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.label1.font = [UIFont defaultFontWithSize:25 bold:NO];
    self.label1.text = @"Закрытая группа";
    self.label1.textColor = COLOR(151, 152, 151);
    self.label2.font = [UIFont defaultFontWithSize:14 bold:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
