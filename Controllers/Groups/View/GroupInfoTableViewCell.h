//// Header

#import <UIKit/UIKit.h>

@interface GroupInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@end
