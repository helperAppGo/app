//// Header

#import "GroupInfoViewController.h"
#import "GroupBannerTableViewCell.h"
#import "GroupInfoTableViewCell.h"
#import "MemberListTableViewCell.h"
#import "LoaderCell.h"

@interface GroupInfoViewController ()<UITableViewDataSource, UITableViewDelegate>{
    GroupListModel *_model;
//    NSArray *_titles;
    NSMutableArray *_memberList;
    BOOL _complete;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation GroupInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _model = self.info[@"group"];
    
    _titleLabel.text = trGroupInfoTitle;
    
    _memberList = @[].mutableCopy;
    
//    _titles = @[@"Краткое описание",
//                @"Полное описание",
//                @"Дополнительная информация"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    
    UINib *bannerNib = [UINib nibWithNibName:@"GroupBannerTableViewCell" bundle:nil];
    [self.tableView registerNib:bannerNib forCellReuseIdentifier:@"banner"];
    
    UINib *nib2 = [UINib nibWithNibName:@"MemberListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib2 forCellReuseIdentifier:@"member"];
    
    UINib *nib = [UINib nibWithNibName:@"GroupInfoTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"info"];
    
    UINib *nib3 = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:nib3 forCellReuseIdentifier:@"loader"];
    
    [self getInfo];
    [self getMembers];
}

-(void)getInfo{
    NSDictionary *params = @{@"data": [@{@"id":@(_model.objId)} convertToJsonString]};
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] getRequest:@"groups" params:params callback:^(id response, ErrorObj *error) {
        if (response) {
            _model = [[GroupListModel alloc] initWithDict:response[@"response"]];
            [weakSelf.tableView reloadData];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)getMembers{
    if (_complete) {
        return;
    }
    NSDictionary *params = @{@"id_group":@(_model.objId),
                             @"offset":@(_memberList.count),
                             @"limit":@(50)};
    __weak typeof(self) weakSelf = self;
    _complete = YES;
    [[ApiManager sharedManager] getRequest:@"groups/members/list" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            NSArray *items = response[@"response"];
            for (NSDictionary *dict in items) {
                UserObj *user = [UserObj createUser:dict];
                [_memberList addObject:user];
            }
            _complete = items.count < 50;
            [weakSelf.tableView reloadData];
        } else{
            
        }
    }];
}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 6;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section < 4) {
        return 1;
    }
    if (section == 4) {
        return 3;
    }
    if (section == 5) {
        return 2 + _memberList.count + 1;
    }
    return 10;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 || section == 3) {
        return 0;
    }
    return 30;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0 || section == 3) {
        return nil;
    }
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = COLOR(239, 239, 244);
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, 300, 30)];
    label.font = [UIFont defaultFontWithSize:14 bold:NO];
    [view addSubview:label];
    if (section == 5) {
        label.text = [[NSString stringWithFormat:@"%@ участников",@(_model.followersCount)] uppercaseString];
        return view;
    }
    NSString *str = trGroupInfoDesc;
    if (section == 2) {
        str = trGroupInfoFull;
    }
    if (section == 4) {
        str = trGroupInfoAdd;
    }
    label.text = [str uppercaseString];
    
    return view;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        GroupInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.iconView sd_setImageWithURL:[NSURL URLWithString:_model.avatar] placeholderImage:[UIImage imageNamed:@"team"]];
        cell.nameLabel.text = _model.name;
        return cell;
    }
    
    if (indexPath.section == 1 || indexPath.section == 2) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"desc"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"desc"];
            cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
            cell.textLabel.numberOfLines = 15;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.text = indexPath.section == 1 ? _model.desc : _model.fullDesc;
        return cell;
    }
   
    if (indexPath.section == 3) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"desc"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"desc"];
            cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
            cell.textLabel.numberOfLines = 15;
        }
        cell.textLabel.text = trGroupInfoAdmin;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    if (indexPath.section == 4) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"info2"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"info2"];
            cell.textLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
        }
        if (indexPath.row == 0) {
            cell.textLabel.text = trGroupInfoWeb;
            cell.detailTextLabel.text = _model.web;
            return cell;
        }
        cell.textLabel.text = indexPath.row == 1 ? trCity : trAddress;
        cell.detailTextLabel.text = indexPath.row == 1 ? _model.cityString : _model.address;
        return cell;
    }
    
    if (indexPath.row == 0 || indexPath.row == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"share"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"share"];
            cell.textLabel.textColor = appBlueColor;
            cell.textLabel.font = [UIFont defaultFontWithSize:15 bold:NO];
        }
        cell.imageView.image = indexPath.row == 1 ? [UIImage imageNamed:@"addMember"] : [UIImage imageNamed:@"memberFav"];
        cell.textLabel.text = indexPath.row == 0 ? trGroupInfoInvite : trGroupInfoAddMember;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    
    if (indexPath.row == _memberList.count + 2) {
        
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        } else{
            [cell.loader startAnimating];
            cell.loader.hidden = NO;
        }
        return cell;
        
    }
    MemberListTableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:@"member"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UserObj *user = _memberList[indexPath.row - 2];
    [cell.iconView sd_setImageWithURL:[NSURL URLWithString:user.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    cell.label.text = user.nameString;
    return cell;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _memberList.count + 2 && indexPath.section == 4) {
        [self getMembers];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 5) {
        if (indexPath.row == 0) {
            //
            return;
        }
        if (indexPath.row == 1) {
            return;
        }
        UserObj *user = _memberList[indexPath.row - 2];
        [self showAction:user];
    }
    if (indexPath.section == 3) {
        BaseViewController *controller = [self createController:@"UserListViewController"];
        controller.type = 1;
        controller.info = @{@"listType":@(4),@"groupId":@(_model.objId)};
        [self.navigationController pushViewController:controller animated:YES];
    }
}


-(void)showAction:(UserObj *)member{
    CDUser *user = [[SettingsManager instance] currentUser];
    if (user.objId == member.objId) {
        [self memberProfile:member];
        return;
    }
    if (_model.isAdmin == 0) {
        [self memberProfile:member];
        return;
    }
    UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *complainPost = [UIAlertAction actionWithTitle:trView style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self memberProfile:member];
    }];
    [alertController addAction:complainPost];
    
    
    UIAlertAction *admin = [UIAlertAction actionWithTitle:trGroupInfoToAdmin style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self memberToAdmin:member];
    }];
    [alertController addAction:admin];
    
    UIAlertAction *block = [UIAlertAction actionWithTitle:trGroupInfoBlock style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self memberBlock:member];
    }];
    [alertController addAction:block];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:trCancel style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
    }];
    [alertController addAction:actionCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)memberProfile:(UserObj *)member{
    [self openUserProfile:member];
}

-(void)memberBlock:(UserObj *)member{
    //groups/members/ban
    NSDictionary *params = @{@"id_group":@(_model.objId),
                             @"id_user":@(member.objId)};
    [self  showLoader];
    __weak typeof(self) weakSelf = self;
    [[ApiManager sharedManager] putRequest:@"groups/members/ban" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            
        } else{
            [weakSelf showErrorMessage:error.message];
        }
        NSLog(@"%@",response);
    }];
}

-(void)memberToAdmin:(UserObj *)member{
    NSDictionary *params = @{@"id_group":@(_model.objId),
                             @"id_user":@(member.objId)};
    __weak  typeof(self) weakSelf = self;
    [self showLoader];
    [[ApiManager sharedManager] putRequest:@"groups/members/admin" params:params callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            NSLog(@"%@",response);
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
    
}

@end
