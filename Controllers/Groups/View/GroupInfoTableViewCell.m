//// Header

#import "GroupInfoTableViewCell.h"

@implementation GroupInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.nameLabel.font = [UIFont defaultFontWithSize:16 bold:YES];
    self.iconView.layer.cornerRadius = 30;
    self.iconView.clipsToBounds = YES;
    self.iconView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
