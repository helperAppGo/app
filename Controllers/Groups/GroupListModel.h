//// Header

#import <Foundation/Foundation.h>

@interface GroupListModel : NSObject
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger canPost;
@property (nonatomic, assign) NSInteger followersCount;
@property (nonatomic, assign) double distance;
@property (nonatomic, assign) NSInteger fileAva;
@property (nonatomic, assign) NSInteger fileCover;
@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, assign) NSInteger cityId;
@property (nonatomic, assign) NSInteger isRequested;
@property (nonatomic, assign) NSInteger isAdmin;
@property (nonatomic, assign) NSInteger isBanned;
@property (nonatomic, assign) NSInteger isMember;
@property (nonatomic, assign) float geoLat;
@property (nonatomic, assign) float geoLong;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *cover;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *fullDesc;
@property (nonatomic, strong) NSString *web;
@property (nonatomic, strong) NSString *typeString;
@property (nonatomic, assign) NSInteger type;

-(NSString *)folowerCountString;
-(NSString *)cityString;
-(id)initWithDict:(NSDictionary *)dict;

@end
