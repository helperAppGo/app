//// Header

#import "ItemViewController.h"
#import "QuestionListTableViewCell.h"
#import "GroupListTableViewCell.h"
#import "GBCommentsViewController.h"

@interface ItemViewController (){
    FeedModel *_feed;
    QuestionModel *_question;
    GroupListModel *_group;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ItemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _feed = self.info[@"feed"];
    _question = self.info[@"question"];
    _group = self.info[@"group"];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UINib *nib = [UINib nibWithNibName:@"QuestionListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"question"];
    {
        UINib *nib = [UINib nibWithNibName:@"GroupListTableViewCell" bundle:nil];
        [self.tableView registerNib:nib forCellReuseIdentifier:@"group"];
    }
}

-(UITableView *)table{
    return self.tableView;
}

-(NSMutableArray *)feedItems{
    return @[_feed].mutableCopy;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_feed.mediaObjects.count) {
        return 100;
    }
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_feed) {
        return [self feedCell:_feed indexPath:indexPath tableView:tableView];
    }
    if (_group) {
        GroupListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"group"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.contentView.backgroundColor = self.view.backgroundColor;
        GroupListModel *model = _group;
        [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"team"]];
        cell.nameLabel.text = model.name;
        cell.label.text = model.desc;
        cell.indexPath = indexPath;
        __weak typeof(self) weakSelf = self;
        [cell setBlock:^(NSIndexPath *path, NSInteger type) {
            [weakSelf cellBtnClicked:path type:type];
        }];
        cell.locationLabel.text = model.address;
        cell.categoryLabel.text = model.typeString;
        cell.memberLabel.text = model.folowerCountString;
        cell.locationBtn.hidden = model.address.length == 0;
        if (model.isMember || model.isRequested) {
            [cell.rightBtn setImage:[UIImage imageNamed:@"isMember"] forState:UIControlStateNormal];
        } else{
            [cell.rightBtn setImage:[UIImage imageNamed:@"addMember"] forState:UIControlStateNormal];
        }
        return cell;

    }
    if (_question) {
        QuestionListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"question"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        QuestionModel *model = _question;
        [cell.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
        cell.nameLabel.text = model.name;
        cell.timeLabel.text = model.timeString;
        cell.answerLabel.text = model.answerString;
        if (model.distance) {
            cell.locationBtn.hidden = NO;
            cell.locationLabel.hidden = NO;
            cell.locationLabel.text = model.distanceString;
        } else{
            cell.locationBtn.hidden = YES;
            cell.locationLabel.hidden = YES;
        }
        cell.categoryLabel.text = [NSString stringWithFormat:@"%@",@(model.categoryId)];
        
        cell.indexPath = indexPath;
        __weak typeof(self) weakSelf = self;
        [cell setClickBlock:^(NSIndexPath *path, FeedActionType type) {
            [weakSelf questionAction:path action:type];
        }];
        
        if (model.text.length) {
            cell.titleLabel.text = model.text;
        } else{
            cell.titleLabel.text = nil;
            cell.titleH.constant = 0;
        }
        
        if (model.mediaObjects.count) {
            FeedMediaObject *obj = model.mediaObjects[0];
            [cell.mediaView sd_setImageWithURL:[NSURL URLWithString:[obj imgUrl]]];
            
            if ([obj.type isEqualToString:@"image"]) {
                cell.playBtn.hidden = YES;
                cell.mediaH.constant = obj.h;
            }
            
            if ([obj.type isEqualToString:@"video"]) {
                cell.playBtn.hidden = NO;
                cell.mediaH.constant = obj.h;
            }
            
            if ([obj.type isEqualToString:@"youtube"]) {
                cell.playBtn.hidden = NO;
                cell.mediaH.constant = obj.h;
            }
        } else {
            cell.mediaH.constant = 0;
        }
        
        return cell;
    }
    
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_group) {
        [self openGroupInfo:_group];
    }
}

-(void)openAnswers:(QuestionModel *)model{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:model.objId itemType:ECommentItemTypeAnswer];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)questionAction:(NSIndexPath *)indexPath action:(FeedActionType )type{
    QuestionModel *model = _question;
    
    if (type == FeedActionTypeAnswers) {
        [self openAnswers:model];
    }
    
    if (type == FeedActionTypeImage) {
//        [self openImage:model];
    }
    
    if (type == FeedActionTypeLocation) {
        //        [self locationClicked:model indexPath:indexPath];
    }
    
    if (type == FeedActionTypeVideo) {
//        [self openVideo:model];
    }
    
    if (type == FeedActionTypeAction) {
        CDUser *user = [[SettingsManager instance] currentUser];
        UIAlertController  *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        if (model.userId == user.objId) {
            UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Видалити вопрос" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                //                [self deletePost:model];
            }];
            [alertController addAction:deleteAction];
            
        } else{
            
            UIAlertAction *complainPost = [UIAlertAction actionWithTitle:@"Поскаржитись на вопрос" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //                [self reportPost:model];
            }];
            [alertController addAction:complainPost];
            
            //            if (model.userId == model.wallId) {
            //                UIAlertAction *blockAlert = [UIAlertAction actionWithTitle:@"Заблокувати користувача" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            ////                    [self banUser:model];
            //                }];
            //                [alertController addAction:blockAlert];
            //            }
            
        }
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Відміна" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alertController addAction:actionCancel];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)cellBtnClicked:(NSIndexPath *)indexPath type:(NSInteger)type{
    if (type == 1) {
        [self memberClicked:indexPath];
    }
    if (type == 2) {
        [self followersClicked:indexPath];
    }
}

-(void)followersClicked:(NSIndexPath *)indexPath{
    GroupListModel *model = _group;
    BaseViewController *controller = [self createController:@"GroupMemberListViewController"];
    controller.type = 1;
    controller.info = @{@"group":model};
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)memberClicked:(NSIndexPath *)indexPath{
    GroupListModel *model = _group;
    NSDictionary *params = @{@"id_group":@(model.objId)};
    __weak typeof(self) weakSelf = self;
    [self showLoader];
    if (model.isRequested || model.isMember) {
        NSDictionary *dict = @{@"data":[params convertToJsonString]};
        [[ApiManager sharedManager] deleteRequest:@"groups/members" params:dict callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                model.isMember = 0;
                model.isRequested = 0;
                if (model.type != 0) {
                    model.followersCount -= 1;
                }
                [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    } else{
        [[ApiManager sharedManager] postRequest:@"groups/members" params:params callback:^(id response, ErrorObj *error) {
            [weakSelf hideLoader];
            if (response) {
                model.isMember = 1;
                if (model.type == 0) {
                    model.isRequested = 1;
                } else{
                    model.followersCount += 1;
                }
                [weakSelf.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            } else{
                [weakSelf showErrorMessage:error.message];
            }
        }];
    }
}


@end
