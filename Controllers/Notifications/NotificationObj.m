//// Header

#import "NotificationObj.h"
#import "NSString+MD5Addition.h"

@implementation NotificationObj

+(NSInteger)appBadge{
    return 34;
}

- (id)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.globalType = [NULL_TO_NIL(dict[@"global_type"]) integerValue];
        self.itemId = [NULL_TO_NIL(dict[@"id_item"]) integerValue];
        self.objId = [NULL_TO_NIL(dict[@"id_notification"]) integerValue];
        
        self.subItemId = [NULL_TO_NIL(dict[@"id_subitem"]) integerValue];
        self.userId = [NULL_TO_NIL(dict[@"id_user"]) integerValue];
        self.read = [NULL_TO_NIL(dict[@"is_read"]) integerValue];
        
        {
            NSString *typeItem = NULL_TO_NIL(dict[@"type_item"]);
            self.itemType = [self itemTypeWithString:typeItem];
            NSString *typeSubItem = NULL_TO_NIL(dict[@"type_subitem"]);
            self.subItemType = [self itemTypeWithString:typeSubItem];
        }
        
        {
          
            NSString *typeNotification = NULL_TO_NIL(dict[@"type_notification"]);
            if ([typeNotification isEqualToString:@"follow"]) {
                self.notificationType = NotificationTypeFriendRequest;
                
            } else if ([typeNotification isEqualToString:@"accepted"]) {
                self.notificationType = NotificationTypeFriendRequestAccepted;
                
            } else if ([typeNotification isEqualToString:@"reference"]) {
                self.notificationType = NotificationTypeFriendRequest;
                
            } else if ([typeNotification isEqualToString:@"post"]) {
                self.notificationType = NotificationTypePost;
                
            } else if ([typeNotification isEqualToString:@"repost"]) {
                self.notificationType = NotificationTypeRepost;
                
            } else if ([typeNotification isEqualToString:@"like"]) {
                self.notificationType = NotificationTypeLike;
                
            } else if ([typeNotification isEqualToString:@"comment"]) {
                self.notificationType = NotificationTypeComment;
                
            } else if ([typeNotification isEqualToString:@"answer"]) {
                self.notificationType = NotificationTypeAnswer;
                
            } else if ([typeNotification isEqualToString:@"registration"]) {
                self.notificationType = NotificationTypeRegistration;
                
            } else if ([typeNotification isEqualToString:@"avatar"]) {
                self.notificationType = NotificationTypeAvatar;
                
            } else if ([typeNotification isEqualToString:@"question"]) {
                self.notificationType = NotificationTypeQuestion;
                
            } else{
                self.notificationType = NotificationTypeFriendRequest;
            }
        }
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ssZ";
        self.date = [formatter dateFromString:NULL_TO_NIL(dict[@"time"])];
        self.dict = dict;
        
        //self.topString = [self generateTopString];
        
    }
    return self;
}

-(NSInteger)itemTypeWithString:(NSString *)string{
    /*
     case "question": return ITEM_TYPE_QUESTION;
     case "post": return ITEM_TYPE_POST;
     case "answer": return ITEM_TYPE_ANSWER;
     case "comment": return ITEM_TYPE_COMMENT;
     case "user": return ITEM_TYPE_USER;
     case "group": return ITEM_TYPE_GROUP;
     default: return ITEM_TYPE_UNKNOWN;
     */
    
    if ([string isEqualToString:@"question"]) {
        return NotificationItemQuestion;
        
    } else if ([string isEqualToString:@"post"]) {
        return NotificationItemPost;
        
    } else if ([string isEqualToString:@"answer"]) {
        return NotificationItemAnswer;
        
    } else if ([string isEqualToString:@"comment"]) {
        return  NotificationItemComment;
        
    } else if ([string isEqualToString:@"group"]) {
        return NotificationItemGroup;
        
    }else if ([string isEqualToString:@"user"]) {
        return NotificationItemUser;
        
    } else{
        return NotificationItemUnknown;
    }
}


-(NSAttributedString *)generateTopString{
    
    NSInteger langId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"] integerValue];
    NSString *resultString = @"";
    if (self.globalType == 1) {
        // For post
        if (self.notificationType == NotificationTypeLike) {
            if (self.itemType == NotificationItemPost) {
                if (langId == 1) {
                    resultString = @"подобається ваш пост";
                } else if (langId == 3) {
                    resultString = @"like your post";
                } else{
                    resultString = @"нравиться ваш пост";
                }
                
            }
            if (self.itemType == NotificationItemComment) {
                if (langId == 1) {
                    resultString = @"подобається ваш коментарій";
                } else if (langId == 3) {
                    resultString = @"like your comment";
                } else{
                    resultString = @"нравиться ваш коммент";
                }
                
            }
        }
        if (self.notificationType == NotificationTypeComment) {
            if (langId == 1) {
                resultString = @"прокоментував (-ла)";
            } else if (langId == 3) {
                resultString = @"commented";
            } else{
                resultString = @"прокоментировал(-ла)";
            }
        }
        if (self.notificationType == NotificationTypeRegistration) {
            if (langId == 1) {
                resultString = @"Зареєструвався новий користувач! Привітайте новачка!";
            } else if (langId == 3) {
                resultString = @"A new user was registered! Congratulate the novice!";
            } else{
                resultString = @"Зарегистрировался новый пользователь! Поздравьте новачка!";
            }
            
        }
        if (self.notificationType == NotificationTypeAvatar) {
            if (langId == 1) {
                resultString = @"оновив (-ла) фото профілю";
            } else if (langId == 3) {
                resultString = @"updated profile photo";
            } else{
                resultString = @"обновил(-ла) фото профиля";
            }
            
        }
        if (self.notificationType == NotificationTypeFriendRequest) {
            if (langId == 1) {
                resultString = @"відправив (-ла) вам заявку в друзі";
            } else if (langId == 3) {
                resultString = @"sent you friend request";
            } else{
                resultString = @"отправил(-ла) вам заявку в друзья";
            }
        }
        if (self.notificationType == NotificationTypeFriendRequestAccepted) {
            if (langId == 1) {
                resultString = @"теперь ваш друг";
            } else if (langId == 3) {
                resultString = @"now your friend";
            } else{
                resultString = @"теперь ваш друг";
            }
           
            
        }
        NSString *userName = self.user.nameString;
        userName = userName ? userName : @"";
        NSString *commentText = resultString;
        NSString *resultString = [NSString stringWithFormat:@"%@ %@",userName, commentText];
        NSMutableAttributedString *atr =  [[NSMutableAttributedString alloc] initWithString:resultString];
        NSDictionary *params = @{NSFontAttributeName : [UIFont defaultFontWithSize:13 bold:NO],
                                 NSUnderlineStyleAttributeName : @(1)};
        [atr addAttributes:params range:[resultString rangeOfString:commentText]];
        NSDictionary *params2 = @{NSFontAttributeName : [UIFont defaultFontWithSize:13 bold:YES]};
        [atr addAttributes:params2 range:[resultString rangeOfString:userName]];
        
        return atr;
        
    }
    
    if (self.globalType == 2) {
        //        Global type = 2
        //        Group
        if ([[self.user nameString] containsString:@"null"]) {
            return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",self.group.name]];
        }
        return [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ > %@", self.user.nameString, self.group.name]];
        
        
    }
    /*
     if (langId == 1) {
     resultString = @"теперь ваш друг";
     } else if (langId == 3) {
     resultString = @"now your friend";
     } else{
     resultString = @"теперь ваш друг";
     }
     
     asked
     answered your question
     like your answer
     
     added post

     */
    if (self.globalType == 3) {
        NSString *commentText = @"";
        NSString *userName = self.user.nameString;
        if (self.notificationType == NotificationTypeQuestion) {
            if (langId == 1) {
                commentText = @"Запитав (-ла)";
            } else if (langId == 3) {
                commentText = @"asked";
            } else{
                commentText = @"Спросил(-ла)";
            }
        }
        if (self.notificationType == NotificationTypeAnswer) {
            if (langId == 1) {
                commentText = @"відповів (-ла) на ваше запитання";
            } else if (langId == 3) {
                commentText = @"answered your question";
            } else{
                commentText = @"ответил(-ла) на ваш вопрос";
            }
            
        }
        if (self.notificationType == NotificationTypeLike) {
            if (langId == 1) {
                commentText = @"подобається ваша відповідь";
            } else if (langId == 3) {
                commentText = @"like your answer";
            } else{
                commentText = @"нравиться ваш ответ";
            }
            
        }
        userName  = userName ? userName : @"";
        resultString = [NSString stringWithFormat:@"%@ %@",userName, commentText];
        NSMutableAttributedString *atr =  [[NSMutableAttributedString alloc] initWithString:resultString];
        NSDictionary *params = @{NSFontAttributeName : [UIFont defaultFontWithSize:13 bold:NO],
                                 NSUnderlineStyleAttributeName : @(1)};
        [atr addAttributes:params range:[resultString rangeOfString:commentText]];
        NSDictionary *params2 = @{NSFontAttributeName : [UIFont defaultFontWithSize:13 bold:YES]};
        [atr addAttributes:params2 range:[resultString rangeOfString:userName]];
        
        return atr;
        
    }
    //    Global type = 3
    //    create question (Спросил(-ла):
    //                     answers (ответил(-ла) на ваш вопрос)
    //                     like answers (нравиться ваш ответ)
    
    
    //    Type:
    //        post_like (нравиться)
    //        comments post (прокоментировал(-ла):
    //                       Registration (Зарегистрировался новый пользователь! Поздравьте новачка!
    //                                     like comment (нравиться комментарий)
    //                                     avatar post (обновил(-ла) фото профиля)
    //                                     user follow (отправил(-ла) вам заявку в друзья)
    //                                     accepted user (Теперь ваш друг)
    //
    //    }
    return [[NSAttributedString alloc] initWithString:@"top string"];
}


-(NSString *)descString{
    NSInteger langId = [[[NSUserDefaults standardUserDefaults] objectForKey:@"appLanguage"] integerValue];
    
    if (self.globalType == 2) {
        // Group
        //добавил запись :
        if ([[self.user nameString] containsString:@"null"]) {
            return self.feed.text;
        }
        NSString *commentText = @"";
        if (langId == 1) {
            commentText = @"додав запис";
        } else if (langId == 3) {
            commentText = @"added post";
        } else{
            commentText = @"добавил запись";
        }
        NSString *enter = [NSString stringWithFormat:@"%@\n",commentText];
        return [NSString stringWithFormat:@"%@%@", enter,self.feed.text];
        
        
    }
    if (self.globalType == 3) {
        if (self.notificationType == NotificationTypeAnswer) {
            return self.answer.text;
        }
        if (self.notificationType == NotificationTypeQuestion) {
            return self.question.text;
        }
        return self.question.text;
    }
    
    if (self.notificationType == NotificationTypeComment) {
        return self.answer.text;
    }
    
    return self.feed.text;
}

#pragma mark - Parsing method
+(NotificationObj *)parsing:(NSDictionary *)dict users:(NSArray *)users groups:(NSArray *)groups answers:(NSArray *)answers comments:(NSArray *)comments posts:(NSArray *)posts questions:(NSArray *)questions{
    
    NotificationObj * notification = [[NotificationObj alloc] initWithDict:dict];
    if (notification.notificationType == NotificationTypeAnswer){
        QuestionAnswerModel *answer = [[QuestionAnswerModel alloc] initWithDict:[self searchItem:answers itemId:notification.itemId]];
        QuestionModel *question = [[QuestionModel alloc] initWithDict:[self searchItem:questions itemId:answer.questionId]];
        notification.answer = answer;
        notification.question = question;
        notification.user = [UserObj createUser:[self searchItem:users itemId:answer.userId]];
        
    }
    
    if (notification.notificationType == NotificationTypeComment){
        QuestionAnswerModel *answer = [[QuestionAnswerModel alloc] initWithDict:[self searchItem:comments itemId:notification.itemId]];
        FeedModel *feed = [[FeedModel alloc] initWithDict:[self searchItem:posts itemId:answer.postId]];
        notification.answer = answer;
        notification.feed = feed;
        notification.user = [UserObj createUser:[self searchItem:users itemId:notification.subItemId]];
        
    }
    
    
    //        switch (notification.getNotificationType()) {
    //            case Notification.TYPE_ANSWER:
    //                Answer answer = answersIdMap.get(notification.getItemId());
    //                notification.setAnswer(answer);
    //                notification.setQuestion(questionsIdMap.get(answer.getQuestionId()));
    //                notification.setUser(usersIdMap.get(answer.getUserId()));
    //                break;
    
    //            case Notification.TYPE_COMMENT:
    //                Answer comment = commentsIdMap.get(notification.getItemId());
    //                notification.setAnswer(comment);
    //                notification.setQuestion(postsIdMap.get(comment.getPostId()));
    //                notification.setUser(usersIdMap.get(comment.getUserId()));
    //                break;
    
    if (notification.notificationType == NotificationTypeLike) {
        notification.user = [UserObj createUser:[self searchItem:users itemId:notification.subItemId]];
        
        if (notification.itemType  == NotificationItemPost) {
            notification.feed = [[FeedModel alloc] initWithDict:[self searchItem:posts itemId:notification.itemId]];
            
        } else if(notification.itemType == NotificationItemComment){
            notification.answer = [[QuestionAnswerModel alloc] initWithDict:[self searchItem:comments itemId:notification.itemId]];
            notification.feed = [[FeedModel alloc] initWithDict:[self searchItem:questions itemId:notification.answer.postId]];
            
        } else if(notification.itemType == NotificationItemAnswer){
            notification.answer = [[QuestionAnswerModel alloc] initWithDict:[self searchItem:answers itemId:notification.itemId]];
            notification.question = [[QuestionModel alloc] initWithDict:[self searchItem:questions itemId:notification.answer.questionId]];
            
        }
    }
    
    //            case Notification.TYPE_LIKE:
    //                int itemType = notification.getItemType();
    //                notification.setUser(usersIdMap.get(notification.getSubItemId()));
    //                if (itemType == Notification.ITEM_TYPE_POST) {
    //                    Question postLike = postsIdMap.get(notification.getItemId());
    //                    notification.setQuestion(postLike);
    //                }
    //                else if (itemType == Notification.ITEM_TYPE_COMMENT) {
    //                    Answer commentLike = commentsIdMap.get(notification.getItemId());
    //                    Question postLike = postsIdMap.get(commentLike.getPostId());
    //                    notification.setAnswer(commentLike);
    //                    notification.setQuestion(postLike);
    //                }
    //                else if (itemType == Notification.ITEM_TYPE_ANSWER) {
    //                    Answer answerLike = answersIdMap.get(notification.getItemId());
    //                    Question questionLike = questionsIdMap.get(answerLike.getQuestionId());
    //                    notification.setAnswer(answerLike);
    //                    notification.setQuestion(questionLike);
    //                }
    //                break;
    
    if (notification.notificationType == NotificationTypePost ||
        notification.notificationType == NotificationTypeRepost ||
        notification.notificationType == NotificationTypeAvatar) {
        if (notification.subItemType == NotificationItemGroup) {
            notification.group = [[GroupListModel alloc] initWithDict:[self searchItem:groups itemId:notification.subItemId]];
            notification.feed = [[FeedModel alloc] initWithDict:[self searchItem:posts itemId:notification.itemId]];
            notification.user = [UserObj createUser:[self searchItem:users itemId:notification.feed.userId]];
            
        } else{
            notification.question = [[QuestionModel alloc] initWithDict:[self searchItem:questions itemId:notification.itemId]];
            notification.user = [UserObj createUser:[self searchItem:users itemId:notification.subItemId]];
        }
        
    }
    //            case Notification.TYPE_POST:
    //            case Notification.TYPE_REPOST:
    //            case Notification.TYPE_AVATAR:
    //                Question postLike = postsIdMap.get(notification.getItemId());
    //                notification.setQuestion(postLike);
    //                notification.setUser(usersIdMap.get(notification.getSubItemId()));
    //                // Group post
    //                if (notification.getSubItemType() == Notification.ITEM_TYPE_GROUP)
    //                    notification.setGroup(groupsIdMap.get(-postLike.getWallId()));
    //                break;
    
    //            case Notification.TYPE_QUESTION:
    //                Question question = questionsIdMap.get(notification.getItemId());
    //                notification.setQuestion(question);
    //                notification.setUser(usersIdMap.get(notification.getSubItemId()));
    //                break;
    
    if (notification.notificationType == NotificationTypeFriendRequest ||
        notification.notificationType == NotificationTypeFriendRequestAccepted ||
        notification.notificationType == NotificationTypeRegistration) {
        notification.user = [UserObj createUser:[self searchItem:users itemId:notification.itemId]];
    }
    //            case Notification.TYPE_FRIEND_REQUEST:
    //            case Notification.TYPE_FRIEND_REQUEST_ACCEPTED:
    //            case Notification.TYPE_REGISTRATION:
    //                User user = usersIdMap.get(notification.getItemId());
    //                notification.setUser(user);
    //                break;
    //        }
    if (notification.notificationType == NotificationTypeQuestion) {
        notification.question = [[QuestionModel alloc] initWithDict:[self searchItem:questions itemId:notification.itemId]];
        notification.user = [UserObj createUser:[self searchItem:users itemId:notification.subItemId]];
    }
    notification.topString = [notification generateTopString];
    return notification;
}




+(id)searchItem:(NSArray *)items itemId:(NSInteger)itemId{
    for (NSDictionary *dict in items) {
        NSInteger objId = [dict[@"id"] integerValue];
        if (objId == itemId) {
            return dict;
            break;
        }
    }
    return @{};
}

@end
