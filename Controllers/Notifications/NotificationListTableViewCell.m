//// Header

#import "NotificationListTableViewCell.h"

@implementation NotificationListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.containerView.layer.borderColor = [[UIColor whiteColor] colorWithAlphaComponent:0.6].CGColor;
    self.containerView.layer.borderWidth = 0.7;
    self.containerView.layer.cornerRadius = 5;
    
    self.avatarBtn.layer.cornerRadius = self.avatarBtn.width/2;
    self.avatarBtn.clipsToBounds = YES;
    self.avatarBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarBtn.layer.borderWidth = 0.8;
    
    self.topLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.descLabel.font = [UIFont defaultFontWithSize:14 bold:NO];
    self.topLabel.textColor = [UIColor whiteColor];
    self.descLabel.textColor = [UIColor whiteColor];
    
    self.timeLabel.font = [UIFont defaultFontWithSize:12 bold:NO];
    self.timeLabel.textColor = [UIColor whiteColor];
    self.timeLabel.adjustsFontSizeToFitWidth = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)layoutSubviews{
    [super layoutSubviews];
    self.containerView.layer.cornerRadius = 8;
    self.containerView.layer.masksToBounds = false;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 0);
    self.containerView.layer.shadowColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8].CGColor;
    self.containerView.layer.shadowOpacity = 0.7;
    self.containerView.layer.shadowRadius = 4;
}

@end
