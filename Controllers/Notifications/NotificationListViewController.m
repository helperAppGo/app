//// Header

#import "NotificationListViewController.h"
#import "NotificationListTableViewCell.h"
#import <AFNetworking/UIButton+AFNetworking.h>
#import "LoaderCell.h"
#import "GBCommentsViewController.h"

@interface NotificationListViewController (){
    NSInteger _notificationType;
    NSMutableArray *_list;
    UIRefreshControl *_refreshControl;
    BOOL _complete;
}
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation NotificationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    [self.backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.topLabel.font  = [UIFont defaultFontWithSize:14 bold:NO];
    self.topLabel.textColor = [UIColor whiteColor];
    self.topLabel.text = @"Уведомления";
    self.topLabel.superview.backgroundColor = [UIColor blackColor];
    
    _titleLabel.text = @"Уведомления";
    _list = @[].mutableCopy;
    UINib *nib = [UINib nibWithNibName:@"NotificationListTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"list"];
    
    UINib *loaderNib = [UINib nibWithNibName:@"LoaderCell" bundle:nil];
    [self.tableView registerNib:loaderNib forCellReuseIdentifier:@"loader"];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    self.tableView.backgroundColor = [UIColor blackColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorInset = UIEdgeInsetsZero;

    _refreshControl = [[UIRefreshControl alloc] init];
    [self.tableView addSubview:_refreshControl];
    [_refreshControl addTarget:self action:@selector(getItemsNow) forControlEvents:UIControlEventValueChanged];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
}

-(void)readNotifications{
    [[ApiManager sharedManager] postRequest:@"" params:@{} callback:^(id response, ErrorObj *error) {
        NSLog(@"%@",response);
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

-(NSString *)typeString{
    return self.info[@"type"];
}

-(void)getItemsNow{
    NSDictionary *params = @{@"limit":@(25),
                             @"offset":@(0),
                             @"type":[self typeString]};
    
    __weak typeof(self) weakSelf = self;
    
    [[ApiManager sharedManager] getRequest:@"notifications/last" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        [_refreshControl endRefreshing];
        if (response) {
            [_list removeAllObjects];
            [weakSelf parsingResponse:response];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}
-(void)clearNotifications{
    if ([self.info[@"type"] isEqualToString:@"user"]) {
        [[SettingsManager instance] saveObject:nil key:@"notification1"];
        
    } else if([self.info[@"type"] isEqualToString:@"question"]){
        [[SettingsManager instance] saveObject:nil key:@"notification2"];
        
    } else if([self.info[@"type"] isEqualToString:@"group"]){
        [[SettingsManager instance] saveObject:nil key:@"notification3"];
    }
}

-(void)getItems{
    // type ['user', 'group', 'question'] (can be empty)
    if (_complete) {
        return;
    }
    
    NSDictionary *params = @{@"limit":@(25),
                           @"offset":@(_list.count),
                           @"type":[self typeString]};
    
    __weak typeof(self) weakSelf = self;
    _complete = YES;
    [[ApiManager sharedManager] getRequest:@"notifications/last" params:@{@"data":[params convertToJsonString]} callback:^(id response, ErrorObj *error) {
        if (response) {
            [weakSelf clearNotifications];
            [weakSelf parsingResponse:response];
            [weakSelf.tableView reloadData];
        } else{
            [weakSelf showErrorMessage:error.message];
        }
    }];
}

-(void)parsingResponse:(NSDictionary *)response{
    NSDictionary *dict = NULL_TO_NIL(response[@"response"]);
    NSArray *notifications = NULL_TO_NIL(dict[@"notifications"]);
    
    NSArray *answers = NULL_TO_NIL(dict[@"answers"]);
    NSArray *groups = NULL_TO_NIL(dict[@"groups"]);
    NSArray *comments = NULL_TO_NIL(dict[@"comments"]);
    NSArray *posts = NULL_TO_NIL(dict[@"posts"]);
    NSArray *questions = NULL_TO_NIL(dict[@"questions"]);
    NSArray *users = NULL_TO_NIL(dict[@"users"]);
    _complete = notifications.count < 25;
    
    for (NSDictionary *dict in notifications) {
        NotificationObj *notification  = [[NotificationObj alloc] initWithDict:dict];
        if (notification.globalType <= 0) {
            continue;
        }
        [_list addObject:[NotificationObj parsing:dict users:users groups:groups answers:answers comments:comments posts:posts questions:questions]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

#pragma mark - UITableView delegate methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count + 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count){
        LoaderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loader"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.loader.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        cell.contentView.backgroundColor = [UIColor blackColor];
        if (_complete) {
            [cell.loader stopAnimating];
            cell.loader.hidden = YES;
        } else{
            [cell.loader startAnimating];
            cell.loader.hidden = NO;
        }
        return cell;
    }
    NotificationListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"list"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.containerView.backgroundColor = [UIColor blackColor];
    cell.contentView.backgroundColor=  [UIColor blackColor];
    NotificationObj *obj = _list[indexPath.row];
    cell.topLabel.attributedText = obj.topString;
    cell.timeLabel.text = [obj.date timeString];
    cell.descLabel.text = obj.descString;
    NSString *avatar = obj.globalType == 2 ? obj.group.avatar : obj.user.avatar;
    [cell.avatarBtn setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:avatar] placeholderImage:[UIImage imageNamed:@"user_ava"]];
    return cell;
}

-(void)questionCliced:(NotificationObj *)notification{
    if (notification.notificationType == NotificationTypeAvatar) {
        [self getUserProfile:notification.subItemId];
        return;
    }
    
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    NSString *str = [@{@"id":@(notification.question.objId)} convertToJsonString];
    [[ApiManager sharedManager] getRequest:@"questions" params:@{@"data":str} callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            QuestionModel *model = [[QuestionModel alloc] initWithDict:response[@"response"]];
            [weakSelf openQuestion:model];
        }
    }];
}

-(void)answersClicked:(NotificationObj *)notification{
//    [self showLoader];
//    __weak typeof(self) weakSelf = self;
//    NSString *str = [@{@"id":@(notification.question.objId)} convertToJsonString];
//    [[ApiManager sharedManager] getRequest:@"questions" params:@{@"data":str} callback:^(id response, ErrorObj *error) {
//        [weakSelf hideLoader];
//        if (response) {
//            QuestionModel *model = [[QuestionModel alloc] initWithDict:response[@"response"]];
//            [weakSelf openQuestion:model];
//        }
//    }];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:notification.question.objId itemType:ECommentItemTypeAnswer];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
    [[SettingsManager instance] anySound];
}

-(void)groupCliked:(NotificationObj *)notification{
    if (notification.notificationType == NotificationTypeAvatar) {
        [self getUserProfile:notification.subItemId];
        return;
    }
    if (notification.notificationType == NotificationTypeComment) {
        [self openComments:notification.group.objId];
        return;
    }

    BaseViewController *controller = [self createController:@"GroupFeedsViewController"];
    controller.type = 1;
    controller.hidesBottomBarWhenPushed = YES;
    controller.info = @{@"group":notification.group};
    [self.navigationController pushViewController:controller animated:YES];
    
//    NSInteger grId = ABS(notification.group.objId);
//    NSDictionary *params = @{@"data": [@{@"id":@(grId)} convertToJsonString]};
//    __weak typeof(self) weakSelf = self;
//    [self showLoader];
//    [[ApiManager sharedManager] getRequest:@"groups" params:params callback:^(id response, ErrorObj *error) {
//        [weakSelf hideLoader];
//        if (response) {
//            GroupListModel *model = [[GroupListModel alloc] initWithDict:response[@"response"]];
//            [weakSelf openGroup:model];
//        }
//    }];
}

-(void)openGroup:(GroupListModel *)model{
    BaseViewController *controller = [self createController:@"ItemViewController"];
    controller.type = 1;
    controller.info = @{@"group":model};
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)openQuestion:(QuestionModel *)model{
    BaseViewController *controller = [self createController:@"ItemViewController"];
    controller.type = 1;
    controller.info = @{@"question":model};
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)postClicked:(NotificationObj *)notification{
    if (notification.notificationType == NotificationTypeAvatar) {
        [self getUserProfile:notification.subItemId];
        return;
    }
    if (notification.notificationType == NotificationTypeComment) {
        [self openComments:notification.feed.objId];
        return;
    }
    
    [self showLoader];
    __weak typeof(self) weakSelf = self;
    NSString *str = [@{@"id":@(notification.feed.objId)} convertToJsonString];
    [[ApiManager sharedManager] getRequest:@"posts" params:@{@"data":str} callback:^(id response, ErrorObj *error) {
        [weakSelf hideLoader];
        if (response) {
            FeedModel *model = [[FeedModel alloc] initWithDict:response[@"response"]];
            [weakSelf openPost:model];
        }
    }];
}

-(void)openPost:(FeedModel *)model{
    BaseViewController *controller = [self createController:@"ItemViewController"];
    controller.type = 1;
    controller.info = @{@"feed":model};
    [self.navigationController pushViewController:controller animated:YES];

}

-(void)openComments:(NSInteger)objId{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    GBCommentsViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"GBCommentsViewController"];
    [vc configureWithSourceId:objId itemType:ECommentItemTypeFeedComment];
    vc.type = 1;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NotificationObj *obj = _list[indexPath.row];
    NotificationType type = obj.notificationType;
    NSString *str = self.info[@"type"];
    if (obj.itemType == NotificationItemAnswer) {
        [self answersClicked:obj];
        return;
    }
    if (obj.itemType == NotificationItemPost) {
        if (obj.subItemType == NotificationItemGroup) {
            [self groupCliked:obj];
        } else{
            [self postClicked:obj];
        }
        
        return;
    }
    if (([str isEqualToString:@"user"] && type == NotificationTypeComment) || obj.itemType  == NotificationItemComment) {
        [self openComments:obj.answer.postId];
        return;
    }
    
    if (type == NotificationTypeComment ||
        type == NotificationTypeLike ||
        type == NotificationTypePost ||
        type == NotificationTypeAvatar ||
        type == NotificationTypeQuestion ||
        type == NotificationTypeRepost) {

        
        if (obj.subItemType == NotificationItemGroup) {
            [self groupCliked:obj];
        }
        if (obj.subItemType == NotificationItemComment) {
            [self postClicked:obj];
        }
        if (obj.subItemId == NotificationItemPost) {
            [self postClicked:obj];
        }
        if (obj.subItemType == NotificationItemQuestion) {
            [self questionCliced:obj];
        }
        if (obj.subItemType == NotificationItemUser) {
            [self openUserProfile:obj.user];
        }
        
    }
   
    if (type == NotificationTypeFriendRequest ||
        type == NotificationTypeFriendRequestAccepted ||
        type == NotificationTypeRegistration){
        [self openUserProfile:obj.user];
    }
    if (type == NotificationItemPost) {
        [self postClicked:obj];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _list.count) {
        [self getItems];
    }
}

@end
