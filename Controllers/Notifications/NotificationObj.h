//// Header

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Globals.h"
#import "UserObj.h"
#import "FeedModel.h"
#import "QuestionModel.h"
#import "UIFont+Additions.h"
#import "GroupListModel.h"

/*
 "global_type" = 1;
 "id_item" = 16883;
 "id_notification" = 15599;
 "id_subitem" = 1544;
 "id_user" = 1524;
 "is_read" = 0;
 time = "2018-08-03T01:34:35+03:00";
 "type_item" = post;
 "type_notification" = like;
 "type_subitem" = user;

 */
typedef NS_ENUM(NSInteger, NotificationType) {
    NotificationTypeUnknown =  0,
    NotificationTypeFriendRequest,
    NotificationTypeFriendRequestAccepted,
    NotificationTypeReference,
    NotificationTypePost,
    NotificationTypeRepost,
    NotificationTypeLike,
    NotificationTypeComment,
    NotificationTypeAnswer,
    NotificationTypeRegistration,
    NotificationTypeAvatar,
    NotificationTypeQuestion,
};

typedef NS_ENUM(NSInteger, NotificationItemType) {
    NotificationItemUnknown =  0,
    NotificationItemUser,
    NotificationItemPost,
    NotificationItemQuestion,
    NotificationItemComment,
    NotificationItemAnswer,
    NotificationItemGroup,
    
};

//@class UserObj,FeedModel,GroupListModel,QuestionModel,QuestionAnswerModel;

@interface NotificationObj : NSObject

+(NSInteger)appBadge;

@property (nonatomic, assign) NSInteger globalType;
@property (nonatomic, assign) NSInteger itemId;
@property (nonatomic, assign) NSInteger objId;
@property (nonatomic, assign) NSInteger subItemId;
@property (nonatomic, assign) NSInteger userId;

@property (nonatomic, assign) NSInteger read;
@property (nonatomic, strong) NSDictionary *dict;
@property (nonatomic, strong) NSDate *date;

@property (nonatomic, assign) NotificationItemType subItemType;
@property (nonatomic, assign) NotificationItemType itemType;
@property (nonatomic, assign) NotificationType notificationType;

@property (nonatomic, strong) UserObj *user;
@property (nonatomic, strong) FeedModel *feed;
@property (nonatomic, strong) GroupListModel *group;
@property (nonatomic, strong) QuestionModel *question;
@property (nonatomic, strong) QuestionAnswerModel *answer;

@property (nonatomic, strong) NSDictionary *answerDict;
@property (nonatomic, strong) NSDictionary *postDict;


@property (nonatomic, strong) NSAttributedString *topString;
@property (nonatomic, strong) NSString *descString;
-(NSAttributedString *)generateTopString;
- (id)initWithDict:(NSDictionary *)dict;

+(NotificationObj *)parsing:(NSDictionary *)dict users:(NSArray *)users groups:(NSArray *)groups answers:(NSArray *)answers comments:(NSArray *)comments posts:(NSArray *)posts questions:(NSArray *)questions;


@end
