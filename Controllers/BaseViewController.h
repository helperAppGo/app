//// Header

#import <UIKit/UIKit.h>
#import "UIBarButtonItem+PPBadgeView.h"
#import "PPBadgeView.h"

@class UserObj;

@interface BaseViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    UILabel *_titleLabel;
}
@property (nonatomic, strong) NSDictionary *info;
@property (nonatomic, assign) NSInteger type;

-(void)back;
-(void)showLoader;
-(void)hideLoader;
-(void)showErrorMessage:(NSString *)message;
-(void)reloadTableView;

-(void)addBadge:(UIBarButtonItem *)item count:(NSInteger)count;

-(void)soundForLike;

-(BaseViewController *)createController:(NSString *)controllerName;
-(void)getUserProfile:(NSInteger)userId;
-(void)openUserProfile:(UserObj*)user;
-(void)showAlert:(NSString *)message callback:(Callback)callback;
-(void)changeRelation:(UserObj *)user tableView:(UITableView*)tableView index:(NSIndexPath *)indexPath;
-(void)addRecomendationFriend:(UserObj *)user tableView:(UITableView *)tableView;

@end
