//// Header

#import "BaseViewController.h"

#import <NYTPhotoViewer/NYTPhotosViewController.h>
#import <NYTPhotoViewer/NYTPhoto.h>
#import <NYTPhotoViewer/NYTPhotoViewerArrayDataSource.h>
#import "PhotoViewController.h"
#import "UserPhotoView.h"


@interface BaseFeedViewController : BaseViewController
@property (nonatomic) NYTPhotoViewerArrayDataSource *dataSource;

-(void)openAvatar:(NSString *)model;
-(UITableView *)table;
-(NSMutableArray *)feedItems;
-(void)feedAction:(NSIndexPath *)path model:(FeedModel *)model action:(FeedActionType)type sender:(UIButton *)sender;
-(UITableViewCell *)feedCell:(FeedModel *)model indexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView;
-(void)feedClicked:(NSIndexPath *)indexPath action:(FeedActionType)type sender:(UIButton *)sender;

-(void)openGroupInfo:(GroupListModel *)model;

@end
